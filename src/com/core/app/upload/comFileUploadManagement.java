/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.app.upload;

import java.io.File;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.framework.core.IRequest;

import com.core.component.sql.ISqlManagement;
import com.core.component.upload.FileUploadException;
import com.core.component.upload.internal.JakartaFileUploadManagement;

import com.core.app.util.appUtils;

/**
 * <code>Jakarta Commpons FileUpload(http://jakarta.apache.org/commons/fileupload/)</code>
 * 라이브러리를 이용해서 파일 업로드를 담당하는 컴포넌트.
 * 
 * @since 2.0
 */
public class comFileUploadManagement extends JakartaFileUploadManagement {

    private static final long serialVersionUID = 5876327354220929076L;

    private Log log = LogFactory.getLog(FileUploadManagement.class);
    
    protected ISqlManagement sm;

    /**
     * 파일을 업로드할 때는 type이 file인 input element들의 name 속성은 하나로 정해져 있어야 한다.
     * <p>
     * &lt;input type="file" name="j_file"/&gt;
     * </p>
     * 기본값은 j_file이며, 프로젝트에서 변경이 필요할 경우 이 메소드를 재정의한다.
     * (JControl을 이용한 개발을 할 경우엔 javascript도 신경써야 한다)
     * 
     * @param request  
     * @return
     */
    protected String getFileParamKey(IRequest request) {
        return "j_file";
    }

    public void setSqlManagement(ISqlManagement sm) {
        this.sm = sm;
    }

    @Override
	public void createUpload(String docId, IRequest request) {
        createUpload(request.getPath(), docId, request);
    }

    @Override
	public void createUpload(String programId, String docId, IRequest request) {
    	
    	log.info("-------createUpload--------\n" + request.getMap().toString());
    	
        String files[] = request.getStringParams(getFileParamKey(request));
        /*String types[] = request.getStringParams(getFileParamKey(request) + "_type");*/
        if(files == null || files.length == 0)
            return;
        String filePaths[] = request.getStringParams(getFileParamKey(request) + "_dir");
        String fileNames[] = request.getStringParams(getFileParamKey(request) + "_file");
        String fileSize[] = request.getStringParams(getFileParamKey(request) + "_size");
        Map param =appUtils.getAuditableMap(request);
        log.info("========programId : "+programId+"==docId : "+docId);
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        
/*        log.info("========files : "+files[0] +"==filePaths : "+ filePaths[0] +
        		"==fileNames : "+fileNames[0] +"==fileSize : "+ fileSize[0]);*/
        
        Map smap = request.getMap();
        smap.put("j_file_program", programId);
        smap.put("j_file_docId", docId);
        smap.put("j_file_path", filePaths[0]);
        smap.put("j_file_name", fileNames[0]);
        smap.put("j_file_size", fileSize[0]);
        
        for(int i = 0; i < fileNames.length; i++)
        {
            // if(types != null) param.put("j_file_type", types[i]);
        	/*String[] fileTypes = fileNames[i].split(".");
        	if(fileTypes.length > 1) {
        		param.put("j_file_type", fileTypes[1]);
        	}
            param.put("j_file_path", filePaths[i]);
            param.put("j_file_name", fileNames[i]);
            param.put("j_file_size", fileSize[i]);
            //log.info("-------framework createUpload-----param---\n" + param.toString());
            sm.createObject("app.upload.createUpload", param);*/
        	smap.put("j_file_path", filePaths[i]);
        	smap.put("j_file_name", fileNames[i]);
        	smap.put("j_file_size", fileSize[i]);
            sm.createObject("app.upload.createUpload", smap);
        }
        
        
    }

    @Override
 	public void VFUpload(String programId, String docId, IRequest request) 
    {
     	log.info("-------VFUpload--------\n" + request.getMap().toString());

     	Map param = new HashMap();
     	param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        param.put("j_file_path", request.getStringParam("file_dir"));
        param.put("j_file_name", request.getStringParam("file_file"));
        
        param.put("j_user_id", request.getStringParam("userId"));
        param.put("v_key", "");
        param.put("contents", "");
        
        String filename = request.getStringParam("file_file");
        param.put("j_file_size", request.getStringParam("file_size"));
        String[] fileTypes = filename.split(".");
    	 if(fileTypes.length > 1) {
    		param.put("j_file_type", fileTypes[fileTypes.length-1]);
    	 } else {
    		param.put("j_file_type", "");
    	 }
/*         Map param = appUtils.getAuditableMap(request);
         param.put("j_file_program", programId);
         param.put("j_file_docId", docId);
         param.put("j_file_path", request.getStringParam("file_dir"));
         param.put("j_file_name", request.getStringParam("file_file"));
         
         String filename = request.getStringParam("file_file");
         param.put("j_file_size", request.getStringParam("file_size"));
         String[] fileTypes = filename.split(".");
     	 if(fileTypes.length > 1) {
     		param.put("j_file_type", fileTypes[fileTypes.length-1]);
     	 }*/
         log.info("-------VFUpload-----param---\n" + param.toString());
         sm.createObject("app.upload.createUpload", param);
     }
    
    @Override
	public void updateUpload(String programId, String docId, IRequest request) {
    	
    	// log.info("-------updateUpload--------\n" + request.toString());
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);

        String[] fileNameToDelete = request.getStringParams(getFileParamKey(request) + "_delete");
        String[] filePathToDelete = request.getStringParams(getFileParamKey(request) + "_path_delete");

        // 업로딩 될 파일들의 사이즈를 계산한다.
        long maxUploadSize = getMaxFileSize(request);
        long uploadedSize = ((HttpServletRequest) request.getAdapter(HttpServletRequest.class)).getContentLength();
        if (programId != null && docId != null) {
            List uploads = getUploads(programId, docId, null);
            for (int i = 0; i < uploads.size(); i++) {
                Map file = (Map) uploads.get(i);

                boolean skipflag = false;
                // 삭제될 예정인 것은 제외한다.
                if (fileNameToDelete != null) {
                    for (int j = 0; j < fileNameToDelete.length; j++) {
                        if (fileNameToDelete[j].equals(file.get("FILE_NAME"))
                                && filePathToDelete[j].equals(file.get("FILE_PATH"))) {
                            skipflag = true;
                            break;
                        }
                    }
                }
                if (skipflag)
                    continue;

                Number size = (Number) file.get("FILE_SIZE");
                uploadedSize += size.longValue();

                if (log.isDebugEnabled()) {
                    log.error("Total : "
                            + NumberFormat.getNumberInstance().format(uploadedSize)  + "("
                            + NumberFormat.getNumberInstance().format(size.longValue()) + " )/ "
                            + NumberFormat.getNumberInstance().format(maxUploadSize) + " [ " + file.get("FILE_PATH") + "/"  + file.get("FILE_NAME") + "]");
                }
                if (maxUploadSize < uploadedSize) {
                    throw new FileUploadException(
                            new SizeLimitExceededException("Max size is limited to [" + NumberFormat.getNumberInstance().format(maxUploadSize)  + "] bytes"));
                }
            }
        }

        for (int i = 0; fileNameToDelete != null && i < fileNameToDelete.length; i++) {
            File file = new File(rootDirectory + filePathToDelete[i] + "/" + fileNameToDelete[i]);
            if (file.exists()) {
                file.delete();
            }
            param.put("j_file_name", fileNameToDelete[i]);
            param.put("j_file_path", filePathToDelete[i]);
            sm.deleteObject("app.upload.deleteUpload", param);
        }
        createUpload(programId, docId, request);
    }

    @Override
	public void deleteUpload(String programId, String docId, IRequest request) {
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        String filepath = request.getStringParam("FILE_PATH");
        if( !"".equals(filepath)) param.put("j_file_path", filepath);
        String filename = request.getStringParam("FILE_NAME");
        if( !"".equals(filename)) param.put("j_file_name", filename);

        List filesToDelete = sm.getList("app.upload.getDeleteUploads", param);
        for (Iterator all = filesToDelete.iterator(); all.hasNext();) {
            Map each = (Map) all.next();
            File file = new File(rootDirectory + each.get("FILE_PATH") + "/"
                    + each.get("FILE_NAME"));
            if (file.exists()) {
                file.delete();
            }
        }
        sm.deleteObject("app.upload.deleteUpload", param);
    }

    @Override
	public List getUploads(String programId, String docId, IRequest request) {
    	// log.error("-------getUploads--------\n" + request.toString());
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        return sm.getList("app.upload.getUploads", param);
    }
}