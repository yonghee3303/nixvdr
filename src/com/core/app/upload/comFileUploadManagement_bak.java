/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.app.upload;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.framework.core.IRequest;

import mg.base.cryptorSHAEncodeThread;
import mg.base.cryptorSHAThread;

import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;
import com.core.component.thirdparty.PolarisViewer;
import com.core.component.thirdparty.SynapViewer;
import com.core.component.upload.FileUploadException;
import com.core.component.upload.internal.FileUploadRequest;
import com.core.component.upload.internal.JakartaFileUploadManagement;
import com.core.component.util.BaseUtils;
import com.core.component.util.DateUtils;
import com.core.component.util.WebUtils;
import com.core.app.util.appUtils;
import com.core.base.Constants;
import com.core.base.SystemException;

/**
 * <code>Jakarta Commpons FileUpload(http://jakarta.apache.org/commons/fileupload/)</code>
 * 라이브러리를 이용해서 파일 업로드를 담당하는 컴포넌트.
 * 
 * @since 2.0
 */
public class comFileUploadManagement_bak extends JakartaFileUploadManagement {
	

    private static final long serialVersionUID = 5876327354220929076L;

    private Log log = LogFactory.getLog(FileUploadManagement.class);
    
    protected ISqlManagement sm;
    
    /**
     * 파일을 업로드할 때는 type이 file인 input element들의 name 속성은 하나로 정해져 있어야 한다.
     * <p>
     * &lt;input type="file" name="j_file"/&gt;
     * </p>
     * 기본값은 j_file이며, 프로젝트에서 변경이 필요할 경우 이 메소드를 재정의한다.
     * (JControl을 이용한 개발을 할 경우엔 javascript도 신경써야 한다)
     * 
     * @param request  
     * @return
     */
    protected String getFileParamKey(IRequest request) {
        return "file"; // vue js 의 kendo upload component 에서는 files 임
    }

    public void setSqlManagement(ISqlManagement sm) {
        this.sm = sm;
    }
        
    @Override
    public HttpServletRequest uploadFile(HttpServletRequest request)  throws FileUploadException {
        try {
            DiskFileUpload upload = new DiskFileUpload();
            upload.setHeaderEncoding(this.defaultEncoding);
            upload.setSizeMax(getMaxFileSize(request));

            List items = upload.parseRequest(request);

            FileUploadRequest result = new FileUploadRequest(request);
            List files = new ArrayList();

            // 쿼리 스트링으로 넘긴 파라메터를 사용할 수 있도록 추가함
            Enumeration names = request.getParameterNames();
            while (names.hasMoreElements()) {
                String name = (String) names.nextElement();
                result.setParameter(name, request.getParameter(name));
            }
                        
            for (Iterator all = items.iterator(); all.hasNext();) {
                FileItem each = (FileItem) all.next();

                if (each.isFormField()) {
                    // 일반 form field는 기본인코딩으로 해석하여 파라미터로 등록
                    result.setParameter(each.getFieldName(), each.getString(this.defaultEncoding));
                } else {
                    // 업로딩된 각 파일별로 사이즈를 체크한다.
                    if (log.isDebugEnabled()) {
                        log.debug(NumberFormat.getNumberInstance().format( each.getSize())
                                + " / "
                                + NumberFormat.getNumberInstance().format( eachMaxSize) + "[" + each.getName() + "]");
                    }
                    if (eachMaxSize > 0 && each.getSize() > eachMaxSize) {
                        throw new SizeLimitExceededException(
                                "Max size of each file is limited to [" + NumberFormat.getNumberInstance().format(eachMaxSize) + "] bytes");
                    }
                    files.add(each);
                }
            }
            
            String[] actionTypes = handler.getDirectoryPath(result, true).split("/");        		
            String isImageUpload = actionTypes[actionTypes.length-1];
            
            // Image 또는 실행파일은 Path에 DirectoryPath를 포함하지 않는다.
            String folderId = result.getParameter("FOLDER_ID");         
            
            if (folderId == null) {            	
            	 String docId = result.getParameter("DOC_ID");
            	 
            	 Map param = new HashMap();
             	 param.put("DOC_ID", docId);
            	
            	 Map fileMap = (Map)sm.getItem("mg.vdr.getDocument", param);
                 log.info("moveUpload : "+fileMap.toString());      
                 
                 folderId = String.valueOf(fileMap.get("FOLDER_ID"));
            }
            
            String path = handler.getDirectoryPath(result, true);
            
            path += "/" + folderId;
            
            boolean isOverwrite = false;
            Map config = (Map) request.getAttribute(Constants.BEHIND_CONFIG);
            if (config != null && config.containsKey("overwrite")) {
                isOverwrite = new Boolean((String) config.get("overwrite")).booleanValue();
            }

            for (int i = 0; i < files.size(); i++) {
                FileItem each = (FileItem) files.get(i);
                String fileName = getFileNameWithoutPath(each);
                if (fileName.trim().length() == 0) { /* 빈 파일 입력에 대한 처리 */
                    continue;
                }
                File uploadedFile = null;
                
                if (isOverwrite) {
                    uploadedFile = getOverwriteUploadableFile(fileName, path, isImageUpload);
                } else {
                    uploadedFile = getUploadableFile(fileName, path, isImageUpload);
                }
                if (handler.writeFile(uploadedFile, request)) {
                    File directory = uploadedFile.getParentFile();
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                    each.write(uploadedFile);
                }
                
               // String uploadedFileName = URLEncoder.encode(uploadedFile.getName(), "UTF-8");
                //fileName = URLEncoder.encode(fileName, "UTF-8");
                
                result.addUploadedFile(uploadedFile);
                result.setParameter(each.getFieldName(), path + "/" + uploadedFile.getName());
                result.setParameter(each.getFieldName() + "_file", uploadedFile.getName());
                result.setParameter(each.getFieldName() + "_orgFile", fileName);
                result.setParameter(each.getFieldName() + "_dir", path);
                result.setParameter(each.getFieldName() + "_size", String.valueOf(uploadedFile.length()));
            }
            return result;
        } catch (Exception e) {
            throw new FileUploadException(e);
        }
    }
    
    private String getFileNameWithoutPath(FileItem each) {
        String name = each.getName();
        int start = name.lastIndexOf("\\");
        return (start != -1) ? name.substring(start + 1, name.length()) : name;
    }    
    
    @Override
	public void createUpload(String docId, IRequest request) {
        createUpload(request.getPath(), docId, request);
    }

    @Override
	public void createUpload(String programId, String docId, IRequest request) {
    	
    	log.info("-------createUpload--------\n" + request.getMap().toString());
    	/*
    	#j_file_program#,
        #j_file_docId#,
        #j_file_type:VARCHAR:NO_ENTRY#,
        #j_file_path#,
        #j_file_name#,
        #j_file_size#+0,
        #j_user_id#,
    	*/
    	 
    	String files[] = request.getStringParams(getFileParamKey(request));
    	 	
    	if(files == null || files.length == 0) {
            return;
    	} else {
	 
    	}
    	
        String types[] = request.getStringParams(getFileParamKey(request) + "_type");
        
        String filePaths[] = request.getStringParams(getFileParamKey(request) + "_dir");
        String fileNames[] = request.getStringParams(getFileParamKey(request) + "_file");
        String fileSize[] = request.getStringParams(getFileParamKey(request) + "_size");
        // Map param =appUtils.getAuditableMap(request);
        
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        
        String user_id = request.getUser() != null ? request.getUser().getId() : "admin";
        
        param.put("j_user_id", user_id);
                
        for(int i = 0; i < fileNames.length; i++)
        {
        	String[] fileTypes = fileNames[i].split(".");
        	
        	String filePath = filePaths[i];
        	String fileName = fileNames[i];
        	
        	String uploadFilePath = filePath + "/" + fileName;
        	
            String viewerType = String.valueOf(request.findRequestObject("viewerType"));
            String uploadEncrypt = String.valueOf(request.findRequestObject("uploadEncrypt"));
            
            if (viewerType.equals("P")) {
            	String uploadThumbPath = filePath + "/" + docId;
                PolarisViewer polarisView = new PolarisViewer();
                String result = polarisView.polarisConvert("PNG", uploadFilePath, uploadThumbPath, 1, 1);
                
                String uploadTextPath = filePath + "/" + docId + "/text";
                result = polarisView.polarisConvert("TEXT", uploadFilePath, uploadTextPath, -1, -1);
                
                if (result.equals("success")) {
                	String contents = polarisView.polarisTextFileReader(uploadTextPath);
            		param.put("contents", contents);
                }
                
            }else if (viewerType.equals("S")) {
            	// Synap 호출
                SynapViewer synapViewer = SynapViewer.getInstance();
                JSONObject jsonData = synapViewer.synabJobAPI(uploadFilePath, docId);
                if (jsonData != null && jsonData.get("key") != null) {
                	param.put("v_key", jsonData.get("key"));
                }
                
                if (param.get("v_key") == null) {
                    jsonData = synapViewer.synabJobAPI(uploadFilePath, docId);
                    if (jsonData != null && jsonData.get("key") != null) {
                    	param.put("v_key", jsonData.get("key"));
                    }
                }
            }            
            
            if (uploadEncrypt.equals("Y")) {
            	cryptorSHAEncodeThread th = new cryptorSHAEncodeThread(rootDirectory + "/" + uploadFilePath, rootDirectory + "/" +uploadFilePath +"_enc",  "12345");
        		th.start();
        		try{
        			th.join();
        		}catch(Exception e){ 
        			log.info("testDecrypt Error -----"+e.toString());
        		}
        		long retval = th.getResultLong();
        		log.info("retval ----- : "+retval);
            }
            
        	if(fileTypes.length > 1) {
        		param.put("j_file_type", fileTypes[1]);
        	}
            param.put("j_file_path", filePaths[i]);
            param.put("j_file_name", fileNames[i]);
            
            int size = Integer.parseInt(fileSize[i]);
            param.put("j_file_size", size);
            
            log.info("-------framework createUpload-----param---\n" + param.toString());
            
            sm.createObject("app.upload.createUpload", param);
        }
    }

    @Override
 	public void VFUpload(String programId, String docId, IRequest request)  {
     	
     	// log.info("-------VFUpload--------\n" + request.getMap().toString());
    	try {
	         Map param = appUtils.getAuditableMap(request);
	         param.put("j_file_program", programId);
	         param.put("j_file_docId", docId);
	         param.put("j_file_path", request.getStringParam("file_dir"));
	         param.put("j_file_name", request.getStringParam("file_file"));
	         
	         String filename = request.getStringParam("file_file");
	         param.put("j_file_size", request.getStringParam("file_size"));
	         String[] fileTypes = filename.split(".");
	     	 if(fileTypes.length > 1) {
	     		param.put("j_file_type", fileTypes[fileTypes.length-1]);
	     	 }
	         // log.info("-------VFUpload-----param---\n" + param.toString());
	         sm.createObject("app.upload.createUpload", param);
    	}catch(SystemException e) {
    		throw e;
    	}
     }
    
    @Override
 	public void UploadNoUser(String programId, String docId, String userId, IRequest request)  
    { 	
     	// log.info("-------VFUpload--------\n" + request.getMap().toString());
    	try {
	         Map param = appUtils.getAuditableMap(request);
	         param.put("j_file_program", programId);
	         param.put("j_file_docId", docId);
	         param.put("j_user_id", userId);
	         param.put("j_file_path", request.getStringParam("file_dir"));
	         param.put("j_file_name", request.getStringParam("file_file"));
	         
	         String filename = request.getStringParam("file_file");
	         param.put("j_file_size", request.getStringParam("file_size"));
	         String[] fileTypes = filename.split(".");
	     	 if(fileTypes.length > 1) {
	     		param.put("j_file_type", fileTypes[fileTypes.length-1]);
	     	 }
	         // log.info("-------VFUpload-----param---\n" + param.toString());
	         sm.createObject("app.upload.createUpload", param);
    	}catch(SystemException e) {
    		throw e;
    	}
     }
    
    @Override
	public void updateUpload(String programId, String docId, IRequest request) {
    	
    	// log.info("-------updateUpload--------\n" + request.toString());
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        
        String[] fileNameToDelete = request.getStringParams(getFileParamKey(request) + "_delete");
        String[] filePathToDelete = request.getStringParams(getFileParamKey(request) + "_path_delete");

        // 업로딩 될 파일들의 사이즈를 계산한다.
        long maxUploadSize = getMaxFileSize(request);
        long uploadedSize = ((HttpServletRequest) request.getAdapter(HttpServletRequest.class)).getContentLength();
        if (programId != null && docId != null) {
            List uploads = getUploads(programId, docId, null);
            for (int i = 0; i < uploads.size(); i++) {
                Map file = (Map) uploads.get(i);

                boolean skipflag = false;
                // 삭제될 예정인 것은 제외한다.
                if (fileNameToDelete != null) {
                    for (int j = 0; j < fileNameToDelete.length; j++) {
                        if (fileNameToDelete[j].equals(file.get("FILE_NAME"))
                                && filePathToDelete[j].equals(file.get("FILE_PATH"))) {
                            skipflag = true;
                            break;
                        }
                    }
                }
                if (skipflag)
                    continue;

                Number size = (Number) file.get("FILE_SIZE");
                uploadedSize += size.longValue();

                if (log.isDebugEnabled()) {
                    log.error("Total : "
                            + NumberFormat.getNumberInstance().format(uploadedSize)  + "("
                            + NumberFormat.getNumberInstance().format(size.longValue()) + " )/ "
                            + NumberFormat.getNumberInstance().format(maxUploadSize) + " [ " + file.get("FILE_PATH") + "/"  + file.get("FILE_NAME") + "]");
                }
                if (maxUploadSize < uploadedSize) {
                    throw new FileUploadException(
                            new SizeLimitExceededException("Max size is limited to [" + NumberFormat.getNumberInstance().format(maxUploadSize)  + "] bytes"));
                }
            }
        }

        for (int i = 0; fileNameToDelete != null && i < fileNameToDelete.length; i++) {
            File file = new File(rootDirectory + filePathToDelete[i] + "/" + fileNameToDelete[i]);
            if (file.exists()) {
                file.delete();
            }
            param.put("j_file_name", fileNameToDelete[i]);
            param.put("j_file_path", filePathToDelete[i]);
            sm.deleteObject("app.upload.deleteUpload", param);
        }
        
        createUpload(programId, docId, request);
    }

    @Override
	public void deleteUpload(String programId, String docId, IRequest request) {
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        String filepath = request.getStringParam("FILE_PATH");
        if( !"".equals(filepath)) param.put("j_file_path", filepath);
        String filename = request.getStringParam("FILE_NAME");
        if( !"".equals(filename)) param.put("j_file_name", filename);

        List filesToDelete = sm.getList("app.upload.getDeleteUploads", param);
        for (Iterator all = filesToDelete.iterator(); all.hasNext();) {
            Map each = (Map) all.next();
            File file = new File(rootDirectory + each.get("FILE_PATH") + "/"
                    + each.get("FILE_NAME"));
            if (file.exists()) {
                file.delete();
            }
        }
        sm.deleteObject("app.upload.deleteUpload", param);
    }

    @Override
	public List getUploads(String programId, String docId, IRequest request) {
    	// log.error("-------getUploads--------\n" + request.toString());
        Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        return sm.getList("app.upload.getUploads", param);
    }
    
    @Override
    public File copyUpload(String programId, String docId, String targetDocId, String targetFolderId, IRequest request) {

    	Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        
        Map fileMap = (Map)sm.getItem("app.upload.getUploads", param);
        log.info("copyUpload : "+fileMap.toString());
        
        
        String orgFileName = (String)fileMap.get("FILE_NAME");
        String orgFilePath = (String)fileMap.get("FILE_PATH");
        
        String orgfullFilePath = rootDirectory + orgFilePath + "/" + orgFileName;
        
        String newfileName = orgFileName;
        String newPath = "/" + DateUtils.getCurrentDate() + request.getPath() + "/" + targetFolderId;
        String newFilePath = rootDirectory + newPath;
        
        String newFileNamePath = newPath + "/" + orgFileName;
        String newFullFilePath = rootDirectory + newFileNamePath;
        
        
        File orgFile = new File(orgfullFilePath);
        File newFile = new File(newFullFilePath);
        
        if (newFile.exists()) { /* 파일 이름의 중복 처리 */
	        for (int i = 1; i < Integer.MAX_VALUE; i++) {
	            int indexOfDot = orgFileName.lastIndexOf(".");
	            if (indexOfDot == -1) {
	            	newfileName = orgFileName + "_" + i;
	            } else {
	            	newfileName = orgFileName.substring(0, indexOfDot) + "_" + i + orgFileName.substring(indexOfDot);
	            }
	            newFile = new File(newFilePath + "/" + newfileName);
	            if (!newFile.exists()) {
	                break;
	            }
	        }
        }
        
        File newfileDir = new File(newFilePath);
        if(!newfileDir.exists()) newfileDir.mkdirs(); 
        
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(orgFile);
			fos = new FileOutputStream(newFile) ;
			byte[] b = new byte[4096];
			int cnt = 0;
			while((cnt=fis.read(b)) != -1){
				fos.write(b, 0, cnt);
			}
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
		} finally{
			try {
				fis.close();
				fos.close();
			} catch (IOException e) {
				log.error("---------------------------------------------------------------\n" + e.toString());
			}
				
		}
        
        String viewerType = String.valueOf(request.findRequestObject("viewerType"));
        
        if (viewerType.equals("P")) {
        	String uploadThumbPath = newPath + "/" + targetDocId;
        	PolarisViewer polarisView = new PolarisViewer();
            String result = polarisView.polarisConvert("PNG", newFileNamePath, uploadThumbPath, 1, 1);
            
            String uploadTextPath = newPath + "/" + targetDocId + "/text";
            result = polarisView.polarisConvert("TEXT", newFileNamePath, uploadTextPath, -1, -1);
            
            if (result.equals("success")) {
            	String contents = polarisView.polarisTextFileReader(uploadTextPath);
        		param.put("contents", contents);
            }
            
        }else if (viewerType.equals("S")) {
        	// Synap 호출
            SynapViewer synapViewer = SynapViewer.getInstance();
            JSONObject jsonData = synapViewer.synabJobAPI(newFileNamePath, targetDocId);
            if (jsonData != null && jsonData.get("key") != null) {
            	param.put("v_key", jsonData.get("key"));
            }
            
            if (param.get("v_key") == null) {
                jsonData = synapViewer.synabJobAPI(newFileNamePath, targetDocId);
                if (jsonData != null && jsonData.get("key") != null) {
                	param.put("v_key", jsonData.get("key"));
                }
            }
        }  
        
        String user_id = request.getUser() != null ? request.getUser().getId() : "admin";
      
        param.put("j_file_docId", targetDocId);
        param.put("j_user_id", user_id);
        param.put("j_file_path", newPath);
        param.put("j_file_name", newfileName);
        param.put("j_file_size", fileMap.get("FILE_SIZE"));
        
        log.info("-------framework createUpload-----param---\n" + param.toString());
        
        sm.createObject("app.upload.createUpload", param);
		
		return newFile;    	
    }
    
    @Override
    public File moveUpload(String programId, String docId, String targetDocId, String targetFolderId, IRequest request) {
 
    	Map param = new HashMap();
        param.put("j_file_program", programId);
        param.put("j_file_docId", docId);
        
        Map fileMap = (Map)sm.getItem("app.upload.getUploads", param);
        log.info("moveUpload : "+fileMap.toString());
        
        
        String orgFileName = (String)fileMap.get("FILE_NAME");
        String orgFilePath = (String)fileMap.get("FILE_PATH");
        String orgfullPath = rootDirectory + orgFilePath + "/" + orgFileName;
        
        String newfileName = orgFileName;
        String newPath = "/" + DateUtils.getCurrentDate() + request.getPath() + "/" + targetFolderId;
        String newFilePath = rootDirectory + newPath;
        String newFullPath = rootDirectory + newPath + "/" + orgFileName;
        String newSynapFullPath = newPath + "/" + orgFileName;
        
        File orgFile = new File(orgfullPath);
        File newFile = new File(newFullPath);
        
        if (newFile.exists()) { /* 파일 이름의 중복 처리 */
	        for (int i = 1; i < Integer.MAX_VALUE; i++) {
	            int indexOfDot = orgFileName.lastIndexOf(".");
	            if (indexOfDot == -1) {
	            	newfileName = orgFileName + "_" + i;
	            } else {
	            	newfileName = orgFileName.substring(0, indexOfDot) + "_" + i + orgFileName.substring(indexOfDot);
	            }
	            newFile = new File(newFilePath + "/" + newfileName);
	            if (!newFile.exists()) {
	                break;
	            }
	        }
        }
        
        File newfileDir = new File(newFilePath);
        if(!newfileDir.exists()) newfileDir.mkdirs(); 
        
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(orgFile);
			fos = new FileOutputStream(newFile) ;
			byte[] b = new byte[4096];
			int cnt = 0;
			while((cnt=fis.read(b)) != -1){
				fos.write(b, 0, cnt);
			}
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
		} finally{
			try {
				fis.close();
				fos.close();
			} catch (IOException e) {
				log.error("---------------------------------------------------------------\n" + e.toString());
			}
				
		}
		
		Map newParam = new HashMap();
		
		String viewerType = String.valueOf(request.findRequestObject("viewerType"));
        
        if (viewerType.equals("P")) {
        	String uploadThumbPath = newPath + "/" + docId;
        	PolarisViewer polarisView = new PolarisViewer();
            polarisView.polarisConvert("PNG", newSynapFullPath, uploadThumbPath, 1, 1);
            
            String uploadTextPath = newPath + "/" + docId + "/text";
            polarisView.polarisConvert("TEXT", newSynapFullPath, uploadTextPath, -1, -1);
        }else {
        	// Synap 호출
            SynapViewer synapViewer = SynapViewer.getInstance();
            JSONObject jsonData = synapViewer.synabJobAPI(newSynapFullPath, docId);
            if (jsonData != null && jsonData.get("key") != null) {
            	param.put("v_key", jsonData.get("key"));
            }
            
            if (param.get("v_key") == null) {
                jsonData = synapViewer.synabJobAPI(newSynapFullPath, docId);
                if (jsonData != null && jsonData.get("key") != null) {
                	param.put("v_key", jsonData.get("key"));
                }
            }
        }  
         
        String user_id = request.getUser() != null ? request.getUser().getId() : "admin";
      
        newParam.put("j_file_program", programId);
        newParam.put("j_file_docId", targetDocId);
        newParam.put("j_user_id", user_id);
        newParam.put("j_file_path", newPath);
        newParam.put("j_file_name", newfileName);
        newParam.put("j_file_size", fileMap.get("FILE_SIZE"));
        
        log.info("-------framework createUpload-----param---\n" + newParam.toString());
        
        sm.createObject("app.upload.createUpload", newParam);
        
        
        List filesToDelete = sm.getList("app.upload.getDeleteUploads", param);
        for (Iterator all = filesToDelete.iterator(); all.hasNext();) {
            Map each = (Map) all.next();
            File file = new File(rootDirectory + each.get("FILE_PATH") + "/"
                    + each.get("FILE_NAME"));
            if (file.exists()) {
                file.delete();
            }
        }
        sm.deleteObject("app.upload.deleteUpload", param);
		
		return newFile;
    	
    }
    
    
    /**
     * 룸 또는 사용자 삭제일 때 실제 파일도 삭제하기 위함
     * 
     * @param deleteFileList
     * @return
     * @throws Exception
     */
    @Override
    public void deleteTFileUploadList(List deleteFileList) {
        for (int i=0 ; i<deleteFileList.size() ; i++) {
            Map deleteFileMap = (Map)deleteFileList.get(i);
            File file = new File(rootDirectory + deleteFileMap.get("FILE_PATH") + "/"
                    + deleteFileMap.get("FILE_NAME"));
            if (file.exists()) {
                file.delete();
            }
        }
    }    
    
}