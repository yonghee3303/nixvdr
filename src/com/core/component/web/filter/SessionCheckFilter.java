package com.core.component.web.filter;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.component.user.IUser;

public class SessionCheckFilter implements Filter {
	
	private Log log = LogFactory.getLog(SessionCheckFilter.class); 

	public void destroy() {
		log.info(this.config.getFilterName() + " Filter destroyed.");
	}
	
	/**
	 * Allowed Client
	 */
	private String ipAddress = ""; // BaseUtils.getDefaultEncoding();
	private boolean isAllowCheck = false;
	private String allowedService = "";
	private String loginPage="Login.jsp";
	
	/**
	 * FilterConfig 객체.
	 */
	protected FilterConfig config;

	public void init(FilterConfig config) throws ServletException 
	{
		this.config = config;
		String tmpAllowCheck = config.getInitParameter("allowedChecking");
		
		if (tmpAllowCheck != null && tmpAllowCheck.trim().length() > 0) {
			if("true".equals(tmpAllowCheck)) {
				this.isAllowCheck = true;
			}
		}
		
		String ipAddr = config.getInitParameter("IPAddress");
		if (ipAddr != null && ipAddr.trim().length() > 0) {
			this.ipAddress = ipAddr;
		}
		
		String allowedURL = config.getInitParameter("allowedService");
		if (allowedURL != null && allowedURL.trim().length() > 0) {
			this.allowedService = allowedURL;
		}
		
		String loginPage = config.getInitParameter("loginPage");
		if (loginPage != null && loginPage.trim().length() > 0) {
			this.loginPage = loginPage;
		}
		
		// log.info(this.config.getFilterName() + "[" + ipAddr	+ "--" + allowedURL + "] Filter created.");
	}
	
	private boolean checkURL(String url)
	{
		String[] paramURL = this.allowedService.split(",");
		for(int i=0; i < paramURL.length; i++) {
			if(url.contains(paramURL[i]) ) {
				// log.info("SessionCheckFilter :  paramURL[0] :  true  \n" + paramURL[0]);
				return true;
			}
		}
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,	FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String ret = "";
		boolean sessionOk = false;
		String ErrCode = "444";
		String sessionId = "";
		// Check URL blocking access .
				
		String url = req.getServletPath();
		
		//log.info("session request URL-----------------" + url.toString());
		String[] url_tmp = url.split("/");
		//log.info("last URL segment-----------------" + url_tmp[url_tmp.length-1]);
		
	    
		if(checkURL(url)) {
			sessionOk = true;
		} else  if(url.contains("Login.jsp") ||   url.contains("Login.do") || url.contains("Logout.do") || url.contains("Logout")) {
			// Login 인증 접속차단 설정 확인
			if(this.isAllowCheck) {
				// log.info("SessionCheckFilter : Allowed Check. \n" + url + "  : IP --> "+ req.getRemoteAddr() );
				
				String[] allowIp = this.ipAddress.split(",");
				for(int i=0; i < allowIp.length; i++) {
					
					String[] items = allowIp[i].split("~");
					if(items.length > 1) {  // 구간체크
						
						String[] ips = items[0].split("\\.");
						// log.info("SessionCheckFilter : Allowed Check. \n ips --> "+ ips.length );
						int s = Integer.parseInt(ips[3]);
						int e = Integer.parseInt(items[1]);
						
						String[] rips = req.getRemoteAddr().split("\\.");
						int r = Integer.parseInt(rips[3]);
						if(ips[0].equals(rips[0]) &&  ips[1].equals(rips[1]) && ips[2].equals(rips[2])) {   // 허용된 IP 구간대
							if( r >= s && r <= e) {
								//  log.info("SessionCheckFilter : Allowed Check. \n ips --> "+ s + " - " + e + " - " + r);
								sessionOk = true;
								ErrCode = "0";
								break;
							}
						}
					} else {  // IP Check
						if(allowIp[i].equals(req.getRemoteAddr())) {  // 허용된 IP
							// log.info("SessionCheckFilter : Allowed Check. \n 구간대 --> " + allowIp[i]);
							sessionOk = true;
							ErrCode = "0";
							break;
						}
					}
				}
				if(!sessionOk) {
					// log.info("SessionCheckFilter : Allowed Check. \n : IP --> "+ this.ipAddress + " --> Disabled access Ip address.");
					request.getRequestDispatcher("/Error500.jsp").forward(request, response);
				}
			}  else {
				// log.info("SessionCheckFilter : IP Address : \n" + this.ipAddress);
				sessionOk = false;
				ErrCode = "500";
			}
		} else { 
			Cookie[] cookies = req.getCookies();
			// log.info("SessionCheckFilter  getSession -----------> 0");
			for(int i = 0 ; i<cookies.length ; i++) {
			  Cookie cookie = cookies[i];
			  // log.info("SessionCheckFilter  cookie.getName() ----------->  " + cookie.getName());
			  if(cookie.getName().equals("JSESSIONID")) {
				  sessionId = cookie.getValue();
				  break;
			  }
			}
			
			HttpSession session = req.getSession(false);
			if (session != null) {
				// Session 지정 Constants.USER="nc_user";
				IUser userInfo = (IUser) session.getAttribute("j_user");
				if (userInfo != null) {
					
					/* 중복사용자를 막기 위하여  마지막사용자의 세션값과 현재 제출된 세션ID 와 비교하여 
					 * 다른면 오류 777을 리턴함.
					 * 
					SessionManager seMgr = new SessionManager();
					String curSessionId = seMgr.getSessionInfo(userInfo.getLoginId());
					
					if(curSessionId != null && !curSessionId.equals(sessionId)) {
						ErrCode = "777";
					} else {		
						sessionOk = true;
						ErrCode = "0";
					}
					*/
					// 현재 
					sessionOk = true;
					ErrCode = "0";
				} 
			} 
		}
				
		if (sessionOk) {
			// 필터 통과.
			filterChain.doFilter(request, response);
		} else {
			// 에러리턴
			String urlext = url.substring(url.lastIndexOf('.'));
			if (urlext.startsWith(".jsp")) {
				 // miplatform에서 세션없는 경우
				 // 444 리턴 세션 없음 메시지 아이디.

				 ret = "<?xml version='1.0' encoding='UTF-8'?>";
			   	 ret += "<rows>";
		    	 ret += "<userdata name=\"message\">세션 정보가 없습니다.</userdata>";
		    	 ret += "<userdata name=\"data_type\">doSelect</userdata>";
		    	 ret += "<userdata name=\"status\">false</userdata>";
		    	 ret += "<userdata name=\"page\">" + url + "</userdata>";
			   	 ret += "</rows>";
			   	 log.error("-Session Check Filter : --" + ret);
			   	 
			   	  // request.setAttribute(Constants.MESSAGE, getMessageManagement().getMessage("GMSG_1023"));
			   	  request.setAttribute(ret, "text/xml");
			   	  request.getRequestDispatcher("/"+loginPage).forward(request, response);
			} else {
				  String msg = "This session is not valid. You are not logged in or time out.  MyGuard WEB Framawork v2.6.";
				  log.error("================================================================\n" + msg);
			}
		}
	}
}
