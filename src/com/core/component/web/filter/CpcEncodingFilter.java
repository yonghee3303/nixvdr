package com.core.component.web.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;

import com.core.component.user.IUser;
import com.core.component.util.BaseUtils;
import com.core.component.util.WebUtils;



/**
 * <code>ServletRequest</code> 의 <b>Character encoding </b>을 <b>EUC-KR </b>로
 * 변경하는 <code>Filter</code> 로 Apache Tomcat 등 WAS에서 이 설정을 변경할 수 없는 경우에 사용한다.
 * 
 * @since 2.0
 */
public class CpcEncodingFilter implements Filter {
	
	/**
	 * 로그를 기록하는 멤버 필드.
	 */
	private Log log = LogFactory.getLog(CpcEncodingFilter.class); 
	/**
	 * 인코딩
	 */
	private String encoding = "UTF-8"; // BaseUtils.getDefaultEncoding();

	/**
	 * FilterConfig 객체.
	 */
	protected FilterConfig config;

	public void init(FilterConfig config) throws ServletException 
	{
		this.config = config;
		String encoding = config.getInitParameter("encoding");
		if (encoding != null && encoding.trim().length() > 0) {
			this.encoding = encoding;
		}
		log.info(this.config.getFilterName() + "[" + encoding	+ "] Filter created.");
	}

	/**
	 * 입력된 Request의 문자를 Encoding 한다. TODO : Filter Config로 부터 입력된 값으로 Encoding
	 * 한다.
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
	{		
		
		log.info("URL=" + ((HttpServletRequest) request).getRequestURL() + "\n" + WebUtils.getRequestMessage(request));
		
		HttpServletResponse res = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;		
		
		String url = req.getServletPath();
		String[] url_tmp = url.split("/");
		
		if (!( url_tmp[url_tmp.length-1].equals("login") || url_tmp[url_tmp.length-1].equals("Login.do") ) ) {
			HttpSession session = req.getSession(false);
			if (session != null) {
				IUser userInfo = (IUser) session.getAttribute("j_user");
				if (userInfo != null) {				
					log.info("userInfo : "+userInfo.getId());	
					
				}else {
					log.info("userInfo is null");
				}
			}else {
				log.info("session is null");
			}
		}
		

		if("OPTIONS".equals(req.getMethod())){
			res.setHeader("Allow", "GET,HEAD,POST,OPTIONS");
		}

	    res.setHeader("Access-Control-Allow-Origin", "*");
	    res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	    res.setHeader("Access-Control-Max-Age", "3600");
	    res.setHeader("Access-Control-Allow-Headers", "Authorization, Access-Control-Allow-Origin,Content-Type, Access-Control-Allow-Headers, Accept, X-Requested-With, remember-me, pragma, filesize"
	    		+ ", http, data, chrome, chrome-extension, https");

		request.setCharacterEncoding(encoding);
		chain.doFilter(request, response);
		
	}

	public void destroy() {
		log.info(this.config.getFilterName() + " Filter destroyed.");
	}

}
