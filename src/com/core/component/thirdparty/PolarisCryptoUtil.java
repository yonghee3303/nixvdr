package com.core.component.thirdparty;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.util.Base64;
import java.util.Base64.Encoder;


public class PolarisCryptoUtil {

	
	public byte[] getRSAData(String text, String charset, String certPath) {
		
		Security.addProvider(new BouncyCastleProvider());
		
		byte[] result = null;		
		
		try {
			PublicKey publicKey = getPublicKeyFromDerFile(certPath + "/cert/po2017_pub.der");
	        byte[] plainData = text.getBytes(charset);	        
	        Cipher cipher = Cipher.getInstance("RSA/None/OAEPWithSHA256AndMGF1Padding", "BC");
	        byte[] encryptedData = RSAEncrypt(cipher, publicKey, plainData);
	        result = encryptedData;
			
		} catch(Exception e) {
			 System.out.println("getRSAData e    : " + e.toString());
		}
		
		
		return result;
	}
	
	public byte[] getAESData(String text, String charset) {
		
		Security.addProvider(new BouncyCastleProvider());
		
		byte[] result = null;
		String key = "111test987654321";
		
		try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");            
            byte[] keyData = key.getBytes(charset);
            SecretKey secretKey = new SecretKeySpec(keyData, "AES");
            byte[] plainData = text.getBytes(charset);
            byte[] encryptedData = AESEncrypt(cipher, secretKey, plainData);            
            result = encryptedData;			
		} catch(Exception e) {
			 System.out.println("getAESData e    : " + e.toString());
		}
		
		
		return result;
	}
	
	 private PublicKey getPublicKeyFromDerFile(String filename)
             throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
         byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
         X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
         return KeyFactory.getInstance("RSA").generatePublic(spec);
     }
	
	
	private byte[] RSAEncrypt(Cipher cipher, PublicKey publicKey, byte[] plainData)
            throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(plainData);
    }
	
	private static byte[] AESEncrypt(Cipher cipher, SecretKey secretKey, byte[] plainData)
            throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return cipher.doFinal(plainData);
    }
	
	public String URLEncodingBase64(byte[] targetData, String charset) throws UnsupportedEncodingException {
		Encoder encoder = Base64.getEncoder();
     	String result = encoder.encodeToString(targetData);
     	result = URLEncoder.encode(result, charset);
     	
     	return result;
	}
	
	
}
