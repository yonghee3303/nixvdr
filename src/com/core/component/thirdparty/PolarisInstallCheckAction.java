package com.core.component.thirdparty;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;

public class PolarisInstallCheckAction  extends BaseAction {
	
	@Override
	public IResponse run(IRequest request) throws Exception {
		
       return installCheck(request);   
       
    }
	
	private IResponse installCheck(IRequest request) throws Exception 
    {		
		String user_id = request.getUser() != null ? request.getUser().getId() : "admin";
		
		Map item = new HashMap();
    	item.put("USER_ID", user_id);		
		Map map = (Map) getItem("mg.vdr.polaris.selectUserPInstall", item);
		
		String pInstall = String.valueOf(map.get("P_INSTALL"));
		        
        JSONObject obj = new JSONObject();
        
        if (pInstall.equals("Y")) {
        	obj.put("errmsg", "success");
 			obj.put("errcode", "0");
        }else {
        	obj.put("errmsg", "error");
    	    obj.put("errcode", "-1"); 
        }
        
        return write(obj.toString());
    }

}
