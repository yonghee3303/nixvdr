package com.core.component.thirdparty;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.Workspace;

public class PolarisViewer extends Workspace {
	
	private Log log = LogFactory.getLog(PolarisViewer.class);
	
	 /**
     * IParameterManagement 컴포넌트 인스턴스
     */
    protected IParameterManagement pm = (IParameterManagement) getComponent(IParameterManagement.class);
	
    String serverUploadRoot = pm.getString("component.upload.directory");
    String servicedirectory = pm.getString("component.upload.servicedirectory");    
    String servicedthumbdir = servicedirectory + pm.getString("component.path.thumbnail");
    
    String polarisConverterPath = pm.getString("component.path.polarisconverter");
	
	//싱글톤으로 처리
	/*
	 * private static PolarisViewer instance = new PolarisViewer();
	 * 
	 * private PolarisViewer() {}
	 * 
	 * public static PolarisViewer getInstance() { return instance; }
	 */
    
    /**
     * 폴라리스 컨버터 -썸네일 및 텍스트 추출
     * type - PNG (썸네일), TXT (텍스트)
     * uploadFilePath - 업로드된 파일 풀 경로
     * uploadConvertPath - 변환후 파일이 들어갈 폴더 경로
     * startPage - 썸네일 생성시 시작 페이지
     * endPage - 썸네일 생성시 끝 페이지
     */
    public String polarisConvert(String type, String uploadFilePath, String uploadConvertPath, int startPage, int endPage) {
    	
    	log.info("polarisConvert call");
    	
    		
    	//String temp1 = "/efs/ixvdr/work/upload/20200102/vdr/explorer.do/F2019123011353369118320601350/1.pptx";
    	//String temp2 = "/efs/polarisConverter8/result";
    	//String temp3 = "/efs/polarisConverter8/temp";
    	
    	//String cmds[] = new String[] {"java", "-jar", "PolarisConverter8.jar", type, serverUploadRoot + uploadFilePath
    	//		, servicedthumbdir + uploadConvertPath , "1280" , "1280", servicedthumbdir + uploadConvertPath + "/temp"};

    	String cmds[];
    	
    	if (startPage > 0 && endPage > 0) {
    		cmds = new String[11];
    		cmds[cmds.length-2] = String.valueOf(startPage);
    		cmds[cmds.length-1] = String.valueOf(endPage);
    	}else {
    		cmds = new String[9];
    	}
    	
//    			
//		try {
//			uploadFilePath = URLDecoder.decode(uploadFilePath, "UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    	
    	cmds[0] = "java";
    	cmds[1] = "-jar";
    	cmds[2] = "PolarisConverter8.jar";
    	cmds[3] = type;
    	cmds[4] = "\""+serverUploadRoot + uploadFilePath+"\"";
    	cmds[5] = servicedthumbdir + uploadConvertPath;
    	cmds[6] = "1280";
    	cmds[7] = "1280";
    	cmds[8] = servicedthumbdir + uploadConvertPath +"/temp"; 	
    	
    	Executor executor = new DefaultExecutor();
    	CommandLine cmdLine = CommandLine.parse(cmds[0]);
    	for(int i=1; i<cmds.length; i++){
    		log.info("cmds[i] : "+cmds[i]);
    		cmdLine.addArguments(cmds[i],false);
    	}    	
    	   	
    	executor.setWorkingDirectory(new File(polarisConverterPath)); 
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	PumpStreamHandler handler = new PumpStreamHandler(baos, baos);
    	executor.setStreamHandler(handler);
    	executor.setWatchdog(new ExecuteWatchdog(60000*2)); //시간제한
    	try{
    		executor.execute(cmdLine);
    		log.info("baos.toString() : "+baos.toString()); //으로 프로세스 출력 메시지 읽어서 변환 결과 확인
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally {
			try {
				baos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "fail";
			}			
		}
    	
    	return "success";
    }
    
    /**
     * 폴라리스 TXT 로 생성된 파일 읽어서 String 처리
     * type - PNG (썸네일), TXT (텍스트)
     * txtPath - TXT 파일이 들어있는 폴더 : 폴더에 있는 모든 파일 읽음
     */
    public String polarisTextFileReader(String txtPath) {
    	
    	StringBuffer stringBuffer = new StringBuffer();
    	
    	try{
    		File textFolder = new File(servicedthumbdir + "/"+ txtPath);
    		for (File fileEntry : textFolder.listFiles()) {
                File file = fileEntry;
                BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
                String line = "";
                while((line = bufReader.readLine()) != null){
                    log.info("-------line : " + line.toString());  
                    stringBuffer.append(line);
                }
                //.readLine()은 끝에 개행문자를 읽지 않는다.            
                bufReader.close();
    	    }
    		
    		
        }catch (FileNotFoundException e) {
        	log.info("polarisTextFileReader FileNotFoundException : "+e.toString());
        	return "";
        }catch(IOException e){        	
        	log.info("polarisTextFileReader ioexception : "+e.toString());
        	return "";
        }
		
		return stringBuffer.toString();
    }

}
