package com.core.component.thirdparty;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.Workspace;

public class SynapViewer extends Workspace {
	
	private Log log = LogFactory.getLog(SynapViewer.class);
	
	 /**
     * IParameterManagement 컴포넌트 인스턴스
     */
    protected IParameterManagement pm = (IParameterManagement) getComponent(IParameterManagement.class);
	
	private String synapURL = pm.getString("component.url.synap");
	private String synapJob = synapURL + "/SynapDocViewServer/job";
	private String synapStatus = synapURL + "/SynapDocViewServer/status";
	
	
	//싱글톤으로 처리
    private static SynapViewer instance = new SynapViewer();

    private SynapViewer() {}
     
    public static SynapViewer getInstance() {
        return instance;
    }

	public JSONObject synabJobAPI(String uploadFilePath, String fid) {
    	
    	
    	JSONObject jsonResponseData = new JSONObject();
		String uploadFolder = pm.getString("component.upload.directory");
		String filepath = uploadFolder + uploadFilePath;
		
		log.info("synabJobAPI filepath : "+filepath + ", synapJob : "+synapJob);
		
		// localTest -> 서버에서 파일 테스트 하지 않으면 실제 파일이 없으므로;
		//String dumpData = "/home/ixvdr/work/ixvdr/upload/20191204/vdr/explorer.do/F2019120317241115286724662300/testfile1.txt";
		//filepath = dumpData;
		
		JSONObject jsonObject = new JSONObject();
      
        try {
    	  jsonObject.put("fileType","Local");
          jsonObject.put("convertType","1");
          jsonObject.put("filePath",filepath);
          jsonObject.put("fid",fid);
		
          jsonResponseData = post(synapJob, jsonObject.toString());
		} catch (Exception e) {
			log.info("-------synabJobAPI error : "+e.toString());
		}
		
        return jsonResponseData;
	}
	
	public JSONObject synabStatusAPI(String key) {
    	
    	log.info("synabStatusAPI Call : "+key);
    	JSONObject jsonResponseData = new JSONObject();
    	
		String requestURL = synapStatus + "/" + key;

        try {
          jsonResponseData = get(requestURL);
		} catch (Exception e) {
			log.info("-------synabStatusAPI error : "+e.toString());
		}
		
        return jsonResponseData;
	}
	
	
	public JSONObject post(String requestURL, String jsonMessage) {
		
		JSONObject jsonData = new JSONObject();

		try {
			HttpClient client = HttpClientBuilder.create().build(); // HttpClient 생성
			HttpPost postRequest = new HttpPost(requestURL); //POST 메소드 URL 새성 
			postRequest.setHeader("Accept", "application/json");
			postRequest.setHeader("Connection", "keep-alive");
			postRequest.setHeader("Content-Type", "application/json");
			//postRequest.addHeader("Authorization", token); // token 이용시
			postRequest.setEntity(new StringEntity(jsonMessage, "UTF-8")); //json 메시지 입력 

			HttpResponse response = client.execute(postRequest);
			
			ResponseHandler<String> handler = new BasicResponseHandler();
			String body = handler.handleResponse(response);
			
			JSONParser jsonParser = new JSONParser();
			jsonData = (JSONObject)jsonParser.parse(body);

		} catch (Exception e){
			 log.info("-------post error : "+e.toString());
		}
		
		return jsonData;
	}
	
	public JSONObject get(String requestURL) {
		
		JSONObject jsonData = new JSONObject();
		
		try {
			HttpClient client = HttpClientBuilder.create().build(); // HttpClient 생성
			HttpGet getRequest = new HttpGet(requestURL); //GET 메소드 URL 생성
			HttpResponse response = client.execute(getRequest);
			ResponseHandler<String> handler = new BasicResponseHandler();
			String body = handler.handleResponse(response);
			
			JSONParser jsonParser = new JSONParser();
			jsonData = (JSONObject)jsonParser.parse(body);

		} catch (Exception e){
			log.info("-------get error : "+e.toString());
		}
		
		return jsonData;
	}

}
