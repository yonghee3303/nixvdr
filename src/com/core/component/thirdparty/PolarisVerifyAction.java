package com.core.component.thirdparty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import mg.base.BaseAction;

public class PolarisVerifyAction  extends BaseAction {
	
	@Override
	public IResponse run(IRequest request) throws Exception {
		
       return verifyToken(request);   
       
    }
	
	private IResponse verifyToken(IRequest request) throws Exception 
    {		
		String secretKey = pm.getString("component.jwt.key");
		
		HttpServletRequest httpServletRequest = (HttpServletRequest)request.getAdapter(HttpServletRequest.class);
		
		String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
 
        try {
            InputStream inputStream = httpServletRequest.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }
 
        body = stringBuilder.toString();
		
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject)jsonParser.parse(body);
		
		String srt = String.valueOf(jsonObject.get("srt"));		
		
		byte[] keySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);
         
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;         
        Key key = new SecretKeySpec(keySecretBytes, signatureAlgorithm.getJcaName());
	
        
        String polarisUserId = null;
        
        try {
        	Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(srt).getBody();
        	polarisUserId = (String)claims.get("polarisUserId");
        	
        } catch(ExpiredJwtException e) {
        	polarisUserId = null;
        }
		
                
        JSONObject obj = new JSONObject();
                
        if ( polarisUserId != null && !polarisUserId.equals("") ) {
        	
        	Map item = new HashMap();
        	item.put("USER_ID", polarisUserId);
        	item.put("P_INSTALL", "Y");
        	
        	updateObject("mg.vdr.polaris.updateUserPInstall", item);
        	
        	obj.put("errmsg", "success");
 			obj.put("errcode", "0");
        }else {
        	obj.put("errmsg", "error");
    	    obj.put("errcode", "-1"); 
        }
	      
        return write(obj.toString());
    }

}
