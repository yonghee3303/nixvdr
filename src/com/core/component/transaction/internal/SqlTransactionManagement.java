/*
 * Copyright (c) 2010 ITM System. All rights reserved.
 *
 * This software is the confidential and proprietary information of ITM System.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with ITM System.
 */

package com.core.component.transaction.internal;

import com.core.component.sql.ISqlManagement;
import com.core.component.transaction.ITransactionManagement;

/**
 * 
 * @since 3.0
 */
public class SqlTransactionManagement implements ITransactionManagement {
    private ISqlManagement sm;
    
    public void setSqlManagement(ISqlManagement sm) {
        this.sm = sm;
    }
    
    public void begin() {
        sm.begin();
    }

    public void rollback() {
        sm.rollback();
    }

    public void commit() {
        sm.commit();
    }

}