/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml;

import java.util.List;

import com.core.framework.core.IResponse;

/**
 * VariableXml를 위한 XML 응답의 태그(<list>)를 나타내는 인터페이스.
 * 
 * @since 3.0
 */
public interface IVariableXmlTree extends IResponse {

    /**
     * 불필요한 nodeName을 빼고 사용하세요
     * @param list 아이템 리스트.
     * @throws Exception 
     */
    public String addTreeList(List list) throws Exception;

}