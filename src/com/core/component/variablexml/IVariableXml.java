/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml;

import java.util.List;

import com.core.framework.core.IResponse;

/**
 * VariableXml를 위한 XML 응답의 태그(<list>)를 나타내는 인터페이스.
 * 
 * @since 3.0
 */
public interface IVariableXml extends IResponse {

    /**
     * 단일건 XML변환
     * @param Object 아이템 리스트.
     * @throws Exception 
     */
    public String addXml(Object item, String[] columnName) throws Exception;

    /**
     * 리스트건 XML변환
     * @param list 아이템 리스트.
     * @throws Exception 
     */
    public String addXml(List list, String[] columnName) throws Exception;

}