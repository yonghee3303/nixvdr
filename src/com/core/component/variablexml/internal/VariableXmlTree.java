/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml.internal;

import java.util.List;
import java.util.Map;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import com.core.component.variablexml.IVariableXmlTree;
import com.core.framework.core.IOutput;

public class VariableXmlTree implements IVariableXmlTree {

    private String encoding = null;
    
    public VariableXmlTree() {
        //
    }

    protected String getEncoding(){
        if (encoding == null){
            IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
            encoding = pm.getString("framework.config.defaultEncoding");
        }
        return encoding;
    }

    /* addNodeList(List list, String[] columnName)
     * Recode Type 의 XML을 리턴 하는 함수 
     * @see com.core.jafar.response.ITfResponse#addNodeList(java.util.List, java.lang.String[])
     */
    public String addTreeList(List list) throws Exception {
        
        Map mapTree  = null;
        StringBuffer buffer = new StringBuffer();
        
        int lvl=0;
        int newlvl=0;

        // MS IE는 MS949 인코딩(UTF-8이 아닌 인코딩?)에서 xml 헤더가 필요함.
        buffer.append("<?xml version='1.0' encoding='" + getEncoding()
                    + "' standalone='yes'?>");

        // buffer.append("<NODE>");
        for (int i = 0; i < list.size(); i++) {
            mapTree = (Map) list.get(i);
            newlvl = Integer.parseInt(this.getStringWithNullCheck(mapTree, "LVL"));
            
            if(newlvl <= lvl) {
            	for(int j=lvl; j>=newlvl; j--) {
                    buffer.append("</NODE>");
                 }
            }
            buffer.append("<NODE level='" + this.getStringWithNullCheck(mapTree, "LVL") 
                        + "' id='"      + this.getStringWithNullCheck(mapTree, "ID") 
                        + "' label='"     + this.getStringWithNullCheck(mapTree, "LABEL") 
                        + "' type='"     + this.getStringWithNullCheck(mapTree, "TYPE")
                        + "' icon='"     + this.getStringWithNullCheck(mapTree, "ICON")
                        + "'>");
            lvl = Integer.parseInt(this.getStringWithNullCheck(mapTree, "LVL"));
        }
        for(int i=0; i< lvl; i++) {
           buffer.append("</NODE>");
        }

        return buffer.toString();
    }

    public void response(IOutput out) throws Exception {
        // TODO Auto-generated method stub
        
    }

    /**
     * Map에 있는 객체를 String으로 전환한다.
     * 
     * @param map java.util.Map
     * @param key 키
     * @return null인 경우에는 ""을 반환
     */
    private String getStringWithNullCheck(Map map, String key) {
        Object result = map.get(key);
        return (result == null) ? "" : result.toString();
    }

}
