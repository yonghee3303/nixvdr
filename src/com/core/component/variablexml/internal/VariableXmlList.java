/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.core.component.variablexml.IVariableXmlItem;
import com.core.component.variablexml.IVariableXmlList;

/**
* 업무 그룹명 : com.core.component.variablexml.internal
* 서브 업무명 : VariableXmlList.java
* 작성자 : cyLeem
* 작성일 : 2008. 10. 23
* 설 명 : VariableXmlList
*/
public class VariableXmlList implements IVariableXmlList {

    @SuppressWarnings("unchecked")
	private List items = new ArrayList();

    @SuppressWarnings("unchecked")
	private Map attributes = null;

    /**
    * VariableXmlList
    */
    public VariableXmlList() {
        //
    }

    /**
    * @param id
    */
    public VariableXmlList(String id) {
        addAttribute("id", id);
    }

    /**
    * @param id
    * @return
    */
    public IVariableXmlItem createItem(int id) {
        VariableXmlItem result = new VariableXmlItem(new Integer(id).toString());
        addItem(result);
        return result;
    }

    /**
    * @param id
    * @param locale
    * @return
    */
    public IVariableXmlItem createItem(int id, Locale locale) {
        VariableXmlItem result = new VariableXmlItem(new Integer(id).toString(), locale);
        addItem(result);
        return result;
    }

    /**
	* @param item
	*/
	@SuppressWarnings("unchecked")
	protected void addItem(IVariableXmlItem item) {
        items.add(item);
    }

    /**
    * @param name
    * @param value
    */
    @SuppressWarnings("unchecked")
	public void addAttribute(String name, String value) {
        if (attributes == null) {
            attributes = new HashMap();
        }
        attributes.put(name, value);
    }

    /**
	* @return
	*/
	@SuppressWarnings("unchecked")
	public String toXml() {
        StringBuffer buffer = new StringBuffer(1024);
        if (attributes == null || attributes.isEmpty()) {
            buffer.append("<NODES>");
        } else if (attributes != null) {
            buffer.append("<NODES ");
            Iterator keys = attributes.keySet().iterator();
            Object key = null;
            while (keys.hasNext()) {
                key = keys.next();
                buffer.append(key);
                buffer.append("='");
                buffer.append(attributes.get(key));
                buffer.append("' ");
            }
            buffer.append(">");
        }
        for (int i = 0; i < items.size(); i++) {
            ((IVariableXmlItem) items.get(i)).appendXml(buffer);
        }
        buffer.append("</NODES>");
        return buffer.toString();
    }

}
