/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml.internal;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.core.base.ComponentRegistry;
import com.core.base.Constants;
import com.core.component.format.OutFormattedMap;
import com.core.component.parameter.IParameterManagement;
import com.core.component.user.IUser;
import com.core.component.variablexml.IVariableXml;
import com.core.framework.core.IOutput;

public class VariableXml implements IVariableXml {

    protected HttpSession session;

    private HttpSession request;

    private String encoding = null;

    private String name;

    private String value;

    protected String getEncoding(){
        if (encoding == null){
            IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
            encoding = pm.getString("framework.config.defaultEncoding");
        }
        return encoding;
    }

    public String addXml(Object item, String[] columnName) throws Exception {
        // TODO Auto-generated method stub

        int columnLength = columnName.length;

        Map xmlChangSingle = (Map) item;
        StringBuffer buffer = new StringBuffer();

        // MS IE는 MS949 인코딩(UTF-8이 아닌 인코딩?)에서 xml 헤더가 필요함.
        buffer.append("<?xml version='1.0' encoding='" + getEncoding()
                    + "' standalone='yes'?>");

        buffer.append("<NODE>");

        Object key = null;
        for (int i = 0; i < columnLength; i++) {
            String nodeNm = (columnName[i].indexOf('|')<0)? columnName[i] : columnName[i].split("\\|")[1];
            Object obj = xmlChangSingle.get(nodeNm);
            String value = (obj != null) ? obj.toString() : "";

            this.nameValue(nodeNm, value);
            this.appendXml(buffer);
        }

        buffer.append("</NODE>");
        
        return buffer.toString();
    }

    public String addXml(List list, String[] columnName) throws Exception {
        // TODO Auto-generated method stub

        int columnLength = columnName.length;

        StringBuffer buffer = new StringBuffer();

        // MS IE는 MS949 인코딩(UTF-8이 아닌 인코딩?)에서 xml 헤더가 필요함.
        buffer.append("<?xml version='1.0' encoding='" + getEncoding()
                    + "' standalone='yes'?>");

        buffer.append("<NODES>");

        Map xmlChangList = null;
        int j=0;
        for (Iterator iter = list.iterator(); iter.hasNext();) {
            xmlChangList = (Map) iter.next();
            
            buffer.append("<NODE>");

            Object key = null;
            for (int i = 0; i < columnLength; i++) {
                String nodeNm = (columnName[i].indexOf('|')<0)? columnName[i] : columnName[i].split("\\|")[1];
                Object obj = xmlChangList.get(nodeNm);
                String value = (obj != null) ? obj.toString() : "";

                this.nameValue(nodeNm, value);
                this.appendXml(buffer);
            }

            buffer.append("</NODE>");

            j=j+1;
        }

        buffer.append("</NODES>");
        
        return buffer.toString();
        
    }

    public void response(IOutput output) throws Exception {
        // TODO Auto-generated method stub
        
    }

    public void addRequestObject(String name, Object value) {
        if (value instanceof OutFormattedMap) {
            Map outmap = (Map) value;
            outmap.put(Constants.USER, getUser());
        }
        request.setAttribute(name, value);
    }

    public IUser getUser() {
        return (IUser) findSessionObject(Constants.USER);
    }

    public Object findSessionObject(String name) {
        return (session != null) ? session.getAttribute(name) : null;
    }

    public void nameValue(String name, Object value) {
        this.name = name;
        this.value = value == null ? "" : value.toString();
    }

    public void appendXml(StringBuffer buffer) {
        buffer.append("<" + name + "> <![CDATA[" + value + "]]> </" + name+ ">");
    }

}
