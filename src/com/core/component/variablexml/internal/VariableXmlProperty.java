/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml.internal;

public class VariableXmlProperty {

    private String name;

    private String value;

    public VariableXmlProperty(String name, Object value) {
        this.name = name;
        this.value = value == null ? "" : value.toString();
    }

    public String toXml() {
        StringBuffer buffer = new StringBuffer(128);
        appendXml(buffer);
        return buffer.toString();
    }

    public void appendXml(StringBuffer buffer) {
        buffer.append("<" + name + "> <![CDATA[" + value + "]]> </" + name+ ">");
    }

}
