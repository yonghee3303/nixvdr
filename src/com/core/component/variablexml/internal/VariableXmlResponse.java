/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import com.core.component.variablexml.IVariableXmlItem;
import com.core.component.variablexml.IVariableXmlList;
import com.core.component.variablexml.IVariableXmlResponse;
import com.core.framework.core.IOutput;

/**
* 업무 그룹명 : com.core.component.variablexml.internal
* 서브 업무명 : VariableXmlResponse.java
* 작성자 : cyLeem
* 작성일 : 2008. 10. 23
* 설 명 : VariableXmlResponse
*/
public class VariableXmlResponse implements IVariableXmlResponse {

    private String code;

    private String message;

    private String encoding = null;
    
    @SuppressWarnings("unchecked")
	private Map attributes = Collections.EMPTY_MAP;

    @SuppressWarnings("unchecked")
	private List items = Collections.EMPTY_LIST;
    
    @SuppressWarnings("unchecked")
	private List lists = Collections.EMPTY_LIST;
    
    /**
    * 
    */
    public VariableXmlResponse() {
        // 기본 생성자
    }

    /**
    * @param code
    * @param message
    */
    public VariableXmlResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
    * @return
    */
    public IVariableXmlItem createItem() {
        VariableXmlItem result = new VariableXmlItem();
        addItem(result);
        return result;
    }

    /**
    * @param locale
    * @return
    */
    public IVariableXmlItem createItem(Locale locale) {
        VariableXmlItem result = new VariableXmlItem(locale);
        addItem(result);
        return result;
    }

    /**
    * @param id
    * @return
    */
    public IVariableXmlItem createItem(String id) {
        VariableXmlItem result = new VariableXmlItem(id);
        addItem(result);
        return result;
    }

    /**
    * @param id
    * @param locale
    * @return
    */
    public IVariableXmlItem createItem(String id, Locale locale) {
        VariableXmlItem result = new VariableXmlItem(id, locale);
        addItem(result);
        return result;
    }

    /**
    * @return
    */
    public IVariableXmlList createList() {
        VariableXmlList result = new VariableXmlList();
        addList(result);
        return result;
    }

    /**
    * @param id
    * @return
    */
    public IVariableXmlList createList(String id) {
        VariableXmlList result = new VariableXmlList(id);
        addList(result);
        return result;
    }

    /**
	* @param name
	* @param value
	*/
	@SuppressWarnings("unchecked")
	public void setAttribute(String name, String value) {
        if (attributes == Collections.EMPTY_MAP) {
            attributes = new HashMap();
        }
        attributes.put(name, value);
    }
    
    /**
	* @param item
	*/
	@SuppressWarnings("unchecked")
	protected void addItem(IVariableXmlItem item) {
        if (items == Collections.EMPTY_LIST) {
            items = new ArrayList();
        }
        items.add(item);
    }
    
    /**
    * @param list
    */
    @SuppressWarnings("unchecked")
	protected void addList(IVariableXmlList list) {
        if (lists == Collections.EMPTY_LIST) {
            lists = new ArrayList();
        }
        lists.add(list);
    }

    /**
    * @return
    */
    protected String getEncoding(){
        if (encoding == null){
            IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
            encoding = pm.getString("framework.config.defaultEncoding");
        }
        return encoding;
    }
    
    /**
    * @param out
    * @throws Exception
    */
    @SuppressWarnings("unchecked")
	public void response(IOutput out) throws Exception {
        StringBuffer xml = new StringBuffer();
        
        // MS IE는 MS949 인코딩(UTF-8이 아닌 인코딩?)에서 xml 헤더가 필요함.
        HttpServletRequest request = (HttpServletRequest)out.getAdapter(HttpServletRequest.class);
        String agent = request.getHeader("User-Agent");
        if (org.apache.commons.lang.StringUtils.contains(agent, "MSIE")) {
            xml.append("<?xml version='1.0' encoding='" + getEncoding()
                    + "' standalone='yes'?>");
        }
        
//        xml.append("<response");
        if (code != null) {
            xml.append(" code='" + code + "'");
        }
        if (message != null) {
            xml.append(" message='" + message + "'");
        }
        
        String name = null;
        for (Iterator all = attributes.keySet().iterator(); all.hasNext();) {
            name = (String) all.next();
            xml.append(" " + name + "='" + attributes.get(name) + "'");
        }
//        xml.append(">");
        for (int i = 0; i < items.size(); i++) {
            xml.append(((IVariableXmlItem) items.get(i)).toXml());
        }
        for (int i = 0; i < lists.size(); i++) {
            xml.append(((IVariableXmlList) lists.get(i)).toXml());
        }
//        xml.append("</response>");
        
        out.write(xml.toString(), "text/xml");
    }
}
