/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.core.base.ComponentRegistry;
import com.core.component.format.IFormatManagement;
import com.core.component.variablexml.IVariableXmlItem;

/**
* 업무 그룹명 : com.core.component.variablexml.internal
* 서브 업무명 : VariableXmlItem.java
* 작성자 : cyLeem
* 작성일 : 2008. 10. 23
* 설 명 : VariableXmlItem
*/
public class VariableXmlItem implements IVariableXmlItem {

    private String id;

    @SuppressWarnings("unchecked")
	private List properties = new ArrayList();

    private Locale locale;

    private static IFormatManagement fm = (IFormatManagement) ComponentRegistry
            .lookup(IFormatManagement.class);

    /**
    * VariableXmlItem
    */
    public VariableXmlItem() {
        //
    }

    /**
    * @param id
    */
    public VariableXmlItem(String id) {
        this.id = id;
    }

    /**
    * @param locale
    */
    public VariableXmlItem(Locale locale) {
        this.locale = locale;
    }

    /**
    * @param id
    * @param locale
    */
    public VariableXmlItem(String id, Locale locale) {
        this.id = id;
        this.locale = locale;
    }

    /**
    * @param id
    */
    public VariableXmlItem(int id) {
        this.id = new Integer(id).toString();
    }

    /**
    * @param id
    * @param locale
    */
    public VariableXmlItem(int id, Locale locale) {
        this.id = new Integer(id).toString();
        this.locale = locale;
    }

    /**
    * @param name
    * @param value
    */
    public void addProperty(String name, Object value) {
        properties.add(new VariableXmlProperty(name, value));
    }

    /**
    * @param name
    * @param value
    */
    public void addProperty(String name, int value) {
        addProperty(name, new Integer(value).toString());
    }

    /**
    * @param name
    * @param value
    */
    public void addCodeProperty(String name, Object value) {
        addProperty(name, fm.format("code", value, locale));
    }

    /**
    * @param name
    * @param value
    */
    public void addDateProperty(String name, Object value) {
        addProperty(name, fm.format("date", value, locale));
    }

    /**
    * @param name
    * @param value
    */
    public void addNumberProperty(String name, Object value) {
        addProperty(name, fm.format("number", value, locale));
    }

    /**
    * @param name
    * @param value
    * @param params
    * @param formatter
    */
    public void addFormatProperty(String name, Object value, String[] params,
            String formatter) {
        addProperty(name, fm.format(formatter, value, locale, params));
    }

    /**
    * @return
    */
    public String toXml() {
        StringBuffer buffer = new StringBuffer(256);
        appendXml(buffer);
        return buffer.toString();
    }

    /**
    * @param buffer
    */
    public void appendXml(StringBuffer buffer) {

        if (id == null) {
            buffer.append("<NODE>");
        } else {
            buffer.append("<NODE id='");
            buffer.append(id);
            buffer.append("'>");
        }
        for (int i = 0; i < properties.size(); i++) {
            ((VariableXmlProperty) properties.get(i)).appendXml(buffer);
        }
        buffer.append("</NODE>");
    }

}
