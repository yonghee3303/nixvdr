/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml;

/**
 * VariableXml를 위한 XML 응답의 태그(item)를 나타내는 인터페이스.
 * 
 * @since 3.0
 */
public interface IVariableXmlItem {

    /**
     * property 태그로 표현되는 속성을 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     */
    void addProperty(String name, Object value);

    /**
     * property 태그로 표현되는 속성을 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     */
    void addProperty(String name, int value);

    /**
     * property 태그로 표현되는 속성을 추가한다. 값은 코드 아이디이며 이를 코드 값으로 전환하여 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     */
    void addCodeProperty(String name, Object value);

    /**
     * property 태그로 표현되는 속성을 추가한다. 값은 날짜이며 이를 형식 전환하여 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     */
    void addDateProperty(String name, Object value);

    /**
     * property 태그로 표현되는 속성을 추가한다. 값은 숫자이며 이를 형식 전환하여 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     */
    void addNumberProperty(String name, Object value);

    /**
     * property 태그로 표현되는 속성을 추가한다. 값을 주어진 formatter를 이용하여 형식 전환하여 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     * @param formatter
     *            IFormatter 구현 클래스의 아이디
     * @param params
     *            파라미터 배열
     */
    void addFormatProperty(String name, Object value, String[] params, String formatter);

    /**
     * 객체의 내용을 XML 형태로 전환한다.
     * 
     * @return XML 형태로 전환된 문자.
     */
    String toXml();
    
    /**
     * 객체의 내용을 XML 형태로 전환한다.
     * 
     * @return XML 형태로 전환된 문자.
     */
    void appendXml(StringBuffer buffer);    

}