/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml;

/**
 * VariableXml를 위한 XML 응답의 태그(<list>)를 나타내는 인터페이스.
 * 
 * @since 3.0
 */
public interface IVariableXmlList {

    /**
     * id가 있는 IVariableXmlItem 객체를 반환한다. id가 있는 IVariableXmlItem 객체는 여러 개를 만들 수 있다.
     * 
     * @param id 0부터 시작하는 순서
     * @return IVariableXmlItem
     */
    IVariableXmlItem createItem(int id);
    
    /**
     * list의 XML 표현에 나타날 Attribute를 추가한다. 
     * @param name Attribute name 
     * @param value Attribute Value
     */
    void addAttribute(String name, String value);

    
    /**
     * 객체의 내용을 XML 형태로 전환한다.
     * @return XML 형태로 전환된 문자.
     */
    String toXml();

}