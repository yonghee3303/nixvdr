/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml;

import com.core.framework.core.IResponse;

/**
 * VariableXml를 위한 XML 응답의 최상위 태그(response)를 나타내는 인터페이스.
 * 
 * @since 3.0
 */
public interface IVariableXmlResponse extends IResponse {

    /**
     * id가 없는 IVariableXmlItem 객체를 반환한다. id가 없는 IVariableXmlItem 객체를 2 이상은 만들 수 없다.
     * 
     * @return IVariableXmlItem
     */
    IVariableXmlItem createItem();

    /**
     * id가 있는 IVariableXmlItem 객체를 반환한다. id가 있는 IVariableXmlItem 객체는 여러 개를 만들 수 있다.
     * 
     * @return IVariableXmlItem
     */
    IVariableXmlItem createItem(String id);

    /**
     * id가 없는 IVariableXmlList 객체를 반환한다. id가 없는 IVariableXmlList 객체를 2 이상은 만들 수 없다.
     * 
     * @return IVariableXmlList
     */
    IVariableXmlList createList();

    /**
     * id가 있는 IVariableXmlList 객체를 반환한다. id가 있는 IVariableXmlList 객체는 여러 개를 만들 수 있다.
     * 
     * @return IVariableXmlList
     */
    IVariableXmlList createList(String id);

    /**
     * response 태그에 속성을 추가한다.
     * 
     * @param name
     *            속성 이름
     * @param value
     *            속성 값
     */
    void setAttribute(String name, String value);

}