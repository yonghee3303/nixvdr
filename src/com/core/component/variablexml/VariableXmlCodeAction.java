/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.variablexml;

import java.util.List;

import com.core.base.ComponentRegistry;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import mg.base.BaseAction;

public class VariableXmlCodeAction extends BaseAction {

    private ICodeManagement cm = (ICodeManagement) getComponent(ICodeManagement.class);

    private String encoding = null;

    protected String getEncoding(){
        if (encoding == null){
            IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
            encoding = pm.getString("framework.config.defaultEncoding");
        }
        return encoding;
    }

    @Override
	public IResponse run(IRequest request) {
        List codes = null;
        String selector = request.getStringParam("selector");
        if (request.hasParameter("code")) {
            String id = request.getStringParam("code");
            codes = selector == null ? cm.getCodes(id) : cm.getCodes(id,
                    selector);
        } else {
            String childId = request.getStringParam("childCode");
            ICode child = cm.getCode(childId);
            codes = selector == null ? cm.getCodes(child.getParent().getId())
                    : cm.getCodes(child.getParent().getId(), selector);
        }

        StringBuffer buffer = new StringBuffer();

        // MS IE는 MS949 인코딩(UTF-8이 아닌 인코딩?)에서 xml 헤더가 필요함.
        buffer.append("<?xml version='1.0' encoding='" + getEncoding()
                    + "' standalone='yes'?>");

        buffer.append("<NODES>");
        for (int i = 0; i < codes.size(); i++) {
            ICode each = (ICode) codes.get(i);
            buffer.append("<NODE id='" + each.getAlias() + "' label='" + each.getName() + "'/>");
        }
        buffer.append("</NODES>");

        return writeXml(buffer.toString());
    }

}
