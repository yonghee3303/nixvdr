/*
 * Copyright (c) 2010 Genetics.. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics..
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics..
 */

package com.core.component.code.tag;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

import com.core.base.ComponentRegistry;
import com.core.base.Constants;
import com.core.base.SystemException;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.message.IMessageManagement;


/**
* 업무 그룹명 : com.core.component.code.tag
* 서브 업무명 : CodeTag.java
* 설 명 : <code>Select</code> 박스로 코드를 화면에 출력하는 JSP Custom tag 클래스.
*/
public class CodeTag extends TagSupport {

    private static final long serialVersionUID = -1903193073987012110L;
    
    protected Logger log = Logger.getLogger(this.getClass());

    /**
     * 메모리에 있는 <code>Code</code> 객체를 관리하는 컴포넌트.
     */
    protected ICodeManagement cm = (ICodeManagement) ComponentRegistry
            .lookup(ICodeManagement.class);

    protected IMessageManagement mm = (IMessageManagement) ComponentRegistry
            .lookup(IMessageManagement.class);

    protected String id; /* 출력되는 코드 리스트의 상위 코드의 아이디 */

    protected String name; /* Select 박스(radio / checkbox 그룹)의 이름 */

    protected String attribute = ""; /*
                                         * Select 박스(radio / checkbox 그룹)가 가질 수
                                         * 있는 속성
                                         */

    protected String view; /* select | radio | checkbox | dual | dual2 구분자 */

    protected String type; //"SITE_CTX_02".equals((String)(pageContext.getSession().getAttribute("COMS_DDS_SITE_CTX")))?"sl":"";

    protected String selector;

    protected String value;

    protected String defaultValue;

    protected String header;

    /* select box */
    protected String selectOpen;

    protected String selectClose;

    protected String selectOption;

    /* dual select box */
    protected String selectGroupOpen;

    protected String selectGroupClose;

    /* radio, chkbox */
    protected String inputTag;

    /**
    * CodeTag
    */ 
    public CodeTag(){
        selectOpen="<select id=\"$name$\" name=\"$name$\" $cssClass$ $attribute$  >";
        selectClose = "</select>";
        selectOption = "<option value=\"$value$\" $checked$ >$codeName$</option>";
        selectGroupOpen = "<optgroup label=\"$codeName$\">";
        selectGroupClose = "</optgroup>";
        inputTag = "<input type=\"$view$\" name=\"$name$\" "
                + " value=\"$value$\" $checked$ $attribute$"
                + " class=\"code\"> $codeName$</input>";
    }
    
    /**
     * @param parentId
     * @throws JspException
     */
    @Override
	public void setId(String parentId) {
    	log.error(parentId);
        try {
            this.id = (String) ExpressionUtil.evalNotNull("code", "id",
                    parentId, String.class, this, pageContext);
            
            setType(null);
        } catch (JspException e) {
            throw new SystemException(e);
        }
    }

    /**
     * @param name
     */
    public void setName(String name) {
    	log.error(name);
        this.name = name;
    }

    /**
    * @param type
    */
    public void setType(String type) {
    	log.error(type);
    	if(type != null && type.trim().length() < 1) {
        	;
        } else {
        	HttpSession sess = pageContext.getSession();
        	type = "";
        	if(sess != null) {
        		type = "SITE_CTX_02".equals(sess.getAttribute("COMS_DDS_SITE_CTX"))?"sl":"";
        	}
        	
        }
    	this.type = type;
    }

    /**
    * @param view
    */
    public void setView(String view) {
    	log.error(view);
        this.view = view;
    }

    /**
     * @param attribute
     */
    public void setAttribute(String attribute) {
    	log.error(attribute);
        this.attribute = (attribute == null) ? "" : attribute;
    }

    /**
    * @param value
    * @throws JspException
    */
    public void setValue(String value) throws JspException {
    	log.error(value);
        this.value = (String) ExpressionUtil.evalNotNull("code", "value",
                value, String.class, this, pageContext);
    }

    /**
    * @param selector
    */
    public void setSelector(String selector) {
    	log.error(selector);
        this.selector = selector;
    }

    /**
    * @param defaultValue
    * @throws JspException
    */
    public void setDefault(String defaultValue) throws JspException {
    	log.error(defaultValue);
        this.defaultValue = (String) ExpressionUtil.evalNotNull("code",
                "default", defaultValue, String.class, this, pageContext);
    }

    /**
    * @param header
    */
    public void setHeader(String header) {
    	log.error(header);
        this.header = header;
    }

    /**
     * @return
     * @throws JspException
     */
    @Override
	@SuppressWarnings("unchecked")
	public int doEndTag() throws JspException {
        String codeId = StringUtils.isEmpty(value) ? defaultValue : value;

        if (codeId != null) {
            codeId = codeId.trim();
        } else {
            codeId = "";
        }

        List codes = cm.getCodes(id, selector);
    	
        try {
        	String site_ctx = (String)(pageContext.getSession().getAttribute("COMS_DDS_SITE_CTX"));
        	Locale locale = null;
        	if("SITE_CTX_02".equals(site_ctx)) {
        		locale = Locale.US;
        	}
        	
        	render(codeId, codes, locale);
        } catch (IOException e) {
            throw new JspException(e);
        }
        return EVAL_PAGE;
    }

    /**
     * @param codeId
     * @param out
     * @param codes
     * @param locale
     * @param view
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
	protected void render(String codeId, List codes, Locale locale)
            throws IOException {
        JspWriter out = pageContext.getOut();
        if (view == null) {
            view = "select"; // 리스트 출력의 기본 뷰는 select
        }
        if (view.equalsIgnoreCase("radio")) {
            if (!StringUtils.isEmpty(header))
                out.print(getHeader(locale));
            ICode code = null;
            boolean checked = false;
            for (Iterator it = codes.iterator(); it.hasNext();) {
                code = (ICode) it.next();
                if (code.isActive()) {
                    checked = (code.getId().equals(codeId));
                    out.print(getHtmlTag(code, checked, locale));
                }
            }
        } else if (view.equalsIgnoreCase("checkbox")) {
            String[] splitedCodeIds = codeId.split(",");
            if (!StringUtils.isEmpty(header))
                out.print(getHeader(locale));
            boolean checked = false;
            ICode code = null;
            for (Iterator it = codes.iterator(); it.hasNext();) {
                code = (ICode) it.next();
                if (code.isActive()) {
                    for (int i = 0; i < splitedCodeIds.length; i++) {
                        if ((splitedCodeIds[i].trim()).equalsIgnoreCase(code
                                .getId())) {
                            checked = true;
                            break;
                        }
                    }
                    out.print(getHtmlTag(code, checked, locale));
                }
            }
        } else if (view.equalsIgnoreCase("select")) {
            ICode parentCode = cm.getCode(id);
            String style = (parentCode.getStyle() != null) ? " class=\""
                    + parentCode.getStyle() + "\" " : "";
            String temp = replace(selectOpen, "$name$", name);
            temp = replace(temp, "$cssClass$", style);
            temp = replace(temp, "$attribute$", attribute);
            out.print(temp);

            temp = null;
            if (header == null) {
                temp = replace(selectOption, "$value$", "");
                if (name.startsWith("q_")) {
                    temp = replace(temp, "$codeName$", mm
                            .getMessage("GMSG_1031", locale));
                } else {
                    temp = replace(temp, "$codeName$", mm
                            .getMessage("GMSG_1032", locale));
                }
            } else if (!"".equals(header)) {
                temp = getHeader(locale);
            }
            out.print(replace(temp, "$checked$", ""));
            ICode code = null;
            boolean checked = false;
            for (Iterator it = codes.iterator(); it.hasNext();) {
                code = (ICode) it.next();
                if (code.isActive()) {
                    checked = (code.getId().equals(codeId));
                    out.print(getHtmlTag(code, checked, locale));
                }
            }
            out.print(selectClose);
        } else if (view.equalsIgnoreCase("dual")
                || view.equalsIgnoreCase("dual2")) {

            String style = "";
            ICode parentCode = cm.getCode(id);
            if (parentCode.getStyle() != null) {
                style = " class=\"" + parentCode.getStyle() + "\" ";
            }
            String temp = replace(selectOpen, "$name$", name);
            temp = replace(temp, "$cssClass$", style);
            temp = replace(temp, "$attribute$", attribute);
            out.print(temp);

            if (header == null) {
                temp = replace(selectOption, "$value$", "");
                if (name.startsWith("q_")) {
                    temp = replace(temp, "$codeName$", mm
                            .getMessage("GMSG_1031", locale));
                } else {
                    temp = replace(temp, "$codeName$", mm
                            .getMessage("GMSG_1032", locale));
                }
            } else if (!"".equals(header)) {
                temp = getHeader(locale);
            }
            out.print(temp);
            ICode code = null;
            List secondaryCodes = null;
            ICode secondaryCode = null;
            boolean checked = false;
            for (Iterator it = codes.iterator(); it.hasNext();) {
                code = (ICode) it.next();
                if (code.isActive()) {
                    out.print(replace(selectGroupOpen,
                            "$codeName$", getCodeName(code, locale)));
                    secondaryCodes = cm.getCodes(code.getId(), selector);
                    for (Iterator all = secondaryCodes.iterator(); all
                            .hasNext();) {
                        secondaryCode = (ICode) all.next();
                        if (secondaryCode.isActive()) {
                            checked = (secondaryCode.getId()
                                    .equals(codeId));
                            out
                                    .print(getHtmlTag(secondaryCode, checked,
                                            locale));
                        }
                    }
                    out.print(selectGroupClose);
                }
            }
            out.print(selectClose);
        } else {
            throw new SystemException(
                    "Wrong value for attribute 'view' in <ui:code>");
        }
    }

    /**
    * @param code
    * @param locale
    * @return
    */
    private String getCodeName(ICode code, Locale locale) {
        String codename = null;
        //log.error("locale = " + locale + ", al = " + code.getName() + ", sl = " + code.getNameSl() + ", nm = " + code.getName(locale));
        if(locale == null) {
	        if (Constants.SL.equals(type)) {
	            codename = code.getNameSl();
	        } else if (Constants.AL.equals(type)) {
	            codename = code.getAlias();
	        } else {
	            codename = code.getName();
	        }
        } else {
        	if("US".equals(locale.getCountry())) {
        		codename = code.getNameSl();
        	} else {
        		codename = code.getName(locale);	
        	}

            if(codename == null || codename.length() < 1) {
            	codename = code.getName(locale);        	
            }
        }
        
        
        return codename;
    }

    /**
    * @param code
    * @param locale
    * @return
    */
    private String getCodeId(ICode code, Locale locale) {
        return (code == null) ? "" : (view.equalsIgnoreCase("dual2") ? code
                .getParent().getId()
                + "|" : "")
                + code.getId();
    }

    /**
    * @param code
    * @param checked
    * @param locale
    * @return
    */
    private String getHtmlTag(ICode code, boolean checked, Locale locale) {
        String codeId = getCodeId(code, locale);
        String codeName = (code == null) ? header : getCodeName(code, locale);
        
        if (view.equalsIgnoreCase("radio") || view.equalsIgnoreCase("checkbox")) {
            String temp = replace(inputTag, "$view$", view);
            temp = replace(temp, "$name$", name);
            temp = replace(temp, "$value$", codeId);
            temp = replace(temp, "$checked$",
                    (checked ? "checked=\"checked\"" : ""));
            temp = replace(temp, "$attribute$", attribute);
            temp = replace(temp, "$codeName$", codeName);
            return temp;
        }
        String temp = replace(selectOption, "$value$", codeId);
        temp = replace(temp, "$checked$",
                (checked ? "selected=\"selected\"" : ""));
        temp = replace(temp, "$codeName$", codeName);
        return temp;
    }

    /**
    * @param locale
    * @return
    */
    private String getHeader(Locale locale) {
        boolean checked = (defaultValue != null && defaultValue.equals(""));
        return getHtmlTag(null, checked, locale);
    }
    
    /**
    * @param text
    * @param repl
    * @param with
    * @return
    */
    private String replace(String text, String repl, String with){
        with = (with == null)? "" : with;
        return StringUtils.replace(text, repl, with);
    }

}