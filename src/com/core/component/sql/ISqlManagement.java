/*
 * Copyright (c) 2010 ITM System. All rights reserved.
 *
 * This software is the confidential and proprietary information of ITM System.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with ITM System.
 */

package com.core.component.sql;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.core.base.IRefreshable;

/**
 * iBATIS SqlMapper를 사용하기 위한 인터페이스.
 * 
 * @since 2.0
 */
public interface ISqlManagement extends IRefreshable {

    /**
     * stmt의 실행결과를 <code>Object</code> 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param id
     *            찾고자 하는 row의 Key
     * @return Object 실행 결과
     */
    Object getItem(String mapStmtName, Object id);

    /**
     * stmt의 실행결과를 item에 반영하여 반환한다. 즉 변수의 item과 반환되는 객체는 동일한 객체이다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param id
     *            찾고자 하는 row의 Key
     * @param item
     *            실행결과를 담을 객체
     * @return 실행 결과
     */
    Object loadItem(String mapStmtName, Object id, Object item);

    /**
     * stmt의 실행결과를 <code>List</code> 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getList(String mapStmtName);

    /**
     * stmt의 실행결과를 <code>List</code> 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param obj
     *            검색조건을 담고 있는 객체
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getList(String mapStmtName, Object obj);

    /**
     * 결과를 List 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param pageNo
     *            검색결과 중 가져오게 될 페이지 번호
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getPageList(String mapStmtName, int pageNo);

    /**
     * 검색 조건에 부합되는 결과를 List 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param pageNo
     *            검색결과 중 가져오게 될 페이지 번호
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getPageList(String mapStmtName, Object query, int pageNo);

    /**
     * 검색 조건에 부합되는 결과를 페이지 단위 List 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param obj
     *            검색조건을 담고 있는 객체
     * @param pageNo
     *            검색결과 중 가져오게 될 페이지 번호
     * @param pageSize
     *            검색결과에 대한 페이지 사이즈.
     * 
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getPageList(String mapStmtName, Object obj, int pageNo, int pageSize);

    /**
     * 검색 결과를 key에 대한 값을 키로하는 java.util.Map으로 변환하여 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param query
     *            검색 조건
     * @param key
     *            반환되는 Map의 키로 사용될 값을 지칭하는 키
     * @return java.util.Map
     */
    Map getMap(String mapStmtName, Object query, String key);

    /**
     * 검색 결과를 key에 대한 값을 키로하고, value에 대한 값을 값으로 하는 java.util.Map으로 변환하여 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param query
     *            검색 조건
     * @param key
     *            반환되는 Map의 키로 사용될 값을 지칭하는 키
     * @param value
     *            반환되는 Map의 값으로 사용될 값을 지칭하는 키
     * @return java.util.Map
     */
    Map getMap(String mapStmtName, Object query, String key, String value);

    /**
     * 현재 사용되고 있는 DataSource를 가져 온다.
     * 
     * @return DataSource
     */
    DataSource getDataSource1();
    
    DataSource getDataSource2();

    /**
     * 현재 사용되고 있는 Connection을 가져 온다.
     * 
     * @return Connection
     */
    Connection getCurrentLocalConnection1();
    Connection getCurrentLocalConnection2();

    /**
     * 작업을 수행할 때 사용할 Connection을 지정한다.
     * 
     * @param con
     *            java.sql.Connection
     */
    void loadLocalConnection1(Connection con);
    void loadLocalConnection2(Connection con);

    /**
     * 객체의 데이터를 Database에 저장한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param obj
     *            저장할 데이터를 갖고 있는 객체
     */
    void createObject(String mapStmtName, Object obj);

    /**
     * 데이터를 Database에서 삭제한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param obj
     *            삭제할 데이터의 정보를 담고 있는 객체
     * @return 삭제된 레코드 수
     */
    int deleteObject(String mapStmtName, Object obj);

    /**
     * 데이터를 Database에 update 한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param obj
     *            업데이트 할 데이터를 담고 있는 객체
     * @return 변경된 레코드 수
     */
    int updateObject(String mapStmtName, Object obj);

    /**
     * 트랜젝션을 시작 한다.
     */
    void begin();

    /**
     * 트랜젝션을 commit 한다.
     */
    void commit();

    /**
     * 트랜젝션을 rollback 한다.
     */
    void rollback();

    /**
     * Batch 트랜젝션을 시작 한다.
     */
    void beginBatch();

    /**
     * Batch 트랜젝션을 종료 한다.
     */
    void endBatch();

    /**
     * SqlMapper Instance를 반환한다.
     * 
     * @param config
     *            새로 생성할 SqlMapper 객체 인스턴스를 위한 configuration 파일의 이름
     * @return ISqlMapper
     */
    ISqlManagement getOtherMapper(String config);

    /**
     * StoredProcedure, StoredFunction을 실행하고 결과를 반환한다.
     * 
     * @param procId
     *            실행하고자 하는 procedure 또는 function의 id
     * @param map
     *            프로시저에 전달되는 데이터를 담고 있는 map
     * @return Object
     */
    Object executeProcedure(String procId, Map map);
    
    /**
     * DBMS 벤더별 최적 쿼리를 수행하여 , 검색 조건에 부합되는 결과를 페이지 단위 List 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param params
     *            검색조건을 담고 있는 객체
     * @param pageNo
     *            검색결과 중 가져오게 될 페이지 번호
     * 
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getOptimalPageList(String mapStmtName, Map params, int pageNo);       
    
    /**
     * DBMS 벤더별 최적 쿼리를 수행하여, 검색 조건에 부합되는 결과를 페이지 단위 List 형태로 반환한다.
     * 
     * @param mapStmtName
     *            실행할 SqlMap statement의 id
     * @param params
     *            검색조건을 담고 있는 객체
     * @param pageNo
     *            검색결과 중 가져오게 될 페이지 번호
     * @param pageSize
     *            검색결과에 대한 페이지 사이즈.
     * 
     * @return List 실행 결과를 Domain Object들의 List 형태로 반환
     */
    List getOptimalPageList(String mapStmtName, Map params, int pageNo, int pageSize);    

}
