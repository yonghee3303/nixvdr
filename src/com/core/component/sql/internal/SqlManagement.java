/*
 * Copyright (c) 2010 ITM System. All rights reserved.
 *
 * This software is the confidential and proprietary information of ITM System.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with ITM System.
 */

package com.core.component.sql.internal;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.app.action.SqlActionLoader;
import com.core.base.SystemException;
import com.core.component.navigation.PageList;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.DuplicationException;
import com.core.component.sql.IOptimalQueryExecutor;
import com.core.component.sql.ISqlManagement;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapSession;

/**
 * iBATIS Framework를 사용하기 위한 인터페이스 <code>ISqlManagement</code> 를 구현한 클래스.
 * 
 * @since 2.0
 */
public class SqlManagement implements ISqlManagement {

    protected static int defaultListSize = -1;

    protected static int pkErrorCode = -1;

    protected static int fkErrorCode = -1;
    
    //sqlMap1 은 CRUD 중 CUD 를 처리
    protected SqlMapClient sqlMap1 = null;
    
    //sqlMap2 은 CRUD 중 R 를 처리
    protected SqlMapClient sqlMap2 = null;

    //sqlMap1 세팅파일
    protected String configFile1;
    
    //sqlMap2 세팅파일
    protected String configFile2;

    protected IParameterManagement pm;

    protected IOptimalQueryExecutor optimalQuery;
    
    private Log log = LogFactory.getLog(SqlActionLoader.class);

    public SqlManagement() {
        // 기본 생성자
    }

    protected SqlManagement(String config) {
    	sqlMap1 = SqlMapLoader.getSqlMap(config);
    }

    public void setParameterManagement(IParameterManagement pm) {
        this.pm = pm;
        // fkErrorCode = pm.getInt("component.sql.fkErrorCode");
    }

    public void setOptimalQueryExecutor(IOptimalQueryExecutor optimalQuery) {
        this.optimalQuery = optimalQuery;
    }

    public void setConfigFile1(String configFile1) {
        this.configFile1 = configFile1;
    }
    
    public void setConfigFile2(String configFile2) {
        this.configFile2 = configFile2;
    }

    protected SqlMapClient getSqlMap1() {
        return sqlMap1;
    }
    
    protected SqlMapClient getSqlMap2() {
        return sqlMap2;
    }

    public void init() {
        try {
        	log.info("init------configFile1configFile1configFile1----------------\n" + configFile1);
        	sqlMap1 = SqlMapLoader.getSqlMap(configFile1);
        	sqlMap2 = SqlMapLoader.getSqlMap(configFile2);
            if (optimalQuery == null) {
                optimalQuery = new OracleOptimalQueryExcecutor();
            }
        } catch (Exception e) {
        	sqlMap1 = new FailedSqlMapClient(this);
        	sqlMap2 = new FailedSqlMapClient(this);
            throw new SystemException(e);
        }
    }

	public DataSource getDataSource1() {
    	return getSqlMap1().getDataSource();
    }
	
	public DataSource getDataSource2() {
    	return getSqlMap2().getDataSource();
    }

    public Connection getCurrentLocalConnection1() {
		try {
			return getSqlMap1().getCurrentConnection();
		} catch (SQLException e) {
			throw new SystemException(e);
		}
    }
    
    public Connection getCurrentLocalConnection2() {
		try {
			return getSqlMap2().getCurrentConnection();
		} catch (SQLException e) {
			throw new SystemException(e);
		}
    }

    public void loadLocalConnection1(Connection con) {
    	try {
            getSqlMap1().setUserConnection(con);
        } catch (SQLException e) {
            throw new SystemException();
        }
    }
    
    public void loadLocalConnection2(Connection con) {
    	try {
            getSqlMap2().setUserConnection(con);
        } catch (SQLException e) {
            throw new SystemException();
        }
    }

    public void begin() {
        try {
        	getSqlMap1().startTransaction();
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public void beginBatch() {
        try {
        	getSqlMap1().startBatch();
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public void endBatch() {
        try {
        	getSqlMap1().executeBatch();
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public void commit() {
        try {
        	getSqlMap1().commitTransaction();
        	getSqlMap1().endTransaction();
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public void rollback() {
        try {
        	getSqlMap1().endTransaction();
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public void createObject(String mapStmtName, Object obj) {
        try {
        	getSqlMap1().insert(mapStmtName, obj);
        } catch (SQLException e) {
            if (pkErrorCode == -1) {
                pkErrorCode = pm.getInt("component.sql.pkErrorCode");
            }
            if (e.getErrorCode() == pkErrorCode) {
                throw new DuplicationException(e);
            }
            throw new SystemException(e);
        }
    }

    public int updateObject(String mapStmtName, Object obj) {
        try {
            return getSqlMap1().update(mapStmtName, obj);
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public int deleteObject(String mapStmtName, Object obj) {
        try {
            return getSqlMap1().delete(mapStmtName, obj);
        } catch (SQLException e) {
            if (fkErrorCode == -1) {
                fkErrorCode = pm.getInt("component.sql.fkErrorCode");
            }
            if (e.getErrorCode() == fkErrorCode) {
                throw new SystemException("GMSG_1041", e);
            }
            throw new SystemException(e);
        }
    }

    public Object getItem(String mapStmtName, Object id) {
        try {
            return getSqlMap2().queryForObject(mapStmtName, id);
        } catch (SQLException e) {        	
            throw new SystemException(e);
        }
    }

    public Object loadItem(String mapStmtName, Object id, Object item) {
        try {
            return getSqlMap2().queryForObject(mapStmtName, id, item);
        } catch (SQLException e) { 
            throw new SystemException(e);
        }
    }

    public List getList(String mapStmtName) {
        return getList(mapStmtName, null);
    }

    public List getList(String mapStmtName, Object obj) {
        try {
            return getSqlMap2().queryForList(mapStmtName, obj);
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public List getPageList(String mapStmtName, int pageNo) {
        if (defaultListSize == -1) {
            defaultListSize = pm.getInt("component.navigation.defaultListSize");
        }
        return getPageList(mapStmtName, null, pageNo, defaultListSize);
    }

    public List getPageList(String mapStmtName, Object query, int pageNo) {
        if (defaultListSize == -1) {
            defaultListSize = pm.getInt("component.navigation.defaultListSize");
        }
        return getPageList(mapStmtName, query, pageNo, defaultListSize);
    }

    public List getPageList(String mapStmtName, Object query, int pageNo,
            int listSize) {

        pageNo = pageNo < 1 ? 1 : pageNo;
        try {
            return new PageList(getSqlMap2().queryForList(mapStmtName, query,
                    (pageNo - 1) * listSize, listSize), getTotalCount(
                    mapStmtName, query), pageNo, listSize);
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }
    
    public List getOptimalPageList(String mapStmtName, Map query, int pageNo) {
        if (defaultListSize == -1) {
            defaultListSize = pm.getInt("component.navigation.defaultListSize");
        }
        return getOptimalPageList(mapStmtName, query, pageNo, defaultListSize);
    }    

    public List getOptimalPageList(String mapStmtName, Map query, int pageNo,
            int listSize) {
        return optimalQuery.getOptimalPageList(this,mapStmtName,query,pageNo, listSize);
    }

    public Map getMap(String mapStmtName, Object query, String key) {
        try {
            return getSqlMap2().queryForMap(mapStmtName, query, key);
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public Map getMap(String mapStmtName, Object query, String key, String value) {
        try {
            return getSqlMap2().queryForMap(mapStmtName, query, key, value);
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    private int getTotalCount(String mapStmtName, Object query)
            throws SQLException {
        return ((Integer) getSqlMap2().queryForObject(mapStmtName + "Count",
                query)).intValue();

    }

    public ISqlManagement getOtherMapper(String config) {
        return new SqlManagement(config);
    }

    public Object executeProcedure(String procId, Map map) {
        try {
            return getSqlMap1().queryForObject(procId, map);
        } catch (SQLException e) {
            throw new SystemException(e);
        }
    }

    public void refresh() {
        init();
    }

}