/*
 * Copyright (c) 2010 ITM System. All rights reserved.
 *
 * This software is the confidential and proprietary information of ITM System.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with ITM System.
 */

package com.core.component.sql.internal;

import com.core.app.action.SqlActionLoader;
import com.core.base.SystemException;
import com.core.component.sql.ISqlMapLoader;
import com.core.component.sql.internal.FailedSqlMapClient;
import com.core.component.sql.internal.OracleOptimalQueryExcecutor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * iBATIS Framework를 사용하기 위한 인터페이스 <code>ISqlManagement</code> 를 구현한 클래스.
 * 
 * @since 2.0
 */
public class JdbcSqlManagement extends SqlManagement {
    
	private Log log = LogFactory.getLog(SqlActionLoader.class);
	
    protected ISqlMapLoader sqlLoader;
        
    public JdbcSqlManagement() {
        // 기본 생성자
    }

    public void init() {
        try {
        	// log.debug("----------------------------------------------------------------------------\n" + configFile);
            sqlMap1 = sqlLoader.getSqlMap(configFile1);
            sqlMap2 = sqlLoader.getSqlMap(configFile2);
            // log.debug("----------------------------------------------------------------------------\n" + sqlMap);
            if (optimalQuery == null) {
                optimalQuery = new OracleOptimalQueryExcecutor();
            }
        } catch (Exception e) {
            sqlMap1 = new FailedSqlMapClient(this);
            sqlMap2 = new FailedSqlMapClient(this);
            throw new SystemException(e);
        }
    }

    /**
     * @param sqlLoader
     */
    public void setSqlLoader(ISqlMapLoader sqlLoader) {
        this.sqlLoader = sqlLoader;
    }

}