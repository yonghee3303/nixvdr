/*
 * Copyright (c) 2010 ITM System. All rights reserved.
 *
 * This software is the confidential and proprietary information of ITM System.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with ITM System.
 */

package com.core.component.sql.internal;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.time.StopWatch;

import com.core.component.monitor.IMonitorManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.IOptimalQueryExecutor;
import com.core.component.sql.ISqlManagement;

/**
 * 
 * @since 3.0
 */
public class SqlMonitorManagement implements ISqlManagement {

    /**
     * 
     */
		
    private static final long serialVersionUID = -4641367394224992609L;

    protected SqlManagement sm;

    protected IMonitorManagement mm;

    protected Map monitors = new HashMap();

    public SqlMonitorManagement() {
        sm = new SqlManagement();
    }

    public void setConfigFile1(String configFile1) {
        sm.setConfigFile1(configFile1);
    }
    
    public void setConfigFile2(String configFile2) {
        sm.setConfigFile2(configFile2);
    }

    public void setParameterManagement(IParameterManagement pm) {
        sm.setParameterManagement(pm);
    }

    public void setMonitorManagement(IMonitorManagement mm) {
        this.mm = mm;
    }

    public void setOptimalQueryExecutor(IOptimalQueryExecutor optimalQuery) {
        sm.setOptimalQueryExecutor(optimalQuery);
    }

    public void init() {
        sm.init();
    }

    public void begin() {
        sm.begin();
    }

    public void beginBatch() {
        sm.beginBatch();
    }

    public void commit() {
        sm.commit();
    }

    public void createObject(String mapStmtName, Object obj) {
        StopWatch sw = new StopWatch();
        sw.start();
        sm.createObject(mapStmtName, obj);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
    }

    public int deleteObject(String mapStmtName, Object obj) {
        StopWatch sw = new StopWatch();
        sw.start();
        int result = sm.deleteObject(mapStmtName, obj);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public void endBatch() {
        sm.endBatch();
    }

    public Object executeProcedure(String procId, Map map) {
        StopWatch sw = new StopWatch();
        sw.start();
        Object result = sm.executeProcedure(procId, map);
        sw.stop();
        mm.monitor(ISqlManagement.class, procId, sw.getTime());
        return result;
    }

    public Connection getCurrentLocalConnection1() {
        return sm.getCurrentLocalConnection1();
    }

    public DataSource getDataSource1() {
        return sm.getDataSource1();
    }
    
    public Connection getCurrentLocalConnection2() {
        return sm.getCurrentLocalConnection2();
    }

    public DataSource getDataSource2() {
        return sm.getDataSource2();
    }

    public Object getItem(String mapStmtName, Object id) {
        StopWatch sw = new StopWatch();
        sw.start();
        Object result = sm.getItem(mapStmtName, id);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public Object loadItem(String mapStmtName, Object id, Object item) {
        StopWatch sw = new StopWatch();
        sw.start();
        Object result = sm.loadItem(mapStmtName, id, item);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public List getList(String mapStmtName) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getList(mapStmtName);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public List getList(String mapStmtName, Object obj) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getList(mapStmtName, obj);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public ISqlManagement getOtherMapper(String config) {
        return sm.getOtherMapper(config);
    }

    public List getPageList(String mapStmtName, int pageNo) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getPageList(mapStmtName, pageNo);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public List getPageList(String mapStmtName, Object query, int pageNo) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getPageList(mapStmtName, query, pageNo);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public List getPageList(String mapStmtName, Object obj, int pageNo,
            int pageSize) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getPageList(mapStmtName, obj, pageNo, pageSize);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public List getOptimalPageList(String mapStmtName, Map params, int pageNo,
            int pageSize) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getOptimalPageList(mapStmtName, params, pageNo,
                pageSize);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public List getOptimalPageList(String mapStmtName, Map params, int pageNo) {
        StopWatch sw = new StopWatch();
        sw.start();
        List result = sm.getOptimalPageList(mapStmtName, params, pageNo);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public Map getMap(String mapStmtName, Object query, String key) {
        StopWatch sw = new StopWatch();
        sw.start();
        Map result = sm.getMap(mapStmtName, query, key);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public Map getMap(String mapStmtName, Object query, String key, String value) {
        StopWatch sw = new StopWatch();
        sw.start();
        Map result = sm.getMap(mapStmtName, query, key, value);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public void loadLocalConnection1(Connection con) {
        sm.loadLocalConnection1(con);
    }
    
    public void loadLocalConnection2(Connection con) {
        sm.loadLocalConnection2(con);
    }

    public void rollback() {
        sm.rollback();
    }

    public int updateObject(String mapStmtName, Object obj) {
        StopWatch sw = new StopWatch();
        sw.start();
        int result = sm.updateObject(mapStmtName, obj);
        sw.stop();
        mm.monitor(ISqlManagement.class, mapStmtName, sw.getTime());
        return result;
    }

    public void refresh() {
        sm.refresh();
    }

}