/*
 * Copyright (c) 2010 ITM System. All rights reserved.
 *
 * This software is the confidential and proprietary information of ITM System.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with ITM System.
 */

package com.core.framework.action.internal;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;

import com.core.base.ComponentRegistry;
import com.core.base.ILoader;
import com.core.component.bean.IBeanManagement;
import com.core.component.xml.JdomTemplate;

/**
 * 타입에 대한 설명.
 * 
 * @since 3.0
 */
public class XmlActionLoader extends JdomTemplate implements ILoader {

    private String fileName = "config/action/action.xml";

    private String extension = ".do";

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setExtension(String extension) {
        this.extension = "." + extension;
    }

    public Map loadMap() {
        IBeanManagement bm = (IBeanManagement) ComponentRegistry.lookup(IBeanManagement.class);
        Map result = new HashMap();
        
        // String tmpFile = getClass().getClassLoader().getResource(fileName).getFile().replace("bin\\content", "standalone\\deployments");
        // File file = new File(tmpFile);
        
        File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
        setFile(file);
        Document document = getSaxDocument();
        Element root = document.getRootElement();

        List list = root.getChildren("action");
        for (Iterator all = list.iterator(); all.hasNext();) {
            Element each = (Element) all.next();
            String id = each.getAttributeValue("id");
            String type = each.getChildTextTrim("type");
            
            if (id.equals("/polaris/verifyToken")) {
            	result.put(id + ".json", bm.load(type));
            }else {
            	result.put(id + extension, bm.load(type));
            }
            
            
        }
        return result;
    }

    public Collection loadCollection() {
        throw new UnsupportedOperationException();
    }

}
