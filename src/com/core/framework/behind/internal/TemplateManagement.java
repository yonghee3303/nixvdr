package com.core.framework.behind.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.core.component.util.BaseUtils;
import com.core.component.util.IMapIteratorStrategy;
import com.core.framework.behind.ITemplateManagement;

/**
* 업무 그룹명 : com.core.framework.behind.internal
* 서브 업무명 : TemplateManagement.java
* 작성자 : 이병기
* 작성일 : 2008. 11. 11
* 설 명 : TemplateManagement
*/
public class TemplateManagement implements ITemplateManagement {

    protected Map mappings;
    protected List mapkeys;

    protected String baseTemplate;
  
    /**
    * 
    */
    public TemplateManagement() {
    }

    /**
     * setMappings
     * @param map
     */
    public void setMappings(Map map) {
        mappings = new HashMap();
        mapkeys = new ArrayList();
        
        BaseUtils.iterateMap(map, new IMapIteratorStrategy() {

            public void iterate(Map target, Object key, Object value) {
                if ("base".equals(key)) {
                    baseTemplate = (String) value;
                }
                else {
                    mappings.put(key, value);
                    mapkeys.add(key);
                }
            }
        });
        
        Collections.sort(mapkeys);
    }

    /**
     * getTemplate
     */
    public String getTemplate(PageContext pageContext) {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        String path = request.getServletPath();
        //System.out.println("request servlet path = [" + path + "]");
        String key;
        for (int i=(mapkeys.size()-1); i >=0 ; i--) {
            key = (String) mapkeys.get(i);
            //System.out.println("template key = [" + key + "]");
            if (path.startsWith(key)) {
            	//System.out.println("matched template key = [" + key + "," + path + "]");
                return (String) mappings.get(key);
            }
        }

        return baseTemplate;
    }
}
