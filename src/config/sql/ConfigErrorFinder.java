package config.sql;

import java.io.File;
import java.io.FilenameFilter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ConfigErrorFinder {

    private static DocumentBuilder builder;

    public static void main(String[] args) throws Exception {
        File configDirectory = getFileInResource("/config/sql");

        File[] configFiles = configDirectory.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith("sql-config.xml");
            }
        });

        for (int i = 0; configFiles != null && i < configFiles.length; i++) {
            System.out.println(":: " + configFiles[i].getName()
                    + " 파일에 대한 검증 수행 :: ");
            Document document = parse(configFiles[i]);
            NodeList nodeList = document.getElementsByTagName("sqlMap");
            for (int j = 0; j < nodeList.getLength(); j++) {
                Element node = (Element) nodeList.item(j);
                String sqlFileName = node.getAttribute("resource");
                try {
                    parse(getFileInResource("/" + sqlFileName));
                } catch (Exception e) {
                    // org.xml.sax.ErrorHandler를 통한 에러 처리
                }
            }
            System.out.println("");
        }
    }

    private static File getFileInResource(String path) {
        File file = new File(ConfigErrorFinder.class.getResource(path)
                .getFile());
        return file;
    }

    private static Document parse(File file) throws Exception {
        DocumentBuilder builder = getDocumentBuilder();
        return builder.parse(file);
    }

    private static DocumentBuilder getDocumentBuilder() throws Exception {
        if (builder == null) {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory
                    .newInstance();
            builderFactory.setValidating(true);
            builder = builderFactory.newDocumentBuilder();
            builder.setErrorHandler(new ErrorHandler() {

                public void warning(SAXParseException e) throws SAXException {
                    reportError(e, "경고");
                }

                public void fatalError(SAXParseException e) throws SAXException {
                    reportError(e, "치명적 오류");
                }

                public void error(SAXParseException e) throws SAXException {
                    reportError(e, "오류");
                }

                private void reportError(SAXParseException e, String type) {
                    System.out.println("["
                            + type
                            + "] "
                            + e.getSystemId().substring(
                                    e.getSystemId().indexOf("classes/") + 8)
                            + "(" + e.getLineNumber() + ", "
                            + e.getColumnNumber() + ") - "
                            + e.getLocalizedMessage());
                }

            });
        }
        return builder;
    }

}
