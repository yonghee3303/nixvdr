package mg.log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import mg.base.BaseAction;
import mg.base.cryptorSHA;

import org.json.simple.JSONObject;

import com.core.base.ComponentRegistry;
import com.core.component.compress.ICompressManagement;
import com.core.component.util.FileUtils;
import com.core.component.util.WebUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class LogFileAction extends BaseAction{
	
	public IResponse run(IRequest request) throws Exception {
		  
       if (request.isMode("tx_service")) { //사본 파일이 없을 때 로그 기록을 위해 사용되는 서비스 > 얘를 cp한테 알려주면 되
            return serviceUpload(request);
        } else {
        	return write(null);
        }
    }
	
	private void textReplace(Map map) {
        Iterator it = map.keySet().iterator(); 
        while(it.hasNext()) {
        	String key = (String)it.next();
        	String value = this.getStringWithNullCheck(map, key);
        	map.put(key, value.replace("▦", "#"));
        	//log.info(":::::::::: Replace Key :: " + key + "\n Replace Value :: " + map.get(key));
        }
	}
	private String createDocId(Map map) {
    	//데이터 타입
    	String dataType = ((String)map.get("docId")).substring(0, 3);
    	//현재 시간
    	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    	String reqDate = formatter.format(new Date());
    	//작업PC IP 3자리(. 제외)
    	String ip = (String)map.get("WORK_PC_IP");
    	ip = ip.replace(".", "");
    	ip = ip.substring(ip.length()-3);
    	
    	return dataType+reqDate+ip+getRandomString(8);
	}
	private IResponse serviceUpload(IRequest request) 
	{	
        Map map = request.getMap();
        log.info("--------serviceUpload--###-------------\n-------------------------1");
        textReplace(map);	//파라미터 특수문자 처리
        log.info("--------serviceUpload--###-------------\n" + map.toString());
        
         // Creation of Json Response 
 	   	 JSONObject obj = new JSONObject();
        
        try {  
        	//docId 값 생성
        	String docId;
        	String logtype = request.getStringParam("logtype");
        	
        	if(logtype.equals("PrintText")) {
        		docId = (String)map.get("docId"); 
        	} else {
        		docId = createDocId(map);
        	}
        	map.put("docId", docId);
        	
        	map.put("dbName", pm.getString("component.sql.database"));
        
	        tx.begin(); 
	        if(logtype.equals("Copy")) {
	        	createObject("mg.log.insertFileIO", map);
	        } else if(logtype.equals("Print")) {
	        	createObject("mg.log.insertFilePrn", map);
	        } else if(logtype.equals("Attach")) {
	        	createObject("mg.log.insertFileAtth", map);
	        } else if(logtype.equals("Clip")) {
	        	createObject("mg.log.insertClipLog", map);	        	
	        } else if(logtype.equals("PrintText")) {	//PRN_CONTEXT 업데이트
	        	updateObject("mg.log.updatePrnContext", map);
	        }
	        tx.commit();
	        // 성공시 Response
   	        obj.put("errcode", "0");
  			obj.put("errmsg", "serviceUpload success.");
  			obj.put("UPLOAD_UUID", docId);
       	} catch (Exception e) {
            tx.rollback();
            obj.put("errcode", "-1");
  			obj.put("errmsg", "Fail ClipCopy Upload \n" + e.getMessage());
  			log.error("-----------------------------------------------------------------------------\n" + e.getMessage());
       }
        return write(obj.toString());
    }  
		
}
