package mg.log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.base.ComponentRegistry;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class LogAction extends BaseAction {

	/**
	 * Log Manager
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public IResponse run(IRequest request) throws Exception {

		/*if (request.isMode("selectLogServerList")) { // 로그서버 리스트 조회
			return selectLogServerList(request);
		} else if (request.isMode("selectLogServerList2")) { // 로그서버 리스트 조회
			return selectLogServerList2(request);
		} else if (request.isMode("saveLogServer")) { // 로그서버 저장
			return saveLogServer(request);
		} else if (request.isMode("deleteLogServer")) { // 로그서버 삭제
			return deleteLogServer(request);
		} else if(request.isMode("selectSvrUserList")) {	//로그수집정보 사업장별 사용자 리스트 조회
			return selectSvrUserList(request);
		} else if(request.isMode("saveLogUser")) {	//로그수집정보 사용자 정보 저장
			return saveLogUser(request);
		} else if (request.isMode("selectConfigList")) { // Config 리스트 조회
			return selectConfigList(request);
		} else if (request.isMode("saveConfig")) { // Config 저장
			return saveConfig(request);
		} else if (request.isMode("deleteConfig")) { // Config 삭제
			return deleteConfig(request);
		} else */if (request.isMode("selectLogList")) { // 로그 목록 조회
			return selectLogList(request);
		}/* else if(request.isMode("docShiftLogin")) { //문서이관로그인
			return docShiftLogin(request);
		} else if(request.isMode("selectClipContent")) { //클립로그 컨텐츠 조회
			return selectClipContent(request);
		} else if(request.isMode("selectDeptCd")) { //사용자 부서코드 조회
			return selectDeptCd(request);
		} else if(request.isMode("selectSvrCheck")) { //사업장 코드 유효성 검증
			return selectSvrCheck(request);
		} else if(request.isMode("excelLogList")) { //로그 목록 Excel 다운로드
			return excelLogList(request);
		} else if(request.isMode("selectPrnContext")) { //프린트로그 컨텐츠 조회
			return selectPrnContext(request);
		} */else {
			JSONObject retobj = new JSONObject();
			retobj.put("errcode", "-1");
			retobj.put("errmsg", "not found mod value.");
			return write(retobj.toString());
		}
	}

	/**
	 * 로그서버 리스트 조회
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectLogServerList(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		try {
			List lists;
			lists = getList("mg.log.selectLogServerList", request.getMap());

			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j],
							this.getStringWithNullCheck(map, cols[j]));
				}
			}

			return writeXml(responseSelData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseSelData(
					mm.getMessage("COMG_1001", request.getUser().getLocale()),
					"false", "doQuery", gdRes.getGridXmlDatas()));
		}
	}
	
	/**
	 * 로그서버 리스트 조회 (사용자 수집정보관리 트리메뉴 조회용)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectLogServerList2(IRequest request) throws Exception {
		
		Map map = null;
		JSONArray jArray = new JSONArray();
		
		try {
			List lists;
			lists = getList("mg.log.selectLogServerList", request.getMap());

			for (int i = 0; i < lists.size(); i++) {
				JSONObject jObj = new JSONObject();
				map = (Map) lists.get(i);
				jObj.put("SVR_ID", map.get("SVR_ID"));
				jObj.put("SVR_NM", map.get("SVR_NM"));
				jArray.add(jObj);
			}
			StringBuffer buffer = new StringBuffer();
			buffer.append(jArray);
			return writeXml(buffer.toString());
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return write(mm.getMessage("COMG_1001", request.getUser().getLocale()));
		}
	}

	/**
	 * 로그서버 저장
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse saveLogServer(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");

		try {
			Map item = null;
			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();

				for (int j = 0; j < cols.length; j++) {
					String tmps = request
							.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				item.put("LOGIN_ID", request.getUser().getId());
				if (Integer.parseInt(getItem("mg.log.countLogServer", item).toString()) > 0) {
					updateObject("mg.log.updateLogServer", item);
				} else {
					createObject("mg.log.insertLogServer", item);
				}
			}
			return writeXml(responseTranData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doSave", "doSaveEnd"));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseTranData(
					mm.getMessage("COMG_1007", request.getUser().getLocale()),
					"false", "doSave", "doSaveEnd"));
		}
	}
	
	/**
     * 로그서버 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteLogServer(IRequest request) throws Exception 
    { 
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.log.deleteLogServer", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
           log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    
    /**
	 * 로그수집정보 사업장별 사용자 리스트 조회
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectSvrUserList(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		try {
			List lists;
			lists = getList("mg.log.selectSvrUserList", request.getMap());

			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j],
							this.getStringWithNullCheck(map, cols[j]));
				}
			}

			return writeXml(responseSelData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseSelData(
					mm.getMessage("COMG_1001", request.getUser().getLocale()),
					"false", "doQuery", gdRes.getGridXmlDatas()));
		}
	}
	
	/**
	 * 로그수집정보 사용자 정보 저장
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse saveLogUser(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");

		try {
			Map item = null;
			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();

				for (int j = 0; j < cols.length; j++) {
					String tmps = request
							.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				item.put("LOGIN_ID", request.getUser().getId());
				
				updateObject("mg.log.updateLogUser", item);
				//SYSUSER 테이블 값이 추가될 경우 LO_USER_INFO 정보는 자동으로 등록된다. 
				/*if (Integer.parseInt(getItem("mg.log.countLogServer", item).toString()) > 0) {
					updateObject("mg.log.updateLogServer", item);
				} else {
					createObject("mg.log.insertLogServer", item);
				}*/
			}
			return writeXml(responseTranData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doSave", "doSaveEnd"));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseTranData(
					mm.getMessage("COMG_1007", request.getUser().getLocale()),
					"false", "doSave", "doSaveEnd"));
		}
	}
	
	/**
	 * Config 리스트 조회
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectConfigList(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		try {
			List lists;
			lists = getList("mg.log.selectConfigList", request.getMap());
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j],
							this.getStringWithNullCheck(map, cols[j]));
				}
			}

			return writeXml(responseSelData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseSelData(
					mm.getMessage("COMG_1001", request.getUser().getLocale()),
					"false", "doQuery", gdRes.getGridXmlDatas()));
		}
	}

	/**
	 * Config 저장
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse saveConfig(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");

		try {
			Map item = null;
			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();

				for (int j = 0; j < cols.length; j++) {
					String tmps = request
							.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				item.put("LOGIN_ID", request.getUser().getId());
				if (Integer.parseInt(getItem("mg.log.countConfig", item).toString()) > 0) {
					updateObject("mg.log.updateConfig", item);
				} else {
					createObject("mg.log.insertConfig", item);
				}
			}
			return writeXml(responseTranData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doSave", "doSaveEnd"));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseTranData(
					mm.getMessage("COMG_1007", request.getUser().getLocale()),
					"false", "doSave", "doSaveEnd"));
		}
	}
	
	/**
     * Config 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteConfig(IRequest request) throws Exception 
    { 
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.log.deleteConfig", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
           log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    
    /**
	 * 로그 목록 조회
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectLogList(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		try {
			List lists;
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			lists = getList("mg.log.selectLogList", smap);
			
			log.info("----------------------------------------------------------------selectLogList");
			
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}

			return writeXml(responseSelData(
					mm.getMessage("COMG_1002", request.getUser().getLocale()),
					"true", "doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n"
					+ e.toString());
			return writeXml(responseSelData(
					mm.getMessage("COMG_1001", request.getUser().getLocale()),
					"false", "doQuery", gdRes.getGridXmlDatas()));
		}
	}
	
	
	/**
     * 문서이관 로그인
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse docShiftLogin(IRequest request) throws Exception 
     {
	   	 JSONObject obj = new JSONObject();
	   	 
    	 try {
   		String dbName = pm.getString("component.sql.database");
       	Map map = (Map)getItem("app.user.getUserByProperties" + getDBName(dbName), request.getStringParam("user_id").toString());
   		
       	log.info("docShiftLogin-------------------------------------------------------\n"+map);
       	
       	if (map == null || map.size() == 0) {
       		 obj.put("user_id", request.getStringParam("user_id"));
    	   		 obj.put("errcode", "-1");
    			 obj.put("errmsg", "not found user !!");
       	 } else {
       		 obj.put("user_name", map.get("NAME"));
       		 obj.put("user_id", request.getStringParam("user_id"));
       		 obj.put("authCode", null);
    	   		 obj.put("errcode", "0");
    			 obj.put("errmsg", "Success.");
       	 }
	   		 return write(obj.toString());
	   		
        } catch(Exception e) {
      	 	log.error("---getApprover Exception Error -------------------------\n" + e.toString());
      	 	obj.put("errcode", "-1");
			obj.put("errmsg", e.toString());
      	 	return write(obj.toString());
        }
     }
     
     
     /**
 	 * 클립로그 컨텐츠 조회
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse selectClipContent(IRequest request) throws Exception {
 		Map map = new HashMap();
 		JSONObject obj = new JSONObject();

 		try {
			Map item = (Map) getItem("mg.log.selectClipContent", request.getMap());
			obj.put("SCRT_DOC_NO", item.get("SCRT_DOC_NO"));
			obj.put("USER_ID", item.get("USER_ID"));
			obj.put("USER_NM", item.get("USER_NM"));
			obj.put("CONTENT", item.get("CONTENT"));
			
			return write(obj.toJSONString());
		} catch (Exception e) {
			log.error("---getClipContent Exception Error -------------------------\n" + e.toString());
      	 	obj.put("errcode", "-1");
			obj.put("errmsg", e.toString());
      	 	return write(obj.toString());
		}
 	}
 	
 	/**
 	 * 프린트로그 컨텐츠 조회
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse selectPrnContext(IRequest request) throws Exception {
 		Map map = new HashMap();
 		JSONObject obj = new JSONObject();

 		try {
			Map item = (Map) getItem("mg.log.selectPrnContext", request.getMap());
			obj.put("SCRT_DOC_NO", item.get("SCRT_DOC_NO"));
			obj.put("USER_ID", item.get("USER_ID"));
			obj.put("USER_NM", item.get("USER_NM"));
			obj.put("CONTENT", item.get("CONTENT"));
			
			return write(obj.toJSONString());
		} catch (Exception e) {
			log.error("---getPrnContext Exception Error -------------------------\n" + e.toString());
      	 	obj.put("errcode", "-1");
			obj.put("errmsg", e.toString());
      	 	return write(obj.toString());
		}
 	}
 	
    /**
	 * 사용자 부서코드 조회
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectDeptCd(IRequest request) throws Exception {
		Map map = new HashMap();
		JSONObject obj = new JSONObject();
		
		try {
			Map item = (Map) getItem("mg.log.selectDeptCd", request.getMap());
			obj.put("DEPT_CD", item.get("DEPT_CD"));
			obj.put("P_DEPT_CD", item.get("P_DEPT_CD"));
			
			return write(obj.toJSONString());
		} catch (Exception e) {
			log.error("---selectDeptCd Exception Error -------------------------\n" + e.toString());
      	 	obj.put("errcode", "-1");
			obj.put("errmsg", e.toString());
      	 	return write(obj.toString());
		}
	}
	
	/**
	 * 사업장 코드 유효성 검증
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectSvrCheck(IRequest request) throws Exception {
		Map map = new HashMap();
		JSONObject obj = new JSONObject();
		
		try {
			Map item = (Map) getItem("mg.log.selectSvrCheck", request.getMap());
			obj.put("SVR_ID", item.get("SVR_ID"));
			obj.put("SVR_NM", item.get("SVR_NM"));
			
			return write(obj.toJSONString());
		} catch (Exception e) {
			log.error("---selectDeptCd Exception Error -------------------------\n" + e.toString());
      	 	obj.put("errcode", "-1");
			obj.put("errmsg", e.toString());
      	 	return write(obj.toString());
		}
	}
	
	/**
	 * 로그 목록 ExcelDownload
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse excelLogList(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		String width = request.getStringParam("grid_col_width");
	  	String[] widths = width.split(",");
	  	String header = request.getStringParam("grid_col_header");
	  	String[] headers = header.split(",");
	  	String hidden = request.getStringParam("grid_col_hidden");
	  	String[] hiddens = hidden.split(",");
	  	String type = request.getStringParam("grid_col_type");
	  	String[] types = type.split(",");
	  	String comboId = request.getStringParam("grid_col_combo_id");
	  	String[] comboIds = comboId.split(",");
	  	
	  	try {
		     // Excel Header 설정
		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
             IExcelDocument doc = excel.createDocument("LogSearch");
             int[] col_width;
       		 List<String> col_width_list = new ArrayList<String>();
       		 List<String> col_id_list = new ArrayList<String>();
       		 String colStr = "";
       		 String locale = request.getUser().getLocale().getLanguage();
       		 Map comboMap = new HashMap(); 
       		 
             for(int i=0; i< cols.length; i++) {
            	 if("N".equals(hiddens[i])) {
            		 if("FILE_ICON".equals(cols[i]) || "PRN_CONTEXT".equals(cols[i])) {
            			 continue;
            		 }
            		 doc.addTitleCell(headers[i]);
            		 col_width_list.add(widths[i]);
            		 col_id_list.add(cols[i]);

            		 if("coro".equals(types[i]) && !"".equals(comboIds[i]) && comboIds[i] != null) {
            			 Map combo = new HashMap();
            			 String p_cd = comboIds[i];
            			 String col_id = cols[i];
            			 
            			 ICodeManagement sysCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
            			 List sCode = sysCode.getCodes(p_cd);
            			 
            			 for(Iterator all = sCode.iterator(); all.hasNext();) {
            				 ICode icd = (ICode) all.next();
            				 if("en".equals(locale)) {
            					 combo.put(icd.getCd(), icd.getNameSl());
            				 } else {
            					 combo.put(icd.getCd(), icd.getName());
            				 }
            			 }
            			 comboMap.put(col_id, combo);
            		 }
            		 if("USER_ID".equals(cols[i])) {
            			 colStr += "REPLACE(USER_ID, '<![CDATA[<]]>VDI<![CDATA[>]]>', '') AS USER_ID,";
            		 } else {
            			 colStr += cols[i]+",";            			             			 
            		 }
            	 }/* else {
            		 log.info("hidden id :: " + cols[i]);
            	 }*/
             }
             if(colStr.length() > 0) {
            	 colStr = colStr.substring(0, colStr.length()-1);
             }
             col_width = new int[col_width_list.size()];
             for(int i=0; i<col_width_list.size(); i++) {
            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
             }
             doc.setColumnsWidth(col_width);
     
 	         Map smap = request.getMap();
 	         smap.put("colStr", colStr);
 	         smap.put("LOCALE", locale);
 	         
 	         List lists = getList("mg.log.selectExcelLogList",smap);
 	         //log.info("colStr : " + colStr);
 	         
 	         for (int i=0; i < lists.size(); i++) {
 	         	Map map =(Map) lists.get(i);
 	         	doc.nextRow();
 	         	
 	         	for(int j=0; j < col_id_list.size(); j++) {
 	         		if(comboMap.get(col_id_list.get(j)) != null) {
 	         			String colId = col_id_list.get(j);
 	         			Map combo = (Map)comboMap.get(colId);
 	         			String comboText = (String)combo.get(this.getStringWithNullCheck(map,colId));
 	         			doc.addAlignedCell(comboText, CellStyleFactory.ALIGN_LEFT);
 	         		} else {
 	         			doc.addAlignedCell(this.getStringWithNullCheck(map,col_id_list.get(j)), CellStyleFactory.ALIGN_LEFT); 	         			 	         			
 	         		}
 	         	}
 	         }
 	         return doc.download(request, "LogSearch_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
      	 	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
      	     return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
	}
}
