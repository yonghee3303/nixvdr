package mg.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.SystemException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mg.base.GridData;
import mg.base.BaseAction;
import mg.vf.AgentAction;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.base.ComponentRegistry;
import com.core.component.util.BaseUtils;
import com.core.component.sql.ISqlManagement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CpApproverResultListener  implements Job {

	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	
	private String getStringWithNullCheck(Map map, String key) 
	 {
	        Object result = map.get(key);
	        return (result == null) ? null : result.toString();
	  }
	
	private String validFolder(String folder)
	{//들어온 폴더 확인
		String filePath = pm.getString("component.upload.resultdirectory").replace("/", "\\");
		File resultFolder = new File(filePath+"\\"+folder);
		String fileList[] = resultFolder.list();
		//log.info("2----fileList.length: "+fileList.length);
		//그 폴더 내에 파일 중 EOF 파일이 있는지 확인
		if(fileList.length>0){
			for(int i=0; i<fileList.length; i++){
				//log.info("fileList[i]--------"+fileList[i]);
				String temp[] = fileList[i].split("\\.");
				for(int j=0; j<temp.length; j++){
					//log.info("temp[j]--------"+temp[j]);
					if("eof".equals(temp[j])){
						//log.info("4----");
						return "1";
					}
				}
			}
		}
		//log.info("5----");
		return "0";
	}
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		String filePath = pm.getString("component.upload.resultdirectory").replace("/", "\\");
		String tempPath = pm.getString("component.upload.tempdirectory").replace("/","\\");
		String dbName = pm.getString("component.sql.database");
		log.info(filePath+": filePath \t "+tempPath+": tempPath");
		Map map = new HashMap();
		log.info("결재 연동 배치 Stratr!!!!!");
		String docNo="";
		//log.info("1----");
		File resultFolders = new File(filePath);
		String FolderList[] = resultFolders.list();
		//if(resultFolders.isDirectory()){
		if(FolderList.length==0){
			log.info("결재연동 배치 -------------------------------------읽어올 폴더가 없습니다.");
			return;
		}
		
		for(int j=0; j<FolderList.length; j++){
			
			if("0".equals(validFolder(FolderList[j]))){
				log.info("결재 연동 배치 ------------------------------------------다운로드가 완료되지 않았습니다.");
				return;
			}
			//log.info("3----");
			//폴더이동
			File resultFiles = new File(filePath+"\\"+FolderList[j]);
			File tempFiles = new File(tempPath+"\\"+FolderList[j]);
			if(resultFiles.exists()){
				resultFiles.renameTo(tempFiles);
			}
			String fileList[]= tempFiles.list();
			
		for(int i=0; i<fileList.length; i++){
/*			//원본파일 읽어오기
			File resultFile = new File(filePath+"\\"+fileList[i]);
			
			//읽은 파일 이동
			File tempFile = new File(tempPath+"\\"+fileList[i]);
			if(resultFile.exists()){
				resultFile.renameTo(tempFile);
			}*/
			File tempFile = new File(tempPath+"\\"+FolderList[j]+"\\"+fileList[i]);
			//log.info("결재 연동 배치 : 결재 결과 파일 읽어오는 중----------------------------"+fileList[i]);
			//확장자 검사
			boolean isXml =false;
			String temp[] = fileList[i].split("\\.");
			for(int k=0; k<temp.length; k++){
				//log.info("temp[k]--------"+temp[k]);
				if("xml".equals(temp[k])){
					isXml =true;
				}
			}
			if(isXml){
				try{
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(tempFile); //xml문서 자체를 표현하는 최상의 노드
				Element root = doc.getDocumentElement();
				int lastIndex;
				NodeList tempNode = root.getElementsByTagName("messageCode");
				docNo = tempNode.item(0).getFirstChild().getNodeValue();
				map.put("DOC_NO", docNo);
				//문서번호로 쿼리를 해서 보안반출인지, 원문반출인지 확인, 승인처리를 해야될지 말지도....
				Map authoMap ;
				//log.info("dbName--------------------"+dbName);
				if(dbName.equals("mssql")){
					authoMap = (Map)sm.getItem("mg.cp.getCpDocInfo",map);
				}else{
					authoMap = (Map)sm.getItem("mg.cp.getCpDocInfo_"+dbName, map);
				}
				String authority = this.getStringWithNullCheck(authoMap,"AUTHORITY");
				
				//log.info("결재 연동 배치 :-----------------------------------------authority : "+ authority);
				tempNode = root.getElementsByTagName("title");
				String title = tempNode.item(0).getFirstChild().getNodeValue();

				tempNode = root.getElementsByTagName("userId");//승인자ID
				lastIndex = tempNode.getLength()-1;
				String senderId=tempNode.item(1).getFirstChild().getNodeValue();
				String approvalId = tempNode.item(lastIndex).getFirstChild().getNodeValue();
				
				tempNode = root.getElementsByTagName("actType");
				lastIndex = tempNode.getLength()-1;
				String actType = tempNode.item(lastIndex).getFirstChild().getNodeValue();
				log.info("결재 연동 배치 :  \n---------DocNo---- : "+docNo+"\n--------approvalId--- : "+approvalId+"\n--------senderId--- : "+senderId
						+"\n--------actType(APT001 : 승인 , APT002 :  반려)--- : "+actType);
				if(map.containsKey("CP_STS_CD")) map.remove("CP_STS_CD"); // 여러건 처리 시 문제 발생.
				
				if(senderId.equals(approvalId)) actType="APT001";
				String approvalYn="";
				if("APT001".equals(actType)){ //승인
					approvalYn="Y";
					if("6".equals(authority)) { 
						//원문반출일 경우 04로 변경,
		   	 		    map.put("CP_STS_CD", "04");  // 승인시 상태 코드를 04 : 완료 상태로 만든다.
		   	 		    //log.info("승인일 경우--------------------------------------------------------------CP_STS_CD=04 \t원문반출"+authority);
			 		}else{
			 			//보안반출일 경우 02로 변경
						map.put("CP_STS_CD", "02");
						//log.info("승인일 경우--------------------------------------------------------------CP_STS_CD=02 \t보안반출"+authority);
			 		}
				}else if("APT002".equals(actType)){ //반려
					approvalYn="N";
					map.put("CP_STS_CD", "07");
					//log.info("반려일 경우--------------------------------------------------------------CP_STS_CD=07");
				} else{  //EP 오류로 종종 APT003 코드를 보내 올 경우, 오류 코드로 변경
					approvalYn="N";
					map.put("CP_STS_CD", "05");
					log.info("APT001,APT002 외의 결과("+actType+") 일 경우 ----------------------------------CP_STS_CD=05");
				}
				
				/**/
				tempNode = root.getElementsByTagName("opinion");
				lastIndex = tempNode.getLength()-1;
				if(tempNode.item(lastIndex).getFirstChild()!=null){
				String approvalMsg =  tempNode.item(lastIndex).getFirstChild().getNodeValue();
				//log.info("결재 연동 배치 :  ----결재메세지----------- : "+approvalMsg);
				map.put("APPROVAL_MSG",approvalMsg);
				}else{
					map.put("APPROVAL_MSG","");
				}
				
				// 문서상태 변경
				sm.updateObject("mg.cp.updateSTS", map); 
	   	 	
	   	 		// 승인자 등록 --> 현재 승인한 사용자가 승인자 임.
	   	 		map.put("APPROVER_ID", approvalId); 
	   	 		sm.updateObject("mg.cp.updateDocApprover", map);
			
	   	 		if("Y".equals(pm.getString("cliptorplus.vf.export"))) {
	   	 			// 반출함 사용시 
	   	 			map.put("isApproval", approvalYn);
	   	 			map.put("USER_ID", approvalId);
	   	 			map.put("apvReason",  this.getStringWithNullCheck(map, "APPROVAL_MSG"));
	   	 			AgentAction agentAction = new AgentAction();
	   	 			agentAction.setExportStat(map);
	   	 		}
	   	 		log.info("결재 연동 배치 ---------------- 정상적으로 완료");
			}catch(Exception e){
				log.info("결재 연동 배치 오류 발생 : \n 문서번호 :"+docNo+"\n----결재완료 파일 :"+fileList[i]+" 읽어오는 중 오류-------\n"+e.toString());
				File errorFiles = new File(tempPath+"\\error\\"+FolderList[j]);
				//읽은 파일 이동
				if(tempFiles.exists()){
					tempFiles.renameTo(errorFiles);
				}
				//log.info("결재 연동 배치 오류난 결재완료 파일 "+fileList[i]+"---------"+tempPath+"\\error 폴더 로 이동");
			}//try - catch
			}//if(isXml)
		}
	} //for i
	}//for j
}
