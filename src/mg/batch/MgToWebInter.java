package mg.batch;

import java.util.HashSet;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javassist.NotFoundException;

import javax.servlet.http.HttpSession;
import javax.transaction.SystemException;
import javax.xml.rpc.ServiceException;

import mg.base.GridData;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.LGC.esb.Profile.DeptInfoListVO;
import com.LGC.esb.Profile.EmployeeProfileListVO;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.base.ComponentRegistry;
import com.core.component.util.BaseUtils;
import com.core.component.sql.ISqlManagement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* 업무 그룹명 : mg.batch
* 서브 업무명 : MgHRInterlock.java
* 작성자 : whchoi
* 작성일 : 2016. 05. 16 
* 설 명 : MgWeb / LogManager HR 연동
* 최종 수정일 : 2016. 05. 16
* 수정 내용 : CP,LM내 HR 연동 파일 병합. 
* 참조 사항 : MG -> MGWeb 연동시 직책정보 연동(component.hr.functionCd.mapping)을 사용할 경우
* 	1) TB_USERCODE 테이블 FUNCTIONCD 컬럼 추가
* 	2) MG Console HR 연동시 프로시저내 FUCNTIONCD 값 추가
*	3) 사이트별 P_DEPT_CD 입력값 변경
*
* @수정시 주의사항1 : CP와 LM에 동일한 소스가 존재한다. 수정시 양쪽 모두 수정할 것
* @수정시 주의사항2 : 사이트 추가시 "사이트명".equals(pm.getString("component.site.company") 으로 활용해 추가할 것 
* @반영시 주의사항 : 소스<->DB간 P_DEPT_CD 값이 일치하는지 확인 (LGU+, ILJIN 필수 확인)
* @적용 사이트 : LGCHEM, LGU, ILJIN
*/ 

public class MgToWebInter implements Job {

	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);

	public MgToWebInter() {
	
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.error("----Batch---MgToWebInter------Start--- ");
		
		///////////////////// MG -> MGWEB 동기화 /////////////////////
		if("Y".equals(pm.getString("component.hr.mgToWeb.useYn"))) {
			//부서정보 연동
			if(mgDeptInfoList()) {
				log.error("-----------Success-----------mgDeptInfoList---------");
			} else {
				log.error("-----------Failed-----------mgDeptInfoList---------");
			}
			//사원정보 연동
			if(mgEmployeeProfileInfoList()) {
				log.error("-----------Success-----------mgEmployeeProfileListNew---------");
			} else {
				log.error("-----------Failed-----------mgEmployeeProfileListNew---------");
			}
		}
		
		if("LogManager".equals(pm.getString("component.site.product"))) {
			///////////////////// LogManager(MGWEB -> LM) 동기화 /////////////////////
			// 사업장정보 연동
			if(lmSvrInfoList()) {
				log.error("-----------Success-----------lmSvrInfoList---------");
			} else {
				log.error("-----------Failed-----------lmSvrInfoList---------");
			}
			//사업장별  LM 사원 정보 연동
			if(lmUserInfoList()) {
				log.error("-----------Success-----------lmUserInfoList---------");
			} else {
				log.error("-----------Failed-----------lmUserInfoList---------");
			}
		}                             
	}
	
	// 부서정보 연동 (MG -> MGWEB)
	public Boolean mgDeptInfoList()
	{
		try {
			log.error("MgToWebInter mgDeptInfoList Start");
			Map map = new HashMap();
			map.put("dbName", pm.getString("component.sql.database"));
			
			//TB_DPTCODE 부서리스트 호출
			List deptList = sm.getList("mg.batch.getMgDeptList", map);
			log.error("deptInfoList : " + deptList.size());

			//최상위 부서코드(P_DEPT_CD = -1) 존재여부 확인
			if( Integer.parseInt(sm.getItem("mg.batch.countRootDept", null).toString() ) == 0 ) {
				log.info("rootDept 추가");
				Map smap = new HashMap();
				smap.put("P_DEPT_CD", "-1");
				smap.put("CHILD_YN", "Y");
				smap.put("USE_YN", "Y");
				smap.put("LOGIN_ID", "admin");
				smap.put("DEPT_CD", pm.getString("component.site.company"));
				smap.put("DEPT_NM", pm.getString("component.site.company"));
				
				sm.createObject("mg.batch.insertRootDept", smap);
			}
			
			// 결과값 조회 
			for(int i=0; i<deptList.size(); i++) {
				
				// 리턴받은 부서정보 -> DB update 트랜잭션 시작
				
				Map deptMap = (Map) deptList.get(i);
				
		    	//deptMap.put("P_DEPT_NM", pDeptNm);
		    	deptMap.put("LOGIN_ID", "admin");
		    	deptMap.put("USE_YN", "Y");
		    	//deptMap.put("DEPT_ENM", "");
		    	
		    	if(String.valueOf(deptMap.get("TREE_LEN")).equals("2")) {	//최상위 코드 ROOT 코드와 연결
		    		deptMap.put("P_DEPT_CD", pm.getString("component.site.company"));
		    	}
		    	
		    	if(sm.getItem("mg.batch.getDept",deptMap) != null){
		    		log.info("------------------------------update MDEPT : " + deptMap.get("DEPT_CD") + " / " + deptMap.get("DEPT_NM"));
		    		sm.updateObject("mg.batch.updateDept", deptMap);
		    	} else{
		    		log.info("------------------------------insert MDEPT : " + deptMap.get("DEPT_CD") + " / " + deptMap.get("DEPT_NM"));
		    		sm.createObject("mg.batch.insertDept", deptMap);
		    	}		    		
			}
			
			//부서 코드가 없는 사용자, 혹은 TB_DPTCODE 트리코드 미연결로 등록되지 않은 부서의 사용자가 들어갈 부서 삽입
			int dptCnt = Integer.parseInt(sm.getItem("mg.batch.getUnknownDeptCnt", null).toString());
			if(dptCnt == 0 ) {
				Map deptMap = new HashMap();
				deptMap.put("LOGIN_ID", "admin");
				deptMap.put("P_DEPT_CD", pm.getString("component.site.company"));
				sm.createObject("mg.batch.insertUnknownDept", deptMap);
			}
			int deleteCnt = sm.deleteObject("mg.batch.checkDeptDel", null);	//형식이 맞지 않는 부서 삭제
			log.error("------------------------------ MDEPT CHECK FORM, DELETE  -------------------------------- : " + deleteCnt);
			int updateCnt = sm.updateObject("mg.batch.updateDeptUseYn", null);	//TB_DPTCODE내 삭제된 부서 USE_YN = 'N' 변경
			log.error("------------------------------ MDEPT CHECK COMPARE TB_DPTCODE, UPDATE -------------------------------- : " + updateCnt);
			
			log.error("MgHRInterlock mgDeptInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
			
	}
	
	// 사원정보 연동  (MG -> MGWEB)
	public Boolean mgEmployeeProfileInfoList(){
		
		try {
			
			log.error("MgToWebInter mgEmployeeProfileInfoList Start");
			
			String functionPCd = "A0201";
			String positionPCd = "A0204";
			Map functionMap = null;
			Map positionMap = null;
			
			//직책 정보 코드 매핑 (직책, 직급 값이 코드정보로 매핑되어 있지 않을 경우 사용)
			if("Y".equals(pm.getString("component.hr.functionCd.mapping"))) {
				int cnt = Integer.parseInt(sm.getItem("mg.batch.getCodeCnt", functionPCd).toString());
				//미등록 직책 정보
				List functionList = sm.getList("mg.batch.getNonRegiFunctionList");
				for(int i=0; i<functionList.size(); i++) {
					Map tmpMap = (Map)functionList.get(i);	//CD_NM
					
					//null값 무시
					if(tmpMap.get("CD_NM") == null) {
						log.info("function CD_NM null");
						continue;
					}
					
					tmpMap.put("P_CD", functionPCd);
					tmpMap.put("LOGIN_ID", "admin");
					tmpMap.put("CNT", ++cnt);
					tmpMap.put("dbName", pm.getString("component.sql.database"));
					sm.createObject("mg.batch.insertHrCode", tmpMap);					
				}
				//P_CD(A0201)로 등록된 코드 리스트 
				List functionMappingList = sm.getList("mg.batch.getCdNmMappingList", functionPCd);
				functionMap = new HashMap<String, String>();
				
				for(int i=0; i<functionMappingList.size(); i++) {
					Map tmpMap = (Map)functionMappingList.get(i);
					functionMap.put(tmpMap.get("CD_NM"), tmpMap.get("CD_KEY"));
				}
			}
			//직급(직위) 정보 코드 매핑
			if("Y".equals(pm.getString("component.hr.positionCd.mapping"))) {
				int cnt = Integer.parseInt(sm.getItem("mg.batch.getCodeCnt", positionPCd).toString());
				//직위&직책 모두 없을 경우에 대비한 직위 등록 
				if(cnt == 0) {
					Map tmpMap = new HashMap();
					tmpMap.put("P_CD", positionPCd);
					tmpMap.put("LOGIN_ID", "admin");
					tmpMap.put("CNT", ++cnt);
					tmpMap.put("CD_NM", "No Position");
					tmpMap.put("dbName", pm.getString("component.sql.database"));
					sm.createObject("mg.batch.insertHrCode", tmpMap);
				}
				//미등록 직급(직위) 정보
				List positionList = sm.getList("mg.batch.getNonRegiPositionList");
				for(int i=0; i<positionList.size(); i++) {
					Map tmpMap = (Map)positionList.get(i);
					
					//null값 무시
					if(tmpMap.get("CD_NM") == null) {
						log.info("position CD_NM null");
						continue;
					}
					
					tmpMap.put("P_CD", positionPCd);
					tmpMap.put("LOGIN_ID", "admin");
					tmpMap.put("CNT", ++cnt);
					tmpMap.put("dbName", pm.getString("component.sql.database"));
					sm.createObject("mg.batch.insertHrCode", tmpMap);					
				}
				//P_CD(A0204)로 등록된 코드 리스트 
				List positionMappingList = sm.getList("mg.batch.getCdNmMappingList", positionPCd);
				positionMap = new HashMap<String, String>();
				
				for(int i=0; i<positionMappingList.size(); i++) {
					Map tmpMap = (Map)positionMappingList.get(i);
					positionMap.put(tmpMap.get("CD_NM"), tmpMap.get("CD_KEY"));
				}
			}
			
			//TB_USERCODE 리스트
			List userList = sm.getList("mg.batch.getMgUserList");
			log.info("mgEmployeeProfileInfoList : " + userList.size());
			
			//결과값 조회		
			for(int i=0;i<userList.size();i++){
			
				Map userMap = (Map)userList.get(i);
				/*userMap.put("USER_ENM","");
				userMap.put("EMP_NO","");*/
				userMap.put("WORK_CD", "C");
				userMap.put("LOGIN_ID","admin");
				userMap.put("dbName", pm.getString("component.sql.database"));
				
				//직책, 직급 정보 : 한글 명칭 -> 코드화
				if("Y".equals(pm.getString("component.hr.functionCd.mapping"))) {
					userMap.put("FUNCTION_CD", functionMap.get(userMap.get("FUNCTION_CD")));
				}
				if("Y".equals(pm.getString("component.hr.positionCd.mapping"))) {
					userMap.put("POSITION_CD", positionMap.get(userMap.get("POSITION_CD")));
				}
				
				if("LGCHEM".equals(pm.getString("component.site.company"))){
					userMap.put("EMAIL",userMap.get("USER_ID")+"@lgchem.com");
				}
				
				//직책&직급 모두 없을 경우 01(No Position) 직위 설정
				if(userMap.get("FUNCTION_CD") == null && userMap.get("POSITION_CD") == null) {
					userMap.put("POSITION_CD", "01");
				}
				//부서내 특정 사용자 등록여부 조회(1: 부서내 해당 사용자 등록 / 0 : 부서내 해당 사용자 없음 / -1 : 해당 부서 없으나 사용자는 존재 / -2 : 해당 부서 및 사용자 없음 )
				int userCnt = Integer.parseInt(sm.getItem("mg.batch.getDeptUserCnt", userMap).toString() );
				if(userCnt > 0 ) {
					log.info("------------------------------update SYSUSER : " + userMap.get("USER_ID") + " / " + userMap.get("USER_NM"));
					sm.updateObject("mg.batch.updateUser", userMap);
				} else if(userCnt == 0) {
					log.info("------------------------------insert SYSUSER : " + userMap.get("USER_ID") + " / " + userMap.get("USER_NM"));
					sm.createObject("mg.batch.insertUser", userMap);
					sm.createObject("mg.batch.insertUserRoll", userMap);
				} else if(userCnt == -1 ) {
					userMap.put("DEPT_CD", "unknown");
					log.info("------------------------------update no Dept SYSUSER : " + userMap.get("USER_ID") + " / " + userMap.get("USER_NM"));
					sm.updateObject("mg.batch.updateUser", userMap);
				} else if(userCnt == -2) {
					userMap.put("DEPT_CD", "unknown");
					log.info("------------------------------insert no Dept SYSUSER : " + userMap.get("USER_ID") + " / " + userMap.get("USER_NM"));
					sm.createObject("mg.batch.insertUser", userMap);
					sm.createObject("mg.batch.insertUserRoll", userMap);
				} else {
					log.info("------------------------------ERROR batch mgEmployeeProfileInfoList, check user dept_cd : USER_ID(" + userMap.get("USER_ID") + ") / USER_NM(" + userMap.get("USER_NM") + ") / DEPT_CD(" + userMap.get("DEPT_CD") + ")");
				}
			}
			//TB_USERCODE내 삭제된 사용자 퇴사처리
			int updateCnt = sm.updateObject("mg.batch.updateUserWorkCd", null);
			log.error("------------------------------ SYSUSER CHECK COMPARE TB_USERCODE, UPDATE -------------------------------- : " + updateCnt);
			
			log.error("MgHRInterlock mgEmployeeProfileInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
	}
	
	//LM 사업장정보 연동
	public Boolean lmSvrInfoList()
	{
		try {
			log.error("MgToWebInter lmSvrInfoList Start");
			
			//서비스 호출
			List deptList = null;
			
			//고객사에서 제공하는 사업장 정보 사용 
			if("SVR".equals(pm.getString("logmanager.svr.tree"))){
				if("LGCHEM".equals(pm.getString("component.site.company"))){
					deptList = sm.getList("mg.batch.getLgSvrList");
				} else {
					throw new Exception("MgToWebInter lmSvrInfoList Mode(SVR) Error : Unregistered parameter Code[component.site.company]");
				}
				log.error("deptInfoList : " + deptList.size());
		
				// 결과값 조회
				for(int i=0; i<deptList.size(); i++) {
					
					Map deptMap = (Map) deptList.get(i);
			    	deptMap.put("LOGIN_ID", "admin");
			    	      
					//사업장 정보 얻어오기
			    	String svrId = (String)deptMap.get("SVR_ID");
					String svrNm = (String)deptMap.get("SVR_NM");
										
					if(svrId == null) {
						deptMap.put("SVR_ID", "unknown");
					} else {
						deptMap.put("SVR_ID", svrId);
					}
					if(svrNm == null) {
						deptMap.put("SVR_NM", "unknown(ID:"+deptMap.get("SVR_ID")+")");
					} else {
						deptMap.put("SVR_NM", svrNm);
					}
					if( Integer.parseInt(sm.getItem("mg.batch.countLmSvr",deptMap).toString() ) > 0 ) {
		    			log.info("------------------------------------update LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
	    				sm.updateObject("mg.batch.updateLmSvr", deptMap);
			    	} else{
			    		log.info("------------------------------------insert LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
			    		setDefaultUrl(deptMap);	//Default URL 삽입
		    			sm.createObject("mg.batch.insertLmSvr", deptMap);
			    	}
				}
			} else {	//부서정보기반 사업장 정보 호출
				//String rootDept = (String)sm.getItem("mg.batch.getRootDept", null);
				String rootDept = pm.getString("component.site.company");
				
				if(pm.getBoolean("logmanager.multi.server")) {
					deptList = sm.getList("mg.batch.getDeptList");
					log.error("deptInfoList : " + deptList.size());
					
					//사업장 정보 중복 방지
					Set svrInfo = new HashSet();
			
					// 결과값 조회
					for(int i=0; i<deptList.size(); i++) {
						
						Map deptMap = (Map) deptList.get(i);
				    	deptMap.put("LOGIN_ID", "admin");
				    	deptMap.put("COMPANY", rootDept);
				    	deptMap.put("dbName", pm.getString("component.sql.database")); 
						//개별 사업장 정보 얻어오기
			    		Map svrMap;
			    		/* if("LGCHEM".equals(pm.getString("component.site.company"))){
			    			svrMap = (Map)sm.getItem("mg.batch.getLgLmSvrInfo",deptMap);	//현재는 SVR 사업장 리스트를 호출하기에 사용하지 않는다.
			    		} else { */
			    			svrMap = (Map)sm.getItem("mg.batch.getLmSvrInfo",deptMap);
			    		//}
			    		
						String svrId;
						String svrNm;
						svrId = (String)svrMap.get("SVR_ID");
						svrNm = (String)svrMap.get("SVR_NM");
						//log.info("-------------------------------------------사업장 정보 얻어오기----"+svrNm);
						if(svrId != null) {
							deptMap.put("SVR_ID", svrId);
							deptMap.put("SVR_NM", svrNm);
							
							if( Integer.parseInt(sm.getItem("mg.batch.countLmSvr",deptMap).toString() ) > 0 ) {
								if(!svrInfo.contains(svrId)) {
					    			log.info("------------------------------------update LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
				    				sm.updateObject("mg.batch.updateLmSvr", deptMap);
				    				svrInfo.add(svrId);
					    		}
					    	} else{
					    		log.info("------------------------------------insert LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
					    		setDefaultUrl(deptMap);	//Default URL 삽입
				    			sm.createObject("mg.batch.insertLmSvr", deptMap);
			    				svrInfo.add(svrId);
					    	}	
						}
					}
				} else {
					//단일 사업장 운영
					Map deptMap = new HashMap();
					deptMap.put("SVR_ID", rootDept);
					
					if(Integer.parseInt(sm.getItem("mg.batch.countLmSvr",deptMap).toString() ) == 0 ) {
						deptMap.put("SVR_NM", pm.getString("component.site.company"));
						deptMap.put("LOGIN_ID", "admin");
						
						log.info("------------------------------------insert LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
			    		setDefaultUrl(deptMap);	//Default URL 삽입
		    			sm.createObject("mg.batch.insertLmSvr", deptMap);
					}
				}
			}
			log.error("MgHRInterlock lmSvrInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
    }
	
	//LM 사용자정보 연동
	public Boolean lmUserInfoList()
	{
		try {
			log.error("MgHRInterlock lmUserInfoList Start");
			      
			//사용자 정보 조회(USER_ID, USER_NM, USER_ENM, SVR_ID)
			List userList = null;
			
			//고객사에서 제공하는 사업장 정보 사용
			if("SVR".equals(pm.getString("logmanager.svr.tree"))){
				if("LGCHEM".equals(pm.getString("component.site.company"))){
					userList = sm.getList("mg.batch.getLgSvrUserList");
				} else {
					throw new Exception("MgToWebInter lmUserInfoList Mode(SVR) Error : Unregistered parameter Code[component.site.company]");
				}
				log.error("userInfoList : " + userList.size());
				
				//결과값 조회		
				for(int i=0;i<userList.size();i++){
			
					Map userMap = (Map)userList.get(i);
					int userCnt = Integer.parseInt(sm.getItem("mg.batch.getLmUserCnt", userMap).toString() );
					userMap.put("LOGIN_ID", "admin");
					
					if(userCnt > 0 ) {
						log.info("------------------------------------update LO_USER_INFO--------- : " + userMap.get("USER_ID"));
						sm.updateObject("mg.batch.updateLmUser", userMap);						
					} else {
						log.info("------------------------------------insert LO_USER_INFO--------- : " + userMap.get("USER_ID"));
						sm.createObject("mg.batch.insertLmUser", userMap);		       			
					}													
				}
			} else {
				String rootDept = (String)sm.getItem("mg.batch.getRootDept", null);
				Map map = new HashMap();
				map.put("dbName", pm.getString("component.sql.database"));
				map.put("COMPANY", rootDept);
				
				/*if("LGCHEM".equals(pm.getString("component.site.company"))){
					userList = sm.getList("mg.batch.getLgUserList");	//현재는 SVR 모드를 사용하기에 사용하지 않는다.
				} else {*/
					userList = sm.getList("mg.batch.getUserList", map);
				//}
				log.error("userInfoList : " + userList.size());
				
				//결과값 조회		
				for(int i=0;i<userList.size();i++){
			
					Map userMap = (Map)userList.get(i);
					userMap.put("LOGIN_ID", "admin");
					userMap.put("dbName", pm.getString("component.sql.database"));
					//단일 사업장 운용시 SVR_ID 강제 부여
					if(!pm.getBoolean("logmanager.multi.server")) {
						userMap.put("SVR_ID", rootDept);
					}
					if(userMap.get("DEPT_CD") != null) {
						int userCnt = Integer.parseInt(sm.getItem("mg.batch.getDeptLmUserCnt", userMap).toString());
						if(userCnt > 0 ) {	//부서내 특정 사용자 로그매니저 등록여부 조회
							log.info("------------------------------------update LO_USER_INFO--------- : " + userMap.get("USER_ID"));
							sm.updateObject("mg.batch.updateLmUser", userMap);						
						} else if(userCnt == 0) {
							log.info("------------------------------------insert LO_USER_INFO--------- : " + userMap.get("USER_ID"));
							sm.createObject("mg.batch.insertLmUser", userMap);		       			
						} else {
							log.info("------------------------------ERROR batch lmUserInfoList, check user dept_cd : USER_ID(" + userMap.get("USER_ID") + ") / USER_NM(" + userMap.get("USER_NM") + ") / DEPT_CD(" + userMap.get("DEPT_CD") + ")");
						}
					}													
				}
			}

			log.error("MgHRInterlock lmUserInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
	}
	
	private void setDefaultUrl(Map deptMap) {//Default URL 삽입
		String baseUrl = pm.getString("component.site.url");
		
		if(!"true".equals(pm.getString("component.contextPath.root"))) {
			baseUrl += pm.getString("component.contextPath.path");
		}
		String logUpUrl = baseUrl + "/Logfile.do?mod=tx_create&{PARAM}";
		String prnUpUrl = baseUrl + "/Logfile.do?mod=tx_create&{PARAM}";
		String logServiceUrl = baseUrl + "/Logfile.do?mod=tx_service&{PARAM}";
	
		deptMap.put("LOG_UP_URL", logUpUrl);
		deptMap.put("PRN_UP_URL", prnUpUrl);
		deptMap.put("LOG_SERVICE_URL", logServiceUrl);
	}
}
