package mg.batch;

import java.util.HashSet;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.transaction.SystemException;
import javax.xml.rpc.ServiceException;

import mg.base.GridData;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.LGC.esb.Profile.DeptInfoListVO;
import com.LGC.esb.Profile.EmployeeProfileListVO;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.base.ComponentRegistry;
import com.core.component.util.BaseUtils;
import com.core.component.sql.ISqlManagement;
import com.lgchem.www.AppIntLGChem.BatchDeptInfoList;
import com.lgchem.www.AppIntLGChem.BatchDeptInfoListServiceLocator;
import com.lgchem.www.AppIntLGChem.BatchEmployeeProfileListNew;
import com.lgchem.www.AppIntLGChem.BatchEmployeeProfileListNewServiceLocator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* 업무 그룹명 : mg.batch
* 서브 업무명 : ViewHRInterlock.java
* 작성자 : mjkim
* 작성일 : 2018. 08. 16 
* 설 명 : 사이트 정보 HR View 테이블을 이용한 HR 연동
* 최종 수정일 : 2018. 08. 16
* 수정 내용 : 
* 참조 사항 : 배치 전에 HR_XXX 라는 이름의 View를 생성해야한다. 생성 스크립트 예시는 ViewHRInterlock.xml 파일 참고. 
*/ 

public class ViewHRInterlock  implements Job {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
			
	public ViewHRInterlock() {
		
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		log.info("** View HR Interlock **");
		Map basicMap = new HashMap();
		basicMap.put("dbName", pm.getString("component.sql.database"));
		basicMap.put("USE_YN", "Y");
		basicMap.put("LOGIN_ID", "admin");
		basicMap.put("COMPANY_ID", pm.getString("component.site.company"));
		basicMap.put("COMPANY_NM", pm.getString("component.site.company.name"));
				
		deptInfoUpldate(basicMap);
		userInfoUpldate(basicMap);
		if("Y".equals(pm.getString("component.hr.functionCd.mapping")))  functionCdUpldate(basicMap);
		if("Y".equals(pm.getString("component.hr.positionCd.mapping"))) 	positionCdUpldate(basicMap);
		
	}
	private void deptInfoUpldate(Map map){
		
		map.put("P_DEPT_CD", "-1");
		List deptList = sm.getList("mg.batch.hr.getDept", map);
		if( deptList.size()== 0 ) {
			log.info("deptInfoUpldate ::  ROOT_DEPT 추가");
			Map topDept = new HashMap();
			topDept.put("P_DEPT_CD", "-1");
			topDept.put("CHILD_YN", "Y");
			topDept.put("USE_YN", "Y");
			topDept.put("LOGIN_ID", "admin");
			topDept.put("DEPT_CD", pm.getString("component.site.company"));
			topDept.put("DEPT_NM", pm.getString("component.site.company.name"));
			sm.createObject("mg.batch.hr.insertDept", topDept);
		}
		map.remove("P_DEPT_CD");
		
		
		sm.updateObject("mg.batch.hr.updateHRDept", map);
		sm.createObject("mg.batch.hr.insertHRDept", map);
	}
	
	private void userInfoUpldate(Map map){
		map.put("includeWorkCd", pm.getString("component.hr.workCd.includeYn"));
		log.info("userInfoUpldate ::  USER 정보 update :: ");
		sm.updateObject("mg.batch.hr.updateHRUser", map);
		if("N".equals(pm.getString("component.hr.workCd.includeYn")))	sm.updateObject("mg.batch.hr.updateRetiredUser", map);//퇴직자 처리
		log.info("userInfoUpldate ::  USER 정보 insert ");
		sm.createObject("mg.batch.hr.insertHRUser", map);
		log.info("userInfoUpldate ::  USER Roll 정보 insert ");
		sm.createObject("mg.batch.hr.insertHRUserRoll", map);		
	}
	
	private void positionCdUpldate(Map map){
		log.info("userInfoUpldate ::  Position Code 정보 update ");
		sm.updateObject("mg.batch.hr.updateHRPositionCd", map);
		log.info("userInfoUpldate ::  Position Code 정보 insert ");
		sm.createObject("mg.batch.hr.insertHRPositionCd", map);
	
	}
	
	private void functionCdUpldate(Map map){
		log.info("userInfoUpldate ::  Function Code 정보 update ");
		sm.updateObject("mg.batch.hr.updateHRFunctionCd", map);
		log.info("userInfoUpldate ::  Function Code 정보 insert ");
		sm.createObject("mg.batch.hr.insertHRFunctionCd", map);
	}
}
	
	
     
     


