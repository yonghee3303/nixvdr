package mg.batch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mg.base.BaseAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;

public class CpServiceListener  implements Job {

	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	
	public CpServiceListener() {
		
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		log.info("-----------------------------------------------------------------------------------------------CpServiceListener 동작 중입니다.");
		Map map = new HashMap();
		String result="0";
		 try{
			//1. CP_DOC_SERVICE의 내용을 읽어온다. 상태가 01인것만...
			 List lists = sm.getList("mg.batch.getService", map);
			
			 for (int i = 0; i < lists.size(); i++) {
	 	          	Map service =(Map) lists.get(i);
	 	          	service.put("STS_CD", "04"); //01: 접수, 02:등록, 03: 오류. 04:대기 
	 	          	sm.begin();
	 	          	sm.updateObject("mg.batch.updateServiceSTS", service); 
	 	          	log.info("STS_CD : 04 ------------------------------------------대기 상태로 변경");
	 	          	sm.commit();
			 } //서버 두대가 돌다보면 중복되기도 함, 이를 방지하기 위해 읽어온 애들은 바로 대기 상태 변경
			 
			 for (int i = 0; i < lists.size(); i++) {
	 	          	Map service =(Map) lists.get(i);
	 	          	String senderMail = service.get("SENDER_EMAIL").toString();
	 	          	String senderId =  (String)sm.getItem("mg.cp.getUserIDFromEmail", senderMail);
	 	          	service.put("SENDER_ID", senderId);
	 	          	result = enrollCp(service);
			 }
		 }catch(Exception e){
			 log.error("-----------------------------------------------------------------------------------------------CpServiceListener 동작 중 오류"+e.toString());
		 }
	}
	
	private String enrollCp(Map map) throws Exception{
		// TODO Auto-generated method stub
		String result ="0";
		Map service = new HashMap();
		//	UPDATE CP_DOC_SERVICE SET STS_CD=#STS_CD#, CHG_DT=GETDATE(), CHG_ID=#CHG_ID# WHERE DOC_NO=#DOC_NO#
		service.put("DOC_NO", map.get("DOC_NO"));
		service.put("CHG_ID", map.get("SENDER_ID"));
		try{	
				sm.begin();
				//1. CP_DOC_MASTER 등록
				log.info("enrollCp-----------------------------------------------------------------CP_DOC_MASTER 등록");
				enrollMaster(map);
				
				//2. 정책 정보 가져오기
				map.put("CD", map.get("POLICY_NO"));
				Map policyMap = (Map)sm.getItem("mg.master.getSingleCpGrp",map);
				log.info("enrollCp-----------------------------------------------------------------정책 정보 갖고 오기 : " + policyMap.toString());
				
				//3. CP_DOC_USER 등록
				log.info("enrollCp-----------------------------------------------------------------CP_DOC_USER 등록");
				enrollDocUser(map, policyMap);
				
				//4. CP_JOB_SCHEDULE 등록
				log.info("enrollCp-----------------------------------------------------------------CP_JOB_SCHEDULE 등록");
				enrollSchedule(map.get("DOC_NO").toString());
				
				//5. TFILE_UPLOAD 저장
				log.info("enrollCp-----------------------------------------------------------------TFILE_UPLOAD 등록");
				enrollUploadFileInfo(map);
				
				//6. CP_DOC_SERVICE 상태 변경
				service.put("STS_CD", "02"); //01: 접수, 02:등록, 03: 오류, 04:대기
				sm.updateObject("mg.batch.updateServiceSTS", service); 
				sm.commit();
		}catch(Exception e){
				sm.rollback();
				sm.begin();
				service.put("STS_CD", "03"); //01: 접수, 02:등록, 03: 오류, 04:대기
				sm.updateObject("mg.batch.updateServiceSTS", service); 
				sm.commit();
				result ="-1";
				log.info("enrollCp-------------------------------------------------------------------------------------"+e.toString());
		}		
		return result;
	}
	
	private void enrollMaster(Map map) throws Exception{
		String docNo =map.get("DOC_NO").toString();

		log.info("enrollMaster-----------------------------------------------------------------CP_DOC_MASTER 등록 : "+docNo);
		//CP_DOC_MASTER에 넣기 위한 정보들 map형태로 생성
		Map mstMap = new HashMap();
		mstMap.put("DOC_NO", docNo);
		if(map.get("SUBJECT") == null || "".equals(map.get("SUBJECT").toString()) ) map.put("SUBJECT", "None Title");
		mstMap.put("DOC_TITLE", map.get("SUBJECT"));
		mstMap.put("WAITING_TIME", "10");
		mstMap.put("CREATOR_EMAIL",map.get("SENDER_EMAIL"));
		mstMap.put("FILE_NUM", map.get("FILE_NUM").toString());
		String temp[] = map.get("FILE_LIST").toString().split("\\|");
		String fileList="";
		String firstTemp[];
		String firstFile="";
		for(int i=0; i < temp.length; i++){
			if(i==0){
				fileList = map.get("FILE_PATH").toString() +temp[i];
				firstTemp = temp[i].split("/");
				firstTemp = firstTemp[firstTemp.length-1].split("\\.");
				firstFile = firstTemp[0];
			}else fileList +="|"+map.get("FILE_PATH").toString() +temp[i];
		}
		mstMap.put("FILE_LIST", fileList);
		mstMap.put("USER_ID", map.get("SENDER_ID"));
		mstMap.put("P_DOC_NO", "");
		mstMap.put("EXE_FILE", pm.getString("cliptorplus.export.default.filename.service")+firstFile+pm.getString("cliptorplus.export.default.extension")); //첫번째 파일과 동일한 파일명을 갖는 보안파일 생성
		mstMap.put("IN_PROCESS", "");
		mstMap.put("EXE_RULE", "USER Creation");
		mstMap.put("AUTHORITY", "4");
		mstMap.put("OUT_CL_CD", "01");
		mstMap.put("APPROVER_ID", "");
		mstMap.put("DOC_MSG", "");
		mstMap.put("LOGIN_ID", map.get("SENDER_ID"));
		mstMap.put("IN_CL_CD", "S");
		//CP_DOC_MASTER 등록
		sm.createObject("mg.cp.insertDocMaster", mstMap);
		
	}
	
	private void enrollDocUser(Map map, Map policy) throws Exception{
		//수신자 정보 갖고 오기
			Map userMap = new HashMap();
	        userMap.put("dbName", pm.getString("component.sql.database"));
	    	String receiverList[] = map.get("RECEIVER_EMAIL").toString().split("\\|");
	    	String receiverPassList[] = map.get("RECEIVER_PW").toString().split("\\|");
	        for (int i=0; i< receiverList.length; i++){
	    		userMap.put("DOC_NO", map.get("DOC_NO"));
	    		userMap.put("USER_ID",receiverList[i]);
	    		Map tempUser =(Map)sm.getItem("mg.cppp.selectMemberInfo",userMap);
	    		userMap.put("COMPANY_ID", tempUser.get("COMPANY_ID"));
	    		userMap.put("POLICY", policy.get("POLICY").toString());
	    		userMap.put("POLICY_DOC", policy.get("POLICY_DOC").toString());
	    		userMap.put("MOD_STOP", "0");
	    		if(policy.get("EXPIRE_DAY") != null && !"0".equals(policy.get("EXPIRE_DAY").toString())){
	    			Calendar calendar = new GregorianCalendar(Locale.KOREA);
	    			calendar.setTime(new Date());
	    			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)+Integer.parseInt(policy.get("EXPIRE_DAY").toString()));
	    			userMap.put("EXPIRE_DT", formatter.format(calendar.getTime()));
	    		}else{
	    			userMap.put("EXPIRE_DT", "");
	    		}
	    		userMap.put("READ_COUNT", policy.get("READ_COUNT").toString());
	    		userMap.put("MESSAGE", "");
	    		userMap.put("MAC_LIST", "");
	    		userMap.put("LOGIN_ID", map.get("SENDER_ID"));
	    		userMap.put("POLICY_NO", policy.get("POLICY_NO").toString());
	    		userMap.put("POLICY_DISP", policy.get("POLICY_DISP").toString());
	    		userMap.put("OUTDEVICE", "");
	    		userMap.put("EMAIL",receiverList);
	    		userMap.put("PASSWD", receiverPassList[i]);
	    		
	    		//CP_DOC_USER 등록
	    		sm.createObject("mg.cp.insertDocUser",userMap);   		
	    	}//for
	 
	}
	
	private void enrollSchedule(String docNo) throws Exception{
		log.info("enrollSchedule-----------------------------------------------------------------CP_JOB_SCHEDULE 등록 : "+docNo);
		Map scheMap = new HashMap();
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		 String scheNo = formatter.format(new Date()) + BaseAction.getRandomString(6);
		 scheMap.put("DOC_NO", docNo);
		 scheMap.put("SCHE_NO", scheNo);
      	 scheMap.put("CP_STS_CD", "02"); //이미 승인이 떨어진 상태에서 요청이 오기 때문에 바로 대기
      	 scheMap.put("EXE_RULE", "USER Creation");
      	//log.info("interfaceMail-----------------------------------------------------------------job schedule 정보 입력 : " + scheMap.toString());
		 sm.createObject("mg.cp.saveJobSchedule", scheMap);
	}
	
	private void enrollUploadFileInfo(Map map) throws Exception{
		Map fileMap = new HashMap();
		String[] fileNameList = map.get("FILE_LIST").toString().split("\\|");
		String filePath =map.get("FILE_PATH").toString();
		//log.info("fileList.lenght----------------------------------------------------------------------"+fileList.length);
		for(int i=0; i<fileNameList.length; i++){
			fileMap.put("PGM_ID", "regMgmt");
			fileMap.put("DOC_ID", map.get("DOC_NO"));
			fileMap.put("FILE_PATH",filePath+fileNameList[i].substring(0,fileNameList[i].lastIndexOf("/")));
			fileMap.put("FILE_NAME",fileNameList[i].substring(fileNameList[i].lastIndexOf("/")+1));	
			//fileMap.put("FILE_SIZE", 0);
			//log.info("fileMap["+i+"]---------------------------------------------------------"+fileMap.toString());
			sm.createObject("mg.vf.insertUploadMappingFile", fileMap);
		}
		
	}

}
