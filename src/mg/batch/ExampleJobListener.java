package mg.batch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

/**
 * 스케줄러 예제. 리스너의 역할을 함.
 * 
 * @since 3.5
 */
public class ExampleJobListener implements JobListener {

    private Log log = LogFactory.getLog(ExampleJobListener.class);

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void jobToBeExecuted(JobExecutionContext arg0) {
        log.error("스케줄러 실행 전");
    }

    public void jobExecutionVetoed(JobExecutionContext arg0) {
        log.error("스케줄러 실행 실패");
    }

    public void jobWasExecuted(JobExecutionContext arg0,
            JobExecutionException arg1) {
        log.error("스케줄러 실행 완료");
    }

}
