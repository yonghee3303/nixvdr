package mg.batch;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.transaction.SystemException;

import mg.base.GridData;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.base.ComponentRegistry;
import com.core.component.util.BaseUtils;
import com.core.component.sql.ISqlManagement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SuInterface  implements Job {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);

	public SuInterface() {
		
	}
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		//1. SU_LINK에 IN_STS_CD 값이 01인 것만 찾는다.
		String dbName = pm.getString("component.sql.database");
		List lists;
		Map map = new HashMap();
		try{
			lists = sm.getList("mg.batch.getSuLink"); // IN_STS_CD 가 01이면서, RENT_CL_CD가 1이 아닌 List 
			 log.info("-----------------------------------------------------------보안 USB 결재 연동 작업 대기 항목이 "+lists.size()+"개 있습니다.");
			for (int i = 0; i < lists.size(); i++) {
				 map = (Map) lists.get(i);
				 sm.begin();
				if(map.get("USB_ID") != null){ 	//기간, 횟수 연장이 들어왔을 때, 보안 USB_ID가 null로 들어온 후 하루 한번 배치가 돌아 USB_ID를 업데이트 한다고 함. 
					 Map createIdmap = (Map)sm.getItem("mg.su.selectRentId",map);
			  		  map.put("USB_CREATE_ID", createIdmap.get("USB_CREATE_ID"));
					//기간연장 : 배치로 바로 바로 수정
					 if("2".equals((String)map.get("RENT_CL_CD"))){
						 log.info("-----------------------------------------------------------기간 연장 작업 대기 중인 USB_ID : "+map.get("USB_ID"));
						  sm.updateObject("mg.batch.editExpireDt", map);	
						  log.info("-----------------------------------------------------------해당 USB 기간 연장 작업이 완료 되었습니다."); 
					 }
					 //횟수연장 : 배치로 바로 바로 수정
					 else if("3".equals((String)map.get("RENT_CL_CD"))){
						 log.info("-----------------------------------------------------------사용 횟수 연장 작업 대기 중인 USB_ID : "+map.get("USB_ID"));
						 sm.updateObject("mg.batch.editReadCnt", map);	
						 log.info("-----------------------------------------------------------해당 USB 사용 횟수 연장 작업이 완료 되었습니다."); 
					 }
					 //사용 가능 MAC_LIST 추가 : 배치로 바로 바로 수정
					 else if("4".equals((String)map.get("RENT_CL_CD"))){
						 log.info("-----------------------------------------------------------사용 MAC 추가 작업 대기 중인 USB_ID : "+map.get("USB_ID"));
						 sm.updateObject("mg.batch.editMacList", map);	
						 log.info("-----------------------------------------------------------해당 USB 사용 MAC 추가 작업 작업이 완료 되었습니다."); 
					 }
					 
					//IN_STS_CD 02로 변경	
					 map.put("IN_STS_CD", "02");
					 sm.updateObject("mg.batch.updateSuLinkStatus", map);
					 sm.commit();
					 log.info("-----------------------------------------------------------해당 USB의 IN_STS_CD를 완료로 변경하였습니다.."); 
				}//if
			 }//for
		}catch(Exception e){
				//IN_STS_CD 03로 변경
			 	map.put("IN_STS_CD", "03");
			 	sm.updateObject("mg.batch.updateSuLinkStatus", map);
			 	sm.commit();
			 	log.info("-----------------------------------------------------------보안 USB 결재 연동 중 오류가 발생 하였습니다. : "+ e.toString());
		}
        
	}
}