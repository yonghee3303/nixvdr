package mg.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.base.ComponentRegistry;
import com.core.component.log.ILogManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;


public class WASLogDeleteJob  implements Job {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	
	 private ILogManagement lm = (ILogManagement)ComponentRegistry.lookup(ILogManagement.class);
			
	public WASLogDeleteJob() {
		
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		log.info("** WASLogDeleteJob **");				
		log.info(lm.getAppender("File").getProperty("File"));
		
		try {
			deleteLog4j();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void deleteLog4j() throws FileNotFoundException{
		
		String logFilePath = (String)lm.getAppender("File").getProperty("File");
		int lastIndex = logFilePath.lastIndexOf("/");
		
		logFilePath=logFilePath.substring(0,lastIndex);
				
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			Date date = new Date();			
			//Calendar calendar = Calendar.getInstance();			
			//calendar.setTime(date);		
			
			//String today = sdf.format(date);
			
    		File textFolder = new File(logFilePath);
    		
    		for (File fileEntry : textFolder.listFiles()) {
    			
    			lastIndex = fileEntry.toString().lastIndexOf(".");
    			String extName =fileEntry.toString().substring(lastIndex+1);
    			
    			if (!extName.equals("log")) {
    				
    				log.info("-------extName : " + extName);
    				
    				try {
        				Date fileDate = sdf.parse(extName);         				
        				long diffTime = date.getTime() - fileDate.getTime(); 
        				long diffDay = diffTime / (24*60*60*1000);    				
        				diffDay = Math.abs(diffDay);
        				log.info("diffDay : "+diffDay);
        				
        				if (diffDay > 2) {
        					fileEntry.delete();
        				}        				
        				
        			} catch(Exception e) {
        				log.info("Exception : "+e.toString());
        			}
    			}  
    			
    	    }
    		
    		
        }catch(Exception e){        	
        	log.info("polarisTextFileReader ioexception : "+e.toString());
        }
	}
	
}
	
	
     
     


