package mg.batch;

import java.util.HashSet;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.transaction.SystemException;
import javax.xml.rpc.ServiceException;

import mg.base.GridData;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.LGC.esb.Profile.DeptInfoListVO;
import com.LGC.esb.Profile.EmployeeProfileListVO;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.base.ComponentRegistry;
import com.core.component.util.BaseUtils;
import com.core.component.sql.ISqlManagement;
import com.lgchem.www.AppIntLGChem.BatchDeptInfoList;
import com.lgchem.www.AppIntLGChem.BatchDeptInfoListServiceLocator;
import com.lgchem.www.AppIntLGChem.BatchEmployeeProfileListNew;
import com.lgchem.www.AppIntLGChem.BatchEmployeeProfileListNewServiceLocator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* 업무 그룹명 : mg.batch
* 서브 업무명 : MgHRInterlock.java
* 작성자 : whchoi
* 작성일 : 2016. 03. 15 
* 설 명 : MgWeb / LogManager HR 연동
* 최종 수정일 : 2016. 03. 15
* 수정 내용 : 주석 처리 
* 참조 사항 : MG -> MGWeb 연동시 직책정보 연동(component.hr.functionCd.mapping)을 사용할 경우
* 	1) TB_USERCODE 테이블 FUNCTIONCD 컬럼 추가
* 	2) MG Console HR 연동시 프로시저내 FUCNTIONCD 값 추가
*/ 

public class MgHRInterlock  implements Job {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
			
	public MgHRInterlock() {
		
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		log.info("----Batch---MgHRInterlock------Start--- ");
		
		if("LGCHEM".equals(pm.getString("component.site.company"))){
			if("CP".equals(pm.getString("component.site.product").toUpperCase())){
				//JobDataMap data = context.getJobDetail().getJobDataMap();
		        //data.getString("QUEUE_MODE").equals("Service IP");
		        
				///////////////////// LG -> MGWEB 동기화 /////////////////////
				// 부서정보 연동
		        if(deptInfoList()){
		        	log.info("-----------Success-----------deptInfoList---------");
		        }else{
		        	log.error("-----------Failed-----------deptInfoList---------");
		        }
		        // 사원정보 연동
		        if(employeeProfileListNew()){
		        	log.info("-----------Success-----------employeeProfileListNew---------");
		        }else{
		        	log.error("-----------Failed-----------employeeProfileListNew---------");
		        }
			} else if("LogManager".equals(pm.getString("component.site.product"))) {
				///////////////////// MG -> MGWEB 동기화 /////////////////////
				//부서정보 연동
				if(mgDeptInfoList()){
					log.info("-----------Success-----------mgDeptInfoList---------");
				}else{
					log.error("-----------Failed-----------mgDeptInfoList---------");
				}
				//사원정보 연동
				if(mgEmployeeProfileInfoList()){
					log.info("-----------Success-----------mgEmployeeProfileListNew---------");
				}else{
					log.error("-----------Failed-----------mgEmployeeProfileListNew---------");
				}
				
				///////////////////// LogManager(MGWEB -> LM) 동기화 /////////////////////
				//부서정보 연동
				if(lmSvrInfoList()){
					log.info("-----------Success-----------lmSvrInfoList---------");
				}else{
					log.error("-----------Failed-----------lmSvrInfoList---------");
				}
				//사원정보 연동
				if(lmUserInfoList()){
					log.info("-----------Success-----------lmUserInfoList---------");
				}else{
					log.error("-----------Failed-----------lmUserInfoList---------");
				}
			}
		} else if("ILJIN".equals(pm.getString("component.site.company"))){
			if("CP".equals(pm.getString("component.site.product"))){
				///////////////////// MG -> MGWEB 동기화 /////////////////////
				//부서정보 연동
				if(mgDeptInfoList()){
					log.info("-----------Success-----------mgDeptInfoList---------");
				}else{
					log.error("-----------Failed-----------mgDeptInfoList---------");
				}
				//사원정보 연동
				if(mgEmployeeProfileInfoList()){
					log.info("-----------Success-----------mgEmployeeProfileListNew---------");
				}else{
					log.error("-----------Failed-----------mgEmployeeProfileListNew---------");
				}
			}
		}

		log.info("----Batch---MgHRInterlock------End--- ");
                           
	}

	
	// 부서정보 연동 (LG -> MGWEB)
	public Boolean deptInfoList(){
		
		try {
			
			log.info("MgHRInterlock deptInfoList Start");
			
			// 자동 생성되어진 웹서비스 클래스 선언
			BatchDeptInfoListServiceLocator locator = new BatchDeptInfoListServiceLocator();
			log.info("deptInfoList - 1-----------------------------------------"+locator.getBatchDeptInfoListSoapPortAddress());
			BatchDeptInfoList service = locator.getBatchDeptInfoListSoapPort();
			log.info("deptInfoList - 2");

			// 서비스 호출
			DeptInfoListVO[] outVo = service.batchDeptInfoList();
			log.info("deptInfoList - 3");
			
			// 결과값 조회
			for(int i=0;i<outVo.length;i++){
				/*log.info("["+i+"] DIVISION["+outVo[i].getDIVISION() +
                				"]|CODE_NUMB["+outVo[i].getCODE_NUMB()+
                				"]|CODE_NAME_KO["+outVo[i].getCODE_NAME_KO()+
                				"]|CODE_NAME_EN["+outVo[i].getCODE_NAME_EN()+
                				"]|CODE_NAME_CN["+outVo[i].getCODE_NAME_CN()+
                				"]|CODE_UPER["+outVo[i].getCODE_UPER()+
                				"]|CREATEDATE["+outVo[i].getCREATEDATE()+
                				"]|UPDATEDATE["+outVo[i].getUPDATEDATE()+			                         		                                      
                				"]");*/

				/*log.info("["+i+"] 테스트DIVISION["+outVo[i].getDIVISION() +
								"]|테스트CODE_NUMB["+outVo[i].getCODE_NUMB()+                         		                                      
								"]");*/
				
				// 리턴받은 부서정보 -> DB update 트랜잭션 시작
				
				Map deptMap = new HashMap();
				deptMap.put("DEPT_CD", outVo[i].getCODE_NUMB());
				deptMap.put("DEPT_NM", outVo[i].getCODE_NAME_KO());
				deptMap.put("DEPT_ENM", outVo[i].getCODE_NAME_EN());
				deptMap.put("P_DEPT_CD", outVo[i].getCODE_UPER());
				deptMap.put("LOGIN_ID", "admin");
				deptMap.put("USE_YN", outVo[i].getVALID_FLAG());
				
				String pDeptNm;
				if(sm.getItem("mg.batch.getPDept",deptMap) != null){
					pDeptNm =sm.getItem("mg.batch.getPDept",deptMap).toString();
					log.info("-------------------------------------------상위 부서 이름 얻어오기----"+pDeptNm);
				}else{
					pDeptNm = "";
				}
		    	deptMap.put("P_DEPT_NM", pDeptNm);
		    	
		    	if(sm.getItem("mg.batch.getDept",deptMap) != null){
		    		log.info("------------------------------------update---------"+outVo[i].getCODE_NUMB());
		    		sm.updateObject("mg.batch.updateDept", deptMap);
		    	}else{
		    		log.info("------------------------------------insert---------"+outVo[i].getCODE_NUMB());
		    		sm.createObject("mg.batch.insertDept", deptMap);
		    	}
					
			}
			log.info("MgHRInterlock deptInfoList End");
			return true;
		} catch(ServiceException e1) {
			//log.error("=========MgHRInterlock========deptInfoList=================");
			log.error("ServiceException");
			e1.printStackTrace();
			return false;
		} catch(RemoteException e2) {
			//log.error("=========MgHRInterlock========deptInfoList=================");
			log.error("RemoteException");
			e2.printStackTrace();
			return false;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
		
	}
	
	
	
	// 사원정보 연동 (LG -> MGWEB)
	public Boolean employeeProfileListNew(){
		
		try {
			
			log.info("MgHRInterlock employeeProfileListNew Start");
			// 자동 생성되어진 웹서비스 클래스 선언
			BatchEmployeeProfileListNewServiceLocator locator = new BatchEmployeeProfileListNewServiceLocator();
			log.info("employeeProfileListNew - 1---------------------------------------------------"+locator.getBatchEmployeeProfileListNewSoapPortAddress());
			BatchEmployeeProfileListNew service = locator.getBatchEmployeeProfileListNewSoapPort();
			log.info("employeeProfileListNew - 2");
			
			      
			//서비스 호출
			EmployeeProfileListVO[] outVo = service.batchEmployeeProfileList();
			log.info("employeeProfileListNew - 3");
			//결과값 조회		
	
			for(int i=0;i<outVo.length;i++){
				
				/*log.info("["+i+"] SA_USER["+outVo[i].getSA_USER() +
                						"]|SA_PHON_O["+outVo[i].getSA_PHON_O()+
                						"]|SA_PHON_D["+outVo[i].getSA_PHON_D()+
                						"]|SA_PHON_H["+outVo[i].getSA_PHON_H()+
                						"]|SA_SABUN["+outVo[i].getSA_SABUN()+
                						"]|SA_NAME["+outVo[i].getSA_NAME()+
                						"]|SA_NAME_E["+outVo[i].getSA_NAME_E()+
                						"]|SA_DEPT["+outVo[i].getSA_DEPT()+
                						"]|SA_DEPT_NAME["+outVo[i].getSA_DEPT_NAME()+
                						"]|SA_DEPT_NAME_E["+outVo[i].getSA_DEPT_NAME_E()+
                						"]|SA_DEPT_NAME_C["+outVo[i].getSA_DEPT_NAME_C()+
                						"]|SA_JOBX["+outVo[i].getSA_JOBX()+
                						"]|SA_JOBX_NAME["+outVo[i].getSA_JOBX_NAME()+
                						"]|SA_JOBX_NAME_E["+outVo[i].getSA_JOBX_NAME_E()+
                						"]|SA_JOBX_NAME_C["+outVo[i].getSA_JOBX_NAME_C()+
                						"]|SA_FUNC["+outVo[i].getSA_FUNC()+
                						"]|SA_FUNC_NAME["+outVo[i].getSA_FUNC_NAME()+
                						"]|SA_FUNC_NAME_E["+outVo[i].getSA_FUNC_NAME_E()+
                						"]|SA_FUNC_NAME_C["+outVo[i].getSA_FUNC_NAME_C()+
                						"]|SA_RECT["+outVo[i].getSA_RECT()+
                						"]|SA_TEMP["+outVo[i].getSA_TEMP()+
                						"]|SA_TEMP_DATE["+outVo[i].getSA_TEMP_DATE()+
                						"]|SA_HAND["+outVo[i].getSA_HAND()+
                						"]|SA_SYSTEM["+outVo[i].getSA_SYSTEM()+
                						"]|SA_PHON_AREA["+outVo[i].getSA_PHON_AREA()+
                						"]|SA_COMP["+outVo[i].getSA_COMP()+
                						"]|SA_PHON_HAREA["+outVo[i].getSA_PHON_HAREA()+
                						"]|SA_EDEPT_NAME["+outVo[i].getSA_EDEPT_NAME()+
                						"]|SA_SABUN_NEW["+outVo[i].getSA_SABUN_NEW()+
                						"]|SA_DEPT_NEW["+outVo[i].getSA_DEPT_NEW()+
                						"]|SA_HNAME["+outVo[i].getSA_HNAME()+
                						"]|SSO_EX_FLAG["+outVo[i].getSSO_EX_FLAG()+
                						"]|SSO_PS_DATE["+outVo[i].getSSO_PS_DATE()+
                						"]|SSO_MAILSERVER["+outVo[i].getSSO_MAILSERVER()+
                						"]|SA_SABUN_LEADER["+outVo[i].getSA_SABUN_LEADER()+			                                      
                						"]|SA_GNMU["+outVo[i].getSA_GNMU()+
                						"]|SA_GNMU_NAME["+outVo[i].getSA_GNMU_NAME()+
                						"]|SSO_LOCATE["+outVo[i].getSSO_LOCATE()+
                						"]" );*/

				/*	log.info("["+i+"] 테스트SA_USER["+outVo[i].getSA_USER() +
										"]|테스트SA_PHON_O["+outVo[i].getSA_PHON_O()+
										"]|테스트SA_GNMU_NAME["+outVo[i].getSA_GNMU_NAME()+
										"]|테스트SSO_LOCATE["+outVo[i].getSSO_LOCATE()+
										"]" );*/
				
				// 리턴받은 사원정보 -> DB update 트랜잭션 시작
				Map userMap = new HashMap();

				userMap.put("USER_ID",outVo[i].getSA_USER());
				userMap.put("USER_NM",outVo[i].getSA_NAME());
				userMap.put("USER_ENM",outVo[i].getSA_NAME_E());
				userMap.put("DEPT_CD",outVo[i].getSA_DEPT_NEW());
				userMap.put("POSITION_CD",outVo[i].getSA_JOBX());
				userMap.put("FUNCTION_CD",outVo[i].getSA_FUNC());
				String compCd = outVo[i].getSA_COMP();
				if("DB".equals(compCd.substring(0, 2)) && Integer.parseInt(compCd.substring(2, 4)) > 0 && Integer.parseInt(compCd.substring(2, 4)) <= 10){
					log.info("---------email ---------- : "+outVo[i].getSA_USER()+"@farmhannong.com");
					userMap.put("EMAIL",outVo[i].getSA_USER()+"@farmhannong.com");	
				}else{
					log.info("---------email ---------- : "+outVo[i].getSA_USER()+"@lgchem.com");
					userMap.put("EMAIL",outVo[i].getSA_USER()+"@lgchem.com");
				}
				userMap.put("TEL_NO",outVo[i].getSA_PHON_O());
				userMap.put("EMP_NO",outVo[i].getSA_SABUN_NEW());
				userMap.put("LOGIN_ID", "admin");
				userMap.put("WORK_CD", "C");
				userMap.put("APPROVER_ID", outVo[i].getSA_SABUN_LEADER());
				userMap.put("isUseSabun", "Y");
				try{
					if( Integer.parseInt(sm.getItem("mg.sys.countUser", userMap).toString() ) > 0 ) {
						log.info("------------------------------------update---------"+outVo[i].getSA_USER());
			       		sm.updateObject("mg.batch.updateUser", userMap);
			       	} else {
			       		log.info("------------------------------------insert---------"+outVo[i].getSA_USER());
			       		sm.createObject("mg.batch.insertUser", userMap); 
			       	}
					log.info("------------------------------------deleteAllApprover---------"+outVo[i].getSA_USER());
					sm.deleteObject("mg.batch.deleteAllApproverOfUser", userMap); //단일 승인자 사용
					log.info("------------------------------------insertApprover---------"+outVo[i].getSA_SABUN_LEADER());
					sm.createObject("mg.batch.insertApprover", userMap);
				}catch(com.core.base.SystemException se){ //키 관련해서 오류가 나면 그 이후의 정보들은 무시하고 멈추는 것을 막기 위해... 
					log.error("-------------------------------------------------------------"+se.toString());
				}
			}
			
			log.info("MgHRInterlock employeeProfileListNew End");
			return true;
		} catch(ServiceException e1) {
			//log.error("==========MgHRInterlock==========employeeProfileListNew================");
			log.error("ServiceException");
			e1.printStackTrace();
			return false;
		} catch(RemoteException e2) {
			//log.error("==========MgHRInterlock==========employeeProfileListNew================");
			log.error("RemoteException");
			e2.printStackTrace();
			return false;
		}catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
		
	}
	
	// 부서정보 연동 (MG -> MGWEB)
	public Boolean mgDeptInfoList(){
		
		try {
			log.info("MgHRInterlock mgDeptInfoList Start");
			
			// 서비스 호출
			List deptList = sm.getList("mg.batch.getMgDeptList");
			log.info("deptInfoList : " + deptList.size());

			//최상위 부서코드 존재여부 확인
			if( Integer.parseInt(sm.getItem("mg.batch.countRootDept", null).toString() ) == 0 ) {
				log.info("rootDept 추가");
				Map smap = new HashMap();
				smap.put("DEPT_CD", pm.getString("component.site.company"));
				smap.put("DEPT_NM", pm.getString("component.site.company.name"));
				smap.put("P_DEPT_CD", "-1");
				smap.put("CHILD_YN", "Y");
				smap.put("LOGIN_ID", "admin");
				smap.put("USE_YN", "Y");
				
				sm.createObject("mg.batch.insertRootDept", smap);
			}
			
			// 결과값 조회
			for(int i=0; i<deptList.size(); i++) {
				
				// 리턴받은 부서정보 -> DB update 트랜잭션 시작
				
				Map deptMap = (Map) deptList.get(i);
				
		    	//deptMap.put("P_DEPT_NM", pDeptNm);
		    	deptMap.put("LOGIN_ID", "admin");
		    	deptMap.put("USE_YN", "Y");
		    	//deptMap.put("DEPT_ENM", "");
		    	if(String.valueOf(deptMap.get("TREE_LEN")).equals("2")) {	//최상위 코드 ROOT 코드와 연결
		    		deptMap.put("P_DEPT_CD",  pm.getString("component.site.company"));
		    	}
		    	
		    	if(sm.getItem("mg.batch.getDept",deptMap) != null){
		    		log.info("------------------------------update MDEPT : " + deptMap.get("DEPT_CD") + " / " + deptMap.get("DEPT_NM"));
		    		sm.updateObject("mg.batch.updateDept", deptMap);
		    	} else{
		    		log.info("------------------------------insert MDEPT : " + deptMap.get("DEPT_CD") + " / " + deptMap.get("DEPT_NM"));
		    		sm.createObject("mg.batch.insertDept", deptMap);
		    	}		    		
			}
			log.info("------------------------------ MDEPT CHECK FORM, DELETE  --------------------------------");
			sm.deleteObject("mg.batch.checkDeptDel", null);	//형식이 맞지 않는 부서 삭제
			log.info("------------------------------ MDEPT CHECK COMPARE TB_DPTCODE, UPDATE --------------------------------");
			sm.updateObject("mg.batch.updateDeptUseYn", null);	//TB_DPTCODE내 삭제된 부서 USE_YN = 'N' 변경
			
			log.info("MgHRInterlock mgDeptInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
		
	}
	
	// 사원정보 연동  (MG -> MGWEB)
	public Boolean mgEmployeeProfileInfoList(){
		
		try {
			log.info("MgHRInterlock mgEmployeeProfileInfoList Start");
			
			String functionPCd = "A0201";
			String positionPCd = "A0204";
			Map functionMap = null;
			Map positionMap = null;
			
			//직책 정보 코드 매핑 (직책, 직급 값이 코드정보로 매핑되어 있지 않을 경우 사용)
			if("Y".equals(pm.getString("component.hr.functionCd.mapping"))) {
				int cnt = Integer.parseInt(sm.getItem("mg.batch.getCodeCnt", functionPCd).toString());
				//미등록 직책 정보
				List functionList = sm.getList("mg.batch.getNonRegiFunctionList");
				for(int i=0; i<functionList.size(); i++) {
					Map tmpMap = (Map)functionList.get(i);	//CD_NM
					tmpMap.put("P_CD", functionPCd);
					tmpMap.put("LOGIN_ID", "admin");
					tmpMap.put("CNT", ++cnt);
					sm.createObject("mg.batch.insertHrCode", tmpMap);					
				}
				List functionMappingList = sm.getList("mg.batch.getCdNmMappingList", functionPCd);
				functionMap = new HashMap<String, String>();
				
				for(int i=0; i<functionMappingList.size(); i++) {
					Map tmpMap = (Map)functionMappingList.get(i);
					functionMap.put(tmpMap.get("CD_NM"), tmpMap.get("CD_KEY"));
				}
			}
			//직급(직위) 정보 코드 매핑
			if("Y".equals(pm.getString("component.hr.positionCd.mapping"))) {
				int cnt = Integer.parseInt(sm.getItem("mg.batch.getCodeCnt", positionPCd).toString());
				//미등록 직급(직위) 정보
				List positionList = sm.getList("mg.batch.getNonRegiPositionList");
				for(int i=0; i<positionList.size(); i++) {
					Map tmpMap = (Map)positionList.get(i);
					tmpMap.put("P_CD", positionPCd);
					tmpMap.put("LOGIN_ID", "admin");
					tmpMap.put("CNT", ++cnt);
					sm.createObject("mg.batch.insertHrCode", tmpMap);					
				}
				List positionMappingList = sm.getList("mg.batch.getCdNmMappingList", positionPCd);
				positionMap = new HashMap<String, String>();
				
				for(int i=0; i<positionMappingList.size(); i++) {
					Map tmpMap = (Map)positionMappingList.get(i);
					positionMap.put(tmpMap.get("CD_NM"), tmpMap.get("CD_KEY"));
				}
			}
			
			//서비스 호출
			List userList = sm.getList("mg.batch.getMgUserList");
			log.info("mgEmployeeProfileInfoList : " + userList.size());
			
			//결과값 조회		
			for(int i=0;i<userList.size();i++){
			
				Map userMap = (Map)userList.get(i);
				/*userMap.put("USER_ENM","");
				userMap.put("EMP_NO","");*/
				userMap.put("WORK_CD", "C");
				userMap.put("LOGIN_ID","admin");
				userMap.put("dbName", pm.getString("component.sql.database"));
				
				//직책, 직급 정보 : 한글 명칭 -> 코드화
				if("Y".equals(pm.getString("component.hr.functionCd.mapping"))) {
					userMap.put("FUNCTION_CD", functionMap.get(userMap.get("FUNCTION_CD")));
				}
				if("Y".equals(pm.getString("component.hr.positionCd.mapping"))) {
					userMap.put("POSITION_CD", positionMap.get(userMap.get("POSITION_CD")));
				}
					
				
				if("LGCHEM".equals(pm.getString("component.site.company"))){
					userMap.put("EMAIL",userMap.get("USER_ID")+"@lgchem.com");
				}

				//직책/직급 모두 없을 경우 01(??????) 직책 설정
				if(userMap.get("FUNCTION_CD") == null && userMap.get("POSITION_CD") == null) {
					userMap.put("FUNCTION_CD", "01");
				}
				
				int userCnt = Integer.parseInt(sm.getItem("mg.batch.getDeptUserCnt", userMap).toString() );
				
				if(userCnt > 0 ) {	//부서내 특정 사용자 등록여부 조회
					log.info("------------------------------update SYSUSER : " + userMap.get("USER_ID") + " / " + userMap.get("USER_NM"));
					sm.updateObject("mg.batch.updateUser", userMap);
				} else if(userCnt == 0) {
					log.info("------------------------------insert SYSUSER : " + userMap.get("USER_ID") + " / " + userMap.get("USER_NM"));
					sm.createObject("mg.batch.insertUser", userMap);
					sm.createObject("mg.batch.insertUserRoll", userMap);
					
				} else {
					log.info("------------------------------ERROR batch mgEmployeeProfileInfoList, check user dept_cd : USER_ID(" + userMap.get("USER_ID") + ") / USER_NM(" + userMap.get("USER_NM") + ") / DEPT_CD(" + userMap.get("DEPT_CD") + ")");
				}
			}
			log.info("------------------------------ SYSUSER CHECK COMPARE TB_USERCODE, UPDATE --------------------------------");
			sm.updateObject("mg.batch.updateUserWorkCd", null);
			
			log.info("MgHRInterlock mgEmployeeProfileInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
	}
	
	
	
	//LM 사업장정보 연동
	public Boolean lmSvrInfoList(){
		
		try {
			
			log.info("MgHRInterlock lmSvrInfoList Start");
			
			// 서비스 호출
			List deptList = sm.getList("mg.batch.getDeptList");
			log.info("deptInfoList : " + deptList.size());
			
			//사업장 정보 중복 방지
			Set svrInfo = new HashSet();
	
			// 결과값 조회
			for(int i=0; i<deptList.size(); i++) {
				
				Map deptMap = (Map) deptList.get(i);
		    	deptMap.put("LOGIN_ID", "admin");
		    	      
				//사업장 정보 얻어오기
	    		Map svrMap = (Map)sm.getItem("mg.batch.getLmSvrInfo",deptMap);
				String svrId;
				String svrNm;
				svrId = (String)svrMap.get("SVR_ID");
				svrNm = (String)svrMap.get("SVR_NM");
				//log.info("-------------------------------------------사업장 정보 얻어오기----"+svrNm);
				if(svrId != null) {
					deptMap.put("SVR_ID", svrId);
					deptMap.put("SVR_NM", svrNm);
					
					if( Integer.parseInt(sm.getItem("mg.batch.countLmSvr",deptMap).toString() ) > 0 ) {
						if(!svrInfo.contains(svrId)) {
			    			log.info("------------------------------------update LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
		    				sm.updateObject("mg.batch.updateLmSvr", deptMap);
		    				svrInfo.add(svrId);
			    		}
			    	} else{
			    		log.info("------------------------------------insert LO_SVR_INFO--------- : " + deptMap.get("SVR_ID") + " / " + deptMap.get("SVR_NM"));
		    			sm.createObject("mg.batch.insertLmSvr", deptMap);
	    				svrInfo.add(svrId);
			    	}	
				}
			}
			log.info("MgHRInterlock lmSvrInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
		
	}
	
	//LM 사용자정보 연동
	public Boolean lmUserInfoList(){
		
		try {
			
			log.info("MgHRInterlock lmUserInfoList Start");
			      
			//서비스 호출
			List userList = sm.getList("mg.batch.getUserList");
			log.info("userInfoList : " + userList.size());
	    	
			//결과값 조회		
			for(int i=0;i<userList.size();i++){
		
				Map userMap = (Map)userList.get(i);
				userMap.put("dbName",pm.getString("component.sql.database"));

				if(userMap.get("DEPT_CD") != null) {
					
					int userCnt = Integer.parseInt(sm.getItem("mg.batch.getDeptLmUserCnt", userMap).toString() );
					
					if(userCnt > 0 ) {	//부서내 특정 사용자 로그매니저 등록여부 조회
						log.info("------------------------------------update LO_USER_INFO--------- : " + userMap.get("USER_ID"));
						sm.updateObject("mg.batch.updateLmUser", userMap);						
					} else if(userCnt == 0) {
						log.info("------------------------------------insert LO_USER_INFO--------- : " + userMap.get("USER_ID"));
						sm.createObject("mg.batch.insertLmUser", userMap);		       			
					} else {
						log.info("------------------------------ERROR batch lmUserInfoList, check user dept_cd : USER_ID(" + userMap.get("USER_ID") + ") / USER_NM(" + userMap.get("USER_NM") + ") / DEPT_CD(" + userMap.get("DEPT_CD") + ")");
					}
				}													
			}
			log.info("MgHRInterlock lmUserInfoList End");
			return true;
		} catch(Exception e3) {
			log.error("-------------------------------------------------------------"+e3.toString());
			e3.printStackTrace();
			return false;
		}
	}
}
	
	
     
     


