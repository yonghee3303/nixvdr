package mg.batch;

/*
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
*/
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 스케줄러 예제. 로그를 기록함.
 * 
 * @since 3.5
 */

public class ExampleJob implements Job {
	// private Logger log = LoggerFactory.getLogger(ExampleJob.class);
	
	private Log log = LogFactory.getLog(ExampleJob.class);
	
    private static int count;
    
    public ExampleJob() {
    
    }
    
    @Override
    public void execute(JobExecutionContext context)
            throws JobExecutionException {
    	
        log.error("예제 스케줄러 실행 : " + ++count);
        
        System.out.println("Executing Job");
        

    }

}