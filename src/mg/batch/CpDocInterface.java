package mg.batch;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.component.parameter.IParameterManagement;
import com.core.base.ComponentRegistry;
import com.core.component.sql.ISqlManagement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CpDocInterface  implements Job {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);

	public CpDocInterface() {
		
	}
	/**
	* Cliptor-Plus Interface Program
	* by Cylim 
	*/
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		JobDataMap data = context.getJobDetail().getJobDataMap();
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curDt = formatter.format(new Date());
			
        log.info("--------------------------------------------------------------------------------\nLOG Job Run !! - " + data.getString("QUEUE_MODE"));
                
        if(data.getString("QUEUE_MODE").equals("CP_DOC_LINK")) {   /** 반출신청 현황조회 */
            readDocLink();                         
        }
	}
	
	
	 /*
     * 랜덤한 문자열을 원하는 길이만큼 반환합니다.
     * 
     * @param length 문자열 길이
     * @return 랜덤문자열
     */
    private static String getRandomString(int length)
    {
      StringBuffer buffer = new StringBuffer();
      Random random = new Random();

      for (int i=0 ; i<length ; i++)
      {
        buffer.append(random.nextInt(9));
      }
      return buffer.toString();
    }
    
	private String getStringWithNullCheck(Map map, String key) 
	 {
	        Object result = map.get(key);
	        return (result == null) ? null : result.toString();
	  }
	
	
	private Map dataValidation(Map map) throws Exception{
		
		// 수 일치 validation -----수신자ID,비밀번호,수신자이름,회사이름,MAC주소
  		String user_id = (String)map.get("R_USER_ID");
  		String user_pw = (String)map.get("R_USER_PW");
   		String user_nm = (String)map.get("R_USER_NM");
   		String company_nm = (String)map.get("COMPANY_NM");
   		String mac_list = (String)map.get("MAC_LIST");
   		log.info("1-----------dataValidation");
   		String[] userIds = user_id.split("\\|");
   		String[] userPws = user_pw.split("\\|");
   		String[] userNms = user_nm.split("\\|");
   		String[] companyNms = company_nm.split("\\|");
   		String[] mac_lists = mac_list.split("\\^"); // 수신자별 mac list 구분자는 ^ 사용
   		log.info("2-----------dataValidation");
   		Map result = new HashMap();
   		
   		if(userIds.length!=userPws.length){
   			// 수신자 비밀번호 오류(04), 메시지 업데이트
   			log.error("-------수신자 비밀번호 오류-------");
   			result.put("validValue", "false");
   			result.put("job_desc", "Not Matched-R_USER_PW");
      		return result;
   		}else if(userIds.length!=userNms.length){
   			// 수신자 이름 오류(04), 메시지 업데이트
   			log.error("-------수신자 이름 오류-------");
   			result.put("validValue", "false");
   			result.put("job_desc", "Not Matched-R_USER_NM");
   			return result;
   		}else if(userIds.length!=companyNms.length){
   			// 회사이름 오류(04), 메시지 업데이트
   			log.error("-------수신자 회사이름 오류-------");
   			result.put("validValue", "false");
   			result.put("job_desc", "Not Matched-COMPANY_NM");
   			return result;
   		}else if(userIds.length!=mac_lists.length){
   			// MAC주소 오류(04), 메시지 업데이트
   			log.error("-------수신자 MAC주소 오류-------");
   			result.put("validValue", "false");
   			result.put("job_desc", "Not Matched-MAC_LIST");
   			return result;
   		}else{
   			log.info("-------데이터 Validation 성공-------");
   			result.put("validValue", "true");
   			return result;
   		}
		
	}
	
	
		
	/*
     * Interface Table을 읽어서 스케쥴을 등록 한다.
     * @param request
     * @return
     */
     private boolean readDocLink()
     { 		 
 		 try {
 			 log.info("-------------------readDocLink 진입----------------------");
 			List lists;
 			String dbName = pm.getString("component.sql.database");
 			Map temp = new HashMap();
 			if(".exe".equals(pm.getString("cliptorplus.export.default.extension"))) temp.put("DEFAULT_CRE_TYPE","E");
 			else temp.put("DEFAULT_CRE_TYPE","D");
 			
 			if("mssql".equals(dbName)) {
	       		lists = sm.getList("mg.batch.getLinkTable",temp);
	       	 } else {
	       		lists = sm.getList("mg.batch.getLinkTable_" + dbName,temp);
	       	 }
	 		 
	 		
	 		/*if(lists.size()==0){
	 			log.info("대기 Link Table 내역이 없습니다.");
	 			return false;
	 		}*/
	 		
	 		//log.info("대기중인 Link Table 내역 : " + lists.toString());
		 	//log.info("대기중 내역 lists size : " + lists.size());
	 		
	       	//-----------------농협생명 Failed while autocommiting a connection 에러--------------------------
	       	 
	       	
	 		 for (int i = 0; i < lists.size(); i++) {
	 			 
		          	Map map = (Map) lists.get(i);
		          	Map param = new HashMap();
		          	param.put("dbName", dbName);
		            DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		       		String scheNo = formatter.format(new Date()) + getRandomString(6);
		       		param.put("SCHE_NO", scheNo);
		       		param.put("CP_STS_CD", "02");
		       		param.put("EXE_RULE", "02");
	        		param.put("CP_DOC_STATUS", "02");
	        		param.put("JOB_PROGRESS", 0);
	        		param.put("JOB_DESC","");
	        		param.put("DOC_NO", map.get("DOC_NO"));
	        		param.put("APPROVE_DT", map.get("APPROVE_DT"));
	        		param.put("DOC_TITLE", map.get("DOC_TITLE"));
	        		param.put("EXE_FILE", map.get("EXE_FILE"));
	        		param.put("FILE_NUM", map.get("FILE_NUM"));
	        		param.put("USER_ID", map.get("USER_ID"));
	        		param.put("USER_NM", map.get("USER_NM"));
		          	
	        		//log.info("LINK MAC_LIST : " + map.get("MAC_LIST"));
	        		//log.info("파일 개수 : " + map.get("FILE_LIST").toString().split("\\|").length);
	        		sm.begin();
	        		
	        		// readDocLink 작업 수행 ------ TRANSFER_FLAG = 'Y'
	        		log.info("------------------------------------readDocLink 작업 수행 -");
	         		sm.createObject("mg.batch.updateDocLink", param);
	         		
	         		// CP_JOB_STATUS 등록
	         		sm.createObject("mg.batch.insertDocStatus", param);
	         		log.info("------------------------------------CP_JOB_STATUS 등록 -");
	         		// 데이터 검증 수행
	         		Map validResult = dataValidation(map);
	         		
	         		//log.info("데이터 검증 결과 : " + validResult.get("validValue"));
	         		
	         		if("true".equals(validResult.get("validValue"))) {
	         			//데이터 정상시
	         			log.info("------------------------------------데이터 정상 -");
	         			//master 등록
	         			if(insertDocMaster(map)) {
	         				//수신자 등록
	    		            if(insertDocUser(map)) {
	    		            	
	    		            	sm.createObject("mg.cp.saveJobSchedule", param); //스케쥴 등록
	    		            	log.info("------------------------------------스케쥴 등록 -");
	    		            	// 신청자가 등록되지 않은 경우 SYSUSER 신청자 등록 - log 확인 위함
	    		         		if( Integer.parseInt(sm.getItem("mg.sys.countUser", param).toString() ) <= 0 ) {
	    		         			log.info("-------기존에 없는 신청자 -------SYSUSER에 simple 등록");
	    		         			sm.createObject("mg.sys.insertUserSimple", param);
	    		         		}
	    		         		
	    		         		sm.commit();
	    		         		return true;
	    		         		
	    		            }
	    		            log.error("수신자 등록 실패");
	         			}
	         			log.error("마스터 등록 실패");
	         			sm.commit();
	         			return false;
	         			
	         		}else{
	         			//데이터 오류시
	         			param.put("cp_sts_cd", "05");
		         		param.put("job_desc", validResult.get("job_desc"));
		         		param.put("sche_no", scheNo);
		       			sm.createObject("mg.cp.setStatus", param);
		       			sm.commit();
		       			return false;
	         		}
        		
	 		 }
	 		 
 		 } catch(Exception e) {
 			 //sm.rollback(); //rollback하지 않음-오류처리(05)로 대체
 			 log.error("CpDocInterface : readDocLink Error  : " + e.getMessage());
 			 sm.commit();
 			 return false;
 		 }
 		 return true;
     }
     
     // Doc Master 등록
     // #DOC_NO#,#DOC_TITLE#,#WAITING_TIME#,#CREATOR_EMAIL#,#FILE_NUM#,#FILE_LIST#,#USER_ID#,#P_DOC_NO#,#EXE_FILE#,#IN_PROCESS#,#EXE_RULE#,#AUTHORITY#,#OUT_CL_CD#,#APPROVER_ID#,#DOC_MSG#,GETDATE(),#LOGIN_ID#, GETDATE())
     private boolean insertDocMaster(Map map) 
     {	 
    	 try {
	    	 Map param = map;
	    	 param.put("WAITING_TIME", 10);   // Waiting TIme은 문서 크기에 따라 자동 계산 한다.
	    	 param.put("CREATOR_EMAIL", this.getStringWithNullCheck(map,"EMAIL"));
	    	 param.put("EXE_RULE", "DB_LINK");
	    	 param.put("OUT_CL_CD", "4"); 
	    	 param.put("DOC_MSG", this.getStringWithNullCheck(map,"MESSAGE"));
	    	 param.put("USER_NM",  this.getStringWithNullCheck(map,"R_USER_NM"));
	    	 param.put("LOGIN_ID",  this.getStringWithNullCheck(map,"USER_ID"));
	    	 param.put("APPROVER_ID",  "");
	          	if("2".equals(pm.getString("component.site.product.version"))){
	          		 param.put("CRE_TYPE", this.getStringWithNullCheck(map,"CRE_TYPE"));//생성타입 E: EXE, D:CPD
 	          	}
	    
	    	 //-----------------------------------------------------------------------------
	    	 
	    	 String file_list = (String)param.get("FILE_LIST");
	    	 String file_path = (String)param.get("FILE_PATH");
	  
	    	 String[] file = file_list.split("\\|");
	    	 
	    	 //각 배열에 경로를 붙인다
	    	 for(int i=0;i<file.length;i++){
	    		  file[i] = file_path +"/" + file[i];
	    	 }
	    	 
	    	 file_list = "";
	    	 
	    	 //스트링배열을 하나의 스트링으로 합친다
	    	 for(int i=0;i<file.length;i++){
	    		 
	    		 if(i==file.length-1){
	    			 file_list = file_list + file[i];
	    		 }else{
	    			 file_list = file_list + file[i] + "|";
	    		 }
	    	 }
	    	 
	    	 param.put("FILE_LIST", file_list);
	    	 
	    	 //--------------------------------------------------------------------
	    	param.put("IN_CL_CD", "L"); 
	    	 log.info("CpDocInterface : insertDocMaster : " + param.toString());
	    	
	    	 sm.createObject("mg.cp.insertDocMaster", param); 
	    	 
	    	 return true;
    	 }	 catch(Exception e) {
    		 log.error("CpDocInterface : insertDocMaster Error 등록 실패 : " + e.getMessage());
    		 return false;
    	 }
     }
     
     // 수신자 등록
     // #DOC_NO#, 	ISNULL(MAX(SEQ) + 1, 1),		#COMPANY_ID#,		#USER_ID#,		#POLICY#,	#POLICY_DOC#,	#MOD_STOP#,#EXPIRE_DT#,#READ_COUNT#,	#MESSAGE#,#USER_NM#,#MAC_LIST#,GETDATE(), #LOGIN_ID#
     
     private boolean insertDocUser(Map map) 
     {	 
    	 try {
    		 log.info("insertDocUser 진입");
	    	 String user_id = (String)map.get("R_USER_ID");
	    	 String user_pw = (String)map.get("R_USER_PW");
    		 String user_nm = (String)map.get("R_USER_NM");
    		 String company_nm = (String)map.get("COMPANY_NM");
    		 String mac_list = (String)map.get("MAC_LIST");

    		 String[] userIds = user_id.split("\\|");
    		 String[] userPws = user_pw.split("\\|");
    		 String[] userNms = user_nm.split("\\|");
    		 String[] companyNms = company_nm.split("\\|");
    		 String[] mac_lists = mac_list.split("\\^"); // 수신자별 mac list 구분자는 ^ 사용
    		 
    		 /*for(int i=0;i<mac_lists.length;i++){
    			 log.info("userNms : " + userNms[i]);
    			 log.info("mac_list : " + mac_lists[i]);
    		 }*/
    		 
    		 
    		 log.info("userIds.length : " + userIds.length);
    		 
    		 for(int i=0; i< userIds.length; i++) {    		 
	    		 Map param = map;
	    		 param.put("USER_ID",  userIds[i]);
	    		 param.put("PASSWD",  userPws[i]);
	    		 param.put("COMPANY_NM",  companyNms[i]);
	    		 param.put("USER_NM",  userNms[i]);
	    		 param.put("EXE_RULE", "");
	    		 param.put("APPROVER_ID", "");
	    		 param.put("POLICY", this.getStringWithNullCheck(map,"IS_POLICY1")  + this.getStringWithNullCheck(map,"IS_POLICY2") + this.getStringWithNullCheck(map,"IS_POLICY3") + this.getStringWithNullCheck(map,"IS_POLICY4") + this.getStringWithNullCheck(map,"IS_POLICY5"));
		    	 param.put("POLICY_DOC", this.getStringWithNullCheck(map,"IS_POLICY_DOC1") +  this.getStringWithNullCheck(map,"IS_POLICY_DOC2")  + this.getStringWithNullCheck(map,"IS_POLICY_DOC3") + this.getStringWithNullCheck(map,"IS_POLICY_DOC4") + this.getStringWithNullCheck(map,"IS_POLICY_DOC5") );
		    	 param.put("LOGIN_ID",  this.getStringWithNullCheck(map,"USER_ID"));
		    	 param.put("COMPANY_ID", "001");
		    	 param.put("OUTDEVICE", "");
		    	 if(" ".equals(mac_lists[i])){
		    		 //log.info("MAC주소 없음");
		    		 param.put("MAC_LIST", "");
		    	 }else{
		    		 param.put("MAC_LIST", mac_lists[i]); 
		    	 }
		    	 
		    	 
		    	 log.info("CpDocInterface : insertDocUser : " + param.toString());
		    	 
		    	 param.put("dbName", pm.getString("component.sql.database"));
		    	 
		    	 sm.createObject("mg.cp.insertDocUser", param);
    		 }
	    	 
	    	 return true;
    	 }	 catch(Exception e) {
    		 log.error("CpDocInterface : insertDocUser Error : " + e.getMessage());
    		 return false;
    	 }
      }
     
     
     /*
      * CP Job 진행 상태를 Update 한다..
      * @param request
      * @return
      */
      private boolean setDocStatus()
      {
    	  
    	  
    	  return true;
      }

}
