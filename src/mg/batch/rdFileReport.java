package mg.batch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;

public class rdFileReport implements Job {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");

	public rdFileReport() {
		
	}
	
	/**
	* Cliptor-Plus Interface Program
	* by Cylim 
	*/
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		JobDataMap data = context.getJobDetail().getJobDataMap();
		
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String curDT = formatter.format(new Date());
			
        log.info("--------------------------------------------------------------------------------\nLOG Job Run !! - " + data.getString("fromEmail"));
                
        // if(data.getString("QUEUE_MODE").equals("CP_DOC_LINK")) {   /** 반출신청 현황조회 */
            // readDonLink();                           
        // }
        checkFileIO(data.getString("CHECKCOUNT"), curDT, data.getString("fromEmail"));
	}
    
    private String getStringWithNullCheck(Map map, String key) 
	{
	        Object result = map.get(key);
	        return (result == null) ? "" : result.toString();
	}
    
	private boolean checkFileIO(String checkCount, String curDT, String fromEMail)
	{
		try {
			
			Map param = new HashMap();
			param.put("ALERTCOUNT", Integer.parseInt(checkCount));
			param.put("CURDATE", curDT);
						
			List lists = sm.getList("mg.batch.getFileReport", param);
			String context="";
						
			if(lists.size() > 0) {
				for (int i = 0; i < lists.size(); i++) {
		          	Map map =(Map) lists.get(i);
		          	context += "<tr>";
		          	context += "<td>" +  this.getStringWithNullCheck(map,"DPTNAME") + "</td>";
		          	context += "<td>" +  this.getStringWithNullCheck(map,"USERNAME") + "</td>";
		          	context += "<td>" +  this.getStringWithNullCheck(map,"FILEACTION") + "</td>";
		          	context += "<td>" +  this.getStringWithNullCheck(map,"CNT")  + "</td>";
					context += "</tr>";
				}
			}
			
			String fileName = "config/scheduler/templet.html";
			String mailContext = readFile(fileName).replace("MAILBODY", context);
			
			log.info("--FROM EMAIL------------------>\n" + fromEMail);
			
			 IMail mail = mailHandle.createHtmlMail();
			 mail.setSubject("MyGuard File I/O Report");
	         mail.setFrom(fromEMail);

	         log.info("--component.smtp.hostName----------------->\n" + pm.getString("component.smtp.hostName"));
	         
	          // 메일 발송대상자를 가져온다.
	         lists = sm.getList("mg.batch.getSysAdmin", param);
	         for (int i = 0; i < lists.size(); i++) {
		          	Map map =(Map) lists.get(i);
		          	mail.addTo(this.getStringWithNullCheck(map,"EMAIL"));
		          	log.info( "--TO EMAIL------------------>\n" +   this.getStringWithNullCheck(map,"EMAIL"));
			 }
	         	         
	         mail.setHtmlMessage(mailContext);
	         // mail.setTextMessage(mailContext);
	         mail.send();
									
			return true;
		} catch(Exception e) {
			log.error("rdFileReport : checkFileIO Error : " + e.getMessage());
			return false;
		}
	}
	
	private String readFile(String filename)
	{
	   String content = null;
	   File file = new File(getClass().getClassLoader().getResource(filename).getFile()); //for ex foo.txt
	   try {
	       FileReader reader = new FileReader(file);
	       char[] chars = new char[(int) file.length()];
	       reader.read(chars);
	       content = new String(chars);
	       reader.close();
	   } catch (IOException e) {
	       e.printStackTrace();
	   }
	   return content;
	}

}
