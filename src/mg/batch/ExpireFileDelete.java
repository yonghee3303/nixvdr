package mg.batch;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.core.component.parameter.IParameterManagement;
import com.core.base.ComponentRegistry;
import com.core.component.sql.ISqlManagement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ExpireFileDelete  implements Job {

	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	
	private String getStringWithNullCheck(Map map, String key) 
	 {
	        Object result = map.get(key);
	        return (result == null) ? "" : result.toString();
	  }
	//UploadPath에 있는 기간이 지난 폴더들  return 
	private String uploadFolderList()
	{
		Map map = new HashMap();
		
		map.put("dbName", pm.getString("component.sql.database"));
		map.put("EXPIRE_DAY", pm.getInt("component.file.delete.allExpireDay"));
		
		String standardDay = (String)sm.getItem("mg.batch.expireFolder",map).toString();
		standardDay = standardDay.replace("-", "");
		
		// 해당 경로에 날짜별로 폴더가 생성된다. ex) 20160728
		String rootPath = pm.getString("component.upload.directory").replace("/", "\\"); 
		File folders = new File(rootPath);
		String FolderList[] = folders.list();
		String tempDeleteFolders ="";
		
		for(int i=0; i<FolderList.length; i++){
			//log.info("FolderList["+i+"]--------------------------"+FolderList[i]);
			if(Integer.parseInt(FolderList[i]) <= Integer.parseInt(standardDay)){
				if(tempDeleteFolders.equals("")) tempDeleteFolders= FolderList[i];
				else tempDeleteFolders += ","+FolderList[i];
			}
		}
		//log.info("uploadFolderList==================================tempDeleteFolders--------------------------------------------------------------"+tempDeleteFolders);
		
		return tempDeleteFolders;
	}
	
	//MgExePath에 있는 기간이 지난 폴더들  return 
		private String exeFolderList()
		{
			Map map = new HashMap();
			
			map.put("dbName", pm.getString("component.sql.database"));
  			map.put("EXPIRE_DAY", pm.getInt("component.file.delete.allExpireDay"));
			
			String standardDay = (String)sm.getItem("mg.batch.expireFolder",map).toString();
			standardDay = standardDay.replace("-", "");
			
			// 해당 경로에 날짜별로 폴더가 생성된다. ex) 20160728, R2016072
			String rootPath = pm.getString("component.upload.exedirectory").replace("/", "\\"); 
			File folders = new File(rootPath);
			String FolderList[] = folders.list();
			String tempDeleteFolders ="";
			
			for(int i=0; i<FolderList.length; i++){
				//log.info("FolderList["+i+"]--------------------------"+FolderList[i]);
				if(FolderList[i].indexOf("R") == -1){
					if(Integer.parseInt(FolderList[i]) <= Integer.parseInt(standardDay)){
						if(tempDeleteFolders.equals("")) tempDeleteFolders= FolderList[i];
						else tempDeleteFolders += ","+FolderList[i];
					}
				}else{
					if((Integer.parseInt(FolderList[i].substring(1, FolderList[i].length()))+1) <= Integer.parseInt(standardDay.substring(0, standardDay.length()-1))){
						if(tempDeleteFolders.equals("")) tempDeleteFolders= FolderList[i];
						else tempDeleteFolders += ","+FolderList[i];
					}
				}
			}
			//log.info("exeFolderList====================================tempDeleteFolders--------------------------------------------------------------"+tempDeleteFolders);
			
			return tempDeleteFolders;
		}
		
	//입력한 경로의 하위 파일 모두 삭제
	private void deleteSubFolder(String path){		
		File selectDir = new File(path);
		if(selectDir.isDirectory()){
			File[] innerFileList = selectDir.listFiles();
				
			for(File innerdir: innerFileList){
				if(innerdir.isDirectory()){
					deleteSubFolder(innerdir.getAbsolutePath());
				}else{
					if(innerdir.delete()){
						log.info("findSubFolder-sub------------------------------------------------"+innerdir.getAbsolutePath()+"삭제 성공");
					}else{
						log.info("findSubFolder-sub------------------------------------------------"+innerdir.getAbsolutePath()+"삭제 실패");
					}
				}
			}
			if(selectDir.delete()){
				log.info("findSubFolder----------------------------------------------------"+path+"삭제 성공");
			}else{
				log.info("findSubFolder----------------------------------------------------"+path+"삭제 실패");
			}
		}else{
			log.info("존재하지 않는 경로 입니다."+path);
		}
	}
	
	//입력한 경로의 하위에 파일 또는 폴더가 없으면 해당 폴더 삭제
	private boolean deleteFolder(String path){
		File selectDir = new File(path);
		boolean result = false;
		if(selectDir.isDirectory()){
			File[] innerFileList = selectDir.listFiles();
		 	if(innerFileList.length==0){
		 		result = selectDir.delete();
		 		if(result)log.info("하위 파일 및 폴더가 없는 폴더 삭제 실패");
		 	}
		}else{
			log.info("이미 삭제된 경로입니다."+path);
		}
		return result;
	}
	
	private void deleteUploadSubFolder(String uploadPath, String path){
		String[] tempPath = path.split("/"); //ex->   /20150201/mg/Upload.do/20150201143504830830
			if(deleteFolder(uploadPath+"\\"+tempPath[1]+"\\"+tempPath[2]+"\\"+tempPath[3])){
				if(deleteFolder(uploadPath+"\\"+tempPath[1]+"\\"+tempPath[2])){
					deleteFolder(uploadPath+"\\"+tempPath[1]);
				}	
			}//해당 날짜 폴더에 다른 파일들이 없으면 폴더 삭제\
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		if("DAY".equals(pm.getString("component.delete.directory.standard"))){
			log.info("ExpireFileDeleteListener-------------------------------------------------일정 기간이 지난 파일 삭제 ----시작");
		
			String uploadPath = pm.getString("component.upload.directory").replace("/", "\\");
			String exePath = pm.getString("component.upload.exedirectory").replace("/", "\\");	
			String uploadFolders = uploadFolderList();
			String exeFolders = exeFolderList();
				
			if(!"".equals(uploadFolders)){
				log.info("ExpireFileDeleteListener-------------------------------------------------uploadDeleteFolders------삭제 시작");
				String[] uploadDeleteFolders = uploadFolders.split(",");
				
				for(int i =0; i<uploadDeleteFolders.length; i++){
				//	log.info("ExpireFileDeleteListener-------------------------------------------------uploadDeleteFolders["+i+"] : "+uploadPath+"\\"+uploadDeleteFolders[i]);
					String targetPath = uploadPath+"\\"+uploadDeleteFolders[i];
					deleteSubFolder(targetPath);
				}
			}else{
				log.info("ExpireFileDeleteListener-------------------------------------------------uploadDeleteFolders------삭제할 원본 파일 폴더가 없습니다.");
			}
			
			if(!"".equals(exeFolders)){
				log.info("ExpireFileDeleteListener-------------------------------------------------exeDeleteFolders-------삭제 시작");
				String[] exeDeleteFolders = exeFolders.split(",");
				for(int i =0; i<exeDeleteFolders.length; i++){	
				//	log.info("ExpireFileDeleteListener-------------------------------------------------exeDeleteFolders["+i+"] : "+exePath+"\\"+exeDeleteFolders[i]);
					String targetPath = exePath+"\\"+exeDeleteFolders[i];
					deleteSubFolder(targetPath);
				}
			}else{
				log.info("ExpireFileDeleteListener-------------------------------------------------exeDeleteFolders------삭제할 보안 파일 폴더가 없습니다.");
			}
			log.info("ExpireFileDeleteListener-------------------------------------------------일정 기간이 지난 파일 삭제 ----완료");
		}else{
			
			log.info("ExpireFileDeleteListener-------------------------------------------------반출 만료일 "+ pm.getInt("component.file.delete.expireDay")+" 지난 파일 삭제 ----시작");	
			//문서번호,반출만료일, 삭제여부, 파일경로, 반출구분, 정보를 list로 뽑기
			Map map = new HashMap();
			map.put("dbName", pm.getString("component.sql.database"));
  			map.put("EXPIRE_DAY", pm.getInt("component.file.delete.expireDay"));
  			map.put("CP_STS_CD", "04");
  			List lists = sm.getList("mg.batch.selectExpireExportInfo", map);

  			for(int i=0; i<lists.size(); i++){
  				Map exportMap =(Map) lists.get(i);
  				if("4".equals(this.getStringWithNullCheck(exportMap, "AUTHORITY"))){
  					if("VFExport".equals(this.getStringWithNullCheck(exportMap, "PGM_ID")) || "regMgmt".equals(this.getStringWithNullCheck(exportMap, "PGM_ID"))){
  						log.info("사본파일 삭제");
  						String uploadPath = pm.getString("component.upload.directory").replace("/", "\\");
  	  					deleteSubFolder(uploadPath+this.getStringWithNullCheck(exportMap, "FILE_PATH").replace("/", "\\"));
  	  					deleteUploadSubFolder(uploadPath, this.getStringWithNullCheck(exportMap, "FILE_PATH"));
  	  					
	  	  				log.info("보안파일 삭제");
	  					String exePath = pm.getString("component.upload.exedirectory").replace("/", "\\")+"\\"+this.getStringWithNullCheck(exportMap, "DOC_NO").substring(0,8);
	  					deleteSubFolder(exePath+"\\"+this.getStringWithNullCheck(exportMap, "DOC_NO"));
	  					deleteFolder(exePath);//해당 날짜 폴더에 다른 파일들이 없으면 폴더 삭제
	  					
  					}
  				}else if("6".equals(this.getStringWithNullCheck(map, "AUTHORITY"))){
	  					log.info("사본파일 삭제");
	  					String uploadPath = pm.getString("component.upload.directory").replace("/", "\\");
	  	  				deleteSubFolder(uploadPath+this.getStringWithNullCheck(exportMap, "FILE_PATH").replace("/", "\\"));
	  	  				deleteUploadSubFolder(uploadPath, this.getStringWithNullCheck(exportMap, "FILE_PATH"));
  				}
  				sm.updateObject("mg.batch.updateFileDelYn", exportMap);	
  			}//for
			
  			log.info("ExpireFileDeleteListener-------------------------------------------------신청,진행,오류,실패,반려 상태의 반출만료일로 부터 +"+ pm.getInt("component.file.delete.expireDay")+" 지난 파일 삭제 ----시작");	
  			map.put("CP_STS_CD", "ALL"); //대기,완료 외의 항목들 (신청,진행,오류,실패,반려)
  			map.put("EXPIRE_DAY", pm.getInt("component.file.delete.allExpireDay"));
  			lists = sm.getList("mg.batch.selectExpireExportInfo", map);
  			for(int i=0; i<lists.size(); i++){
  				Map allMap =(Map) lists.get(i);
  				if("4".equals(this.getStringWithNullCheck(allMap, "AUTHORITY"))){
  					if("VFExport".equals(this.getStringWithNullCheck(allMap, "PGM_ID")) || "regMgmt".equals(this.getStringWithNullCheck(allMap, "PGM_ID"))){
  						log.info("사본파일 삭제");
  						String uploadPath = pm.getString("component.upload.directory").replace("/", "\\");
  	  					deleteSubFolder(uploadPath+this.getStringWithNullCheck(allMap, "FILE_PATH").replace("/", "\\"));
  	  					deleteUploadSubFolder(uploadPath, this.getStringWithNullCheck(allMap, "FILE_PATH"));
  	  					
	  	  				log.info("보안파일 삭제");
	  					String exePath = pm.getString("component.upload.exedirectory").replace("/", "\\")+"\\"+this.getStringWithNullCheck(allMap, "DOC_NO").substring(0,8);
	  					deleteSubFolder(exePath+"\\"+this.getStringWithNullCheck(allMap, "DOC_NO"));
	  					deleteFolder(exePath);//해당 날짜 폴더에 다른 파일들이 없으면 폴더 삭제
  					}			
  				}else if("6".equals(this.getStringWithNullCheck(allMap, "AUTHORITY"))){
  					log.info("사본파일 삭제");
  					String uploadPath = pm.getString("component.upload.directory").replace("/", "\\");
  	  				deleteSubFolder(uploadPath+this.getStringWithNullCheck(allMap, "FILE_PATH").replace("/", "\\"));
  	  				deleteUploadSubFolder(uploadPath, this.getStringWithNullCheck(allMap, "FILE_PATH"));
  				}
  				sm.updateObject("mg.batch.updateFileDelYn", allMap);	
  			}//for
			
  			log.info("ExpireFileDeleteListener-------------------------------------------------업로드만 한 파일 삭제 : "+map.toString());	
			//3. 업로드만한 파일 : 당일 업로드 한 것은 그 다음날 지울 수 있도록 한다. 혹여 배치 작업 중 업로드되는 파일을 지울 수 있기 때문
  			lists = sm.getList("mg.batch.uselessUploadFile", map);
  			for(int i=0; i<lists.size(); i++){
  				Map uploadMap =(Map) lists.get(i);	
  				String uploadPath = pm.getString("component.upload.directory").replace("/", "\\");
  				String today = (String)sm.getItem("mg.batch.SysdateForFileDelete",map).toString().replace("-", "");
  				String[] tempPath=this.getStringWithNullCheck(uploadMap, "FILE_PATH").split("/");
  				
  				if(Integer.parseInt(tempPath[1]) < Integer.parseInt(today)){
		  			deleteSubFolder(uploadPath+this.getStringWithNullCheck(uploadMap, "FILE_PATH").replace("/", "\\")); 
		  			deleteUploadSubFolder(uploadPath, this.getStringWithNullCheck(uploadMap, "FILE_PATH"));
		  			uploadMap.put("DOC_NO", this.getStringWithNullCheck(uploadMap, "DOC_ID"));
		  			sm.updateObject("mg.batch.updateFileDelYn", uploadMap);
  				}//if
  			}//for
		}//else
	}
}


