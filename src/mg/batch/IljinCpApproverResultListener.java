package mg.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;

public class IljinCpApproverResultListener  implements Job {

	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	
	private String getStringWithNullCheck(Map map, String key) 
	 {
	        Object result = map.get(key);
	        return (result == null) ? null : result.toString();
	  }
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
	
		String dbName = pm.getString("component.sql.database");
		Map map = new HashMap();
		String docNo="";
		
		/*
		 * 
		 SELECT SN, STATUS FROM IF_DOC@IERP
			WHERE DOC_TYPE = '108'
				AND SN IN (SELECT DOC_NO FROM CP_JOB_SCHEDULE WHERE CP_STS_CD = '01')
				AND (STATUS = '004' OR STATUS = '008')
		 */
		
		try {
			log.info("결재 승인/반려 문서 조회---------------------------------------------------------");
			List lists = sm.getList("mg.batch.getIljinApproveDoc", null);
			String status = null;
			String approvalId = null;
			
			if(lists.size()==0){
				log.info("-------------------------------------읽어올 문서가 없습니다.");
				return;
			} 
			for(int i=0; i<lists.size(); i++) {
				map = (Map)lists.get(i);
				docNo = map.get("DOC_NO").toString();
				status = map.get("STATUS").toString();
				if(!"004".equals(status)) {	//반려가 아닐 경우 (반려일 경우 승인자 정보는 null값이 넘어옴
					approvalId = map.get("APPROVER_ID").toString();					
				}
								
				log.info("----------------------------------------------------------------------DocNo"+docNo);
				//문서번호로 쿼리를 해서 보안반출인지, 원문반출인지 확인, 승인처리를 해야될지 말지도....
				Map authMap;
				//log.info("dbName--------------------"+dbName);
				if(dbName.equals("mssql")){
					authMap = (Map)sm.getItem("mg.cp.getCpDocInfo",map);
				}else{
					authMap = (Map)sm.getItem("mg.cp.getCpDocInfo_"+dbName, map);
				}
				String authority = this.getStringWithNullCheck(authMap,"AUTHORITY");
				
				log.info("----------------------------------------------------------------------authority : "+ authority);
				
				String approvalYn="";
				if("008".equals(status)) {	//승인
					approvalYn="Y";
					if("6".equals(authority)) { 
						//원문반출일 경우 04로 변경,
		   	 		    map.put("CP_STS_CD", "04");  // 승인시 상태 코드를 04 : 완료 상태로 만든다.
		   	 		    log.info("승인일 경우--------------------------------------------------------------CP_STS_CD=04 \t원문반출"+authority);
			 		}else{
			 			//보안반출일 경우 02로 변경
						map.put("CP_STS_CD", "02");
						log.info("승인일 경우--------------------------------------------------------------CP_STS_CD=02 \t보안반출"+authority);
			 		}
					
					//승인자 등록
					String approvalMsg = "";
					map.put("APPROVAL_MSG", approvalMsg);
					map.put("APPROVER_ID", approvalId);
		   	 		sm.updateObject("mg.cp.updateDocApprover", map);
		   	 		
				} else if("004".equals(status))	{	//반려
					approvalYn="N";
					map.put("CP_STS_CD", "07");
					log.info("반려일 경우--------------------------------------------------------------CP_STS_CD=07");
				}

				// 문서상태 변경
				sm.updateObject("mg.cp.updateSTS", map); 
			}
		} catch(Exception e){
			log.info("문서번호 :"+docNo+"---------------결재 문서 읽어오는 중 오류-------------------------------------------------------"+e.toString());
		}//try - catch
	}	
}
