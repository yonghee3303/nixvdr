package mg.batch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.core.base.ComponentRegistry;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.component.message.IMessageManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;

public class CpDocExecution   implements Job {
	private IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
    
	static public int conFlag=0;
	
	public CpDocExecution() {
		
	}
	
	public class ConnectInfo {
		String ip;
		int port;
		int process_cnt;
		int exe_process;
		int cpu;
		int usage_cnt;
		int usage_server_idx;
	}
	
	/**
	* Cliptor-Plus Interface Program
	* by Cylim 
	*/
	public void execute(JobExecutionContext context) throws JobExecutionException {	
		
		JobDataMap data = context.getJobDetail().getJobDataMap();
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String curDt = formatter.format(new Date());
			
        log.info("--->\n Runnig Road Banencing Server !!");
       	runLoadBalencingServer();                           
	}
	
	private String getStringWithNullCheck(Map map, String key) 
	 {
	        Object result = map.get(key);
	        return (result == null) ? null : result.toString();
	  }
	
	
	/*
	 * Modified by cyLeem  2015/12/31
	 *   Error of LoadBalencing
	 */
	private void runLoadBalencingServer()
	{
		String errorIp=""; //오류 시 메일 발송에 첨부할 서버 IP
		try {
				Map agentMap = new HashMap();
				
				List lists;
	 			String dbName = pm.getString("component.sql.database");
		       	 if("mssql".equals(dbName)) {
		       		lists = sm.getList("mg.cp.getAgentList", agentMap);
		       	 } else {
		       		lists = sm.getList("mg.cp.getAgentList_" + dbName, agentMap);
		       	 }
		       	Collections.shuffle(lists);  // 
				
		       	// 가용한 서버 IP, Port를 가져오는 함수입니다.
		       	ConnectInfo conInfo = getUsageServer(lists);
 	    
		 	    // 실행명령 --> EXEsche_no
		 	    if(conInfo != null && conInfo.exe_process  > 0) {  // 실행 가능한 서버 Agent가 존재 하면
		 	    	log.info("ROWNUM : " + conInfo.exe_process);
		 	    	
		 	    	Map inmap = new HashMap();
		 	    	inmap.put("ROWNUM", conInfo.exe_process);
		 	    	
		 	    	// 실행할 스케쥴을 가져 온다.
		 	    	
		 	    	List exeList;
		 	    	
		 	    	 if("mssql".equals(dbName)) {
		 	    		exeList = sm.getList("mg.cp.getExeSchedule", inmap);
			       	 } else {
			       		exeList = sm.getList("mg.cp.getExeSchedule_" + dbName, inmap);
			       	 }
		 	    	
		 	    	
		 	    	for (int j = 0; j < exeList.size(); j++) {
		 	    		if(conInfo.exe_process > j) {
		 	    			sm.begin();
			 	          	Map schemap =(Map) exeList.get(j);
		 	          		log.info("가용 프로세스 : "+conInfo.exe_process+" / Index : "+j+" / IP : "+conInfo.ip+" / Port : "+conInfo.port);
			 	          	if(this.getStringWithNullCheck(schemap,"WORK_TYPE").equals("EXEC")){
			 	          		//CP 생성 스케쥴
			 	          		log.info("getExeSchedule : " + "EXEC " + this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          	
				 	          	if(execCpAgent(conInfo.ip, conInfo.port, "EXEC" + this.getStringWithNullCheck(schemap,"SCHE_NO"), schemap) ) {
				 	          		// 실행 성공이면 스케쥴 상태 변경
				 	          		Map smap = new HashMap();
				 	          		smap.put("sche_no",this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          		smap.put("cp_sts_cd", "03");
				 	          		smap.put("job_desc", "");
				 	          		
				 	          		sm.updateObject("mg.cp.setSchedule", smap);	
				 	          		sm.updateObject("mg.cp.setStatus", smap);
				 	          		log.info("------------------------------------스케쥴 상태 변경 완료");
				 	          	}
				 	          	
			 	          	}else{
			 	          		//Repacking 해제 스케쥴
			 	          		log.info("getExeSchedule : " + "REPU " + this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          	// 실행 서버를 찾아 온다
				 	          	int exe_index=0;
				 	          	
				 	          	if(execCpAgent(conInfo.ip, conInfo.port, "REPU" + this.getStringWithNullCheck(schemap,"SCHE_NO"), schemap) ) {
				 	          		// 스케쥴 전달 성공이면 스케쥴 상태 변경 - 진행
				 	          		Map smap = new HashMap();
				 	          		smap.put("sche_no",this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          		smap.put("cp_sts_cd", "03");
				 	          		smap.put("job_desc", "");
				 	          		
				 	          		sm.updateObject("mg.cp.setRepackingSchedule", smap);
				 	          		sm.updateObject("mg.cp.setStatus", smap);
				 	          	}
			 	          	}
			 	           sm.commit();
		 	    		} // ---- END.
		 	    		
		 	    	} // END For
		 	    }
	      }catch(Exception e) {
	    	  	sm.commit();
		    	 log.error("--runRoadBalencingServer-------------------------------------------------------------\n" + e.toString());
		    	
	      }
	}
	
	private ConnectInfo getUsageServer(List lists)
	{
		String errorIp=""; //오류 시 메일 발송에 첨부할 서버 IP
		try {
			ConnectInfo conInfo = new ConnectInfo();
			conInfo.exe_process=0;
			conInfo.usage_server_idx = -1;
			ConnectInfo resInfo = null;
			
			for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	conInfo.ip =  this.getStringWithNullCheck(map, "IP_ADDR"); //서버IP
	          	conInfo.port = Integer.parseInt(map.get("PORT").toString()); //서버Port
	          	conInfo.process_cnt = Integer.parseInt(map.get("PROCESS_CNT").toString()); //최대 프로세스 갯수
				String server_msg = "";
				 
	            // 소켓을 생성하여 연결을 요청한다.
				log.info("서버에 연결중입니다. 서버IP : " + conInfo.ip + ", Port : " + conInfo.port);
				errorIp=conInfo.ip;
	            Socket socket = new Socket(conInfo.ip, conInfo.port);
	            
	            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	 			// 서버에 데이터를 송신
	 			OutputStream out = socket.getOutputStream();
	 			// 서버에 데이터 송신
	 			out.write("GETDATA".getBytes());
	 			out.flush();
	 					   
	 			// 소켓의 입력스트림을 얻는다.
	 			server_msg = in.readLine().replaceAll("\\#!=", "^");
	 			// GETDATA000#!=1#!=
	            log.info("서버로부터 받은 메세지 : " + server_msg);
	            
	            String[] s_msgs = server_msg.replace("GETDATA", "").split("\\^");
	
	            conInfo.cpu = Integer.parseInt(s_msgs[0]);
	            conInfo.usage_cnt = Integer.parseInt(s_msgs[1]); //현재 실행 중인 프로세스 
	            
	            log.info(" usage_cnt[i] : " +  conInfo.usage_cnt);
	            
	            // server_cnt = i;     
	            
	            in.close();
				out.close();
	            socket.close();
	            		         
	            // 실행 가능한 서버 Agnet Job 도출
	            if(conInfo.process_cnt - conInfo.usage_cnt > 0 && conInfo.usage_server_idx < 0) {
	            	conInfo.exe_process = conInfo.process_cnt - conInfo.usage_cnt;
	            	conInfo.usage_server_idx = i;
	            	
	            	resInfo = new ConnectInfo();
	            	resInfo.exe_process = conInfo.exe_process;
	            	resInfo.ip = conInfo.ip;
	            	resInfo.port = conInfo.port;
	            	resInfo.usage_cnt = conInfo.usage_cnt;
	            }
	            
	            // 서버 상태 로그 기록
		        setServerAgentLog(map, conInfo.usage_cnt, conInfo.cpu, "01", "");
	 	    }
			return resInfo;
		  }catch(ConnectException ce){
	    	  	 log.error("--runRoadBalencingServer-------------------------------------------------------------\nLBClient가 동작을 하지 않고 있거나, IP가 잘못되었습니다. \n" + ce.toString());
	    	  	 if(conFlag==0) sendErrorMail(errorIp);
	      }catch(Exception e) {
		    	 log.error("--runRoadBalencingServer-------------------------------------------------------------\n" + e.toString());
	      }
		  return null;
	}
	
	
/*
	private void runLoadBalencingServer()
	{
		String errorIp=""; //오류 시 메일 발송에 첨부할 서버 IP
		String exeIp="";
		int exePort=23651;
		try {
				Map agentMap = new HashMap();
				
				List lists;
	 			String dbName = pm.getString("component.sql.database");
		       	 if("mssql".equals(dbName)) {
		       		lists = sm.getList("mg.cp.getAgentList", agentMap);
		       	 } else {
		       		lists = sm.getList("mg.cp.getAgentList_" + dbName, agentMap);
		       	 }
				
				
				String ip[] = new String[lists.size()];      // 서버당 접속 IP Address
				int port[] = new int[lists.size()];			  // 서버당 접속 Port
				int process_cnt[] = new int[lists.size()];  // 서버당 최대 실행 Agent 수
				int usage_cnt[] = new int[lists.size()];    // 서버당 가용 Agent 수
				int cpu[] =  new int[lists.size()];  		  // 서버당 CPU 점유율
				int server_cnt=0;    							  // 서버 개수
				int exe_process = 0;
				
		 	    for (int i = 0; i < lists.size(); i++) {
	 	          	Map map =(Map) lists.get(i);
	 	          	
	 	          	ip[i] =  this.getStringWithNullCheck(map, "IP_ADDR");
	 	          	port[i] = Integer.parseInt(map.get("PORT").toString());
	 	          	process_cnt[i] = Integer.parseInt(map.get("PROCESS_CNT").toString());
					String server_msg = "";
					 
		            // 소켓을 생성하여 연결을 요청한다.
					log.info("서버에 연결중입니다. 서버IP : " + ip[i] + ", Port : " + port[i]);
					errorIp=ip[i];
		            Socket socket = new Socket(ip[i], port[i]);
		            
		            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		 			// 서버에 데이터를 송신
		 			OutputStream out = socket.getOutputStream();
		 			// 서버에 데이터 송신
		 			out.write("GETDATA".getBytes());
		 			out.flush();
		 					   
		 			// 소켓의 입력스트림을 얻는다.
		 			server_msg = in.readLine().replaceAll("\\#!=", "^");
		 			// GETDATA000#!=1#!=
		            log.info("서버로부터 받은 메세지 : " + server_msg);
		            
		            String[] s_msgs = server_msg.replace("GETDATA", "").split("\\^");

		            cpu[i] = Integer.parseInt(s_msgs[0]);
		            usage_cnt[i] = Integer.parseInt(s_msgs[1]);
		            
		            log.info(" usage_cnt["+i+"] : " +  usage_cnt[i]);
		            
		            server_cnt = i;     
		            
		            in.close();
					out.close();
		            socket.close();
		            		         
		            // 실행 가능한 서버 Agnet Job 도출
		            if(process_cnt[i] - usage_cnt[i] > 0) {
		            	exe_process = process_cnt[i] - usage_cnt[i];
			            if(exe_process>0){
			            	exeIp=ip[i];
			            	exePort=port[i];
			            	log.info("---------------------------------------실행 가능한 서버 IP/Port : "+exeIp+" / "+exePort);
			            }
		            }
		            
		            // 서버 상태 로그 기록
		           
			        setServerAgentLog(map, usage_cnt[i], cpu[i], "01", "");
		 	    }
		 	    log.info("ROWNUM : " + exe_process);
		 	    		 	    
		 	    // 실행명령 --> EXEsche_no
		 	    if(exe_process > 0) {  // 실행 가능한 서버 Agent가 존재 하면
		 	    	
		 	    	Map inmap = new HashMap();
		 	    	inmap.put("ROWNUM", exe_process);
		 	    	
		 	    	// 실행할 스케쥴을 가져 온다.
		 	    	
		 	    	List exeList;
		 	    	
		 	    	 if("mssql".equals(dbName)) {
		 	    		exeList = sm.getList("mg.cp.getExeSchedule", inmap);
			       	 } else {
			       		exeList = sm.getList("mg.cp.getExeSchedule_" + dbName, inmap);
			       	 }
		 	    	
		 	    	
		 	    	for (int j = 0; j < exeList.size(); j++) {
		 	    		if(exe_process >= j) {
		 	    			 sm.begin();
			 	          	Map schemap =(Map) exeList.get(j);
			 	          	
			 	          	if(this.getStringWithNullCheck(schemap,"WORK_TYPE").equals("EXEC")){
			 	          		//CP 생성 스케쥴
			 	          		log.info("getExeSchedule : " + "EXEC " + this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          	// 실행 서버를 찾아 온다
				 	          	int exe_index=0;
				 	          	
				 	          	if(execCpAgent(exeIp, exePort, "EXEC" + this.getStringWithNullCheck(schemap,"SCHE_NO"), schemap) ) {
				 	          		// 실행 성공이면 스케쥴 상태 변경
				 	          		Map smap = new HashMap();
				 	          		smap.put("sche_no",this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          		smap.put("cp_sts_cd", "03");
				 	          		smap.put("job_desc", "");
				 	          		
				 	          		sm.updateObject("mg.cp.setSchedule", smap);	
				 	          		sm.updateObject("mg.cp.setStatus", smap);
				 	          		log.info("------------------------------------스케쥴 상태 변경 완료");
				 	          	}
				 	          	
			 	          	}else{
			 	          		//Repacking 해제 스케쥴
			 	          		log.info("getExeSchedule : " + "REPU " + this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          	// 실행 서버를 찾아 온다
				 	          	int exe_index=0;
				 	          	
				 	          	if(execCpAgent(ip[exe_index], port[exe_index], "REPU" + this.getStringWithNullCheck(schemap,"SCHE_NO"), schemap) ) {
				 	          		// 스케쥴 전달 성공이면 스케쥴 상태 변경 - 진행
				 	          		Map smap = new HashMap();
				 	          		smap.put("sche_no",this.getStringWithNullCheck(schemap,"SCHE_NO"));
				 	          		smap.put("cp_sts_cd", "03");
				 	          		smap.put("job_desc", "");
				 	          		
				 	          		sm.updateObject("mg.cp.setRepackingSchedule", smap);
				 	          		sm.updateObject("mg.cp.setStatus", smap);
				 	          	}
			 	          	}
			 	           sm.commit();
		 	    		} // ---- END.
		 	    		
		 	    	} // END For
		 	    }
		 	     conFlag=0;
	      }catch(ConnectException ce){
	    	  	 log.error("--runRoadBalencingServer-------------------------------------------------------------\nLBClient가 동작을 하지 않고 있거나, IP가 잘못되었습니다. \n" + ce.toString());
	    	  	 if(conFlag==0) sendErrorMail(errorIp);
	      }catch(Exception e) {
		    	 log.error("--runRoadBalencingServer-------------------------------------------------------------\n" + e.toString());
		    	// sm.commit();
	      }
	}
	*/
	
	private void sendErrorMail(String errorIp){
		
			try{
       			String[] managers = pm.getString("cliptorplus.mail.manager.mailaddress").split("\\|");
       			//log.info("메일보낼 수신자 수 : " + mailAddr.size());
    			
    			for(int i=0;i<managers.length;i++) {			    				
	       			IMail mail = mailHandle.createHtmlMail();
	       			log.info("오류 시--------------------------------------메일 수신자 :" + managers[i]);
	       			mail.addTo(managers[i]);
	       			mail.setFrom(managers[i]);
	    	    	//mail.setSubject(mm.getMessage("MAIL_0021"));
	       		    mail.setSubject(mm.getMessage("MAIL_0091"));//보안파일 생성 중 오류가 발생
	    	          // 메일 템플릿을 읽어 온다.
	    	 		 String fileName = "config/templates/mail_Template.html";
	    	 		 String mailContext = readFile(fileName);
	    	 		
	    	 		 mailContext = mailContext.replace("SYSTEMNAME", mm.getMessage("MAIL_0091"));
	    			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0092"));
	    			 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.url"));
	    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));
	    			 
	    			 mailContext = mailContext.replace("MAIL_CONTENT", errorIp+mm.getMessage("MAIL_0093"));
	    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_0094"));
					 mailContext = mailContext.replace("COMP_NM", errorIp);
					 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_0095"));
					 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			   		 String curDt = formatter.format(new Date());
					 mailContext = mailContext.replace("USER_NM", curDt);
					 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_0096"));
					 Object[] params = {errorIp};
					 mailContext = mailContext.replace("REQ_DT", mm.getMessage("MAIL_0073", params));
					 mailContext = mailContext.replace("LINK", "");
					 mailContext = mailContext.replace("BUTTONTEXT", "");
					 
	    			 // 메일 발송
	    			 mail.setHtmlMessage(mailContext);
	    	         mail.send();					    	         
    			}
    			conFlag=1;
    		}catch(Exception e){
    			log.info(mm.getMessage("MAIL_0098"));
    		}

	}
	 private String readFile(String filename)
		{   	
	    	String content = null;
	 	   File file = new File(getClass().getClassLoader().getResource(filename).getFile()); //for ex foo.txt
	 	   try {
	 	       //FileReader reader = new FileReader(file); //한글깨짐
	 		   InputStreamReader reader= new InputStreamReader(new FileInputStream(file),"UTF-8"); //한글깨지지않음
	 		   char[] chars = new char[(int) file.length()];
	 	       reader.read(chars);
	 	       content = new String(chars);
	 	       reader.close();
	 	       log.info("------------------------------readFile");
	 	   } catch (IOException e) {
	 		   log.error("-----------------------------readFile Exception-----"+e.toString());
	 	       e.printStackTrace();
	 	   }
	 	   return content;
		}
	 private boolean execCpAgent(String ip, int port, String sche_no, Map map )
	 {
		 String server_msg="";
		 try {
			 	
			    log.info("서버에 연결중입니다. 서버IP : " + ip + ", Port : " + port+ ", ScheduleNo : "+sche_no);
	            Socket socket = new Socket(ip, port);

	             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	 			 // 서버에 데이터를 송신
	 		 	 OutputStream out = socket.getOutputStream();
	 			boolean checkState =false;
	 			map.put("CP_STS_CD", "02");    // 최종 작업 요청전 상태를 체크 한다.
	 			log.info("execCpAgent----------------------------------1");
	 			if("REPU".equals((sche_no.substring(0, 4)))){
 					checkState = Integer.parseInt(sm.getItem("mg.batch.checkRepSchedule",map).toString())>0;
	 			}else{
 					checkState = Integer.parseInt(sm.getItem("mg.batch.checkSchedule",map).toString())>0;
	 			}
				if( checkState ) {

		 			// 서버에 데이터 송신
		 			out.write(sche_no.getBytes());
		 			out.flush();
		 			log.info("execCpAgent----------------------------------2");		   
		 			// 소켓의 입력스트림을 얻는다.
		 			server_msg = in.readLine();
		 			// GETDATA000#!=1#!=
		            log.info("서버로부터 받은 메세지 : " + server_msg);
		            
		            in.close();
					out.close();
		            socket.close();
				}
				log.info("execCpAgent----------------------------------3");
	            if("OK".equals(server_msg.substring(0, 2))) {
	            	return true;
	            } else {
	            	 log.error("-----execCpAgent : returned Server Message. ----------------------------------------------------------\n" );
	            	return false;
	            }
		 } catch(Exception ex) {
			   log.error("-----execCpAgent :----------------------------------------------------------\n" + ex.toString());
	    	   return false;
		 }
	 }
	 
	 private int getExeAgent(String[] cpu, String[] process_cnt, String[] usage_cnt)
	 {	 
		 return 1;
	 }

	 
	 // 서버 상태 로그 기록
 	 // (#AGENT_NO#, TO_CHAR(SYSDATE, 'YYYYMMDD'), TO_CHAR(SYSDATE, 'HH24MISS'), #PROCESS#, #CPU#, #AGENT_STS_CD#, #AGENT_MSG#)
	 private boolean setServerAgentLog(Map map, int process, int cpu, String status_cd, String msg)
	 {
		 try {
			 	/*log.info("Map : " + map.toString());
			 	log.info("Process : " + process);
			 	log.info("cpu : " + cpu);
			 	log.info("status_cd : " + status_cd);
			 	log.info("msg : " + msg);*/
			 	log.info("------------------------------------setServerAgentLog 진입");
			 	
			    map.put("PROCESS", process);
	            map.put("CPU", cpu);
	            map.put("AGENT_STS_CD", status_cd);
	            map.put("AGENT_MSG", msg);
	            sm.begin();
	            String dbName = pm.getString("component.sql.database");
	            
	            if("mssql".equals(dbName)) {
	            	sm.createObject("mg.cp.saveServerAgent", map); //함수 호출 전에 begin()
		       	 } else {
		       		sm.createObject("mg.cp.saveServerAgent_" + dbName, map); 
		       	 }
	            log.info("------------------------------------setServerAgentLog insert 후");			 	
	           sm.commit();
		        return true;
		 } catch(Exception ex) {
			 	sm.rollback();
			 	log.error("-------------setServerAgentLog-----------\n" + ex.getMessage());	
			 	return false;
		 }
	 }
	
}
