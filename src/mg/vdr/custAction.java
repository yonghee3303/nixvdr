package mg.vdr;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;

import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import com.core.component.util.DateUtils;


public class custAction extends BaseAction {
	
	
	public static final int VDR_CUST_ID_DIGIT = 5;
    /**
     * VDR 거래처 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("select")) {   /** 조회 */
            return selectCust(request);                                                
        }else if(request.isMode("save")) {   /** 저장 */
            return saveCust(request);                           
        }else if(request.isMode("delete")) {   /** 삭제        */
            return deleteCust(request);                           
        }else { 
            return write(null);
        }
    }
    
    /**
     * 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectCust(IRequest request) throws Exception 
     {
     	  JSONObject obj = new JSONObject();
 		  String col = request.getStringParam("grid_col_id");
 	 	  String[] cols = col.split(",");
 	 	  
 	 	  Map map = null;
 	      GridData gdRes = new GridData();
 	 	
 	 	  try {
 	 		  Map smap = request.getMap();
 	 		  smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC) );
 	 		   	 		  
 	 		  String dbName = pm.getString("component.sql.database");
 	 		  List lists = getList("mg.vdr.selectCust"+getDBName(dbName), smap);
 	 		  
 	 		  JSONArray jArray = new JSONArray();
 	    
 	          for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	JSONObject listdata = new JSONObject();
 	          	
 	          	listdata.put("ROW_ID", String.valueOf(i+1));
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	          	log.info("------selectSample-=----listdata------ > " + listdata.toString());
 	          	
 	          	jArray.add(listdata);
 	         }
 	         
 	         obj.put("data", jArray);
 			 obj.put("errmsg", "success");
 			 obj.put("errcode", "0");
 			 log.info("------selectSample-=----result------ > " + obj.toString());
 			return write(obj.toString());
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", e.toString());
 			 obj.put("errcode", "-1");
 	    	// return write(obj.toString());
 	    	  return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
 	     }
     } 
     
     /**
      * 저장
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse saveCust(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);

         JSONArray jArray = new JSONArray();
         String dbName = pm.getString("component.sql.database");
	      System.out.println(dbName);
         try {
        	 tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	    item = new HashMap();
 	       	JSONObject listdata_save = new JSONObject();
 	       	listdata_save.put("ROW_ID", String.valueOf(i+1));
 	       	 	for(int j=0; j < cols.length; j++ ) {
 	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		             item.put(cols[j], tmps);     
 		           listdata_save.put(cols[j], tmps );
 	         	}
 	       	 	item.put("LOGIN_ID",  user_id);
 	      	    item.put("BILL_TYPE_CD", "");
 	       	 	 	       	    
 		      	if( Integer.parseInt(getItem("mg.vdr.countCust", item).toString() ) > 0 ) 
 		       	{
 		      		log.info("기존거래처 업데이트");
 		       		updateObject("mg.vdr.updateCust"+getDBName(dbName), item);
 		       		System.out.println("update");
 		       	} else 
 		       	{ 		       	
	 		    	int count = Integer.parseInt(getItem("mg.vdr.countMaxCust",item).toString()) + 1; 
	 		       	String companyId = "C" + DateUtils.getCurrentDate("yyyyMMdd") + makeVdrCustCode(); //(String.format("00000",count);
	 		        item.put("CUST_ID", companyId);
 		       		
	 		        createObject("mg.vdr.insertCust"+getDBName(dbName), item);
 		       	} 
 		      	log.info("----------------saveCust--------------------item---------------------\n" + item.toString());
 		  	 jArray.add(listdata_save);
 	        }
 	       tx.commit();
            obj.put("data", jArray);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * 삭제 	
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse deleteCust(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);

         
         try {
 	        Map item = null;
 	       
 	   	
 	        for (int i = 0; i < ids.length; i++) {
 	       	 	item = new HashMap();
 	         	
 	       	 	for(int j=0; j < cols.length; j++ ) {
 	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		             item.put(cols[j], tmps);     
 		           
 	         	}
 	       	 	deleteObject("mg.vdr.deleteCust", item);
 	        }
 	        obj.put("errmsg", "success");
			obj.put("errcode", "0");
	        return write(obj.toString());
         } catch(Exception e) {
            log.error("---------------------------------------------------------------\n" + e.toString());
            obj.put("errmsg", mm.getMessage("COMG_1008", loc));
			obj.put("errcode", "-1");   
            return write(obj.toString());
         }
     }
     
	private String makeVdrCustCode() {
		long seedL = System.currentTimeMillis();
		System.out.println(seedL);
		Random generator = new Random(seedL);
		int rndVal = generator.nextInt();
		System.out.println(rndVal);
		int digit = VDR_CUST_ID_DIGIT;
		String cdIdx = (new Long(seedL)).toString() + (new Long(rndVal)).toString();
		if (cdIdx.length() > digit ) {
			int endIdx = cdIdx.length() - 1;
			cdIdx = cdIdx.substring(endIdx - digit, endIdx);
		}

		return cdIdx;

	}
}
