package mg.vdr;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class sampleAction extends BaseAction {
    /**
     * VDR 예저 프로그램
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("select")) {   /** 조회 */
            return selectSample(request);                                                
        }else if(request.isMode("save")) {   /** 저장 */
            return saveSample(request);                           
        }else if(request.isMode("delete")) {   /** 삭제        */
            return deleteSample(request);                           
        }else { 
            return write(null);
        }
    }
    
    /**
    * 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectSample(IRequest request) throws Exception 
    {
    	  JSONObject obj = new JSONObject();
		  String col = request.getStringParam("grid_col_id");
	 	  String[] cols = col.split(",");
	 	  
	 	  Map map = null;
	      GridData gdRes = new GridData();
	      String dbName = pm.getString("component.sql.database");
	 	  try {
	 		  Map smap = request.getMap();
	 		  smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC) );
	 		  
	 		  // log.info("------selectSample-=----smap------ > " + smap.toString());
	 		  
	 		  // String dbName = pm.getString("component.sql.database");
	 		  // List lists = getList("mg.vdr.selectSample"+getDBName(dbName),smap);
	 		  
	 		  List lists = getList("mg.vdr.selectSample"+getDBName(dbName), smap);
	 		  JSONArray jArray = new JSONArray();
	    
	          for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	JSONObject listdata = new JSONObject();
	          	
	          	listdata.put("ROW_ID", String.valueOf(i+1));
	          	for(int j=0; j < cols.length; j++ ) {
	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	          	// log.info("------selectSample-=----listdata------ > " + listdata.toString());
	          	
	          	jArray.add(listdata);
	         }
	         
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 // log.info("------selectSample-=----result------ > " + obj.toString());
			return write(obj.toString());
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", e.toString());
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	    	 // return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
	     }
    } 
    
    /**
     * 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveSample(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
        String dbName = pm.getString("component.sql.database");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("LOGIN_ID",  user_id);
	      	    
	       	 	log.info("----------------saveSample--------------------item---------------------\n" + item.toString());
	       	 	
		       	if( Integer.parseInt(getItem("mg.sys.countPgm", item).toString() ) > 0 ) 
		       	{
		       		updateObject("mg.vdr.updateSample"+getDBName(dbName), item);
		       	} else 
		       	{
		       		createObject("mg.vdr.insertSample"+getDBName(dbName), item);
		       	}
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", loc), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
       	    log.error("---------------------------------------------------------------\n" + e.toString());
            return writeXml(responseTranData(mm.getMessage("COMG_1007", loc),"false", "doSave", "doSaveEnd"));
        }
    }
    
    /**
     * 삭제 	
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteSample(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.vdr.deleteSample", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", loc), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
           log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseTranData(mm.getMessage("COMG_1008", loc),"false", "doSave", "doSaveEnd"));
        }
    }
    
}
