package mg.vdr;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;

public class contextAction extends BaseAction {
    /**
     * VDR Context 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("createRoom")) {   /** 프로젝트(Room) 생성 */
            return createRoom(request);    
        }else if(request.isMode("createRoomUser")) {   /** 프로젝트(Room) 참여자 등록 */
            return createRoomUser(request);
        }else if(request.isMode("selectUserJoined")) {   /** 프로젝트(Room) 참여자 조회 */
            return selectUserJoined(request);
        }else if(request.isMode("selectRoom")) {   /** 프로젝트(Room) 선택*/
                return selectRoom(request);    
        }else if(request.isMode("getPolicyNum")){
        		return getPolicyNum(request);
        }else if(request.isMode("inviteUser")){
        	return null;
//        		return inviteUser(request);
        }
        else { 
            return write(null);
        }
    }
    
   
    
    /**
     * 룸 생성
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse createRoom(IRequest request) throws Exception 
    {
    	
    	//리턴할 json 오브젝트 만들기
   	    JSONObject obj = new JSONObject();

   	    //request로부터 ids와 컬럼명스트링 가져온다
   	    String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
        String user_nm =  request.getUser() != null ? request.getUser().getName() : "관리자";
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
        String ctrt_cl_cd = request.getStringParam("ROOM_CL_CD"); // ROOM_CL_CD : 0.임시사용, 1.계약, 
  	    
        //스트링배열에 컬럼명들을 따로 담는다.
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        //roomid를 생성해준다.
        String RoomId = getMakeId("R");
    	      
        try {
	        
        	tx.begin();
        	Map item = null;
        	for(int i=0; i<ids.length; i++){
        		
        		item = new HashMap();
        		
        		for(int j=0; j<cols.length; j++){
        			String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
        			item.put(cols[j], tmps);
        		}
        		
        		//addUser에서 error났을 때 locale을 넣어주기 위한 put
        		item.put("LOCALE", loc);
        		
        		//vdr_room을 위한 put
            	item.put("LOGIN_ID", user_id);
        		item.put("R_DT", DateUtils.getCurrentDate("yyyyMMdd"));
        		item.put("ROOM_ID", RoomId);
        		
        		
        		//vdr_room_join을 위한 put
            	item.put("JOIN_DT", DateUtils.getCurrentDate("yyyyMMdd"));
            	item.put("MAIL_SEND_YN", "N") ;
            	
            	//vdr_room_user을 위한 put
            	 item.put("USER_NM", user_nm);
        		
        		
        		// 계약이 아닌 임시사용일 경우 CTRT_NO,CTRT_DT,CTRT_ID에 공백을 넣어준다. 
          	    if("0".equals(ctrt_cl_cd)) {
          	    	item.put("CTRT_NO", "");
          	    	item.put("CTRT_DT", "");
          	    	item.put("CTRT_ID", "");
          	    }
          	    
        		
        		
    	        log.info("----------------createRoom :: VDR+ROOM 추가 ---------------------\n" + item.toString());
    	        createObject("mg.vdr.createRoom"+getDBName(pm.getString("component.sql.database")), item);
    	        
    	        log.info("----------------createRoomUser :: USER, JOIN 추가 ---------------------\n" + item.toString());
            	obj = addUser(item);
        	
        		
        	}
        	tx.commit();
        	return write(obj.toString());
	        
        } catch(Exception e) {
       	    log.error("---------------------------------------------------------------\n" + e.toString());
       	    
   	        obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");   
            return write(obj.toString());
        }
    }
    
    /**
     * 룸 참여자 등록
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse createRoomUser(IRequest request) throws Exception 
    {
   	    JSONObject obj = new JSONObject();   	    
        String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
        String user_nm =  request.getUser() != null ? request.getUser().getName() : "관리자";
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
        
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");

        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        
	        
        Map item = null;
        for(int i=0; i<ids.length; i++){		
	        item = new HashMap();
			item.put("LOCALE", loc);
			for(int j=0; j<cols.length; j++){
				String tmps = request.getStringParam("1_" + cols[j]);
				item.put(cols[j], tmps);
			}
	
			//vdr_room_join을 위한 put
	   	 	item.put("LOGIN_ID",  user_id);
	   	 	item.put("JOIN_DT", DateUtils.getCurrentDate("yyyyMMdd"));
	    	item.put("MAIL_SEND_YN", "N") ;
	    	item.put("JOIN_CL_CD","1");
	    	
	    	
	    	//vdr_room_user을 위한 put
	    	 item.put("USER_NM", user_nm);
	        
	        obj = addUser(item);
        }
	    return write(obj.toString());
    }
    
    
    private JSONObject addUser(Map map){
    	JSONObject obj = new JSONObject();
    	
    	Map item = new HashMap();
    	item = map;
    	Object locale = map.get("LOCALE");
    	Locale loc = (Locale)locale;
    	
    	try{
    	
    		createObject("mg.vdr.createRoomJoin"+getDBName(pm.getString("component.sql.database")), item);
    		createObject("mg.vdr.createRoomUser"+getDBName(pm.getString("component.sql.database")), item);
	        
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			
			return obj;

	    }catch(Exception e) {
	   	    log.error("---------------------------------------------------------------\n" + e.toString());
	   	    obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");   
	        
			return obj;
	    }
    }
    
    
    private IResponse getPolicyNum(IRequest request) throws Exception
    {
    	JSONObject obj = new JSONObject();
    	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
    	try {
    		Map smap = request.getMap();
    		String colValue =null;
    		List polNum = getList("mg.vdr.getPolicyNum", smap);
    		  for (int i = 0; i < polNum.size(); i++) {
  	          	Map map =(Map) polNum.get(i);
  	    		 colValue = this.getStringWithNullCheck(map,"POLICY_NO");
  	    		
    		  }
    		 List lists = getList("mg.master.getComboCpGrp", request.getMap());
      	     Map map = null;
      	     JSONArray jArray = new JSONArray();
      	     
              for (int i = 0; i < lists.size(); i++) {
            	  JSONObject data= new JSONObject();
            	  map =(Map) lists.get(i);
                 
            		data.put("text",  (String)map.get("NM") );
    				data.put("value", (String)map.get("CD") );
    				
    				jArray.add(data);
            	  
              	//buffer.append("<option value='" + map.get("CD") + "'>" + map.get("NM") + "</option>");
                  // log.debug( "-------------------------------------------------------------------\n" + map.toString());
              }
    		  
      		obj.put("data", jArray);
    		  
    		
    		obj.put("selected", colValue);
   		 	obj.put("errmsg", "success");
   		 	obj.put("errcode", "0");
    		
    	}
    	catch(Exception e){
    		
    		log.error("---------------------------------------------------------------\n" + e.toString());
       	    
   	        obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");   
            return write(obj.toString());
    		
    		
    	}
    	
		return write(obj.toString());
    }
    
    
    /**
    * 룸 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectRoom(IRequest request) throws Exception 
    {
    	  JSONObject obj = new JSONObject();
		  String col = request.getStringParam("grid_col_id");
	 	  String[] cols = col.split(",");
	 	   	 	
	 	  try {
	 		  Map smap = request.getMap();
	 		  smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC) );
	 		  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
	 		  smap.put("LOGIN_ID", user_id);
	 		   	 		  
	 		  List lists = getList("mg.vdr.selectRoom", smap);
	 		  JSONArray jArray = new JSONArray();
	    
	 		  
	 		 
	          for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	JSONObject listdata = new JSONObject();
	          	
	          	listdata.put("ROW_ID", String.valueOf(i+1));
	          	
	          	for(int j=0; j < cols.length; j++ ) {
	          		String colValue = this.getStringWithNullCheck(map,cols[j]);
	          		listdata.put(cols[j], colValue);
	          	}
	          	// log.info("------selectSample-=----listdata------ > " + listdata.toString());
	          	jArray.add(listdata);
	         }
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 // log.info("------selectSample-=----result------ > " + obj.toString());
			return write(obj.toString());
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", e.toString());
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	    	 // return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
	     }
    } 
    
    /**
    * 프로젝트(Room) 참여자 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectUserJoined(IRequest request) throws Exception 
    {
    	  JSONObject obj = new JSONObject();
	 	   	 	
	 	  try {
	 		  Map smap = request.getMap();
	 		  smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC) );
	 		  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
	 		   	 		  
	 		  List lists = getList("mg.vdr.selectUserJoined", smap);
	 		  JSONArray jArray = new JSONArray();
   			
	          for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	JSONObject listdata = new JSONObject();
	          	
				  String colValue = this.getStringWithNullCheck(map,"R_USER_ID");
				listdata.put("text", colValue);
				listdata.put("value", colValue);
	          	jArray.add(listdata);
	         }
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			return write(obj.toString());
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", e.toString());
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }

    }
}
