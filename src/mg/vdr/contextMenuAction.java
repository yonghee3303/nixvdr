package mg.vdr;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.common.CpcConstants;

public class contextMenuAction extends BaseAction{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public IResponse run(IRequest request) throws Exception {
		   if(request.isMode("selectComment")) {   /*코멘트 조회 */
	            return selectComment(request);                           
	        }else  if(request.isMode("saveComment")) {   /*코멘트 등록 */
	            return saveComment(request);                           
	        }else  if(request.isMode("deleteComment")) {   /*코멘트 삭제 */
	            return deleteComment(request);                           
	        }if(request.isMode("selectHistory")) {   /*문서이력 조회 */
	            return selectHistory(request);                           
	        }else { 
	            return write(null);
	        }
	}
	
	 /**
	    * 
	    * @param request
	    * @return
	    * @throws Exception
	    */
	    private IResponse selectComment(IRequest request) throws Exception 
	    {
	    	JSONObject obj = new JSONObject();
	      	String col = request.getStringParam("grid_col_id");
	      	String[] cols = col.split(",");
	        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
	    	String dbName = pm.getString("component.sql.database");
	   
	         try {
	        	 Map map = null;
	        	 List lists = null;
	   	       	 if("mssql".equals(dbName)) {
	   	       		lists = getList("mg.vdr.selectComment",request.getMap());
	   	       	 } else {
	   	       		lists = getList("mg.vdr.selectComment" +getDBName(pm.getString("component.sql.database")),request.getMap());
	   	       	 }
	   	     
	         	 JSONArray jArray = new JSONArray();
	      	    
		          for (int i = 0; i < lists.size(); i++) {
		          	 map =(Map) lists.get(i);        	
		          	JSONObject listdata = new JSONObject();
		     		listdata.put("ROW_ID", String.valueOf(i+1));
		       		
		          	for(int j=0; j < cols.length; j++ ) {
		          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j])) ; 
		          	}  	
		          	jArray.add(listdata);
		          }
	 			 
	          	 obj.put("data", jArray);
	 			 obj.put("errmsg", "success");
	 			 obj.put("errcode", "0");
	 			 log.info("------countComment-=----result------ > " + obj.toString());
	 			 return write(obj.toString());
	 	     } catch(Exception e) {
	 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	 			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
	 			 obj.put("errcode", "-1");
	 	    	 return write(obj.toString());
	 	     }
	    } 
	    
	    /**
	     * 
	     * @param request
	     * @return
	     * @throws Exception
	     */
	    private IResponse saveComment(IRequest request) throws Exception 
	    {
	     	JSONObject obj = new JSONObject();
	        JSONArray jArray = new JSONArray();

	        try {
	        	 Map item = request.getMap();
		           	
		       	 if("mssql".equals(pm.getString("component.sql.database"))) {
		       		createObject("mg.vdr.insertComment", item);
		       	 }else{
		       		createObject("mg.vdr.insertComment" + getDBName(pm.getString("component.sql.database")), item);
		       	 }	       	  

				 obj.put("errmsg", "success");
				 obj.put("errcode", "0");
				 return write(obj.toString());
		     } catch(Exception e) {
		    	 obj.put("errmsg", mm.getMessage("COMG_1007"));
				 obj.put("errcode", "-1");
		    	 return write(obj.toString());
		     }

	    }
	    private IResponse deleteComment(IRequest request) throws Exception 
	     {
	    	 JSONObject obj = new JSONObject();
	    	 JSONArray jArray = new JSONArray();
	         String ids_info = request.getStringParam("ids");
	         String cols_ids = request.getStringParam("col_ids");
	         String[] cols = cols_ids.split(",");
	         String[] ids = ids_info.split(",");
	         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
	         
	         try {
	 	        Map item = null;
	 	       JSONObject listdata;
	 	        for (int i = 0; i < ids.length; i++) {
	 	       	 	item = new HashMap();
		 	       	listdata = new JSONObject();
		 	       	item.put("ROOM_ID", request.getStringParam("ROOM_ID"));
		 	       	item.put("DOC_ID", request.getStringParam("DOC_ID"));
	 	       	 	for(int j=0; j < cols.length; j++ ) {
	 	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 		             item.put(cols[j], tmps);     
	 		            listdata.put(cols[j], tmps);     
	 	         	}
	 	       	 	deleteObject("mg.vdr.deleteComment"+getDBName(pm.getString("component.sql.database")), item);
	 	       	 	jArray.add(listdata);
	 	        }
	 	        obj.put("data", jArray);
	 	        obj.put("errmsg", mm.getMessage("COMG_1002", loc));
				obj.put("errcode", "0");
				  log.info("------delete-=----result------ > " + obj.toString());
		        return write(obj.toString());
	         } catch(Exception e) {
	            log.error("---------------------------------------------------------------deleteComment" + e.toString());
	            obj.put("errmsg", mm.getMessage("COMG_1008", loc));
				obj.put("errcode", "-1");   
	            return write(obj.toString());
	         }
	     }
	    
	    /**
		    * 
		    * @param request
		    * @return
		    * @throws Exception
		    */
		    private IResponse selectHistory(IRequest request) throws Exception 
		    {
		    	JSONObject obj = new JSONObject();
		    	String user_id =  request.getUser() != null ? request.getUser().getId() : "iXVDR@inzent.com";
		      	String col = request.getStringParam("grid_col_id");
		      	String[] cols = col.split(",");
		        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
		    	String dbName = pm.getString("component.sql.database");
		    	Map item = request.getMap();
		     	item.put("R_USER_ID", user_id);
		    	  
		         try {
		        	 Map map = null;
		        	 List lists = null;
		   	       	 if("mssql".equals(dbName)) {
		   	       		lists = getList("mg.vdr.selectHistory",request.getMap());
		   	       	 } else {
		   	       		
		   	       		 if("folder".equals(request.getStringParam("LIST_TYPE"))){	   	       		
		   	       			 lists = getList("mg.vdr.selectFolderHistory" +getDBName(pm.getString("component.sql.database")),item);
		   	       		 }else{
		   	       			 lists = getList("mg.vdr.selectDocHistory" +getDBName(pm.getString("component.sql.database")),item);
		   	       		 }
		   	       		
		   	       	 }
		   	     
		         	 JSONArray jArray = new JSONArray();
		      	    
			          for (int i = 0; i < lists.size(); i++) {
			          	 map =(Map) lists.get(i);        	
			          	JSONObject listdata = new JSONObject();
			     		listdata.put("ROW_ID", String.valueOf(i+1));
			       		
			          	for(int j=0; j < cols.length; j++ ) {
			          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
			          	}  	
			          	jArray.add(listdata);
			          }
		 			 
		          	 obj.put("data", jArray);
		 			 obj.put("errmsg", "success");
		 			 obj.put("errcode", "0");
		 			log.info("------selectHistory-=----result------ > " + obj.toString());
		 			 return write(obj.toString());
		 	     } catch(Exception e) {
		 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
		 			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
		 			 obj.put("errcode", "-1");
		 	    	 return write(obj.toString());
		 	     }
		    } 
	}
