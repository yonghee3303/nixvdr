package mg.vdr;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.component.upload.IFileUploadManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.common.CpcConstants;

public class roomAction extends BaseAction {
	
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);
	
    /**
     * VDR 탐색기 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("createRoom")) {   /** 프로젝트(Room) 생성 */
            return createRoom(request);    
        }else if(request.isMode("createRoomUser")) {   /** 프로젝트(Room) 참여자 등록 */
            return createRoomUser(request);
        }else if(request.isMode("selectUserJoined")) {   /** 프로젝트(Room) 참여자 조회 */
            return selectUserJoined(request);
        }else if(request.isMode("selectRoom")) {   /** 프로젝트(Room) 선택*/
            return selectRoom(request);    
        }else if(request.isMode("selectRoomMng")) {   /** 관리중인 프로젝트 조회 */
            return selectRoomMng(request);    
        }else if(request.isMode("getPolicyNum")){
        	return getPolicyNum(request);
        }else if(request.isMode("getLastVisitRoom")){ /** 최근 방문한 ROOM 조회 */
        	return getLastVisitRoom(request);
        }else if(request.isMode("getInviteUserRoom")){	/** 룸에 초대한 사용자 ID를 가져온다. */
        	return getInviteUserRoom(request);
        }else if(request.isMode("selectRoomMngUserInfo")){ /** 룸을 개설한 사용자 정보를 가져온다. */
        	return selectRoomMngUserInfo(request);
        }else if(request.isMode("deleteRoom")){ /** 룸 삭제 */
        	return deleteRoom(request);
        }else if(request.isMode("deleteRoomUser")){ /** 룸 사용자 삭제 */
        	return deleteRoomUser(request);
        }else if(request.isMode("getRoomParticipantsList")){ /** 룸 참여자 조회 */
        	return getRoomParticipantsList(request);
        }else if(request.isMode("updateRoomAuth")){ /** 룸별 권한 업데이트 */
        	return updateRoomAuth(request);
        }else if(request.isMode("getRoomAdminList")){ /** 룸 관리자 리스트를 가져온다. */
        	return getRoomAdminList(request);
        }else if(request.isMode("getAuthList")){ /** 권한 목록을 가져온다. */
        	return getAuthList(request);
        }else if(request.isMode("getRoomSandboxAuthSetInfo")){ /** 룸에 설정된 권한에 따른 샌드박스 권한 설정을 가져온다. */
        	return getRoomSandboxAuthSetInfo(request);
        }else if(request.isMode("getDashRoom")){ /** 대시보드 룸 리스트를 가져온다. */
        	return getDashRoom(request);
        }else{ 
            return write(null);
        }
    }
    
   
    
    /**
     * 룸 생성
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse createRoom(IRequest request) throws Exception 
    {
   	    JSONObject obj = new JSONObject();

        String[] cols = request.getStringParam("col_ids").split(",");
        
        String user_id =  request.getUser().getId();
        String user_nm =  request.getUser().getName();
  	    
        
        try {
        	
        	Map item = new HashMap();
        	
        	item.put("R_USER_ID", user_id);
        	
        	tx.begin();
        	
        	for(String col : cols) {
        		item.put(col, request.getStringParam("1_" + col));
        	}
        	
        	if(null != item.get("IS_FREE") && "true".equals(item.get("IS_FREE").toString())) {
        		
				SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
				Calendar cal = Calendar.getInstance();
			 	cal.setTime(new Date());	   	    				
			    String startDate = dayTime.format(cal.getTime());
			    String startDateCtrtNm = startDate.substring(0, 4) + "-" + startDate.substring(4, 6) + "-" + startDate.substring(6, 8);
			    
			    cal.add(Calendar.MONTH, 1);
			    String endDate = dayTime.format(cal.getTime());
			    String endDateCtrtNm = endDate.substring(0, 4) + "-" + endDate.substring(4, 6) + "-" + endDate.substring(6, 8);
			    
			    item.put("CTRT_NM", mm.getMessage("COMG_1082", new Locale(("JP".equals(item.get("LANG").toString()))? "JA" : item.get("LANG").toString())) + " (" + startDateCtrtNm + " ~ " + endDateCtrtNm + ")");
			    item.put("S_DT", startDate);
			    item.put("E_DT", endDate);
				item.put("CTRT_NO", "FR" + DateUtils.getCurrentDate("yyyymmddhhmmss") + makeRamdomNumber(10));
				item.put("CTRT_CL_CD", "TR");
				item.put("USER_ID", user_id);        	    			 

			    createObject("mg.vdr.saveCustTrCont" + getDBName(pm.getString("component.sql.database")), item);
			    
        	}else {
        		
        		Map custContInfo = (Map) getItem("mg.vdr.selectCustCont" + getDBName(pm.getString("component.sql.database")), item);
        		Map userRoomCntMap = (Map)getItem("mg.vdr.selectCustContUserRoomCnt" + getDBName(pm.getString("component.sql.database")), item);
        		
        		int USER_CNT = NumberUtils.toInt(custContInfo.get("ROOM_CNT").toString());
        		int USE_USER_CNT = (null == userRoomCntMap)? 0 : NumberUtils.toInt(userRoomCntMap.get("ROOM_CNT").toString());
        		
        		if(0 < USER_CNT && USER_CNT < USE_USER_CNT + 1) {
           	        obj.put("errmsg", "error");
        			obj.put("errcode", "-2");   
                    return write(obj.toString());
        		}
        	}
        		
    		//vdr_room을 위한 put
        	item.put("LOGIN_ID", user_id);
    		item.put("R_DT", DateUtils.getCurrentDate("yyyyMMdd"));
    		item.put("ROOM_ID", getMakeId("R"));
    		
    		//vdr_room_join을 위한 put
        	item.put("JOIN_DT", DateUtils.getCurrentDate("yyyyMMdd"));
        	item.put("MAIL_SEND_YN", "N") ;
        	item.put("JOIN_YN","Y");
        	item.put("USER_NM", user_nm);
    		
	        createObject("mg.vdr.createRoom" + getDBName(pm.getString("component.sql.database")), item);
	        
	        item.put("ADMIN", "Y");
	        item.put("VIEW", "Y");
	        item.put("EDIT", "Y");
	        
        	obj = addUser(item);
        	
        	//기본 폴더 생성
        	item.put("FOLDER_NM",  mm.getMessage("COMG_1081", new Locale(("JP".equals(item.get("LANG").toString()))? "JA" : item.get("LANG").toString())));
        	item.put("USERID",  user_id);
        	item.put("OWNER_ID",  user_id);
       	    item.put("FOLDER_STYLE", "folder");
	       	item.put("CREATOR_ID", user_id);
	       	item.put("MODIFIER_ID", user_id);
	       	item.put("IS_DELETE", "N");
	       	item.put("FOLDER_ORDER", 0);
	       	item.put("FOLDER_ID", getMakeId("F"));
	       	item.put("P_FOLDER_ID", "-1");
        	
 	       	createObject("mg.vdr.createFolder" + getDBName(pm.getString("component.sql.database")), item);
 	        createObject("mg.vdr.createFolderAuth" + getDBName(pm.getString("component.sql.database")), item);
 	        
 	        createObject("mg.vdr.createRoomSandboxAuthSetInfo" + getDBName(pm.getString("component.sql.database")), item);
        	
        	tx.commit();
        	
        	return write(obj.toString());
	        
        } catch(Exception e) {
   	        obj.put("errmsg", mm.getMessage("COMG_1007"));
			obj.put("errcode", "-1");   
            return write(obj.toString());
        }
    }
    
    /**
     * 룸 참여자 등록
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse createRoomUser(IRequest request) throws Exception 
    {
   	    JSONObject obj = new JSONObject();   	    
   	    
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");

        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        Map item = null;
        
        boolean isExistSelf = false;
        
        for(int i = 0; i < ids.length; i++){
        	if(request.getUser().getId().equals(request.getStringParam(ids[i] + "_R_USER_ID"))) {
        		isExistSelf = true;
        		break;
        	}
        }
        
        if(isExistSelf) {
	   	    obj.put("errmsg", "fail");
			obj.put("errcode", "-3");
			return write(obj.toString());        	
        }
        
        String ROOM_ID = request.getMap().get("ROOM_ID").toString();
        
    	Map contractRoomInfo = (Map) getItem("mg.vdr.getContractRoomInfo" + getDBName(pm.getString("component.sql.database")), new HashMap() {{put("ROOM_ID", ROOM_ID);}});
    	
    	int CONTRACT_UCOUNT = (null == contractRoomInfo.get("CONTRACT_UCOUNT"))? 0 : NumberUtils.toInt(contractRoomInfo.get("CONTRACT_UCOUNT").toString());
    	int USE_UCOUNT = (null == contractRoomInfo.get("USE_UCOUNT"))? 0 : NumberUtils.toInt(contractRoomInfo.get("USE_UCOUNT").toString());
    	
    	for(int i = 0; i < ids.length; i++){
    		
    		item = new HashMap();
    		
    		item.put("ROOM_ID", ROOM_ID);
    		item.put("R_USER_ID", request.getStringParam(ids[i] + "_R_USER_ID"));
    		
    		if(0 == getList("mg.vdr.selectUserJoined", item).size()) {
    			USE_UCOUNT++;
    		}
    	}
    	
    	if(CONTRACT_UCOUNT < USE_UCOUNT) {
	   	    obj.put("errmsg", "fail");
			obj.put("errcode", "-2");
			return write(obj.toString());
    	}
    		
        for(int i = 0; i < ids.length; i++){
	        
        	item = new HashMap();
        	
			for(int j = 0; j < cols.length; j++){
				item.put(cols[j], request.getStringParam(ids[i] + "_" + cols[j]));
			}
			
	   	 	item.put("LOGIN_ID",  request.getUser().getId());
	   	 	item.put("JOIN_DT", DateUtils.getCurrentDate("yyyyMMdd"));
	    	item.put("MAIL_SEND_YN", "Y") ;
	    	item.put("JOIN_CL_CD","1");
	    	item.put("MAIL_SEND_CERT_ID", UUID.randomUUID().toString());
	    	item.put("USER_NM", request.getUser().getName());
	    	item.put("ROOM_ID", ROOM_ID);
	    	
	    	List lists = getList("mg.vdr.selectUserJoined", item);
	    	
	    	if(0 == lists.size()) {	    		
	    		obj = addUser(item);
	    	}else{
				try {
					updateObject("mg.vdr.updateRoomJoin", item);
					updateObject("mg.vdr.updateRoomUser" + getDBName(pm.getString("component.sql.database")), item);					
					obj.put("errmsg", "success");
					obj.put("errcode", "0");
				}catch (Exception e) {
					obj.put("errmsg", "fail");
					obj.put("errcode", "-1");
				}
	    	}
	    	
    		String R_USER_ID = item.get("R_USER_ID").toString();
    		String CONTENT = pm.getString("component.url.invite") + "/" +item.get("MAIL_SEND_CERT_ID").toString();
    		String LANG = (null == request.getStringParam("LANG"))? "KO" : request.getStringParam("LANG");
    		
    		this.sendMail(R_USER_ID, R_USER_ID, CONTENT, 1, LANG);
        }
	    
    	return write(obj.toString());
    }
    
    private JSONObject addUser(Map map){
    	JSONObject obj = new JSONObject();
    	
    	Map item = new HashMap();
    	item = map;
    	Object locale = map.get("LOCALE");
    	Locale loc = (Locale)locale;
    	
    	try{
    	
    		createObject("mg.vdr.createRoomJoin"+getDBName(pm.getString("component.sql.database")), item);
    		createObject("mg.vdr.createRoomUser"+getDBName(pm.getString("component.sql.database")), item);
	        
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			
    		JSONObject data = new JSONObject();
    		data.put("ROOM_ID", item.get("ROOM_ID"));
			obj.put("data", data);
			
			return obj;

	    }catch(Exception e) {
	   	    log.error("---------------------------------------------------------------\n" + e.toString());
	   	    obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");   
	        
			return obj;
	    }
    }
    
    
    private IResponse getPolicyNum(IRequest request) throws Exception
    {
    	JSONObject obj = new JSONObject();
    	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
    	try {
    		Map smap = request.getMap();
    		String colValue =null;
    		List polNum = getList("mg.vdr.getPolicyNum", smap);
    		  for (int i = 0; i < polNum.size(); i++) {
  	          	Map map =(Map) polNum.get(i);
  	    		 colValue = this.getStringWithNullCheck(map,"POLICY_NO");
  	    		
    		  }
    		 List lists = getList("mg.master.getComboCpGrp", request.getMap());
      	     Map map = null;
      	     JSONArray jArray = new JSONArray();
      	     
              for (int i = 0; i < lists.size(); i++) {
            	  JSONObject data= new JSONObject();
            	  map =(Map) lists.get(i);
                 
            		data.put("text",  (String)map.get("NM") );
    				data.put("value", (String)map.get("CD") );
    				
    				jArray.add(data);
            	  
              	//buffer.append("<option value='" + map.get("CD") + "'>" + map.get("NM") + "</option>");
                  // log.debug( "-------------------------------------------------------------------\n" + map.toString());
              }
    		  
      		obj.put("data", jArray);
    		  
    		
    		obj.put("selected", colValue);
   		 	obj.put("errmsg", "success");
   		 	obj.put("errcode", "0");
    		
    	}
    	catch(Exception e){
    		
    		log.error("---------------------------------------------------------------\n" + e.toString());
       	    
   	        obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");   
            return write(obj.toString());
    		
    		
    	}
    	
		return write(obj.toString());
    }
    
    
    /**
    * 룸 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectRoom(IRequest request) throws Exception 
    {
    	  JSONObject obj = new JSONObject();
		  String col = request.getStringParam("grid_col_id");
	 	  String[] cols = col.split(",");
	 	   	 	
	 	  try {
	 		  Map smap = request.getMap();
	 		  smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC) );
	 		  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
	 		  smap.put("LOGIN_ID", user_id);
	 		  
	 		  List lists = null;
	 		  
	 		  if(null != smap.get("type") && "all".equals(smap.get("type").toString())) {
	 			 lists = getList("mg.vdr.selectAllRoom", smap);
	 		  }else if(null != smap.get("Preview") && "true".equals(smap.get("Preview").toString())) {
		 			 lists = getList("mg.vdr.selectYesRoom"+ getDBName(pm.getString("component.sql.database")), smap);
	 		  }else{
	 			 lists = getList("mg.vdr.selectRoom", smap);
	 		  }
	 		  
	 		  
	 		  JSONArray jArray = new JSONArray();
	 		 
	          for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	JSONObject listdata = new JSONObject();
	          	
	          	listdata.put("ROW_ID", String.valueOf(i+1));
	          	
	          	for(int j=0; j < cols.length; j++ ) {
	          		String colValue = this.getStringWithNullCheck(map,cols[j]);
	          		listdata.put(cols[j], colValue);
	          	}
	          	// log.info("------selectSample-=----listdata------ > " + listdata.toString());
	          	jArray.add(listdata);
	         }
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 // log.info("------selectSample-=----result------ > " + obj.toString());
			return write(obj.toString());
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", e.toString());
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	    	 // return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
	     }
    } 
    
    /**
    * 프로젝트(Room) 참여자 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectUserJoined(IRequest request) throws Exception 
    {
    	  JSONObject obj = new JSONObject();
	 	   	 	
	 	  try {
	 		  Map smap = request.getMap();
	 		  smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC) );
	 		  smap.put("LOGIN_ID", request.getUser() != null ? request.getUser().getId() : "admin");
	 		
	 		 List lists;
	 		 
	 		 if(null != smap.get("SAND"))  lists = getList("mg.vdr.selectUserJoined_SANDBOX", smap);
	 		 else lists = getList("mg.vdr.selectUserJoined", smap);
	 		  		 		  
	 		  JSONArray jArray = new JSONArray();
   			
	          for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	JSONObject listdata = new JSONObject();
	          	
				  String colValue = this.getStringWithNullCheck(map,"R_USER_ID");
				listdata.put("text", colValue);
				listdata.put("value", colValue);
	          	jArray.add(listdata);
	         }
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			return write(obj.toString());
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", e.toString());
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }

    }
    

	/**
	 * 최근 방문한 ROOM 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getLastVisitRoom(IRequest request) throws Exception {
		
		JSONObject obj = new JSONObject();

		String user_id = (null != request.getUser()) ? request.getUser().getId() : "admin";

		try {

			Map item = request.getMap();
			List lists;
			item.put("R_USER_ID", user_id);
			
			if(null != item.get("Preview") && "true".equals(item.get("Preview").toString())) {
				lists = getList("mg.vdr.getYesLastVisitRoom"+ getDBName(pm.getString("component.sql.database")), item);
			}else{
				lists = getList("mg.vdr.getLastVisitRoom" + getDBName(pm.getString("component.sql.database")), item);
			}

			long CURRENT_TIME = System.currentTimeMillis();
			
			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();

				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}
				
				if(null != item.get("Preview") && "true".equals(item.get("Preview").toString())) {
					
					long LAST_REG_DT_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(map.get("LAST_REG_DT").toString()).getTime();
					long DIFF_TIME = CURRENT_TIME - LAST_REG_DT_TIME;
					
					listData.put("DIFF_TIME", (0 > DIFF_TIME)? 0 : DIFF_TIME / 1000);
				}

				jArray.add(listData);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", e.toString());
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}
	
	/**
	 * 룸에 초대한 사용자 ID를 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getInviteUserRoom(IRequest request) throws Exception {
		
		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			List lists = getList("mg.vdr.getInviteUserRoom" + getDBName(pm.getString("component.sql.database")), item);

			JSONArray jArray = new JSONArray();
			
			if(0 < lists.size()) {
				
				Map map = (Map) lists.get(0);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();

				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}

				jArray.add(listData);
				
				if("1".equals(map.get("ISREGUSER").toString())) {
					
					item.put("ROOM_ID", map.get("ROOM_ID"));
					item.put("USER_ID", map.get("R_USER_ID"));
					
					updateObject("mg.vdr.updateRoomJoinYN" + getDBName(pm.getString("component.sql.database")), item);
					
					createObject("mg.vdr.insertFolderAuth" + getDBName(pm.getString("component.sql.database")), item);
					
					createObject("mg.vdr.insertDocAuth" + getDBName(pm.getString("component.sql.database")), item);
				}
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", e.toString());
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}
	
	/**
	 * 룸을 개설한 사용자 정보를 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectRoomMngUserInfo(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		String user_id =  (null != request.getUser())? request.getUser().getId() : "admin";
		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {
	
			Map item = request.getMap();
			item.put("USER_ID",  user_id);
			Map	map = (Map) getItem("mg.vdr.selectRoomMngUserInfo" + getDBName(pm.getString("component.sql.database")), item);
			
			
			
			if(null == map) throw new Exception();
			
			Iterator<String> keys = map.keySet().iterator();

			JSONObject data = new JSONObject();

			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			
			return write(obj.toString());
		}
	}	
	
	/**
	 * 사용자가 개설한 룸의 개수를 가져옴. 
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectRoomMng(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();
		String user_id =  (null != request.getUser())? request.getUser().getId() : "admin";
		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {
			int count = Integer.parseInt(getItem("mg.vdr.selectRoomMsg" + getDBName(pm.getString("component.sql.database")), user_id).toString());
			
			obj.put("data", count);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			
			return write(obj.toString());
		}
	}	
	
    /**
     * 룸 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteRoom(IRequest request) throws Exception {
    	
   	    JSONObject obj = new JSONObject();
   	    
   	    Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
    	      
        try {
	        
        	Map item = request.getMap();
        	
        	if(null == item.get("ROOM_ID")) throw new Exception();
        	
        	List deleteFileList = getList("mg.vdr.delete.seleteTFileUploadList", item);
        	
        	tx.begin();
        	
            deleteObject("mg.vdr.delete.deleteTFileUpload", item);
            deleteObject("mg.vdr.delete.deleteTrash", item);
            deleteObject("mg.vdr.delete.deleteLog", item);
            deleteObject("mg.vdr.delete.deleteFavorites", item);
            deleteObject("mg.vdr.delete.deleteComment", item);
            deleteObject("mg.vdr.delete.deleteDocAuth", item);
            deleteObject("mg.vdr.delete.deleteDocHist", item);
            deleteObject("mg.vdr.delete.deleteDoc", item);
            deleteObject("mg.vdr.delete.deleteFolderAuth", item);
            deleteObject("mg.vdr.delete.deleteFolder", item);
            deleteObject("mg.vdr.delete.deleteRoomAuthSet", item);
            deleteObject("mg.vdr.delete.deleteRoomUser", item);
            deleteObject("mg.vdr.delete.deleteRoomJoin", item);     
            deleteObject("mg.vdr.delete.deleteRoomAuthSet", item);
            deleteObject("mg.vdr.delete.deleteRoom", item);
			
			item.put("MOD_STOP", "2");
			updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), item);
			
			obj.put("errmsg", "success");
			obj.put("errcode", "0");        	
        	
        	tx.commit();
        	
        	fum.deleteTFileUploadList(deleteFileList);
        	
        	return write(obj.toString());
	        
        } catch(Exception e) {
       	    log.error("---------------------------------------------------------------\n" + e.toString());
       	    
   	        obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");   
            return write(obj.toString());
        }
    }
    
    /**
     * 사용자를 삭제
     * 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteRoomUser(IRequest request) throws Exception {

       JSONObject obj = new JSONObject();
       
       Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
        
       try {

          Map item = request.getMap();
          
          
          if(null == item.get("ROOM_ID") || null == item.get("R_USER_ID")) throw new Exception();
          
          //updateObject("mg.vdr.deleteUser" + getDBName(pm.getString("component.sql.database")), item);
          
          
          List deleteFileList = getList("mg.vdr.delete.seleteTFileUploadList", item);
          
          tx.begin();
          
          item.put("D_USER_ID", item.get("R_USER_ID"));
          
          deleteObject("mg.vdr.delete.deleteTFileUpload", item);
          deleteObject("mg.vdr.delete.deleteTrash", item);
          deleteObject("mg.vdr.delete.deleteLog", item);
          deleteObject("mg.vdr.delete.deleteFavorites", item);
          deleteObject("mg.vdr.delete.deleteComment", item);
          deleteObject("mg.vdr.delete.deleteDocAuth", item);
          deleteObject("mg.vdr.delete.deleteDocHist", item);
          deleteObject("mg.vdr.delete.deleteDoc", item);
          deleteObject("mg.vdr.delete.deleteFolderAuth", item);
          deleteObject("mg.vdr.delete.deleteFolder", item);
          deleteObject("mg.vdr.delete.deleteRoomUser", item);
          deleteObject("mg.vdr.delete.deleteRoomJoin", item);  
          
          item.put("MOD_STOP", "2");
          updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), item);
          
          obj.put("errmsg", "success");
          obj.put("errcode", "0");
          
          tx.commit();
          
          fum.deleteTFileUploadList(deleteFileList);
          
          return write(obj.toString());
          
       } catch(Exception e) {
    	   
    	   log.error("---------------------------------------------------------------\n" + e.toString());
    	   
    	   obj.put("errmsg", mm.getMessage("COMG_1007", loc));
           obj.put("errcode", "-1");
          
           return write(obj.toString());
       }
    }
    
	/**
     * 룸 참여자 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getRoomParticipantsList(IRequest request) throws Exception 
     {
     	  JSONObject obj = new JSONObject();
 		  
     	  String col = request.getStringParam("grid_col_id");
     	  
 	 	  String[] cols = col.split(",");
 	 	  
 	 	  Map map = null;
 	 	
 	 	  try {
 	 		  
 	 		  Map smap = request.getMap();
 	 		   	 		  
 	 		  List lists = getList("mg.vdr.getRoomParticipantsList" + getDBName(pm.getString("component.sql.database")), smap);
 	 		  
 	 		  JSONArray jArray = new JSONArray();
 	    
 	          for (int i = 0; i < lists.size(); i++) {
 	        	  
 	        	  map = (Map) lists.get(i);
 	        	  
 	        	  JSONObject listdata = new JSONObject();
 	        	  
 	        	  for(int j = 0; j < cols.length; j++) {
 	        		  listdata.put(cols[j], this.getStringWithNullCheck(map, cols[j]));
 	        	  }
 	        	  
 	        	  jArray.add(listdata);
 	         }
 	         
 	         obj.put("data", jArray);
 			 obj.put("errmsg", "success");
 			 obj.put("errcode", "0");

 			return write(obj.toString());
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", e.toString());
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
     }
     
     /**
      * 룸별 권한 업데이트
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateRoomAuth(IRequest request) throws Exception {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 try {
    		 
    		 Map item = request.getMap();
    		 
    		 tx.begin();
    		 
    		 JSONParser parser = new JSONParser();
    		 
    		 JSONArray update_list = (JSONArray) parser.parse((String) item.get("UPDATE_LIST"));
    		 
    		 for(int i = 0; i < update_list.size(); i++) {
    			 
    			 Map param = new ObjectMapper().readValue(((JSONObject) update_list.get(i)).toJSONString(), Map.class);
    			 
    			 param.put("ROOM_ID", item.get("ROOM_ID"));
    			 
    			 String POLICY = null;
    			 
    			 boolean isExistExpireDt = (null != param.get("SB_EXPIRE_DT")) && (0 < param.get("SB_EXPIRE_DT").toString().trim().length());
    			 boolean isExistReadCount = (null != param.get("SB_READ_COUNT")) && (0 < param.get("SB_READ_COUNT").toString().trim().length());
    			 
    			 if(isExistExpireDt && isExistReadCount) POLICY = "11111";
    			 else if(isExistExpireDt)				 POLICY = "11011";
    			 else if(isExistReadCount)				 POLICY = "10111";
    			 else 									 POLICY = "10011";
    			 
    			 param.put("POLICY", POLICY);
    			 
    			 updateObject("mg.vdr.updateRoomAuth" + getDBName(pm.getString("component.sql.database")), param);
    		 }

    		 obj.put("errmsg", "success");
    		 obj.put("errcode", "0");
    		 
    		 tx.commit();
    		 
    		 return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 obj.put("errmsg", mm.getMessage("COMG_1007"));
        	 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * 룸 관리자 리스트를 가져온다.
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse getRoomAdminList(IRequest request) throws Exception 
     {
 		JSONObject obj = new JSONObject();

 		try {

 			Map item = request.getMap();

 			List lists = getList("mg.vdr.getRoomAdminList" + getDBName(pm.getString("component.sql.database")), item);

 			JSONArray jArray = new JSONArray();

 			for (int i = 0; i < lists.size(); i++) {

 				Map map = (Map) lists.get(i);

 				Iterator<String> keys = map.keySet().iterator();

 				JSONObject listData = new JSONObject();

 				while (keys.hasNext()) {
 					String key = keys.next();
 					listData.put(key, this.getStringWithNullCheck(map, key));
 				}

 				jArray.add(listData);
 			}

 			obj.put("data", jArray);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");

 			return write(obj.toString());

 		} catch (Exception e) {
 			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			obj.put("errmsg", e.toString());
 			obj.put("errcode", "-1");
 			return write(obj.toString());
 		}
     }

     /**
      * 권한 목록을 가져온다.
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse getAuthList(IRequest request) throws Exception
     {
     	JSONObject obj = new JSONObject();
     	
     	try {
     		
     		Map smap = request.getMap();
     		
     		List<Map> lists = getList("mg.vdr.getAuthList" + getDBName(pm.getString("component.sql.database")), request.getMap());
     		
     		JSONArray jArray = new JSONArray();
     		
     		for(Map map : lists) {
     			
     			JSONObject data = new JSONObject();
     			
     			data.put("text", map.get("CD_NM").toString());
     			data.put("value", map.get("CD_KEY").toString());
     			
     			jArray.add(data);
     		}
     		  
       		obj.put("data", jArray);
       		obj.put("errmsg", "success");
       		obj.put("errcode", "0");
     		
     	}catch(Exception e){
     		obj.put("errmsg", mm.getMessage("COMG_1007"));
 			obj.put("errcode", "-1");
 			return write(obj.toString());
     	}
     	
 		return write(obj.toString());
     }   
     
     /**
      * 룸에 설정된 권한에 따른 샌드박스 권한 설정을 가져온다.
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse getRoomSandboxAuthSetInfo(IRequest request) throws Exception 
     {
 		JSONObject obj = new JSONObject();

 		try {

 			Map item = request.getMap();
 			
 			Map resultMap = (Map) getItem("mg.vdr.getRoomSandboxAuthSetInfo" + getDBName(pm.getString("component.sql.database")), item);
 			
 			Iterator<String> keys = resultMap.keySet().iterator();
 			
 			JSONObject returnData = new JSONObject();
 			
 			while (keys.hasNext()) {
 				String key = keys.next();
 				returnData.put(key, this.getStringWithNullCheck(resultMap, key));
 			} 			
 			
 			obj.put("data", returnData);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");

 			return write(obj.toString());

 		} catch (Exception e) {
 			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			obj.put("errmsg", e.toString());
 			obj.put("errcode", "-1");
 			return write(obj.toString());
 		}
     }
     
     /**
      * 대시보드 룸 리스트 조회
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse getDashRoom(IRequest request) throws Exception 
     {
 		JSONObject obj = new JSONObject();
 		JSONArray jArray = new JSONArray();

 		try {

 			Map item = request.getMap();
 			item.put("LOGIN_ID", request.getUser() != null ? request.getUser().getId() : "ixvdr@inzent.com");	 		  
 			
 			List<Map> lists = getList("mg.vdr.selectYesRoom"+ getDBName(pm.getString("component.sql.database")), item);
 			
 			for(Map myroom : lists) {
 				
 				Iterator<String> keys = myroom.keySet().iterator();
 				
 				JSONObject listData = new JSONObject();

 				while (keys.hasNext()) {
 					String key = keys.next();
 					listData.put(key, this.getStringWithNullCheck(myroom, key));
 				}
 				
 				Map user = new HashMap();
 				user.put("ROOM_ID", (String)myroom.get("ROOM_ID"));
 				
 				List<Map> roomUser = getList("mg.vdr.getRoomUser" + getDBName(pm.getString("component.sql.database")), user);
 				listData.put("USERCount", roomUser.size());
 				listData.put("MEMBER", (null != roomUser.get(0).get("USERNAME"))? roomUser.get(0).get("USERNAME") : null);
 				
 				jArray.add(listData);
 			}

 			obj.put("data", jArray);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");

 			return write(obj.toString());

 		} catch (Exception e) {
 			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			obj.put("errmsg", e.toString());
 			obj.put("errcode", "-1");
 			return write(obj.toString());
 		}
     }
     
}
