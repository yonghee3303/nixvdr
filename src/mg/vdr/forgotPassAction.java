package mg.vdr;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.component.crypto.ICryptoManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.common.CpcConstants;

public class forgotPassAction extends BaseAction {

	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
	@Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("getUserInfo")) { /** 유저 정보를 가져온다. */
            return getUserInfo(request);    
        }else if(request.isMode("sendCertId")) { /** 인증 번호를 저장 후 전달 한다. */
        	return sendCertId(request);
        }else if(request.isMode("updatePassword")) { /** 비밀번호 업데이트 */
        	return updatePassword(request);
        }else{ 
            return write(null);
        }
    }
	
	/**
	 * 유저 정보를 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getUserInfo(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {

			Map item = request.getMap();

			Map map = (Map) getItem("mg.vdr.forgotpass.getUserInfo" + getDBName(pm.getString("component.sql.database")), item);
			
			if(null == map) throw new Exception();
			
			Iterator<String> keys = map.keySet().iterator();

			JSONObject data = new JSONObject();

			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			
			return write(obj.toString());
		}
	}
	
	/**
	 * 인증 번호를 저장 후 전달 한다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse sendCertId(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {

			Map item = request.getMap();
			
			String certStr = "";
			
			for(int i = 0; i < 6; i++) {
				certStr += (int) (Math.random() * 10);
			}
			
			this.sendMail(item.get("USER_ID").toString(), certStr, "", 2, (null == item.get("LANG"))? "KO" : item.get("LANG").toString());
			
			JSONObject data = new JSONObject();
			
			data.put("certStr", certStr);
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");
			
            return write(obj.toString());
		}
	}
    
	/**
	 * 비밀번호 업데이트
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse updatePassword(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {

			Map item = request.getMap();
			
			item.put("PASSWD", crypto.encode(this.getStringWithNullCheck(item, "PASSWD")));
			
			updateObject("mg.vdr.forgotpass.updatePassword" + getDBName(pm.getString("component.sql.database")), item);
			
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");
			
            return write(obj.toString());
		}
	} 
}