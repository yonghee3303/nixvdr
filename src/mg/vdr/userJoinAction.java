package mg.vdr;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.component.crypto.ICryptoManagement;
import com.core.component.upload.IFileUploadManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;

public class userJoinAction extends BaseAction {
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (request.isMode("select")) { /** 회원목록 조회 */
			return selectUserList(request);
		} else if (request.isMode("save")) { /** 회원저장 */
			return saveUser(request);
		} else if (request.isMode("delete")) { /** 회원삭제 */
			return deleteUser(request);
		} else if (request.isMode("sendUserVerifiNumber")) { /** 인증번호 전송 */
			return sendUserVerifiNumber(request);
		} else {
			return write(null);
		}
	}

	/**
	 * 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectUserList(IRequest request) throws Exception {
		JSONObject obj = new JSONObject();
		JSONArray jArray = new JSONArray();
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");

		Map map = null;
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("LOCALE", request.getUser() != null ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC));
			smap.put("dbName", dbName);


			List lists = getList("mg.vdr.selectUser" + getDBName(dbName), smap);

			for (int i = 0; i < lists.size(); i++) {
				
				map = (Map) lists.get(i);
				JSONObject listdata = new JSONObject();
				listdata.put("ROW_ID", String.valueOf(i + 1));
				
				for (int j = 0; j < cols.length; j++) {
					listdata.put(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}

				jArray.add(listdata);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			return write(obj.toString());
		} catch (Exception e) {
			obj.put("errmsg", e.toString());
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}

	/**
	 * 저장
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse saveUser(IRequest request) throws Exception {
		
		JSONObject obj = new JSONObject();
		JSONArray jArray = new JSONArray();
		
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		
		String user_id = request.getUser() != null ? request.getUser().getId() : "ixvdr@inzent.com";
		Locale loc = request.getUser() != null ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		String dbName = pm.getString("component.sql.database");

		

		try {
			
			tx.begin();

			for (int i = 0; i < ids.length; i++) {
				
				Map item = new HashMap();
				JSONObject listdata_save = new JSONObject();
				listdata_save.put("ROW_ID", String.valueOf(i + 1));
				
				for (int j = 0; j < cols.length; j++) {
					
					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
					listdata_save.put(cols[j], tmps);
					
				}
			
				if (pm.getBoolean("component.signon.password.encrypted")) {
					//사용자 정보 수정 시 비밀번호 변경을 원치 않을 때 null
					String password = (null == request.getStringParam(ids[i] + "_CHANGE"))? crypto.encode(this.getStringWithNullCheck(item, "PASSWD")) : null;
					item.put("PASSWD", password);
				}else{
					item.put("PASSWD", "");
				}
				
				// item.put("FUNCTION_CD","99");
				item.put("LOGIN_ID", user_id);
				item.put("WORK_CD", "C");
				item.put("EMAIL", item.get("USER_ID"));
				
				// "S04"
				if (!"".equals(item.get("ROLL_GRP_CD"))) {
					log.info("------------saveUser-------item-----------\n" + item.toString());
					if (Integer.parseInt(getItem("mg.vdr.countUser", item).toString()) > 0) {
						log.info("기존유저 업데이트");
						updateObject("mg.vdr.updateUser" + getDBName(dbName), item);
// 			       		// 권한등록
						log.info("기존유저 업데이트");
						updateObject("mg.sys.deleteAuth", item);
						createObject("mg.sys.insertAuth", item);
						log.info("기존유저 업데이트");
					} else {
						log.info("새유저 생성");
						createObject("mg.vdr.insertUser" + getDBName(dbName), item);
						// 권한등록
						// updateObject("mg.sys.deleteAuth", item);
						createObject("mg.sys.insertAuth" + getDBName(dbName), item);

						if (null != item.get("IS_INVITE") && "1".equals(item.get("IS_INVITE").toString())) {

							updateObject("mg.vdr.updateRoomJoin" + getDBName(dbName), item);

							createObject("mg.vdr.insertFolderAuth" + getDBName(dbName), item);

							createObject("mg.vdr.insertDocAuth" + getDBName(dbName), item);
						}
					}
				}
				jArray.add(listdata_save);
			}
			tx.commit();
			obj.put("data", jArray);
			obj.put("errmsg", mm.getMessage("COMG_1002", loc));
			obj.put("errcode", "0");
			log.info("------save-=----result------ > " + obj.toString());
			return write(obj.toString());

		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());

			obj.put("errmsg", mm.getMessage("COMG_1007", loc));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}

	/**
	 * 삭제
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse deleteUser(IRequest request) throws Exception {
		
		JSONObject obj = new JSONObject();
		JSONArray jArray = new JSONArray();
		JSONObject listdata;
		
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");

		try {
			
			 Map item = null;
			 tx.begin();
			 
			 for (int i = 0; i < ids.length; i++) {
				 
					 item = new HashMap();
					 listdata = new JSONObject();
				  	
					 for (int j = 0; j < cols.length; j++) {
						
						String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
						item.put(cols[j], tmps);
						listdata.put(cols[j], tmps);
					
					 }
					
					 listdata.put("ROW_ID", String.valueOf(i + 1));
					 item.put("LOGIN_ID", item.get("USER_ID"));
					 List<Map> lists = getList("mg.vdr.selectYesRoom"+ getDBName(pm.getString("component.sql.database")), item);
 				 
					 for(Map room : lists) {
						 
						  List deleteFileList = getList("mg.vdr.delete.seleteTFileUploadList", item);
						  
						  item.put("ROOM_ID", room.get("ROOM_ID"));
						  if(null == item.get("ROOM_ID")) throw new Exception();
						
					 	  if("Y".equals(room.get("ADMIN"))) item.put("D_USER_ID", null); //회원이 권리자인 룸
						  else 		   						item.put("D_USER_ID", item.get("USER_ID")); // 회원이 사용자인 룸

				          deleteObject("mg.vdr.delete.deleteTFileUpload", item);
				          deleteObject("mg.vdr.delete.deleteTrash", item);
				          deleteObject("mg.vdr.delete.deleteLog", item);
				          deleteObject("mg.vdr.delete.deleteFavorites", item);
				          deleteObject("mg.vdr.delete.deleteComment", item);
				          deleteObject("mg.vdr.delete.deleteDocAuth", item);
				          deleteObject("mg.vdr.delete.deleteDocHist", item);
				          deleteObject("mg.vdr.delete.deleteDoc", item);
				          deleteObject("mg.vdr.delete.deleteFolderAuth", item);
				          deleteObject("mg.vdr.delete.deleteFolder", item);
				          deleteObject("mg.vdr.delete.deleteRoomUser", item);
				          deleteObject("mg.vdr.delete.deleteRoomJoin", item);   
				          
				          if("Y".equals(room.get("ADMIN"))) {
				        	  deleteObject("mg.vdr.delete.deleteRoomAuthSet", item);
				        	  deleteObject("mg.vdr.delete.deleteRoom", item);
				          }

			          	  item.put("MOD_STOP", "2");
				          updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), item);
						  fum.deleteTFileUploadList(deleteFileList);
						         
					}  
		
					 deleteObject("mg.vdr.deleteUser", item);
					 jArray.add(listdata);
			 }
			 
			tx.commit();
			
			obj.put("data", jArray);
			obj.put("errmsg", mm.getMessage("COMG_1002"));
			obj.put("errcode", "0");
			return write(obj.toString());
		} catch (Exception e) {
			tx.rollback();
			obj.put("errmsg", mm.getMessage("COMG_1008"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}

	/**
	 * 인증번호 전송
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse sendUserVerifiNumber(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		Locale loc = (null != request.getUser()) ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);

		boolean isErrorExistUser = false;

		try {

			Map item = request.getMap();
			String user_id = item.get("user_id").toString();

			item.put("USER_ID", user_id);

			Map map = (Map) getItem("mg.vdr.selectUser" + getDBName(pm.getString("component.sql.database")), item);

			if (null != map) {
				isErrorExistUser = true;
				throw new Exception();
			}

			String verifiNumber = "";

			for (int i = 0; i < 6; i++) {
				verifiNumber += (int) (Math.random() * 10);
			}

			sendMail(user_id, verifiNumber, "", 6, (null == item.get("LANG")) ? "KO" : item.get("LANG").toString());

			JSONObject data = new JSONObject();

			data.put("verifiNumber", verifiNumber);

			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {

			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", (isErrorExistUser) ? mm.getMessage("COMG_1080", loc) : mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");

			return write(obj.toString());
		}
	}

}
