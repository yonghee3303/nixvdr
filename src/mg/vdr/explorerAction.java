package mg.vdr;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.component.crypto.ICryptoManagement;
import com.core.component.thirdparty.PolarisCryptoUtil;
import com.core.component.thirdparty.SynapViewer;
import com.core.component.upload.IFileUploadManagement;
import com.core.component.user.IUser;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import mg.base.BaseAction;
import mg.base.cryptorSHAEncodeThread;
import mg.base.cryptorSHAThread;
import mg.common.CpcConstants;
import mg.common.FolderTreePrivVO;
import mg.common.TreeMaker;
import mg.common.TreeVO;

public class explorerAction extends BaseAction 
{	
	
	/** 파일 업로드를 위한 정의 */
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
	private String viewerType = pm.getString("component.viewer.type");
	private String uploadEncrypt = pm.getString("component.upload.encrypt");
	
    /**
     * VDR 탐색기 Navigation 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception 
    {    
    	log.info("request isMode : "+request.getMap().toString());
        if(request.isMode("getFolderTree")) {   /** 폴더 목록 조회 */
            return getFolderTree(request);                                                
        }else if(request.isMode("getFolderTreeTrash")) { /** 폴더 목록 휴지통 조회 */
        	return getFolderTreeTrash(request);
        }else if(request.isMode("getFolderTreeAddbookmark")) { /** 폴더 목록 즐겨찾기 조회 */
        	return getFolderTreeAddbookmark(request);
        }else if(request.isMode("getDocList")) {   /** 폴더 문서 조회 */
            return getDocList(request);                                                
        }else if(request.isMode("getMyBookmark")) {   /** 나의 즐겨찾기 조회 */
            return getMyBookmarkList(request);  
        }else if(request.isMode("getTrash")) {   /** 휴지통 조회 */
            return getTrashList(request);  
        }else if(request.isMode("getDocMeta")) { /** 메터검색 조회 */
        	return getDocMeta(request);	
        }else if(request.isMode("getFolderTreeByPfid")) {   /** 특정 폴더의 하위 폴더 목록 조회 (By Parent folder id)*/
            return getFolderTreeByPFolderId(request);                                                
        }else if(request.isMode("createFolder")) {   /** 폴더 생성 */
            return createFolder(request);
        }else if(request.isMode("updateFolder")) {   /** 폴더(권한) 수정 */
            return updateFolder(request);
        }else if(request.isMode("updateFolderAuth")) {   /** 폴더 권한 수정 */
            return updateFolderAuth(request);
        }else if(request.isMode("updateFolderAuthMulti")) {   /** 폴더 권한 수정(Multiple) */
            return updateFolderAuthMulti(request);
        }else if(request.isMode("updateDocAuthMulti")) {   /** 문서 권한 수정(Multiple) */
            return updateDocAuthMulti(request);
        }else if(request.isMode("deleteFolder")) {   /** 폴더삭제*/
            return deleteFolder(request);
        }else if(request.isMode("restoreFolder")) {   /** 폴더복원*/
            return restoreFolder(request);
        }else if(request.isMode("removeDoc")) {   /** 문서삭제*/
            return removeDoc(request);
        }else if(request.isMode("restoreDoc")) {   /** 문서 복원*/
            return restoreDoc(request);
        }else if(request.isMode("trashCleanFolder")) {   /** 폴더 완전 삭제(휴지통 비우기)*/
            return trashCleanFolder(request);
        }else if(request.isMode("trashCleanDoc")) {   /** 문서 완전 삭제(휴지통 비우기)*/
            return trashCleanDoc(request);
        }else if(request.isMode("getCtxMenu")) {   /** 폴더및 문서의 Context Menu 조회*/
            return getContextMenu(request);
        }else if(request.isMode("tx_create")) {   /** Upload + 문서등록*/
            return createDoc(request);
        }else if(request.isMode("tx_update")) {   /** Upload + 문서리비전 - 기존문서 업로드 */
            return revisionDoc(request);
        }else if(request.isMode("updateDoc")) {   /** 폴더(권한) 수정 */
            return updateDoc(request);
        }else if(request.isMode("getDocument")) {   /** 문서  정보 조회 */
            return getDocument(request);                                                
        }else if(request.isMode("updateBookmark")) {   /** Update Bookmark(favorite) */
            return updateBookmark(request);                                                
        }else if(request.isMode("getFolderTreePriv")) {   /** 폴더 권한 가져오기(Tree) */
            return getFolderTreePriv(request);
        }else if(request.isMode("downloadCPD")) {   /** CPD 생성을 위한 CP Master 등록 */
            return makeCPD(request);  
        }else if(request.isMode("getFolderSubTree")) {   /** 선택된 폴더의 하위 경로를 가져옴 */
            return getFolderSubTree(request);  
        }else if(request.isMode("downloadFile")) {   /** 문서 다운로드 */
            return downloadFIle(request);  
        }else if(request.isMode("DECODE")) {   /** 복호화 테스트 */
            return TestDecode(request);  
        }else if(request.isMode("viewerDoc")) { /** 문서 뷰 */
        	return viewerDoc(request);
        }else if(request.isMode("thumbnailDoc")) { /** 문서 썸네일 */
        	return thumbnailDoc(request);
        }else if(request.isMode("pasteTarget")) { /** 대상 붙혀넣기 */
        	return pasteTarget(request);	
        }else if(request.isMode("pasteMove")) { /** 대상 잘라내기 */
        	return pasteMove(request);	
        }else if(request.isMode("pasteDoc")) { /** 문서 복사 */
        	return pasteDoc(request);	
        }else if(request.isMode("moveDoc")) { /** 문서 이동 */
        	return moveDoc(request);	
        }else if(request.isMode("sendAlarm")) { /** 알람 메세지 */
            return sendAlarm(request);
        }else if(request.isMode("getTimeLine")) { /** 타임라인 조회 */
        	return getTimeLine(request);	
        }else if(request.isMode("getFolderPath")) { /** 폴더 경로 조회 */
        	return getFolderPath(request);	
        }else if(request.isMode("selectSandBoxUserAuth")) { /** 유저별 샌드박스 권한을 가져온다. */
        	return selectSandBoxUserAuth(request);	
        }else if(request.isMode("selectFolderInfo")) { /** 폴더 정보 조회 */
        	return selectFolderInfo(request);	
        }else if(request.isMode("selectDocInfo")) { /** 문서 정보 조회 */
        	return selectDocInfo(request);	
        }else if(request.isMode("updateTargetAuth")) { /** 폴더 / 문서 권한 수정. */
        	return updateTargetAuth(request);	
        }else if(request.isMode("selectDocAuthInfo")) { /** 문서 권한 정보를 가져온다. */
        	return selectDocAuthInfo(request);	
        }else if(request.isMode("getUserInfo")) { /** 유저 정보를 가져온다. */
            return getUserInfo(request);
        }else if(request.isMode("getStorageCapacity")) { /** 저장용량을 가져온다. */
        	return getStorageCapacity(request);	
        }else if(request.isMode("getSandboxVersion")) { /** 샌드박스 버전을 체크한다. */
        	return getSandboxVersion(request);	
        }else if(request.isMode("sandboxDownload")) { /** 샌드박스를 다운로드한다. */
        	return sandboxDownload(request);	
        }else if(request.isMode("testCode")) { /** 테스트를 하기 위함 */
        	return testCode(request);
        }else if(request.isMode("NimsouCreateFolder")) { /** NIMSOU 자료실 폴더생성*/
        	return NimsouCreateFolder(request);
        }else if(request.isMode("NimsouDeleteFolder")) { /** NIMSOU 자료실 폴더삭제*/
        	return NimsouDeleteFolder(request);
        }else if(request.isMode("NimsouGetFolderAuth")) { /** NIMSOU 자료실 폴더목록 조회*/
        	return NimsouGetFolderSetAuth(request);
        }else if(request.isMode("NimsouFolderAuthSet")) { /** NIMSOU 자료실 폴더권한*/
        	return NimsouFolderAuthSet(request);
        }else{ 
            return write(null);
        }
    }
    


	private IResponse TestDecode(IRequest request) throws Exception 
    {
    	
    	
    	String src = request.getStringParam("src");
    	
    	 log.info("------TestDecode---- > \n" + src);
    	 log.info("------pm.getString(\"cliptorplus.auth.key\")---- > \n" + pm.getString("cliptorplus.auth.key"));
    	
    	 String tmppw = crypto.decodeAES(src, pm.getString("cliptorplus.auth.key"));
    	 
    	 String comppw = crypto.encode(tmppw);
    	 
    	 String  password  = (String)getItem("mg.cp.getPasswordSysuser",request.getMap());  
    	 
    	 log.info("------TestDecode---- > \n" + comppw + " ---- " + password);
    	 
    	 if(password.equals(comppw)) {
    		 return write("비밀번호 일치");
    	 }
    	 else {	 
    		 return write("비밀번호 불일치");
    	 }
    }
    
    private JSONObject makeTag(Map mp)
    {
    	JSONObject item = new JSONObject();
    	item.put("FOLDER_PATH", mp.get("FOLDER_PATH"));
    	item.put("FOLDER_STYLE", mp.get("FOLDER_STYLE"));
    	return item;
    }
        
    private List<TreeVO> makefolder(List lists) {
		// covert lists to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.size(); i++) {
			Map mp = (Map) lists.get(i);
			// JSONObject data = new JSONObject();
			TreeVO tVO = new TreeVO();
			tVO.setKey((String) mp.get("FOLDER_ID"));
			tVO.setParent((String) mp.get("P_FOLDER_ID"));
			tVO.setTitle((String) mp.get("FOLDER_NM"));
			tVO.setTag(makeTag(mp));
			tVO.setACES((String) mp.get("ACES"));
			tVO.setFAD((String) mp.get("FAD"));
			tVO.setRED((String) mp.get("RED"));
			tVO.setWRT((String) mp.get("WRT"));
			tVO.setPRT((String) mp.get("PRT"));
			tVO.setDLT((String) mp.get("DLT"));
			tVO.setDOWN((String) mp.get("DOWN"));
			
			lst.add(tVO);
		}
		return lst;
    }
    /**
     * covert lists to List<FolderTreePrivVO>
     * @param lists
     * @return
     */
    private List<FolderTreePrivVO> makefolderPriv(List lists) {
		List<FolderTreePrivVO> lst = new ArrayList<FolderTreePrivVO>();
		for (int i = 0; i < lists.size(); i++) {
			Map mp = (Map) lists.get(i);
			FolderTreePrivVO tVO = new FolderTreePrivVO();
			tVO.setKey((String) mp.get("FOLDER_ID"));
			tVO.setParent((String) mp.get("P_FOLDER_ID"));
			tVO.setTitle((String) mp.get("FOLDER_NM"));
			tVO.setACES((String)mp.get("ACES"));
			tVO.setFAD((String)mp.get("FAD"));
			tVO.setRED((String)mp.get("RED"));
			tVO.setWRT((String)mp.get("WRT"));
			tVO.setPRT((String)mp.get("PRT"));
			tVO.setDLT((String)mp.get("DLT"));
			tVO.setDOWN((String)mp.get("DOWN"));
			tVO.setTag(makeTag(mp));
			lst.add(tVO);
		}
		return lst;
    }
        
    private JSONObject makeRoom(Map mp)
    {
    	JSONObject item = new JSONObject();
    	item.put("key", (String) mp.get("ROOM_ID"));
    	item.put("parent", "");
    	item.put("isFolder",true);
    	item.put("title", (String) mp.get("ROOM_NM"));
    	item.put("tag", "");
    	item.put("isRegUser", (null != mp.get("ISREGUSER") && "1".equals(mp.get("ISREGUSER").toString()))? "1" : "0");
    	item.put("view", mp.get("VIEW"));
    	item.put("edit", mp.get("EDIT"));
    	item.put("max_auth", mp.get("MAX_AUTH"));
    	
    	return item;
    }
    
    /**
    * 탐색기의 폴더 조회 (by folder id)
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getFolderTreeByPFolderId(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();

     	Map smap = request.getMap();
     	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
		smap.put("LOCALE", loc );
		String dbName = pm.getString("component.sql.database");
		smap.put("dbName", dbName);
		String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
		smap.put("user_id", user_id);
		
        try {
        	
        	List lists;
        	List listsPf;
        	if(dbName.equals("mssql")){
        		listsPf = getList("mg.vdr.selectFolderTreeByFid",smap);
        	}else{
        		listsPf = getList("mg.vdr.selectFolderTreeByFid_" + dbName, smap);  // postgre
        	}

        	if(dbName.equals("mssql")){
        		lists = getList("mg.vdr.selectFolderTreeByPfid",smap);
        	}else{
        		lists = getList("mg.vdr.getFolderSubTree_" + dbName, smap);
        	}
        	lists.addAll(listsPf);
        	
        	log.info("------getFolderTree---lists--- > \n" + lists.toString());
        	 
        	// JSONArray jArray = new JSONArray();
        	TreeMaker tm = new TreeMaker();

		 	 List<TreeVO> lst = makefolder(lists);
		 	log.info("------getFolderTree---lst--- > \n" + lst.toString());
		 	
			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
			 
			 log.info("------getFolderTree------ > \n" + treeData.toString());
			 			 
			 /*	 String roomId = request.getStringParam("ROOM_ID");
		 	 Map r_map = (Map)getItem("mg.vdr.getRoomInfo", smap);
			 log.info("------getFolderTree---r_map--- > \n" + r_map.toString());*/
			 
	         obj.put("data", treeData);
			 obj.put("errmsg", mm.getMessage("COMG_1002", loc));
			 obj.put("errcode", "0");
			 
			 
			 
			  ObjectMapper mapper = new ObjectMapper();
			  String str = "";
			  try {
					str = mapper.writeValueAsString(obj);
			  }
			  catch (IOException ex) {
					log.error("[getFolderTree] " + ex.getMessage());
			   }
			   log.info("------getFolderTree-=----str------ > " + str);
			 
			 
			return write(str);
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    } 
    /**
    * 탐색기의 폴더 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getFolderTree(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();
     	// String col = request.getStringParam("grid_col_id");
     	// String[] cols = col.split(",");

     	Map smap = request.getMap();
     	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
		smap.put("LOCALE", loc );
		String dbName = pm.getString("component.sql.database");
		smap.put("dbName", dbName);
		
		String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
		smap.put("USER_ID", user_id);
		// log.info("------getFolderTree---IRequest--- > \n" + smap.toString());
        
        try {
        	
        	List lists;
       		lists = getList("mg.vdr.selectFolerTree",smap);
            
        	
//        	if(dbName.equals("mssql")){
//        		lists = getList("mg.vdr.selectFolerTree",smap);
//        	}else{
//        		lists = getList("mg.vdr.selectFolerTree_" + dbName, smap);
//        	}
        	
        	log.info("------getFolderTree---lists--- > \n" + lists.toString());
        	 
        	JSONArray jArray = new JSONArray();
        	TreeMaker tm = new TreeMaker();

		 	 List<TreeVO> lst = makefolder(lists);
			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
			 			 
			 Map r_map = (Map)getItem("mg.vdr.getRoomInfo", smap);
			 
			 log.info("------getFolderTree---r_map--- > \n" + r_map.toString());
			 
			 JSONObject root = new JSONObject();
			 root = makeRoom(r_map);
			 root.put("children", treeData);
			 
			 jArray.add(root);
                
	         obj.put("data", jArray);
			 obj.put("errmsg", mm.getMessage("COMG_1002", loc));
			 obj.put("errcode", "0");
			 
			 log.info("------getFolderTree------ > \n" + obj.toString());
			 
			  ObjectMapper mapper = new ObjectMapper();
			  String str = "";
			  try {
					str = mapper.writeValueAsString(obj);
			  }
			  catch (IOException ex) {
					log.error("[getFolderTree] " + ex.getMessage());
			   }
				
			   log.info("------getFolderTree-=----str------ > " + str);
			 
			 
			return write(str);
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    } 
    
    /**
    * 폴더 목록 휴지통 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getFolderTreeTrash(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();

     	Map smap = request.getMap();
		
		String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";

		smap.put("USER_ID", user_id);
        
        try {
        	
        	List lists = getList("mg.vdr.getFolderTreeTrash", smap);
        	 
        	JSONArray jArray = new JSONArray();
        	TreeMaker tm = new TreeMaker();

		 	 List<TreeVO> lst = makefolder(lists);
			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
			 			 
			 Map r_map = (Map) getItem("mg.vdr.getRoomInfo", smap);
			 
			 JSONObject root = new JSONObject();
			 
			 root = makeRoom(r_map);
			 
			 root.put("children", treeData);
			 
			 jArray.add(root);
                
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 
			 ObjectMapper mapper = new ObjectMapper();
			 
			 String str = "";
			 
			 try {
				 str = mapper.writeValueAsString(obj);
			 }catch (IOException ex) {
				 log.error("[getFolderTree] " + ex.getMessage());
			 }
			 
			 log.info("------getFolderTree-=----str------ > " + str);
			 
			 return write(str);			 
			 
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1001"));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    }    
    
    /**
    * 폴더 목록 즐겨찾기 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getFolderTreeAddbookmark(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();

     	Map smap = request.getMap();
		
		String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";

		smap.put("USER_ID", user_id);
        
        try {
        	
        	List lists = getList("mg.vdr.getFolderTreeAddbookmark", smap);
        	 
        	JSONArray jArray = new JSONArray();
        	TreeMaker tm = new TreeMaker();

		 	 List<TreeVO> lst = makefolder(lists);
			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
			 			 
			 Map r_map = (Map) getItem("mg.vdr.getRoomInfo", smap);
			 
			 JSONObject root = new JSONObject();
			 
			 root = makeRoom(r_map);
			 
			 root.put("children", treeData);
			 
			 jArray.add(root);
                
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 
			 ObjectMapper mapper = new ObjectMapper();
			 
			 String str = "";
			 
			 try {
				 str = mapper.writeValueAsString(obj);
			 }catch (IOException ex) {
				 log.error("[getFolderTree] " + ex.getMessage());
			 }
			 
			 log.info("------getFolderTree-=----str------ > " + str);
			 
			 return write(str);			 
			 
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1001"));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    }        
    
    /**
     * 탐색기의 문서목록 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getDocList(IRequest request) throws Exception 
     {
    	 
    	 String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	 
    	 JSONObject obj = new JSONObject();
      	 String col = request.getStringParam("grid_col_id");
      	 String[] cols = col.split(",");
      	 String authOnly = request.getStringParam("authonly");
      	 String dbName = pm.getString("component.sql.database");
      	 
      	 Map smap = request.getMap();
      	 
      	 try {
      		 
      		 if(authOnly != null && "true".equals(authOnly ))  user_id = request.getStringParam("R_USER_ID");
      		 
      		 smap.put("R_USER_ID", user_id);
      		 
      		 List lists = null;
      		
      		 if(null != smap.get("SEARCH_FILE_NAME") && 0 < smap.get("SEARCH_FILE_NAME").toString().trim().length()) {
      			 lists = getList("mg.vdr.getDocSearchList" + getDBName(dbName), smap);
      		 }else{
      			 lists = getList("mg.vdr.getDocList" + getDBName(dbName), smap);
      		 }
      		 
      		 JSONArray jArray = new JSONArray();
      		 
      		 for (int i = 0; i < lists.size(); i++) {
      			 
      			 Map map =(Map) lists.get(i);
      			 
      			 JSONObject listdata = new JSONObject();
      			 
      			 listdata.put("ROW_ID", String.valueOf(i + 1));
      			 
      			 for(int j = 0; j < cols.length; j++){
      				 listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
      			 }
      			 
      			 jArray.add(listdata);
      		 }
      		 
      		 obj.put("data", jArray);
 			 obj.put("errmsg", "success");
 			 obj.put("errcode", "0");
 			 return write(obj.toString());
 	     }catch(Exception e) {
 			 obj.put("errmsg", mm.getMessage("COMG_1001"));
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
     } 
     
     /**
      * 폴더 생성
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse createFolder(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
    	 
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         String dbName = pm.getString("component.sql.database");

         try {
        	
        	tx.begin();
        	
        	Map item = request.getMap();
        	
        	item.put("FOLDER_NM",  URLDecoder.decode((String) item.get("FOLDER_NM"), "UTF-8"));
        	item.put("USERID",  user_id);
        	item.put("OWNER_ID",  user_id);
       	    item.put("FOLDER_STYLE", "folder");
	       	item.put("CREATOR_ID", user_id);
	       	item.put("MODIFIER_ID", user_id);
	       	item.put("R_USER_ID", user_id);
	       	item.put("IS_DELETE", "N");
	       	item.put("FOLDER_ORDER", 0);
       	    
 	       	String folderId = getMakeId("F");
 	        
 	       	item.put("FOLDER_ID", folderId);	
 	        
 	       	log.info("----------------createFolder-------------------------------------\n" + item.toString());
 	        
 	       	createObject("mg.vdr.createFolder" + getDBName(dbName), item);

 	        createObject("mg.vdr.createFolderAuth" + getDBName(dbName), item);
 	        
 	       if(null != item.get("FAVORITE_YN")) updateObject("mg.vdr.updateBookmark", item);
 	        
 	        if(null != item.get("UPDATE_LIST")) {
 	        	
 	        	item.put("USER_AUTH", "Y");
 	        	
 	        	updateObject("mg.vdr.updateFolderUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
 	        	
 	        	updateObject("mg.vdr.updateFolderUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
 	        	
 	        	JSONParser parser = new JSONParser();
 	        	
 	        	JSONArray update_list = (JSONArray) parser.parse(URLDecoder.decode((String) item.get("UPDATE_LIST"), "UTF-8"));
 	        	
 	        	for(int i = 0; i < update_list.size(); i++) {
 	        		
 	        		Map param = new ObjectMapper().readValue(((JSONObject) update_list.get(i)).toJSONString(), Map.class);
 	        		
 	        		param.put("FOLDER_ID", folderId);
 	        		
 	        		updateObject("mg.vdr.updateFolderUserAuth" + getDBName(pm.getString("component.sql.database")), param);
 	        	}
 	        }else{
 	        	
 	        	Map pFolderInfo = (Map) getItem("mg.vdr.selectFolderInfo" + getDBName(pm.getString("component.sql.database")), new HashMap() {{put("FOLDER_ID", item.get("P_FOLDER_ID"));}});
 	        	
 	        	if(null != pFolderInfo && "Y".equals(pFolderInfo.get("USER_AUTH").toString())){
 	        		
 	        		item.put("USER_AUTH", "Y");
 	 	        	updateObject("mg.vdr.updateFolderUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
 	 	        	
 	 	        	updateObject("mg.vdr.updateFolderUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
 	 	        	
 	 	        	item.put("ACCESS", "Y");
 	 	        	item.put("VIEW", "Y");
 	 	        	item.put("EDIT", "Y");
 	 	        	
 	 	        	updateObject("mg.vdr.updateFolderUserAuth" + getDBName(pm.getString("component.sql.database")), item);
 	        	}
 	        }

 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");

 			item.put("LOG_CL_CD", "FC");
 			item.put("USER_ID", user_id);
 			
 			insertAccessLog(item);
 			
 			tx.commit();
 			
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
    
     /**
      * 폴더 변경
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateFolder(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         String dbName = pm.getString("component.sql.database");
         try {
 	        Map item = request.getMap();
 	        // FOLDER_ID, #FOLDER_NM#,#P_FOLDER_ID#,#FOLDER_STYLE#,#FOLDER_ORDER#,
        	item.put("LOGIN_ID",  user_id);
        	item.put("FOLDER_ORDER", Integer.valueOf((String)item.get("FOLDER_ORDER")));
       	    	
 	        log.info("----------------updateObject-------------------------------------\n" + item.toString());
 	        updateObject("mg.vdr.updateFolder"+getDBName(dbName), item);
 	        
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 			
 			item.put("LOG_CL_CD", "FN");
 			item.put("USER_ID", user_id);

 			insertAccessLog(item);	
 			
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * 폴더 권한 변경
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateFolderAuth(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         String dbName = pm.getString("component.sql.database");
         try {
 	        Map item = request.getMap();
        	item.put("LOGIN_ID",  user_id);
        	// ROOM_ID, FOLDER_ID, R_USER_ID
        	// #ACES#,  	FAD = #FAD#, 	RED = #RED#,WRT = #WRT#,PRT = #PRT#,DLT = #DLT#,DOWN = #DOWN#,
        	
 	        updateObject("mg.vdr.updateFolderAuth"+getDBName(dbName), item);

 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 	        return write(obj.toString());
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * 폴더 삭제
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse deleteFolder(IRequest request) throws Exception 
     {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	 
    	 try {
    		 
    		 tx.begin();
    		 
    		 Map item = request.getMap();
    		 
    		 item.put("R_USER_ID", user_id);
    		 item.put("FOLDER_IDS_LIST", item.get("FOLDER_IDS").toString().split(","));
    		 item.put("isTotal", true);
    		 
    		 List lists = getList("mg.vdr.getFolderChildFullTarget" + getDBName(pm.getString("component.sql.database")), item);
    		 
    		 List<String> DOC_IDS_LIST = new ArrayList<String>();

    		 for(int i = 0; i < lists.size(); i++) {
    			 
    			 Map map = (Map) lists.get(i);
    			 
    			 if("FOLDER".equals(map.get("TYPE").toString())) continue; 

    			 DOC_IDS_LIST.add(map.get("DOC_ID").toString());
    		 }    		 

      		 for (int i = 0; i < lists.size(); i++) {
      			 
      			 Map map = (Map) lists.get(i);
      			 
      			 map.put("R_USER_ID", item.get("R_USER_ID"));
      			 
      			 if("FOLDER".equals(map.get("TYPE").toString())){
      				 updateObject("mg.vdr.removeFolder", map);
      				 createObject("mg.vdr.createTrash", map);
      			 }else{

      	    		 updateObject("mg.vdr.removeDoc", map);
      	    		 createObject("mg.vdr.createTrash", map);
      	    		 
      	    		 map.remove("R_USER_ID");
      	    		 
      	    		 updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), map);
      	    		 
      	    		 ObjectMapper mapper = new ObjectMapper();
      	    		 
      	    		 JSONParser parser = new JSONParser();
      	    		 
      	    		 List cpMasterList = getList("mg.cp.selectCPMaster" + getDBName(pm.getString("component.sql.database")), map);      	    		 
      	    		
      	    		 for(int j = 0; j < cpMasterList.size(); j++) {
      	    			 
      	    			 Map cpMasterMap = (Map) cpMasterList.get(j);
      	    			 
      	    			 JSONObject jsonObj = (JSONObject) parser.parse(cpMasterMap.get("FILE_LIST").toString());
      	    			 
      	    			 JSONArray jsonArrayDocs = (JSONArray) parser.parse(jsonObj.get("docs").toString());
      	    			 
      	    			 if(0 < jsonArrayDocs.size()) {
      	    				 
      	    				 JSONArray updateJsonArrayDocs = new JSONArray();
      	    				 
      	    				 for(int k = 0; k < jsonArrayDocs.size(); k++) {
      	    					 
      	    					 JSONObject changeObj = (JSONObject) parser.parse(jsonArrayDocs.get(k).toString());

      	    					 if(DOC_IDS_LIST.contains(changeObj.get("DOC_ID").toString())) {
      	    						 changeObj.put("DEL_YN", "Y");
      	    					 }
      	    					 
      	    					 updateJsonArrayDocs.add(changeObj);
      	    				 }
      	    				 
      	    				 jsonObj.put("docs", updateJsonArrayDocs);
      	    				 
      	    				 map.put("FILE_LIST", mapper.writeValueAsString(jsonObj));
      	    				 map.put("DOC_NO", cpMasterMap.get("DOC_NO"));
      	    				 
      	    				 updateObject("mg.cp.updateCPMaster" + getDBName(pm.getString("component.sql.database")), map);
      	    			 }
      	    		 }
      			 }
      		 }
      		 
      		 List folderInfoList = getList("mg.vdr.getFolderInfoList" + getDBName(pm.getString("component.sql.database")), item);
      		 
      		 for(int i = 0; i < folderInfoList.size(); i++) {
      			 
      			 Map folderInfo = (Map) folderInfoList.get(i);
      			 
      			 folderInfo.put("LOG_CL_CD", "FD");
      			 folderInfo.put("USER_ID", user_id);
      			 folderInfo.put("LANG", item.get("LANG"));
        		 
        		 insertAccessLog(folderInfo);
      		 }
    		 
    		 obj.put("errmsg", "success");
    		 obj.put("errcode", "0");
    		 
    		 tx.commit();
    		 
    		 return write(obj.toString());
    	 }catch(Exception e) {
    		 tx.rollback();
    		 obj.put("errmsg", mm.getMessage("COMG_1007"));
    		 obj.put("errcode", "-1");
    		 return write(obj.toString());
    	 }
     }
     
     /**
      * 폴더 복원
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse restoreFolder(IRequest request) throws Exception 
     {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	 
    	 try {
    		 
    		 tx.begin();
    		 
    		 Map item = request.getMap();
    		 
    		 item.put("R_USER_ID", user_id);
    		 item.put("FOLDER_IDS_LIST", item.get("FOLDER_IDS").toString().split(","));
    		 item.put("isTotal", true);
    		 
    		 List lists = getList("mg.vdr.getFolderChildFullTarget" + getDBName(pm.getString("component.sql.database")), item);
    		 
    		 List<String> DOC_IDS_LIST = new ArrayList<String>();

    		 for(int i = 0; i < lists.size(); i++) {
    			 
    			 Map map = (Map) lists.get(i);
    			 
    			 if("FOLDER".equals(map.get("TYPE").toString())) continue; 

    			 DOC_IDS_LIST.add(map.get("DOC_ID").toString());
    		 }
    		 
      		 for (int i = 0; i < lists.size(); i++) {
      			 
      			 Map map = (Map) lists.get(i);
      			 
      			 map.put("R_USER_ID", item.get("R_USER_ID"));
      			 
      			 if("FOLDER".equals(map.get("TYPE").toString())){
      				 updateObject("mg.vdr.restoreFolder", map);
      				 deleteObject("mg.vdr.deleteTrash", map);
      			 }else{
      				 updateObject("mg.vdr.restoreDoc", map);
      				 deleteObject("mg.vdr.deleteTrash", map);
      				 
      	    		 map.remove("R_USER_ID");
      	    		 
      	    		 updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), map);
      	    		 
      	    		 ObjectMapper mapper = new ObjectMapper();
      	    		 
      	    		 JSONParser parser = new JSONParser();
      	    		 
      	    		 List cpMasterList = getList("mg.cp.selectCPMaster" + getDBName(pm.getString("component.sql.database")), map);      	    		 
      	    		
      	    		 for(int j = 0; j < cpMasterList.size(); j++) {
      	    			 
      	    			 Map cpMasterMap = (Map) cpMasterList.get(j);
      	    			 
      	    			 JSONObject jsonObj = (JSONObject) parser.parse(cpMasterMap.get("FILE_LIST").toString());
      	    			 
      	    			 JSONArray jsonArrayDocs = (JSONArray) parser.parse(jsonObj.get("docs").toString());
      	    			 
      	    			 if(0 < jsonArrayDocs.size()) {
      	    				 
      	    				 JSONArray updateJsonArrayDocs = new JSONArray();
      	    				 
      	    				 for(int k = 0; k < jsonArrayDocs.size(); k++) {
      	    					 
      	    					 JSONObject changeObj = (JSONObject) parser.parse(jsonArrayDocs.get(k).toString());
      	    					
      	    					 if(DOC_IDS_LIST.contains(changeObj.get("DOC_ID").toString())) {
      	    						 changeObj.put("DEL_YN", "N");
      	    					 }
      	    					 
      	    					 updateJsonArrayDocs.add(changeObj);
      	    				 }
      	    				 
      	    				 jsonObj.put("docs", updateJsonArrayDocs);
      	    				 
      	    				 map.put("FILE_LIST", mapper.writeValueAsString(jsonObj));
      	    				 map.put("DOC_NO", cpMasterMap.get("DOC_NO"));
      	    				 
      	    				 updateObject("mg.cp.updateCPMaster" + getDBName(pm.getString("component.sql.database")), map);
      	    			 }
      	    		 }      	    		 
      			 }
      		 }
    		 
    		 obj.put("errmsg", "success");
    		 obj.put("errcode", "0");
    		 
    		 tx.commit();
    		 
    		 return write(obj.toString());
    	 }catch(Exception e) {
    		 tx.rollback();
    		 obj.put("errmsg", mm.getMessage("COMG_1007"));
    		 obj.put("errcode", "-1");
    		 return write(obj.toString());
    	 }
     }     
     
     /**
      * 탐색기의  ConextMenu 리스트 조회
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse getContextMenu(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
       	  
    	  Map smap = request.getMap();
       	  
  		  String user_id = request.getUser() != null ? request.getUser().getId() : "admin";
  		  
  		  smap.put("USER_ID", user_id);
  		  
          try {
        	  
        	  if(null == smap.get("TYPE")) throw new Exception();
        	  
        	  String TYPE = smap.get("TYPE").toString();
        	  
        	  List authMenuList = new ArrayList();
        	  
        	  smap.put("MENU_ID", "CTX");
        	  
          	  List lists = getList("mg.base.selectMenu" + getDBName(pm.getString("component.sql.database")), smap);
        	  
    		  smap.put("SELECT_AUTH", "room");
    		  
    		  Map userAuthInfo = (Map) getItem("mg.vdr.getRoomParticipantsList" + getDBName(pm.getString("component.sql.database")), smap);
    		  
    		  boolean view = "Y".equals(userAuthInfo.get("VIEW").toString());
    		  boolean edit = "Y".equals(userAuthInfo.get("EDIT").toString());

			  String[] selectedIds = smap.get("ids").toString().split(",");
			  
    		  if("0".equals(smap.get("ISSELECTED").toString())) {
    			  if("main".equals(TYPE) || "bookmark".equals(TYPE)){
    				  
    				  if(selectedIds[0].startsWith("R")){
    					  
    					  if(edit) {
    						  authMenuList.add("CTX_0021"); //신규폴더
    						  authMenuList.add("CTX_0002"); //붙여넣기
    					  }
    					  
    				  }else if(selectedIds[0].startsWith("F")){
    					  
    					  smap.put("FOLDER_ID", selectedIds[0]);
    					  
    					  Map folderInfo = (Map) getItem("mg.vdr.selectFolderInfo" + getDBName(pm.getString("component.sql.database")), smap);
    					  
    					  if("Y".equals(folderInfo.get("USER_AUTH").toString())){
    						  
    						  Map folderAuthInfo = (Map) getItem("mg.vdr.selectFolderAuthInfo" + getDBName(pm.getString("component.sql.database")), smap);
    						  
    						  if("Y".equals(folderAuthInfo.get("EDIT").toString())) {
    							  authMenuList.add("CTX_0021"); //신규폴더
        						  authMenuList.add("CTX_0022"); //업로드
        						  authMenuList.add("CTX_0002"); //붙여넣기
    						  }
    					  }else{
    						  if(edit){
    							  authMenuList.add("CTX_0021"); //신규폴더
        						  authMenuList.add("CTX_0022"); //업로드  
        						  authMenuList.add("CTX_0002"); //붙여넣기
    						  }
    					  }
    				  }
    			  }
    		  }else{
    			  
    			  List<String> folderIdList = new ArrayList();
    			  List<String> docIdList = new ArrayList();
    			  
    			  for(String selectedId : selectedIds) {
    				  if(selectedId.startsWith("F")) folderIdList.add(selectedId);
    				  else 						     docIdList.add(selectedId);
    			  }
    			  
    			  smap.put("FOLDER_IDS_LIST", folderIdList);
    			  smap.put("DOC_IDS_LIST", docIdList);
    			  
    			  boolean isExistFolder = (0 < folderIdList.size())? true : false;
    			  boolean isExistDoc = (0 < docIdList.size())? true : false;
    			  boolean isExistTotal = isExistFolder && isExistDoc;
    			  
    			  smap.put("isExistFolder", isExistFolder);
    			  smap.put("isExistDoc", isExistDoc);
    			  smap.put("isExistTotal", isExistTotal);
    			  
    			  Map totalUserAuthInfo = (Map) getItem("mg.vdr.getTotalUserAuth" + getDBName(pm.getString("component.sql.database")), smap);
    			  
    			  int totalSize = folderIdList.size() + docIdList.size();
    			  int userAuthSize = NumberUtils.toInt(totalUserAuthInfo.get("CNT").toString());
    			  
    			  if(0 < userAuthSize) {
    				  if(totalSize == userAuthSize){
    					  view = "Y".equals(totalUserAuthInfo.get("VIEW").toString());
        				  edit = "Y".equals(totalUserAuthInfo.get("EDIT").toString());    					  
    				  }else{
    					  view = view && "Y".equals(totalUserAuthInfo.get("VIEW").toString());
        				  edit = edit && "Y".equals(totalUserAuthInfo.get("EDIT").toString());    					  
    				  }
    			  }
    			  
    			  if("main".equals(TYPE) || "bookmark".equals(TYPE)){
    				
        			  if(1 == (folderIdList.size() + docIdList.size())) authMenuList.add("CTX_0007"); //이름 바꾸기
        			  authMenuList.add("CTX_0013"); //즐겨찾기
        			  authMenuList.add("CTX_0014"); //즐겨찾기 해제
        			  
        			  if(view) {
        				  authMenuList.add("CTX_0004"); //다운로드
        				  if(1 == (folderIdList.size() + docIdList.size())) authMenuList.add("CTX_0017"); //문서이력
        			  }
        			  
        			  if(edit) {
        				  authMenuList.add("CTX_0001"); //복사
        				  authMenuList.add("CTX_0003"); //붙여넣기
        				  authMenuList.add("CTX_0008"); //삭제        				  
        				  
        				  if(1 == folderIdList.size()) {
        					  authMenuList.add("CTX_0002"); //붙여넣기
        				  }
        			  }
        			  
        			  Map adminUserMap = (Map) getItem("mg.vdr.getRoomAdminList" + getDBName(pm.getString("component.sql.database")), smap);
        			  
        			  if(null != adminUserMap) {
        				  if(1 == (folderIdList.size() + docIdList.size())) authMenuList.add("CTX_0015"); //권한
        			  }
        			  
    			  }else if("trash".equals(TYPE)){
    				  if(edit){
    					  authMenuList.add("CTX_0019");
    	    			  authMenuList.add("CTX_0020");    					  
    				  }
    			  }
    		  }
          	  
 	          for (int i = 0; i < lists.size(); i++) { 	        	  
 	        	  Map map =(Map) lists.get(i); 	        	  
 	        	  if(!authMenuList.contains(map.get("MENU_ID").toString())) continue;
 	        	  obj.put(this.getStringWithNullCheck(map,"MENU_URL"), this.getStringWithNullCheck(map,"USE_YN"));
 	          }
  			 
 	          obj.put("errmsg", "success");
  			  obj.put("errcode", "0");
  			 return write(obj.toString());
  	     } catch(Exception e) {
  	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
  			 obj.put("errmsg", mm.getMessage("COMG_1001"));
  			 obj.put("errcode", "-1");
  	    	 return write(obj.toString());
  	     }
      } 
     
     
     /**
      * 문서 삭제
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse deleteDoc(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         
         try {
 	        Map item = request.getMap();
 	        // #DOC_ID#,#R_USER_ID#,#ROOM_ID#

 	        log.info("----------------deleteDocAuth-------------------------------------\n" + item.toString());
 	        deleteObject("mg.vdr.deleteDocAuth", item);
 	        
 	        log.info("----------------deleteDoc-------------------------------------\n" + item.toString());
	        deleteObject("mg.vdr.deleteDoc", item);


 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 			
 			item.put("LOG_CL_CD", "DD");
 			item.put("USER_ID", user_id);
 			
	        insertAccessLog(item);
	        
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     
     /**
      *  문서 리비전 (기존 문서 Update) 
      * @param request
      * @return
      * @throws Exception
      *  
      */
     private IResponse revisionDoc(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
    	 String user_id =  request.getUser() != null ? request.getUser().getId() :  request.getStringParam("R_USER_ID");
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         
    	 try {
    		 tx.begin();  //jhmun 임시
    		 
    		 // 리비전은 신규문서가 등록되고 등록된 ID(UUID, TFILE_UPLOAD 테이블에는 doc_id) 가 VDR_DOC에 갱신된다.
    		 // VDR_DOC 에는 VER 가 올라가고 - VDR_DOC_HIST에 기존 버전정보가 등록된다.
    		 // 1.0   ==> Left는 사용자에 의하여 버전 업,  Right는 시스템에 의한 버전업 

        	String dbName = pm.getString("component.sql.database");
 	        Map item = request.getMap();
 	        
 	        String uuid = getMakeId("D"); // UUID
 	        
 	        String filename = request.getStringParam("file_file");
 	        log.info("filename : "+ filename);
 	        
 	        if(filename == null){
				 log.info("-------------------------\n  revisionDoc : file name is null. \n");
 	        }
	 	    
 	        int pos = filename.lastIndexOf( "." );
 	        
	 	    String ext = filename.substring( pos + 1 );
	 	   
	 	    // ROOM_ID 찾기  --> pgmId
	 	    Map r_map = (Map)getItem("mg.vdr.getDocumentInfo", item);
		 	String pgmId = this.getStringWithNullCheck(r_map, "ROOM_ID"); 
		 	
		 	log.info("createUpload pgmId : "+pgmId+", uuid : "+uuid );	 	
		 	
		 	request.addRequestObject("viewerType", viewerType);
 	        fum.createUpload(pgmId, uuid, request);
 	        
 	        int doc_size = Integer.parseInt(request.getStringParam("file_size"));
 	        
 	        //',#FOLDER_ID#, #ROOM_ID#, R_USER_ID
 	        // DOC_ID 와 UUID 는 최초 동일 하고 버전업 될떄 UUID는 바뀐다.
 	        // item.put("DOC_ID",  docId);  --> Request에서 넣어 줌
 	        item.put("UUID", uuid);
 	        item.put("DOC_NM",  filename);
 	        item.put("DOC_SIZE", doc_size );
 	        item.put("DOC_EXTENSION",  ext);
        	item.put("OWNER_ID",  user_id);
	       	item.put("CREATOR_ID", user_id);
	       	item.put("MODIFIER_ID", user_id);
	       	item.put("IS_DELETE", "N");
	       	
	       	String tmpver = this.getStringWithNullCheck(r_map, "VER"); 
	       	
	       	String[] vers = tmpver.split("\\.");
	       	
	        int u_ver=1;
	       	
	        if(vers.length > 1)  u_ver = Integer.parseInt(vers[1]) + 1;
	       	
	       	String ver = vers[0] + "." + Integer.toString(u_ver);
	       	item.put("VER", ver);
       	    
 	        log.info("----------------revisionDoc-------------------------------------\n" + item.toString());
 	        createObject("mg.vdr.revisionDoc"+getDBName(dbName), item);
 	       	        
 			
 			item.put("LOG_CL_CD", "DU");  // 문서 업로드
 			item.put("USER_ID", user_id);
 			item.put("ROOM_ID", r_map.get("ROOM_ID"));
 			
 			//insertAccessLog(item);	 
 			insertVdrHistory(item);
 			
 			ObjectMapper mapper = new ObjectMapper();  		  
 			JSONParser parser = new JSONParser();
 			
 			List list = getList("mg.cp.selectCPMaster" + getDBName(pm.getString("component.sql.database")), item);
  		  
 			for(int i = 0; i < list.size(); i++) {
 				Map map = (Map)list.get(i);
 				JSONObject jsonObj = (JSONObject) parser.parse(map.get("FILE_LIST").toString());
 				JSONArray jsonArrayDocs = (JSONArray) parser.parse(jsonObj.get("docs").toString());
 				
 				if(0 < jsonArrayDocs.size()) {
 					JSONArray updateJsonArrayDocs = new JSONArray(); 					
 					for(int j = 0; j < jsonArrayDocs.size(); j++) {
 						JSONObject changeObj = (JSONObject) parser.parse(jsonArrayDocs.get(j).toString());
 						if(-1 < item.get("DOC_ID").toString().indexOf(changeObj.get("DOC_ID").toString())) {
 							
 							if (changeObj.get("VER") == null) {
 								changeObj.put("VER", ver);
 							}else {
 								changeObj.replace("VER", ver);
 							}						
 							
 						}
 						updateJsonArrayDocs.add(changeObj);
 					}
 					jsonObj.put("docs", updateJsonArrayDocs);
 					item.put("FILE_LIST", mapper.writeValueAsString(jsonObj));
 					item.put("DOC_NO", map.get("DOC_NO"));
 					updateObject("mg.cp.updateCPMaster" + getDBName(pm.getString("component.sql.database")), item);    				  
 				}
 			}
 			
 			 			
 			tx.commit();
	        
	        obj.put("data", item);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");	
 			
 	        return write(obj.toString());
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
    	 
     }
     

     /**
      * 문서 생성(등록)
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse createDoc(IRequest request) throws Exception 
     { 
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         
         HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
         if("OPTIONS".equals(httpRequest.getMethod())){
        	 // To avoid nested transaction
 			obj.put("errmsg", mm.getMessage("COMG_1002",loc));
 			obj.put("errcode", "0");
 	        return write(obj.toString());
         }
         try {
        	tx.begin(); 
        	String dbName = pm.getString("component.sql.database");
 	        Map item = request.getMap();
 	       /* String docId = getMakeId("D");
 	        String pgmId = (String)item.get("ROOM_ID");
 	        
 	        String filename = request.getStringParam("file_file");
 	        String orgFilename = request.getStringParam("file_orgFile");*/
 	        String docId = request.getStringParam("pgmId");
	        String pgmId = request.getStringParam("docId");
	        
	        String filename = docId+pgmId;
	        String orgFilename = "0";
 	        if(filename == null){
				 log.info("file name is null. \n");
 	        }
 	        
 	        
	 	    int pos = filename.lastIndexOf( "." );
	 	    String ext = filename.substring( pos + 1 );
	 	    
	 	   Enumeration params = request.getParameterNames();
		 	  System.out.println("----------------------------");
		 	  while (params.hasMoreElements()){
		 	      String name = (String)params.nextElement();
		 	      System.out.println(name + " : " +request.getStringParam(name));
		 	  }
		 	  System.out.println("----------------------------");
		 	 

		 	request.addRequestObject("viewerType", viewerType);
		 	request.addRequestObject("uploadEncrypt", uploadEncrypt);  
		 	
		 	
 	        fum.createUpload(pgmId, docId, request );
 	        log.info("createUpload end");
 	        
 	        
 	        //String orgFilename = request.getStringParam("files_orgFile");
 	        //// pos = orgFilename.lastIndexOf( "." );
	 	    //String ext = orgFilename.substring( pos + 1 );
 	        
 	        int doc_size = Integer.parseInt(request.getStringParam("file_size"));
 	        
 	        item.put("DOC_NM",  filename);
 	        int count = (int)getItem("mg.vdr.countDocNM", item);
 	        
 	        pos = orgFilename.lastIndexOf( "." ); 	        
 	        String frontFilename = orgFilename.substring(0, pos); 	       
 	        
 	        if (count != 0) { 	        	
 	        	for (int i=1 ; i < Integer.MAX_VALUE ; i++) {
 	        		filename = frontFilename + "_" + i + "." + ext;
 	        		item.put("DOC_NM",  filename);
 	        		count = (int)getItem("mg.vdr.countDocNM", item);
 	        		if (count == 0) {
 	        			break;
 	        		}
 	        	}
 	        } 	        
 	        
 	        //',#FOLDER_ID#, #ROOM_ID#, R_USER_ID
 	        // DOC_ID 와 UUID 는 최초 동일 하고 버전업 될떄 UUID는 바뀐다.
 	        item.put("DOC_ID",  docId);
 	        item.put("UUID",  docId);
 	        //item.put("DOC_NM",  filename);
 	        item.put("DOC_SIZE", doc_size );
 	        item.put("DOC_EXTENSION",  ext);
        	item.put("OWNER_ID",  user_id);
	       	item.put("CREATOR_ID", user_id);
	       	item.put("MODIFIER_ID", user_id);
	       	item.put("R_USER_ID", user_id);
	       	item.put("IS_DELETE", "N");
	       	item.put("VER", "1.0");
	       	item.put("USERID", user_id);
       	    
 	        log.info("----------------createDoc-------------------------------------\n" + item.toString() + ", item : "+item.hashCode());
 	        createObject("mg.vdr.createDoc"+getDBName(dbName), item);
 	       
 	        log.info("----------------createDocAuth-------------------------------------\n" + item.toString() + ", item : "+item.hashCode()); 	        
 	        createObject("mg.vdr.createDocAuth" + getDBName(dbName), item);
 	        
 	        if(null != item.get("FAVORITE_YN")) updateObject("mg.vdr.updateBookmark", item); 	        	
 	        
 	        if(null != item.get("UPDATE_LIST")) {
 	        	
 	        	item.put("USER_AUTH", "Y");
 	        	
 	        	updateObject("mg.vdr.updateDocUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
 	        	
 	        	updateObject("mg.vdr.updateDocUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
 	        	
 	        	JSONParser parser = new JSONParser();
 	        	
 	        	JSONArray update_list = (JSONArray) parser.parse((String) item.get("UPDATE_LIST"));
 	        	
 	        	for(int i = 0; i < update_list.size(); i++) {
 	        		
 	        		Map param = new ObjectMapper().readValue(((JSONObject) update_list.get(i)).toJSONString(), Map.class);
 	        		
 	        		param.put("DOC_ID", docId);
 	        		
 	        		updateObject("mg.vdr.updateDocUserAuth" + getDBName(pm.getString("component.sql.database")), param);
 	        	}
 	        }else {
 	        	
 	        	Map pFolderInfo = (Map) getItem("mg.vdr.selectFolderInfo" + getDBName(pm.getString("component.sql.database")), new HashMap() {{put("FOLDER_ID", item.get("FOLDER_ID"));}});
 	        	
 	        	if(null != pFolderInfo && "Y".equals(pFolderInfo.get("USER_AUTH").toString())){
 	        		
 	        		item.put("USER_AUTH", "Y");
 	        		
 	        		updateObject("mg.vdr.updateDocUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
 	        		
 	        		updateObject("mg.vdr.updateDocUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
 	        		
 	 	        	item.put("ACCESS", "Y");
 	 	        	item.put("VIEW", "Y");
 	 	        	item.put("EDIT", "Y"); 	
 	 	        	
 	 	        	updateObject("mg.vdr.updateDocUserAuth" + getDBName(pm.getString("component.sql.database")), item);
 	        	}
 	        }

	        obj.put("data", item);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 			
 			item.put("LOG_CL_CD", "DC");
 			item.put("USER_ID", user_id);
 			
 			insertAccessLog(item);	 
 			insertVdrHistory(item);
 			
 			tx.commit();
 			
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * 문서 변경
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateDoc(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         String dbName = pm.getString("component.sql.database");
         try {
        	tx.begin(); 
 	        Map item = request.getMap();
        	item.put("LOGIN_ID",  user_id);
       	    	
 	        log.info("----------------updateObject-------------------------------------\n" + item.toString());
 	        updateObject("mg.vdr.updateDoc"+getDBName(dbName), item);
 	        
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");

 			item.put("LOG_CL_CD", "DN");
 			item.put("USER_ID", user_id);
 			
 			insertAccessLog(item);
 			tx.commit();
 			
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }

    /**
     * 탐색기의 문서 정보 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getDocument(IRequest request) throws Exception 
     {
     	JSONObject obj = new JSONObject();
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");

      	Map smap = request.getMap();
      	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
 		smap.put("LOCALE", loc );
 		String dbName = pm.getString("component.sql.database");
 		smap.put("dbName", dbName);
 		
 		 log.info("------getDocument---IRequest--- > \n" + smap.toString());
         
         try {
         	
         	List lists;
         	if(dbName.equals("mssql")){
         		lists = getList("mg.vdr.getDocument",smap);
         	}else{
         		lists = getList("mg.vdr.getDocument_" + dbName, smap);
         	}
         	
         	 JSONArray jArray = new JSONArray();
      	    
	          for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	JSONObject listdata = new JSONObject();
	          	
	          	listdata.put("ROW_ID", String.valueOf(i+1));
	          	for(int j=0; j < cols.length; j++ ) {
	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	          	
	          	jArray.add(listdata);
	          }
 			 
          	 obj.put("data", jArray);
 			 obj.put("errmsg", "success");
 			 obj.put("errcode", "0");
 			 return write(obj.toString());
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
     } 
    /**
     * Update Bookmark (favorite)
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse updateBookmark(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         
    	 String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         
         try {
        	 
        	 Map item = request.getMap();
        	 
        	 item.put("R_USER_ID", user_id);
        	 
        	 if("FOLDER".equals(item.get("TYPE").toString())){
        		 
        		 item.put("FOLDER_IDS_LIST", item.get("FOLDER_IDS").toString().split(","));
        		 
        		 List lists = getList("mg.vdr.getFolderChildFullTarget" + getDBName(pm.getString("component.sql.database")), item);
        		 
          		 for (int i = 0; i < lists.size(); i++) {
          			 
          			 Map map = (Map) lists.get(i);
          			 
          			 map.put("R_USER_ID", item.get("R_USER_ID"));
          			 map.put("FAVORITE_YN", item.get("FAVORITE_YN"));
          			 
          			 updateObject("mg.vdr.updateBookmark", map);
          		 }
        		 
        	 }else{
        		 String[] DOC_IDS = item.get("DOC_IDS").toString().split(",");
        		 
        		 for(String DOC_ID : DOC_IDS) {
        			 item.put("DOC_ID", DOC_ID);
        			 item.put("FOLDER_ID", item.get("FOLDER_IDS"));
        			 updateObject("mg.vdr.updateBookmark", item);        			                                                                   
        		 }
        	 }
        	 
        	 obj.put("errmsg", "success");
        	 obj.put("errcode", "0");
        	 return write(obj.toString());
         }catch(Exception e) {
    	     obj.put("errmsg", mm.getMessage("COMG_1007"));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     } 
     
    /**
     * 폴더 권한 가져오기(Tree)
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getFolderTreePriv(IRequest request) throws Exception 
     {
    	JSONObject obj = new JSONObject();

     	Map smap = request.getMap();
     	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
		smap.put("LOCALE", loc );
		String dbName = pm.getString("component.sql.database");
		smap.put("dbName", dbName);
		
		 log.info("------getFolderTreePriv---IRequest--- > \n" + smap.toString());
        
        try {
        	
        	List lists;

        	if(dbName.equals("mssql")){
        		lists = getList("mg.vdr.getFolderTreePriv",smap);
        	}else{
        		lists = getList("mg.vdr.getFolderTreePriv_" + dbName, smap);
        	}
        	
        	log.info("------getFolderTreePriv---lists--- > \n" + lists.toString());
        	 
        	JSONArray jArray = new JSONArray();
        	TreeMaker tm = new TreeMaker();

		 	 List<FolderTreePrivVO> lst = makefolderPriv(lists);
			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
			 			 
	         obj.put("data", treeData);
			 obj.put("errmsg", mm.getMessage("COMG_1002", loc));
			 obj.put("errcode", "0");
			 
			 log.info("------getFolderTreePriv------ > \n" + obj.toString());
			 
			  ObjectMapper mapper = new ObjectMapper();
			  String str = "";
			  try {
					str = mapper.writeValueAsString(obj);
			  }
			  catch (IOException ex) {
					log.error("[getFolderTree] " + ex.getMessage());
			   }
				
			   log.info("------getFolderTreePriv-=----str------ > " + str);
			 
			 
			return write(str);
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
     } 
     /**
      * 폴더 권한 변경
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateFolderAuthMulti(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);         
         
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	 item = new HashMap();
 	       	 
 	       	 for(int j=0; j < cols.length; j++ ) {
 	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		             item.put(cols[j], tmps);     
 	         	}
				 item.put("LOGIN_ID", user_id);
				 item.put("ROOM_ID", request.getStringParam("ROOM_ID"));
				 item.put("R_USER_ID", request.getStringParam("R_USER_ID"));
				 
				 log.info("------------updateFolderAuthMulti-------item-----------\n" + item.toString());
				 log.info("업데이트");
				 updateObject("mg.vdr.updateFolderAuth", item);
 	        }
 	        tx.commit();

 			obj.put("errmsg", mm.getMessage("COMG_1002",loc));
 			obj.put("errcode", "0");
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     /**
      * 문서 권한 변경
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateDocAuthMulti(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	 item = new HashMap();
 	       	 
 	       	 for(int j=0; j < cols.length; j++ ) {
 	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		             item.put(cols[j], tmps);     
 	         	}
				 item.put("LOGIN_ID", user_id);
				 item.put("ROOM_ID", request.getStringParam("ROOM_ID"));
				 item.put("R_USER_ID", request.getStringParam("R_USER_ID"));
				 
				 log.info("------------updateDocAuthMulti-------item-----------\n" + item.toString());
				 log.info("업데이트");
				 updateObject("mg.vdr.updateDocAuth", item);
 	        }
 	        tx.commit();

 			obj.put("errmsg", mm.getMessage("COMG_1002",loc));
 			obj.put("errcode", "0");
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * CPD 생성을 위한 마스터 등록
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse makeCPD(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
/*         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
*/
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         String dbName = pm.getString("component.sql.database");
                  
         try {
         	tx.begin();
         	
         	Map inDD = request.getMap();
         	
         	/*   Room ID, Folder ID, Doc ID, Doc Title을 파라메터로 가져온다.
         	 *   DOC Master, DOC User , DOC Schedule 정보를 생성한다.
         	 */
         	String roomId = request.getStringParam("ROOM_ID");
         	String folders = request.getStringParam("folder");   // folder_id|folder_id
         	String docs = request.getStringParam("docs");  // doc_id,doc_id
         	// String title = request.getStringParam("title");  // doc_id,doc_id
         	String cpDocNo = getCpDocNo("");   // CP 문서번호의 채번
         	inDD.put("DOC_NO", cpDocNo);
         	inDD.put("LOGIN_ID", user_id);
         	
	   		 //1. CP_DOC_MASTER 반출 문서 등록
	   		 // ROOM_ID, EXE_FILE, DOC_MSG, DOC_TITLE, 
	   		 // inDD.put("DOC_TITLE","");
	   		 inDD.put("OUT_CL_CD", "01");     // 01:CPD 반출, 02 : 원문반출
	   		 inDD.put("IN_CL_CD", "C"); 
	   		 inDD.put("WATING_TIME", 10);
	   		 // inDD.put("CRE_TYPE", 'E');   // CreateType D: .cpd E: .exe 형태 
	   		 inDD.put("AUTHORITY", "4");   // 0=비밀번호, 1=메일인증(X), 2=PK인증(X) ,3:ID,PASS인증, 4:Web인증 ,5:무인증 ,6:Web인증+자체인증FILE_LIST : | 로 리스트 더함.
	   		 inDD.put("FILE_NUM",1);
	   		 inDD.put("EXE_RULE", "VDR Creation");
	   		 inDD.put("COMPANY_ID", "0001");
	   		 inDD.put("FILE_LIST", getFolderList(request));	 
	   		 // inDD.put("DOC_MSG", "");
   		     
	   		String scheNo="";
         	//master 등록
 			if(setDocMaster(inDD, dbName)) {
 				//수신자 등록
 				if(setDocUser(inDD, dbName)) {
 					// 스케쥴 등록 -- CP_JOB_SCHEDULE 상태코드등록
 		            scheNo = getCpDocNo("");   // CP 생성 스케쥴
 		    		inDD.put("SCHE_NO", scheNo);
 		       	 	inDD.put("CP_STS_CD", "02");  // 자가승인 또는 승인절차 없이 생성
 					createObject("mg.vdr.saveJobSchedule" +getDBName(dbName), inDD); 
	            }
 			}
 			
 			inDD.put("USER_ID", user_id);
 			inDD.put("ROOM_ID", roomId);
 			inDD.put("LOG_CL_CD", "ED");
 			inDD.put("DOC_TITLE", inDD.get("DOC_TITLE"));
 			inDD.put("FOLDER_ID", cpDocNo);
 			inDD.put("DOC_ID", cpDocNo);
 			
 			insertAccessLog(inDD);
         	
 	        tx.commit();

 			obj.put("errmsg", mm.getMessage("COMG_1002",loc));
 			obj.put("DOC_NO", cpDocNo);
 			obj.put("SCHE_NO", scheNo);
 			obj.put("errcode", "0");
 	        return write(obj.toString());      
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("----------makeCPD-----------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
    	     obj.put("DOC_NO", "");
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }     
     
     private boolean setDocMaster(Map inDD, String dbName)
     {
    	 try {
    		
    		 /*
    			#DOC_NO#,
    			#DOC_TITLE#,
    			#WAITING_TIME#,
    			#CREATOR_EMAIL#,
    			#FILE_NUM#,
    			#FILE_LIST#,
    			#USER_ID#,
    			#P_DOC_NO#,    --> null
    			#EXE_FILE#,   --> 실행파일명
    			#IN_PROCESS#,
    			#EXE_RULE#,
    			#AUTHORITY#,
    			#OUT_CL_CD#,
    			#APPROVER_ID#,
    			#DOC_MSG#,
    			#LOGIN_ID#, 
    			#IN_CL_CD#,
    			#CRE_TYPE#
    		*/
    		 
    		createObject("mg.vdr.insertDocMaster" +getDBName(dbName), inDD); 
    		log.info("----------setDocMaster--------- \n Master 등록 완료");

    		 return true;
    	 } catch(Exception e) {
    		 log.error("--------setDocMaster-------------------------------------------------------\n" + e.toString());
    		 return false;
    	 }
     }
     
     private boolean setDocUser(Map inDD, String dbName)
     {
    	 try {
    		 createObject("mg.vdr.insertDocUser" +getDBName(dbName), inDD); 
     		 log.info("----------setDocUser--------- \n Doc User 등록 완료");

    		 return true;
    	 } catch(Exception e) {
    		 log.error("--------setDocUser-------------------------------------------------------\n" + e.toString());
    		 return false;
    	 }
     }
     
     private String getFolderList(IRequest request)
     {
    	 try {
    		 /*   Room ID, Folder ID, Doc ID, Doc Title을 파라메터로 가져온다.
          	 *   DOC Master, DOC User , DOC Schedule 정보를 생성한다.
          	 */
          	String roomId = request.getStringParam("ROOM_ID");
          	String folder = request.getStringParam("folders");   // folder_id|folder_id
          	String doc = request.getStringParam("docs");  // doc_id,doc_id
          	String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
          	String auth_type = request.getStringParam("auth_type");
          	        	
          	JSONObject FileList = new JSONObject();
          	JSONArray jArray = new JSONArray(); 
          	          	
          	Map smap = new HashMap();
  			smap.put("ROOM_ID", roomId);
  			smap.put("user_id", user_id);
  			smap.put("R_USER_ID", user_id);
  			
  			log.info("--------getFolderList-----------------------folder-------------------------------\n" +folder);
  			
          	int f_cnt;
          	if(folder.length() > 2) {
          		String[] folders = folder.split(",");
          		for(int i=0; i < folders.length; i++) {
          			smap.put("FOLDER_ID", folders[i]);
          			
          			log.info("--------getFolderList-------------------------------------------------------\n" +smap.toString());
          			
          			JSONObject jsonFolder = new JSONObject();
          			jsonFolder.put("FOLDER_ID", folders[i]);
          			String folderName = getFolderName(smap);
      				jsonFolder.put("FOLDER_PATH", folderName);
      				
      				smap.put("auth_type", auth_type);
      				jsonFolder.put("files", getFileList(smap));
      				
      				jArray.add(jsonFolder);

          			List lists = getList("mg.vdr.getFolderSubTree"+getDBName(pm.getString("component.sql.database")),smap);       			
          			for(int j=0; j < lists.size(); j++) {
          				Map mapFolder = (Map)lists.get(j);
          				jsonFolder = new JSONObject();
          				jsonFolder.put("FOLDER_ID", this.getStringWithNullCheck(mapFolder, "FOLDER_ID"));
          				jsonFolder.put("FOLDER_PATH", folderName + "\\" + this.getStringWithNullCheck(mapFolder, "FOLDER_PATH"));
          				
          				mapFolder.put("auth_type", auth_type);
          				jsonFolder.put("files", getFileList(mapFolder));
          				
          				jArray.add(jsonFolder);
          				
          				log.info("--------getFolderList-------------------------------------------------------\n" +jsonFolder.toString());
          			}
          		}
          	}
          	FileList.put("folders", jArray );
          	FileList.put("docs", getFolderDocList(doc, user_id));

    		return FileList.toString();
    	 }catch(Exception e) {
    		 log.error("--------getFolderList-------------------------------------------------------\n" + e.toString());
    		 return "";
    	 }
     }
     
     private  JSONArray getFolderDocList(String doc, String user_id)
     {
    	 JSONArray jArray = new JSONArray();
    	 
    	 try {
    		 
    		 if(0 < doc.trim().length()) {
    			 
        		 String[] docs = doc.split(",");
        		 
        		 Map smap = new HashMap();
        		 
           		 for(int i=0; i < docs.length; i++) {
           			smap.put("DOC_ID", docs[i]);
           			smap.put("R_USER_ID",  user_id);
           			
        		 	Map retDoc = (Map)getItem("mg.vdr.getDocInfo",smap);
    				JSONObject jsonFile = new JSONObject();
    				jsonFile.put("DOC_ID", docs[i]);
    				jsonFile.put("DOC_NM", this.getStringWithNullCheck(retDoc, "DOC_NM"));
    				jsonFile.put("VER", this.getStringWithNullCheck(retDoc, "VER"));
    				jArray.add(jsonFile);
    			 }    			 
    		 }

    		 return jArray;
    	 } catch(Exception e) {
    		 log.error("--------getFolderDocList-------------------------------------------------------\n" + e.toString());
    		 return jArray;
    	 } 
     }
     
     private JSONArray getFileList(Map mapFolder)
     {
    	 JSONArray jArray = new JSONArray(); 
    	 try {
    		 List files = getList("mg.vdr.getFolderDocList",mapFolder );
			 for(int k=0; k < files.size(); k++) {
				 Map mapFile = (Map)files.get(k);
				 JSONObject jsonFile = new JSONObject();
				 jsonFile.put("DOC_ID", this.getStringWithNullCheck(mapFile, "DOC_ID"));
				 jsonFile.put("DOC_NM", this.getStringWithNullCheck(mapFile, "DOC_NM"));
				 jsonFile.put("VER", this.getStringWithNullCheck(mapFile, "VER"));
				 jArray.add(jsonFile);
			 }
    		 return jArray;
    	 } catch(Exception e) {
    		 log.error("--------getFileList-------------------------------------------------------\n" + e.toString());
    		 return jArray;
    	 } 
     }
     
     private String getFolderName(Map mapFolder)
     {
    	 try {
    		  Map rmap = (Map)getItem("mg.vdr.getFolderInfo", mapFolder);
    		  return this.getStringWithNullCheck(rmap, "FOLDER_NM");
    	 } catch(Exception e) {
    		 log.error("--------getFolderName-------------------------------------------------------\n" + e.toString());
    		 return "";
    	 } 
     }
     
     
     private IResponse getFolderSubTree(IRequest request) throws Exception 
     {
     	JSONObject obj = new JSONObject();

      	Map smap = request.getMap();
      	Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
 		smap.put("LOCALE", loc );
 		String dbName = pm.getString("component.sql.database");
 		smap.put("dbName", dbName);
         
         try {
        	 String ret = getFolderList(request);
        	 log.info("------getFolderSubTree-=----ret------ > " + ret);
        	 
 			return write(ret);
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
     } 
     
     
     private IResponse downloadFIle(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();

       	Map smap = request.getMap();
       	log.info("- ixVDR Download Map  :  " + smap.toString());
  		
    	 try {
    		 Map map;
    		 
    		 String room_id =  smap.get("ROOM_ID").toString();
    		 if("".equals(room_id)) {
    		   map = (Map)getItem("mg.vdr.getDocInfoWithOutRoomID", smap);	           
  	           map.put("UUID", map.get("UUID"));	           
  	           map.put("ROOM_ID", map.get("ROOM_ID"));
    		 } else {
    		    map = (Map)getItem("mg.vdr.getDownloadDocInfo", smap);	           
	            map.put("UUID", map.get("UUID"));	           
	            map.put("ROOM_ID", room_id);
    	     }
	           
	         Map fileMap = (Map)getItem("mg.vdr.uploadFilePath", map);	           
	         String fileName = this.getStringWithNullCheck(fileMap, "FILE_NAME");	           
	         String downFile = pm.getString("component.upload.directory") + "/" + this.getStringWithNullCheck(fileMap, "FILE_PATH") + "/" + fileName;
	         
	         if (uploadEncrypt.equals("Y")) {
	        	 
	        	 log.info("uploadEncrypt.equals(\"Y\") ");
	        	 
	        	 log.info("downFile : "+downFile);
	        	 
	        	 File file = new File(downFile+"_enc");
	        	 if(!file.exists()) {
					cryptorSHAEncodeThread th = new cryptorSHAEncodeThread(downFile, downFile+"_enc",  "12345");
					th.start();
					try{
						th.join();
					}catch(Exception e){ 
						log.info("testDecrypt Error -----"+e.toString());
					}
					long retval = th.getResultLong();
					log.info("retval ----- : "+retval);
	        	 }
	        	 
	        	 downFile = downFile+"_enc";
	         }
	         
	         return downloadFile(downFile); 
    	 } catch(Exception e) {
 	    	 log.error("---File Not Found----------------------------------------------------------\n" + e.toString());
 			return downloadFile(null);
 	     }
     }     
     
     /**
      * 사이냅 뷰어 웹 뷰어 
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse viewerDoc(IRequest request) throws Exception 
     {    	 
    	 
    	 String secretKey = pm.getString("component.jwt.key");
    	 JSONObject obj = new JSONObject();
         String user_id = request.getUser() != null ? request.getUser().getId() : "admin";
         String user_nm = request.getUser() != null ? request.getUser().getName() : "";
         
         
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
                 
         
         long expiredTime = 1000 * 60l;
         
         Date expireTime = new Date();
         expireTime.setTime(expireTime.getTime() + expiredTime); 
         byte[] keySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);         
         SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;         
         Key key = new SecretKeySpec(keySecretBytes, signatureAlgorithm.getJcaName());
         
         Map<String, Object> headerMap = new HashMap<String, Object>();

         headerMap.put("typ","JWT");
         headerMap.put("alg","HS256");
         
         Map<String, Object> map= new HashMap<String, Object>();

         map.put("polarisUserId", user_id);

         String jwt = Jwts.builder().setHeader(headerMap)
                 .setClaims(map)
                 .setExpiration(expireTime)
                 .signWith(signatureAlgorithm, keySecretBytes)
                 .compact();
         
         PolarisCryptoUtil polarisCryptoUtil = new PolarisCryptoUtil();
         
         tx.begin(); 
         
         try {
 	        Map requestMap = request.getMap();
 	        String docId = String.valueOf(requestMap.get("DOC_ID"));
 	        requestMap.put("UUID",docId); 	 	        
 	        Map fileMap = (Map)getItem("mg.vdr.uploadFilePath", requestMap); 	 	        
 	        String fileName = this.getStringWithNullCheck(fileMap, "FILE_NAME");
 	         	        
 	        String path = "";
 	        
 	        if (requestMap.get("TYPE").equals("P")) {
 	        	String serverURL = pm.getString("component.url.server");
 	        	String downloadDefault = serverURL + pm.getString("component.path.download");
 	        	String downloadURL = downloadDefault + "&ROOM_ID="+requestMap.get("ROOM_ID") + "&DOC_ID="+docId; 	        
 	        	
 	        	downloadURL = polarisCryptoUtil.URLEncodingBase64(downloadURL.getBytes("UTF-8"), "UTF-8");
 	        	fileName = polarisCryptoUtil.URLEncodingBase64(fileName.getBytes("UTF-8"), "UTF-8");
 	        	 	        	
 	        	String tempToken = jwt;
 	        	String tempData = "111test987654321"; 	        	
 	        	
 	        	String polarisconverterPath = pm.getString("component.path.polarisconverter"); 	        	
 	        	String sk = polarisCryptoUtil.URLEncodingBase64(polarisCryptoUtil.getRSAData(tempData, "UTF-8", polarisconverterPath), "UTF-8");
 	        	String srt = polarisCryptoUtil.URLEncodingBase64(polarisCryptoUtil.getAESData(tempToken, "UTF-8"), "UTF-8"); 	        	
 	        	
 	        	String perms = polarisCryptoUtil.URLEncodingBase64(polarisCryptoUtil.getAESData("OPEN", "UTF-8"), "UTF-8");	        	
 	        	String wmt = polarisCryptoUtil.URLEncodingBase64(polarisCryptoUtil.getAESData("DIAGONAL", "UTF-8"), "UTF-8"); 	        	
 	        	
 	        	long time = System.currentTimeMillis(); 
 	        	SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
 	        	String str = dayTime.format(new Date(time));
 	        	
 	        	String wmtx = loc + " " + user_nm + " " + str;
 	        	wmtx = polarisCryptoUtil.URLEncodingBase64(polarisCryptoUtil.getAESData(wmtx, "UTF-8"), "UTF-8");
 	        	
 	        	path = "polaris-viewer://Open?url="+downloadURL+"&filename="+fileName+"&sk="+sk+"&srt="+srt+"&wmt="+wmt+"&wmtx="+wmtx+"&perms="+perms+"&type=aXh2ZHI%3d";
 	        	
 	        	obj.put("viewerType", "P"); 
 	        	
 	        }else {
 		        String synapURL = pm.getString("component.url.synap");  
 	 	        String v_key = this.getStringWithNullCheck(fileMap, "V_KEY");
 		           
 	            if (v_key.equals("")) {   	
 	        
 	              	
 	              	String dbName = pm.getString("component.sql.database"); 	            	
 	            	String uploadFilePath = this.getStringWithNullCheck(fileMap, "FILE_PATH")+"/"+fileName;
 	            	SynapViewer httpService = SynapViewer.getInstance();
 	            	
 	            	JSONObject jsonData = httpService.synabJobAPI(uploadFilePath, docId);
 	            	log.info("viewerDoc jsonData11 : "+jsonData);

 	            	Map param = new HashMap();
 	            	param.put("UUID", docId);
 	            	param.put("ROOM_ID", requestMap.get("ROOM_ID"));
 	            	            	
 	            	if (jsonData != null && jsonData.get("key") != null) {            		
 	            		v_key = String.valueOf(jsonData.get("key"));
 	            		param.put("v_key", v_key);
 	            		createObject("mg.vdr.updateFileKey"+getDBName(dbName), param);
 	            	} 
 	            	
 	            }
 	            
 	            path = synapURL + "/SynapDocViewServer/viewer/doc.html?key="+v_key+ "&convType=img&convLocale=ko_KR&contextPath=/SynapDocViewServer"; 	          
 	            obj.put("viewerType", "S"); 
 	        }
 	               
            obj.put("path", path);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 			
 			Map item = new HashMap();
 			item.put("DOC_ID",docId);
 			
 			Map docMap = (Map)getItem("mg.vdr.getDownloadDocInfo", item);
 			item = request.getMap();		
 			item.put("LOG_CL_CD", "DR");
 			item.put("USER_ID", user_id);    
 			item.put("DOC_ID", docId); 
 			item.put("FOLDER_ID", docMap.get("FOLDER_ID")); 
 			item.put("DOC_NM", docMap.get("DOC_NM")); 
   		  
 			insertAccessLog(item);
 			tx.commit();
 			
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1001", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }
     
     /**
      * 썸네일 
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse thumbnailDoc(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);

         try {
 	        Map requestMap = request.getMap();
 	        
 	        String docId = String.valueOf(requestMap.get("DOC_ID"));
 	        requestMap.put("UUID",docId);
 	        
 	        Map fileMap = (Map)getItem("mg.vdr.uploadFilePath", requestMap);
 	        if (requestMap.get("TYPE").equals("P")) { 
 	        	
 	        	String fileName = this.getStringWithNullCheck(fileMap, "FILE_NAME");
 	        	
 	        	if (fileName != null) {
 	        		int pos = fileName.lastIndexOf( "." );
 	 		 	    String ext = fileName.substring( pos + 1 );
 	 		 	    
 	 		 	    String thumbUrl = pm.getString("component.url.server") + pm.getString("component.path.thumbnail");	 
	 	        	
	 	        	obj.put("viewerType", "P");
	 	        	obj.put("docId", docId);
 	 	        	
	 	        	if (ext.equals("xlsx") || ext.equals("xls")) {
	 	        		obj.put("path", thumbUrl + "/" + this.getStringWithNullCheck(fileMap, "FILE_PATH") + "/" + docId + "/1_1");
 	 		 	    }else {
 	 		 	    	obj.put("path", thumbUrl + "/" + this.getStringWithNullCheck(fileMap, "FILE_PATH") + "/" + docId + "/1");
 	 		 	    }
	 	        	
 	        	}
 	            
 	        } else if (requestMap.get("TYPE").equals("S")) {
 	        	String v_key = this.getStringWithNullCheck(fileMap, "V_KEY");
 	           
 	            if (v_key.equals("")) {   	
 
 	              	String dbName = pm.getString("component.sql.database");
 	            	
 	            	String uploadFilePath = this.getStringWithNullCheck(fileMap, "FILE_PATH")+"/"+this.getStringWithNullCheck(fileMap, "FILE_NAME");
 	            	SynapViewer synapViewer = SynapViewer.getInstance();
 	            	
 	            	JSONObject jsonData = synapViewer.synabJobAPI(uploadFilePath, docId); 	            	            	
 	            	if (jsonData != null && jsonData.get("key") != null) {            		
 	            		v_key = String.valueOf(jsonData.get("key"));
 	            		
 	            		Map param = new HashMap();
 	 	            	param.put("UUID", docId);
 	 	            	param.put("ROOM_ID", requestMap.get("ROOM_ID"));
 	            		param.put("v_key", v_key);
 	            		createObject("mg.vdr.updateFileKey"+getDBName(dbName), param);
 	            	} 
 	            }
 	            
 	            SynapViewer httpService = SynapViewer.getInstance();
 	            JSONObject jsonData = httpService.synabStatusAPI(v_key);

 	            obj.put("viewerType", "S");
 	            obj.put("docId", docId);            
 	            obj.put("key", v_key);
 	            obj.put("totalPage", jsonData.get("pageNum"));
 	        }
            
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 			
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1001", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }    
     
     /**
      * 대상 잘라내기
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse pasteMove(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         
     	 String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	 
    	 String dbName = pm.getString("component.sql.database");
    	 
         try {
        	 
        	 tx.begin();
        	 
        	 Map item = request.getMap();
        	 
        	 item.put("R_USER_ID", user_id);
        	 item.put("ids", item.get("P_TARGET_ID").toString());
        	 
        	 Map folderInfo = (Map)getItem("mg.vdr.getFolderInfo" + getDBName(dbName), item);
        	        	 
        	 String[] TARGET_IDS = item.get("TARGET_IDS").toString().split(",");
        	 
        	 for(String TARGET_ID : TARGET_IDS){
        		 
        		 item.put("P_USER_AUTH", folderInfo.get("USER_AUTH").toString());
        		 
        		 if(TARGET_ID.equals(item.get("P_TARGET_ID")))	throw new Exception();
        		 
        		 if(TARGET_ID.startsWith("F")) {
        			 
        			 item.put("FOLDER_ID", TARGET_ID);
        			 updateObject("mg.vdr.moveFolder" + getDBName(dbName), item);
        			 
        			 if("Y".equals(item.get("P_USER_AUTH").toString())) {
        				 updateObject("mg.vdr.folderUserAuth" + getDBName(dbName), item);
        				 updateObject("mg.vdr.moveFolderAuth" + getDBName(dbName), item);
        				 updateObject("mg.vdr.docUserAuth" + getDBName(dbName), item);
        				 updateObject("mg.vdr.moveDocAuth" + getDBName(dbName), item);
        			 }
        			 
        		 }else{

        			 item.put("DOC_ID", TARGET_ID);
        			 updateObject("mg.vdr.moveDoc" + getDBName(dbName), item);
        			 
        			 if("Y".equals(item.get("P_USER_AUTH").toString())) updateObject("mg.vdr.moveDocOnly" + getDBName(dbName), item);
        		 }
        	 }
        	 
        	 tx.commit();
        	 
        	 obj.put("errmsg", "success");
        	 obj.put("errcode", "0");
        	 return write(obj.toString());
         }catch(Exception e){
        	 tx.rollback();
    	     obj.put("errmsg", mm.getMessage("COMG_1007"));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }    
     
     /**
      * 대상 붙혀넣기
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse pasteTarget(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         
    	 String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	 
    	 String dbName = pm.getString("component.sql.database");
         
         try {
        	 
        	 tx.begin();
        	 
        	 Map item = request.getMap();
        	 
        	 item.put("R_USER_ID", user_id);
        	 item.put("ids", item.get("P_TARGET_ID").toString());
        	 
        	 Map parentTargetInfo = (Map)getItem("mg.vdr.getFolderInfo" + getDBName(dbName), item);
        	 
        	 String[] TARGET_IDS = item.get("TARGET_IDS").toString().split(",");
        	 
        	 List<String> FOLDER_IDS_LIST = new ArrayList<String>();
        	 List<String> DOC_IDS_LIST = new ArrayList<String>();
        	 
        	 for(String TARGET_ID : TARGET_IDS){
        		 if(TARGET_ID.startsWith("F")) FOLDER_IDS_LIST.add(TARGET_ID);
        		 else						   DOC_IDS_LIST.add(TARGET_ID);
        	 }
        	 
        	 if(0 < FOLDER_IDS_LIST.size()){
        		 
        		 item.put("FOLDER_IDS_LIST", FOLDER_IDS_LIST);
        		 item.put("R_USER_ID", user_id);
        		 
        		 List<Map> fullTargetList = getList("mg.vdr.getFolderChildFullTarget" + getDBName(dbName), item);
        		 
        		 Map changeIdMap = new HashMap<String, Object>();
        		 
        		 for(Map targetInfoMap : fullTargetList) {
        			 changeIdMap.put(targetInfoMap.get("TARGET_ID").toString(), ("FOLDER".equals(targetInfoMap.get("TYPE").toString()))? getMakeId("F") : getMakeId("D"));
        		 }
        		 
        		 for(Map targetInfoMap : fullTargetList) {
        			 
        			 if(!"FOLDER".equals(targetInfoMap.get("TYPE").toString())) continue;
        			 
        			 targetInfoMap.put("USER_ID", user_id);
        			 targetInfoMap.put("TARGET_ID", changeIdMap.get(targetInfoMap.get("TARGET_ID").toString()).toString());
        			 targetInfoMap.put("P_TARGET_ID", ("1".equals(targetInfoMap.get("DEPTH").toString()))? item.get("P_TARGET_ID").toString() : changeIdMap.get(targetInfoMap.get("P_TARGET_ID").toString()).toString());        			 
        			 targetInfoMap.put("P_USER_AUTH", (null == parentTargetInfo)? "N" : parentTargetInfo.get("USER_AUTH").toString());
        			 
    				 createObject("mg.vdr.copyFolder" + getDBName(dbName), targetInfoMap);
    				 createObject("mg.vdr.copyFolderAuth" + getDBName(dbName), targetInfoMap);
    				 createObject("mg.vdr.copyFolderFavorites" + getDBName(dbName), targetInfoMap);
        		 }
        		 
        		 for(Map targetInfoMap : fullTargetList) {
        			 
        			 if("FOLDER".equals(targetInfoMap.get("TYPE").toString())) continue;
        			 
        			 targetInfoMap.put("USER_ID", user_id);
        			 targetInfoMap.put("TARGET_ID", changeIdMap.get(targetInfoMap.get("TARGET_ID").toString()).toString());
        			 targetInfoMap.put("P_TARGET_ID", ("1".equals(targetInfoMap.get("DEPTH").toString()))? item.get("P_TARGET_ID").toString() : changeIdMap.get(targetInfoMap.get("P_TARGET_ID").toString()).toString());
        			 targetInfoMap.put("P_USER_AUTH", (null == parentTargetInfo)? "N" : parentTargetInfo.get("USER_AUTH").toString());
        				 
    				 String ROOM_ID = item.get("ROOM_ID").toString();
    				 String DOC_ID = targetInfoMap.get("DOC_ID").toString();
    				 String TARGET_ID = targetInfoMap.get("TARGET_ID").toString();
    				 String P_TARGET_ID = targetInfoMap.get("P_TARGET_ID").toString();

    				 targetInfoMap.put("DOC_NM", fum.copyUpload(ROOM_ID, DOC_ID, TARGET_ID, P_TARGET_ID, request).getName());
    				 
    				 createObject("mg.vdr.copyDoc" + getDBName(dbName), targetInfoMap);
    				 createObject("mg.vdr.copyDocAuth" + getDBName(dbName), targetInfoMap);
    				 createObject("mg.vdr.copyDocFavorites" + getDBName(dbName), targetInfoMap);
        		 }
        	 }
        	 
        	 if(0 < DOC_IDS_LIST.size()){
        		 
        		 Map targetInfoMap = new HashMap<String, Object>();
        		 
        		 targetInfoMap.put("USER_ID", user_id);
        		 targetInfoMap.put("P_TARGET_ID", item.get("P_TARGET_ID").toString());
        		 
        		 for(String DOC_ID : DOC_IDS_LIST) {

        			 targetInfoMap.put("TARGET_ID", getMakeId("D"));
        			 targetInfoMap.put("DOC_ID", DOC_ID);
        			 targetInfoMap.put("P_USER_AUTH", (null == parentTargetInfo)? "N" : parentTargetInfo.get("USER_AUTH").toString());
        			 
    				 String ROOM_ID = item.get("ROOM_ID").toString();
    				 String TARGET_ID = targetInfoMap.get("TARGET_ID").toString();
    				 String P_TARGET_ID = targetInfoMap.get("P_TARGET_ID").toString();

    				 targetInfoMap.put("DOC_NM", fum.copyUpload(ROOM_ID, DOC_ID, TARGET_ID, P_TARGET_ID, request).getName());        			 
        			 
    				 createObject("mg.vdr.copyDoc" + getDBName(dbName), targetInfoMap);
    				 createObject("mg.vdr.copyDocAuth" + getDBName(dbName), targetInfoMap);
    				 createObject("mg.vdr.copyDocFavorites" + getDBName(dbName), targetInfoMap);
        		 }
        	 }
        	 
        	 tx.commit();
        	 
        	 obj.put("errmsg", "success");
        	 obj.put("errcode", "0");
        	 return write(obj.toString());
         }catch(Exception e){
        	 tx.rollback();
    	     obj.put("errmsg", mm.getMessage("COMG_1007"));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
     }     
     
     /**
      * 문서 복사
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse pasteDoc(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         
         try {
        	tx.begin(); 
        	String dbName = pm.getString("component.sql.database");
 	        Map item = request.getMap();
 	        
 	        item.put("R_USER_ID", user_id);
 	        
 	        String roomId = (String)item.get("ROOM_ID"); 
 	        String docId = (String)item.get("DOC_ID"); 
 	        
 	        
			Enumeration params = request.getParameterNames();
			System.out.println("----------------------------");
			while (params.hasMoreElements()){
				String name = (String)params.nextElement();
				System.out.println(name + " : " +request.getStringParam(name));
			}
			System.out.println("----------------------------");
	 	      
	 	    String newDocId = getMakeId("D");
	 	    String targetfolderId = (String)item.get("FOLDER_ID");
	 	    
	 	    request.addRequestObject("viewerType", viewerType);
	 	  
		 	File newFile = fum.copyUpload(roomId, docId, newDocId, targetfolderId, request);
 	        log.info("copyUpload end");
 	       
 	        
 	        Map docInfo = (Map)getItem("mg.vdr.getDocInfo", item);
 	        
 	        item.put("DOC_ID",  newDocId);
 	        item.put("UUID",  newDocId);
 	        item.put("DOC_NM",  newFile.getName());
 	        item.put("DOC_SIZE", docInfo.get("DOC_SIZE") );
 	        item.put("DOC_EXTENSION",  docInfo.get("DOC_EXTENSION"));
        	item.put("OWNER_ID",  user_id);
	       	item.put("CREATOR_ID", user_id);
	       	item.put("MODIFIER_ID", user_id);
	       	item.put("IS_DELETE", "N");
	       	item.put("VER", "1.0");
       	    
 	        log.info("----------------createDoc-------------------------------------\n" + item.toString());
 	        createObject("mg.vdr.createDoc"+getDBName(dbName), item);
 	        
 	        // 폴더권한등록
 	        // #ROOM_ID#,#DOC_ID#,#R_USER_ID#,#ACES#,#RED#,#WRT#,#PRT#,#DLT#,#DOWN#,GETDATE(),#CHG_ID#)
 	        // R_USER_ID : 넘겨준 참여자 ID
 	        // R_USER_ID 가 admin이면 또는 생정자 이면 모든권한 부여함.   --> 추후 로직 재검토 함.
 	        //String r_user_id = request.getStringParam("R_USER_ID"); 
 	        
 	        item.put("CHG_ID", user_id);
 	       
 	        log.info("----------------createDocAuth-------------------------------------\n" + item.toString());
 	        createObject("mg.vdr.createDocAuth"+getDBName(dbName), item);

	        tx.commit();
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
         
     }
     
     
     /**
      * 문서 복사
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse moveDoc(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
         
         try {
        	tx.begin(); 
        	String dbName = pm.getString("component.sql.database");
 	        Map item = request.getMap();
 	        
 	        item.put("R_USER_ID", user_id);
 	        
 	        String roomId = (String)item.get("ROOM_ID"); 
 	        String docId = (String)item.get("DOC_ID"); 
	 	    
	 	    //log.info("createUpload start request : "+request.);
	 	    
			Enumeration params = request.getParameterNames();
			System.out.println("----------------------------");
			while (params.hasMoreElements()){
				String name = (String)params.nextElement();
				System.out.println(name + " : " +request.getStringParam(name));
			}
			System.out.println("----------------------------");
	 	      
	 	    String newDocId = getMakeId("D");
	 	    String targetfolderId = (String)item.get("FOLDER_ID");
	 	  
		 	File newFile = fum.moveUpload(roomId, docId, newDocId, targetfolderId, request);
 	        log.info("copyUpload end");
 	    
 	        
 	       log.info("----------------copyUpload-------------------------------------\n" + item.toString());
 	        
 	        Map docInfo = (Map)getItem("mg.vdr.getDocInfo", item);
 	        Map newItem = request.getMap();
 	        newItem.put("ROOM_ID",  roomId);
 	        newItem.put("DOC_ID",  newDocId);
 	        newItem.put("UUID",  newDocId);
 	        
 	       log.info("----------------copyUpload -------------newItem----------------------\n" + newItem.toString());
 	        newItem.put("DOC_NM",  newFile.getName());
 	        newItem.put("DOC_SIZE", docInfo.get("DOC_SIZE") );
 	        newItem.put("DOC_EXTENSION",  docInfo.get("DOC_EXTENSION"));
 	        
 	        
 	       log.info("----------------copyUpload -------------newItem----------------------\n" + newItem.toString());
 	        newItem.put("OWNER_ID",  user_id);
 	        newItem.put("CREATOR_ID", user_id);
 	        newItem.put("MODIFIER_ID", user_id);
 	        newItem.put("IS_DELETE", "N");
 	        newItem.put("VER", "1.0");
       	    
 	        log.info("----------------createDoc-------------------------------------\n" + newItem.toString());
 	        createObject("mg.vdr.createDoc"+getDBName(dbName), newItem);
 	        
 	        // 폴더권한등록
 	        // #ROOM_ID#,#DOC_ID#,#R_USER_ID#,#ACES#,#RED#,#WRT#,#PRT#,#DLT#,#DOWN#,GETDATE(),#CHG_ID#)
 	        // R_USER_ID : 넘겨준 참여자 ID
 	        // R_USER_ID 가 admin 이면 또는 생정자 이면 모든권한 부여함.   --> 추후 로직 재검토 함. 	     
 	        item.put("CHG_ID", user_id);
 	       
 	        log.info("----------------createDocAuth-------------------------------------\n" + item.toString());
 	        createObject("mg.vdr.createDocAuth"+getDBName(dbName), newItem);
 	        

	        log.info("----------------deleteDocAuth-------------------------------------\n" + item.toString());
	        deleteObject("mg.vdr.deleteDocAuth", item);
	        
	        log.info("----------------deleteDoc-------------------------------------\n" + item.toString());
	        deleteObject("mg.vdr.deleteDoc", item);

	        tx.commit();
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 	        return write(obj.toString());
 	        
         } catch(Exception e) {
        	 tx.rollback();
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	    
    	     obj.put("errmsg", mm.getMessage("COMG_1007", loc));
 			 obj.put("errcode", "-1");   
             return write(obj.toString());
         }
         
     }
     
     /**
      * 나의 즐겨찾기 문서목록 조회 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse getMyBookmarkList(IRequest request) throws Exception 
      {
    	  
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	  
    	  JSONObject obj = new JSONObject();
    	  
    	  String col = request.getStringParam("grid_col_id");
    	  
    	  String[] cols = col.split(",");
    	  
    	  String docOnly = request.getStringParam("doconly");
    	  
    	  Map smap = request.getMap();

    	  String dbName = pm.getString("component.sql.database");
    	  
    	  try {
    		  
    		  smap.put("R_USER_ID", user_id);
    		  
    		  List lists = null;
    		  
    		  if(null != smap.get("SEARCH_FILE_NAME") && 0 < smap.get("SEARCH_FILE_NAME").toString().trim().length()) {
    			  lists = getList("mg.vdr.getMyBookmarkSearchList" + getDBName(dbName), smap);
    		  }else{
    			  lists = getList("mg.vdr.getMyBookmarkList" + getDBName(dbName), smap);
    		  }
    		  
    		  JSONArray jArray = new JSONArray();
    		  
    		  for(int i = 0; i < lists.size(); i++) {
    			  
    			  Map map = (Map)lists.get(i);
    			  
    			  JSONObject listdata = new JSONObject();
    			  
    			  listdata.put("ROW_ID", String.valueOf(i + 1));
    			  
    			  for(int j = 0; j < cols.length; j++) {
    				  listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
    			  }
    			  
    			  jArray.add(listdata);
    		  }
    		  
    		  obj.put("data", jArray);
  			  obj.put("errmsg", "success");
  			  obj.put("errcode", "0");
  			  
  			  return write(obj.toString());
  		 }catch(Exception e) {
  			 obj.put("errmsg", mm.getMessage("COMG_1001"));
  			 obj.put("errcode", "-1");
  	    	 return write(obj.toString());
  	     }
      } 
      
      /**
       * 휴지통 문서목록 조회 
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse getTrashList(IRequest request) throws Exception 
      {
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";

    	  JSONObject obj = new JSONObject();
    	  
    	  String col = request.getStringParam("grid_col_id");
    	  
    	  String[] cols = col.split(",");

    	  Map smap = request.getMap();
    	  
    	  String dbName = pm.getString("component.sql.database");

    	  try {
    		  
    		  smap.put("R_USER_ID", user_id);

    		  List lists = null;
    		  
    		  if(null != smap.get("SEARCH_FILE_NAME") && 0 < smap.get("SEARCH_FILE_NAME").toString().trim().length()) {
    			  lists = getList("mg.vdr.getTrashSearchList" + getDBName(dbName), smap);
    		  }else{
    			  lists = getList("mg.vdr.getTrashList" + getDBName(dbName), smap);
    		  }

    		  JSONArray jArray = new JSONArray();

    		  for(int i = 0; i < lists.size(); i++) {
    			  
    			  Map map =(Map) lists.get(i);
    			  
    			  JSONObject listdata = new JSONObject();

    			  listdata.put("ROW_ID", String.valueOf(i + 1));
    			  
    			  for(int j = 0; j < cols.length; j++) {
    				  listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
    			  }
    			  
    			  jArray.add(listdata);
    		  }

    		  obj.put("data", jArray);
    		  obj.put("errmsg", "success");
    		  obj.put("errcode", "0");
    		  return write(obj.toString());
    	  } catch(Exception e) {
    		  obj.put("errmsg", mm.getMessage("COMG_1001"));
    		  obj.put("errcode", "-1");
    		  return write(obj.toString());
    	  }
      } 
       
      /**
       * 문서 삭제 -> 휴지통 이동
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse removeDoc(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
    	  
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";

    	  try {
    		  
    		  tx.begin(); 
    		  
    		  Map item = request.getMap();
    		  
    		  item.put("R_USER_ID", user_id);
    		  
    		  String[] DOC_IDS = item.get("DOC_IDS").toString().split(",");
    		  
    		  for(String DOC_ID : DOC_IDS){
    			  
    			  item.put("DOC_ID", DOC_ID);
    			  
        		  updateObject("mg.vdr.removeDoc", item);
        		  createObject("mg.vdr.createTrash", item); 		  
        		  
        		  item.remove("R_USER_ID");
        		  updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), item);
        		  
        		  ObjectMapper mapper = new ObjectMapper();
        		  
        		  JSONParser parser = new JSONParser();
        		  
        		  List list = getList("mg.cp.selectCPMaster" + getDBName(pm.getString("component.sql.database")), item);
        		  
        		  for(int i = 0; i < list.size(); i++) {
        			  
        			  Map map = (Map)list.get(i);
        			  
        			  JSONObject jsonObj = (JSONObject) parser.parse(map.get("FILE_LIST").toString());
        			  
        			  JSONArray jsonArrayDocs = (JSONArray) parser.parse(jsonObj.get("docs").toString());
        			  
        			  if(0 < jsonArrayDocs.size()) {
            			  
        				  JSONArray updateJsonArrayDocs = new JSONArray();
            			  
            			  for(int j = 0; j < jsonArrayDocs.size(); j++) {
            				  
            				  JSONObject changeObj = (JSONObject) parser.parse(jsonArrayDocs.get(j).toString());
            				  
            				  if(-1 < item.get("DOC_IDS").toString().indexOf(changeObj.get("DOC_ID").toString())) {
            					  changeObj.put("DEL_YN", "Y");
            				  }
            				  
            				  updateJsonArrayDocs.add(changeObj);
            			  }
            			  
            			  jsonObj.put("docs", updateJsonArrayDocs);

            			  item.put("FILE_LIST", mapper.writeValueAsString(jsonObj));
            			  item.put("DOC_NO", map.get("DOC_NO"));
            			  
            			  updateObject("mg.cp.updateCPMaster" + getDBName(pm.getString("component.sql.database")), item);    				  
        			  }
        		  }
    		  }
    		  
    		  item.put("DOC_IDS_LIST", DOC_IDS);
    		  List docInfoList = getList("mg.vdr.getDocInfoList" + getDBName(pm.getString("component.sql.database")), item);
    		  
    		  for(int i = 0; i < docInfoList.size(); i++) {
    			  
    			  Map docInfo = (Map) docInfoList.get(i);
    			  
    			  docInfo.put("LOG_CL_CD", "DD");
    			  docInfo.put("USER_ID", user_id);    
    			  docInfo.put("LANG", item.get("LANG"));
        		  
        		  insertAccessLog(docInfo);    			  
    		  }
    		  
    		  obj.put("errmsg", "success");
    		  obj.put("errcode", "0");
    		  
    		  tx.commit();
    		  
    		  return write(obj.toString());

    	  } catch(Exception e) {
    		  tx.rollback();
    		  obj.put("errmsg", mm.getMessage("COMG_1007"));
    		  obj.put("errcode", "-1");   
    		  return write(obj.toString());
    	  }
      }
      
      /**
       * 휴지통 이동 -> 복원
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse restoreDoc(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
    	  
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";

    	  try {
    		  
    		  tx.begin(); 
    		  
    		  Map item = request.getMap();
    		  
    		  item.put("R_USER_ID", user_id);
    		  
    		  String[] DOC_IDS = item.get("DOC_IDS").toString().split(",");
    		  
    		  for(String DOC_ID : DOC_IDS){
    			  
    			  item.put("DOC_ID", DOC_ID);
    			  
        		  updateObject("mg.vdr.restoreDoc", item);
        		  createObject("mg.vdr.deleteTrash", item);
        		  
        		  item.remove("R_USER_ID");
        		  updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), item);
        		  
        		  ObjectMapper mapper = new ObjectMapper();
        		  
        		  JSONParser parser = new JSONParser();
        		  
        		  List list = getList("mg.cp.selectCPMaster" + getDBName(pm.getString("component.sql.database")), item);
        		  
        		  for(int i = 0; i < list.size(); i++) {
        			  
        			  Map map = (Map)list.get(i);
        			  
        			  JSONObject jsonObj = (JSONObject) parser.parse(map.get("FILE_LIST").toString());
        			  
        			  JSONArray jsonArrayDocs = (JSONArray) parser.parse(jsonObj.get("docs").toString());
        			  
        			  if(0 < jsonArrayDocs.size()) {
            			  
        				  JSONArray updateJsonArrayDocs = new JSONArray();
            			  
            			  for(int j = 0; j < jsonArrayDocs.size(); j++) {
            				  
            				  JSONObject changeObj = (JSONObject) parser.parse(jsonArrayDocs.get(j).toString());
            				  
            				  if(-1 < item.get("DOC_IDS").toString().indexOf(changeObj.get("DOC_ID").toString())) {
            					  changeObj.put("DEL_YN", "N");  
            				  }
            				  
            				  updateJsonArrayDocs.add(changeObj);
            			  }
            			  
            			  jsonObj.put("docs", updateJsonArrayDocs);

            			  item.put("FILE_LIST", mapper.writeValueAsString(jsonObj));
            			  item.put("DOC_NO", map.get("DOC_NO"));
            			  
            			  updateObject("mg.cp.updateCPMaster" + getDBName(pm.getString("component.sql.database")), item);    				  
        			  }
        		  }    			  
    		  }
    		  
    		  obj.put("errmsg", "success");
    		  obj.put("errcode", "0");
    		  tx.commit();
    		  return write(obj.toString());
    	  } catch(Exception e) {
    		  tx.rollback();
    		  obj.put("errmsg", mm.getMessage("COMG_1007"));
    		  obj.put("errcode", "-1");   
    		  return write(obj.toString());
    	  }
      }
      
      /**
       * 휴지통 완전삭제
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse trashCleanFolder(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
    	  
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";

    	  try {
    		  
    		  tx.begin(); 
    		  
    		  Map item = request.getMap();
    		  
    		  item.put("R_USER_ID", user_id);
    		  item.put("FOLDER_IDS_LIST", item.get("FOLDER_IDS").toString().split(","));
    		  item.put("isTotal", true);
    		  
    		  List lists = getList("mg.vdr.getFolderChildFullTarget" + getDBName(pm.getString("component.sql.database")), item);
    		  
    		  for (int i = 0; i < lists.size(); i++) {
    			  
    			  Map map = (Map) lists.get(i);
    			  
    			  if("FOLDER".equals(map.get("TYPE").toString())) continue;
    			  
    			  map.put("R_USER_ID", item.get("R_USER_ID"));
    			  
    			  deleteObject("mg.vdr.deleteTrash", map);		  
   				  deleteObject("mg.vdr.deleteDocLog", map);
   				  deleteObject("mg.vdr.deleteDocFavorites", map); 
   				  deleteObject("mg.vdr.deleteDocComment", map);    
   				  deleteObject("mg.vdr.deleteDocHist", map);
   				  deleteObject("mg.vdr.deleteDocVer", map); 
   				  deleteObject("mg.vdr.deleteDocAuth", map);  	        
   				  deleteObject("mg.vdr.deleteDoc", map);
   				 
   				  fum.deleteUpload(map.get("ROOM_ID").toString(), map.get("DOC_ID").toString(), request);
   				 
   				  map.remove("R_USER_ID");
   				  map.put("MOD_STOP", "2");  
   				 
   				  updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), map);
    		  }
    		  
    		  for (int i = 0; i < lists.size(); i++) {
    			  
    			  Map map = (Map) lists.get(i);
    			  
    			  if(!"FOLDER".equals(map.get("TYPE").toString())) continue;
    			  
    			  map.put("R_USER_ID", item.get("R_USER_ID"));
    			  
    			  deleteObject("mg.vdr.deleteFolderFavorites", map);
    			  deleteObject("mg.vdr.deleteFolderAuth", map);
    			  deleteObject("mg.vdr.deleteFolder", map);
    		  }
    		  
    		  tx.commit();

    		  obj.put("errmsg", "success");
    		  obj.put("errcode", "0");
    		  
    		  return write(obj.toString());

    	  } catch(Exception e) {
    		  obj.put("errmsg", mm.getMessage("COMG_1007"));
    		  obj.put("errcode", "-1");   
    		  return write(obj.toString());
    	  }
      }      
      
      /**
       * 휴지통 완전삭제
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse trashCleanDoc(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	  Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);

    	  try {
    		  tx.begin(); 
    		  
    		  Map item = request.getMap();
    		  
    		  item.put("R_USER_ID", user_id);
    		  
    		  String[] DOC_IDS = item.get("DOC_IDS").toString().split(",");
    		  
    		  for(String DOC_ID : DOC_IDS){
    			  
    			  item.put("DOC_ID", DOC_ID);
    				  
        		  deleteObject("mg.vdr.deleteTrash", item);		  
        		  deleteObject("mg.vdr.deleteDocLog", item);
        		  deleteObject("mg.vdr.deleteDocFavorites", item); 
        		  deleteObject("mg.vdr.deleteDocComment", item);    
        		  deleteObject("mg.vdr.deleteDocHist", item);
        		  deleteObject("mg.vdr.deleteDocVer", item); 
      	          deleteObject("mg.vdr.deleteDocAuth", item);  	        
      	          deleteObject("mg.vdr.deleteDoc", item);  	    	          
      	          
      	          fum.deleteUpload(String.valueOf(item.get("ROOM_ID")), String.valueOf(item.get("DOC_ID")), request);
      	          
    	  		  item.remove("R_USER_ID");
    	  		  item.put("MOD_STOP", "2");
    	  		  updateObject("mg.cp.deleteCP" + getDBName(pm.getString("component.sql.database")), item);    			  
    		  }
    		  
    		  tx.commit();

    		  obj.put("errmsg", "success");
    		  obj.put("errcode", "0");
    		  
    		  return write(obj.toString());

    	  } catch(Exception e) {
    		  log.error("---------------------------------------------------------------\n" + e.toString());

    		  obj.put("errmsg", mm.getMessage("COMG_1007", loc));
    		  obj.put("errcode", "-1");   
    		  return write(obj.toString());
    	  }
      }
      
      /**
       * 20191218 jnsim
       * 알림 메시지
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse sendAlarm(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
    	  String user_id =  request.getUser() != null ? request.getUser().getId() : "admin";
    	  Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);

    	  try {
    		  Map paramMap = request.getMap();
    		  
    		  //user_id = "test@test.com";
    		  
    		  paramMap.put("R_USER_ID", user_id);
    		  Map userInfoMap = (Map)getItem("mg.vdr.getUserInfo",paramMap);
    		  String userEmail = String.valueOf(userInfoMap.get("EMAIL"));
    		  
    		  Map docInfoMap = (Map)getItem("mg.vdr.getDocInfo",paramMap);
    		  String docNm = String.valueOf(docInfoMap.get("DOC_NM"));
    		  
    		  String type = String.valueOf(paramMap.get("TYPE"));
    		  int alarmType = (type == null || type.equals("")) ? 0 : Integer.parseInt(type);
    		  
    		  String content = "";
    		  
        	  //type 
        	  //1.가입초대 
        	  //2.비밀번호 재설정 인증번호
        	  //3.룸에 신규 문서 등록
        	  //4.본인작성 문서에 코멘트 추가
        	  //5.본인작성 문서의 버전업
        	  switch (alarmType) {
    	    	  case 1:
    	    		  content = "http://localhost:8080/index.html#/portal/login";
    	    		  break;
    	    	  case 2:
    	    		  content = "1234";
    	    		  break;
    	    	  case 3:
    	    		  content = docNm;
    	    		  break;
    	    	  case 4:
    	    		  content = docNm;
    	    		  break;
    	    	  case 5:
    	    		  content = docNm;
    	    		  break;
        	  }
    		  
        	  sendMail(userEmail, docNm, content, alarmType, (null == paramMap.get("LANG"))? "KO" : paramMap.get("LANG").toString());
    		  
    		  
    		  obj.put("errmsg", "success");
    		  obj.put("errcode", "0");
    		  return write(obj.toString());

    	  } catch(Exception e) {
    		  log.error("---------------------------------------------------------------\n" + e.toString());

    		  obj.put("errmsg", mm.getMessage("MAIL_0072", loc));
    		  obj.put("errcode", "-1");   
    		  return write(obj.toString());
    	  }
      }
     
	/**
	 * 타임라인 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getTimeLine(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		String user_id = (null != request.getUser()) ? request.getUser().getId() : "admin";
		Locale loc = (null != request.getUser()) ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);

		try {

			Map item = request.getMap();
			List lists;
			item.put("R_USER_ID", user_id);
			
			if(null != item.get("Preview") && "true".equals(item.get("Preview").toString())) {
				lists = getList("mg.vdr.getYesTimeLine"+ getDBName(pm.getString("component.sql.database")), item);
			}else{
				lists = getList("mg.vdr.getTimeLine" + getDBName(pm.getString("component.sql.database")), item);
			}

			long CURRENT_TIME = System.currentTimeMillis();
			
			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();

				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}
				
				if(null != item.get("Preview") && "true".equals(item.get("Preview").toString())) {
					
					long REG_DT_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(map.get("REG_DT").toString()).getTime();
					long DIFF_TIME = CURRENT_TIME - REG_DT_TIME;
					
					listData.put("DIFF_TIME", (0 > DIFF_TIME)? 0 : DIFF_TIME / 1000);
				}

				jArray.add(listData);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}      
	/**
	 * 메터검색 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getDocMeta(IRequest request) throws Exception {
		
		String user_id =  request.getUser() != null ? request.getUser().getId() : "iXVDR@inzent.com";
		
     	JSONObject obj = new JSONObject();
      	
     	String col = request.getStringParam("grid_col_id");
      	
     	String[] cols = col.split(",");
     	
     	Map item = request.getMap();
     	
     	item.put("R_USER_ID", user_id);
     	
     	try {
     		
     		JSONArray jArray = new JSONArray();
     		
     		List lists = getList("mg.vdr.getDocMeta" + getDBName(pm.getString("component.sql.database")), item);
     		
     		for(int i = 0; i < lists.size(); i++) {
     			
     			Map map =(Map) lists.get(i);
	          	
     			JSONObject listdata = new JSONObject();
	          	
	          	listdata.put("ROW_ID", String.valueOf(i + 1));
	          	
	          	for(int j = 0; j < cols.length; j++) {
	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	          	
	          	jArray.add(listdata);
	        }
     		
     		obj.put("data", jArray);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
			return write(obj.toString());
 	     }catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", mm.getMessage("COMG_1001"));
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
	} 
	
	/**
	 * 폴더 경로 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getFolderPath(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {

			Map item = request.getMap();
			List lists = getList("mg.vdr.getFolderPath" + getDBName(pm.getString("component.sql.database")), item);
			
			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();

				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}

				jArray.add(listData);
			}
			
			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			
			return write(obj.toString());
		}
	}
	
	/**
	 * 유저별 샌드박스 권한을 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectSandBoxUserAuth(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		Locale loc = (null != request.getUser())? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		
		try {

			Map item = request.getMap();
			
			Map map = (Map) getItem("mg.vdr.selectSandBoxUserAuth" + getDBName(pm.getString("component.sql.database")), item);
			
			if(null == map) throw new Exception();
			
			Iterator<String> keys = map.keySet().iterator();

			JSONObject data = new JSONObject();

			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			
			return write(obj.toString());
		}
	}
	
	/**
	 * 폴더 정보 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectFolderInfo(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			Map map = (Map) getItem("mg.vdr.selectFolderInfo" + getDBName(pm.getString("component.sql.database")), item);
			
			if(null == map) throw new Exception();
			
			Iterator<String> keys = map.keySet().iterator();

			JSONObject data = new JSONObject();

			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}	
	
	/**
	 * 문서 정보 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectDocInfo(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			Map map = (Map) getItem("mg.vdr.selectDocInfo" + getDBName(pm.getString("component.sql.database")), item);
			
			if(null == map) throw new Exception();
			
			Iterator<String> keys = map.keySet().iterator();

			JSONObject data = new JSONObject();

			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}	
	
	/**
	 * 폴더 / 문서 권한 수정.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse updateTargetAuth(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();
		
		try {
			
			tx.begin();
			
			Map item = request.getMap();
			
			if("folder".equals((String) item.get("LIST_TYPE"))) {
				
	        	updateObject("mg.vdr.updateFolderUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
	        	
	        	if("Y".equals(item.get("USER_AUTH").toString())) {
	        		
	        		updateObject("mg.vdr.updateFolderUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
	        		
	        		if(null != item.get("UPDATE_LIST")) {
		        		
	        			JSONParser parser = new JSONParser();
			        	
			        	JSONArray update_list = (JSONArray) parser.parse((String) item.get("UPDATE_LIST"));
			        	
			        	for(int i = 0; i < update_list.size(); i++) {
			        		
			        		Map param = new ObjectMapper().readValue(((JSONObject) update_list.get(i)).toJSONString(), Map.class);
			        		
			        		param.put("FOLDER_ID", item.get("FOLDER_ID"));
			        		
			        		updateObject("mg.vdr.updateFolderUserAuth" + getDBName(pm.getString("component.sql.database")), param);
			        	}
			        	
			        	Integer targetDownUserAuthCnt = (Integer) getItem("mg.vdr.selectTargetDownUserAuthCnt" + getDBName(pm.getString("component.sql.database")), item);
			        	
			        	if(0 < targetDownUserAuthCnt){
				        	
			        		updateObject("mg.vdr.updateFolderDownUserAuth" + getDBName(pm.getString("component.sql.database")), item);
			        		
			        		updateObject("mg.vdr.updateDocDownUserAuth" + getDBName(pm.getString("component.sql.database")), item);
			        		
			        		updateObject("mg.vdr.updateFolderDownUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
			        		
			        		updateObject("mg.vdr.updateDocDownUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
			        	}
	        		}
	        	}else{
	        		updateObject("mg.vdr.updateFolderUserAuthRelease" + getDBName(pm.getString("component.sql.database")), item);
	        	}

			}else{
				
 	        	updateObject("mg.vdr.updateDocUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
 	        	
 	        	if("Y".equals(item.get("USER_AUTH").toString())) {
 	        		
 	        		updateObject("mg.vdr.updateDocUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
 	 	        	
 	        		if(null != item.get("UPDATE_LIST")) {
 	 	        		
 	        			JSONParser parser = new JSONParser();
 	 	 	        	
 	 	 	        	JSONArray update_list = (JSONArray) parser.parse((String) item.get("UPDATE_LIST"));
 	 	 	        	
 	 	 	        	for(int i = 0; i < update_list.size(); i++) {
 	 	 	        		
 	 	 	        		Map param = new ObjectMapper().readValue(((JSONObject) update_list.get(i)).toJSONString(), Map.class);
 	 	 	        		
 	 	 	        		param.put("DOC_ID", item.get("DOC_ID"));
 	 	 	        		
 	 	 	        		updateObject("mg.vdr.updateDocUserAuth" + getDBName(pm.getString("component.sql.database")), param);
 	 	 	        	} 	        			
 	        		}
 	        	}else{
 	        		updateObject("mg.vdr.updateDocUserAuthRelease" + getDBName(pm.getString("component.sql.database")), item);
 	        	}
			}
			
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			tx.commit();
			
			return write(obj.toString());
		} catch (Exception e) {
			tx.rollback();
			obj.put("errmsg", mm.getMessage("COMG_1007"));
			obj.put("errcode", "-1");
            return write(obj.toString());
		}
	}
	
	/**
	 * 문서 권한 정보를 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectDocAuthInfo(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			item.put("USER_ID", request.getUser().getId());
			
			Map map = (Map) getItem("mg.vdr.selectDocAuthInfo" + getDBName(pm.getString("component.sql.database")), item);
			
			Iterator<String> keys = map.keySet().iterator();

			JSONObject data = new JSONObject();

			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}
	
	
	/**
	 * 유저 정보를 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getUserInfo(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			if (request.getUser() != null) {
				IUser user = request.getUser();
				
				obj.put("USER_ID", user.getId());
				obj.put("USER_NM", user.getName());
				obj.put("USER_ROLL", user.getProperty("USER_ROLL"));
				obj.put("errmsg", "success");
				obj.put("errcode", "0");
			}
			else {
				obj.put("errmsg", mm.getMessage("COMG_1001"));
				obj.put("errcode", "-1");
			}
			
			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}
	
	/**
	 * 저장용량을 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getStorageCapacity(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			item.put("USER_ID", request.getUser().getId());
			
			Map map = (Map) getItem("mg.vdr.getTotalDocCapacity" + getDBName(pm.getString("component.sql.database")), item);
			Map roomInfoMap = (Map)getItem("mg.vdr.getRoomInfo", item);
			
			JSONObject data = new JSONObject();
			
			data.put("totalDocCapacity", (null == map)? 0 : NumberUtils.toLong(map.get("FILE_SIZE").toString()));
			data.put("storageCapacity", (null == roomInfoMap)? 0 : NumberUtils.toLong(roomInfoMap.get("CONTRACT_STORAGE").toString()));
			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}	
	
	/**
	 * 샌드박스 버전정보 가져온다.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getSandboxVersion(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();

		try {

			Map item = request.getMap();
			
			item.put("TYPE", "SB");			
			Map map = (Map)getItem("mg.vdr.getDataroomInfo", item);

			JSONObject data = new JSONObject();
			data.put("patchVersion", map.get("PATCH_VERSION"));			
			data.put("moduleVersion", map.get("MODULE_VERSION"));			
			obj.put("data", data);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}	
	
	private IResponse sandboxDownload(IRequest request) throws Exception 
	{
		JSONObject obj = new JSONObject();

		Map smap = request.getMap();
		log.info("- ixVDR Download Map  :  " + smap.toString());

		try {

			String downFile = pm.getString("component.path.sandbox");
			return downloadFile(downFile); 
			
		} catch(Exception e) {
			log.error("---File Not Found----------------------------------------------------------\n" + e.toString());
			return downloadFile(null);
		}
	}  

	 /**
     * 20191218 jnsim
     * 테스트를 위한 메소드
     */
    private IResponse testCode(IRequest request) throws Exception 
    {
  	  JSONObject obj = new JSONObject();
  	  
  	//sendMailTest();  	  
  	  
  	  	cryptorSHAEncodeThread th = new cryptorSHAEncodeThread("C:/Dev/upload/20200310/vdr/explorer.do/F2020031018041455853682882004/111.pptx", "c:/temp/123",  "12345");
		th.start();
		try{
			th.join();
		}catch(Exception e){ 
			log.info("testDecrypt Error -----"+e.toString());
		}
		long retval = th.getResultLong();
		
		
		
		log.info("retval : "+retval);
		
		
		cryptorSHAThread th1 = new cryptorSHAThread("c:/temp/111", "c:/temp/aaa.pptx",  "12345");
		th1.start();
		try{
			th1.join();
		}catch(Exception e){ 
			log.info("testDecrypt Error -----"+e.toString());
		}
		long retval1 = th1.getResultLong();
		
		log.info("retval1 : "+retval1);
  	  
  	  obj.put("errmsg", "success");
		  obj.put("errcode", "0");
		  
		  
		  
		  
		  
		  
  	  return write(obj.toString());
    }
    
    
    
    
    
    
    
    
    /**
     * 폴더 생성
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse NimsouCreateFolder(IRequest request) throws Exception 
    {
   	 JSONObject obj = new JSONObject();
   	 
	   	String ids_info = request.getStringParam("ids");
	    String cols_ids = request.getStringParam("col_ids");
	    String[] cols = cols_ids.split(",");
	    String[] ids = ids_info.split(",");
        String user_id = request.getUser().getId();
        String dbName = pm.getString("component.sql.database");

        try {
       	
       	tx.begin();
       	
       	Map item = null;
       	for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);
		        }

       	String p_menu_id = request.getStringParam(ids[i] +"_P_FOLDER_ID");
       	String menu_id = request.getStringParam(ids[i] +"_FOLDER_ID");
       	String menu_nm = request.getStringParam(ids[i] +"_FOLDER_NM");

       	item.put("P_MENU_ID",  p_menu_id);
       	item.put("MENU_ID",  menu_id);
       	item.put("MENU_NM",  menu_nm);
       	item.put("USERID",  user_id);
       	item.put("OWNER_ID",  user_id);
  	    item.put("FOLDER_STYLE", "F");
       	item.put("CREATOR_ID", user_id);
       	item.put("MODIFIER_ID", user_id);
       	item.put("R_USER_ID", user_id);
       	item.put("IS_DELETE", "N");
       	item.put("FOLDER_ORDER", 0);
  	    
       		
        
       	log.info("----------------createFolder-------------------------------------\n" + item.toString());
       	log.info(Integer.parseInt(getItem("mg.vdr.nimsouCountFolder", item).toString()));
       	if( Integer.parseInt(getItem("mg.vdr.nimsouCountFolder", item).toString() ) > 0 ) {
 	        log.info("기존폴더 업데이트");
 			updateObject("mg.vdr.NimsouUpdateFolder", item);
 		} else {
 			String folderId = getMakeId("F");
 	       	item.put("FOLDER_ID", folderId);
 			log.info("새폴더 생성");
 			createObject("mg.vdr.NimsouCreateFolder", item);
 			createObject("mg.vdr.NimsouCreateAuth", item);
 	    }
       	
       	log.info("createObject");

	        /*createObject("mg.vdr.NimsouCreateFolderAuth", item);*/
	        
	      /* if(null != item.get("FAVORITE_YN")) updateObject("mg.vdr.updateBookmark", item);
	        
	        if(null != item.get("UPDATE_LIST")) {
	        	
	        	item.put("USER_AUTH", "Y");
	        	
	        	updateObject("mg.vdr.updateFolderUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
	        	
	        	updateObject("mg.vdr.updateFolderUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
	        	
	        	JSONParser parser = new JSONParser();
	        	
	        	JSONArray update_list = (JSONArray) parser.parse(URLDecoder.decode((String) item.get("UPDATE_LIST"), "UTF-8"));
	        	
	        	for(int i = 0; i < update_list.size(); i++) {
	        		
	        		Map param = new ObjectMapper().readValue(((JSONObject) update_list.get(i)).toJSONString(), Map.class);
	        		
	        		param.put("FOLDER_ID", folderId);
	        		
	        		updateObject("mg.vdr.updateFolderUserAuth" + getDBName(pm.getString("component.sql.database")), param);
	        	}
	        }else{
	        	
	        	Map pFolderInfo = (Map) getItem("mg.vdr.selectFolderInfo" + getDBName(pm.getString("component.sql.database")), new HashMap() {{put("FOLDER_ID", item.get("P_FOLDER_ID"));}});
	        	
	        	if(null != pFolderInfo && "Y".equals(pFolderInfo.get("USER_AUTH").toString())){
	        		
	        		item.put("USER_AUTH", "Y");
	 	        	updateObject("mg.vdr.updateFolderUserAuthFlag" + getDBName(pm.getString("component.sql.database")), item);
	 	        	
	 	        	updateObject("mg.vdr.updateFolderUserAuthAdmin" + getDBName(pm.getString("component.sql.database")), item);
	 	        	
	 	        	item.put("ACCESS", "Y");
	 	        	item.put("VIEW", "Y");
	 	        	item.put("EDIT", "Y");
	 	        	
	 	        	updateObject("mg.vdr.updateFolderUserAuth" + getDBName(pm.getString("component.sql.database")), item);
	        	}
	        }*/
       	}
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

/*			item.put("LOG_CL_CD", "FC");
			item.put("USER_ID", user_id);
			
			insertAccessLog(item);*/
			
			tx.commit();
			String ret = responseTranData("msg","true", "doSave", "doSaveEnd");
	        return writeXml(ret);
	        /*return write(obj.toString());*/
	        
        } catch(Exception e) {
       	 tx.rollback();
       	 log.error("---------------------------------------------------------------\n" + e.toString());
       	 obj.put("errmsg", "fail");
			 obj.put("errcode", "-1");   
            return write(obj.toString());
        }
    }
    
    /**
     * 폴더 권한 설정
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse NimsouFolderAuthSet(IRequest request) throws Exception 
    {
   	 JSONObject obj = new JSONObject();
   	 
	   	String ids_info = request.getStringParam("ids");
	    String cols_ids = request.getStringParam("col_ids");
	    String[] cols = cols_ids.split(",");
	    String[] ids = ids_info.split(",");
        String user_id = request.getUser().getId();
        String dbName = pm.getString("component.sql.database");

        try {
       	
       	tx.begin();
       	
       	Map item = null;
       	for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);
		        }

       	String set_user_id = request.getStringParam(ids[i] +"_USER_ID");
       	String aces = request.getStringParam(ids[i] +"_ACES");
       	String red = request.getStringParam(ids[i] +"_RED");
       	String fad = request.getStringParam(ids[i] +"_FAD");
       	String dlt = request.getStringParam(ids[i] +"_DLT");
       	String menu_id = request.getStringParam("CHECK_P_ID");

       	item.put("SET_USER_ID",  set_user_id);
       	item.put("ACES",  aces);
       	item.put("RED",  red);
       	item.put("FAD",  fad);
       	item.put("DLT",  dlt);
       	item.put("MENU_ID",  menu_id);
        
       	log.info("----------------createFolder-------------------------------------\n" + item.toString());
       	log.info(Integer.parseInt(getItem("mg.vdr.nimsouCountFolderAuth", item).toString()));
       	if( Integer.parseInt(getItem("mg.vdr.nimsouCountFolderAuth", item).toString() ) > 0 ) {
 	        log.info("기존유저권한 업데이트");
 			updateObject("mg.vdr.NimsouUpdateFolderAuth", item);
 		} else {
 			log.info("유저권한 생성");
 			createObject("mg.vdr.NimsouCreateFolderNewAuth", item);
 	    }
       	
       	log.info("createObject");

       	}
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

/*			item.put("LOG_CL_CD", "FC");
			item.put("USER_ID", user_id);
			
			insertAccessLog(item);*/
			
			tx.commit();
			String ret = responseTranData("msg","true", "doSave", "doSaveEnd");
	        return writeXml(ret);
	        /*return write(obj.toString());*/
	        
        } catch(Exception e) {
       	 tx.rollback();
       	 log.error("---------------------------------------------------------------\n" + e.toString());
       	 obj.put("errmsg", "fail");
			 obj.put("errcode", "-1");   
            return write(obj.toString());
        }
    }
    
    /**
     * 탐색기의 폴더 조회 
     * @param request
     * @return
     * @throws Exception
     */

     public List NimsouGetFolderTreeS(IUser s_user) throws Exception
     {
    	 
    	 JSONObject obj = new JSONObject();
      	// String col = request.getStringParam("grid_col_id");
      	// String[] cols = col.split(",");

      	Map smap = new HashMap();
      	List lists = null;

 		// log.info("------getFolderTree---IRequest--- > \n" + smap.toString());
         
         try {
        	 log.info("request 이전"+s_user);
         	String user_id = s_user.getId();/*request.getUser().getId();*/
         	log.info("권한을 위한 request 유저아이디 : "+user_id);
         	smap.put("USER_ID", user_id);
         	
        		lists = getList("mg.vdr.NimsouSelectFolderTree",smap);
             
         	
//         	if(dbName.equals("mssql")){
//         		lists = getList("mg.vdr.selectFolerTree",smap);
//         	}else{
//         		lists = getList("mg.vdr.selectFolerTree_" + dbName, smap);
//         	}
         	
         	log.info("------getFolderTree---lists--- > \n" + lists.toString());
         	 
         	JSONArray jArray = new JSONArray();
         	TreeMaker tm = new TreeMaker();

 		 	 List<TreeVO> lst = makefolder(lists);
 			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
 			 			 
 			 /*Map r_map = (Map)getItem("mg.vdr.getRoomInfo", smap);
 			 
 			 log.info("------getFolderTree---r_map--- > \n" + r_map.toString());*/
 			 
 			 JSONObject root = new JSONObject();
 			 /*root = makeRoom(r_map);*/
 			 root.put("children", treeData);
 			 
 			 jArray.add(root);
                 
 	         obj.put("data", jArray);
 			 obj.put("errmsg", "true");
 			 obj.put("errcode", "0");
 			 
 			 log.info("------getFolderTree------ > \n" + obj.toString());
 			 
 			  ObjectMapper mapper = new ObjectMapper();
 			  String str = "";
 			  try {
 					str = mapper.writeValueAsString(obj);
 			  }
 			  catch (IOException ex) {
 					log.error("[getFolderTree] " + ex.getMessage());
 			   }
 				
 			   log.info("------getFolderTree-=----str------ > " + str);
 			 
 			 
 			return lists;
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", "false");
 			 obj.put("errcode", "-1");
 	    	 return lists;
 	     }
         /*try {
         	
         	List lists;
         	List treeLists = new ArrayList<Object>();
         	Map<String,String> treeMaps = new HashMap<>();
         	
        		lists = getList("mg.vdr.NimsouSelectFolderTree",smap);
         	
         	log.info("------getFolderTree---lists--- > \n" + lists.toString());
         	
         	String stringList  = "";

         	for (int i=0; i<lists.size(); i++) {
         		int j = lists.size()-1;
    			Map m = (Map) lists.get(i);

    			log.info("------for문 초기--- > \n");
    			
    			String folder_id = (String) m.get("FOLDER_ID");
    			treeMaps.put("FOLDER_ID",folder_id);
    			String p_folder_id = (String) m.get("P_FOLDER_ID");
    			treeMaps.put("P_FOLDER_ID",p_folder_id);
    			String folder_nm = (String) m.get("FOLDER_NM");
    			treeMaps.put("FOLDER_NM",folder_nm);
    			
    			log.info("------map 데이터 넣기 완료--- > \n"+treeMaps.toString());
    			
    			treeLists.add(treeMaps);
    			
    			log.info("------List에 map 데이터 넣기 완료--- > \n");
    		}

         	log.info("------getFolderTree---treeList--- > \n" + treeLists);
         	
         	String stringTreeLists = treeLists.toString();
         	
         	log.info("------getFolderTree---stringTreeLists--- > \n" + stringTreeLists);
         	 			   
 			String ret = responseSelectTreeFolder(stringTreeLists,"status", "doSave", "doSaveEnd");
 			log.info(ret);
 			return writeXml(ret);*/
     }
     
     public String NimsouChangePWDShow(IUser s_user) throws Exception
     {
    	 
    	 JSONObject obj = new JSONObject();
      	// String col = request.getStringParam("grid_col_id");
      	// String[] cols = col.split(",");

      	Map smap = new HashMap();
      	String isDef = "0000";

 		// log.info("------getFolderTree---IRequest--- > \n" + smap.toString());
         
         try {
        	 log.info("request 이전"+s_user);
         	String user_id = s_user.getId();/*request.getUser().getId();*/
         	log.info("권한을 위한 request 유저아이디 : "+user_id);
         	smap.put("USER_ID", user_id);
         	
         	if( Integer.parseInt(getItem("mg.vdr.checkNimsPassDef", smap).toString() ) > 0 ) {
     	        log.info("디폴트 비번상태");
     	       isDef = "1111";
     		} else {
     			log.info("변경된 비번상태");
     			isDef = "0000";
     	    }
         	log.info("패스워드 상태 : "+isDef);
 			return isDef;
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", "false");
 			 obj.put("errcode", "-1");
 	    	 return isDef;
 	     }
     }
     
     private IResponse NimsouGetFolderSetAuth(IRequest request) {
         String msg="", flag="";

         try {
         	tx.begin();
 	        Map item = null;
 	        String user_id = request.getUser().getId();
 	        String folder_id = request.getStringParam("FOLDER_ID");
 	        
		   	item.put("USER_ID", user_id);
		   	item.put("FOLDER_ID", folder_id);
		    
			String userAuth = getItem("mg.vdr.NimsouGetFolderSetAuth",item).toString();

 	        tx.commit();
 	        msg = userAuth;
 	        flag = "setTrue";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 	        flag = "false";
         }     
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");

         return writeXml(ret);
 	}
     
     /**
      * 자료실 권한에따른 아이콘 읽어들이기 
      * @param request
      * @return
      * @throws Exception
      */
      public List NimsouGetFolderTreeAuth() throws Exception 
      {
     	 JSONObject obj = new JSONObject();
       	// String col = request.getStringParam("grid_col_id");
       	// String[] cols = col.split(",");

       	Map smap = new HashMap();
       	List lists = null;

  		// log.info("------getFolderTree---IRequest--- > \n" + smap.toString());
          
          try {
          	
          	
         		lists = getList("mg.vdr.NimsouGetFolderTreeAuth",smap);

          	log.info("------getFolderTree---lists--- > \n" + lists.toString());
          	 
          	JSONArray jArray = new JSONArray();
          	TreeMaker tm = new TreeMaker();

  		 	 List<TreeVO> lst = makefolder(lists);
  			 List<TreeVO> treeData  = tm.makeTreeByHierarchy(lst);
  			 			 
  			 /*Map r_map = (Map)getItem("mg.vdr.getRoomInfo", smap);
  			 
  			 log.info("------getFolderTree---r_map--- > \n" + r_map.toString());*/
  			 
  			 JSONObject root = new JSONObject();
  			 /*root = makeRoom(r_map);*/
  			 root.put("children", treeData);
  			 
  			 jArray.add(root);
                  
  	         obj.put("data", jArray);
  			 obj.put("errmsg", "true");
  			 obj.put("errcode", "0");
  			 
  			 log.info("------getFolderTree------ > \n" + obj.toString());
  			 
  			  ObjectMapper mapper = new ObjectMapper();
  			  String str = "";
  			  try {
  					str = mapper.writeValueAsString(obj);
  			  }
  			  catch (IOException ex) {
  					log.error("[getFolderTree] " + ex.getMessage());
  			   }
  				
  			   log.info("------getFolderTree-=----str------ > " + str);
  			 
  			 
  			return lists;
  	     } catch(Exception e) {
  	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
  			 obj.put("errmsg", "false");
  			 obj.put("errcode", "-1");
  	    	 return lists;
  	     }
      }
    
     /**
      * 폴더 삭제
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse NimsouDeleteFolder(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
         String user_id = request.getUser().getId();
	        
		 
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	item = new HashMap();
 	       		for(int j=0; j < cols.length; j++ ) {
 	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		        item.put(cols[j], tmps);     
 	         	}
 	       		item.put("USER_ID", user_id);
 	       	 	deleteObject("mg.vdr.NimsouDeleteFolder", item);
 	       	 	deleteObject("mg.vdr.NimsouDeleteAuthWFolder", item);
 	        }
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 	        flag = "false";
         }     
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }
    
}