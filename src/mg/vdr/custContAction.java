package mg.vdr;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.component.user.IUser;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.common.CpcConstants;


public class custContAction extends BaseAction {
	
    /**
     * VDR 거래처 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("select")) {   /** 조회 */
            return selectCustCont(request);                                                
        }else if(request.isMode("save")) {   /** 저장 */
            return saveCustCont(request);                           
        }else if(request.isMode("update")) {   /** 수정 */
            return updateCustCont(request);                           
        }else if(request.isMode("updateManager")) {   /** 담당자 수정 */
            return updateCustContManager(request);                           
        }else if(request.isMode("delete")) {   /** 삭제        */
            return deleteCustCont(request);                           
        }else if(request.isMode("getNewContractInfo")) {   /** 신규계약 관련 데이터를 가져온다. */
            return getNewContractInfo(request);                           
        }else if(request.isMode("getContractInfo")) {   /** 계약 관련 데이터를 가져온다. */
            return getContractInfo(request);                           
        }else {
            return write(null);
        }
    }
    
    /**
     * 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectCustCont(IRequest request) throws Exception
     {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 String col = request.getStringParam("grid_col_id");
    	 
    	 String[] cols = col.split(",");
    	 
    	 Map smap = request.getMap();
    	 
    	 try {
    		 
    		 smap.put("R_USER_ID", request.getUser().getId());
    		 
    		 List lists = getList("mg.vdr.selectCustCont" + getDBName(pm.getString("component.sql.database")), smap);
    		 
    		 JSONArray jArray = new JSONArray();
    		 
    		 for(int i = 0; i < lists.size(); i++) {
    			 
    			 Map map = (Map)lists.get(i);
    			 
    			 Map userRoomCntMap = (Map)getItem("mg.vdr.selectCustContUserRoomCnt" + getDBName(pm.getString("component.sql.database")), map);
    			 Map fileSizeMap = (Map)getItem("mg.vdr.selectCustContFileSize" + getDBName(pm.getString("component.sql.database")), map);
    			 
    			 String USER_CNT = ((null != userRoomCntMap) && (null != userRoomCntMap.get("USER_CNT") && 0 < userRoomCntMap.get("USER_CNT").toString().trim().length())? userRoomCntMap.get("USER_CNT").toString() : "0");
    			 String ROOM_CNT = ((null != userRoomCntMap) && (null != userRoomCntMap.get("ROOM_CNT") && 0 < userRoomCntMap.get("ROOM_CNT").toString().trim().length())? userRoomCntMap.get("ROOM_CNT").toString() : "0");
    			 String STORAGE = ((null != fileSizeMap) && (null != fileSizeMap.get("STORAGE") && 0 < fileSizeMap.get("STORAGE").toString().trim().length())? this.getSize(NumberUtils.toLong(fileSizeMap.get("STORAGE").toString())) : "0 B");
    			 
    			 USER_CNT += " / " + ((null != map.get("USER_CNT") && 0 < map.get("USER_CNT").toString().trim().length())? map.get("USER_CNT").toString() : "0");
    			 ROOM_CNT += " / " + ((null != map.get("ROOM_CNT") && 0 < map.get("ROOM_CNT").toString().trim().length())? map.get("ROOM_CNT").toString() : "0");
    			 STORAGE += " / " +  ((null != map.get("STORAGE") && 0 < map.get("STORAGE").toString().trim().length())? this.getSize(NumberUtils.toLong(map.get("STORAGE").toString())) : "0 B");
    			 
    			 map.put("USER_CNT", USER_CNT);
    			 map.put("ROOM_CNT", ROOM_CNT);
    			 map.put("STORAGE", STORAGE);
    			 
    			 JSONObject listdata = new JSONObject();
    			 
    			 listdata.put("ROW_ID", String.valueOf(i + 1));
    			 
    			 for(int j = 0; j < cols.length; j++) {
    				 listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
    			 }
    			 
    			 jArray.add(listdata);
    		 }
    		 
    		 obj.put("data", jArray);
 			 obj.put("errmsg", "success");
 			 obj.put("errcode", "0");
 			 return write(obj.toString());
    	 }catch(Exception e) {
    		 obj.put("errmsg", mm.getMessage("COMG_1001"));
    		 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     } 	 
     }

     /**
      * 저장
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse saveCustCont(IRequest request) throws Exception 
     {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 try {
    		 
    		 tx.begin();
    		 
    		 Map item = request.getMap();
    		 
    		 item.put("R_USER_ID", request.getUser().getId());
    		 
    		 item.put("CTRT_NM",  URLDecoder.decode((String) item.get("CTRT_NM"), "UTF-8"));
    		 item.put("S_DT",  URLDecoder.decode((String) item.get("S_DT"), "UTF-8"));
    		 item.put("E_DT",  URLDecoder.decode((String) item.get("E_DT"), "UTF-8"));
    		 item.put("CONTRACT_COMPANY",  URLDecoder.decode((String) item.get("CONTRACT_COMPANY"), "UTF-8"));
    		 item.put("CONTRACT_EMAIL",  URLDecoder.decode((String) item.get("CONTRACT_EMAIL"), "UTF-8"));
    		 item.put("CONTRACT_TEL",  URLDecoder.decode((String) item.get("CONTRACT_TEL"), "UTF-8"));
    		 item.put("CONTRACT_USERNM",  URLDecoder.decode((String) item.get("CONTRACT_USERNM"), "UTF-8"));
    		 
    		 createObject("mg.vdr.saveCustCont" + getDBName(pm.getString("component.sql.database")), item);
    		 
    		 tx.commit();
    		 obj.put("errmsg", "success");
   		  	 obj.put("errcode", "0");
   		  	 return write(obj.toString());
   		 } catch(Exception e) {
   			 tx.rollback();
   			 obj.put("errmsg", mm.getMessage("COMG_1007"));
   			 obj.put("errcode", "-1");
   			 return write(obj.toString());
   		 }
     }
     
     /**
      * 수정
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateCustCont(IRequest request) throws Exception 
     {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 try {
    		 
    		 tx.begin();
    		 
    		 Map item = request.getMap();
    		 
    		 item.put("R_USER_ID", request.getUser().getId());
    		 
    		 item.put("S_DT",  URLDecoder.decode((String) item.get("S_DT"), "UTF-8"));
    		 item.put("E_DT",  URLDecoder.decode((String) item.get("E_DT"), "UTF-8"));
    		 item.put("CONTRACT_EMAIL",  URLDecoder.decode((String) item.get("CONTRACT_EMAIL"), "UTF-8"));
    		 item.put("CONTRACT_TEL",  URLDecoder.decode((String) item.get("CONTRACT_TEL"), "UTF-8"));
    		 item.put("CONTRACT_USERNM",  URLDecoder.decode((String) item.get("CONTRACT_USERNM"), "UTF-8"));
    		 
    		 updateObject("mg.vdr.updateCustCont" + getDBName(pm.getString("component.sql.database")), item);
    		 
    		 tx.commit();
    		 obj.put("errmsg", "success");
   		  	 obj.put("errcode", "0");
   		  	 return write(obj.toString());
   		 } catch(Exception e) {
   			 tx.rollback();
   			 obj.put("errmsg", mm.getMessage("COMG_1007"));
   			 obj.put("errcode", "-1");
   			 return write(obj.toString());
   		 }
     } 
     
     /**
      * 담당자 수정
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse updateCustContManager(IRequest request) throws Exception 
     {
    	 
    	 JSONObject obj = new JSONObject();
    	 
    	 try {
    		 
    		 tx.begin();
    		 
    		 Map item = request.getMap();
    		 
    		 item.put("R_USER_ID", request.getUser().getId());
    		 
    		 item.put("CONTRACT_EMAIL",  URLDecoder.decode((String) item.get("CONTRACT_EMAIL"), "UTF-8"));
    		 item.put("CONTRACT_TEL",  URLDecoder.decode((String) item.get("CONTRACT_TEL"), "UTF-8"));
    		 item.put("CONTRACT_USERNM",  URLDecoder.decode((String) item.get("CONTRACT_USERNM"), "UTF-8"));
    		 
    		 updateObject("mg.vdr.updateCustContManager" + getDBName(pm.getString("component.sql.database")), item);
    		 
    		 tx.commit();
    		 obj.put("errmsg", "success");
   		  	 obj.put("errcode", "0");
   		  	 return write(obj.toString());
   		 } catch(Exception e) {
   			 tx.rollback();
   			 obj.put("errmsg", mm.getMessage("COMG_1007"));
   			 obj.put("errcode", "-1");
   			 return write(obj.toString());
   		 }
     }      
     
     /**
      * 삭제 	
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse deleteCustCont(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);

         
         try {
 	        Map item = null;
 	       
 	   	
 	        for (int i = 0; i < ids.length; i++) {
 	       	 	item = new HashMap();
 	         	
 	       	 	for(int j=0; j < cols.length; j++ ) {
 	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		             item.put(cols[j], tmps);     
 		           
 	         	}
 	       	 	deleteObject("mg.vdr.deleteCustCont", item);
 	        }
 	        obj.put("errmsg", "success");
			obj.put("errcode", "0");
	        return write(obj.toString());
         } catch(Exception e) {
            log.error("---------------------------------------------------------------\n" + e.toString());
            obj.put("errmsg", mm.getMessage("COMG_1008", loc));
			obj.put("errcode", "-1");   
            return write(obj.toString());
         }
     }
     
 	/**
 	 * 신규계약 관련 데이터를 가져온다.
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse getNewContractInfo(IRequest request) throws Exception {

 		JSONObject obj = new JSONObject();

 		try {

 			Map item = request.getMap();
 			
 			IUser user = request.getUser();
 			
 			item.put("USER_ROLL", user.getProperty("USER_ROLL"));
 			
 			List lists = getList("mg.vdr.getContractTypeList" + getDBName(pm.getString("component.sql.database")), item);
 			
 			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();

				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}

				jArray.add(listData);
			}
 			
 			JSONObject data = new JSONObject();
 			
 			item.put("PARAM_USER_ID", user.getId());
 			Map USER_INFO = (Map) getItem("mg.vdr.selectUser" + getDBName(pm.getString("component.sql.database")), item);
 			data.put("NAME", ("S03".equals(user.getProperty("USER_ROLL")))? USER_INFO.get("COMPANY_NM") : USER_INFO.get("USER_NM"));
 			data.put("manager", USER_INFO.get("USER_NM"));
 			data.put("email", USER_INFO.get("USER_ID"));
 			data.put("contact", USER_INFO.get("TEL_NO"));
 			data.put("CTRT_NO", "CN" + DateUtils.getCurrentDate("yyyymmddhhmmss") + makeRamdomNumber(10));
 			data.put("CONTRACT_TYPE_LIST", jArray);
 			
 			obj.put("data", data);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");

 			return write(obj.toString());

 		} catch (Exception e) {
 			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			obj.put("errmsg", mm.getMessage("COMG_1001"));
 			obj.put("errcode", "-1");
 			return write(obj.toString());
 		}
 	}   
 	
 	/**
 	 * 계약 관련 데이터를 가져온다.
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse getContractInfo(IRequest request) throws Exception {

 		JSONObject obj = new JSONObject();

 		try {

 			Map item = request.getMap();
 			
 			item.put("R_USER_ID", request.getUser().getId());
 			item.put("USER_ROLL", request.getUser().getProperty("USER_ROLL"));
 			
 			Map map = (Map)getItem("mg.vdr.selectCustCont" + getDBName(pm.getString("component.sql.database")), item);
 			
 			JSONObject data = new JSONObject();
 			
 			Iterator<String> keys = map.keySet().iterator();
 			
			while (keys.hasNext()) {
				String key = keys.next();
				data.put(key, this.getStringWithNullCheck(map, key));
			}
			
 			List lists = getList("mg.vdr.getContractTypeList" + getDBName(pm.getString("component.sql.database")), item);
 			
 			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map tmpMap = (Map) lists.get(i);

				Iterator<String> tmpKeys = tmpMap.keySet().iterator();

				JSONObject listData = new JSONObject();

				while (tmpKeys.hasNext()) {
					String key = tmpKeys.next();
					listData.put(key, this.getStringWithNullCheck(tmpMap, key));
				}

				jArray.add(listData);
			}
			
			data.put("CONTRACT_TYPE_LIST", jArray);
 			
			obj.put("data", data);
 			obj.put("errmsg", "success");
 			obj.put("errcode", "0");
 			return write(obj.toString());
 		} catch (Exception e) {
 			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			obj.put("errmsg", mm.getMessage("COMG_1001"));
 			obj.put("errcode", "-1");
 			return write(obj.toString());
 		}
 	} 	
     
     private String getSize(long size) {
         
    	 long n = 1024;
    	 
         String s = "";
         
         double kb = size / n;
         double mb = kb / n;
         double gb = mb / n;
         double tb = gb / n;
         
         if(size < n) {
        	 s = size + " B";
         }else if(size >= n && size < (n * n)) {
             s = String.format("%.2f", kb) + " KB";
         }else if(size >= (n * n) && size < (n * n * n)) {
             s = String.format("%.2f", mb) + " MB";
         }else if(size >= (n * n * n) && size < (n * n * n * n)) {
             s = String.format("%.2f", gb) + " GB";
         }else if(size >= (n * n * n * n)) {
             s = String.format("%.2f", tb) + " TB";
         }
         
         return s;
     }     
}
