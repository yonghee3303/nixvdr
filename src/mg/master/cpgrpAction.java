package mg.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class cpgrpAction extends BaseAction {
    /**
     * CP policy group code management
     * @param request
     * @return // getComboCpGrpHtml
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
               
        if(request.isMode("selectCpGrp")) {   /** Query for policy group         */
            return selectCpGrp(request);                           
        } else if(request.isMode("saveCpGrp")) {   /** Save policy group */
            return saveCpGrp(request);                           
        } else if(request.isMode("deleteCpGrp")) {   /** Save policy group */
            return deleteCpGrp(request);                           
        } else if(request.isMode("comboCpGrp")) {   /** Query for combo code   */
            return getComboCpGrp(request);                           
        } else if(request.isMode("listExportUnit")) {   /** Query for export way code   */
            return getListExportUnit(request);                           
        } else if(request.isMode("getComboCpGrpHtml")) {   /** select code code for CP  */
            return getComboCpGrpHtml(request);                           
        } else if(request.isMode("getComboCpGrpHtmlCpc")) {   /** select combo code for CP cloud  */
            return getComboCpGrpHtmlCpc(request);                           
        } else if(request.isMode("getSingleCpGrp")) {   /** Query for policy detail   */
            return getSingleCpGrp(request);                           
        } else if(request.isMode("getCpGrpList")){	/** Query for policy group list*/
        	return getCpGrpList(request);
        } else if(request.isMode("deleteCpGrp")){
        	return deleteCpGrp(request);                        
        } else if(request.isMode("getCpGrpListJson")){	/** Query for policy group list*/
        	return getCpGrpListJson(request);
        } else if(request.isMode("saveCpGrpJson")) {   /** Save policy group */
            return saveCpGrpJson(request);                           
        } else if(request.isMode("deleteCpGrpJson")) {   /** Save policy group */
            return deleteCpGrpJson(request);                           
        } 
        
        else {
            return write(null);
        }
    }
     
    
    private IResponse deleteCpGrp(IRequest request) {
    	String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	deleteObject("mg.master.deleteCpGrp", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";        
	     } catch (Exception e) {
        	tx.rollback();
        	log.error("------------deleteCpGrp----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	}


	private IResponse getCpGrpList(IRequest request) {
		// TODO Auto-generated method stub
    	String col = request.getStringParam("grid_col_id");
   	  String[] cols = col.split(",");
   	  
   	  Map map = null;
       GridData gdRes = new GridData();
   	
   	  try {
	         Map smap = request.getMap();
	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	       	 smap.put("dbName",pm.getString("component.sql.database"));
	       	 List lists = getList("mg.master.selectCpPolicyGrpList", smap);
	       	 
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
       } catch(Exception e) {
      	 log.error("---getCpGrpList : There is no data.-----------------------------------------------------------\n" + e.toString());
      	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
       }
	}
	
	private IResponse getCpGrpListJson(IRequest request) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		JSONArray jArray = new JSONArray();
		String tab = request.getStringParam("TAB_ID");
    	String col = request.getStringParam("grid_col_id");
   	  String[] cols = col.split(",");
   	  
   	  Map map = null;
       GridData gdRes = new GridData();
   	
   	  try {
   		  
	         Map smap = request.getMap();       
	         JSONObject cpGrpListData;
	        
	         
	         smap.put("POLICY_CL_CD", "1");  
	           
	         List lists;
	         
	         if(Boolean.parseBoolean(request.getStringParam("ADMIN"))) lists = getList("mg.master.selectCpPolicyGrpListAdmin" , smap); 
	       	 else lists = getList("mg.master.selectCpPolicyGrpList" , smap); 
	       	        	 
	         for (int i = 0; i < lists.size(); i++) {
		          	 map =(Map) lists.get(i);
		          	 cpGrpListData = new JSONObject();
		          	 cpGrpListData.put("ROW_ID", String.valueOf(i+1));
		          	 
		          	for(int j=0; j < cols.length; j++ ) cpGrpListData.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
		          	
		          	jArray.add(cpGrpListData);
	         }
	         
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			  log.info("------getCpGrpListJson-=----result------ > " + obj.toString());
			 return write(obj.toString());
	     } catch(Exception e) {
	    	 log.info("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1008", request.getUser().getLocale()));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
	}



	/**
    * Query for policy group
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectCpGrp(IRequest request) throws Exception 
    {
      	  String col = request.getStringParam("grid_col_id");
      	  String[] cols = col.split(",");
      	  
      	  Map map = null;
          GridData gdRes = new GridData();
          String dbName = pm.getString("component.sql.database");
          Map smap =request.getMap();
          smap.put("dbName", dbName);
      	  try {
      		  
      		List lists = getList("mg.master.selectDept",smap);
	       	log.info("-----------selectCpGrp---------------------");
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData("Success", "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	 log.error("---------------------------------------------------------------\n" + e.toString());
         	 return writeXml(responseSelData("There is no data.", "false", "doQuery", gdRes.getGridXmlDatas()));
          }
    } 
    
    /**
     * Policy way code
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectExportUnit(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
           String dbName = pm.getString("component.sql.database");
           Map smap =request.getMap();
           smap.put("dbName", dbName);
       	  try {
       		  
       		List lists = getList("mg.master.selectDept",smap);
 	       	
 	         for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	 
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	         }
 	         return writeXml(responseSelData("Success", "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 log.error("---------------------------------------------------------------\n" + e.toString());
          	 return writeXml(responseSelData("There is no data.","false", "doQuery", gdRes.getGridXmlDatas()));
           }
     } 
    
    
    /**
     * Save Group
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse saveCpGrp(IRequest request) throws Exception 
     {
    	 Map smap = request.getMap();
 		try{
 			tx.begin();
 			
 			smap.put("CHG_ID", request.getUser().getId());	
 			if("".equals(request.getStringParam("POLICY_NO"))){
 				Map map = (Map)getItem("mg.master.getNewPolicyNo",smap);
 				String count ="";
 				if("".equals(this.getStringWithNullCheck(map, "POLICY_NO"))){
 					count="1";
 				}else{
 					count =Integer.toString(Integer.parseInt(this.getStringWithNullCheck(map, "POLICY_NO").substring(1))+1); 
 				}
 				for(int i=0; i <4-count.length(); i++){
 					count = "0"+count;
 				}
 				smap.put("POLICY_NO", "P"+count);
 				createObject("mg.master.insertCpPolicyGrp",smap);
 			}else{
 				updateObject("mg.master.updateCpPolicyGrp", smap);
 			}
 	        tx.commit(); 
 	     	return write("saveCpGrp Success");
 		}catch(Exception e){
 			log.error("saveCpGrp-----------------------------------------------------------"+e.toString());
 			tx.rollback();
 			return write("saveCpGrp Failed");
 		}
     }

    /**
     * Query for combo code
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getComboCpGrp(IRequest request) throws Exception 
     {
         JSONObject obj = new JSONObject();
         
         try {
        	 List<JSONObject> jsonLists = new ArrayList<JSONObject>();
	         JSONObject jsonitem = null;
        	 
	         List lists = getList("mg.master.getComboCpGrp", request.getMap());
	         for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	jsonitem = new JSONObject();
 	          	jsonitem.put("CD",  this.getStringWithNullCheck(map, "CD"));
 	          	jsonitem.put("NM",  this.getStringWithNullCheck(map, "NM"));
 	          	jsonLists.add(jsonitem);
	         }
	         obj.put("list", jsonLists);   
 	   		 obj.put("errcode", "0");
 			 obj.put("errmsg", "CP GRP Code query success.");
 	   		 return write(obj.toString()); 
         } catch(Exception e) {
        	 log.error("---CPGRP : getComboCode.  Exception Error-------------------------\n" + e.toString());
     	 	 obj.put("errcode", "-101");
			 obj.put("errmsg", e.getMessage());
       	 	 return write(obj.toString());
         }
     }
     
     /**
      * Query for export unit code
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse getListExportUnit(IRequest request) throws Exception 
      {
          JSONObject obj = new JSONObject();
          
          try {
         	 List<JSONObject> jsonLists = new ArrayList<JSONObject>();
 	         JSONObject jsonitem = null;
         	 
 	         List lists = getList("mg.master.getListExportUnit", request.getMap());
 	         for (int i = 0; i < lists.size(); i++) {
 	          	Map map =(Map) lists.get(i);
 	          	
 	          	jsonitem = new JSONObject();
  	          	jsonitem.put("CD",  this.getStringWithNullCheck(map, "CD"));
  	          	jsonitem.put("NM",  this.getStringWithNullCheck(map, "NM"));
  	          	jsonLists.add(jsonitem);
 	         }
 	         obj.put("list", jsonLists);   
  	   		 obj.put("errcode", "0");
  			 obj.put("errmsg", "Export Unit Code query success.");
  	   		 return write(obj.toString()); 
          } catch(Exception e) {
         	 log.error("---CPGRP : getListExportUnit.  Exception Error-------------------------\n" + e.toString());
      	 	 obj.put("errcode", "-101");
 			 obj.put("errmsg", e.getMessage());
        	 	 return write(obj.toString());
          }
      }
     
     
     /**
      * Query for Combo code HTML (CP)
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse getComboCpGrpHtml(IRequest request) throws Exception 
      {           	  
    	  try {
    		    List lists = getList("mg.master.getComboCpGrp", request.getMap());
        	    StringBuffer buffer = new StringBuffer();
                
                buffer.append("<select name=\"policyCode\" onchange=\"cpGrpChange();\">");
                Map map = null;

                for (int i = 0; i < lists.size(); i++) {
                	map =(Map) lists.get(i);
                    buffer.append("<option value='" + map.get("CD") + "'>" + map.get("NM") + "</option>");
                    // log.debug( "-------------------------------------------------------------------\n" + map.toString());
                }
            
                buffer.append("</select>");
                return writeXml(buffer.toString());
    	  } catch(Exception ex) {
    		   log.error( "-------------------------------------------------------------------\n" + ex.getMessage());
    		   return writeXml("");
    	  }
     }
     

      private IResponse getComboCpGrpHtmlCpc(IRequest request) throws Exception 
      {           	  
    	  try {
    		    List lists = getList("mg.master.getComboCpGrp", request.getMap());
        	    StringBuffer buffer = new StringBuffer();
                
                Map map = null;

                for (int i = 0; i < lists.size(); i++) {
                	map =(Map) lists.get(i);
                    buffer.append("<option value='" + map.get("CD") + "'>" + map.get("NM") + "</option>");
                    // log.debug( "-------------------------------------------------------------------\n" + map.toString());
                }
            
                return writeXml(buffer.toString());
    	  } catch(Exception ex) {
    		   log.error( "-------------------------------------------------------------------\n" + ex.getMessage());
    		   return writeXml("");
    	  }
     }
     /**
      * Query for policy detail
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse getSingleCpGrp(IRequest request) throws Exception 
      {
          JSONObject obj = new JSONObject();
          
          try {
 	         Map map = (Map)getItem("mg.master.getSingleCpGrp",request.getMap());

          	 obj.put("POLICY_NM",  this.getStringWithNullCheck(map, "POLICY_NM"));
          	 obj.put("POLICY",  this.getStringWithNullCheck(map, "POLICY"));
          	 obj.put("POLICY_DOC",  this.getStringWithNullCheck(map, "POLICY_DOC"));
         	 obj.put("POLICY_DISP",  this.getStringWithNullCheck(map, "POLICY_DISP"));
         	 obj.put("EXPIRE_DAY",  this.getStringWithNullCheck(map, "EXPIRE_DAY"));
         	 obj.put("READ_COUNT",  this.getStringWithNullCheck(map, "READ_COUNT"));
         	 obj.put("WATERMARK_YN",  this.getStringWithNullCheck(map, "WATERMARK_YN"));
         	 obj.put("VDI_YN",  this.getStringWithNullCheck(map, "VDI_YN"));
         	 obj.put("OUTDEVICE",  this.getStringWithNullCheck(map, "OUTDEVICE"));
         	 obj.put("USEREDIT_CD",  this.getStringWithNullCheck(map, "USEREDIT_CD"));

  	   		 obj.put("errcode", "0");
  			 obj.put("errmsg", "success.");
  	   		 return write(obj.toString()); 
          } catch(Exception e) {
         	 log.error("-- getSingleCpGrp Exception Error-------------------------\n" + e.toString());
      	 	 obj.put("errcode", "-101");
 			 obj.put("errmsg",  e.getMessage());
        	 return write(obj.toString());
          }
      }

/**
 * Query for policy group
 * @param request
 * @return
 * @throws Exception
 */
 private IResponse selectCpGrpJson(IRequest request) throws Exception 
 {
   	  String col = request.getStringParam("grid_col_id");
   	  String[] cols = col.split(",");
   	  JSONObject obj = new JSONObject();
   	  JSONArray jArray = new JSONArray();
   	  Map map = null;
       GridData gdRes = new GridData();
       String dbName = pm.getString("component.sql.database");
       Map smap =request.getMap();
       smap.put("dbName", dbName);
   	  try {
   		 JSONObject listdata;
   		List lists = getList("mg.master.selectDept",smap);
	       	log.info("-----------selectCpGrp---------------------");
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	listdata = new JSONObject();
             	listdata.put("ROW_ID", String.valueOf(i+1));
	          	for(int j=0; j < cols.length; j++ ) {
 		             listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	        	jArray.add(listdata); 
	         }
	         log.info("------selectCpGrpJson-=----Locale------ > " + jArray.toString());        
  	    	 obj.put("data", jArray);
  			 obj.put("errmsg", "success");
  			 obj.put("errcode", "0");
  			 return write(obj.toString());
  	     } catch(Exception e) {
  	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
  	    	 obj.put("errmsg", e.toString());
  			 obj.put("errcode", "-1");
  	    	 return write(obj.toString());
  	     }
     } 
 
 /**
  * Save Group
  * @param request
  * @return
  * @throws Exception
  */
  private IResponse saveCpGrpJson(IRequest request) throws Exception 
  {
	   JSONObject obj = new JSONObject();
	   JSONArray jArray = new JSONArray();
	   String ids_info = request.getStringParam("ids");
       String cols_ids = request.getStringParam("col_ids");
       String[] cols = cols_ids.split(",");
       String[] ids = ids_info.split(",");
 	   Map smap = request.getMap();
 	   String count ="";
 	   String dbName = pm.getString("component.sql.database");
 	   int index = 0;
 	   ArrayList<String> langList = new ArrayList<String>();
       Map langitem = new HashMap();
 	   
		try{
			
			tx.begin();
			
			Map item = null;
			
			JSONObject listdata;
	  	     	
	  	    if(null == smap.get("POLICY_NO")) {
	  	    	Map map = (Map)getItem("mg.master.getNewPolicyNo_"+dbName,item);
				if(null == this.getStringWithNullCheck(map, "POLICY_NO"))  count="1";
				else  count = Integer.toString(Integer.parseInt(this.getStringWithNullCheck(map, "POLICY_NO").substring(1))+1); 
	  	    	
	  	    }
	
			for (int i = 0; i < ids.length; i++) {
				
				item = new HashMap();
				listdata = new JSONObject();
				listdata.put("ROW_ID", String.valueOf(i + 1));
	             					      
				for(int j=0; j < cols.length; j++){ 
					 
					 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);	
					 
		             item.put(cols[j], tmps);     
		             
		             listdata.put(cols[j], tmps);
				 }
				
				item.put("CHG_ID", (null != smap.get("CHG_ID"))? (String) smap.get("CHG_ID") : "iXVDR@inzent.com");
	
				if( Integer.parseInt(getItem("mg.master.cpPolicy", item).toString() ) > 0 ) {
					updateObject("mg.master.updateCpPolicyGrp_"+dbName, item);
				}else {
	       			item.put("POLICY_NO", "P0"+count);	
	       			createObject("mg.master.insertCpPolicyGrp_"+dbName, item);	
	       		}
		       	 	
				for(String lang : languageList) {
	       	 		  
			       	 item.put("POLICY_NM", item.get(lang));
			       	 item.put("LANG", lang);
		       	 		
			       	if( Integer.parseInt(getItem("mg.master.cpPolicyLang", item).toString() ) > 0 )   updateObject("mg.master.updateCpPolicyGrpLang_"+dbName, item);
		       		else 																			  createObject("mg.master.insertCpPolicyGrpLang_"+dbName, item);
		       	}	
			}
						
				
			log.info("------saveCpGrpJson-=----Locale------ > " + item.toString());
			
			tx.commit();
	    	obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			return write(obj.toString());
			 
		}catch(Exception e) {
			tx.rollback();
	    	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	    	obj.put("errmsg", e.toString());
			obj.put("errcode", "-1");
	    	return write(obj.toString());
	    }
  }


  private IResponse deleteCpGrpJson(IRequest request) {
	String ids_info = request.getStringParam("ids");
    String cols_ids = request.getStringParam("col_ids");
    String[] cols = cols_ids.split(",");
    String[] ids = ids_info.split(",");
    String msg="", flag="";
    JSONObject obj = new JSONObject();
 	JSONArray jArray = new JSONArray();
 	String dbName = pm.getString("component.sql.database");
 	
    try {
    	
    	tx.begin();
        Map item = null;
        
        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 JSONObject listdata = new JSONObject();
		       	 listdata.put("ROW_ID", String.valueOf(i+1));
		       	 
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
			            listdata.put(cols[j], tmps);     
		         	} 
	       	 	
	       	 	deleteObject("mg.master.deleteCpGrp_"+dbName, item); 
	       	 	deleteObject("mg.master.deleteCpGrpLang_"+dbName, item); 
        }
        
        log.info("------deleteCpGrpJson-=----Locale------ > " + item.toString());        
        
         tx.commit();
   	     obj.put("data", jArray);
		 obj.put("errmsg", "success");
		 obj.put("errcode", "0");
		 return write(obj.toString());
     } catch (Exception e) {
    	 tx.rollback();
    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
    	 obj.put("errmsg", e.toString());
		 obj.put("errcode", "-1");
    	 return write(obj.toString());
    }
   }
}