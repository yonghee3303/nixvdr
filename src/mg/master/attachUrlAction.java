package mg.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class attachUrlAction extends BaseAction {
    /**
     * CP 정책그룹코드 관리 
     * @param request
     * @return // getComboCpGrpHtml
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
               
        if(request.isMode("getCpAttachUrl")) {   /** CP 첨부가능 URL조회         */
            return getCpUrlList(request);                           
        } else if(request.isMode("saveCpUrl")) {   /** CP 첨부가능 URL 저장  */
            return saveCpUrl(request);                           
        }  else if(request.isMode("deleteCpUrl")){ /** CP 첨부가능 URL 삭제  */
        	return deleteCpUrl(request);                        
        } else {
            return write(null);
        }
    }

	private IResponse deleteCpUrl(IRequest request) {
		// TODO Auto-generated method stub
			String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	         
	        try {
	        	tx.begin();
		        Map item = null;
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
		         	} 
		       	 	deleteObject("mg.master.deleteCpUrl", item); 
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
	        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
		        flag = "false";
	        }
	                
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	}

	private IResponse saveCpUrl(IRequest request) {
		String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 for(int j=0; j < cols.length; j++ ) {
	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		        item.put(cols[j], tmps);     
	       	 }
			 if( Integer.parseInt(getItem("mg.master.countCpUrl", item).toString() ) > 0 ) {
			   	updateObject("mg.master.updateCpUrl", item);
			 } else {
			  	createObject("mg.master.insertCpUrl", item); 
			 }
	    }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveCpUrl----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	}

	private IResponse getCpUrlList(IRequest request) {
		String col = request.getStringParam("grid_col_id");
    	  String[] cols = col.split(",");
    	  
    	Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        Map smap =request.getMap();
        smap.put("dbName", dbName);
    	  try {
    		 List lists = getList("mg.master.getCpUrlList",smap);
	       	
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 log.error("---------------------------------------------------------------\n" + e.toString());
       	 return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}
    
}
