package mg.master;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

/**
* 업무 그룹명 : mg.system
* 서브 업무명 : codeAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 17 
* 설 명 : ComCodeAction
*/ 
public class deptAction extends BaseAction {
    /**
     * 조직코드 관리 
     * @param request
     * @return 
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
               
        if(request.isMode("selectDept")) {   /** 조직조회         */
            return selectDept(request);                           
        } else if(request.isMode("save")) { 
            return saveDept(request);                           
        } else if(request.isMode("deptPop")) {   /** 팝업조회         */
            return getPopupList(request);                           
        } else {
            return write(null);
        }
    }
     
    
    /**
    * 코드 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectDept(IRequest request) throws Exception 
    {
      	  String col = request.getStringParam("grid_col_id");
      	  String[] cols = col.split(",");
      	  
      	  Map map = null;
          GridData gdRes = new GridData();
          String dbName = pm.getString("component.sql.database");
          Map smap =request.getMap();
          smap.put("dbName", dbName);
      	  try {
      		  
      		List lists = getList("mg.master.selectDept",smap);
	       	
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	 log.error("---------------------------------------------------------------\n" + e.toString());
         	 return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
          }
    } 
    
    /**
     * 저장 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse saveDept(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         
         try {
	         Map item = null;
	         for (int i = 0; i < ids.length; i++) {
	        	 item = new HashMap();
	        	 
	        	 for(int j=0; j < cols.length; j++ ) {
	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	          	}
	        	item.put("LOGIN_ID",  request.getUser().getId());
	       	         	 
	        	if( Integer.parseInt(getItem("mg.master.countDept", item).toString() ) > 0 ) {
	        		updateObject("mg.master.updateDept", item);
	        	} else {
	        		createObject("mg.master.insertDept", item); 
	        	}
	         }
	         return writeXml(responseTranData("저장 완료 되었습니다.", "true", "doSave", "doSaveEnd"));
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseTranData("저장중 오류로 작업 취소 되었습니다.\n관리자에게 문의 바랍니다..","false", "doSave", "doSaveEnd"));
         }
     }

    /**
     * 코드 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getPopupList(IRequest request) throws Exception 
     {
      	 String col = request.getStringParam("grid_col_id");
      	 String roll = request.getStringParam("roll");
      	 String type = request.getStringParam("type");
      	 String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();
         
         try {
        	 Map smap =  request.getMap();
        	 /*
        	 if("S03".equals(roll) && "plant".equals(type)) {
        		 smap.put("AUTH_DEPT_CD", request.getUser().getProperty("P_DEPT_CD"));
        		 
        	 } 
        	 */
        	 // log.error("-----------AUTH_DEPT_CD-----------------------------------\n" + smap);
        	 
	         List lists = getList("mg.master.selectPop",smap);
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
         }
     }
    
}