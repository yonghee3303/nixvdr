package mg.master;

import java.util.List;
import java.util.Map;

import mg.base.BaseAction;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class sugrpAction extends BaseAction {

	/**
	 * CP 정책그룹코드 관리
	 * 
	 * @param request
	 * @return // getComboSuGrpHtml
	 * @throws Exception
	 */
	@Override
	public IResponse run(IRequest request) throws Exception {

		if (request.isMode("getComboBasicPolicyGrpHtml")) { // 콤보코드(기본정책) 조회
			return getComboBasicPolicyGrpHtml(request);
		} else if (request.isMode("getComboPolicyGrpHtml")) { // 콤보코드(정책그룹) 조회
			return getComboPolicyGrpHtml(request);
		} else if (request.isMode("getBasicPolicy")) { // 기본정책 상세 조회
			return getBasicPolicy(request);
		}
		//
		else {
			return write(null);
		}

		/* if(request.isMode("getComboSuGrpHtml")) { *//** 콤보코드HTML Select박스 */
		/*
		 * return getComboSuGrpHtml(request); }else
		 * if(request.isMode("getPolicySuGrp")){
		 *//** 정책 코드 상세 조회 */
		/*
		 * return getPolicySuGrp(request); } else { return write(null); }
		 */
	}

	/**
	 * 콤보코드 HTML 조회 (기본정책)
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getComboBasicPolicyGrpHtml(IRequest request)
			throws Exception {

		try {
			Map item = request.getMap();
			List lists = getList("mg.master.getComboBasicPolicyGrpHtml", item);
			StringBuffer buffer = new StringBuffer();

			Map map = null;

			// buffer.append("<option value='none'>"+ mm.getMessage("CPIS_0025",
			// request.getUser().getLocale()) +"</option>");
			buffer.append("<option value=''></option>");
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);
				buffer.append("<option value='" + map.get("SU_POLICY_BASIC_CD")
						+ "'>" + map.get("SU_POLICY_BASIC_CD") + "</option>");

				// log.debug("-------------------------------------------------------------------\n"
				// + map.toString());
			}
			// log.info("BasicPolicyInfo : " + buffer.toString());
			return write(buffer.toString());
		} catch (Exception ex) {
			log.error("-------------------------------------------------------------------\n"
					+ ex.getMessage());
			return write("");
		}
	}

	/**
	 * 콤보코드 HTML 조회 (정책그룹)
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getComboPolicyGrpHtml(IRequest request) throws Exception {

		try {
			Map item = request.getMap();
			item.put("LOGIN_ID", request.getUser().getId());
			List lists = getList("mg.master.getComboPolicyGrpHtml", item);
			StringBuffer buffer = new StringBuffer();

			Map map = null;

			// buffer.append("<option value='none'>"+ mm.getMessage("CPIS_0025",
			// request.getUser().getLocale()) +"</option>");
			buffer.append("<option value=''></option>");
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);
				if ("Y".equals(map.get("DEF_YN"))) {
					buffer.append("<option selected value='"
							+ map.get("SU_POLICY_GRP_CD") + "'>"
							+ map.get("SU_POLICY_GRP_NM") + "</option>");
				} else {
					buffer.append("<option value='"
							+ map.get("SU_POLICY_GRP_CD") + "'>"
							+ map.get("SU_POLICY_GRP_NM") + "</option>");
				}
				// log.debug("-------------------------------------------------------------------\n"
				// + map.toString());
			}
			// log.info("BasicPolicyInfo : " + buffer.toString());
			return write(buffer.toString());
		} catch (Exception ex) {
			log.error("-------------------------------------------------------------------\n"
					+ ex.getMessage());
			return write("");
		}
	}

	/**
	 * 기본정책 상세 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getBasicPolicy(IRequest request) {
  		try {
  	       	Map map = request.getMap();
  	       	map.put("dbName", pm.getString("component.sql.database"));
  	       	
  	       	Map item  = (Map)getItem("mg.master.getBasicPolicy", map);
  	       	// Creation of Json Response 
  	       	JSONObject obj = new JSONObject();
  	       	
  	       	//기본정책 정보
  	       	obj.put("SU_POLICY_BASIC_CD", this.getStringWithNullCheck(item, "SU_POLICY_BASIC_CD"));
  	       	obj.put("IS_EXPIRE_DT", this.getStringWithNullCheck(item, "IS_EXPIRE_DT"));
  	       	obj.put("IS_EXPIRE_INIT", this.getStringWithNullCheck(item, "IS_EXPIRE_INIT"));
  	       	obj.put("IS_READCOUNT", this.getStringWithNullCheck(item, "IS_READCOUNT"));
  	       	obj.put("IS_READ_INIT", this.getStringWithNullCheck(item, "IS_READ_INIT"));
  	       	obj.put("IS_WAIT_CLOSE", this.getStringWithNullCheck(item, "IS_WAIT_CLOSE"));
  	       	obj.put("WAIT_TIME", this.getStringWithNullCheck(item, "WAIT_TIME"));
  	       	obj.put("IS_LOGIN_FAIL_INIT", this.getStringWithNullCheck(item, "IS_LOGIN_FAIL_INIT"));
  	       	obj.put("IS_LOGIN_FAIL_COUNT", this.getStringWithNullCheck(item, "IS_LOGIN_FAIL_COUNT"));
  	       	obj.put("USB_ENCODE_CL_CD", this.getStringWithNullCheck(item, "USB_ENCODE_CL_CD"));
  	       	obj.put("USB_IMAGE_SIZE", this.getStringWithNullCheck(item, "USB_IMAGE_SIZE"));
  	       	obj.put("EXPIRE_YMD", this.getStringWithNullCheck(item, "EXPIRE_YMD"));
  	       	obj.put("READCOUNT", this.getStringWithNullCheck(item, "READCOUNT"));
  	       	obj.put("RUN_MAC_YN", this.getStringWithNullCheck(item, "RUN_MAC_YN"));
  	       	obj.put("USER_ATUH_YN", this.getStringWithNullCheck(item, "USER_ATUH_YN"));
  	       	obj.put("IS_EXPIRE_STOP", this.getStringWithNullCheck(item, "IS_EXPIRE_STOP"));
  	       	obj.put("IS_LOGIN_FAIL_STOP", this.getStringWithNullCheck(item, "IS_LOGIN_FAIL_STOP"));
  	       	obj.put("IS_READ_STOP", this.getStringWithNullCheck(item, "IS_READ_STOP"));
  	       	
  	       	//log.info("--------------------------------------"+obj.toString());
  	       	return write(obj.toString());
          } catch(Exception e) {
          	log.error("---getPolicyGrp-------------------------\n" + e.toString());
        	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
          }
  	}

	/**
	 * 콤보코드 HTML 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	/*
	 * private IResponse getComboSuGrpHtml(IRequest request) throws Exception {
	 * try { List lists = getList("mg.master.getComboSuGrp", request.getMap());
	 * StringBuffer buffer = new StringBuffer();
	 * 
	 * buffer.append("<select name=\"policyCode\" onchange=\"suGrpChange();\">");
	 * Map map = null;
	 * 
	 * for (int i = 0; i < lists.size(); i++) { map =(Map) lists.get(i);
	 * buffer.append("<option value='" + map.get("CD") + "'>" + map.get("NM") +
	 * "</option>"); }
	 * 
	 * buffer.append("</select>"); return writeXml(buffer.toString()); }
	 * catch(Exception ex) { log.error(
	 * "-------------------------------------------------------------------\n" +
	 * ex.getMessage()); return writeXml(""); } }
	 */
	/**
	 * 콤보코드 상세 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	/*
	 * private IResponse getPolicySuGrp(IRequest request) { // TODO
	 * Auto-generated method stub try { Map map = request.getMap();
	 * map.put("dbName", pm.getString("component.sql.database"));
	 * map.put("ReturnDay",
	 * pm.getString("component.usb.export.default.returnDay"));
	 * 
	 * Map item = (Map)getItem("mg.master.getPolicySuGrp", map); // Creation of
	 * Json Response JSONObject obj = new JSONObject();
	 * 
	 * obj.put("USB_AUTH_CL_CD", this.getStringWithNullCheck(item,
	 * "USB_AUTH_CL_CD")); obj.put("NO_SAVE", this.getStringWithNullCheck(item,
	 * "NO_SAVE")); obj.put("NO_COPY", this.getStringWithNullCheck(item,
	 * "NO_COPY")); obj.put("NO_PRINT", this.getStringWithNullCheck(item,
	 * "NO_PRINT")); obj.put("IS_EXPIRE_DT", this.getStringWithNullCheck(item,
	 * "IS_EXPIRE_DT")); obj.put("IS_EXPIRE_INIT",
	 * this.getStringWithNullCheck(item, "IS_EXPIRE_INIT"));
	 * obj.put("IS_READCOUNT", this.getStringWithNullCheck(item,
	 * "IS_READCOUNT")); obj.put("IS_READ_INIT",
	 * this.getStringWithNullCheck(item, "IS_READ_INIT"));
	 * obj.put("IS_WAIT_CLOSE", this.getStringWithNullCheck(item,
	 * "IS_WAIT_CLOSE")); obj.put("WAIT_TIME", this.getStringWithNullCheck(item,
	 * "WAIT_TIME")); obj.put("IS_LOGIN_FAIL_INIT",
	 * this.getStringWithNullCheck(item, "IS_LOGIN_FAIL_INIT"));
	 * obj.put("IS_LOGIN_FAIL_COUNT", this.getStringWithNullCheck(item,
	 * "IS_LOGIN_FAIL_COUNT")); obj.put("USB_ENCODE_CL_CD",
	 * this.getStringWithNullCheck(item, "USB_ENCODE_CL_CD"));
	 * obj.put("USB_IMAGE_SIZE", this.getStringWithNullCheck(item,
	 * "USB_IMAGE_SIZE")); obj.put("EXPIRE_YMD",
	 * this.getStringWithNullCheck(item, "EXPIRE_YMD")); obj.put("READCOUNT",
	 * this.getStringWithNullCheck(item, "READCOUNT")); obj.put("USER_AUTH_YN",
	 * this.getStringWithNullCheck(item, "USER_AUTH_YN")); obj.put("RUN_MAC_YN",
	 * this.getStringWithNullCheck(item, "RUN_MAC_YN")); obj.put("RETURN_YMD",
	 * this.getStringWithNullCheck(item, "RETURN_YMD"));
	 * log.info("--------------------------------------"+obj.toString()); return
	 * write(obj.toString()); } catch(Exception e) {
	 * log.error("---getPolicyGrp-------------------------\n" + e.toString());
	 * return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
	 * } }
	 */

}