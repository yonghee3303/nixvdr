package mg.site;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.lgchem.esb.exception.ESBValidationException;
import com.lgchem.esb.exception.ESBTransferException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;
import com.lgchem.esb.adapter.ESBAdapter;
import com.lgchem.esb.adapter.LGChemESBService;

public class LgChemInterlock {

		private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
		private Log log = LogFactory.getLog(this.getClass());
		private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	
			 /**
		     * LG화학 통합결재시스템 연동
		     * 결재 기안 처리
		     **/
		  public String ESB_R(String doc_no, String doc_nm, String user_id, String approval_id) throws Exception{
		 	 log.info("결제 기안 처리 -------doc_no:"+doc_no+"  doc_nm:"+doc_nm+"  user_id:"+user_id+"  approval_id:"+approval_id);
		 	String msg="";
		 	 try{
				 	 ESBAdapter esbAp = new LGChemESBService("APPINT_ESB", "dev");
				 	 Hashtable appParam = new Hashtable();
				 	 String app_id = doc_no.substring(10, doc_no.length());
				 	 log.info("---------------------------------------------결재연동 문서번호 총 10자리, 앞에 년도월일시간를 뜻하는 10자리 삭제 :"+app_id);
				 	 appParam.put("APP_ID", app_id); //각 레거시의 고유 결재 문서
				 	 appParam.put("CATEGORY", "보안문서반출"); // 양식명
				 	 appParam.put("SUBJECT", doc_nm);  // 양식 제목
				 	 appParam.put("REQ_DATE", doc_no.substring(0,12)); //요청일
				 	 appParam.put("MAIN_STATUS", "R"); //결재 main 상태
				 	 appParam.put("SUB_STATUS", "신청"); //결재 sub 상태
				 	 appParam.put("URL", pm.getString("component.site.url")+"/Login_hidden.jsp?docNo="+doc_no); //레거시 결재정보 상세로 이동할 수 있는 FULL URL http://
				 	 appParam.put("R_EMP_NO", user_id);//기안자 사번
				 	 appParam.put("A_EMP_NO", approval_id);//다음 결재자 사번
				 	 
		   	 		msg = esbAp.callESB(appParam); // 0000:성공 9999:실패
		 	 log.info("----------------------------------------결재 기안 처리 :"+msg);
			 }catch(ESBValidationException eV){
			  		log.error("자동전자결재 기안처리 중 정합성 에러--------------------------------------------"+eV.toString());
						msg = "9999";
				
			  }catch(ESBTransferException eF){
			  		log.error("자동전자결재 기안처리 중 시스템 에러--------------------------------------------"+eF.toString());
			  			msg = "9999";
			  }catch(Exception e){
				  	log.error("결재 기안처리 처리 중 에러 --------------------------------------------------------------"+e.toString());
					msg = "9999";
			  }
		 	 return msg;
			}
		  /**
		   * LG화학 통합결재시스템 연동
		   * 결재 진행중 처리
		   **/
		public String ESB_M(String doc_no, String user_id) throws Exception{
			 log.info("결제 진행중 처리 -------doc_no:"+doc_no+"   user_id:"+user_id);
			 String msg="";
			 
			 try{
					 ESBAdapter esbAp = new LGChemESBService("APPINT_ESB", "dev");
					 Hashtable appParam = new Hashtable();
					 String app_id = doc_no.substring(10, doc_no.length());
					 log.info("---------------------------------------------결재연동 문서번호 총 10자리, 앞에 년도월일시간를 뜻하는 10자리 삭제 :"+app_id);
					 appParam.put("APP_ID", app_id); //각 레거시의 고유 결재 문서
					 appParam.put("MAIN_STATUS", "M"); //결재 main 상태
					 appParam.put("SUB_STATUS", "승인"); //결재 sub 상태
					 DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
					 String reqDate = formatter.format(new Date());
					 appParam.put("REQ_DATE",reqDate); //요청일
					 appParam.put("URL", pm.getString("component.site.url")+"/Login_hidden.jsp?docNo="+doc_no); //레거시 결재정보 상세로 이동할 수 있는 FULL URL http://
					 appParam.put("R_EMP_NO", user_id);//승인자 사번
					 //appParam.put("A_EMP_NO", approval_id);//다음 결재자 사번
					 
					 msg = esbAp.callESB(appParam); // 0000:성공 9999:실패
			 log.info("----------------------------------------결재 진행중 처리 :"+msg);
			 }catch(ESBValidationException eV){
			  		log.error("자동전자결재 진행중 처리 중 정합성 에러--------------------------------------------"+eV.toString());
						msg = "9999";
				
			  }catch(ESBTransferException eF){
			  		log.error("자동전자결재 진행중 처리 중 시스템 에러--------------------------------------------"+eF.toString());
			  			msg = "9999";
			  }catch(Exception e){
				  	log.error("결재 진행중 처리 중 에러 --------------------------------------------------------------"+e.toString());
					msg = "9999";
			  }
			 return msg;
		
			}
		/**
		 * LG화학 통합결재시스템 연동
		 * 결재 완료 처리
		 **/
		public String ESB_F(String doc_no, String user_id) throws Exception{
			   log.info("결제 완료 처리 -------doc_no:"+doc_no+"   user_id:"+user_id);
			   String msg="";
			 try{
					 ESBAdapter esbAp = new LGChemESBService("APPINT_ESB", "dev");
					 Hashtable appParam = new Hashtable();
					 String app_id = doc_no.substring(10, doc_no.length());
					 log.info("---------------------------------------------결재연동 문서번호 총 10자리, 앞에 년도월잉시간를 뜻하는 10자리 삭제 :"+app_id);
					 appParam.put("APP_ID", app_id); //각 레거시의 고유 결재 문서
					 appParam.put("MAIN_STATUS", "F"); //결재 main 상태
					 appParam.put("SUB_STATUS", "승인"); //결재 sub 상태
					 DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
					 String reqDate = formatter.format(new Date());
					 appParam.put("REQ_DATE",reqDate); //요청일
					 appParam.put("URL", pm.getString("component.site.url")+"/Login_hidden.jsp?docNo="+doc_no); //레거시 결재정보 상세로 이동할 수 있는 FULL URL http://
					 appParam.put("R_EMP_NO", user_id);//승인자 사번
					 //appParam.put("A_EMP_NO", approval_id);//다음 결재자 사번
					 
					 msg = esbAp.callESB(appParam); // 0000:성공 9999:실패
					 log.info("----------------------------------------결재 완료 처리 :"+msg);
					
					 }catch(ESBValidationException eV){
					  		log.error("자동전자결재 완료처리 중 정합성 에러--------------------------------------------"+eV.toString());
								msg = "9999";
						
					  }catch(ESBTransferException eF){
					  		log.error("자동전자결재 완료처리 중 시스템 에러--------------------------------------------"+eF.toString());
					  			msg = "9999";
					  }catch(Exception e){
						  	log.error("결재 완료 처리 중 에러 --------------------------------------------------------------"+e.toString());
							msg = "9999";
					  }
			 return msg;
			}
			/**
		* LG화학 통합결재시스템 연동
		* 결재 반려 처리
		**/
		public String ESB_C(String doc_no, String user_id) throws Exception{
			 log.info("결제 반려 처리 ------- doc_no:"+doc_no+"   user_id:"+user_id);
			 String msg="";
			 try{
					 ESBAdapter esbAp = new LGChemESBService("APPINT_ESB", "dev");
					 Hashtable appParam = new Hashtable();
					 String app_id = doc_no.substring(10, doc_no.length());
					 log.info("---------------------------------------------결재연동 문서번호 총 10자리, 앞에 년도월일시간를 뜻하는 10자리 삭제 :"+app_id);
					 appParam.put("APP_ID", app_id); //각 레거시의 고유 결재 문서
					 appParam.put("MAIN_STATUS", "C"); //결재 main 상태
					 appParam.put("SUB_STATUS", "반려"); //결재 sub 상태
					 DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
					 String reqDate = formatter.format(new Date());
					 appParam.put("REQ_DATE",reqDate); //요청일
					 appParam.put("URL", pm.getString("component.site.url")+"/Login_hidden.jsp?docNo="+doc_no); //레거시 결재정보 상세로 이동할 수 있는 FULL URL http://
					 appParam.put("R_EMP_NO", user_id);//승인자 사번
					 //appParam.put("A_EMP_NO", approval_id);//다음 결재자 사번
					 
					 msg = esbAp.callESB(appParam); // 0000:성공 9999:실패
			 log.info("----------------------------------------결재 반려 처리 :"+msg);
			 }catch(ESBValidationException eV){
			  		log.error("자동전자결재 반려처리 중 정합성 에러--------------------------------------------"+eV.toString());
						msg = "9999";
				
			  }catch(ESBTransferException eF){
			  		log.error("자동전자결재 반려처리 중 시스템 에러--------------------------------------------"+eF.toString());
			  			msg = "9999";
			  }catch(Exception e){
				  	log.error("결재 반려 처리 중 에러 --------------------------------------------------------------"+e.toString());
					msg = "9999";
			  }
			 return msg;
		}

}
