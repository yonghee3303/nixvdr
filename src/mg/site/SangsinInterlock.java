package mg.site;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.ws.Holder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import util.GuidUtil;

import com.core.base.ComponentRegistry;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.message.IMessageManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;
import com.core.framework.core.IRequest;
/*
import com.sds.acube.app.ws.service.ApproverVO;
import com.sds.acube.app.ws.service.HeaderVO;
import com.sds.acube.app.ws.service.ILegacyService;
import com.sds.acube.app.ws.service.LegacyVO;
import com.sds.acube.app.ws.service.ReceiverVO;
import com.sds.acube.app.ws.service.ReserveVO;
import com.sds.acube.app.ws.service.ResultVO;
import com.sds.acube.app.ws.service.SenderVO;
*/
import com.uniportal.common.ws.domain.AppFileVO;
import com.uniportal.common.ws.domain.ApproverVO;
import com.uniportal.common.ws.domain.HeaderVO;
import com.uniportal.common.ws.service.LegacyService;
import com.uniportal.common.ws.domain.LegacyVO;
import com.uniportal.common.ws.domain.ReceiverVO;
import com.uniportal.common.ws.domain.ReserveVO;
import com.uniportal.common.ws.domain.ResultVO;
import com.uniportal.common.ws.domain.SenderVO;

import javax.xml.ws.WebServiceException;



public class SangsinInterlock {
	
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
    public String readFile(String filename)
	{   	
    	String content = null;
 	   File file = new File(getClass().getClassLoader().getResource(filename).getFile()); //for ex foo.txt
 	   try {
 	       //FileReader reader = new FileReader(file); //한글깨짐
 		   InputStreamReader reader= new InputStreamReader(new FileInputStream(file),"UTF-8"); //한글깨지지않음
 		   char[] chars = new char[(int) file.length()];
 	       reader.read(chars);
 	       content = new String(chars);
 	       reader.close();
 	       //log.info("------------------------------readFile");
 	   } catch (IOException e) {
 		   log.error("-----------------------------readFile Exception-----"+e.toString());
 	       e.printStackTrace();
 	   }
 	   return content;
	}
	
	private String getStringWithNullCheck(Map map, String key) 
	 {
	        Object result = map.get(key);
	        return (result == null) ? null : result.toString();
	  }
	
	//상신브레이크 결재 연동 - 기안처리
    public String saveRequest_SB(IRequest request){
	   String strReturn = "1";  //리턴 코드 : 성공:1, 실패 0
	   try{
    	LegacyVO legacyVO = new LegacyVO();
   		
    	String rqID = request.getUser().getId().toString();
    	String rqNm = request.getUser().getProperty("USER_NM").toString();
    	String rqDptCd = request.getUser().getProperty("DEPT_CD").toString();
    	String rqDptNm = request.getUser().getProperty("DEPT_NM").toString();
    	String rqFunction = request.getUser().getProperty("FUNCTION_CD").toString();
    	String rqSelfExp = request.getUser().getProperty("SELF_EXP_YN").toString();
    	String docNo = request.getStringParam("DOC_NO");
    	
    	int limitFileCount = pm.getInt("component.interlock.attachFile.count"); //결재연동 첨부파일 제한 갯수
		int limitFileSize = pm.getInt("component.interlock.attachFile.size")*1024*1024; //결재연동 첨부파일 제한 사이즈
		
		Map attachFileInfo =(Map)sm.getItem("mg.cp.attachFileGroupInfo",request.getMap()); //DOC_ID, MIN(FILE_NAME) AS MIN_FILE_NAME,TOTAL_FILE_SIZE, MIN_FILE_SIZE 
		log.info("결재연동 기안처리(WEB)");
		int totalFileSize = Integer.parseInt(attachFileInfo.get("TOTAL_FILE_SIZE").toString()); // 해당 문서에 관련된 파일의 총 용량
		//int minFileSize =  Integer.parseInt(attachFileInfo.get("MIN_FILE_SIZE").toString()); //해당 문서에 관련된 파일 중 가장 작은 용량
		//String minFileName = attachFileInfo.get("MIN_FILE_NAME").toString();//해당 문서에 관련된 파일 중 가장 작은 용량을 갖은 파일명
    	
   		//신청자 정보 추가
   		SenderVO senderVO = new SenderVO();
		senderVO.setOrgCode("C000");		//회사코드
		senderVO.setOrgName("상신브레이크");	//회사명
		senderVO.setDeptCode(rqDptCd);		//부서코드
		senderVO.setDeptName(rqDptNm);		//부서명
		senderVO.setPosition(rqFunction);			//직위코드
		senderVO.setUserId(rqID);		//기안자 ID
		//log.info("상신브레이크 결재 연동 - 기안처리--------------------------------------------------------------기안자ID"+rqID);
		senderVO.setUserName(rqNm);	//기안자 이름
		legacyVO.setSender(senderVO);
		
		ReceiverVO receiverVO = new ReceiverVO();
		receiverVO.setOrgCode("C000");
		receiverVO.setOrgName("상신브레이크");
		receiverVO.setDeptCode(rqDptCd);
		receiverVO.setDeptName(rqDptNm);
		receiverVO.setPosition(rqFunction);
		receiverVO.setUserId(rqID);
		receiverVO.setUserName(rqNm);
		legacyVO.setReceiver(receiverVO);
   		
		//2. 처리결과(임의값 지정) 
		ResultVO resultVO = new ResultVO(); //무슨 내용을 넣어야하는 지 확인 필요 
		resultVO.setMessageCode(docNo);  			  //CP_DOCNO
		//log.info("setMessageCode--------------------------------------------------------"+docNo);
		resultVO.setMessageText("DOC_NO");	  	  //처리 메시지 내용 (필요시 사용)
		//resultVO.setMessageDate("");					  //처리일자		 (필요시 사용)
		legacyVO.setResult(resultVO);		
		
		String approverId="";
		ArrayList<String> approver = new ArrayList<String>();
		if("personal".equals(pm.getString("component.export.approver"))) {
			Map approverMap = new HashMap();
 			approverMap.put("USER_ID",  rqID);
	       	List lists = sm.getList("mg.cp.getApproverId",approverMap); //다중결재선때문에 수정, 하지만 우선수위를 정하는 컬럼이 필요할 듯.
	       	for (int i = 0; i < lists.size(); i++) {
	        	 approverMap =(Map) lists.get(i); 
	        	 approver.add(this.getStringWithNullCheck(approverMap, "APPROVER_ID")); 
	         }
	         for(int j =0; j<lists.size(); j++){
	        	 if(approver.get(j).equals(rqID)){
	        		 String temp = approver.get(0);
	        		 approver.set(0, approver.get(j));
	        		 approver.set(j, temp);
	        	 }
	         }
	         approverId=approver.get(approver.size()-1);
		}
		//log.info("승인자 또는 검토자 ID ---------------------------------------------"+approverId);
		
		String dbName = pm.getString("component.sql.database");
		
		
		Map map;
		if("mssql".equals(dbName)){
			map = (Map)sm.getItem("app.user.getUserByProperties", approverId);
		}else{
			map = (Map)sm.getItem("app.user.getUserByProperties_" + dbName, approverId);
			
		}
		
		
		//LegacyVO.Approvers appList = new LegacyVO.Approvers(); 

		ApproverVO approverVO1 = new ApproverVO(); // 기안자
		approverVO1.setSerialOrder("1");					
		approverVO1.setParallelOrder("1");				//결재순서로 기본 1, 이중결재일 경우 신청부서 1, 주관부서 2 
		approverVO1.setUserName(rqNm);				//결재자이름
		approverVO1.setUserId(rqID);
		approverVO1.setPosition(rqFunction);						//결재자 직위코드
		approverVO1.setDeptName(rqDptNm);				//결재자부서명
		approverVO1.setDeptCode(rqDptCd);				//결재자부서코드
		
		if(!"Y".equals(rqSelfExp)) approverVO1.setAskType("ART010");				//결재자 요청타입(ART010:기안, ART020:검토, ART030:협조, ART130:합의, ART040:감사, ART050:결재, ART053:1인전결)
		else approverVO1.setAskType("ART053");				//ART053: 1인전결
		
		approverVO1.setActType("APT001");				//결재자 처리타입(APT001:승인, APT002:반려, APT003:대기, APT004:보류, APT051:찬성, APT052:반대)
		
		String toDate = docNo.substring(0,4)+"-"+docNo.substring(4,6)+"-"+docNo.substring(6,8)+" "+docNo.substring(8,10)+":"+docNo.substring(10, 12)+":"+docNo.substring(12, 14);
		approverVO1.setActDate(toDate);	//결재일자
		//log.info("결재일자------------------------------------------------------------------------------"+toDate);
		//approverVO1.setOpinion(request.getStringParam("DOC_MSG"));						//결재의견 --- 반출사유
		approverVO1.setOpinion("");
		legacyVO.getApprovers().add(approverVO1);
		
		if(approver == null || approver.size()==0){
			log.info("해당 사용자("+rqID+")의 결재선에 승인자가 없습니다. 자가승인 가능한 사람이 아닐 경우 기안시 오류가 발생합니다. SYSUSERAPPROVER을 확인하세요.");
		}else if(!rqID.equals(approverId)){
			//log.info("승인자 또는 첨부자 추가 ----------------"+approverId);
			ApproverVO approverVO = new ApproverVO(); // 결재자 
			approverVO.setSerialOrder("2");					//결재자 수만큼 반복
			approverVO.setParallelOrder("1");				//결재순서로 기본 1, 이중결재일 경우 신청부서 1, 주관부서 2 
			approverVO.setUserName(this.getStringWithNullCheck(map,"USER_NM"));				//결재자이름
			approverVO.setUserId(approverId);
			approverVO.setPosition(this.getStringWithNullCheck(map,"FUNCTION_CD"));						//결재자 직위코드
			approverVO.setDeptName(this.getStringWithNullCheck(map,"DEPT_NM"));				//결재자부서명
			approverVO.setDeptCode(this.getStringWithNullCheck(map,"DEPT_CD"));				//결재자부서코드
			
			if(!"Y".equals(rqSelfExp)) approverVO.setAskType("ART050");				//결재자 요청타입(ART010:기안, ART020:검토, ART030:협조, ART130:합의, ART040:감사, ART050:결재, ART053:1인전결)
			else approverVO.setAskType("ART055");	//참조
			
			approverVO.setActType("APT003");				//결재자 처리타입(APT001:승인, APT002:반려, APT003:대기, APT004:보류, APT051:찬성, APT052:반대)
			approverVO.setActDate("");	//결재일자
			//log.info("결재일자------------------------------------------------------------------------------"+toDate);
			approverVO.setOpinion("");						//결재의견 --- 반출사유
			if("Y".equals(rqSelfExp)){
				if("Y".equals(request.getUser().getProperty("APV_REF_YN").toString())) 
					legacyVO.getApprovers().add(approverVO);	
			}else{
				legacyVO.getApprovers().add(approverVO);
			}
		}
		//legacyVO.setApprovers(appList);
		//4. 본문 
		String filePath = "C:/_harmony/temp/legacy";//첨부파일 경로 정의
		
		java.io.File file = new java.io.File(filePath+"/body.html");		//본문 html //파일 이름을 어떻게 줄 지 상의 필요 
		URL fileURL = file.toURL(); //추상경로명을 나타내는 
		
		StringBuffer strBuf = new StringBuffer();
		String bodyContext = readFile("config/templates/body.html"); //
		//log.info("1----------------------------------------------------------------------------------------saveRequest_SB");
		bodyContext = bodyContext.replace("COMPANY_NM",pm.getString("component.site.company.name"));
		bodyContext = bodyContext.replace("DEPT_NM", rqDptNm);
		bodyContext = bodyContext.replace("USER_NM", rqNm);
		
		bodyContext = bodyContext.replace("DOC_MSG", request.getStringParam("DOC_MSG"));
       	if("6".equals(request.getStringParam("AUTHORITY"))) {
       		bodyContext = bodyContext.replace("AUTHORITY", mm.getMessage("MAIL_1041", request.getUser().getLocale()));
       		bodyContext = bodyContext.replace("", "Web");
       		bodyContext = bodyContext.replace("OUTDEVICE_HIDDEN","visible");
   			bodyContext = bodyContext.replace("RECIVER_HIDDEN","none");
   			bodyContext = bodyContext.replace("POLICY_HIDDEN", "none");
   	 	}else{
   	 		bodyContext = bodyContext.replace("AUTHORITY", mm.getMessage("MAIL_1040", request.getUser().getLocale()));
   	 		bodyContext = bodyContext.replace("OUTDEVICE_HIDDEN","none");
   			bodyContext = bodyContext.replace("RECIVER_HIDDEN","visible");
   			bodyContext = bodyContext.replace("POLICY_HIDDEN", "visible");
   			String ids_info = request.getStringParam("ids");
   	        String cols_ids = request.getStringParam("col_ids");
   	        String[] cols = cols_ids.split(",");
   	        String[] ids = ids_info.split(",");
   	        //docUserList
   	        String tempUser ="";
   	        Map user = new HashMap();
   	        for (int i = 0; i < ids.length; i++) {
   	       	 	for(int j=0; j < cols.length; j++ ) {
   	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
   	       	 	user.put(cols[j], tmps);     
   	         	}
   	       	 	tempUser +="<tr><td>"+(i+1)+"</td><td>"+user.get("COMPANY_NM")+"</td><td>"+user.get("USER_NM")+"</td><td>"+user.get("POSITION_NM")+"</td></tr>";
   	       	 	//PolicySet
   	       	 	String policy =user.get("POLICY").toString();
   	       	 	if("1".equals(policy.substring(0,1))) {
   	       	 		bodyContext = bodyContext.replace("CHKNETWORK", "checked");
   	       	 	}else{
   	       	 		bodyContext = bodyContext.replace("CHKNETWORK", "");
   	       	 	}
   	       	 	String policyDoc = user.get("POLICY_DOC").toString();
   	       	 	if("1".equals(policyDoc.substring(1,2))) {
   	       	 		bodyContext = bodyContext.replace("CHKPRINT", "checked");
   	       	 	} else {
   	       	 		bodyContext = bodyContext.replace("CHKPRINT", "");
   	       	 	}
   	       	 	if("1".equals(policyDoc.substring(2,3))) {
   	       	 		bodyContext = bodyContext.replace("CHKCLIPCOPY", "checked");
   	       	 	} else {
   	       	 		bodyContext = bodyContext.replace("CHKCLIPCOPY", "");
   	       	 	}
   	       	 	if("1".equals(policyDoc.substring(3,4))) {
   	       	 		bodyContext = bodyContext.replace("CHKREPACKING", "checked");
   	       	 	} else {
   	       	 		bodyContext = bodyContext.replace("CHKREPACKING", "");
   	       	 	}
   	       	 	if("1".equals(policyDoc.substring(4,5))) {
   	       	 		bodyContext = bodyContext.replace("CHKEXTCOPY", "checked");
   	       	 	} else {
   	       	 		bodyContext = bodyContext.replace("CHKEXTCOPY", "");
   	       	 	}
   	       	 	if("1".equals(policy.substring(1,2))){
   	       	 		bodyContext = bodyContext.replace("CHKDATE", "checked");
   	       	 		//log.info("EXPIRE_DATE-------------");
   	       	 		bodyContext = bodyContext.replace("EXPIRE_DATE", user.get("EXPIRE_DT").toString()+" "+mm.getMessage("MAIL_1042", request.getUser().getLocale()));
   	       	 		//log.info("EXPIRE_DATE-------------"+ user.get("EXPIRE_DT").toString());
   	       	 	}else{
   	       	 		bodyContext = bodyContext.replace("CHKDATE", "");
   	       	 		bodyContext = bodyContext.replace("EXPIRE_DATE", "");
   	       	 	}
   	       	 	if("1".equals(policy.substring(2,3))) {
   	       	 		bodyContext = bodyContext.replace("CHKREADCNT", "checked");
   	       	 		//log.info("READ_CNT-------------");
   	       	 		bodyContext = bodyContext.replace("READ_CNT", user.get("READ_COUNT").toString());
   	       	 		//log.info("READ_CNT-------------"+user.get("READ_COUNT").toString());
   	       	 	}else{
   	       	 		bodyContext = bodyContext.replace("CHKREADCNT", "");
   	       	 		bodyContext = bodyContext.replace("READ_CNT", "");
   	       	 	}
   	
   	        }
   	        bodyContext = bodyContext.replace("RECIVER_USER",tempUser);
   	       // bodyContext = bodyContext.replace("DATE", "");
   	        
   	 	}
       	String tempFileList=java.net.URLDecoder.decode(request.getStringParam("1_FILE_LIST"), "utf-8");
	   // log.info("2----------------------------------------------------------------------------------------saveRequest_SB : "+tempFileList);
		String files[] = tempFileList.split("\\|");
       	String temp[];
		String fileList = "<tr><td rowspan='"+files.length+"' align='center'>"+ mm.getMessage("MAIL_1043", request.getUser().getLocale()) +"</td>";
		for(int i=0; i<files.length; i++){
			 temp= files[i].split("/");
			if(i==0){
				fileList += "<td>"+temp[temp.length-1]+"</td>";	
			}else{
				fileList +="<tr><td>"+temp[temp.length-1]+"</td></tr>";
			}
		}
	    /*	파일리스트 맨 상단에 메세지 출력 하기 
	     * 
	     * String files[] = tempFileList.split("\\|");
       	String temp[];
		String fileList ="";
		if(files.length <= limitFileCount && totalFileSize <= limitFileSize){
			fileList = "<tr><td rowspan='"+files.length+"' align='center'>"+ mm.getMessage("MAIL_1043", request.getUser().getLocale()) +"</td>";
		}else{
			fileList = "<tr><td rowspan='"+(files.length+1)+"' align='center'>"+ mm.getMessage("MAIL_1043", request.getUser().getLocale()) +"</td>";
		}
		for(int i=0; i<files.length; i++){
			 temp= files[i].split("/");
			if(i==0){
				if(files.length <= limitFileCount && totalFileSize <= limitFileSize){
					fileList += "<td>"+temp[temp.length-1]+"</td>";	
				}else{
					fileList += "<td align='center' style='font-weight: bold; text-decoration: underline;'>"+mm.getMessage("CPMG_1059", limitFileSize, request.getUser().getLocale())+"</td>";	
					fileList += "<tr><td>"+temp[temp.length-1]+"</td></tr>";	
				}
			}else{
				fileList +="<tr><td>"+temp[temp.length-1]+"</td></tr>";
			}
		}*/
       	bodyContext = bodyContext.replace("FILELIST", fileList);
       	if(files.length <= limitFileCount && totalFileSize <= limitFileSize){
       	bodyContext = bodyContext.replace("HEADER", mm.getMessage("MAIL_1020", request.getUser().getLocale()));
       	}else{
       		bodyContext = bodyContext.replace("HEADER", mm.getMessage("MAIL_1020", request.getUser().getLocale())
       				+"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;<span style='text-decoration: underline;'>"
       				+mm.getMessage("CPMG_1059", pm.getInt("component.interlock.attachFile.size"), request.getUser().getLocale())
       				+"</span>");
       	}
       	bodyContext = bodyContext.replace("NEXT", mm.getMessage("MAIL_1021", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("COMPANY_LABEL1", mm.getMessage("MAIL_1022", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("DEPT_LABEL", mm.getMessage("MAIL_1023", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("USER_LABEL", mm.getMessage("MAIL_1024", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("MSG_LABEL", mm.getMessage("MAIL_1025", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("EXPORT_LABEL", mm.getMessage("MAIL_1026", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("RECIPIENT_LABEL", mm.getMessage("MAIL_1027", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("EXPORT_MEDIUM", mm.getMessage("MAIL_1028", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("EXPORT_POLICY", mm.getMessage("MAIL_1029", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("VALIDITY", mm.getMessage("MAIL_1030", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("AVAILABLE_COUNT", mm.getMessage("MAIL_1031", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("COMPANY_LABEL2", mm.getMessage("MAIL_1032", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("RECIPIENT_LABEL2", mm.getMessage("MAIL_1033", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("POSITION_LABEL", mm.getMessage("MAIL_1034", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("INTERNET_ACCESS", mm.getMessage("MAIL_1035", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("DISABLING_MOVE_COPY", mm.getMessage("MAIL_1036", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("DISABLING_PRINT", mm.getMessage("MAIL_1037", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("DISABLING_CLIPBOARD", mm.getMessage("MAIL_1038", request.getUser().getLocale()));
       	bodyContext = bodyContext.replace("DISABLING_REPACKING", mm.getMessage("MAIL_1039", request.getUser().getLocale()));
       	
       	//log.info("3----------------------------------------------------------------------------------------saveRequest_SB");
		strBuf.append(bodyContext);
		//log.info("4----------------------------------------------------------------------------------------saveRequest_SB");
		try {
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF8");
			writer.write(bodyContext, 0, bodyContext.length());                        
			writer.close();
			//log.info("------------------------------writeFile");

		}catch(Exception e){
		    e.printStackTrace();
		}		
		//log.info("5----------------------------------------------------------------------------------------saveRequest_SB");
		DataHandler handler = new DataHandler(fileURL);
		Holder<DataHandler> holder = new Holder<DataHandler>();
		holder.value = handler;
		//log.info("6----------------------------------------------------------------------------------------saveRequest_SB");
//		System.out.println("handler --> " + handler.getContent());
			
		AppFileVO fileVO = new AppFileVO();
		fileVO.setFileName("body.html");
		fileVO.setContent(handler);
		fileVO.setFileType("body");
		//log.info("7----------------------------------------------------------------------------------------saveRequest_SB");
		//LegacyVO.Files legacyfile = new LegacyVO.Files();
		legacyVO.getFiles().add(fileVO);
		//log.info("8----------------------------------------------------------------------------------------saveRequest_SB");
		
		// 파일 첨부 //
		if(files.length <= limitFileCount && totalFileSize <= limitFileSize){
			//log.info("결재연동 파일사이즈 제한, 파일 갯수 제한을 넘지 않았을 경우 모든 파일을 첨부---------------------------------------------------");
	 		for(int i=0; i<files.length; i++){ 
	 			legacyVO.getFiles().add(attachFile(files[i]));
	 		}
		}/*else if(limitFileSize >= minFileSize){ //최소 파일의 용량이 제한 용량보다 적을 경우 첨부 필요.
				if(limitFileSize <totalFileSize){//총 사이즈가 제한 사이즈 이상일 경우, 최소 파일 하나만 첨부한다.
					log.info("결재 연동 파일 사이즈 제한을 초과하여, 최소 파일 하나만 첨부------------------------------------------------------------");
					int tempResult=0;
					for(int i=0; i<files.length; i++){ 
						temp= files[i].split("/");
						if(temp[temp.length-1].equals(minFileName) && tempResult == 0){ //최소 파일 파일 이름이 같은 경우만
				 			legacyfile.getFile().add(attachFile(files[i]));
				 			tempResult=1;
						}//if
					}//for
				}else if(files.length > limitFileCount){//총 사이즈가 제한 용량보다 작지만, 첨부 가능 제한 갯수를 넘었을 경우
					log.info("결재 연동 파일 첨부 갯수를 초과하여, 제한된 갯수만큼 첨부----------------------------------------------------------");
					for(int i=0; i<=limitFileCount; i++){ //제한 갯수 만큼만 첨부한다.
						legacyfile.getFile().add(attachFile(files[i]));
					}//for
				}//else if
		}*/ //어떤 경우든 상관 없이 승인대기함에서 확인이 필요할 경우 첨부하지 않는다.
		//legacyVO.setFiles(legacyfile);
		
		//log.info("11----------------------------------------------------------------------------------------saveRequest_SB");		
		//6. 최종 결재연계정보			
		String strDocId = GuidUtil.getGUID();
		HeaderVO headerVO = new HeaderVO();
		headerVO.setDocType("submit"); 					//기안요청(SUBMIT)/검토요청() 구분하여 필수 입력
		headerVO.setSendServerId("CP"); 				//발신 서버 ID
		headerVO.setReceiveServerId("CP"); 				//수신 서버 ID
		headerVO.setSystemCode("MYGUARD"); 				//업무시스템코드
		headerVO.setBusinessCode("CP");					//업무코드
		headerVO.setOriginDocId(strDocId); 				//문서 ID
		headerVO.setTitle(request.getStringParam("DOC_TITLE"));		//제목
		headerVO.setDocNum("");  						//문서번호(문서분류 있을경우 명기)
		headerVO.setPublication("1");					//공개정보 (1:공개, 2:부분공개, 3:비공개)
		headerVO.setDraftDate(toDate);	//연계일자 //문서번호로 파싱해서 저장
		//log.info("12----------------------------------------------------------------------------------------saveRequest_SB");	    
		legacyVO.setHeader(headerVO);

		//7. 예약필드 (빈값호출) //뭐할때 필요한지
		ReserveVO reserveVO = new ReserveVO();
		reserveVO.setField1("");
		legacyVO.setReserve(reserveVO);	
		//log.info("13----------------------------------------------------------------------------------------saveRequest_SB");		
				
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(LegacyService.class);
		factory.setAddress(pm.getString("component.interlock.request.url"));
		factory.setBindingId(javax.xml.ws.soap.SOAPBinding.SOAP11HTTP_MTOM_BINDING);
		
		LegacyService legacyService = (LegacyService) factory.create();
		LegacyVO legacyResVO = legacyService.insertAppDoc(legacyVO);
		//log.info("14----------------------------------------------------------------------------------------saveRequest_SB");
		strReturn = legacyResVO.getResult().getMessageCode();
		log.info("####legacyResVO ----> " + legacyResVO.getResult().getMessageText());
		log.info("####strReturn -------> " + strReturn);
		log.info("결재 연동 기안 처리(WEB) End----------------------------------------------------------------------------------------saveRequest_SB"+strReturn);
	   }catch(WebServiceException we){
 		   log.error("결재연동 웹서비스 요청 중 -----------------------------------------------------------------------------------------이시스에 문의 "+we.toString());
 	   }catch(Exception e){
			log.info("결재 연동 기안 처리(WEB) ERROR-----------------------------------------saveRequest_SB"+e.toString());
			strReturn ="0";
			
	   }
		return strReturn;
   }
    private AppFileVO attachFile(String path) throws Exception{
    	String temp[]= path.split("/");
    	java.io.File attachFile = new java.io.File(path);
		URL attachFileURL = attachFile.toURL(); //추상경로명을 나타내는
		DataHandler attachHandler= new DataHandler(attachFileURL);
		Holder<DataHandler> attachHolder = new Holder<DataHandler>();
		attachHolder.value = attachHandler;
		AppFileVO attfileVO = new AppFileVO(); 
		attfileVO.setFileName(temp[temp.length-1]);
		attfileVO.setContent(attachHandler);
		attfileVO.setFileType("attach");
		return attfileVO;
    }
    //상신브레이크 결재 연동 - 기안처리
    public String saveCP_SB(IRequest request, Map docMap, JSONArray docUsers){
 	   String strReturn = "1";  //리턴 코드 : 성공:1, 실패 0
 	   try{
 	    	String rqID = request.getUser().getId().toString();
 	    	String rqNm = request.getUser().getProperty("USER_NM").toString();
 	    	String rqDptCd = request.getUser().getProperty("DEPT_CD").toString();
 	    	String rqDptNm = request.getUser().getProperty("DEPT_NM").toString();
 	    	String rqFunction = request.getUser().getProperty("FUNCTION_CD").toString();
 	    	String rqSelfExp = request.getUser().getProperty("SELF_EXP_YN").toString();
 	    	String docNo = docMap.get("DOC_NO").toString();
 	    	
 	    	int limitFileCount = pm.getInt("component.interlock.attachFile.count"); //결재연동 첨부파일 제한 갯수
 			int limitFileSize = pm.getInt("component.interlock.attachFile.size")*1024*1024; //결재연동 첨부파일 제한 사이즈
 			
 			Map attachFileInfo =(Map)sm.getItem("mg.cp.attachFileGroupInfo",docMap); //DOC_ID, MIN(FILE_NAME) AS MIN_FILE_NAME,TOTAL_FILE_SIZE, MIN_FILE_SIZE 
 			//log.info("saveCP_SB-------------------------------------------------------------------------attachFileInfo---------"+attachFileInfo.toString());
 			int totalFileSize = Integer.parseInt(attachFileInfo.get("TOTAL_FILE_SIZE").toString()); // 해당 문서에 관련된 파일의 총 용량
 			//int minFileSize =  Integer.parseInt(attachFileInfo.get("MIN_FILE_SIZE").toString()); //해당 문서에 관련된 파일 중 가장 작은 용량
 			//String minFileName = attachFileInfo.get("MIN_FILE_NAME").toString();//해당 문서에 관련된 파일 중 가장 작은 용량을 갖은 파일명
 	    	
 	    	
 	    	LegacyVO legacyVO = new LegacyVO();
    		
    		//신청자 정보 추가
	    	SenderVO senderVO = new SenderVO();
	 		senderVO.setOrgCode("C000");		//회사코드
	 		senderVO.setOrgName("상신브레이크");	//회사명
	 		senderVO.setDeptCode(rqDptCd);		//부서코드
	 		senderVO.setDeptName(rqDptNm);		//부서명
	 		senderVO.setPosition(rqFunction);			//직위코드
	 		senderVO.setUserId(rqID);		//기안자 ID
	 		//log.info("상신브레이크 결재 연동 - 기안처리--------------------------------------------------------------기안자ID/rqSelfExp : \t"+rqID+"/"+rqSelfExp);
	 		senderVO.setUserName(rqNm);	//기안자 이름
	 		legacyVO.setSender(senderVO);
 		
	 		ReceiverVO receiverVO = new ReceiverVO();
	 		receiverVO.setOrgCode("C000");
	 		receiverVO.setOrgName("상신브레이크");
	 		receiverVO.setDeptCode(rqDptCd);
	 		receiverVO.setDeptName(rqDptNm);
	 		receiverVO.setPosition(rqFunction);
	 		receiverVO.setUserId(rqID);
	 		receiverVO.setUserName(rqNm);
	 		legacyVO.setReceiver(receiverVO);
    		
	 		//2. 처리결과(임의값 지정) 
	 		ResultVO resultVO = new ResultVO(); //무슨 내용을 넣어야하는 지 확인 필요 
	 		resultVO.setMessageCode(docNo);  			  //CP_DOCNO
	 		//log.info("setMessageCode--------------------------------------------------------"+docNo);
	 		resultVO.setMessageText("DOC_NO");	  	  //처리 메시지 내용 (필요시 사용)
	 		//resultVO.setMessageDate("");					  //처리일자		 (필요시 사용)
	 		legacyVO.setResult(resultVO);		
	 		
	 		String approverId="";
			ArrayList<String> approver = new ArrayList<String>();
			if("personal".equals(pm.getString("component.export.approver"))) {
				
				Map approverMap = new HashMap();
	 			approverMap.put("USER_ID",  rqID);
		       	List lists = sm.getList("mg.cp.getApproverId",approverMap); //다중결재선때문에 수정, 하지만 우선수위를 정하는 컬럼이 필요할 듯.
		       	for (int i = 0; i < lists.size(); i++) {
		        	 approverMap =(Map) lists.get(i); 
		        	 approver.add(this.getStringWithNullCheck(approverMap, "APPROVER_ID")); 
		         }
		         for(int j =0; j<lists.size(); j++){
		        	 if(approver.get(j).equals(rqID)){
		        		 String temp = approver.get(0);
		        		 approver.set(0, approver.get(j));
		        		 approver.set(j, temp);
		        	 }
		         }
		         approverId=approver.get(approver.size()-1);
			}
			//log.info("승인자 또는 검토자 ID ---------------------------------------------"+approverId);

			String dbName = pm.getString("component.sql.database");
			Map map;
			
			if("mssql".equals(dbName)){
				map = (Map)sm.getItem("app.user.getUserByProperties", approverId);
			}else{
				map = (Map)sm.getItem("app.user.getUserByProperties_" + dbName, approverId);
			}
			
			//LegacyVO.Approvers appList = new LegacyVO.Approvers(); 
			
			ApproverVO approverVO1 = new ApproverVO(); // 기안자
			approverVO1.setSerialOrder("1");					
			approverVO1.setParallelOrder("1");				//결재순서로 기본 1, 이중결재일 경우 신청부서 1, 주관부서 2 
			approverVO1.setUserName(rqNm);				//결재자이름
			approverVO1.setUserId(rqID);
			approverVO1.setPosition(rqFunction);						//결재자 직위코드
			approverVO1.setDeptName(rqDptNm);				//결재자부서명
			approverVO1.setDeptCode(rqDptCd);				//결재자부서코드
			
			if(!"Y".equals(rqSelfExp)) approverVO1.setAskType("ART010");				//결재자 요청타입(ART010:기안, ART020:검토, ART030:협조, ART130:합의, ART040:감사, ART050:결재, ART053:1인전결)
			else approverVO1.setAskType("ART053");				//ART053: 1인전결
			
			approverVO1.setActType("APT001");				//결재자 처리타입(APT001:승인, APT002:반려, APT003:대기, APT004:보류, APT051:찬성, APT052:반대)
			
	 		String toDate = docNo.substring(1,5)+"-"+docNo.substring(5,7)+"-"+docNo.substring(7,9)+" "+docNo.substring(9,11)+":"+docNo.substring(11, 13)+":"+docNo.substring(13, 15);
	 		approverVO1.setActDate(toDate);	//결재일자
			//log.info("결재일자------------------------------------------------------------------------------"+toDate);
			//approverVO1.setOpinion(request.getStringParam("DOC_MSG"));						//결재의견 --- 반출사유
			approverVO1.setOpinion("");
			legacyVO.getApprovers().add(approverVO1);
			//log.info("saveCP_SB----------------------------------------------------------------------------------------------1");
			if(approver == null || approver.size()==0){
				log.info("해당 사용자("+rqID+")의 결재선에 승인자가 없습니다. 자가승인 가능한 사람이 아닐 경우 기안시 오류가 발생합니다. SYSUSERAPPROVER을 확인하세요.");
			}else if(!rqID.equals(approverId)){
				//log.info("승인자 또는 첨부자 추가 ----------------"+approverId);
				ApproverVO approverVO = new ApproverVO(); // 결재자 
				approverVO.setSerialOrder("2");					//결재자 수만큼 반복
				approverVO.setParallelOrder("1");				//결재순서로 기본 1, 이중결재일 경우 신청부서 1, 주관부서 2 
				approverVO.setUserName(this.getStringWithNullCheck(map,"USER_NM"));				//결재자이름
				approverVO.setUserId(approverId);
				approverVO.setPosition(this.getStringWithNullCheck(map,"FUNCTION_CD"));						//결재자 직위코드
				approverVO.setDeptName(this.getStringWithNullCheck(map,"DEPT_NM"));				//결재자부서명
				approverVO.setDeptCode(this.getStringWithNullCheck(map,"DEPT_CD"));				//결재자부서코드
				
				if(!"Y".equals(rqSelfExp)) approverVO.setAskType("ART050");				//결재자 요청타입(ART010:기안, ART020:검토, ART030:협조, ART130:합의, ART040:감사, ART050:결재, ART053:1인전결)
				else approverVO.setAskType("ART055");	//참조
				//log.info("saveCP_SB----------------------------------------------------------------------------------------------1");
				approverVO.setActType("APT003");				//결재자 처리타입(APT001:승인, APT002:반려, APT003:대기, APT004:보류, APT051:찬성, APT052:반대)
				approverVO.setActDate("");	//결재일자
				//log.info("결재일자------------------------------------------------------------------------------"+toDate);
				approverVO.setOpinion("");						//결재의견 --- 반출사유
				if("Y".equals(rqSelfExp)){
					if("Y".equals(request.getUser().getProperty("APV_REF_YN").toString())) 
						legacyVO.getApprovers().add(approverVO);	
				}else{
					legacyVO.getApprovers().add(approverVO);
				}
			}
			//legacyVO.setApprovers(appList);
	 		//4. 본문 
	 		String filePath = "C:/_harmony/temp/legacy";//첨부파일 경로 정의
	 		
	 		java.io.File file = new java.io.File(filePath+"/body.html");		//본문 html //파일 이름을 어떻게 줄 지 상의 필요 
	 		URL fileURL = file.toURL(); //추상경로명을 나타내는 
	 		
	 		StringBuffer strBuf = new StringBuffer();
	 		String bodyContext = readFile("config/templates/body.html"); //
	 		//log.info("1----------------------------------------------------------------------------------------saveCP_SB");
	 		bodyContext = bodyContext.replace("COMPANY_NM",pm.getString("component.site.company.name"));
	 		//log.info("1-1---------------------------------------------------------------------------------------saveCP_SB");
	 		bodyContext = bodyContext.replace("DEPT_NM", rqDptNm);
	 		//log.info("1-2---------------------------------------------------------------------------------------saveCP_SB");
	 		bodyContext = bodyContext.replace("USER_NM", rqNm);
	 		
	 		bodyContext = bodyContext.replace("DOC_MSG", docMap.get("DOC_MSG").toString());
	        	if("6".equals(docMap.get("AUTHORITY"))) {
	        		//log.info("1-3   원문 반출---------------------------------------------------------------------------------------saveCP_SB");
	        		bodyContext = bodyContext.replace("AUTHORITY", mm.getMessage("MAIL_1041", request.getUser().getLocale()));
	        		String outTempList[] = docMap.get("OUTDEVICE").toString().split(",");
	        		String outTemp="";
	        		ICodeManagement MCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	        		List sCode = MCode.getCodes("A0313");
	        		for (Iterator all = sCode.iterator(); all.hasNext();) {
						ICode icd = (ICode) all.next();
						for(int i=0; i<outTempList.length; i++){
							//log.info("outTempList["+i+"] / icd.getName------------------------------------------------------"+outTempList[i]+" / "+icd.getCd());
							if(icd.getCd().equals(outTempList[i])){
								if(i==0) outTemp +=icd.getName();
								else outTemp +=", "+icd.getName();
							}
						}
	        		}
	        		
	        		JSONParser parser = new JSONParser();
	        		String jsonData = request.getStringParam("fileRegParam");
	        		JSONObject jObj = (JSONObject)parser.parse(jsonData);
	        		JSONArray jURL = (JSONArray)jObj.get("listURL"); 
	          	    for (int i = 0; i < jURL.size(); i++) {
	          		    JSONObject docs = (JSONObject)jURL.get(i);
	          	        outTemp += "<br> Site "+(i+1)+") "+docs.get("CONF_VAL").toString();
	          	    } 	        		
	        		bodyContext = bodyContext.replace("OUTDEVICE", outTemp);
	        		bodyContext = bodyContext.replace("OUTDEVICE_HIDDEN","visible");
	    			bodyContext = bodyContext.replace("RECIVER_HIDDEN","none");
	    			bodyContext = bodyContext.replace("POLICY_HIDDEN", "none");
	    	 	}else{
	    	 		//log.info("1-3   보안 반출---------------------------------------------------------------------------------------saveCP_SB");
	    	 		bodyContext = bodyContext.replace("AUTHORITY", mm.getMessage("MAIL_1040", request.getUser().getLocale()));
	    	 		bodyContext = bodyContext.replace("OUTDEVICE_HIDDEN","none");
	    			bodyContext = bodyContext.replace("RECIVER_HIDDEN","visible");
	    			bodyContext = bodyContext.replace("POLICY_HIDDEN", "visible");
	    			
	    	        String tempUser ="";
	    	       
	    	       // Doc User 등록
		     		JSONArray docUser = docUsers; 
	   		  	for (int i = 0; i < docUser.size(); i++) {
	        		    JSONObject docs = (JSONObject)docUser.get(i);
	        		   Map user = new HashMap();
	        		   user.put("dbName", pm.getString("component.sql.database"));
	  	       	 	   user.put("USER_ID", docs.get("USER_ID").toString());
	  	       	 	   user.put("EMAIL", docs.get("USER_ID").toString());
	  	       	 	   user.put("COMPANY_ID", docs.get("COMPANY_ID").toString());
	  	       	 	   Map userInfo  = (Map)sm.getItem("mg.cp.selectCompUser", user);
	  	       	 	   user.put("USER_NM", this.getStringWithNullCheck(userInfo, "USER_NM"));
	  	       	 	   user.put("COMPANY_NM", this.getStringWithNullCheck(userInfo, "COMPANY_NM"));
	  	       	 	   user.put("POSITION_NM", this.getStringWithNullCheck(userInfo, "POSITION_NM"));
	  	       	 	   user.put("POLICY", docs.get("POLICY").toString());
	  	       	 	   user.put("POLICY_DOC", docs.get("POLICY_DOC").toString());
	  	       	 	   user.put("EXPIRE_DT", docs.get("EXPIRE_DT").toString());
	  	       	 	   user.put("READ_COUNT", docs.get("READ_COUNT").toString());
	
	    	       	 	tempUser +="<tr><td>"+(i+1)+"</td><td>"+user.get("COMPANY_NM")+"</td><td>"+user.get("USER_NM")+"</td><td>"+user.get("POSITION_NM")+"</td></tr>";
	    	       	 	//PolicySet
	    	       	 	String policy =user.get("POLICY").toString();
	    	       	 	if("1".equals(policy.substring(0,1))) {
	    	       	 		bodyContext = bodyContext.replace("CHKNETWORK", "checked");
	    	       	 	}else{
	    	       	 		bodyContext = bodyContext.replace("CHKNETWORK", "");
	    	       	 	}
	    	       	 	String policyDoc = user.get("POLICY_DOC").toString();
	    	       	 	if("1".equals(policyDoc.substring(1,2))) {
	    	       	 		bodyContext = bodyContext.replace("CHKPRINT", "checked");
	    	       	 	} else {
	    	       	 		bodyContext = bodyContext.replace("CHKPRINT", "");
	    	       	 	}
	    	       	 	if("1".equals(policyDoc.substring(2,3))) {
	    	       	 		bodyContext = bodyContext.replace("CHKCLIPCOPY", "checked");
	    	       	 	} else {
	    	       	 		bodyContext = bodyContext.replace("CHKCLIPCOPY", "");
	    	       	 	}
	    	       	 	if("1".equals(policyDoc.substring(3,4))) {
	    	       	 		bodyContext = bodyContext.replace("CHKREPACKING", "checked");
	    	       	 	} else {
	    	       	 		bodyContext = bodyContext.replace("CHKREPACKING", "");
	    	       	 	}
	    	       	 	if("1".equals(policyDoc.substring(4,5))) {
	    	       	 		bodyContext = bodyContext.replace("CHKEXTCOPY", "checked");
	    	       	 	} else {
	    	       	 		bodyContext = bodyContext.replace("CHKEXTCOPY", "");
	    	       	 	}
	    	       	 	if("1".equals(policy.substring(1,2))){
	    	       	 		bodyContext = bodyContext.replace("CHKDATE", "checked");
	    	       	 		//log.info("EXPIRE_DATE-------------");
	    	       	 		bodyContext = bodyContext.replace("EXPIRE_DATE", user.get("EXPIRE_DT").toString()+" "+mm.getMessage("MAIL_1042", request.getUser().getLocale()));
	    	       	 		//log.info("EXPIRE_DATE-------------"+ user.get("EXPIRE_DT").toString());
	    	       	 	}else{
	    	       	 		bodyContext = bodyContext.replace("CHKDATE", "");
	    	       	 		bodyContext = bodyContext.replace("EXPIRE_DATE", "");
	    	       	 	}
	    	       	 	if("1".equals(policy.substring(2,3))) {
	    	       	 		bodyContext = bodyContext.replace("CHKREADCNT", "checked");
	    	       	 		//log.info("READ_CNT-------------");
	    	       	 		bodyContext = bodyContext.replace("READ_CNT", user.get("READ_COUNT").toString());
	    	       	 		//log.info("READ_CNT-------------"+user.get("READ_COUNT").toString());
	    	       	 	}else{
	    	       	 		bodyContext = bodyContext.replace("CHKREADCNT", "");
	    	       	 		bodyContext = bodyContext.replace("READ_CNT", "");
	    	       	 	}
	    	        }
	    	        bodyContext = bodyContext.replace("RECIVER_USER",tempUser);
	    	       // bodyContext = bodyContext.replace("DATE", "");
	    	        
	    	 	}
	       	log.info("2----------------------------------------------------------------------------------------saveCP_SB");
	       	String tempFileList=docMap.get("FILE_LIST").toString();
	       	String files[] = tempFileList.split("\\|");
	       	String temp[];
	    	String fileList = "<tr><td rowspan='"+files.length+"' align='center'>"+ mm.getMessage("MAIL_1043", request.getUser().getLocale()) +"</td>";
	    	for(int i=0; i<files.length; i++){
	    		 temp= files[i].split("/");
	    		if(i==0){
	    			fileList += "<td>"+temp[temp.length-1]+"</td>";	
	    		}else{
	    			fileList +="<tr><td>"+temp[temp.length-1]+"</td></tr>";
	    		}
	    	}
	 		
        	bodyContext = bodyContext.replace("FILELIST", fileList);
           	if(files.length <= limitFileCount && totalFileSize <= limitFileSize){
           		bodyContext = bodyContext.replace("HEADER", mm.getMessage("MAIL_1020", request.getUser().getLocale()));
           	}else{
           		bodyContext = bodyContext.replace("HEADER", mm.getMessage("MAIL_1020", request.getUser().getLocale())
           				+"<br><br>&nbsp;&nbsp;&nbsp;&nbsp;<span style='text-decoration: underline;'>"
           				+mm.getMessage("CPMG_1058", pm.getInt("component.interlock.attachFile.size"), request.getUser().getLocale())
           				+"</span>");
           	} 	
           	bodyContext = bodyContext.replace("NEXT", mm.getMessage("MAIL_1021", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("COMPANY_LABEL1", mm.getMessage("MAIL_1022", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("DEPT_LABEL", mm.getMessage("MAIL_1023", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("USER_LABEL", mm.getMessage("MAIL_1024", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("MSG_LABEL", mm.getMessage("MAIL_1025", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("EXPORT_LABEL", mm.getMessage("MAIL_1026", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("RECIPIENT_LABEL", mm.getMessage("MAIL_1027", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("EXPORT_MEDIUM", mm.getMessage("MAIL_1028", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("EXPORT_POLICY", mm.getMessage("MAIL_1029", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("VALIDITY", mm.getMessage("MAIL_1030", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("AVAILABLE_COUNT", mm.getMessage("MAIL_1031", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("COMPANY_LABEL2", mm.getMessage("MAIL_1032", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("RECIPIENT_LABEL2", mm.getMessage("MAIL_1033", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("POSITION_LABEL", mm.getMessage("MAIL_1034", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("INTERNET_ACCESS", mm.getMessage("MAIL_1035", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("DISABLING_MOVE_COPY", mm.getMessage("MAIL_1036", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("DISABLING_PRINT", mm.getMessage("MAIL_1037", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("DISABLING_CLIPBOARD", mm.getMessage("MAIL_1038", request.getUser().getLocale()));
           	bodyContext = bodyContext.replace("DISABLING_REPACKING", mm.getMessage("MAIL_1039", request.getUser().getLocale()));
           	
        	//log.info("3----------------------------------------------------------------------------------------saveCP_SB");
	 		strBuf.append(bodyContext);
	 		//log.info("4----------------------------------------------------------------------------------------saveCP_SB");
	 		try {
	 			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF8");
	 			writer.write(bodyContext, 0, bodyContext.length());                        
	 			writer.close();
	 			
	 		}catch(Exception e){
	 		    e.printStackTrace();
	 		}		
	 		//log.info("5----------------------------------------------------------------------------------------saveCP_SB");
	 		DataHandler handler = new DataHandler(fileURL);
	 		Holder<DataHandler> holder = new Holder<DataHandler>();
	 		holder.value = handler;
	 		//log.info("6----------------------------------------------------------------------------------------saveCP_SB");
	// 		System.out.println("handler --> " + handler.getContent());
	 			
	 		AppFileVO fileVO = new AppFileVO();
	 		fileVO.setFileName("body.html");
	 		fileVO.setContent(handler);
	 		fileVO.setFileType("body");
	 		//log.info("7----------------------------------------------------------------------------------------saveCP_SB");
	 		//LegacyVO.Files legacyfile = new LegacyVO.Files();
	 		legacyVO.getFiles().add(fileVO);
	 		//log.info("8----------------------------------------------------------------------------------------saveCP_SB");
	 		

	 	// 파일 첨부 //
			if(files.length <= limitFileCount && totalFileSize <= limitFileSize){
				log.info("결재연동 파일사이즈 제한, 파일 갯수 제한을 넘지 않았을 경우 모든 파일을 첨부---------------------------------------------------");
		 		for(int i=0; i<files.length; i++){ 
		 			legacyVO.getFiles().add(attachFile(files[i]));
		 		}
			}/*else if(limitFileSize >= minFileSize){ //최소 파일의 용량이 제한 용량보다 적을 경우 첨부 필요.
					if(limitFileSize <totalFileSize){//총 사이즈가 제한 사이즈 이상일 경우, 최소 파일 하나만 첨부한다.
						log.info("결재 연동 파일 사이즈 제한을 초과하여, 최소 파일 하나만 첨부------------------------------------------------------------");
						int tempResult=0;
						for(int i=0; i<files.length; i++){ 
							String temp[]= files[i].split("/");
							if(temp[temp.length-1].equals(minFileName) && tempResult == 0){ //최소 파일 파일 이름이 같은 경우만
					 			legacyfile.getFile().add(attachFile(files[i]));
					 			tempResult=1;
							}//if
						}//for
					}else if(files.length > limitFileCount){//총 사이즈가 제한 용량보다 작지만, 첨부 가능 제한 갯수를 넘었을 경우
						log.info("결재 연동 파일 첨부 갯수를 초과하여, 제한된 갯수만큼 첨부----------------------------------------------------------");
						for(int i=0; i<=limitFileCount; i++){ //제한 갯수 만큼만 첨부한다.
							legacyfile.getFile().add(attachFile(files[i]));
						}//for
					}//else if
			}*/ //제한을 넘을 경우 첨부 파일을 첨부하지 않는다. 사용자의 혼란을 가중 시키지 않기 위해
	 		//legacyVO.setFiles(legacyfile);
	 		
	 		log.info("11----------------------------------------------------------------------------------------saveCP_SB");		
	 		//6. 최종 결재연계정보			
	 		String strDocId = GuidUtil.getGUID();
	 		HeaderVO headerVO = new HeaderVO();
	 		headerVO.setDocType("submit"); 					//기안요청(SUBMIT)/검토요청() 구분하여 필수 입력
	 		headerVO.setSendServerId("CP"); 				//발신 서버 ID
	 		headerVO.setReceiveServerId("CP"); 				//수신 서버 ID
	 		headerVO.setSystemCode("MYGUARD"); 				//업무시스템코드
	 		headerVO.setBusinessCode("CP");					//업무코드
	 		headerVO.setOriginDocId(strDocId); 				//문서 ID
	 		headerVO.setTitle(docMap.get("DOC_TITLE").toString());		//제목
	 		headerVO.setDocNum("");  						//문서번호(문서분류 있을경우 명기)
	 		headerVO.setPublication("1");					//공개정보 (1:공개, 2:부분공개, 3:비공개)
	 		headerVO.setDraftDate(toDate);	//연계일자 //문서번호로 파싱해서 저장
	 		log.info("12----------------------------------------------------------------------------------------saveCP_SB");	    
	 		legacyVO.setHeader(headerVO);
	
	 		//7. 예약필드 (빈값호출) //뭐할때 필요한지
	 		ReserveVO reserveVO = new ReserveVO();
	 		reserveVO.setField1("");
	 		legacyVO.setReserve(reserveVO);	
	 		log.info("13----------------------------------------------------------------------------------------saveCP_SB");		
	 				
	 		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
	 		factory.setServiceClass(LegacyService.class);
	 		factory.setAddress(pm.getString("component.interlock.request.url"));
	 		factory.setBindingId(javax.xml.ws.soap.SOAPBinding.SOAP11HTTP_MTOM_BINDING);
	 		log.info("14----------------------------------------------------------------------------------------saveCP_SB");		
	 		LegacyService legacyService = (LegacyService) factory.create();
	 		LegacyVO legacyResVO = legacyService.insertAppDoc(legacyVO);
	 		log.info("15----------------------------------------------------------------------------------------saveCP_SB");
	 		strReturn = legacyResVO.getResult().getMessageCode();
	 		log.info("####legacyResVO ----> " + legacyResVO.getResult().getMessageText());
	 		log.info("####strReturn -------> " + strReturn+" \n 결재연동 기안처리(VF) 완료");
 	   }catch(WebServiceException we){
 		   log.error("결재연동(VF) 웹서비스 요청 중 -----------------------------------------------------------------------------------------이시스에 문의 \n"+we.toString());
 	   }catch(Exception e){
 			log.error("결재 연동 기안처리(VF) ERROR-----------------------------------------"+e.toString());
 			strReturn ="0";
 	   }
 		return strReturn;
    }
}
