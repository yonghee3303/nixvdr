package mg.site;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.base.ComponentRegistry;
import com.core.base.Constants;
import com.core.component.message.IMessageManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;
import com.core.framework.core.IRequest;

public class IljinInterlock {
	private IParameterManagement pm = (IParameterManagement)ComponentRegistry.lookup(IParameterManagement.class);
	private IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	private Log log = LogFactory.getLog(this.getClass());
	private ISqlManagement sm = (ISqlManagement)ComponentRegistry.lookup(ISqlManagement.class);
	
	public String readFile(String filename)
	{   	
    	String content = null;
 	   File file = new File(getClass().getClassLoader().getResource(filename).getFile()); //for ex foo.txt
 	   try {
 	       //FileReader reader = new FileReader(file); //한글깨짐
 		   InputStreamReader reader= new InputStreamReader(new FileInputStream(file),"UTF-8"); //한글깨지지않음
 		   char[] chars = new char[(int) file.length()];
 	       reader.read(chars);
 	       content = new String(chars);
 	       reader.close();
 	       log.info("------------------------------readFile");
 	   } catch (IOException e) {
 		   log.error("-----------------------------readFile Exception-----"+e.toString());
 	       e.printStackTrace();
 	   }
 	   return content;
	}
	
	public String saveRequest(IRequest request, String[] ids, String[] cols) {
		
		String docNo = null;
		JSONParser parser = new JSONParser();
		JSONObject s_user = null;
		Locale locale = null;
		
		try {
			//사용자 정보 추출
	      	if(request.getUser() != null) {
	      		s_user = new JSONObject();
	      		
	      		Map params = request.getUser().getProperties();
	      		Iterator it = params.keySet().iterator();
	      		
	      		while(it.hasNext()) {
	      			String key = (String)it.next();
	      			s_user.put(key, params.get(key));
	      		}
	      		locale = request.getUser().getLocale();
	      		
	      	} else {
	      		s_user = (JSONObject)parser.parse(request.getStringParam("userInfo"));
	      		String lang = request.getStringParam("j_janguage");		//language
	    		
	    		if("ko".equals(lang)) {
	    			locale = Locale.KOREA;
	    		} else if("en".equals(lang)) {
	    			locale = Locale.US;
	    		} else if("zh".equals(lang)) {
	    			locale = Locale.CHINA;
	    		} else if("ja".equals(lang)) {
	    			locale = Locale.JAPAN;
	    		} else {
	    			locale = Locale.KOREA;
	    		}
	      	}
	      	
			Map itm = request.getMap();
			
			String rqID = (String)s_user.get("USER_ID");
	    	String rqNm = (String)s_user.get("USER_NM");
	    	String rqDptCd = (String)s_user.get("DEPT_CD");
	    	String rqDptNm = (String)s_user.get("DEPT_NM");
	    	String rqFunction = (String)s_user.get("FUNCTION_CD");
	    	String rqSelfExp = (String)s_user.get("SELF_EXP_YN");
	    	docNo = request.getStringParam("DOC_NO");
	    	
	    	if(!"Y".equals(rqSelfExp)) {
	    		//StringBuffer strBuf = new StringBuffer();
	    		String bodyContext = readFile("config/templates/nobody.html"); //
	    		//log.info("1----------------------------------------------------------------------------------------saveRequest_ILJIN");
	    		bodyContext = bodyContext.replace("COMPANY_NM",pm.getString("component.site.company.name"));
	    		bodyContext = bodyContext.replace("DEPT_NM", rqDptNm);
	    		bodyContext = bodyContext.replace("USER_NM", rqNm);
	    		
	    		bodyContext = bodyContext.replace("DOC_MSG", request.getStringParam("DOC_MSG"));
	    		if("6".equals(request.getStringParam("AUTHORITY"))) {
	    			bodyContext = bodyContext.replace("AUTHORITY", mm.getMessage("MAIL_1041", locale));
	    			bodyContext = bodyContext.replace("", "Web");
	    			bodyContext = bodyContext.replace("OUTDEVICE_HIDDEN","visible");
	    			bodyContext = bodyContext.replace("RECIVER_HIDDEN","none");
	    			bodyContext = bodyContext.replace("POLICY_HIDDEN", "none");
	    		}else{
	    			bodyContext = bodyContext.replace("AUTHORITY", mm.getMessage("MAIL_1040", locale));
	    			bodyContext = bodyContext.replace("OUTDEVICE_HIDDEN","none");
	    			bodyContext = bodyContext.replace("RECIVER_HIDDEN","visible");
	    			bodyContext = bodyContext.replace("POLICY_HIDDEN", "visible");
	    			//String ids_info = request.getStringParam("ids");
	    			//String cols_ids = request.getStringParam("col_ids");
	    			//String[] cols = cols_ids.split(",");
	    			//String[] ids = ids_info.split(",");
	    			
	    			//docUserList
	    			String tempUser ="";
	    			Map userMap = new HashMap();
	    			for (int i = 0; i < ids.length; i++) {
	    				for(int j=0; j < cols.length; j++ ) {
	    					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	    					userMap.put(cols[j], tmps);     
	    				}
	    				tempUser +="<tr><td>"+(i+1)+"</td><td>"+userMap.get("COMPANY_NM")+"</td><td>"+userMap.get("USER_NM")+"</td><td>"+userMap.get("POSITION_NM")+"</td></tr>";
	    				//PolicySet
	    				String policy =userMap.get("POLICY").toString();
	    				if("1".equals(policy.substring(0,1))) {
	    					bodyContext = bodyContext.replace("CHKNETWORK", "checked");
	    				}else{
	    					bodyContext = bodyContext.replace("CHKNETWORK", "");
	    				}
	    				String policyDoc = userMap.get("POLICY_DOC").toString();
	    				if("1".equals(policyDoc.substring(1,2))) {
	    					bodyContext = bodyContext.replace("CHKPRINT", "checked");
	    				} else {
	    					bodyContext = bodyContext.replace("CHKPRINT", "");
	    				}
	    				if("1".equals(policyDoc.substring(2,3))) {
	    					bodyContext = bodyContext.replace("CHKCLIPCOPY", "checked");
	    				} else {
	    					bodyContext = bodyContext.replace("CHKCLIPCOPY", "");
	    				}
	    				if("1".equals(policyDoc.substring(3,4))) {
	    					bodyContext = bodyContext.replace("CHKREPACKING", "checked");
	    				} else {
	    					bodyContext = bodyContext.replace("CHKREPACKING", "");
	    				}
	    				if("1".equals(policyDoc.substring(4,5))) {
	    					bodyContext = bodyContext.replace("CHKEXTCOPY", "checked");
	    				} else {
	    					bodyContext = bodyContext.replace("CHKEXTCOPY", "");
	    				}
	    				if("1".equals(policy.substring(1,2))){
	    					//log.info("EXPIRE_DATE-------------");
	    					bodyContext = bodyContext.replace("EXPIRE_DATE", userMap.get("EXPIRE_DT").toString()+" "+mm.getMessage("MAIL_1042", locale));
	    					//log.info("EXPIRE_DATE-------------"+ user.get("EXPIRE_DT").toString());
	    				}else{
	    					bodyContext = bodyContext.replace("EXPIRE_DATE", mm.getMessage("MAIL_1044", locale));
	    				}
	    				if("1".equals(policy.substring(2,3))) {
	    					//log.info("READ_CNT-------------");
	    					bodyContext = bodyContext.replace("READ_CNT", userMap.get("READ_COUNT").toString());
	    					//log.info("READ_CNT-------------"+user.get("READ_COUNT").toString());
	    				}else{
	    					bodyContext = bodyContext.replace("READ_CNT", mm.getMessage("MAIL_1044", locale));
	    				}
	    				
	    			}
	    			bodyContext = bodyContext.replace("RECIVER_USER",tempUser);
	    			// bodyContext = bodyContext.replace("DATE", "");
	    			
	    		}
	    		//log.info("2----------------------------------------------------------------------------------------saveRequest_ILJIN");
	    		//첨부파일 리스트
	       		Map docMap = new HashMap();
	       		docMap.put("j_file_program", "regMgmt");
	       		docMap.put("j_file_docId", docNo);
	       		
	       		
	       		List lists = sm.getList("app.upload.getUploads", docMap);
	       		
	       		String fileList = "<tr><td style='border:1px solid black;' rowspan='"+lists.size()+"' align='center'>"+ mm.getMessage("MAIL_1043", locale) +"</td>";
	       		for(int i=0; i<lists.size(); i++) {
	       			Map fileMap = (Map)lists.get(i);
	       			String fileNm = (String)fileMap.get("FILE_NAME");
	       			
	       			if(i==0){
	    				//fileList += "<td>"+downTag+"</td>";	
	    				fileList += "<td>"+fileNm+"</td>";
	    			}else{
	    				//fileList +="<tr><td>"+downTag+"</td></tr>";
	    				fileList +="<tr><td>"+fileNm+"</td></tr>";
	    			}
	       		}
	    	
	    		bodyContext = bodyContext.replace("FILELIST", fileList);
	    		bodyContext = bodyContext.replace("HEADER", mm.getMessage("MAIL_1020", locale));
	    		bodyContext = bodyContext.replace("NEXT", mm.getMessage("MAIL_1021", locale));
	    		bodyContext = bodyContext.replace("COMPANY_LABEL1", mm.getMessage("MAIL_1022", locale));
	    		bodyContext = bodyContext.replace("DEPT_LABEL", mm.getMessage("MAIL_1023", locale));
	    		bodyContext = bodyContext.replace("USER_LABEL", mm.getMessage("MAIL_1024", locale));
	    		bodyContext = bodyContext.replace("MSG_LABEL", mm.getMessage("MAIL_1025", locale));
	    		bodyContext = bodyContext.replace("EXPORT_LABEL", mm.getMessage("MAIL_1026", locale));
	    		bodyContext = bodyContext.replace("RECIPIENT_LABEL1", mm.getMessage("MAIL_1027", locale));
	    		bodyContext = bodyContext.replace("EXPORT_MEDIUM", mm.getMessage("MAIL_1028", locale));
	    		bodyContext = bodyContext.replace("EXPORT_POLICY", mm.getMessage("MAIL_1029", locale));
	    		bodyContext = bodyContext.replace("VALIDITY", mm.getMessage("MAIL_1030", locale));
	    		bodyContext = bodyContext.replace("AVAILABLE_COUNT", mm.getMessage("MAIL_1031", locale));
	    		bodyContext = bodyContext.replace("COMPANY_LABEL2", mm.getMessage("MAIL_1032", locale));
	    		bodyContext = bodyContext.replace("RECIPIENT_LABEL2", mm.getMessage("MAIL_1033", locale));
	    		bodyContext = bodyContext.replace("POSITION_LABEL", mm.getMessage("MAIL_1034", locale));
	    		bodyContext = bodyContext.replace("INTERNET_ACCESS", mm.getMessage("MAIL_1035", locale));
	    		bodyContext = bodyContext.replace("DISABLING_MOVE_COPY", mm.getMessage("MAIL_1036", locale));
	    		bodyContext = bodyContext.replace("DISABLING_PRINT", mm.getMessage("MAIL_1037", locale));
	    		bodyContext = bodyContext.replace("DISABLING_CLIPBOARD", mm.getMessage("MAIL_1038", locale));
	    		bodyContext = bodyContext.replace("DISABLING_REPACKING", mm.getMessage("MAIL_1039", locale));
	    		
	    		//strBuf.append(bodyContext);
	    		//log.info("bodyContext :: " + bodyContext);
	    		//log.info("3----------------------------------------------------------------------------------------saveRequest_ILJIN");
	    		
	    		log.info("4------------- 일진글로벌 결재 연동 테이블 값 삽입 : " + itm.get("DOC_NO").toString());
	    		
	    		Map iljinMap = new HashMap();
	    		iljinMap.put("SN", itm.get("DOC_NO").toString());
	    		iljinMap.put("DOC_TYPE", "108");
	    		iljinMap.put("TITLE", itm.get("DOC_TITLE").toString());
	    		iljinMap.put("STATUS", "000");
	    		iljinMap.put("CONTENT", bodyContext);
	    		iljinMap.put("CHANGE_YN", "N");
	    		
	    		sm.createObject("mg.site.tempInsertIljinDoc", iljinMap);
	       		log.info("TempIljinDoc 등록 완료");
	       		sm.createObject("mg.site.insertIljinDoc", iljinMap);
	       		log.info("IljinDoc 등록 완료");
	       		sm.deleteObject("mg.site.deleteTempIfDOc", iljinMap);
	       		log.info("TempIljinDoc 내용 삭제");
	       		
	       		//첨부파일 업로드	       		
	       		for(int i=0; i<lists.size(); i++) {
	       			Map fileMap = (Map)lists.get(i);
	       			String fileNm = (String)fileMap.get("FILE_NAME");
	       			log.info("일진글로벌 첨부파일 업로드 : " + fileNm);
	       			//파일 추출
		       		String filePath = pm.getString("component.upload.directory") + fileMap.get("FILE_PATH") +"\\"+ fileNm;
		       		FileInputStream fileInputStream=null;
		       		
		       		File file = new File(filePath);
		       		
		       		byte[] bFile = new byte[(int) file.length()];
		       		
		       		try {
			            //convert file into array of bytes
					    fileInputStream = new FileInputStream(file);
					    fileInputStream.read(bFile);
					    fileInputStream.close();
			        }catch(Exception e){
			        	log.error("------------일진글로벌 결재연동 실패---------------------------------------------------\n" + e.toString());
						return "0";
			        }
		       		
		       		//FILE_PATH 생성
		       		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
		       		String index;
		       		if(i<10) {
		       			index = "0"+Integer.toString(i+1);
		       		} else {
		       			index = Integer.toString(i+1);
		       		}
		       		String newFileNm = formatter.format(new Date()) + index + "_" + fileNm;
		       		
		       		Map iljinAttach = new HashMap();
	       			iljinAttach.put("DOC_SN", docNo);
	       			iljinAttach.put("FILE_PATH", newFileNm);
		       		iljinAttach.put("CONTENT", bFile);
		       		iljinAttach.put("FILE_SIZE", fileMap.get("FILE_SIZE"));
		       		sm.createObject("mg.site.insertIljinAttach", iljinAttach);
	       		}
	    	}
	    	return "1";
		} catch(Exception e) {
			log.error("------------일진글로벌 결재연동 실패---------------------------------------------------\n" + e.toString());
			return "0";
		}
	}
}
