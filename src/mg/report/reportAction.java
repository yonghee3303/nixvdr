package mg.report;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.message.IMessageManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.util.DateUtils;

public class reportAction  extends BaseAction {	
	
	/**
     * 반출신청 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("selectPcInfo")) {   /** Agnet 설치 현황 조회 */
            return selectPcInfo(request);                           
        } else  if(request.isMode("excelPcInfo")) {   /** Agnet 설치 현황 ExcelDownload */
            return excelPcInfo(request);                           
        } else if(request.isMode("pcInfoLog")) {   /** Agent  설치 로그 조회 */
            return selectPcInfoLog(request);                           
        } else if(request.isMode("excelPcInfoLog")) {   /** Agent  설치 로그 ExcelDownload */
            return excelPcInfoLog(request);                           
        } else if(request.isMode("getPcAccessDt")) {   /** Agent 최근 접속일 조회 */
            return getPcAccessDt(request);                           
        } else if(request.isMode("selectFileIO")) {   /** File IO 로그조회 */
            return selectFileIO(request);                           
        }  else if(request.isMode("excelFileIO")) {   /** File IO 로그 Excel Download*/
            return excelFileIO(request);                           
        }  else if(request.isMode("selectSummeryList")) {   /** File IO 요약조회 */
            return selectSummeryList(request);                           
        }  else if(request.isMode("excelSummeryList")) {   /** File IO 요약 Excel Download*/
            return excelSummeryList(request);                           
        }  else if(request.isMode("selectPrint")) {   /** Printer 로그조회 */
            return selectPrint(request);                           
        }  else if(request.isMode("excelPrint")) {   /** Printer 로그 Excel Download*/
            return excelPrint(request);                           
        } else if(request.isMode("selectAuth")) {   /** 부서별정책설정 조회 */
            return selectAuth(request);                           
        }  else if(request.isMode("excelAuth")) {   /** 부서별정책설정 Excel Download */
            return excelAuth(request);                           
        } else if(request.isMode("execRD_USERAUTH")) {   /** 부서별/개인별 정책 수집*/
            return execRD_USERAUTH(request);                           
        } else if(request.isMode("selectUserAuth")) {   /** 부서별정책설정 사용자*/
            return selectUserAuth(request);                           
        } else if(request.isMode("excelUserAuth")) {   /** 부서별정책설정 사용자 Excel Download*/
            return excelUserAuth(request);                           
        } else if(request.isMode("getAuthCode")) {   /** 정책코드조회*/
            return getAuthCode(request);                           
        } else if(request.isMode("getAuthPart")) {   /** 정책그룹 코드조회*/
            return getAuthPart(request);                           
        } else if(request.isMode("selectUsageAuth")) {   /** 사용자별 정책 조회 */
            return selectUsageAuth(request);                           
        }  else if(request.isMode("excelUsageAuth")) {   /**  정책적용현황 Excel Download */
            return excelUsageAuth(request);                           
        } else if(request.isMode("selectMedia")) {   /** 외장장치 사용로그 조회 */
            return selectMedia(request);                           
        }  else if(request.isMode("excelMedia")) {   /**  외장장치 로그 Excel Download */
            return excelMedia(request);                           
        } else if(request.isMode("selectQuota")) {   /** 디스크 할당(Quota) 조회 */
            return selectQuota(request);                           
        }  else if(request.isMode("excelQuota")) {   /**  디스크 할당(Quota) Excel Download */
            return excelQuota(request);                           
        }   else if(request.isMode("selectAuthHistory")) {   /**  적책정용 History 조회 */
            return selectAuthHistory(request);                           
        } else if(request.isMode("excelAuthHistory")) {   /** 정책적용 Excel Download */
            return excelAuthHistory(request);                           
        } else if(request.isMode("test")) {   /** 테스트 호출 */
            return TestCall(request);                           
        } else { 
            return write(null);
        }
    } 
    
    private IResponse TestCall(IRequest request) throws Exception 
    {
  	    try {
  	    	log.info("------------TestCall-------request.getUser().getLocale()--------\n");
  	    	 Map smap = request.getMap();
 	         smap.put("dbname", "MyGuard_V341");

 	        log.info("------------TestCall--------\n" + smap.toString());
  	    	Map map  = (Map)getItem("mg.report.getPcInfoLastDt",smap);
  	    	
			return write("Last Time : " + this.getStringWithNullCheck(map, "INSTALLTIME"));
      } catch(Exception e) {
   	    log.error("---------------------------------------------------------------\n" + e.toString());
     	 	return write("Test 호출 실패");
      }
    }
    
    /**
     * Agnet 설치 현황 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectPcInfo(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");

       	  Map map = null;
          GridData gdRes = new GridData();
       	 /*
    	  AND A.ISUNINSTALL  <![CDATA[<>]]> 'Y' 
    	  AND CONVERT(date, SUBSTRING(A.LASTACCESSTIME,1,8),112) <![CDATA[>=]]> DATEADD(MONTH, -1, GETDATE())
          */
       	  try {
       		  // String isDate = request.getStringParam("isDate");
          	  // String formDt = request.getStringParam("formDt");
          	  String toDt = request.getStringParam("toDt");
          	  String searchClCd = request.getStringParam("SEARCH_CL_CD");
          	  String curDT = "";
          	  
 	          Map smap = request.getMap();
 	          smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 	          smap.put("DB", "myguard");
 	          /*
 	          if("Y".equals(isDate)) { // 구간설정이 없으면 최신데이터 조회한다.
 	        	  curDT = toDt.replace("-","");
 	          } else {
 	   		  	 curDT = DateUtils.getCurrentDate("yyyyMMdd"); 
 	          }
 	          */
 	          
 	          curDT = toDt.replace("-","");
 	          smap.put("curDT", curDT);  // 조회일 지정
 	          
        	  if("M".equals(searchClCd)) {  // 월 단위 조회
        		 smap.put("dateLenth", 6);
        		 smap.put("COL1", curDT.substring(0,6));
        		 smap.put("COL2", DateUtils.addMonth(curDT, -1).subSequence(0, 6));
        		 smap.put("COL3", DateUtils.addMonth(curDT, -2).subSequence(0, 6));
        		 smap.put("COL4", DateUtils.addMonth(curDT, -3).subSequence(0, 6));
        		 smap.put("COL5", DateUtils.addMonth(curDT, -4).subSequence(0, 6));
        		 smap.put("COL6", DateUtils.addMonth(curDT, -5).subSequence(0, 6));
        		 smap.put("COL7", DateUtils.addMonth(curDT, -6).subSequence(0, 6));
        	  } else {
        		 smap.put("dateLenth", 8);
        		 smap.put("COL1", curDT);
        		 smap.put("COL2", DateUtils.addDay(curDT, -1));
        		 smap.put("COL3", DateUtils.addDay(curDT, -2));
        		 smap.put("COL4", DateUtils.addDay(curDT, -3));
        		 smap.put("COL5", DateUtils.addDay(curDT, -4));
        		 smap.put("COL6", DateUtils.addDay(curDT, -5));
        		 smap.put("COL7", DateUtils.addDay(curDT, -6));
        	  }
 	          
        	  log.info("---selectPcInfo----------------------------------------------------------\n" + smap.toString());
 	          	          
 	          List lists; 
 	          String dbName = pm.getString("component.sql.database");
 	        
 	          if("mssql".equals(dbName)){
 	        	  lists = getList("mg.report.selectPcInfo",smap);
 	          }else{
 	        	  lists = getList("mg.report.selectPcInfo_"+dbName, smap);
 	          }
 	          for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	 
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	          }
 	          return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	  log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
          	  return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
     }
     
     /**
      * Agnet 설치 현황 ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelPcInfo(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
         String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
		     // Excel Header 설정
		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
             IExcelDocument doc = excel.createDocument("Agnet 설치 현황");
       		 int[] col_width = new int[col.length()];
             for(int i=0; i< cols.length; i++) {
            	 doc.addTitleCell(headers[i]);
            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
             }
             doc.setColumnsWidth(col_width);

              // String isDate = request.getStringParam("isDate");
	       	  // String formDt = request.getStringParam("formDt");
	       	  String toDt = request.getStringParam("toDt");
	       	  String searchClCd = request.getStringParam("SEARCH_CL_CD");
	       	  String curDT = "";
	       	  
	          Map smap = request.getMap();
	          smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	          /*
	          if("Y".equals(isDate)) { // 구간설정이 없으면 최신데이터 조회한다.
	        	  curDT = toDt.replace("-","");
	          } else {
	   		  	  curDT = DateUtils.getCurrentDate("yyyyMMdd");
	          }
	          */
	          curDT = toDt.replace("-","");
	          smap.put("curDT", curDT);  // 조회일 지정
		          
	     	  if("M".equals(searchClCd)) {  // 월 단위 조회
	     		 smap.put("dateLenth", 6);
	     		 smap.put("COL1", curDT.substring(0,6));
	     		 smap.put("COL2", DateUtils.addMonth(curDT, -1).subSequence(0, 6));
	     		 smap.put("COL3", DateUtils.addMonth(curDT, -2).subSequence(0, 6));
	     		 smap.put("COL4", DateUtils.addMonth(curDT, -3).subSequence(0, 6));
	     		 smap.put("COL5", DateUtils.addMonth(curDT, -4).subSequence(0, 6));
	     		 smap.put("COL6", DateUtils.addMonth(curDT, -5).subSequence(0, 6));
	     		 smap.put("COL7", DateUtils.addMonth(curDT, -6).subSequence(0, 6));
	     	  } else {
	     		 smap.put("dateLenth", 8);
	     		 smap.put("COL1", curDT);
	     		 smap.put("COL2", DateUtils.addDay(curDT, -1));
	     		 smap.put("COL3", DateUtils.addDay(curDT, -2));
	     		 smap.put("COL4", DateUtils.addDay(curDT, -3));
	     		 smap.put("COL5", DateUtils.addDay(curDT, -4));
	     		 smap.put("COL6", DateUtils.addDay(curDT, -5));
	     		 smap.put("COL7", DateUtils.addDay(curDT, -6));
	     	  }
  	         String dbName = pm.getString("component.sql.database");  	         
	         List lists;
	         if("mssql".equals(dbName)){
	        	 lists= getList("mg.report.selectPcInfo",smap);
	         }else{
	        	 lists= getList("mg.report.selectPcInfo_"+dbName,smap);
	         }
  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Agnet 설치 현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("-excelPcInfo--엑셀 파일 생성중 오류 발생 되었습니다---------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. 관리자에게 문의 바랍니다.");
        }
     }
     
     /**
      * Agnet 설치 로그 조회 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse selectPcInfoLog(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
    	  String[] cols = col.split(",");
    	  
    	  Map map = null;
          GridData gdRes = new GridData();

    	  try {
    		 String dbName = pm.getString("component.sql.database");
    		 String toDt = request.getStringParam("toDt");
  	         Map smap = request.getMap();
  	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
  	         smap.put("curDT", toDt.replace("-",""));  // 조회일 지정
  	         List lists;
  	         if("mssql".equals(dbName)){
  	        	lists = getList("mg.report.selectPcInfoLog",smap);
  	         }else{
  	        	lists = getList("mg.report.selectPcInfoLog_"+dbName,smap);
  	         }
  	         for (int i = 0; i < lists.size(); i++) {
  	          	map =(Map) lists.get(i);
  	          	 
  	          	for(int j=0; j < cols.length; j++ ) {
  	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	          	}
  	         }
  	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
     }
      
      /**
       * PC Info Log ExcelDownload
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse excelPcInfoLog(IRequest request) throws Exception 
       {
     	  String col = request.getStringParam("grid_col_id");
          String[] cols = col.split(",");
     	  String width = request.getStringParam("grid_col_width");
     	  String[] widths = width.split(",");
     	  String header = request.getStringParam("grid_col_header");
     	  String[] headers = header.split(",");

     	  try {
     		     // Excel Header 설정
     		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Agent 설치 Log");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
	     
   	         Map smap = request.getMap();
   	         String toDt = request.getStringParam("toDt");
   	         smap.put("curDT", toDt.replace("-",""));  // 조회일 지정
   	         String dbName = pm.getString("component.sql.database");
   	    
	         List lists;
	         if("mssql".equals(dbName)){
	        	 lists= getList("mg.report.selectPcInfoLog",smap);
	         }else{
	        	 lists= getList("mg.report.selectPcInfoLog_"+dbName,smap);
	         }
   	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
   	         return doc.download(request, "Agent 설치 Log_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
         } catch(Exception e) {
        	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
        	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. 관리자에게 문의 바랍니다.");
         }
      }
            
      
      /**
       * CP 문서정보 Return 
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse getPcAccessDt(IRequest request) throws Exception 
       {   
     	 try {
     		 Map smap = request.getMap();
     		 String dbName = pm.getString("component.sql.database");
     		 
     		 smap.put("DB", "MyGuard.dbo.TB_PCInfo");
     		 Map map;
     		 if("mssql".equals(dbName)){
     			 map  = (Map)getItem("mg.report.getPcInfoLastDt", smap);
     		 }else{
     			 map = (Map)getItem("mg.report.getPcInfoLastDt_"+dbName,smap);
     		 }
     		 // Creation of Json Response 
     		 JSONObject obj = new JSONObject();
     		 obj.put("TOT_CNT", this.getStringWithNullCheck(map, "TOT_CNT"));
     		 obj.put("INSTALLTIME", this.getStringWithNullCheck(map, "INSTALLTIME"));
     		
     		return write(obj.toString());
          } catch(Exception e) {
        	 	log.error("---PC 최근 접속일 조회-------------------------\n" + e.toString());
        	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
          }
       }
       
       
       /**
        * File IO 로그 조회
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse selectFileIO(IRequest request) throws Exception 
        {
      	  String col = request.getStringParam("grid_col_id");
      	  String[] cols = col.split(",");
      	  
      	  Map map = null;
            GridData gdRes = new GridData();

      	  try {
    	         Map smap = request.getMap();
    	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
    	         String dbName = pm.getString("component.sql.database");
    	        
    	         List lists;
    	         if("mssql".equals(dbName)){
    	         	 lists = getList("mg.report.selectFileIO",smap);
    	         }else{
    	        	 lists = getList("mg.report.selectFileIO_"+dbName, smap);
    	         }
    	         for (int i = 0; i < lists.size(); i++) {
    	          	map =(Map) lists.get(i);
    	          	 
    	          	for(int j=0; j < cols.length; j++ ) {
    	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
    	          	}
    	         }
    	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
         	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
       }
        
        /**
         * File IO 로그 ExcelDownload
         * @param request
         * @return
         * @throws Exception
         */
         private IResponse excelFileIO(IRequest request) throws Exception 
         {
       	  String col = request.getStringParam("grid_col_id");
          String[] cols = col.split(",");
       	  String width = request.getStringParam("grid_col_width");
       	  String[] widths = width.split(",");
       	  String header = request.getStringParam("grid_col_header");
       	  String[] headers = header.split(",");

       	  try {
       		     // Excel Header 설정
       		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("File IO Log");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
	     
     	         Map smap = request.getMap();
     	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
     	        String dbName = pm.getString("component.sql.database");
    	        
   	       		List lists;
   	       		if("mssql".equals(dbName)){
   	       			lists = getList("mg.report.selectFileIO",smap);
   	       		}else{
   	       			lists = getList("mg.report.selectFileIO_"+dbName, smap);
   	       		}
     	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
     	         return doc.download(request, "File IO Log_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
           } catch(Exception e) {
          	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
          	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
           }
        }
         
         
         /**
          * File IO 요약 조회
          * @param request
          * @return
          * @throws Exception
          */
          private IResponse selectSummeryList(IRequest request) throws Exception 
          {
        	  String col = request.getStringParam("grid_col_id");
        	  String[] cols = col.split(",");
        	  
        	  Map map = null;
              GridData gdRes = new GridData();

        	  try {
        		  String dbName = pm.getString("component.sql.database");
        		  Map smap = request.getMap();
     	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
      	         List lists;
      	         if("mssql".equals(dbName)){
      	        	 lists = getList("mg.report.selectSummeryList", smap);
      	         }else{
      	        	 
      	        	 lists = getList("mg.report.selectSummeryList_"+dbName, smap);
      	         }
      	         for (int i = 0; i < lists.size(); i++) {
      	          	map =(Map) lists.get(i);
      	          	 
      	          	for(int j=0; j < cols.length; j++ ) {
      	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
      	          	}
      	         }
      	         log.info("----------selectSummeryList---------------------->\n" +map.toString() );
      	       
      	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
            } catch(Exception e) {
           	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
           	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
            }
         }
         
          
          /**
           * File IO 요약 ExcelDownload
           * @param request
           * @return
           * @throws Exception
           */
           private IResponse excelSummeryList(IRequest request) throws Exception 
           {
         	  String col = request.getStringParam("grid_col_id");
              String[] cols = col.split(",");
         	  String width = request.getStringParam("grid_col_width");
         	  String[] widths = width.split(",");
         	  String header = request.getStringParam("grid_col_header");
         	  String[] headers = header.split(",");

         	  try {
         		 // Excel Header 설정
         		 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
  	             IExcelDocument doc = excel.createDocument("File IO SummeryList Log");
  	       		 int[] col_width = new int[col.length()];
  	             for(int i=0; i< cols.length; i++) {
  	            	 doc.addTitleCell(headers[i]);
  	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
  	             }
  	             doc.setColumnsWidth(col_width);
  	     
       	         Map smap = request.getMap();
       	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
       	         String dbName = pm.getString("component.sql.database");
       	         List lists;
       	         if("mssql".equals(dbName)){
       	        	 lists = getList("mg.report.selectSummeryList",smap);
       	         }else{
       	        	lists = getList("mg.report.selectSummeryList_"+dbName,smap);
       	         }
       	         for (int i=0; i < lists.size(); i++) {
  	 	         	Map map =(Map) lists.get(i);
  	 	         	doc.nextRow();
  	 	         	
  	 	         	for(int j=0; j < cols.length; j++) {
  	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
  	 	         	}
  	 	         }
       	         return doc.download(request, "File IO SummeryList Log_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
             } catch(Exception e) {
            	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
            	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
             }
          }
           

          
         /**
          * Print 로그 조회
          * @param request
          * @return
          * @throws Exception
          */
          private IResponse selectPrint(IRequest request) throws Exception 
          {
        	  String col = request.getStringParam("grid_col_id");
        	  String[] cols = col.split(",");
        	  
        	  Map map = null;
              GridData gdRes = new GridData();

        	  try {
      	         Map smap = request.getMap();
      	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
      	         String dbName = pm.getString("component.sql.database");
      	         List lists;
      	         if("mssql".equals(dbName)){
      	        	 lists= getList("mg.report.selectPrint",smap);
      	         }else{
      	        	lists= getList("mg.report.selectPrint_"+dbName,smap);
      	         }
      	         for (int i = 0; i < lists.size(); i++) {
      	          	map =(Map) lists.get(i);
      	          	 
      	          	for(int j=0; j < cols.length; j++ ) {
      	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
      	          	}
      	         }
      	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
            } catch(Exception e) {
           	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
           	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
            }
         }
          
          /**
           * File IO 로그 ExcelDownload
           * @param request
           * @return
           * @throws Exception
           */
           private IResponse excelPrint(IRequest request) throws Exception 
           {
         	  String col = request.getStringParam("grid_col_id");
            String[] cols = col.split(",");
         	  String width = request.getStringParam("grid_col_width");
         	  String[] widths = width.split(",");
         	  String header = request.getStringParam("grid_col_header");
         	  String[] headers = header.split(",");

         	  try {
         		     // Excel Header 설정
         		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
  	             IExcelDocument doc = excel.createDocument("Print Log");
  	       		 int[] col_width = new int[col.length()];
  	             for(int i=0; i< cols.length; i++) {
  	            	 doc.addTitleCell(headers[i]);
  	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
  	             }
  	             doc.setColumnsWidth(col_width);
  	     
       	         Map smap = request.getMap();
       	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
       	         String dbName = pm.getString("component.sql.database");
       	         
       	         List lists;
       	         if("mssql".equals(dbName)){
       	        	 lists= getList("mg.report.selectPrint",smap);
       	         }else{
       	        	lists= getList("mg.report.selectPrint_"+dbName,smap);
       	         }
       	         for (int i=0; i < lists.size(); i++) {
  	 	         	Map map =(Map) lists.get(i);
  	 	         	doc.nextRow();
  	 	         	
  	 	         	for(int j=0; j < cols.length; j++) {
  	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
  	 	         	}
  	 	         }
       	         return doc.download(request, "Print Log_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
             } catch(Exception e) {
            	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
            	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
             }
          }
           
           /**
            * 부서별 정책설정 현황 조회
            * @param request
            * @return
            * @throws Exception
            */
            private IResponse selectAuth(IRequest request) throws Exception 
            {
          	  String col = request.getStringParam("grid_col_id");
          	  String[] cols = col.split(",");
          	  
          	  Map map = null;
                GridData gdRes = new GridData();
                String dbName = pm.getString("component.sql.database");
                List lists;

          	  try {
        	         
          		  if("mssql".equals(dbName)){
          			  lists = getList("mg.report.selectAuth",request.getMap());
          		  }else{
          			  lists = getList("mg.report.selectAuth_"+dbName, request.getMap());
          		  }
          		  for (int i = 0; i < lists.size(); i++) {
        	          	map =(Map) lists.get(i);
        	          	 
        	          	for(int j=0; j < cols.length; j++ ) {
        	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
        	          	}
        	         }
        	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
              } catch(Exception e) {
             	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
             	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
              }
           }
            
            /**
             * 부서별 정책설정 현황 ExcelDownload
             * @param request
             * @return
             * @throws Exception
             */
             private IResponse excelAuth(IRequest request) throws Exception 
             {
           	  String col = request.getStringParam("grid_col_id");
              String[] cols = col.split(",");
           	  String width = request.getStringParam("grid_col_width");
           	  String[] widths = width.split(",");
           	  String header = request.getStringParam("grid_col_header");
           	  String[] headers = header.split(",");

           	  try {
           		     // Excel Header 설정
           		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
    	             IExcelDocument doc = excel.createDocument("부서별 정책설정 현황");
    	       		 int[] col_width = new int[col.length()];
    	             for(int i=0; i< cols.length; i++) {
    	            	 doc.addTitleCell(headers[i]);
    	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
    	             }
    	             doc.setColumnsWidth(col_width);
    	     
         	         Map smap = request.getMap();
         	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
         	         String dbName = pm.getString("component.sql.database");
         	         List lists;
         	         if("mssql".equals(dbName)){
         	        	 lists= getList("mg.report.selectAuth",smap);
         	         }else{
         	        	lists= getList("mg.report.selectAuth_"+dbName,smap);
         	         }
         	         for (int i=0; i < lists.size(); i++) {
    	 	         	Map map =(Map) lists.get(i);
    	 	         	doc.nextRow();
    	 	         	
    	 	         	for(int j=0; j < cols.length; j++) {
    	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
    	 	         	}
    	 	         }
         	         return doc.download(request, "부서별 정책설정 현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
               } catch(Exception e) {
              	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
              	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
               }
            }
           
            /**
             * 부서별 정책설정 사용자 조회
             * @param request
             * @return
             * @throws Exception
             */
             private IResponse selectUserAuth(IRequest request) throws Exception 
             {
           	  	String col = request.getStringParam("grid_col_id");
           	  	String[] cols = col.split(",");
           	  
           	  	Map map = null;
                 GridData gdRes = new GridData();

                try {         	         
           		  	Map smap = request.getMap();
           		  	smap.put("LOCALE", request.getUser().getLocale().getLanguage());
           		  	String dbName = pm.getString("component.sql.database");
           		  	
         	         List lists;
         	         if("mssql".equals(dbName)){
         	        	 lists= getList("mg.report.selectUserAuth",smap);
         	         }else{
         	        	 lists=getList("mg.report.selectUserAuth_"+dbName,smap);
         	         }
         	         for (int i = 0; i < lists.size(); i++) {
         	          	map =(Map) lists.get(i);
         	          	 
         	          	for(int j=0; j < cols.length; j++ ) {
         	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
         	          	}
         	         }
         	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
               } catch(Exception e) {
              	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
              	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
               }
            }
             
             /**
              * 부서별 정책설정 사용자 조회 ExcelDownload
              * @param request
              * @return
              * @throws Exception
              */
              private IResponse excelUserAuth(IRequest request) throws Exception 
              {
            	  String col = request.getStringParam("grid_col_id");
                  String[] cols = col.split(",");
            	  String width = request.getStringParam("grid_col_width");
            	  String[] widths = width.split(",");
            	  String header = request.getStringParam("grid_col_header");
            	  String[] headers = header.split(",");

            	  try {
            		     // Excel Header 설정
            		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	     	             IExcelDocument doc = excel.createDocument("부서별 정책설정 사용자 조회");
	     	       		 int[] col_width = new int[col.length()];
	     	             for(int i=0; i< cols.length; i++) {
	     	            	 doc.addTitleCell(headers[i]);
	     	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	     	             }
	     	             doc.setColumnsWidth(col_width);
	     	     
	          	         Map smap = request.getMap();
	          	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	          	         
	          	         List lists = getList("mg.report.selectUserAuth",smap);
	          	         for (int i=0; i < lists.size(); i++) {
	     	 	         	Map map =(Map) lists.get(i);
	     	 	         	doc.nextRow();
	     	 	         	
	     	 	         	for(int j=0; j < cols.length; j++) {
	     	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	     	 	         	}
	     	 	         }
	          	         return doc.download(request, "부서별 정책설정 사용자 조회_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
	                } catch(Exception e) {
	               	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	               	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
	                }
             }
             
             /**
              * 정책코드 조회 
              * @param request
              * @return
              * @throws Exception
              */
              private IResponse getAuthCode(IRequest request) throws Exception 
              {           	  
            	  try {
	            	    String tagname = request.getStringParam("tagname");
	              	    String default_flag = request.getStringParam("default_flag");
	              	    String authPart = request.getStringParam("AUTHPART");
	              	
	              	    String dbName = pm.getString("component.sql.database");
	              	    
	            	    List lists;
	            	    if("mssql".equals(dbName)){
	            	    	lists= getList("mg.report.getAuthCode",request.getMap());
	            	    }else{
	            	    	lists= getList("mg.report.getAuthCode_"+dbName,request.getMap());
	            	    }
	            	    StringBuffer buffer = new StringBuffer();
	
	                    if(null != authPart || "".equals(authPart)) {
	                    	buffer.append(authPart);
	                    } else {
	                    	authPart = "";
	                    }
	                    
		                buffer.append("<select name=\"" + tagname + authPart + "\">");
		                if(!"".equals(default_flag) && default_flag.equals("Y")) { 
		                	buffer.append("<option value=''></option>");
		                }
		                Map map = null;
		
		                for (int i = 0; i < lists.size(); i++) {
		                	map =(Map) lists.get(i);
		                	
		                    buffer.append("<option value='" + map.get("AuthCode") + "'>" + map.get("AuthName") + "</option>");
		                    
		                    log.debug( "-------------------------------------------------------------------\n" + map.toString());
		                    
		                    // http://localhost:8091/ssms/Code.do?mod=model&tagname=ddd&default_flag=Y?PRODUCT_CD=USIM
		                }
	                
	                    buffer.append("</select>");
	                    return writeXml(buffer.toString());
            	  } catch(Exception ex) {
            		   log.error( "-------------------------------------------------------------------\n" + ex.getMessage());
            		   return writeXml("");
            	  }
             }
              
              /**
               * 정책그룹 조회 
               * @param request
               * @return
               * @throws Exception
               */
               private IResponse getAuthPart(IRequest request) throws Exception 
               {   
            	   JSONObject obj = new JSONObject();
            	   try {
            		   Map smap = request.getMap();
            		   smap.put("dbName", pm.getString("component.sql.database"));
	             	    List lists = getList("mg.report.getAuthPart", smap);
	                    StringBuffer buffer = new StringBuffer();
	 	                Map map = null;
	 	                // obj.put("cnt", lists.size()); // COMG_1003
	 	               
	 	                List<JSONObject> jsonCategories = new ArrayList<JSONObject>();
				        JSONObject jsonCategory = null;
				        
	 	                for (int i = 0; i < lists.size(); i++) {
	 	                	map =(Map) lists.get(i);
	 	                	jsonCategory = new JSONObject();
			 	          	jsonCategory.put("AUTHPART",  map.get("AUTHPART") );
			 	          	jsonCategory.put("PARTNAME", map.get("PARTNAME") );
	 		     			jsonCategories.add(jsonCategory);
	 	                }
	 	                obj.put("list", jsonCategories); 
	                    obj.put("errcode", "0");
	 			        obj.put("errmsg", "Success");	                    
            	   } catch(Exception e) {
            		    log.error("------------getAuthPart----------------------------------------------------------------\n" + e.toString());
   		                obj.put("errcode", "-1");
   			            obj.put("errmsg", "Failed getAuthPart.");
            	   }
            	   return write(obj.toString());
              }
              
              //mail.addTo(getItem("mg.cp.getApproverEmail", itm).toString());   // 상위자
              /**
               * 사용자별 적용권한 조회
               * @param request
               * @return
               * @throws Exception
               */
               private IResponse selectUsageAuth(IRequest request) throws Exception 
               {
             	  String col = request.getStringParam("grid_col_id");
             	  String[] cols = col.split(",");
             	  
             	  Map map = null;
                   GridData gdRes = new GridData();

             	  try {         	         
             		  Map smap = request.getMap();
             		  Map rmap= request.getMap();
             		  String dbName = pm.getString("component.sql.database");
             		  rmap.put("dbName",dbName);
             		  smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", rmap));
             		 // smap.put("dbName", pm.getString("component.sql.database"));
             		  log.info("---selectUsageAuth : TREE_CD----------------------------------------------------------\n" + smap.toString());

           	          List lists;
           	          if("mssql".equals(dbName)){
           	        	  lists= getList("mg.report.selectUsageAuth",smap);
           	          }else{
           	        	  lists= getList("mg.report.selectUsageAuth_"+dbName,smap);
           	          }
           	          for (int i = 0; i < lists.size(); i++) {
	           	          	map =(Map) lists.get(i);
	           	          	 
	           	          	for(int j=0; j < cols.length; j++ ) {
	           	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	           	          	}
           	         }
           	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
                 } catch(Exception e) {
                	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
                	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
                 }
              }
               
           /**
            * 정책적용현황 ExcelDownload
            * @param request
            * @return
            * @throws Exception
            */
            private IResponse excelUsageAuth(IRequest request) throws Exception 
            {
          	  String col = request.getStringParam("grid_col_id");
             String[] cols = col.split(",");
          	  String width = request.getStringParam("grid_col_width");
          	  String[] widths = width.split(",");
          	  String header = request.getStringParam("grid_col_header");
          	  String[] headers = header.split(",");

          	  try {
          		     // Excel Header 설정
          		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	   	             IExcelDocument doc = excel.createDocument("정책 적용 현황");
	   	       		 int[] col_width = new int[col.length()];
	   	             for(int i=0; i< cols.length; i++) {
	   	            	 doc.addTitleCell(headers[i]);
	   	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	   	             }
	   	             doc.setColumnsWidth(col_width);
   	     
        	         Map smap = request.getMap();
        	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
        	         smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
        	         String dbName =pm.getString("component.sql.database");
        	         
        	         List lists;
        	         if("mssql".equals(dbName)){
        	        	 lists= getList("mg.report.selectUsageAuth",smap);
        	         }else{
        	        	 lists= getList("mg.report.selectUsageAuth_"+dbName,smap);
        	         }
        	        for (int i=0; i < lists.size(); i++) {
   	 	         	Map map =(Map) lists.get(i);
   	 	         	doc.nextRow();
   	 	         	
   	 	         	for(int j=0; j < cols.length; j++) {
   	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
   	 	         	}
   	 	         }
        	         return doc.download(request, "정책적용현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
              } catch(Exception e) {
             	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
             	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
              }
           } 
               
            /**
             * 외장장치 사용로그 조회
             * @param request
             * @return
             * @throws Exception
             */
             private IResponse selectMedia(IRequest request) throws Exception 
             {
	           	  String col = request.getStringParam("grid_col_id");
	           	  String[] cols = col.split(",");
	           	  
	           	  Map map = null;
	              GridData gdRes = new GridData();
	
	           	  try {         	         
	           		  	Map smap = request.getMap();
	           		  	smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	           		    smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
	           		    
	           		 log.info("-selectMedia--smap-------------------------------------------------------\n" + smap.toString());
         	            List lists = getList("mg.report.selectMedia",smap);
         	            for (int i = 0; i < lists.size(); i++) {
	           	          	map =(Map) lists.get(i);
	           	          	 
	           	          	for(int j=0; j < cols.length; j++ ) {
	           	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	           	          	}
	           	         log.info("-selectMedia--map-------------------------------------------------------\n" + map.toString());
         	           }
         	           return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
	               } catch(Exception e) {
	              	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	              	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
	               }
            }
             
         /**
          * 외장장치 사용로그 ExcelDownload
          * @param request
          * @return
          * @throws Exception
          */
          private IResponse excelMedia(IRequest request) throws Exception 
          {
        	  String col = request.getStringParam("grid_col_id");
        	  String[] cols = col.split(",");
        	  String width = request.getStringParam("grid_col_width");
        	  String[] widths = width.split(",");
        	  String header = request.getStringParam("grid_col_header");
        	  String[] headers = header.split(",");

        	  try {
        		     // Excel Header 설정
        		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	   	             IExcelDocument doc = excel.createDocument("외장장치 사용 현황");
	   	       		 int[] col_width = new int[col.length()];
	   	             for(int i=0; i< cols.length; i++) {
	   	            	 doc.addTitleCell(headers[i]);
	   	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	   	             }
	   	             doc.setColumnsWidth(col_width);
 	     
      	         Map smap = request.getMap();
      	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
      	         smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
      	         
      	         List lists = getList("mg.report.selectMedia",smap);
      	         for (int i=0; i < lists.size(); i++) {
 	 	         	Map map =(Map) lists.get(i);
 	 	         	doc.nextRow();
 	 	         	
 	 	         	for(int j=0; j < cols.length; j++) {
 	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
 	 	         	}
 	 	         }
      	         return doc.download(request, "외장장치 사용 현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
            } catch(Exception e) {
           	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
           	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
            }
         } 
          
          /**
           * 디스크 할당(Quota) 조회
           * @param request
           * @return
           * @throws Exception
           */
           private IResponse selectQuota(IRequest request) throws Exception 
           {
	           	  String col = request.getStringParam("grid_col_id");
	           	  String[] cols = col.split(",");
	           	  
	           	  Map map = null;
	              GridData gdRes = new GridData();
	
	           	  try {         	         
	           		  	Map smap = request.getMap();
	           		  	smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	           		    smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
	           		    
	           		 log.info("-selectMedia--smap-------------------------------------------------------\n" + smap.toString());
       	             List lists = getList("mg.report.selectQuota",smap);
       	             for (int i = 0; i < lists.size(); i++) {
	           	          	map =(Map) lists.get(i);
	           	          	 
	           	          	for(int j=0; j < cols.length; j++ ) {
	           	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	           	          	}
	           	         log.info("-selectMedia--map-------------------------------------------------------\n" + map.toString());
       	           }
       	           return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
	               } catch(Exception e) {
	              	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	              	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
	               }
          }
           
       /**
        * 디스크 할당(Quota) ExcelDownload
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse excelQuota(IRequest request) throws Exception 
        {
      	  String col = request.getStringParam("grid_col_id");
      	  String[] cols = col.split(",");
      	  String width = request.getStringParam("grid_col_width");
      	  String[] widths = width.split(",");
      	  String header = request.getStringParam("grid_col_header");
      	  String[] headers = header.split(",");

      	  try {
      		     // Excel Header 설정
      		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	   	             IExcelDocument doc = excel.createDocument(" 디스크 할당(Quota) 현황");
	   	       		 int[] col_width = new int[col.length()];
	   	             for(int i=0; i< cols.length; i++) {
	   	            	 doc.addTitleCell(headers[i]);
	   	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	   	             }
	   	             doc.setColumnsWidth(col_width);
	     
    	         Map smap = request.getMap();
    	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
    	         smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
    	         
    	         List lists = getList("mg.report.selectQuota",smap);
    	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
    	         return doc.download(request, "디스크 할당(Quota) 현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
          } catch(Exception e) {
         	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
         	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
          }
       } 
          
        /**
         * 개인별 정책 수집
         * @param request
         * @return
         * @throws Exception
         */
       private IResponse execRD_USERAUTH(IRequest request) {
     	   Map map = null;
     	   try {
     		 
     		   createObject("mg.report.execRD_USERAUTH", request.getMap());
     		   
	           return closePopupWindow( mm.getMessage("COMG_1057",  request.getUser().getLocale()));
     	   } catch(Exception e) {
           	   log.error("--------execRD_USERAUTH-----------------------------\n" + e.toString());
           	   return closePopupWindow( mm.getMessage("COMG_1058",  request.getUser().getLocale()));
            }
        }
       
       
       /**
        * (개인별) 정책적용 내역 조회 (개인별 History)
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse selectAuthHistory(IRequest request) throws Exception 
        {
          	  String col = request.getStringParam("grid_col_id");
          	  String[] cols = col.split(",");
          	  
          	  Map map = null;
             GridData gdRes = new GridData();

          	  try {         	         
          		  	Map smap = request.getMap();
          		  	String dbName = pm.getString("component.sql.database");
          		  	smap.put("dbName", dbName);
          		  	smap.put("LOCALE", request.getUser().getLocale().getLanguage());
          		    smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
          		    
          		    log.info("-selectMedia--smap-------------------------------------------------------\n" + smap.toString());
    	            List lists;
    	            if("mssql".equals(dbName)){
    	            	lists= getList("mg.report.selectAuthHistory",smap);
    	            }else{
    	            	lists= getList("mg.report.selectAuthHistory_"+dbName,smap);
    	            }
    	            for (int i = 0; i < lists.size(); i++) {
          	          	map =(Map) lists.get(i);
          	          	 
          	          	for(int j=0; j < cols.length; j++ ) {
          	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
          	          	}
          	         log.info("-selectMedia--map-------------------------------------------------------\n" + map.toString());
    	           }
    	           return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
              } catch(Exception e) {
             	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
             	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
              }
       }
        
    /**
     * (개인별) 정책적용 내역 ExcelDownload
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse excelAuthHistory(IRequest request) throws Exception 
     {
   	  String col = request.getStringParam("grid_col_id");
   	  String[] cols = col.split(",");
   	  String width = request.getStringParam("grid_col_width");
   	  String[] widths = width.split(",");
   	  String header = request.getStringParam("grid_col_header");
   	  String[] headers = header.split(",");

   	  try {
   		     // Excel Header 설정
   		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
  	             IExcelDocument doc = excel.createDocument("정책 적용 내역");
  	       		 int[] col_width = new int[col.length()];
  	             for(int i=0; i< cols.length; i++) {
  	            	 doc.addTitleCell(headers[i]);
  	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
  	             }
  	             doc.setColumnsWidth(col_width);
     
 	         Map smap = request.getMap();
 	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 	         smap.put("TREE_CD", getItem("mg.report.getDeptTreeCode", request.getMap()));
 	         String dbName = pm.getString("component.sql.database");
 	         
 	         List lists;
 	         if("mssql".equals(dbName)){
 	        	 lists= getList("mg.report.selectAuthHistory",smap);
 	         }else{
 	        	lists= getList("mg.report.selectAuthHistory_"+dbName,smap);
 	         }
 	         for (int i=0; i < lists.size(); i++) {
 	         	Map map =(Map) lists.get(i);
 	         	doc.nextRow();
 	         	
 	         	for(int j=0; j < cols.length; j++) {
 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
 	         	}
 	         }
 	         return doc.download(request, "정책적용 내역_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
       } catch(Exception e) {
      	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
      	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
       }
    } 
}
