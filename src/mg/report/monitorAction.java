package mg.report;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.security.MessageDigest;

import com.core.base.ComponentRegistry;
import com.core.component.mail.IMailManagement;
import com.core.component.message.IMessageManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import java.util.Hashtable;
import javax.naming.*;
import javax.naming.directory.*;

import mg.base.BaseAction;
import mg.base.GridData;

public class monitorAction  extends BaseAction{
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	/**
     * 반출신청 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
       if(request.isMode("test")) {   /** 테스트 호출 */
            return TestCall(request);                           
        } else if(request.isMode("crypto")) {   /** 테스트 호출 */
            return TestCrypto(request);                           
        } else if(request.isMode("Ldap")) {   /** Ldap 테스트 호출 */
            return TestLdap(request);                           
        } else { 
            return write(null);
        }
    } 
    
    private IResponse TestCall(IRequest request) throws Exception 
    {
  	    try {
  	    	log.error("------------TestCall-------request.getUser().getLocale()--------\n");
  	    	 Map smap = request.getMap();
 	         smap.put("dbname", "MyGuard");
  	    	
 	        log.error("------------TestCall--------\n" + smap.toString());
  	    	Map map  = (Map)getItem("mg.report.getPcInfoLastDt",smap);
  	    	
			return write("Last Time : " + this.getStringWithNullCheck(map, "INSTALLTIME"));
      } catch(Exception e) {
   	    log.error("---------------------------------------------------------------\n" + e.toString());
     	 	return write("Test 호출 실패");
      }
    }
    
    public String encryptString(String src) {
       try {
    	   MessageDigest md5 = MessageDigest.getInstance("MD5");
           String eip;
           String encodeVal = "";
           String tst = src;

           byte[] bip = md5.digest(tst.getBytes());
           for (int i = 0; i < bip.length; i++) {
               eip = "" + Integer.toHexString((int) bip[i] & 0x000000ff);
               if (eip.length() < 2) eip = "0" + eip;
               encodeVal = encodeVal + eip;
           }
           return encodeVal.toUpperCase();
       } catch (Exception e) {
           return "";
       }
    } 
    
    private IResponse TestCrypto(IRequest request) throws Exception 
    {
  	    try {
  	    	log.error("------------TestCall-------request.getUser().getLocale()--------\n");
  	    	 // String encodedValue = crypto.encode("MD5", request.getStringParam("text"));
  	    	String encodedValue = crypto.encode(request.getStringParam("text"));
  	    	String encodedValue2 = encryptString(request.getStringParam("text"));	
  	    	// String decodeValue = crypto.decode(encodedValue);
  	    	
			return write("encodedValue : " + encodedValue + "--->" + encodedValue2);
      } catch(Exception e) {
   	    log.error("---------------------------------------------------------------\n" + e.toString());
     	 	return write("Test 호출 실패");
      }
    }

    private IResponse TestLdap(IRequest request) throws Exception 
    {
  	    try {
  	    	// String encodedValue = crypto.encode("MD5", request.getStringParam("text"));
  	    	String userId = request.getStringParam("id");
  	    	String passwd = request.getStringParam("pw");	
  	    	// LDAP Context
  			
  	    	String ldapDomain = pm.getString("cliptorplus.LDAP.domain");
  	  	    String ldapPath = pm.getString("cliptorplus.LDAP.path");
  	  	    String ldapFilter = pm.getString("cliptorplus.LDAP.filter");
  	  	    String ErrMsg="";
  	  	    boolean isSuccess=false;
	  	    
  	  	    log.error("------------ldapDomain path----------\n" + ldapDomain + " : " + ldapPath);
	  	    
	  	    // String securityPrincipal = "uid=" + uid + "," + providerUrl.substring(providerUrl.lastIndexOf("/") + 1);
  	    	
  	    	Hashtable<String, String> properties = new Hashtable<String, String>();
  	    	properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
  	    	properties.put(Context.PROVIDER_URL, ldapDomain);
  	    	properties.put(Context.SECURITY_AUTHENTICATION, "simple");
  	    	// properties.put(Context.SECURITY_AUTHENTICATION, "96");
  	    	properties.put(Context.SECURITY_PRINCIPAL, ldapPath + userId + ldapFilter);
  	    	properties.put(Context.SECURITY_CREDENTIALS, passwd);
  	    	
  	    	try {
				DirContext ctx = new InitialDirContext(properties);
				log.error("------Ldap : DirContext Success  \n");
	            ctx.close();
	            ErrMsg = "Ldap Login Success ";
	            
	  	  	} catch(AuthenticationException e) {
	  	  		ErrMsg = "-Ldap Fail : Wrong Password."+ e.toString();
	  	  		log.error(ErrMsg);
	  	  	} catch(InvalidNameException e) {
		  	  	ErrMsg = "-Ldap Fail : User not Found."+ e.toString();
	  	  		log.error(ErrMsg);
	  	  	} catch(NamingException e) {
		  	  	ErrMsg = "-Ldap Fail : NamingException."+ e.toString();
	  	  		log.error(ErrMsg);
	  	  	} 
  	    	return write(ErrMsg);
      } catch(Exception e) {
   	        log.error("---------------------------------------------------------------\n" + e.toString());
     	 	return write("Test 호출 실패");
      }
    }
          
    
}
