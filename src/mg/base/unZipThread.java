package mg.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.zip.ZipException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

import mg.base.cryptorSHA;

public class unZipThread extends Thread{
	protected int BUFFER_SIZE = 1024 * 4;
	private Log log = LogFactory.getLog(this.getClass());
	String src ;
	String target;
	public unZipThread(String srcFile, String targetPath){
		src = srcFile;
		target=targetPath;
	 }
	@Override
	 public void run(){
		log.info("unZipThread --- \n srcFile : "+src+" \n targetPath : "+target);
		unZip(src, target); 
	 }
	public String getCharset(byte[] buf) throws UnsupportedEncodingException{
		if (buf.length >= 2 && ((buf[0] == -1 && buf[1] == -2) || (buf[0] == -2 && buf[1] == -1))) {
			 return "UTF-8";//"Unicode";
		}
		if (buf.length >= 3 && buf[0] == -17 && buf[1] == -69 && buf[2] == -65) {
		     return "UTF-8";//"UTF-8+";
		}
		int size = new String(buf, "UTF-8").getBytes("UTF-8").length;
		if (size == buf.length) {
		     return "UTF-8";
		}
		return "EUC-KR";//"MS949";
	}
	public String getCharset(File fileName){
		String charset ="UTF-8";
		ZipFile zipFile = null;
		try{
			 zipFile = new ZipFile(fileName);
			Enumeration entries = zipFile.getEntries();
			ZipEntry entry = (ZipEntry) entries.nextElement();
			String entryFileName = entry.getName();
			charset = getCharset(entryFileName.getBytes());			
			//log.info("getCharset-----------\n fileName : "+fileName.getParent()+"\n charset : "+charset);
		}catch(Exception e){
			log.error("getCharset Exception------------------------\n : "+e.toString());
		} finally {
			try {
				if (zipFile != null){
					zipFile.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return charset;
	}

	public int unZip(String zipFileDir, String newDir) {
		//return ZipFileUtil.unZip(new File(zipFileDir), newDir);
		return this.unZip(new File(zipFileDir), newDir);
	}
	@SuppressWarnings("unchecked")
	public int unZip(File fileName, String newDir) {
		return unZip(fileName, newDir, false, null);
	}
	@SuppressWarnings("unchecked")
	public int unZip(File fileName, String newDir, boolean isImgExtChange, String[] extChangeList) {
		Enumeration entries;
		ZipFile zipFile = null;
		try {
			zipFile = new ZipFile(fileName, getCharset(fileName));
			entries = zipFile.getEntries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				String orgDirName = fileName.getParent() + File.separator;
				String entryFileName = entry.getName();
				//log.info("unZip-------------\n orgDirName : "+orgDirName+"\n entryFileName : "+entryFileName +"\n charset : "+getCharset(entryFileName.getBytes()));
				if (entry.isDirectory()) {
					(new File(newDir + "/" + entryFileName)).mkdirs();
					continue;
				} else {
					String[] tmpSplit = null;
					String tmpFileName = null;
					String tmpFileExt = null;
					
					try {
						tmpSplit = entryFileName.split(File.separator);
					} catch (Exception e) {
						tmpSplit = entryFileName.split("\\\\");
					}
					if (tmpSplit.length > 1) {
						String tmpDir = "";
						for (int i = 0; i < tmpSplit.length - 1; i++)
							tmpDir += (tmpSplit[i] + File.separator);
						tmpDir = orgDirName + tmpDir;
						File tmpFile = new File(tmpDir);
						if (!tmpFile.exists())
							tmpFile.mkdir();
					}
					if(isImgExtChange == true) {
						tmpFileName = tmpSplit[tmpSplit.length-1];
						tmpFileExt = tmpFileName.substring(tmpFileName.lastIndexOf(".")+1);
						
						for(int i=0; i<extChangeList.length; i++) {
							if(extChangeList[i].equalsIgnoreCase(tmpFileExt)) {
								entryFileName += "_";
								break;
							}
						}
					}
				}
				FileUtils.forceMkdir(new File(newDir));
				unzipEntry(zipFile.getInputStream(entry), new File(newDir + File.separator + entryFileName));
			}
		}catch(FileNotFoundException fne){
			fne.printStackTrace();
		} catch (ZipException ze) {
			ze.printStackTrace();
			return 0;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}finally {
			try {
				if (zipFile != null){
					zipFile.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return 1;
	}

	/**
     * Zip 파일의 한 개 엔트리의 압축을 푼다.
     *
     * @param zis - Zip Input Stream
     * @param filePath - 압축 풀린 파일의 경로
     * @return
     * @throws Exception
     */
	protected File unzipEntry(InputStream zis, File targetFile) throws Exception {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(targetFile);

            byte[] buffer = new byte[BUFFER_SIZE];
            int len = 0;
            while ((len = zis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        } catch(FileNotFoundException e) { //알집 10.70 버전의 경우 entry에 파일만 갖고오고, 경로는 갖고 오지 않아 아래와 같이 처리.
           // log.info("FileNotFoundException 발생----------------------------------"+targetFile);
        	String fullPath = targetFile.getAbsolutePath();
           String folderPath = fullPath.substring(0, fullPath.lastIndexOf(File.separator));
           (new File(folderPath)).mkdirs();
           
           fos = new FileOutputStream(targetFile);

           byte[] buffer = new byte[BUFFER_SIZE];
           int len = 0;
           while ((len = zis.read(buffer)) != -1) {
               fos.write(buffer, 0, len);
           }
           
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
        return targetFile;
    }

}
