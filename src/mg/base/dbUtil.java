package mg.base;
import java.util.Map;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.util.ArrayList;

public class dbUtil {
	
	private Connection dbConn = null;
	private String dbUrl;    
	private String dbUser;    
	private String dbPwd;

	public boolean  DBConnection() {   
		
		dbUrl  = "jdbc:sqlserver://www.itmsystem.com:1433;databaseName=MgCP"; 
		dbUser = "sa"; 
		dbPwd = "9480itmtltmxpa"; 
		return true;
	}

	public boolean DBConnection(String dbUrl, String dbUser, String dbPwd) {   
		this.dbUrl  = "jdbc:microsoft:sqlserver://" + dbUrl;   
		this.dbUser = dbUser;   
		this.dbPwd = dbPwd;
		return true;
	}

	public Connection getConnection() {        
		try {          
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");    
			dbConn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);  
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return dbConn;  
	}

	public void close(){        

		try {            
			if(!dbConn.isClosed()) {                
				dbConn.close();            
			}
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
	}


}
