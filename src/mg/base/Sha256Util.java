package mg.base;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.jfree.util.Log;

import java.util.Base64;
// import sun.misc.BASE64Encoder;
import java.util.Base64.Encoder;

public class Sha256Util {
	
	static public String getSha256Encryption(String srcStr) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-256");

		md.update(srcStr.getBytes());
		
		byte[] hashValue = md.digest();
		
		// String descStr = hashValue.toString();
		
		/*
		 Encoder base64Encoder = new Encoder();
		String descStr = base64Encoder.encode(hashValue);
		 */
		
		Encoder encoder = Base64.getEncoder();
     	String descStr = encoder.encodeToString(hashValue);
		
		
		
		return descStr;
	}
	
	/*
	 * SHA256 암호호 모듈
	 */
	static public String encryptSha256(String srcStr) throws Exception {
		
	    MessageDigest md = MessageDigest.getInstance("SHA-256");
	
	    byte[] dataBytes = new byte[1024];
		
	    md.update(srcStr.getBytes());
	    byte[] mdbytes = md.digest();
	
	    //convert the byte to hex format method 1
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < mdbytes.length; i++) {
	      sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	    }
	
	    // System.out.println("Hex format : " + sb.toString());
	    //convert the byte to hex format method 2
	    /*
	    StringBuffer hexString = new StringBuffer();
		for (int i=0;i<mdbytes.length;i++) {
		  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
		}
	
		System.out.println("Hex format : " + hexString.toString());
		*/
		return sb.toString();
	}
	
	
	//--------농협생명 적용 암호화 모듈 (key값 사용)-------------
	public static String getEncrypt(String source, byte[] salt)
    {
        String result = "";
        try
        {
            byte[] a = source.getBytes();
            byte[] bytes = new byte[a.length + salt.length];
            System.arraycopy(a, 0, bytes, 0, a.length);
            System.arraycopy(salt, 0, bytes, a.length, salt.length);

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(bytes);

            byte[] byteData = md.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; ++i)
            {
                sb.append(Integer.toString((byteData[i] & 0xFF) + 256, 16).substring(1));
            }

            result = sb.toString();
        } catch (NoSuchAlgorithmException e)
        {
        	//logger.error("{}", new Object[] {e.getMessage(), e});
        }

        return result;
    }
    
    // 임의 salt를 생성
    public static byte[] createSalt(String password) {
		java.util.Random random = new java.util.Random();
		byte[] saltBytes = password.getBytes();
		random.nextBytes(saltBytes);
        return saltBytes;
    }
  //------------------------------------------------------------------------
	
    
}
