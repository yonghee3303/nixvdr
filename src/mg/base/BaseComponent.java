/*
 * Copyright (c) 2010 Genetics. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics.
 */

package mg.base;

import java.util.List;
import java.util.Map;

import com.core.base.ComponentRegistry;
import com.core.component.navigation.PageList;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;

/**
* 업무 그룹명 : coms.base
* 서브 업무명 : BaseComponent.java
* 작성자 : cyLeem
* 작성일 : 2008. 09. 24
* 설 명 : ISqlManagement 컴포넌트를 사용하는 POJO 컴포넌트의 기본 구현 클래스.
*/
public abstract class BaseComponent {

    /**
     * 기본 목록 크기.
     */
    private static int defaultListSize = -1;

    /**
     * ISqlManagement 컴포넌트 인스턴스.
     */
    protected ISqlManagement sm;

    /**
     * ISqlManagement 컴포넌트 인스턴스를 연결한다.
     * 
     * @param sm
     *            ISqlManagement 컴포넌트 인스턴스
     */
    public void setSqlManagement(ISqlManagement sm) {
        this.sm = sm;
    }

    /**
     * 오라클 ROWNUM을 이용해서 페이지 네비페이션 처리를 한다.
     * 
     * @param mapStmtName
     *            SQL ID
     * @param query
     *            검색 조건
     * @param pageNo
     *            페이지 번호
     * @return 검색 결과
     */
    @SuppressWarnings("unchecked")
	protected List getOraclePageList(String mapStmtName, Map query, int pageNo) {
        if (defaultListSize == -1) {
            defaultListSize = ((IParameterManagement) ComponentRegistry
                    .lookup(IParameterManagement.class))
                    .getInt("component.navigation.defaultListSize");
        }
        return getOraclePageList(mapStmtName, query, pageNo, defaultListSize);
    }

    /**
     * 오라클 ROWNUM을 이용해서 페이지 네비페이션 처리를 한다.
     * 
     * @param mapStmtName
     *            SQL ID
     * @param query
     *            검색 조건
     * @param pageNo
     *            페이지 번호
     * @param listSize
     *            목록 크기
     * @return 검색 결과
     */
    @SuppressWarnings("unchecked")
	protected List getOraclePageList(String mapStmtName, Map query, int pageNo,
            int listSize) {
        pageNo = pageNo < 1 ? 1 : pageNo;
        query.put("firstRowIndex", new Integer((pageNo - 1) * listSize + 1));
        query.put("lastRowIndex", new Integer(pageNo * listSize));
        query.put("listSize", new Integer(listSize));
        return new PageList(sm.getList(mapStmtName, query), getTotalCount(
                mapStmtName, query), pageNo, listSize);
    }

    /**
    * @param mapStmtName
    * @param query
    * @return
    */
    private int getTotalCount(String mapStmtName, Object query) {
        return ((Integer) sm.getItem(mapStmtName + "Count", query)).intValue();
    }

}
