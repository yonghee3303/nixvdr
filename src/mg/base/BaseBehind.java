/*
 * Copyright (c) 2010 Genetics. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics.
 */

package mg.base;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.Constants;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.message.IMessageManagement;
import com.core.component.navigation.PageList;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.ISqlManagement;
import com.core.component.upload.FileUploadException;
import com.core.component.user.IUser;
import com.core.component.user.internal.User;
import com.core.component.util.StringUtils;
import com.core.framework.core.Behind;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
* 업무 그룹명 : coms.base
* 서브 업무명 : BaseBehind.java
* 작성자 : 이병기
* 작성일 : 2008. 09. 24
* 설 명 : BaseBehind
*/
public abstract class BaseBehind extends Behind {
    /** Debug 로그 기록을 위한 클래스를 선언한다. */
    protected Log log = LogFactory.getLog(this.getClass());
    

    /**
     * 기본 목록 크기.
     */
    private static int defaultListSize = -1;

    /**
     * ICodeManagement 컴포넌트 인스턴스
     */
    private ICodeManagement cm = (ICodeManagement) getComponent(ICodeManagement.class);

    /**
     * IParameterManagement 컴포넌트 인스턴스
     */
    private IParameterManagement pm = (IParameterManagement) getComponent(IParameterManagement.class);
    
    /**
    * @return ISqlManagement
    */
    protected ISqlManagement getSqlManagement() {
        return this.sm;
    }
    
    /**
    * @return
    */
    protected IMessageManagement getMessageManagement() {
    	return this.mm;
    }

    /**
     * 파라미터 값을 반환한다.
     * 
     * @param key
     *            파라미터 키
     * @return 파라미티 값
     */
    protected String getParameter(String key) {
        return pm.getString(key);
    }

    /**
     * IParameterManagement 컴포넌트 인스턴스를 반환
     */
    protected IParameterManagement getParameterManagement() {
        return pm;
    }

    /**
     * ICode 객체를 반환한다.
     * 
     * @param id
     *            코드 아이디
     * @return ICode 코드
     */
    protected ICode getCode(String id) {
        return cm.getCode(id);
    }

    /**
     * IUser 객체를 추가한 Map을 반환한다.
     * 
     * @param request
     *            IRequest 사용자 요청
     * @return Map IUser 객체를 추가한 사용자 요청 파라미터
     */
    @SuppressWarnings("unchecked")
	protected Map getAuditableMap(IRequest request) {
        Map result = request.getMap();
        result.put(Constants.USER, getUser(request));
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        result.put(Constants.TIMESTAMP, formatter.format(new Date()));
        return result;
    }

    /**
     * IUser 객체를 추가한 FormattedMap을 반환한다.
     * 
     * @param request
     *            IRequest 사용자 요청
     * @return Map IUser 객체를 추가한 사용자 요청 파라미터
     */
    @SuppressWarnings("unchecked" )
	protected Map getAuditableFormattedMap(IRequest request) {
        Map result = request.getFormattedMap();
        IUser jUser = getUser(request);
        result.put(Constants.USER, jUser);
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        result.put(Constants.TIMESTAMP, formatter.format(new Date()));
        return result;
    }

    /**
     * BaseUser 객체를 반환한다.
     * 
     * @param request
     *            IRequest 사용자 요청
     * @return BaseUser 사용자를 나타내는 객체
     */
    public IUser getUser(IRequest request) {
        IUser jUser = null;
        try {
        	jUser = (IUser)request.getUser();
        } catch(Exception e) {
        	log.error(e);
        }

        if(jUser == null) {
        	jUser = new User("guest", "손님");
        	request.addSessionObject(Constants.USER, jUser);
        }
    	return jUser; 
    }

    /**
     * 오라클 ROWNUM을 이용해서 페이지 네비페이션 처리를 한다.
     * 
     * @param mapStmtName
     *            SQL ID
     * @param query
     *            검색 조건
     * @param pageNo
     *            페이지 번호
     * @return 검색 결과
     */
    @SuppressWarnings("unchecked")
	protected List getOraclePageList(String mapStmtName, Map query, int pageNo) {
        if (defaultListSize == -1) {
            defaultListSize = ((IParameterManagement) getComponent(IParameterManagement.class))
                    .getInt("component.navigation.defaultListSize");
        }
        return getOraclePageList(mapStmtName, query, pageNo, defaultListSize);
    }

    /**
     * 오라클 ROWNUM을 이용해서 페이지 네비페이션 처리를 한다.
     * 
     * @param mapStmtName
     *            SQL ID
     * @param query
     *            검색 조건
     * @param pageNo
     *            페이지 번호
     * @param listSize
     *            목록 크기
     * @return 검색 결과
     */
    @SuppressWarnings("unchecked")
	protected List getOraclePageList(String mapStmtName, Map query, int pageNo,
            int listSize) {
        pageNo = pageNo < 1 ? 1 : pageNo;
        query.put("firstRowIndex", new Integer((pageNo - 1) * listSize + 1));
        query.put("lastRowIndex", new Integer(pageNo * listSize));
        query.put("listSize", new Integer(listSize));
        return new PageList(getList(mapStmtName, query), getTotalCount(
                mapStmtName, query), pageNo, listSize);
    }

    /**
    * @param mapStmtName
    * @param query
    * @return
    */
    private int getTotalCount(String mapStmtName, Object query) {
        return ((Integer) getItem(mapStmtName + "Count", query)).intValue();
    }

    /**
     * 사용자가 지정한 목록 크기를 반환한다. 없는 경우에는 기본 값을 반환한다.
     * 
     * @param request
     *            사용자 요청
     * @return 목록 크기
     */
    protected int getListNo(IRequest request) {
        int listSize = request.getIntParam("q_listNo");
        return (listSize == 0) ? pm
                .getInt("component.navigation.defaultListSize") : listSize;
    }

    /**
     * 사용자가 검색을 요청했는지에 대한 여부를 반환한다.
     * 
     * @param request
     *            사용자 요청
     * @return 검색 여부
     */
    protected boolean isQuery(IRequest request) {
        return request.getBooleanParam("isQuery");
    }

    /**
     * Map에 있는 객체를 String으로 전환한다.
     * 
     * @param map
     *            java.util.Map
     * @param key
     *            키
     * @return null인 경우에는 null을 반환
     */
    @SuppressWarnings("unchecked")
	protected String getStringWithNullCheck(Map map, String key) {
        Object result = map.get(key);
        return (result == null) ? null : result.toString();
    }
    

    /**
     * 현재 날짜를 yyyy-MM-dd HH:mm:ss형태로 반환
     * @return 현재 날짜를 yyyy-MM-dd HH:mm:ss형태로 반환
     */
    public static String getCurrentDate() {
        return getCurrentDate("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 현재 날짜를 format형태로 반환
     * @param format
     * @return 현재 날짜를 format형태로 반환
     */
    public static String getCurrentDate(String format) {
        java.util.Date now    = new java.util.Date();
        return getDateString(now, format);
    }

    /**
     * 날짜를 format형태로 반환
     * @param d
     * @param format
     * @return d를 format형태로 반환
     */
    public static String getDateString(Date d, String format) {
        java.text.SimpleDateFormat vans = new java.text.SimpleDateFormat(format);
        return vans.format(d);
    }    
    
    /**
     * 폼을 동적으로 생성하고 폼에 검색관련 파라미터를 설정한후 서브밋하도록 코딩된 포이지를 출력하는 Response객체를 반환 
     * @param req
     * @param action
     * @param method
     * @return 폼을 동적으로 생성하고 폼에 검색관련 파라미터를 설정한후 서브밋하도록 코딩된 포이지를 출력하는 Response객체를 반환
     */
    @SuppressWarnings("unchecked")
	protected IResponse sendRedirectWithParameter(IRequest req, String action, String method) {
    	Map param = this.getAuditableFormattedMap(req);
    	return sendRedirectWithParameter(req, action, method, param);
    }
    
    /**
     * 폼을 동적으로 생성하고 폼에 검색관련 파라미터를 설정한후 서브밋하도록 코딩된 포이지를 출력하는 Response객체를 반환
     * @param req
     * @param action
     * @param method
     * @param param
     * @return 폼을 동적으로 생성하고 폼에 검색관련 파라미터를 설정한후 서브밋하도록 코딩된 포이지를 출력하는 Response객체를 반환
     */
    @SuppressWarnings("unchecked")
	protected IResponse sendRedirectWithParameter(IRequest req, String action, String method, Map param) {
    	HttpServletRequest hreq = (HttpServletRequest)req.getAdapter(HttpServletRequest.class);
    	String path = this.getModulePrefix(req);
    	if("post".equalsIgnoreCase(method)) {
    		if(!action.startsWith("http://") && !action.startsWith("ftp://") && !action.startsWith("@") ) {
    			action = hreq.getContextPath() + path + action;
    		} else if(action.startsWith("@")) {
    			int slashLastPos = path.lastIndexOf("/");
    			int pathCutPos = ((slashLastPos+4) > path.length()) ? path.length():(slashLastPos+4); 
    			boolean validPath = slashLastPos > 0;
   				action = hreq.getContextPath() + (validPath?path.substring(0, pathCutPos):path) + action.substring(1);
    		}
	    	String formId = "BEHIND_GENERATED_" + getCurrentDate("yyyyMMddHHmmss");
	    	StringBuffer buff = new StringBuffer();
	    	buff.append("<html>");
	    	buff.append("<head>");
	    	buff.append("<script type=\"text/javascript\">");
	    	buff.append("function submitForm() {");

	    	String msg = (String)param.get("ALERT_MESSAGE");
	    	if(msg != null && msg.trim().length() > 0) {
	    		msg = StringUtils.replaceAll(msg, "'", "\\'");
	    		msg = StringUtils.replaceAll(msg, "\n", "\\n");
	    		buff.append("alert('" + msg + "');");
	    	}
	    	
	    	buff.append("var f = document.getElementById(\"" + formId + "\");");
	    	buff.append("f.submit();");
    		buff.append("}");
    		buff.append("</script>");
    		buff.append("</head>");
    		buff.append("<body onload=\"submitForm()\">");
	    	buff.append("<form id=\"" + formId + "\" action=\"" + action + "\" method=\"post\">");
	    	buff.append(getSearchParamQueryString(action, param, "POST"));
	    	buff.append("</form>");

	    	msg = (String)param.get("HTML_MESSAGE");
	    	if(msg != null && msg.trim().length() > 0) {
	    		buff.append(msg);
	    	}
	    	
	    	buff.append("</body>");
	    	buff.append("</html>");
	    	return write(buff.toString());
    	} else {
    		if(!action.startsWith("http://") && !action.startsWith("ftp://") && !action.startsWith("@")) {
    			action = path + action;
    		} else if(action.startsWith("@")) {
    			int slashLastPos = path.lastIndexOf("/");
    			int pathCutPos = ((slashLastPos+4) > path.length()) ? path.length():(slashLastPos+4); 
    			boolean validPath = slashLastPos > 0;
   				action = (validPath?path.substring(0, pathCutPos):path) + action.substring(1);
    		}    		
    		return sendRedirect(action + getSearchParamQueryString(action, param, "GET"));
    	}
    }
    
    /**
     * 검색관련 페이징관련 파라미터를 method에 따라 GET방식 혹은 POST방식으로 구성하여 반환
     * @param action
     * @param param
     * @param method
     * @return 검색관련 페이징관련 파라미터를 method에 따라 GET방식 혹은 POST방식으로 구성하여 반환
     */
    @SuppressWarnings("unchecked")
	protected String getSearchParamQueryString(String action, Map param, String method) {
    	if(param == null || param.size() < 1) {
    		return "";
    	}
    	StringBuffer buff = new StringBuffer();
		String key = "";
		Object obj = "";		
		String val = "";
		boolean isPost = "post".equalsIgnoreCase(method);
		try {
			Set ks = param.keySet();
			Iterator it = ks.iterator();
			int i=0;
			while(it.hasNext()) {
				key = (String)it.next();
				if(key != null) {
					obj = param.get(key);
					if( obj != null && (obj instanceof String || obj instanceof String[]) 
							&& ( key.startsWith("q_") 
									|| "orderIndex".equals(key)
									|| "orderThreadIndex".equals(key)
									|| "orderBy".equals(key)
									|| "orderMode".equals(key)
									|| "pageNo".equals(key)) 
							) {
						if(isPost) {		
							if(obj instanceof String[]) {
								for(int x=0; x < ((String[])obj).length; x++) {
									val = ((String[])obj)[i];
									val = val.replaceAll("\"", "\\\"");
									buff.append("<input type=\"hidden\" name=\"" + key + "\" value=\"" + val + "\"/>");
								}
							} else {
								val = (String)obj;
								val = val.replaceAll("\"", "\\\"");
								buff.append("<input type=\"hidden\" name=\"" + key + "\" value=\"" + val + "\"/>");
							}
						} else {
							if(i==0) {
								if(action.indexOf("?") != -1) {
									buff.append("&");
								} else {
									buff.append("?");
								}
								i++;
							} else {
								buff.append("&");
							}
							
							if(obj instanceof String[]) {
								for(int x=0; x < ((String[])obj).length; x++) {
									val = ((String[])obj)[i];
									val = java.net.URLEncoder.encode(val, pm.getString("framework.config.defaultEncoding"));
									buff.append(key + "[" + x + "]=" + val);							
								}
							} else {
								val = (String)obj;
								val = java.net.URLEncoder.encode(val, pm.getString("framework.config.defaultEncoding"));
								buff.append(key + "=" + val);
							}
						}
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return buff.toString();
    }
    
    /**
     * 요청한 페이지의 jsp파일명 모듈명부분을 반환한다. 
     * 모듈 구분에 사용한다.
     *  
     * @param request
     *            사용자 요청
     * @return 모듈구분
     */    
    protected String getModulePrefix(IRequest request) {
        String path = request.getPath();
        return getModulePrefix(path);
    }
    
    /**
     * 요청한 페이지의 jsp파일명 모듈명부분을 반환한다.
    * @param path 원본경로
    * @return 모듈구
    */
    protected String getModulePrefix(String path) {
        int spos = path.length();
        
        if (path.lastIndexOf("Popup.jsp") > -1) {
            spos =  path.lastIndexOf("Popup.jsp");
        } else if (path.lastIndexOf("List.jsp") > -1) {
        	spos =  path.lastIndexOf("List.jsp");
        } else if (path.lastIndexOf("Form.jsp") > -1) {
        	spos =  path.lastIndexOf("Form.jsp");
        } else if (path.lastIndexOf("View.jsp") > -1) {
        	spos =  path.lastIndexOf("View.jsp");
        } else if (path.lastIndexOf(".jsp") > -1) {
        	spos =  path.lastIndexOf(".jsp");
        }
        
        if(spos > -1) {
            try {
                return path.substring(0 , spos);
            } catch(Exception e) {
            	e.printStackTrace();
            }
        }
        return path;    	
    }
    
    /**
     * 저장된 세션중 사이트구분(COMS_DDS_SITE_CTX)을 반환
     * @param request
     * @return 저장된 세션중 사이트구분을 반환
     */
    protected String getSiteCtx(IRequest request) {
    	return (String)request.findSessionObject("COMS_DDS_SITE_CTX");
    }

    /**
     * 세션에 사이트구분(COMS_DDS_SITE_CTX)를 저장
     * @param request
     * @param defaultSiteCtx
     */
    protected void setSiteCtx(IRequest request, String defaultSiteCtx) {
		String path = request.getPath();
		String siteCtx = defaultSiteCtx;
		HttpServletRequest hreq = getRequest(request); 
		String ctx = hreq.getContextPath();
		
		//log.error(ctx);
		
		/**
		if("/intra".equals(ctx)) {
			// 인트라넷
			siteCtx = "SITE_CTX_03";
		} else if("/web".equals(ctx)) {
			if(path.startsWith("/eng")) {
				// 인터넷 영문
				siteCtx = "SITE_CTX_02";
			} else if(path.startsWith("/kor")){
				// 인터넷 국문 
				siteCtx = "SITE_CTX_01";
			}
		}
		/*/
		if(path.startsWith("/eng/")) {
			// 인터넷 영문
			siteCtx = "SITE_CTX_02";
		} else if(path.startsWith("/kor/")){
			// 인터넷 국문 
			siteCtx = "SITE_CTX_01";
		} else if(path.startsWith("/intra/")) {
			// 인트라넷 
			siteCtx = "SITE_CTX_03";
		}
		/**/
		
		//log.error(siteCtx);
		request.addSessionObject("COMS_DDS_SITE_CTX", siteCtx);
    }
    
    /**
     * IRequest로부터 HttpSession를 얻는다.
    * @param request
    * @return HttpSession
    */
    protected HttpSession getSession(IRequest request) {
    	HttpSession sess = (HttpSession)request.getAdapter(HttpSession.class);
    	//log.error(sess); 
    	return sess;
    }    
    
    /**
     * IRequest로부터 ServletContext를 얻는다.
    * @param request
    * @return ServletContext
    */
    protected ServletContext getServletContext(IRequest request) {
    	ServletContext ctx = (ServletContext)request.getAdapter(ServletContext.class);
    	return ctx;
    }    
    
    /**
     * IRequest로부터 HttpServletRequest를 얻는다.
    * @param request
    * @return HttpServletRequest
    */
    protected HttpServletRequest getRequest(IRequest request) {
    	HttpServletRequest req = (HttpServletRequest)request.getAdapter(HttpServletRequest.class);
    	return req;
    }
    
    /**
     * IRequest로부터 HttpServletResponse를 얻는다.
    * @param request
    * @return HttpServletResponse
    */
    protected HttpServletResponse getResponse(IRequest request) {
    	HttpServletResponse res = (HttpServletResponse)request.getAdapter(HttpServletResponse.class);
    	return res;
    }
    
    /**
    * @param request
    * @return
    */
    protected Locale getLocale(IRequest request) {
    	Locale locale = null;
    	if("SITE_CTX_02".equals(getSiteCtx(request))) {
    		locale = Locale.ENGLISH;
    	}
    	return locale;
    }
    
    /**
     * 예외처리
     * @param request IRequest - JSP 파라메타
     * @param cause Throwable 
     * @return IResponse
     */    
    public IResponse handleException(IRequest request, Throwable cause)
            throws Throwable {
        if (cause instanceof IllegalStateException) {
            request.addExceptionMessage(getMessageManagement().getMessage("GMSG_1001", getLocale(request)));
            return null;
        } else if(cause instanceof FileUploadException) {
        	return alertAndBack(getMessageManagement().getMessage("GMSG_1047", getLocale(request)));
        }
        return super.handleException(request, cause);
    }    
}