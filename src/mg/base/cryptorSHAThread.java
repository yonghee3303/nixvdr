package mg.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import mg.base.cryptorSHA;

public class cryptorSHAThread extends Thread{
	private Log log = LogFactory.getLog(this.getClass());
	 String srcFile;
	 String targetFile;
	 String password;
	 long resultLong;

	public cryptorSHAThread(String src,String target,String pass){
		 srcFile = src;
		 targetFile = target;
		 password=pass;
	 }
	
	 public void setSrcFile(String src){
		 srcFile = src;
	 }
	 public void setTargetFile(String target){
		 targetFile = target;
	 }
	 public void setPassword(String pass){
		 password = pass;
	 }
	 public long getResultLong() {
		return resultLong;
	 }
	 
	 
	 @Override
	 public void run(){
		log.info("cryptorSHAThread --- \n srcFile : "+srcFile+" \n targetFile : "+targetFile);
		resultLong = cryptorSHA.MgDecodeFile(srcFile, targetFile, password);
	 }
}
