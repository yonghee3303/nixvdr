package mg.base;

public class stringUtil {

	public static String checkParameter(String srcparam) {   
		
		srcparam = srcparam.replaceAll("<", "");
		srcparam = srcparam.replaceAll(">", "");
		srcparam = srcparam.replaceAll("’", "");
		srcparam = srcparam.replaceAll("&", "");
		srcparam = srcparam.replaceAll("%", "");
		srcparam = srcparam.replaceAll("!", "");
		srcparam = srcparam.replaceAll("--", "");
		return srcparam;
	}
	
	public static String removeLastChar(String src)
	{
		try {
			String str = src;
			if(src.length() > 1){
				str = src.substring(0, src.length()-1);
			}
			return str;
		} catch(Exception e)
		{
			return src;
		}
		
	}

}
