// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2009-03-05 오후 5:41:41
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   GridData.java

package mg.base;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;
import mg.base.DataFormater;

// Referenced classes of package xlib.cmc:
//            GridHeader

public class GridData
{
	private Hashtable ht;
    private Hashtable param;
    private Hashtable av_ht;
    private String message;
    private String status;
    private boolean isSelectable;
    private long totalCount;
    private int row_count;
    private String navigateValue;
    private Vector vt;
    private StringBuffer recsb;
    private StringBuffer exdsb;
    private DataFormater recv_sf;
    private Vector rec_vec;
    private String row_del;
    private String col_del;
    int data_cnt = 1;
    
    //  RowsMerge 관련된 전역변수
	HashMap rows_merge_map = null;
	HashMap merge_skip_map = null;
	Iterator it = null;
	Vector key_vec = null;
	Vector merge_sb = null;
	String key_name = "";
	int group_merge_cnt = 0;
	
	IParameterManagement parameter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
    
    public GridData()
    {
        ht = new Hashtable();
        param = new Hashtable();
        av_ht = new Hashtable();
        vt = new Vector();
        rec_vec = new Vector();
        recsb = new StringBuffer();
        exdsb = new StringBuffer();
        message = "";
        status = "-1";
        totalCount = -1L;
        navigateValue = "";
        isSelectable = true;
        row_count = 0;
        col_del = parameter.getString("dhtmlx.separator.field"); 
        row_del = parameter.getString("dhtmlx.separator.line");
    }
    
    public void setRecvDataFormater(String sf_data) throws Exception
    {
    	recv_sf = new DataFormater(sf_data);
    	row_count = recv_sf.getRowCount();
    }
    
    public void setSelectable(boolean isSelectable)
    {
    	this.isSelectable = isSelectable;
    }
    
    public boolean getSelectable() {
    	return isSelectable;
    }
    
    public int getRowCount()
    {
        return row_count;
    }
    
    public String getValue(String id, int row) throws Exception
    {
    	return recv_sf.getValue(id, row);
    }

    public void setTotalCount(long totalCount)
    {
        this.totalCount = totalCount;
    }

    public long getTotalCount()
    {
        return totalCount;
    }
    
    public int getResTotalCount()
    {
    	return data_cnt;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
 



    public void addParam(String id, String value)
        throws Exception
    {
        param.put(id, value);
    }
    
    public void addValue(String column, String value)
    throws Exception
	{
    	String begin_del = "<";
    	String end_del   = ">";
    	String slash_del = "/";
    	String data      = "";
    	
    	try {
   		
    		if(rec_vec.size() == 0 || rec_vec.contains(column)) {
    			if(rec_vec.size() == 0) {
    				rec_vec.addElement(column);
    				recsb.append("<row id=\""+data_cnt+"\">");
    			} else {
    				data_cnt++;
    				recsb.append("</row>");
    				recsb.append("<row id=\""+data_cnt+"\">");
    				exdsb.append(row_del);
    			}
    		}
    		
    		data = value;
    		//data = data.replaceAll("<", "&lt;");
    		//data = data.replaceAll(">", "&gt;");
    		//data = data.replaceAll("&", "&amp;");
    		
    		recsb.append(begin_del + "cell " + end_del + "<![CDATA[" + data + "]]>" + begin_del + slash_del + "cell" + end_del);
    		exdsb.append(data+col_del);
    	} catch(Exception e) {
    		throw new Exception(e.getMessage()); 
    	}
	}
    
    public void addSubRowValue(String column, String value)
    throws Exception
	{
    	String begin_del = "<";
    	String end_del   = ">";
    	String slash_del = "/";
    	String data      = "";
    	
    	try {
    		if(rec_vec.size() == 0 || rec_vec.contains(column)) {
    			if(rec_vec.size() == 0) {
    				rec_vec.addElement(column);
    				recsb.append("<row id=\""+data_cnt+"\">");
    			} else {
    				data_cnt++;
    				recsb.append("</row>");
    				recsb.append("<row id=\""+data_cnt+"\">");
    				exdsb.append(row_del);
    			}
    		}
    		
    		data = value;
    		//data = data.replaceAll("<", "&lt;");
    		//data = data.replaceAll(">", "&gt;");
    		//data = data.replaceAll("&", "&amp;");
    		
    		recsb.append(begin_del + "cell type=\"sub_row_grid\"" + end_del + "<![CDATA[" + data + "]]>" + begin_del + slash_del + "cell" + end_del);
    		exdsb.append(data+col_del);
    	} catch(Exception e) {
    		throw new Exception(e.getMessage()); 
    	}
	}
    
    public void addRowSpanValue(String column, String value)
    throws Exception
	{
    	String begin_del = "<";
    	String end_del   = ">";
    	String slash_del = "/";
    	String data      = "";
    	String compare_str = "";
		String next_compare_str = "";
		Vector new_merge_vec = null;
		int    find_idx = 0;
    	
    	try {
    		if(rec_vec.size() == 0 || rec_vec.contains(column)) {
    			if(rec_vec.size() == 0) {
    				rec_vec.addElement(column);
    				recsb.append("<row id=\""+data_cnt+"\">");
    			} else {
    				data_cnt++;
    				recsb.append("</row>");
    				recsb.append("<row id=\""+data_cnt+"\">");
    				exdsb.append(row_del);
    			}
    		}
    		
    		data = value;
    		//data = data.replaceAll("<", "&lt;");
    		//data = data.replaceAll(">", "&gt;");
    		//data = data.replaceAll("&", "&amp;");
    		
    		if(rows_merge_map.containsKey(column)) {
    			group_merge_cnt = 0;
				new_merge_vec = new Vector();
				merge_sb = (Vector)rows_merge_map.get(column);
				
				for(int k=0; k < merge_sb.size(); k++) 
				{
					compare_str = (String)merge_sb.elementAt(k);
					if(compare_str.equals(value) && (k==0 || (find_idx+1) == k)) {
						group_merge_cnt++;
						next_compare_str = compare_str;
						find_idx = k;
					} else {
						new_merge_vec.addElement(compare_str);
					}
				}

				if(group_merge_cnt > 0) {
					rows_merge_map.put(column, new_merge_vec);
				}
				
				if(group_merge_cnt > 0) {
					recsb.append(begin_del + "cell rowspan=\""+group_merge_cnt+"\" " + end_del + "<![CDATA[" + data + "]]>" + begin_del + slash_del + "cell" + end_del);
				} else {
					recsb.append(begin_del + "cell " + end_del + "<![CDATA[" + data + "]]>" + begin_del + slash_del + "cell" + end_del);
				}
    				
    		} else {
    			recsb.append(begin_del + "cell " + end_del + "<![CDATA[" + data + "]]>" + begin_del + slash_del + "cell" + end_del);
    		}
    		
    		exdsb.append(data+col_del);
    	} catch(Exception e) {
    		throw new Exception(e.getMessage()); 
    	}
	}

    public void addRowSpanValue(String column, String value, int cnt) throws Exception {
    	
    	String begin_del = "<";
    	String end_del   = ">";
    	String slash_del = "/";
    	String data      = "";
    	String compare_str = "";
		String next_compare_str = "";
		Vector new_merge_vec = null;
		int    find_idx = 0;
    	
    	try {
    		if(rec_vec.size() == 0 || rec_vec.contains(column)) {
    			if(rec_vec.size() == 0) {
    				rec_vec.addElement(column);
    				recsb.append("<row id=\""+data_cnt+"\">");
    			} else {
    				data_cnt++;
    				recsb.append("</row>");
    				recsb.append("<row id=\""+data_cnt+"\">");
    				exdsb.append(row_del);
    			}
    		}
    		
    		data = value;
    		//data = data.replaceAll("<", "&lt;");
    		//data = data.replaceAll(">", "&gt;");
    		//data = data.replaceAll("&", "&amp;");

			recsb.append(begin_del + "cell rowspan=\""+cnt+"\" " + end_del + "<![CDATA[" + data + "]]>" + begin_del + slash_del + "cell" + end_del);
			  		
    		exdsb.append(data+col_del);
    
    	} catch(Exception e) {
    		throw new Exception(e.getMessage()); 
    	}
	}
    
    
    
    public int getMergeGroupCont(StringBuffer matcher_sb, String pattern)
	{
		int group_cnt = 0;
		Matcher mc = Pattern.compile(pattern).matcher(matcher_sb.toString());
        
        while(mc.find()) {
        	group_cnt++;
        }
        
        return group_cnt;
    }
	
	public void setRowsMergeMap(HashMap map)
	{
		this.rows_merge_map = map;
		
		if(rows_merge_map.size() > 0) {
			merge_skip_map = new HashMap();
			it = rows_merge_map.keySet().iterator();
			while(it.hasNext()) {
				key_name = (String)it.next();
				key_vec = new Vector();
				merge_skip_map.put(key_name, key_vec);
			}
		}
	}

    public String getParam(String id)
        throws Exception
    {
        return (String)param.get(id);
    }

    public String[] getParams()
    {
        String params[] = new String[param.size()];
        return (String[])param.values().toArray(params);
    }

    public String[] getParamNames()
    {
        String paramnames[] = new String[param.size()];
        return (String[])param.keySet().toArray(paramnames);
    }    
    
    public String getGridXmlDatas()
    {
        return recsb.toString();
    }
    
    public String getGridExcelData()
    {
    	return exdsb.toString();
    }
    
    public void setGridExcelHeadData(String header)
    {
    	exdsb.append(header);
    }
    public GridData getHeader(String string) {
		// TODO Auto-generated method stub
		return null;
	}
}