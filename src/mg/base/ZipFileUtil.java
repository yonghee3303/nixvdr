package mg.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.zip.ZipException;

import org.apache.commons.io.FileUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ZipFileUtil {
	
	protected int BUFFER_SIZE = 1024 * 4;
    protected Log log = LogFactory.getLog(this.getClass());

	public void zip(String sourcePath, String outZip)throws Exception{
		
		 try {
			 FileOutputStream fout = new FileOutputStream(outZip);
			 ZipOutputStream zout = new ZipOutputStream(fout);
			 zout.setEncoding("UTF-8");//한글깨짐 방지를 위한 인코딩 셋팅
			 File resultFolders = new File(sourcePath);
			 zipEntry(sourcePath, resultFolders, "", zout);
			 zout.close();
		 	} catch (Exception e) {
		 		throw e;
		 	}
		}
	 protected void zipEntry(String sourcePath, File sourceFile, String targetDir, ZipOutputStream zipOutStream) throws Exception {
			File files[] = sourceFile.listFiles();
			for(int i=0; i < files.length; i++){
				String filename = files[i].toString().substring(files[i].toString().lastIndexOf("\\")+1);
				if(files[i].isDirectory()){
					if(files[i].list().length ==0){ //빈 폴더 일 경우
						zipOutStream.putNextEntry(new ZipEntry(targetDir+"\\"+filename+"\\"));
					}else{
						String filePath = files[i].getPath();
						filePath = filePath.substring(sourcePath.length()+1, filePath.length());
						zipEntry(sourcePath, files[i], filePath, zipOutStream);
					}
				}else{
					
					byte[] buf = new byte[BUFFER_SIZE];
					FileInputStream fin = new FileInputStream(files[i]);
					zipOutStream.putNextEntry(new ZipEntry(targetDir+"\\"+filename ));//압축될 파일명
					 int length;
					 while((length = fin.read(buf)) > 0){
						 zipOutStream.write(buf, 0, length);
					 }
					 zipOutStream.closeEntry();
					 fin.close();
				}
			}
	}			
	public String getCharset(byte[] buf) throws UnsupportedEncodingException{
		if (buf.length >= 2 && ((buf[0] == -1 && buf[1] == -2) || (buf[0] == -2 && buf[1] == -1))) {
			 return "UTF-8";//"Unicode";
		}
		if (buf.length >= 3 && buf[0] == -17 && buf[1] == -69 && buf[2] == -65) {
		     return "UTF-8";//"UTF-8+";
		}
		int size = new String(buf, "UTF-8").getBytes("UTF-8").length;
		if (size == buf.length) {
		     return "UTF-8";
		}
		return "EUC-KR";//"MS949";
	}
	public String getCharset(File fileName){
		String charset ="UTF-8";
		ZipFile zipFile = null;
		try{
			 zipFile = new ZipFile(fileName);
			Enumeration entries = zipFile.getEntries();
			ZipEntry entry = (ZipEntry) entries.nextElement();
			String entryFileName = entry.getName();
			charset = getCharset(entryFileName.getBytes());			
			//log.info("getCharset-----------\n fileName : "+fileName.getParent()+"\n charset : "+charset);
		}catch(Exception e){
			log.error("getCharset------------------------\n : "+e.toString());
		} finally {
			try {
				if (zipFile != null)
					zipFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return charset;
	}
	public int unZip(String zipFileDir, String newDir) {
		//return ZipFileUtil.unZip(new File(zipFileDir), newDir);
		return this.unZip(new File(zipFileDir), newDir);
	}

	@SuppressWarnings("unchecked")
	public int unZip(File fileName) {
		Enumeration entries;
		ZipFile zipFile = null;
		try {
			//zipFile = new ZipFile(fileName, "EUC-KR");
			zipFile = new ZipFile(fileName, "UTF-8");
			entries = zipFile.getEntries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				String orgDirName = ((fileName.getParent() + File.separator).getBytes("UTF-8")).toString(); //다국어 지원을 위해 UTF-8로 지정, 사용자가 반드시 유니코드로 지정하여 압축 파일을 생성 해야함. 
				String entryFileName = ((entry.getName()).getBytes("UTF-8")).toString();
				//log.info("entryFileName------------------------->"+entryFileName);
				if (entry.isDirectory()) {
					(new File(orgDirName + entryFileName)).mkdir();
					continue;
				} else {
					
					String[] tmpSplit = null;
					try {
						tmpSplit = entryFileName.split(File.separator);
					} catch (Exception e) {
						tmpSplit = entryFileName.split("\\\\");
					}
					if (tmpSplit.length > 1) {
						String tmpDir = "";
						for (int i = 0; i < tmpSplit.length - 1; i++)
							tmpDir += (tmpSplit[i] + File.separator);
						tmpDir = orgDirName + tmpDir;
						File tmpFile = new File(tmpDir);
						if (!tmpFile.exists())
							tmpFile.mkdir();
					}
				}
				unzipEntry(zipFile.getInputStream(entry), new File(orgDirName + File.separator + entryFileName));
			}
		} catch (ZipException ze) {
			ze.printStackTrace();
			return 0;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			try {
				if (zipFile != null)
					zipFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return 1;
	}

	@SuppressWarnings("unchecked")
	public int unZip(File fileName, String newDir) {
		return unZip(fileName, newDir, false, null);
	}

	@SuppressWarnings("unchecked")
	public int unZip(File fileName, String newDir, boolean isImgExtChange, String[] extChangeList) {
		Enumeration entries;
		ZipFile zipFile = null;
		try {
			zipFile = new ZipFile(fileName, getCharset(fileName));
			entries = zipFile.getEntries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				String orgDirName = fileName.getParent() + File.separator;
				String entryFileName = entry.getName();
				//log.info("unZip-------------\n orgDirName : "+orgDirName+"\n entryFileName : "+entryFileName +"\n charset : "+getCharset(entryFileName.getBytes()));
				if (entry.isDirectory()) {
					(new File(newDir + "/" + entryFileName)).mkdirs();
					continue;
				} else {
					String[] tmpSplit = null;
					String tmpFileName = null;
					String tmpFileExt = null;
					
					try {
						tmpSplit = entryFileName.split(File.separator);
					} catch (Exception e) {
						tmpSplit = entryFileName.split("\\\\");
					}
					if (tmpSplit.length > 1) {
						String tmpDir = "";
						for (int i = 0; i < tmpSplit.length - 1; i++)
							tmpDir += (tmpSplit[i] + File.separator);
						tmpDir = orgDirName + tmpDir;
						File tmpFile = new File(tmpDir);
						if (!tmpFile.exists())
							tmpFile.mkdir();
					}
					if(isImgExtChange == true) {
						tmpFileName = tmpSplit[tmpSplit.length-1];
						tmpFileExt = tmpFileName.substring(tmpFileName.lastIndexOf(".")+1);
						
						for(int i=0; i<extChangeList.length; i++) {
							if(extChangeList[i].equalsIgnoreCase(tmpFileExt)) {
								entryFileName += "_";
								break;
							}
						}
					}
				}
				FileUtils.forceMkdir(new File(newDir));
				unzipEntry(zipFile.getInputStream(entry), new File(newDir + File.separator + entryFileName));
			}
		}catch(FileNotFoundException fne){
			
		} catch (ZipException ze) {
			ze.printStackTrace();
			return 0;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}finally {
			try {
				if (zipFile != null)
					zipFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return 1;
	}
	
	/**
     * Zip 파일의 한 개 엔트리의 압축을 푼다.
     *
     * @param zis - Zip Input Stream
     * @param filePath - 압축 풀린 파일의 경로
     * @return
     * @throws Exception
     */
	protected File unzipEntry(InputStream zis, File targetFile) throws Exception {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(targetFile);

            byte[] buffer = new byte[BUFFER_SIZE];
            int len = 0;
            while ((len = zis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        } catch(FileNotFoundException e) { //알집 10.70 버전의 경우 entry에 파일만 갖고오고, 경로는 갖고 오지 않아 아래와 같이 처리.
           // log.info("FileNotFoundException 발생----------------------------------"+targetFile);
        	String fullPath = targetFile.getAbsolutePath();
           String folderPath = fullPath.substring(0, fullPath.lastIndexOf(File.separator));
           (new File(folderPath)).mkdirs();
           
           fos = new FileOutputStream(targetFile);

           byte[] buffer = new byte[BUFFER_SIZE];
           int len = 0;
           while ((len = zis.read(buffer)) != -1) {
               fos.write(buffer, 0, len);
           }
           
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
        return targetFile;
    }
    
    /**
     * @param fileName - 압축 파일의 경로
     * @return 압축파일 카운트
     * @throws Exception
     * 압축파일 내부 파일 개수가 1개일 경우 파일명를 반환, 아닐 경우 null 반환
     */
    @SuppressWarnings("unchecked")
	public String innerFileNm(String fileName) {
		Enumeration entries;
		ZipFile zipFile = null;
		String fileNm = null;
		int count = 0;
		
		try {
			zipFile = new ZipFile(fileName, "UTF-8");
			entries = zipFile.getEntries();
			while (entries.hasMoreElements()) {
				if(count++ >= 1) {
					return null;
				}
				ZipEntry entry = (ZipEntry) entries.nextElement();
				fileNm = entry.getName();
			}
		} catch (ZipException ze) {
			ze.printStackTrace();
			return null;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (zipFile != null)
					zipFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fileNm;
	}
    
    /**
     * @param fileName - 압축 파일의 경로
     * @return 압축해제 허용 여부 (true:원본파일 다운로드, false:압축파일 다운로드)
     * @throws Exception
     * maxSize가 -1일 경우 해당 기능 비활성화
     */
    @SuppressWarnings("unchecked")
	public boolean zipSizeCheck(String fileName, int maxSize) {
    	
    	boolean result = true;
    	
    	if(maxSize != -1) {
    		File file = new File(fileName);
    		if(file.exists()) {
	    		long fileSize = file.length();
	    		if(fileSize > maxSize) {
	    			result = false;
	    		}
	    	} else {
	    		result = false;
	    	}
    	}
    	
		return result;
	}
}