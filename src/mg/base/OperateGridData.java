// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   OperateGridData.java

package mg.base;

import java.io.OutputStream;
import java.io.Writer;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.framework.core.IRequest;
import com.core.component.util.TokenizerUtil;
import mg.base.DataFormater;

import com.core.base.ComponentRegistry;
import com.core.component.parameter.IParameterManagement;

// Referenced classes of package xlib.cmc:
//            GridData, GridHeader

public class OperateGridData
{
	IParameterManagement parameter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	private static final Log log = LogFactory.getLog("OperateGridData.class"); 
	private static String FieldChar = "~";
	private static String LineChar = "|";
	
	public OperateGridData()
    {
    }
	
	
	public static GridData parse2(HttpServletRequest req) throws Exception
	{
		Enumeration param_en = req.getParameterNames();
	    GridData th = new GridData();
	    String parameter = "";
	    String ids_info = req.getParameter("ids") == null ? "" : req.getParameter("ids");
	    String cols_ids = req.getParameter("col_ids") == null ? "" : req.getParameter("cols_ids");
	    String grid_col_id = req.getParameter("grid_col_id") == null ? "" : req.getParameter("grid_col_id");
	    String col_id   = "";
		String ids_data = "";
		String field_data = "";
		String ex_header = "";
		TokenizerUtil st = null;
		String[] col_names  = null;
		String[] row_depths = null;
		int col_depth = 0;
		int row_depth = 0;
		
		try
	    {
			//req.setCharacterEncoding("UTF-8");
	        //res.setContentType("text/xml;charset=UTF-8");
			// xml_res = res;
			
			if(grid_col_id != null && grid_col_id.trim().length() > 0) {
				ex_header = grid_col_id.replaceAll(",", FieldChar + FieldChar + LineChar);
				th.setGridExcelHeadData(ex_header);
			}
			
			if(ids_info != null && ids_info.trim().length() > 0) {
				th.setSelectable(false);
			}
			
			if(!th.getSelectable()) {
				if(ids_info != null && ids_info.trim().length() > 0 && cols_ids != null && cols_ids.trim().length() > 0) {
					
					if(cols_ids.indexOf(",") > 0) {
						ids_data = cols_ids.replaceAll(",", FieldChar + FieldChar + LineChar);
					}
					
					if(ids_info.indexOf(",") > 0) {
						TokenizerUtil sg_rows = new TokenizerUtil(ids_info, ",");
						row_depths = new String[sg_rows.countTokens()];
						
						for(row_depth=0; row_depth < row_depths.length; row_depth++) {
							row_depths[row_depth] = sg_rows.nextToken();
						}
					} else {
						row_depths = new String[1];
						row_depths[0] = ids_info;
					}
					
					TokenizerUtil sg_columns = new TokenizerUtil(cols_ids, ",");
					col_names = new String[sg_columns.countTokens()];
					
					for(col_depth=0; col_depth < col_names.length; col_depth++) {
						col_names[col_depth] = sg_columns.nextToken();
					}
					
					for(row_depth = 0; row_depth < row_depths.length; row_depth++) {
						for(col_depth=0; col_depth < col_names.length; col_depth++) {
							field_data = req.getParameter(row_depths[row_depth] + "_" + col_names[col_depth]) + FieldChar;
							ids_data  += field_data;
						}
						ids_data += LineChar;
					}
					
					th.setRecvDataFormater(ids_data);
				}
			}
			
	    	while(param_en.hasMoreElements()) {
				parameter = (String)param_en.nextElement();
				th.addParam(parameter, req.getParameter(parameter));
			}
	    }
	    catch(Exception e)
	    {
	        throw e;
	    }
	    return th;
	}
    
    public static GridData parse(IRequest request)    throws Exception
	{
		Enumeration param_en = request.getParameterNames();
		
	    GridData th = new GridData();
	    String parameter = "";
	    String ids_info = request.getStringParam("ids") == null ? "" : request.getStringParam("ids");
	    String cols_ids = request.getStringParam("col_ids") == null ? "" : request.getStringParam("col_ids");
	    String grid_col_id = request.getStringParam("grid_col_id") == null ? "" : request.getStringParam("grid_col_id");
	    String col_id   = "";
		String ids_data = "";
		String field_data = "";
		String ex_header = "";
		TokenizerUtil st = null;
		String[] col_names  = null;
		String[] row_depths = null;
		int col_depth = 0;
		int row_depth = 0;
		
		
		try
	    {
			// request.setCharacterEncoding("UTF-8");
	        // res.setContentType("text/xml;charset=UTF-8");
			
			if(grid_col_id != null && grid_col_id.trim().length() > 0) {
				ex_header = grid_col_id.replaceAll(",", FieldChar + FieldChar + LineChar);
				th.setGridExcelHeadData(ex_header);
			}
			
			if(ids_info != null && ids_info.trim().length() > 0) {
				th.setSelectable(false);
			}
			
			if(!th.getSelectable()) {
				log.error("=====th.getSelectable()====ids_data=========>\n" + th.getSelectable() );
				
				log.error("=====ids_info===========>\n" + ids_info + " , " + ids_info.trim().length());
				log.error("=====cols_ids===========>\n" + cols_ids + " , " + cols_ids.trim().length());
				
				if(ids_info != null && ids_info.trim().length() > 0 && cols_ids != null && cols_ids.trim().length() > 0) {
					
					if(cols_ids.indexOf(",") > 0) {
						ids_data = cols_ids.replaceAll(",",  FieldChar + FieldChar + LineChar);
					}
					
					log.error("=========ids_data=========>\n" + ids_data );
					if(ids_info.indexOf(",") > 0) {
						TokenizerUtil sg_rows = new TokenizerUtil(ids_info, ",");
						row_depths = new String[sg_rows.countTokens()];
						
						for(row_depth=0; row_depth < row_depths.length; row_depth++) {
							row_depths[row_depth] = sg_rows.nextToken();
						}
					} else {
						row_depths = new String[1];
						row_depths[0] = ids_info;
					}
					
					TokenizerUtil sg_columns = new TokenizerUtil(cols_ids, ",");
					col_names = new String[sg_columns.countTokens()];
					
					for(col_depth=0; col_depth < col_names.length; col_depth++) {
						col_names[col_depth] = sg_columns.nextToken();
					}
					
					for(row_depth = 0; row_depth < row_depths.length; row_depth++) {
						for(col_depth=0; col_depth < col_names.length; col_depth++) {
							field_data = request.getStringParam(row_depths[row_depth] + "_" + col_names[col_depth]) + FieldChar;
							ids_data  += field_data;
						}
						ids_data += LineChar;
					}
					
					log.error("=====rowcount====ids_data=========>\n" + ids_data );
					th.setRecvDataFormater(ids_data);
				}
			}
			
	    	while(param_en.hasMoreElements()) {
				parameter = (String)param_en.nextElement();
				th.addParam(parameter, request.getStringParam(parameter));
			}
	    }
	    catch(Exception e)
	    {
	        throw e;
	    }
	    return th;
	}
	    
    
    public static void write(HttpServletRequest req, HttpServletResponse res, GridData th, Writer out)
    throws Exception
    {
    	write(req, res, th, out, "");
    }
    
    public static void write(HttpServletRequest req, HttpServletResponse res, GridData th, Writer out, String grid_obj)
        throws Exception
    {
        StringBuffer recsb  = new StringBuffer();
        String [] res_params= null;
        String [] res_param_names = null;
        StringReader sr_xml = null;
        DataFormater sf_excel;
        String menu_flag    = "";
    	String data         = "";
    	String column       = "";
    	String value        = "";
    	String excel_zip_str= "";
    	int data_cnt        = 0;
    	
        try
        {
        	recsb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        	res_params       = th.getParams();
        	res_param_names  = th.getParamNames();
        	
            if(th.getSelectable()) {
            	//if(th.getResTotalCount() > 0) {
            		//recsb.append("<rows pos=\"\" total_count=\""+th.getResTotalCount()+"\">");
            	//} else recsb.append("<rows>");
            	
            	recsb.append("<rows>");
            	
            	if(th.getGridXmlDatas().length() > 0) {
            		recsb.append(th.getGridXmlDatas());
                	recsb.append("</row>");
            	}
            	
            	recsb.append("<userdata name=\"message\">"+th.getMessage()+"</userdata>");
				recsb.append("<userdata name=\"data_type\">doSelect</userdata>");
				recsb.append("<userdata name=\"status\">"+th.getStatus()+"</userdata>");
				
				for(int k=0; k < res_param_names.length; k++) 
            	{
					recsb.append("<userdata name=\""+res_param_names[k]+"\">"+res_params[k]+"</userdata>");
            	}
				
				excel_zip_str = th.getGridExcelData();
				
				sf_excel = new DataFormater(th.getGridExcelData()+ LineChar);
				
				if(grid_obj != null && grid_obj.length() > 0) {
					req.getSession().putValue(grid_obj+"excel_data", sf_excel);
				} else {
					req.getSession().putValue("excel_data", sf_excel);
				}
				
				//recsb.append("<userdata name=\"excel_data\">"+excel_zip_str+"</userdata>");
				recsb.append("</rows>");
				
        	} else {
        		recsb.append("<data>");
        		recsb.append("<action type=\"doSaveEnd\" ");
	    		recsb.append(" message = \""+th.getMessage()+"\" ");
				recsb.append(" data_type = \"doData\" ");
				recsb.append(" status = \""+th.getStatus()+"\" ");
				
				for(int k=0; k < res_param_names.length; k++) 
            	{
					recsb.append(" "+res_param_names[k]+" = \""+res_params[k]+"\" ");
            	}
	    		
				recsb.append("></action>");
	    		recsb.append("</data>");
        	}
            
            /*
            res.reset() ;
            res.setContentType("application/smnet;");
            res.setHeader ("Content-Type", "application/smnet; charset=UTF-8");
            res.setHeader ("Content-Disposition", "attachment; filename=servlet.xml" );
            res.setHeader ("Content-Transfer-Encoding", "binary"); 
            res.setHeader ("Content-Length", ""+recsb.toString().length());
            */
            
            res.setHeader ("Content-Length", ""+recsb.toString().getBytes("UTF-8").length);
            
            sr_xml = new StringReader(recsb.toString());
            char cbuf[] = new char[1024];
            int leng = 0;
            
            while((leng = sr_xml.read(cbuf)) > 0) {
            	out.write(cbuf, 0, leng);
            }
        }
        catch(Exception e)
        {
            throw e;
        } finally {
        	//out.write(recsb.toString());
        	out.flush();
        	out.close();
        }
    }
    
    public static void TreeWrite(HttpServletRequest req, HttpServletResponse res, GridData th, Writer out, String grid_obj)
    throws Exception
	{
	    StringBuffer recsb  = new StringBuffer();
	    String [] res_params= null;
	    String [] res_param_names = null;
	    StringReader sr_xml = null;
	    DataFormater sf_excel;
	    String menu_flag    = "";
		String data         = "";
		String column       = "";
		String value        = "";
		String excel_zip_str= "";
		int data_cnt        = 0;
		
	    try
	    {
	    	recsb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	    	res_params       = th.getParams();
	    	res_param_names  = th.getParamNames();
	    	
	        if(th.getSelectable()) {
	        	//if(th.getResTotalCount() > 0) {
	        		//recsb.append("<rows pos=\"\" total_count=\""+th.getResTotalCount()+"\">");
	        	//} else recsb.append("<rows>");
	        	
	        	recsb.append("<rows>");
	        	
	        	if(th.getGridXmlDatas().length() > 0) {
	        		recsb.append(th.getGridXmlDatas());
	        	}
	        	
	        	recsb.append("<userdata name=\"message\">"+th.getMessage()+"</userdata>");
				recsb.append("<userdata name=\"data_type\">doSelect</userdata>");
				recsb.append("<userdata name=\"status\">"+th.getStatus()+"</userdata>");
				
				for(int k=0; k < res_param_names.length; k++) 
	        	{
					recsb.append("<userdata name=\""+res_param_names[k]+"\">"+res_params[k]+"</userdata>");
	        	}
				
				excel_zip_str = th.getGridExcelData();
				sf_excel = new DataFormater(th.getGridExcelData());
				
				if(grid_obj != null && grid_obj.length() > 0) {
					req.getSession().putValue(grid_obj+"excel_data", sf_excel);
				} else {
					req.getSession().putValue("excel_data", sf_excel);
				}
				
				//recsb.append("<userdata name=\"excel_data\">"+excel_zip_str+"</userdata>");
				recsb.append("</rows>");
				
	    	} else {
	    		recsb.append("<data>");
	    		recsb.append("<action type=\"doSaveEnd\" ");
	    		recsb.append(" message = \""+th.getMessage()+"\" ");
				recsb.append(" data_type = \"doData\" ");
				recsb.append(" status = \""+th.getStatus()+"\" ");
				
				for(int k=0; k < res_param_names.length; k++) 
	        	{
					recsb.append(" "+res_param_names[k]+" = \""+res_params[k]+"\" ");
	        	}
	    		
				recsb.append("></action>");
	    		recsb.append("</data>");
	    	}
	        
	        /*
	        res.reset() ;
	        res.setContentType("application/smnet;");
	        res.setHeader ("Content-Type", "application/smnet; charset=UTF-8");
	        res.setHeader ("Content-Disposition", "attachment; filename=servlet.xml" );
	        res.setHeader ("Content-Transfer-Encoding", "binary"); 
	        res.setHeader ("Content-Length", ""+recsb.toString().length());
	        */
	        
	        res.setHeader ("Content-Length", ""+recsb.toString().getBytes("UTF-8").length);
	        
	        sr_xml = new StringReader(recsb.toString());
	        char cbuf[] = new char[1024];
	        int leng = 0;
	        
	        while((leng = sr_xml.read(cbuf)) > 0) {
	        	out.write(cbuf, 0, leng);
	        }
	    }
	    catch(Exception e)
	    {
	        throw e;
	    } finally {
	    	//out.write(recsb.toString());
	    	out.flush();
	    	out.close();
	    }
	}
    
    public static GridData cloneResponseGridData(GridData gdReq)
        throws Exception
    {
        GridData gdRes = new GridData();
        gdRes.setGridExcelHeadData(gdReq.getGridExcelData());
        return gdRes;
    }

    private static void ZipWrite(byte senddata[], OutputStream out)
        throws Exception
    {
        //out.write("WISEGRIDDATAJ=".getBytes());
        ZipOutputStream zout = new ZipOutputStream(out);
        zout.putNextEntry(new ZipEntry("data.xml"));
        zout.setLevel(1);
        zout.write(senddata);
        zout.closeEntry();
        zout.close();
    }

    private static String padding(String pad, String value)
    {
        if(pad.length() < value.length())
            return "";
        else
            return pad.substring(0, pad.length() - value.length()) + value;
    }

    private static String[] splitTokenData(String tokendata, String delm)
    {
        int idx = tokendata.indexOf(delm);
        String ret[] = new String[2];
        ret[0] = tokendata.substring(0, idx);
        ret[1] = tokendata.substring(idx + 1, tokendata.length());
        return ret;
    }    
}