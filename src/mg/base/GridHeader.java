// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2009-03-05 오후 5:42:52
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   GridHeader.java

package mg.base;

import java.util.Vector;

// Referenced classes of package xlib.cmc:
//            GridValue

public class GridHeader
{

    public GridHeader(String id, String datatype)
    {
        imageurl = null;
        listvalues = null;
        listhiddenvalues = null;
        this.id = id;
        this.datatype = datatype;
        vt = new Vector(1000, 1000);
    }

    public void addValue(String value, String hiddenvalue)
        throws Exception
    {
        if(datatype.equals("L") || datatype.equals("I"))
        {
            throw new Exception("(" + id + ") Not available data type!");
        } else
        {
            vt.addElement(new GridValue(value, hiddenvalue));
            return;
        }
    }

    public void addValue(String value, String hiddenvalue, int imageindex)
        throws Exception
    {
        if(!datatype.equals("I"))
        {
            throw new Exception("(" + id + ") Not available data type!");
        } else
        {
            vt.addElement(new GridValue(value, hiddenvalue, imageindex));
            return;
        }
    }

    public void setComboValues(String values[], String hiddenvalues[])
        throws Exception
    {
        if(!datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        if(values.length != hiddenvalues.length)
            throw new Exception("check combo data length!");
        listvalues = new String[values.length];
        listhiddenvalues = new String[hiddenvalues.length];
        for(int i = 0; i < values.length; i++)
        {
            listvalues[i] = values[i];
            listhiddenvalues[i] = hiddenvalues[i];
        }

    }

    public void setImageURLs(String imageurls[])
        throws Exception
    {
        if(!datatype.equals("I"))
            throw new Exception("(" + id + ") Not available data type!");
        imageurl = new String[imageurls.length];
        for(int i = 0; i < imageurl.length; i++)
            imageurl[i] = imageurls[i];

    }

    public void addSelectedIndex(int idx)
        throws Exception
    {
        if(!datatype.equals("L"))
        {
            throw new Exception("(" + id + ") Not available data type!");
        } else
        {
            vt.addElement(String.valueOf(idx));
            return;
        }
    }

    public void addSelectedHiddenValue(String hiddenvalue)
        throws Exception
    {
        if(!datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        int idx = -1;
        if(listhiddenvalues != null)
        {
            for(int i = 0; i < listhiddenvalues.length; i++)
            {
                if(!listhiddenvalues[i].equals(hiddenvalue))
                    continue;
                idx = i;
                break;
            }

        }
        vt.addElement(String.valueOf(idx));
    }

    public String getID()
    {
        return id;
    }

    public String getDataType()
    {
        return datatype;
    }

    public String getValue(int idx)
        throws Exception
    {
        if(datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return ((GridValue)vt.get(idx)).value;
    }

    public String[] getComboValues()
        throws Exception
    {
        if(!datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return listvalues;
    }

    public String getHiddenValue(int idx)
        throws Exception
    {
        if(datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return ((GridValue)vt.get(idx)).hiddenvalue;
    }

    public String[] getComboHiddenValues()
        throws Exception
    {
        if(!datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return listhiddenvalues;
    }

    public String[] getImageURLs()
        throws Exception
    {
        if(!datatype.equals("I"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return imageurl;
    }

    public int getImageURLIndex(int idx)
        throws Exception
    {
        if(!datatype.equals("I"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return ((GridValue)vt.get(idx)).imageindex;
    }

    public int getSelectedIndex(int idx)
        throws Exception
    {
        if(!datatype.equals("L"))
            throw new Exception("(" + id + ") Not available data type!");
        else
            return Integer.parseInt((String)vt.get(idx));
    }

    public int getRowCount()
    {
        return vt.size();
    }

    private Vector vt;
    private String datatype;
    private String id;
    private String imageurl[];
    private String listvalues[];
    private String listhiddenvalues[];
}