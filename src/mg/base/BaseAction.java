/*
 * Copyright (c) 2010 Genetics. All rights reserved.
 *
 * This software is the confidential and proprietary information of Genetics.
 * You shall not disclose such Confidential Information and shall use it
 * only in accordance wih the terms of the license agreement you entered into
 * with Genetics.
 */

package mg.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.base.Constants;
import com.core.base.SystemException;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.naming.INamingManagement;
import com.core.component.navigation.PageList;
import com.core.component.parameter.IParameterManagement;
import com.core.component.sql.DuplicationException;
import com.core.component.transaction.ITransactionManagement;
import com.core.component.util.BaseUtils;
import com.core.component.variablexml.IVariableXml;
import com.core.component.variablexml.IVariableXmlResponse;
import com.core.component.variablexml.IVariableXmlTree;
import com.core.component.variablexml.internal.VariableXml;
import com.core.component.variablexml.internal.VariableXmlResponse;
import com.core.component.variablexml.internal.VariableXmlTree;
import com.core.framework.action.IAction;
import com.core.framework.core.Action;
import com.core.framework.core.IExceptionManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
* 업무 그룹명 : mg.base
* 서브 업무명 : BaseAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 19
* 설 명 : IAction 컴포넌트 기본 구현 클래스.
*/
public abstract class BaseAction extends Action {

    private static int defaultListSize = -1;

    /**
    * PARAM_TYPES
    */
    @SuppressWarnings("unchecked")
	protected static final Class[] PARAM_TYPES = {IRequest.class};

    /**
    * BASE_PACKAGE_NAME
    */
    protected static final String BASE_PACKAGE_NAME = "MyGuard";
    
	//다국어 처리
    public static final List<String> languageList = Arrays.asList(new String[]{ "KO", "EN", "JP" });

    protected Log log = LogFactory.getLog(this.getClass());

    protected ITransactionManagement tx = (ITransactionManagement) getComponent(ITransactionManagement.class);

    protected INamingManagement nm = (INamingManagement) getComponent(INamingManagement.class);

    protected IExceptionManagement em = (IExceptionManagement) getComponent(IExceptionManagement.class);
    

    //메일발송 핸들러
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");

    /**
    * @param request
    * @return
    * @throws Exception
    */
    public abstract IResponse run(IRequest request) throws Exception;

    /**
    * @param request
    * @return
    * @throws Exception
    */
    public IResponse execute(IRequest request) throws Exception {
    	
    	if(request.getStringParam("mod") == null) {
    	   log.info("[BaseAction] mod is null");
    	}
    	else {
    		log.info("[BaseAction] mod --> " + request.getStringParam("mod"));
    	}
    	
    	if(!request.getStringParam("mod").equals("uploadData")) request.uploadFile();
        IResponse res = null;
        String mode = request.getStringParam("mode", "");
        if (mode.startsWith("tx")) {
            try {
                tx.begin();
                res = callMethod(request, this);
                tx.commit();
            } catch (Exception e) {
                request.rollbackFileUpload();
                tx.rollback();
                throw e;
            }
        } else {
            try {
                if (isModeRequest(request)) {
                    res = callMethod(request, this);
                } else {
                    res = run(request);
                }
           } catch (Exception e) {
               throw e;
           }
        }

        return res;
    }

    /**
    * @param request
    * @return
    */
    protected boolean isModeRequest(IRequest request) {
        return request.hasParameter(Constants.MODE)
                && request.findRequestObject(Constants.MODE) == null;

    }

    /**
    * @param request
    * @param action
    * @return
    * @throws Exception
    */
    protected IResponse callMethod(IRequest request, IAction action)
            throws Exception {
        Method method = action.getClass().getMethod(
                request.getStringParam(Constants.MODE), PARAM_TYPES);
        return (IResponse) method.invoke(action, new Object[] { request });
    }

    /**
    * @param list
    * @return
    */
    @SuppressWarnings("unchecked")
	protected List getIdList(List list) {
        List newList = new ArrayList();
        String id = null;
        for (int i=0; i < list.size(); i++) {
            id = ((String)((Map)list.get(i)).get("ID"));
            newList.add(id);
        }
        return newList;
    }

    /**
     * ICodeManagement 컴포넌트 인스턴스
     */
    private ICodeManagement cm = (ICodeManagement) getComponent(ICodeManagement.class);

    /**
     * IParameterManagement 컴포넌트 인스턴스
     */
    protected IParameterManagement pm = (IParameterManagement) getComponent(IParameterManagement.class);

    /**
     * 파라미터 값을 반환한다.
     * 
     * @param key
     *            파라미터 키
     * @return 파라미티 값
     */
    protected String getParameter(String key) {
        return pm.getString(key);
    }

    /**
     * ICode 객체를 반환한다.
     * 
     * @param id
     *            코드 아이디
     * @return ICode 코드
     */
    protected ICode getCode(String id) {
        return cm.getCode(id);
    }

    /**
     * IUser 객체를 추가한 Map을 반환한다.
     * 
     * @param request
     *            IRequest 사용자 요청
     * @return Map IUser 객체를 추가한 사용자 요청 파라미터
     */
    @SuppressWarnings("unchecked")
	protected Map getAuditableMap(IRequest request) {
        Map result = request.getMap();
        result.put(Constants.USER, request.getUser());
        // Constatns.USER = "j_user"
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        result.put(Constants.TIMESTAMP, formatter.format(new Date()));
        // Constants.TIMESTAMP = "j_timeStamp"
        return result;
    }

    /**
     * IUser 객체를 추가한 FormattedMap을 반환한다.
     * 
     * @param request
     *            IRequest 사용자 요청
     * @return Map IUser 객체를 추가한 사용자 요청 파라미터
     */
    @SuppressWarnings("unchecked")
	protected Map getAuditableFormattedMap(IRequest request) {
        Map result = request.getFormattedMap();
        result.put(Constants.USER, request.getUser());
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        result.put(Constants.TIMESTAMP, formatter.format(new Date()));
        return result;
    }


    /**
     * 오라클 ROWNUM을 이용해서 페이지 네비페이션 처리를 한다.
     * 
     * @param mapStmtName
     *            SQL ID
     * @param query
     *            검색 조건
     * @param pageNo
     *            페이지 번호
     * @return 검색 결과
     */
    @SuppressWarnings("unchecked")
	protected List getOraclePageList(String mapStmtName, Map query, int pageNo) {
        if (defaultListSize == -1) {
            defaultListSize = ((IParameterManagement) getComponent(IParameterManagement.class)).getInt("component.navigation.defaultListSize");
        }
        return getOraclePageList(mapStmtName, query, pageNo, defaultListSize);
    }

    /**
     * 오라클 ROWNUM을 이용해서 페이지 네비페이션 처리를 한다.
     * 
     * @param mapStmtName
     *            SQL ID
     * @param query
     *            검색 조건
     * @param pageNo
     *            페이지 번호
     * @param listSize
     *            목록 크기
     * @return 검색 결과
     */
    @SuppressWarnings("unchecked")
	protected List getOraclePageList(String mapStmtName, Map query, int pageNo,
            int listSize) {
        pageNo = pageNo < 1 ? 1 : pageNo;
        query.put("firstRowIndex", new Integer((pageNo - 1) * listSize + 1));
        query.put("lastRowIndex", new Integer(pageNo * listSize));
        query.put("listSize", new Integer(listSize));
        return new PageList(getList(mapStmtName, query), getTotalCount(
                mapStmtName, query), pageNo, listSize);
    }

    /**
    * @param mapStmtName
    * @param query
    * @return
    */
    private int getTotalCount(String mapStmtName, Object query) {
        return ((Integer) getItem(mapStmtName + "Count", query)).intValue();
    }

    /**
     * 사용자가 검색을 요청했는지에 대한 여부를 반환한다.
     * 
     * @param request
     *            사용자 요청
     * @return 검색 여부
     */
    protected boolean isQuery(IRequest request) {
        return request.getBooleanParam("isQuery");
    }

    /**
     * Map에 있는 객체를 String으로 전환한다.
     * 
     * @param map java.util.Map
     * @param key 키
     * @return null인 경우에는 null을 반환
     */
    @SuppressWarnings("unchecked")
	protected String getStringWithNullCheck(Map map, String key) {
        Object result = map.get(key);
        return (result == null) ? "" : result.toString();
    }

	protected String getStringNullCheck(String key) {
        return (key == null) ? "" : key.toString();
    }

    
    /**
    * @param request
    * @param cause
    * @return
    * @throws Throwable
    */
    @Override
	public IResponse handleException(IRequest request, Throwable cause) throws Throwable {

        @SuppressWarnings("unused")
		HttpServletRequest req = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);

        String message =  "N/A";

        // response 객체를 생성한다.
//        MiPlatformResponse miResponse = new MiPlatformResponse(request);


        if (cause != null) {
            // cause로부터 원래의 Exception클래스를 찾아옴.
            Throwable realCause = BaseUtils.getRootCause(cause);

            if ( realCause == null ) {
                realCause = cause;
            }

            // Exception에따라 ERROR 코드 설정.
            if ( realCause instanceof DuplicationException
                    || cause instanceof DuplicationException ) {
            	;
                //  Duplication 오류
//                miResponse.responseCode("-1", pm.getString(Mp3rdConstants.ERROR), "데이타 중복 오류 입니다.");

            } else {

                String msg = realCause.getMessage();

                if ( msg == null || msg.trim().length() <= 0 || msg.equalsIgnoreCase("null") ) {

                    msg = "시스템 장애입니다\n관리자에게 문의하세요.";

                } else {

                    String tmpMsg = msg.toUpperCase();

                    if ( tmpMsg.startsWith("ORA-00001") ) {

                        msg = "데이타 중복 오류 입니다.";

                    } else if ( tmpMsg.startsWith("ORA") ) {

                        // 오라클 오류중 ORA-00001가 아니면...
                        msg = "시스템 장애입니다\n관리자에게 문의하세요.";

                    } else {

                        String errorCode = realCause.getMessage();
                        String[] params
                            = (realCause instanceof SystemException)?
                                ((SystemException)realCause).getMessageParameter() : null;
                        msg = mm.getMessage(errorCode, params);

                    }
                }
//                miResponse.responseCode("-1", pm.getString(Mp3rdConstants.ERROR), msg);
            }

            // 오류 로그를 작성하기 위한 메세지 찾기
            String errorCode = realCause.getMessage();
            String[] params
                = (realCause instanceof SystemException)?
                    ((SystemException)realCause).getMessageParameter() : null;
            String detail = mm.getMessage(errorCode, params);

            if (detail != null) {
                message = detail.equals(errorCode) ? detail : "[" + errorCode + "] " + detail;
            } else {
                message = realCause.getMessage();
                if (message == null) {
                    message = realCause.toString();
                }
            }

            // WAS log 기록
            realCause.printStackTrace();

            // DB에 log 기록
            //if ( errorCode != null || errorCode.equals(detail) ) {
            //em.logError(req, message, realCause);
            //}
        }

//        miResponse.response();

        return skip();
    }

    /**
     * VariableXml 가능하게 하는 XML 파일을 출력한다.
     * @return IResponse
     */
    public IVariableXmlResponse variablexmlResponse() {
        return new VariableXmlResponse();
    }

    /**
     * VariableXml 가능하게 하는 XML 파일을 출력한다.
     * @param code 코드
     * @param message 메시지 
     * @return IResponse
     */
    public IVariableXmlResponse variablexmlResponse(String code, String message) {
        return new VariableXmlResponse(code, message);
    }

    /**
     * VariableXml 가능하게 하는 Tree XML 파일을 출력한다.
     * @return IResponse
     */
    public IVariableXmlTree variablexmlTree() {
        return new VariableXmlTree();
    }

    /**
     * VariableXml 가능하게 하는 XML 파일을 출력한다.
     * @return IResponse
     */
    public IVariableXml variablexml() {
        return new VariableXml();
    }
    
    /**
    * @param request
    * @param str
    * @return
    */
    @SuppressWarnings("unchecked")
	public List getGridUpdatedList(IRequest request,String str) {
        return parseGridData(request.getStringParam(str),"u");
    }

    /**
    * @param request
    * @param str
    * @return
    */
    @SuppressWarnings("unchecked")
	public List getGridDeletedList(IRequest request,String str) {
        return parseGridData(request.getStringParam(str), "d");
    }

    /**
    * @param request
    * @param str
    * @return
    */
    @SuppressWarnings("unchecked")
	public List getGridInsertedList(IRequest request,String str) {
        /*
        List list = parseGridData(request.getStringParam(str), "i");
        List result = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            if ("true".equalsIgnoreCase((String) map.get("chk"))) {
                result.add(list.get(i));
            }
        }
        return result;
        */
        return parseGridData(request.getStringParam(str), "i");
    }
    
    /**
    * @param request
    * @param str
    * @return
    */
    @SuppressWarnings("unchecked")
	public List getArrayList(IRequest request,String str) {
        return parseGridData(request.getStringParam(str), "x");
    }

    /**
	* @param data
	* @param type
	* @return
	*/
	@SuppressWarnings("unchecked")
	private List parseGridData(String data, String type) {
        String[] columns = data.split("▩");
        String[] indexes = columns[0].split("▦");
        List result = new ArrayList();
        Map item = null;
        String[] datas = new String[columns.length];
        for (int i = 1; i < columns.length; i++) {
            datas = columns[i].split("▦");
            item = new HashMap();
            for (int j = 0; j < indexes.length; j++) {
                if(j<datas.length) {
                    item.put(indexes[j], datas[j]);     
                }else {
                    item.put(indexes[j], "");
                }
            }
            if (("x").equals(type)) {
            	result.add(item);
            } else if (type.equalsIgnoreCase(datas[1])) {
                result.add(item);
            }
        }
        return result;
    }
	
    /**
	* @param data
	* @param type
	* @return
	*/
	@SuppressWarnings("unchecked")
	private List parseDhtmlData(IRequest request)
	{
		String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");

        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        List result = new ArrayList();
        Map item = null;
        
        for (int i = 1; i <= ids.length; i++) {       	
        	item = new HashMap();
 
       	 	for(int j=0; j < cols.length; j++ ) {
       	 		String tmps = request.getStringParam(i + "_" + cols[j]);
	            item.put(cols[j], tmps);     
         	}
	        result.add(item);
        }
        return result;
    }
	
    public String responseSelData(String msg, String status, String mode, String seldata) {
   	 String ret =  null;
   	 
   	 ret = "<?xml version='1.0' encoding='UTF-8'?>";
   	 ret += "<rows>";
   	 if(seldata.length() > 10) {
	    	 ret += seldata;
	    	 ret += "</row>";
	    	 ret += "<userdata name=\"message\">"+msg+"</userdata>";
	    	 ret += "<userdata name=\"data_type\">doSelect</userdata>";
	    	 ret += "<userdata name=\"status\">"+status+"</userdata>";
   	 } else {
	    	 ret += "<userdata name=\"message\">"+msg+"</userdata>";
	    	 ret += "<userdata name=\"data_type\">doSelect</userdata>";
	    	 ret += "<userdata name=\"status\">"+status+"</userdata>";
   	 }
   	 ret += "</rows>";
   	 return ret;
    }
    
    public String responseTranData(String msg, String status, String mode, String retfucntion) {
   	 String ret =  null;
   	 
   	 ret = "<?xml version='1.0' encoding='UTF-8'?>";
   	 ret += "<data>";
   	 ret += "<action type=\"doSaveEnd\" ";
   	 ret += " message=\"" + msg + "\" ";
   	 ret += " data_type=\"doSave\" ";
   	 ret += " status = \"" + status + "\" ";
   	 ret += " mode=\"" + mode + "\" ";
   	 ret += "></action>";
   	 ret += "</data>";
   
   	 return ret;
    }
    
    public String responseNimsFolderAuth(String msg, String status, String mode, String retfucntion) {
      	 String ret =  null;
      	 
      	 ret = "<?xml version='1.0' encoding='UTF-8'?>";
      	 ret += "<data>";
      	 ret += "<action type=\"doAuthEnd\" ";
      	 ret += " message=\"" + msg + "\" ";
      	 ret += " data_type=\"doAuth\" ";
      	 ret += " status = \"" + status + "\" ";
      	 ret += " mode=\"" + mode + "\" ";
      	 ret += "></action>";
      	 ret += "</data>";
      
      	 return ret;
       }
    
   public String responseSelectTreeFolder(String msg, String status, String mode, String seldata) {
      	 String ret =  null;

      	ret = "<?xml version='1.0' encoding='UTF-8'?>";
      	 ret += "<data>";
      	 ret += "<action type=\"doSelectTreeEnd\" ";
      	 ret += " message=\"" + msg + "\" ";
      	 ret += " data_type=\"doSave\" ";
      	 ret += " status = \"" + status + "\" ";
      	 ret += " mode=\"" + mode + "\" ";
      	 ret += "></action>";
      	 ret += "</data>";
      
      	 return ret;
       }
    
    public void makeExcelReport(String menu_id, IExcelDocument doc, List dList) 
    {
 	    Map hmap = new HashMap();
        Map colmap = null, map = null;
        List hList = null;
        int[] col_width;
        
        hmap.put("MENU_ID", menu_id);
        hList = getList("mg.close.excelHeader", hmap);
        col_width = new int[hList.size()];
        	
        for(int i=0; i < hList.size(); i++ ) {
	           	colmap =(Map) hList.get(i);
	           	doc.addTitleCell(this.getStringWithNullCheck(colmap, "COL_NM"));
	           	col_width[i] = Integer.parseInt(this.getStringWithNullCheck(colmap, "COL_WIDTH")) * 40;
        }
        doc.setColumnsWidth(col_width);
        
        for (int i=0; i < dList.size(); i++) {
        	   	map =(Map) dList.get(i);
        	   	doc.nextRow();
        	
         	for(int j=0; j < hList.size(); j++ ) {
         		colmap =(Map) hList.get(j);
         		
         		String tmp_cd = this.getStringWithNullCheck(colmap, "COL_CL_CD");
         		if("ron".equals(tmp_cd)) {
         			doc.addCSNumericCell(Integer.parseInt(this.getStringWithNullCheck(map, this.getStringWithNullCheck(colmap, "COL_ID"))));
         		} else {	
	            		doc.addAlignedCell(this.getStringWithNullCheck(map, this.getStringWithNullCheck(colmap, "COL_ID")), 
	            									get_align(this.getStringWithNullCheck(colmap, "COL_ALIGN")));
         		}
         	}
        }
    }
    
    public short get_align(String align)
    {
 	   short ret_align = CellStyleFactory.ALIGN_LEFT;

 	   if("left".equals(align)) {
			   ret_align = CellStyleFactory.ALIGN_LEFT; 
		   }else if("center".equals(align)) {
			   ret_align = CellStyleFactory.ALIGN_CENTER; 
		   }else if("right".equals(align)) {
			   ret_align = CellStyleFactory.ALIGN_RIGHT; 
		   }
 	   return ret_align;
    }
    
    /*
     * 랜덤한 문자열을 원하는 길이만큼 반환합니다.
     * 
     * @param length 문자열 길이
     * @return 랜덤문자열
     */
    public static String getRandomString(int length)
    {
      StringBuffer buffer = new StringBuffer();
      Random random = new Random();

      for (int i=0 ; i<length ; i++)
      {
        buffer.append(random.nextInt(9));
      }
      return buffer.toString();
    }
    
    /*
     *  DB에 따른 SQL Map 생성 함수
     */
    public String getDBName(String dbName)
    {
         if("mssql".equals(dbName)) return "";
         else return "_" + dbName;
    }
    

    public String getItemData(String sqlnm, Map item)
    {
  	  try {
  		  return (String)getItem(sqlnm, item);
  	  } catch(Exception e) {
  		  return "";
  	  }
    }
    
    public String readFile(String filename)
	{   	
    	String content = null;
 	   File file = new File(getClass().getClassLoader().getResource(filename).getFile()); //for ex foo.txt
 	   try {
 	       //FileReader reader = new FileReader(file); //한글깨짐
 		   InputStreamReader reader= new InputStreamReader(new FileInputStream(file),"UTF-8"); //한글깨지지않음
 		   char[] chars = new char[(int) file.length()];
 	       reader.read(chars);
 	       content = new String(chars);
 	       reader.close();
 	       log.info("------------------------------readFile");
 	   } catch (IOException e) {
 		   log.error("-----------------------------readFile Exception-----"+e.toString());
 	       e.printStackTrace();
 	   }
 	   return content;
	}
    
    // CP 문서번호 취득
    public String getCpDocNo(String prefix)
    {
	  	DateFormat docFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
	  	return prefix + docFormatter.format(new Date()) + getRandomString(6);
    }
    
    // VDR 문서번호 취득
    public String getMakeId(String prefix) 
    {
    	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    	String reqDate = formatter.format(new Date());
    	return prefix+reqDate+getRandomString(11);
	}
    
    // vdr access log insert
    protected void insertAccessLog(Map<String, Object> item) {
    	
    	try{
    		
    		if(null == item.get("USER_ID")) throw new Exception("ACCESS LOG ERROR ::: NOT EXIST USER_ID");
    		
    		if(null == item.get("ROOM_ID")) throw new Exception("ACCESS LOG ERROR ::: NOT EXIST ROOM_ID");
    		
    		if(null == item.get("LOG_CL_CD")) throw new Exception("ACCESS LOG ERROR ::: NOT EXIST LOG_CL_CD");
    		
    		String log_cl_cd = item.get("LOG_CL_CD").toString();
    		
    		if(item.get("LANG").equals("JP")) {item.put("LANG","JA");}
    		
    		if("F".equals(log_cl_cd.substring(0, 1))) {
    			if(null == item.get("FOLDER_ID")){
    				throw new Exception("ACCESS LOG ERROR ::: NOT EXIST FOLDER_ID");	
    			}else{
    				
    				String log_desc = "";
    				   							    				
    				if("FC".equals(log_cl_cd)) 		log_desc += mm.getMessage("CPMG_1243", item.get("FOLDER_NM"), new Locale((String) item.get("LANG"))); 
    				else if("FD".equals(log_cl_cd)) log_desc += mm.getMessage("CPMG_1244", item.get("FOLDER_NM"), new Locale((String) item.get("LANG")));
    				else if("FN".equals(log_cl_cd)) log_desc += item.get("FOLDER_NM_ORG") + " > " + mm.getMessage("CPMG_1245", item.get("FOLDER_NM") , new Locale((String) item.get("LANG")));
    				
    				item.put("LOG_DESC", log_desc);
    			}
    		}else if("D".equals(log_cl_cd.substring(0, 1))) {
    			if( null == item.get("FOLDER_ID") || null == item.get("DOC_ID") ){
    				throw new Exception("ACCESS LOG ERROR ::: NOT EXIST FOLDER_ID OR DOC_ID");	
    			}else{
    				
    				String log_desc = "";
    				
    				if("DC".equals(log_cl_cd)) 		 log_desc += mm.getMessage("CPMG_1246", item.get("DOC_NM"), new Locale((String) item.get("LANG"))); 
    				else if("DD".equals(log_cl_cd))  log_desc += mm.getMessage("CPMG_1247", item.get("DOC_NM"), new Locale((String) item.get("LANG"))); 
    				else if("DR".equals(log_cl_cd))  log_desc += mm.getMessage("CPMG_1248", item.get("DOC_NM"), new Locale((String) item.get("LANG"))); 
    				else if("DN".equals(log_cl_cd))  log_desc += item.get("DOC_NM_ORG") + " > " + mm.getMessage("CPMG_1249", item.get("DOC_NM"), new Locale((String) item.get("LANG"))); 
    				
    				item.put("LOG_DESC", log_desc);
    			}
    		}else if("E".equals(log_cl_cd.substring(0, 1))) {
    			if("ED".equals(log_cl_cd)) item.put("LOG_DESC", mm.getMessage("CPMG_1250", item.get("DOC_TITLE").toString(), new Locale((String) item.get("LANG"))));
    		}
    		
    		createObject("common.insertAccessLog" + getDBName(pm.getString("component.sql.database")), item);
    		
    	}catch (Exception e){
    		log.error("---------------------------------------------------------------\n" + e.toString());
		}
    }
    
    // vdr access log insert
    protected void insertVdrHistory(Map<String, Object> item) {
    	
    	try{
    		
    		if(null == item.get("USER_ID")) throw new Exception("ACCESS LOG ERROR ::: NOT EXIST USER_ID");
    		
    		if(null == item.get("ROOM_ID")) throw new Exception("ACCESS LOG ERROR ::: NOT EXIST ROOM_ID");   		
    		
    		createObject("common.insertVdrHistory" + getDBName(pm.getString("component.sql.database")), item);
    		
    	}catch (Exception e){
    		log.error("---------------------------------------------------------------\n" + e.toString());
		}
    }
    
       
    /**
     * 20191218 jnsim
     * 메일 발송을 위한 메소드 - VDR 알림 메세지 형태에 맞춤
     */
//    protected void sendMail2(String to, String subject, String content, int type) {
//    	
//    	AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
//    	
//    	String FROM = "jnshim@inzent.com";
//        String TO = "jnshim@inzent.com";
//        String SUBJECT = "Amazon SES test (AWS SDK for Java)";
//        String HTMLBODY = "<h1>Amazon SES test (AWS SDK for Java)</h1>"
//          + "<p>This email was sent with <a href='https://aws.amazon.com/ses/'>"
//          + "Amazon SES</a> using the <a href='https://aws.amazon.com/sdk-for-java/'>" 
//          + "AWS SDK for Java</a>";
//       
//        
//        SendEmailRequest request = new SendEmailRequest()
//              .withDestination(
//                  new Destination().withToAddresses(TO) // 받는 사람
//              )
//              .withMessage(new Message()
//            		  .withBody(new Body()
//            				  .withHtml(new Content()
//            						  .withCharset("UTF-8").withData(HTMLBODY)))
//            		  .withSubject(new Content()
//        				  .withCharset("UTF-8").withData(SUBJECT)))
//    				  .withSource(FROM);
//        
//        client.sendEmail(request);
//          
//    }    
    
    
    // 메일 테스트 용도
    protected void sendMailTest() {
    	sendMail("jnshim@inzent.com", "Test", "로컬", 1, "KO");
    }
    
    /**
     * 20191218 jnsim
     * 메일 발송을 위한 메소드 - VDR 알림 메세지 형태에 맞춤
     */    
    //type 
	  //1.가입초대 
	  //2.비밀번호 재설정 인증번호
	  //3.룸에 신규 문서 등록
	  //4.본인작성 문서에 코멘트 추가
	  //5.본인작성 문서의 버전업
	  //6.회원가입 인증 메일
	  //99.계약번호
    protected void sendMail(String to, String subject, String content, int type, String lang) {
    	
    	log.info("sendMail call!");
    	if(lang.equals("JP")) lang = "JA";
    	

    	String FROM = "ixvdr@inzent.com";
    	String FROMNAME = "iXVDR Admin";

    	if(type == 7) to = "kiyh@inzent.com";
    	
    	String HOST = pm.getString("component.smtp.hostName");
    	String SMTP_USERNAME = pm.getString("component.smtp.username");
    	String SMTP_PASSWORD = pm.getString("component.smtp.password"); 
    	
    	//25 or 587
    	int PORT = 587;
    	//int PORT = 25;

    	// Create a Properties object to contain connection configuration information.
    	Properties props = System.getProperties();
    	props.put("mail.transport.protocol", "smtp");
    	props.put("mail.smtp.port", PORT); 
    	
    	//props.put("mail.smtp.ssl.enable", "true"); //the default value is false if not set
        props.put("mail.smtp.auth", "true");
        //props.put("mail.smtp.auth.login.disable", "true");  //the default authorization order is "LOGIN PLAIN DIGEST-MD5 NTLM". 'LOGIN' must be disabled since Email Delivery authorizes as 'PLAIN'
        props.put("mail.smtp.starttls.enable", "true");   //TLSv1.2 is required
        props.put("mail.smtp.starttls.required", "true");  //Oracle Cloud Infrastructure required
  
    	
    	//props.put("mail.smtp.auth", "true");
    	//props.put("mail.smtp.socketFactory.port", "465");  
    	//props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
    	//props.put("mail.smtp.debug", "true");

    	// Create a Session object to represent a mail session with the specified properties. 
    	Session session = Session.getDefaultInstance(props);
    	session.setDebug(true);

    	// Create a message with the specified information. 
    	MimeMessage msg = new MimeMessage(session);
    	Transport transport = null;
    	try {
    		log.info("Email Sending...");
    		
    		msg.setFrom(new InternetAddress(FROM,FROMNAME));
    		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
    		//msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
    		transport = session.getTransport();

        	String mailSubject;
        	String fileName;
        	String mailContext;

        	if(1 == type) mailSubject = mm.getMessage("VDR_MAIL_SUBJECT_000" + type, new Locale(lang));
        	else mailSubject = mm.getMessage("VDR_MAIL_SUBJECT_000" + type, subject, new Locale(lang));
        	msg.setSubject(mailSubject, "EUC-KR");

        	//type 
        	//1.가입초대 
        	//2.비밀번호 재설정 인증번호
        	//3.룸에 신규 문서 등록
        	//4.본인작성 문서에 코멘트 추가
        	//5.본인작성 문서의 버전업
        	//6.회원가입 인증 메일
        	//7.FAQ 관리자 메일 발송
        	//99.계약번호
        	switch (type) {
        	case 1:
        	case 2:
        	case 6:
        	case 7:
        	case 99:
        		fileName = "config/templates/mail_layout.html";
        		break;
        	case 3:
        		fileName = "config/templates/mail_Template_NoBtn.html";;
        		break;
        	case 4:
        		fileName = "config/templates/mail_Template_NoBtn.html";
        		break;
        	case 5:
        		fileName = "config/templates/mail_Template_NoBtn.html";
        		break;
        	default:
        		fileName = "config/templates/mail_Template_NoBtn.html";
        		break;
        	}

        	mailContext = readFile(fileName);

        	mailContext = mailContext.replace("SERVER_ADDR", pm.getString("component.url.server"));

        	mailContext = mailContext.replace("COMMON_SUB_FOOTER", (type != 7)? mm.getMessage("COMMON_SUB_FOOTER", new Locale(lang)) :"");
        	mailContext = mailContext.replace("COMMON_SUB_HEADER", (type != 7)? mm.getMessage("COMMON_SUB_HEADER", new Locale(lang)) :"");
        	mailContext = mailContext.replace("COMMON_FOOTER", mm.getMessage("COMMON_FOOTER", new Locale(lang)));

        	if(1 == type || 2 == type || 6 == type || 7 == type || 99 == type) {
        		mailContext = mailContext.replace("MAIL_HEADER", mm.getMessage("MAIL_HEADER000" + type, new Locale(lang)));
        		mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_CONTENT000" + type, subject, new Locale(lang)));
        		mailContext = mailContext.replace("MAIL_BTN", mm.getMessage("MAIL_BTN000" + type, content, new Locale(lang)));
        	}
        	
        	// 메일 발송
        	msg.setContent(mailContext,"text/html; charset=EUC-KR");    		
    		transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
    		// Send the email.
    		transport.sendMessage(msg, msg.getAllRecipients());
    		
    		log.info("Email is sented");    		
    	} catch (Exception e) {
    		System.out.println("The email was not sent.");
    		System.out.println("Error message: " + e.toString());
    	} finally
    	{
    		// Close and terminate the connection.
    		try {
    			transport.close();
    		} catch (MessagingException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    } 
    
    public String makeRamdomNumber(int range) {
		long seedL = System.currentTimeMillis();
		Random random = new Random(seedL);
		return String.valueOf(Math.abs(random.nextInt(range)));
	}
  
}
