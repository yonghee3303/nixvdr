package mg.base;

import java.security.InvalidKeyException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mg.base.base64.Base64;
import mg.base.padding.BlockPadding;


public class AriaEncryptUtil {
	
	private static final Log LOG = LogFactory.getLog("SeedEncryptUtil.class"); 

	/**
	 * ARIA encryption algorithm block size
	 */
	private static final int ARIA_BLOCK_SIZE = 16;
	
	/**
	 * ARIA algorithm to encrypt the data.
	 * @param data Target Data
	 * @param key Masterkey
	 * @param keySize Masterkey Size
	 * @param charset Data character set
	 * @return Encrypted data
	 * @throws UnsupportedEncodingException If character is not supported
	 * @throws InvalidKeyException If the Masterkey is not valid
	 */
	public static String encrypt(String data, byte[] key, int keySize, String charset)
	throws UnsupportedEncodingException, InvalidKeyException {
		
		byte[] encrypt = null;
		if( charset == null ) {
			encrypt = BlockPadding.getInstance().addPadding(data.getBytes(), ARIA_BLOCK_SIZE);
		} else {
			encrypt = BlockPadding.getInstance().addPadding(data.getBytes(charset), ARIA_BLOCK_SIZE);
		}
		
		ARIAEngine engine = new ARIAEngine(keySize);
		engine.setKey(key);
		engine.setupEncRoundKeys();
		
		int blockCount = encrypt.length / ARIA_BLOCK_SIZE;
		for( int i = 0; i < blockCount; i++ ) {
			
			byte buffer[] = new byte[ARIA_BLOCK_SIZE];
			System.arraycopy(encrypt, (i * ARIA_BLOCK_SIZE), buffer, 0, ARIA_BLOCK_SIZE);
			
			buffer = engine.encrypt(buffer, 0);
			// log.error("-----------------encrypt buffer------------------------------\n" + buffer);
			System.arraycopy(buffer, 0, encrypt, (i * ARIA_BLOCK_SIZE), buffer.length);
		}
		
		return Base64.toString(encrypt);
	}
	
	private static byte[] getByteMarkUp(int length, byte[] data) 
	{
		if(length<data.length){
		  String value = new String(data);
		  int len = 0;
		  for(int i =0;i<value.length();i++){  
			  //1바이트, 2바이트, 3바이트 문자만큼 길이 합산
			  len += value.substring(i, i+1).getBytes().length;
			  if(length<len){
				  for(int j=value.substring(0,i).getBytes().length; j< data.length;j++){
					  data[j] = 32; //공백으로 집어 넣음
				  }
				  break;
			  }
		  } 
	  }
	  return data;
	}

	
	/**
	 * ARIA algorithm to decrypt the data.
	 * @param data Target Data
	 * @param key Masterkey
	 * @param keySize Masterkey Size
	 * @param charset Data character set
	 * @return Decrypted data
	 * @throws UnsupportedEncodingException If character is not supported
	 * @throws InvalidKeyException If the Masterkey is not valid
	 */
	public static String decrypt(String data, byte[] key, int keySize, String charset)
	throws UnsupportedEncodingException, InvalidKeyException {

		byte[] decrypt = Base64.toByte(data);
		
		// byte[] decrypt = data.getBytes();
		// log.error("-----------------decrypt Byte------------------------------\n" + decrypt);
		
		ARIAEngine engine = new ARIAEngine(keySize);
		engine.setKey(key);
		engine.setupDecRoundKeys();
		
		int blockCount = decrypt.length / ARIA_BLOCK_SIZE;
		for( int i = 0; i < blockCount; i++ ) {
			
			byte buffer[] = new byte[ARIA_BLOCK_SIZE];
			System.arraycopy(decrypt, (i * ARIA_BLOCK_SIZE), buffer, 0, ARIA_BLOCK_SIZE);
			
			buffer = engine.decrypt(buffer, 0);
			
			// log.error("-----------------decrypt buffer------------------------------\n" + buffer);
			
			System.arraycopy(buffer, 0, decrypt, (i * ARIA_BLOCK_SIZE), buffer.length);
		}
		
		// log.error("-----------------decrypt Byte------------------------------\n" + decrypt);
		
		if( charset == null ) {
			return new String(BlockPadding.getInstance().removePadding(decrypt, ARIA_BLOCK_SIZE));
		} else {
			return new String(BlockPadding.getInstance().removePadding(decrypt, ARIA_BLOCK_SIZE), charset);
		}
		
		/*
		byte[] retval = BlockPadding.getInstance().removePadding(decrypt, ARIA_BLOCK_SIZE);
		
		
		 //  log. ("공백처리된 문자열|"+(new String(b,0,length))+"|");
		 // System.out.println("바이트수: "+ (new String(b,0,length)).getBytes().length);
		return new String(getByteMarkUp(3, retval), charset);
		*/
	}
}
