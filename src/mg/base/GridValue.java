// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   GridValue.java

package mg.base;


public class GridValue
{

    public GridValue(String value, String hiddenvalue)
    {
        this.value = value;
        this.hiddenvalue = hiddenvalue;
    }

    public GridValue(String value, String hiddenvalue, int idx)
    {
        this.value = value;
        this.hiddenvalue = hiddenvalue;
        imageindex = idx;
    }

    public String value;
    public String hiddenvalue;
    public int imageindex;
}