package mg.base;

import java.io.File;
import java.nio.file.Files;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mg.base.cryptorSHAThread;

public class FileUtils {

	protected Log log = LogFactory.getLog(this.getClass());
	
	public FileUtils() {
        // 객체 생성 방지
    }
	
    /**
     * 컴포넌트 지정된 폴더의 파일들을 DiskCryptor 암호화 한다.
     *   (재귀방법으로 하위폴더 및 파일을 찾아 암호화 한다.)
     * 
     * @param sourcePath 암호화 소스 폴더
     * 			  output 암호화 한 파일을 위치 시키는 폴더
     * 			  authCode 암호화 Key 
     */
	public void dcDecrypt(String sourcePath, String output, String authCode) throws Exception {

        // 압축 대상(sourcePath)이 디렉토리나 파일이 아니면 리턴한다.
        File sourceFile = new File(sourcePath);
        if (!sourceFile.isFile() && !sourceFile.isDirectory()) {
            throw new Exception("can not find the files in the compressed target.");
        }
        dcEntry(sourceFile, sourcePath, output, authCode); // Zip 파일 생성
    }
    
    /**
     * 컴포넌트 지정된 폴더의 파일들을 DiskCryptor 암호화 한다.
     *  (재귀방법으로 하위폴더 및 파일을 찾아 암호화 한다.)
     * 
     * @param sourcePath 암호화 소스 폴더
     * 			  output 암호화 한 파일을 위치 시키는 폴더
     * 			  authCode 암호화 Key 
     */
    public void dcEntry(File sourceFile, String sourcePath, String outPath, String authCode) throws Exception {
        // sourceFile 이 디렉토리인 경우 하위 파일 리스트 가져와 재귀호출      	
        if (sourceFile.isDirectory()) {
            if (sourceFile.getName().equalsIgnoreCase(".metadata")) { 
                return;
            }
           // log.info(" -- isDirectory sourceFile --- \n" +  sourceFile.getPath() + " , sourcePath :" + sourcePath);
            
            if(sourcePath.length() < sourceFile.getPath().length()) {
	              String sFilePath = sourceFile.getPath();
	              String zipEntryName = sFilePath.substring(sourcePath.length() + 1, sFilePath.length());
	              
	              // log.info(" -- isDirectory sourceFile --- \n" +  outPath + " , zipEntryName :" + zipEntryName);
	              makeFolder(outPath + "/" + zipEntryName, false);
            }
            
            File[] fileArray = sourceFile.listFiles(); // sourceFile 의 하위 파일 리스트
            for (int i = 0; i < fileArray.length; i++) {
                dcEntry(fileArray[i], sourcePath, outPath, authCode); // 재귀 호출
            }
        } else { // sourcehFile 이 디렉토리가 아닌 경우

            String sFilePath = sourceFile.getPath();
            String zipEntryName = sFilePath.substring(sourcePath.length() + 1, sFilePath.length());
            
            // log.info(" -- sFilePath --- \n" +  sFilePath + ", outPath : " + outPath + "/" + zipEntryName);
            cryptorSHAThread th = new cryptorSHAThread(sFilePath, outPath + "/" + zipEntryName, authCode);
			    th.start();
				try{
					th.join();
				}catch(Exception e){ 
					log.info("dcEntry > cryptorSHAThread Error -----"+e.toString());
				}
			     long retval =th.getResultLong();
           // long retval = cryptorSHA.MgDecodeFile(sFilePath, outPath + "/" + zipEntryName, authCode);
                              
            if(retval == -1) fileCopy(sFilePath, outPath + "/" + zipEntryName);
        }
    }
    
    /**
     * 컴포넌트 지정된 위치에 폴더를 생성 한다.
     * 
     * @param fileName 파일경로에 포함된 Directory를 생성
     *            isDelete 하위폴더에 파일이 있을 경우 삭제 여부 ( True : 삭제, False : 유지)
     * @Return true or false
     */
    public void makeFolder(String fileName, boolean isDelete) throws Exception 
    {
    	    File desti = new File(fileName);
            if(!desti.exists()) {
        		desti.mkdirs(); 
        	}else {
        		if(isDelete) {
	    			//있다면 현재 디렉토리 파일을 삭제 
	    			File[] destroy = desti.listFiles(); 
	    			for(File des : destroy) {
	    				des.delete(); 
	    			}
        		}
        	}
    }
    
    /**
     * 컴포넌트 지정된 폴더 및 파일들을 삭제 한다.
     *  (재귀방법을 통하여 하위폴더를 찾아서 삭제 한다.
     * 
     * @param path 삭제 대상 폴더 또는 파일
     * @Return true or false
     */
    public void fileCopy(String inFileName, String outFileName) throws Exception 
    {
  		   File srcFile = new File(inFileName);
  		   File desFile = new File(outFileName);
  		   
  		   Files.copy(srcFile.toPath(), desFile.toPath());
    }
    
    /**
     * 컴포넌트 지정된 폴더 및 파일들을 삭제 한다.
     *  (재귀방법을 통하여 하위폴더를 찾아서 삭제 한다.
     * 
     * @param path 삭제 대상 폴더 또는 파일
     * @Return true or false
     */
    public boolean deleteFolder(File path) throws Exception {
        if(!path.exists()) {
            return false;
        }
         
        File[] files = path.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
          	  deleteFolder(file);
            } else {
                file.delete();
            }
        }
        return path.delete();
    }
}
