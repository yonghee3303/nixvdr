package mg.su;

import java.util.List;
import java.util.Map;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class exportListAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		 if(request.isMode("selectExportList")) {   /** 조회    */
	            return getExportList(request);
		 } else {
			 return write(null);
		 }
		
		
	}

	private IResponse getExportList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists = getList("mg.su.selectExportList",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
        
	}

}
