package mg.su;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.SeedEncryptUtil;

public class exportMgmtAction extends BaseAction{

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(request.isMode("selectExportMgmt")) {   			/** 조회    */
            return getExportMgmt(request);
		}else if(request.isMode("selectDetailInfo")){		/** 상세정보 조회*/
			return getDetailInfo(request);
		}else if(request.isMode("selectApplyPolicy")) {  	/** 정책조회*/
            return getApplyPolicy(request);
		}else if(request.isMode("updateUsbStatus")) {   	/** USB 상태 변경 */
            return setUsbStatus(request);
		}else if(request.isMode("updatePolicy")) {   		/** 정책 변경 */
            return saveUsbPolicy(request);
		}else if(request.isMode("updatePolicySite")) {   	/** 정책 변경 (결재연동) -- 만료일자, 실행횟수 수정*/
            return updateExpireDtAndReadCnt(request);
		}else if(request.isMode("selectLostList")) {   		/** 분실항목 */
            return getLostList(request);
		}else if(request.isMode("selectReturnDtList")){		/** 반납일이 지난 항목 */
			return getReturnDtList(request); 				 	
		}else if(request.isMode("selectExpireDtList")){		/** 만료일이 지난 항목 */
			return getExpireDtList(request); 				 	
		}else if(request.isMode("selectUseBanList")){		/** 사용 금지 항목 */
			return getUseBanList(request); 				 	
		}else if(request.isMode("selectMalList")){			/** 폐기항목 조회*/
			return getMalList(request);
		}else if(request.isMode("selectOnlyExportList")){
			return getExportList(request); 
		}else if(request.isMode("saveLoginMessage")){	/* 메세지 입력하면 저장 */	
			return saveLoginMessage(request);
		} else if(request.isMode("getLoginMessage")){	/* 메세지 호출 */	
			return getLoginMessage(request);
		} else if(request.isMode("updateUsbExportPolicy")) {   /** 반출USB 정책 변경 */
	        return saveUsbExportPolicy(request);
		} else if(request.isMode("usbPassReset")){	//보안USB 비밀번호 초기화
			return usbPassReset(request);
		}
		 else {
			 return write(null);
		}
	}
	///mg/exportMgmt.do?mod=updateExpireDt&USB_ID=[USB ID]&USB_CREATE_ID=[USB 반출 ID]&USER_ID=[변경 요청자]&EXPIRE_YMD=[만료일자]&READCOUNT=[USB실행횟수]
	private IResponse updateExpireDtAndReadCnt(IRequest request) {
		Map map = request.getMap();
		JSONObject obj = new JSONObject();
		
		try{
			tx.begin();
			String dbName = pm.getString("component.sql.database");
			map.put("dbName", dbName);	
			map.put("POL_CHG_YN", "Y");
			
			//결재승인 시 정책 셋트 변경
			if("".equals(this.getStringWithNullCheck(map,"SU_POLICY_GRP_CD"))){
				map.remove("SU_POLICY_GRP_CD");
			}
			if("".equals(this.getStringWithNullCheck(map,"IS_EXPIRE_DT"))){
				map.remove("IS_EXPIRE_DT");
			}
			if("".equals(this.getStringWithNullCheck(map,"IS_READCOUNT"))){
				map.remove("IS_READCOUNT");
			}
			updateObject("mg.su.updateExpireDtAndReadCnt", map);
			
			map.put("WORK_TYPE", "002");
        	map.put("IP_ADDR", ((HttpServletRequest)request.getAdapter(HttpServletRequest.class)).getRemoteAddr());
        	if(request.getUser() == null) map.put("USER_ID", pm.getString("component.exprot.approverNull"));
        	else map.put("USER_ID", request.getUser().getId());
        	
        	createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), map); 
			
			obj.put("errcode", "0");
        	obj.put("errmsg", "Success Update Exipre Date");
			tx.commit();
		    return write(obj.toString());
		}catch(Exception e){
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
        	obj.put("errcode", "-1");
            obj.put("errmsg", "Fail Update Expire Date");
            
		    return write(obj.toString());
		}
	}

	//메세지 저장
	private IResponse saveLoginMessage(IRequest request) {
		Map map = request.getMap();
		JSONObject obj = new JSONObject();
		
		try{
			tx.begin();
			map.put("dbName", pm.getString("component.sql.database"));	
			updateObject("mg.su.updateLoginMessage", map);
			obj.put("errcode", "0");
        	obj.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
			tx.commit();
		    return write(obj.toString());
		}catch(Exception e){
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
        	obj.put("errcode", "1");
            obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
            
		    return write(obj.toString());
		}
	}
	
	//메세지 호출
	private IResponse getLoginMessage(IRequest request) {
		
		Map map = request.getMap();
		JSONObject obj = new JSONObject();
		
		try{
			log.info("getMessage Param :: " + map.toString());
			
	  		//item 선언하고, item 에 로그인 메세지를 담는다 
			map.put("dbName", pm.getString("component.sql.database"));
			
			String msg = this.getStringNullCheck((String)this.getItem("mg.su.getLoginMessage", map));
			obj.put("LOGIN_MSG", msg);
			obj.put("errcode", "0");
        	obj.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
	        
			return write(obj.toString());
		}catch(Exception e){
			log.error("---------------------------------------------------------------\n" + e.toString());
        	obj.put("errcode", "1");
            obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
            
		    return write(obj.toString());
		}
	
	}
	
	
	private IResponse getExportList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
       
     	Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	       		 lists = getList("mg.su.selectOnlyExportList",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectOnlyExportList_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	private IResponse getMalList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
       
     	Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	       		 lists = getList("mg.su.selectMalList",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectMalList_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	private IResponse getUseBanList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectUseBanList",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectUseBanList_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }	
	}

	private IResponse getExpireDtList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectExpireDtList",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectExpireDtList_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }	
	}
	private IResponse getReturnDtList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectReturnDtList",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectReturnDtList_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }	
	}

	private IResponse getLostList(IRequest request) {
		// TODO Auto-generated method stub
		
		//분실항목만 가져옴
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectLostList",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectLostList_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }			
	}
	
	

	private IResponse saveUsbPolicy(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        Map myMap = request.getMap();
        GridData gdRes = new GridData();
		try{
			tx.begin();
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
			updateObject("mg.su.updateUsbPolicy", smap);
			updateObject("mg.su.updateRentMgt", smap);
			
			myMap.remove("USB_ID");
			myMap.remove("USB_CREATE_ID");
			myMap.remove("USB_NM");
			
			
			myMap.put("dbName", pm.getString("component.sql.database"));
			myMap.put("RENDER_ID", request.getStringParam("RENDER_ID"));
			log.info(myMap.toString());
			List lists;
	       	String dbName = pm.getString("component.sql.database");
	       	lists = getList("mg.su.selectExportMgmt"+getDBName(dbName),myMap);
        	smap.put("WORK_TYPE", "002");
        	smap.put("IP_ADDR", ((HttpServletRequest)request.getAdapter(HttpServletRequest.class)).getRemoteAddr());
        	createObject("mg.su.insertWebLog"+getDBName(dbName), smap);//정책변경에 관한 SU_LOG에 로그 기록
	       	 
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        tx.commit();
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
		}catch(Exception e) {
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
			return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
		}
	}

	private IResponse setUsbStatus(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        Map myMap = request.getMap();
        GridData gdRes = new GridData();
		try{
			tx.begin();
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			if("05".equals(smap.get("RENT_STS_CD"))){
				
				smap.put("MOD_STOP", pm.getString("component.usb.lost.modeStop"));
				updateObject("mg.su.updateModstop",smap);
				//---------------------------------------------------------------------------------------라이센스 관련
				map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
				String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

				String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
				int limitUsb = Integer.parseInt(plainLicense);
				log.info("-------------------------------------------해제 전 남은 USB 갯수"+limitUsb);
				limitUsb = limitUsb +1;
				
				String modifyCount=String.valueOf(limitUsb);
				String paddingCount="";
				//다시 암호화 하기 위해서 패딩을 주어야 함..
				for(int i= 0; i<(16-modifyCount.length());i++){
				paddingCount = paddingCount.concat("0");
				}
				paddingCount = paddingCount.concat(modifyCount);
				String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");

				map.clear();
				map.put("OTHERDATA1", cipherLicense);
				updateObject("mg.su.updateLicense", map);
				map.clear();
			//-----------------------------------------------------------------------------------------------------------
			}else if("03".equals(smap.get("RENT_STS_CD"))){
				//라이센스 USB갯수 정보 확인---------------------------------------------------------------------------------
				map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
				String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

				String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
				int limitUsb = Integer.parseInt(plainLicense);
				log.info("-------------------------------------------등록 전 남은 USB 갯수"+limitUsb);
				//라이센스가 부족하면 로그로 알려줌.
				if(limitUsb>0){
					limitUsb = limitUsb -1;
					String modifyCount=String.valueOf(limitUsb);
					String paddingCount="";
					//다시 암호화 하기 위해서 패딩을 주어야 함..
					for(int i= 0; i<(16-modifyCount.length());i++){
						paddingCount = paddingCount.concat("0");
					}
					paddingCount = paddingCount.concat(modifyCount);
					String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");
					
					smap.put("MOD_STOP", "N");
					updateObject("mg.su.updateModstop",smap);
					map.clear();
					map.put("OTHERDATA1", cipherLicense);
					updateObject("mg.su.updateLicense", map);
					map.clear();
				//---------------------------------------------------------------------------------------
				}else{
					log.error("----------------------------------------라이센스가 부족하여 더이상 등록하실 수 없습니다. ");
					return write("라이센스가 부족하여 더이상 등록하실 수 없습니다.");
				}
			}
			updateObject("mg.su.updateUsbStatus_MASTER", smap);
			updateObject("mg.su.updateUsbStatus_RENT_MGT", smap);
			
	        tx.commit(); 
			
	        myMap.put("dbName", pm.getString("component.sql.database"));
			myMap.remove("USB_ID");
			myMap.remove("USB_CREATE_ID");
		
			 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectExportMgmt",myMap);
	       	 } else {
	       		 lists = getList("mg.su.selectExportMgmt_" + dbName, myMap);
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
		}catch(Exception e) {
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
			return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
		}
	}

	//하단 폼에 설정된 정책들을 표시하기 위한...
	private IResponse getApplyPolicy(IRequest request) {
		// TODO Auto-generated method stub
		try {
   		 
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
	    	Map map = (Map)getItem("mg.su.selectApplyPolicy",smap);
	    	
	    		// Creation of Json Response 
	    		 JSONObject obj = new JSONObject();
	    		obj.put("USB_ID", this.getStringWithNullCheck(map, "USB_ID"));
	    		obj.put("USB_CREATE_ID", this.getStringWithNullCheck(map, "USB_CREATE_ID"));
	    		obj.put("MOD_STOP", this.getStringWithNullCheck(map, "MOD_STOP"));
	    		obj.put("IS_MODIFIED_USER_AUTH", this.getStringWithNullCheck(map, "IS_MODIFIED_USER_AUTH"));
	    		
	    		obj.put("NO_SAVE", this.getStringWithNullCheck(map, "NO_SAVE"));
	    		obj.put("NO_COPY", this.getStringWithNullCheck(map, "NO_COPY"));
	    		obj.put("NO_PRINT", this.getStringWithNullCheck(map, "NO_PRINT"));
	    		
	    		obj.put("USB_NM", this.getStringWithNullCheck(map, "USB_NM"));
	    		obj.put("IS_EXPIRE_DT", this.getStringWithNullCheck(map, "IS_EXPIRE_DT"));
	    		obj.put("EXPIRE_YMD", this.getStringWithNullCheck(map, "EXPIRE_YMD"));
	    		//obj.put("IS_EXPIRE_INIT", this.getStringWithNullCheck(map, "IS_EXPIRE_INIT"));
	    		//obj.put("RETURN_YMD", this.getStringWithNullCheck(map, "RETURN_YMD"));
	    		
	    		obj.put("IS_READCOUNT", this.getStringWithNullCheck(map, "IS_READCOUNT"));
	    		obj.put("READCOUNT", this.getStringWithNullCheck(map, "READCOUNT"));
	    		obj.put("IS_READ_INIT", this.getStringWithNullCheck(map, "IS_READ_INIT"));
	    		
	    		obj.put("WAIT_TIME", this.getStringWithNullCheck(map, "WAIT_TIME"));
	    		obj.put("IS_WAIT_CLOSE", this.getStringWithNullCheck(map, "IS_WAIT_CLOSE"));
	    		
	    		obj.put("IS_LOGIN_FAIL_STOP", this.getStringWithNullCheck(map, "IS_LOGIN_FAIL_STOP"));
	    		obj.put("IS_LOGIN_FAIL_INIT", this.getStringWithNullCheck(map, "IS_LOGIN_FAIL_INIT"));
	    		obj.put("IS_LOGIN_FAIL_COUNT", this.getStringWithNullCheck(map, "IS_LOGIN_FAIL_COUNT"));
	    		obj.put("RENT_STS_NM", this.getStringWithNullCheck(map, "RENT_STS_NM"));
	    		obj.put("MAC_ADDR", this.getStringWithNullCheck(map, "MAC_ADDR"));
	    		obj.put("SU_POLICY_GRP_CD", this.getStringWithNullCheck(map, "SU_POLICY_GRP_CD"));
	    		obj.put("IS_APPROVAL", this.getStringWithNullCheck(map, "IS_APPROVAL"));
	    		
	    		obj.put("IS_EXPIRE_STOP", this.getStringWithNullCheck(map, "IS_EXPIRE_STOP"));
	    		obj.put("IS_EXPIRE_INIT", this.getStringWithNullCheck(map, "IS_EXPIRE_INIT"));
	    		obj.put("IS_READ_STOP", this.getStringWithNullCheck(map, "IS_READ_STOP"));
	    		obj.put("RENT_STS_CD",  this.getStringWithNullCheck(map, "RENT_STS_CD"));
	    		obj.put("USECOUNT", this.getStringWithNullCheck(map, "USECOUNT"));
	    		
	    		return write(obj.toString());
	         } catch(Exception e) {
	       	 	log.error("---SU 적용 정책 불러오기 실패-------------------------\n" + e.toString());
	       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
	         }		
	}
	


	private IResponse getExportMgmt(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectExportMgmt",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectExportMgmt_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
        
	}
	private IResponse getDetailInfo(IRequest request) {
		// TODO Auto-generated method stub	
		try {
		    		 
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
		    	Map map = (Map)getItem("mg.su.selectDetailInfo",smap);
		    		// Creation of Json Response 
		    		JSONObject obj = new JSONObject();
		    		obj.put("USB_MODEL", this.getStringWithNullCheck(map, "USB_MODEL"));
		    		obj.put("USB_CAPACITY", this.getStringWithNullCheck(map, "USB_CAPACITY"));
		    		
		    		obj.put("REG_DT", this.getStringWithNullCheck(map, "REG_DT"));
		    		obj.put("REG_USER_NM", this.getStringWithNullCheck(map, "REG_USER_NM"));
		    		obj.put("REG_DEPT_NM", this.getStringWithNullCheck(map, "REG_DEPT_NM"));
		   
		    		obj.put("REG_POSITION_NM", this.getStringWithNullCheck(map, "REG_POSITION_NM"));
		    		obj.put("RENT_YMD", this.getStringWithNullCheck(map, "RENT_YMD"));
		    		obj.put("RENT_USER_NM", this.getStringWithNullCheck(map, "RENT_USER_NM"));
		    		obj.put("RENT_DEPT_NM", this.getStringWithNullCheck(map, "RENT_DEPT_NM"));
		    		obj.put("RENT_POSITION_NM", this.getStringWithNullCheck(map, "RENT_POSITION_NM"));
		    		obj.put("RENT_STS_NM", this.getStringWithNullCheck(map, "RENT_STS_NM"));

		    		
		    		return write(obj.toString());
		         } catch(Exception e) {
		       	 	log.error("---SU 상세 정보 불러오기 실패-------------------------\n" + e.toString());
		       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
		         }		
		
	}
	
	//반출 USB 정책변경
	private IResponse saveUsbExportPolicy(IRequest request) {
		
		Map map = request.getMap();
		JSONObject obj = new JSONObject();
		JSONObject result = new JSONObject();
		
		try{
			log.info("반출 USB 정책 변경 시작");
			tx.begin();
			JSONParser jp = new JSONParser();
			Map smap = new HashMap(); 
			obj = (JSONObject)jp.parse((String) map.get("POLICY"));
			
			Iterator it = obj.keySet().iterator();
			
			while(it.hasNext()) {
				String key = (String)it.next();
				smap.put(key, obj.get(key));
			}
			
			String dbName = pm.getString("component.sql.database");
			smap.put("dbName", dbName);
			smap.put("POL_CHG_YN", "Y");
			updateObject("mg.su.updateUsbExportPolicy", smap);
			updateObject("mg.su.updateUsbRent", smap);
			log.info("반출 USB 정책 변경 완료");
			smap.put("WORK_TYPE", "002");
			smap.put("USER_ID", request.getUser().getId());
			
			log.info("정책변경 로그 기록");
			if("mssql".equals(dbName)) {
				createObject("mg.su.insertWebLog", smap);//정책변경에 관한 SU_LOG에 로그 기록
			} else {
				createObject("mg.su.insertWebLog_"+dbName, smap);//정책변경에 관한 SU_LOG에 로그 기록
			}

	        tx.commit();

	        result.put("errcode", "0");
	        result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
	        
		}catch(Exception e) {
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
        	result.put("errcode", "-1");
            result.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		}
		 
	    return write(result.toString());
	}
	
	//보안USB 비밀번호 초기화
	private IResponse usbPassReset(IRequest request) {
		
		Map map = request.getMap();
		JSONObject result = new JSONObject();
		
		try{
			log.info("반출 USB 비밀번호 초기화");
			tx.begin();
			
			map.put("STATUS", "Y");
			updateObject("mg.su.usbPassReset", map);
			
			
			String dbName = pm.getString("component.sql.database");
			map.put("dbName", dbName);
			
			
			log.info("정책변경 로그 기록");
			map.put("WORK_TYPE", "018");
			map.put("USER_ID", request.getUser().getId());
			
			if("mssql".equals(dbName)) {
				createObject("mg.su.insertWebLog", map);//비밀번호 초기화에 관한 SU_LOG에 로그 기록
			} else {
				createObject("mg.su.insertWebLog_"+dbName, map);//비밀번호 초기화에 관한 SU_LOG에 로그 기록
			}
	        tx.commit();

	        result.put("errcode", "0");
	        result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
	        
	        return write(result.toString());   
	        
		}catch(Exception e) {
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
        	result.put("errcode", "1");
            result.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
            
		    return write(result.toString());
		}
	}
}
