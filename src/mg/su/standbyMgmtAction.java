package mg.su;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class standbyMgmtAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		
		if(request.isMode("getDetailStandby")){
		 	return getDetailStandby(request);
		}else {
			 return write(null);
		}
	}
	
	private IResponse getDetailStandby(IRequest request) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		Map smap = request.getMap();
    	smap.put("dbName", pm.getString("component.sql.database"));
    	try{
		Map map = (Map)getItem("mg.su.selectStandbyDetail",smap);
		
		obj.put("RENDER_NM",this.getStringWithNullCheck(map,"RENDER_NM"));
		obj.put("RENDER_ID",this.getStringWithNullCheck(map,"RENDER_ID"));
		obj.put("DEPT_NM",this.getStringWithNullCheck(map,"DEPT_NM"));
		obj.put("IS_EXPIRE_DT",this.getStringWithNullCheck(map,"IS_EXPIRE_DT"));
		obj.put("EXPIRE_YMD",this.getStringWithNullCheck(map,"EXPIRE_YMD"));
		obj.put("USB_TYPE_CD",this.getStringWithNullCheck(map,"USB_TYPE_CD"));
		obj.put("RENT_DESC",this.getStringWithNullCheck(map,"RENT_DESC"));
		obj.put("RUN_MAC_ADDR",this.getStringWithNullCheck(map,"RUN_MAC_ADDR"));
		obj.put("IS_READCOUNT",this.getStringWithNullCheck(map,"IS_READCOUNT"));
		obj.put("READCOUNT",this.getStringWithNullCheck(map,"READCOUNT"));
		
		obj.put("ERROR_CD", "0");
		obj.put("ERROR_MSG", "");
    	
    	}catch(Exception e){
    		log.info("------------------------------------------------------------------"+e.toString());
    		obj.put("ERROR_CD", "-1");
    		obj.put("ERROR_MSG", "해당 대기 정보를 불러 오는 중 오류가 발생하였습니다.");
    	}
		return write(obj.toString());
	}
}
