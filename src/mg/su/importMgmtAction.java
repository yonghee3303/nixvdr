package mg.su;

import java.util.List;
import java.util.Map;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.SeedEncryptUtil;

public class importMgmtAction extends BaseAction{

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(request.isMode("selectImportMgmt")) {   	/** 조회    */
            return getImportMgmt(request);
		 }else if(request.isMode("selectAllMgmt")){		/** 모든 USB 조회*/
			 return getAllMgmt(request);
		 }else if(request.isMode("selectLostList")){ 	/** 분실 항목만 조회 */
			 return getLostList(request);
		 }else if(request.isMode("updateUsbStatus")){	/** 분실 및 분실 해지*/
			 return setUsbStatus(request);
		 }else {
			 return write(null);
		 }
	}

	private IResponse setUsbStatus(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
     	Map map = null;
        GridData gdRes = new GridData();
		try{
			tx.begin();
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			if("lost".equals(smap.get("STATE")) || "malfunc".equals(smap.get("STATE"))){ //잃어버렸을 때

				//---------------------------------------------------------------------------------------라이센스 관련
				map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
				String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

				String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
				int limitUsb = Integer.parseInt(plainLicense);
				log.info("-------------------------------------------해제 전 남은 USB 갯수"+limitUsb);
				limitUsb = limitUsb +1;
				
				String modifyCount=String.valueOf(limitUsb);
				String paddingCount="";
				//다시 암호화 하기 위해서 패딩을 주어야 함..
				for(int i= 0; i<(16-modifyCount.length());i++){
				paddingCount = paddingCount.concat("0");
				}
				paddingCount = paddingCount.concat(modifyCount);
				String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");

				map.clear();
				map.put("OTHERDATA1", cipherLicense);
				updateObject("mg.su.updateLicense", map);
				map.clear();	
			}else if("find".equals(smap.get("STATE"))){ //찾았을 때
				//라이센스 USB갯수 정보 확인---------------------------------------------------------------------------------
				map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
				String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

				String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
				int limitUsb = Integer.parseInt(plainLicense);
				log.info("-------------------------------------------등록 전 남은 USB 갯수"+limitUsb);
				//라이센스가 부족하면 로그로 알려줌.
				if(limitUsb>0){
					limitUsb = limitUsb -1;
					String modifyCount=String.valueOf(limitUsb);
					String paddingCount="";
					//다시 암호화 하기 위해서 패딩을 주어야 함..
					for(int i= 0; i<(16-modifyCount.length());i++){
						paddingCount = paddingCount.concat("0");
					}
					paddingCount = paddingCount.concat(modifyCount);
					String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");

					map.clear();
					map.put("OTHERDATA1", cipherLicense);
					updateObject("mg.su.updateLicense", map);
					map.clear();
				}else{
					tx.rollback();
					log.error("----------------------------------------라이센스가 부족하여 더이상 등록하실 수 없습니다. ");
					return write("라이센스가 부족하여 더이상 등록하실 수 없습니다.");
				}
			}
			
			//잃어버렸을 때
			//rent_sts_cd를 "05"로 바꿔 준다.
			if("lost".equals(smap.get("STATE"))){ //잃어버렸을 때
				smap.put("RENT_STS_CD", "05");	
			}else if("malfunc".equals(smap.get("STATE"))){
				smap.put("RENT_STS_CD", "04");	
			}else if("find".equals(smap.get("STATE"))){
				//log.info(getItem("mg.su.getCountRent",smap).toString());
				//찾았을 때
				//rent_sts_cd를 전에 반출된 적이 있다면 "02", 반출된 적이 없다면 "01"
				//SELECT COUNT(*) FROM SU_RENT_MGT WHERE USB_ID=#USB_ID# 로 했을 때 count가 0이라면 "01" 0이상이라면 "02"
				int rentCount = Integer.parseInt(getItem("mg.su.getCountRent",smap).toString());
				if(rentCount>0){
					smap.put("RENT_STS_CD", "02");
				}else{
					smap.put("RENT_STS_CD", "01");
				}
			}
			updateObject("mg.su.updateUsbStatus_MASTER", smap);
			smap.clear();
	        List lists;
		       	 String dbName = pm.getString("component.sql.database");
		       	 if("mssql".equals(dbName)) {
		       		 lists = getList("mg.su.selectImportMgmt",smap);
		       	 } else {
		       		 lists = getList("mg.su.selectImportMgmt_" + dbName,smap);
		       	 }
		        for (int i = 0; i < lists.size(); i++) {
		         	map =(Map) lists.get(i);
		         	
		         	for(int j=0; j < cols.length; j++ ) {
		         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
		         	}
		        }   

	        tx.commit(); 
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
		}catch(Exception e) {
			tx.rollback();
			log.error("---------------------------------------------------------------\n" + e.toString());
			return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
		}
	}

	private IResponse getLostList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
       
     	Map map = null;
        GridData gdRes = new GridData();
        Map smap = request.getMap();
        try{
        	List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	       		 lists = getList("mg.su.selectImportLostList",smap);
	       	 } else {
	       		 lists = getList("mg.su.selectImportLostList_" + dbName, smap);
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	private IResponse getAllMgmt(IRequest request) {
		// TODO Auto-generated method stub

		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
       
     	Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	       		 lists = getList("mg.su.selectAllMgmt",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectAllMgmt_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	private IResponse getImportMgmt(IRequest request) {
		// TODO Auto-generated method stub
	
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
       
     	Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	       		 lists = getList("mg.su.selectImportMgmt",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectImportMgmt_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
        
	}
}
