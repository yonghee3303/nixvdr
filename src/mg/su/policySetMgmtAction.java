package mg.su;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.site.LgChemInterlock;
import mg.site.SangsinInterlock;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.component.mail.IMail;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
 * 업무 그룹명 : mg.su 서브 업무명 : policySetMgmtAction.java 작성자 : whchoi 작성일 : 2016. 03. 26 설 명 : SU 정책설정
 */
public class policySetMgmtAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {

		if (request.isMode("selectPolicyGrpList")) { // 정책그룹 조회
			return selectPolicyGrpList(request);
		} else if (request.isMode("savePolicyGrp")) { // 정책그룹 저장
			return savePolicyGrp(request);
		} else if (request.isMode("deletePolicyGrp")) { // 정책그룹 삭제
			return deletePolicyGrp(request);
		} else if (request.isMode("getComboPolicyGrpHtml")) { // 콤보코드(정책그룹) 조회
			return getComboPolicyGrpHtml(request);
		} else if(request.isMode("savePolicySet")){
			return savePolicySet(request);
		} else if(request.isMode("deletePolicySet")){
			return deletePolicySet(request);
		} else if(request.isMode("selectPolicySetList")){
			return selectPolicySetList(request);
		}else {
			return write(null);
		}
	}

	private IResponse selectPolicySetList(IRequest request) {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");

		Map map = null;
		GridData gdRes = new GridData();

		try {
			Map smap = request.getMap();
			String dbName = pm.getString("component.sql.database");
			smap.put("dbName", dbName);
			List lists = getList("mg.su.selectPolicyList", smap);
			
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);
				map.put("LOCALE", request.getUser().getLocale().getLanguage());
				map.put("dbName", dbName);
				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j],	this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false", "doQuery", gdRes.getGridXmlDatas()));
		}
	}

	private IResponse deletePolicySet(IRequest request) {
		String ids_info = request.getStringParam("ids");
	    String cols_ids = request.getStringParam("col_ids");
	    String[] cols = cols_ids.split(",");
	    String[] ids = ids_info.split(",");
	    
	    try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.su.deletePolicySet", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
	    } catch(Exception e) {
	       log.error("---------------------------------------------------------------\n" + e.toString());
	   	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
	    }
	}

	private IResponse savePolicySet(IRequest request) {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		
		try {
			tx.begin();
			Map item = null;
		    for (int i = 0; i < ids.length; i++) {
		    	item = new HashMap();
		    	
		    	for(int j=0; j < cols.length; j++ ) {
		    		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		    		item.put(cols[j], tmps);
		    	}
		    	item.put("LOGIN_ID",  request.getUser().getId());
		    	
		    	if( Integer.parseInt(getItem("mg.su.countPolicySet", item).toString() ) > 0 ) {
	        		updateObject("mg.su.updatePolicySet", item);
	        	} else {
	        		item.put("SET_ID",  item.get("SET_CL_CD") + (String)item.get("PC_CL_CD"));
	        		createObject("mg.su.insertPolicySet", item);	
	        	 }
		    }
		    tx.commit();
		    return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
		} catch(Exception e) {
			tx.rollback();
		    log.error("---------------------------------------------------------------\n" + e.toString());
		    return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
		}
	}

	
	
	
	private IResponse selectPolicyGrpList(IRequest request) {		
		
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");

		Map map = null;
		GridData gdRes = new GridData();

		try {
			Map smap = request.getMap();
			smap.put("LOCALE", request.getUser().getLocale().getLanguage());
			smap.put("USER_ROLL", request.getUser().getProperty("USER_ROLL").toString());	
			//smap.put("USER_ROLL", "S02");	//테스트용
			//smap.put("BIZ_CD", "");			//테스트용
			smap.put("LOGIN_ID", request.getUser().getId());

			String dbName = pm.getString("component.sql.database");
			smap.put("dbName", dbName);
			List lists = getList("mg.su.selectPolicyGrpList", smap);
			
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j],	this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false", "doQuery", gdRes.getGridXmlDatas()));
		}
	}
	
	private IResponse savePolicyGrp(IRequest request) {

		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		
		try {
			tx.begin();
			Map item = null;
		    for (int i = 0; i < ids.length; i++) {
		    	item = new HashMap();
		    	
		    	for(int j=0; j < cols.length; j++ ) {
		    		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		    		item.put(cols[j], tmps);
		    	}
		  	         	 
		   	 	//세트명이 비어있을 경우
		   	 	if(this.getStringWithNullCheck(item, "SU_POLICY_GRP_CD").length() == 0) {
		   	 		item.put("LOGIN_ID",  request.getUser().getId());
		   	 		Map bizCd = (Map)getItem("mg.su.selectBizCd", item);
		   	 		item.put("BIZ_CD", bizCd.get("BIZ_CD"));
		   	 		createObject("mg.su.insertPolicyGrp"+getDBName(pm.getString("component.sql.database")), item);
		   	 	} else {
		   	 		updateObject("mg.su.updatePolicyGrp", item);
		   	 	}
		    }
		    tx.commit();
		    return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
		} catch(Exception e) {
			tx.rollback();
		    log.error("---------------------------------------------------------------\n" + e.toString());
		    return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
		}
	}
	
	private IResponse deletePolicyGrp(IRequest request) {
		String ids_info = request.getStringParam("ids");
	    String cols_ids = request.getStringParam("col_ids");
	    String[] cols = cols_ids.split(",");
	    String[] ids = ids_info.split(",");
	    
	    try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	updateObject("mg.su.deletePolicyGrp", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
	    } catch(Exception e) {
	       log.error("---------------------------------------------------------------\n" + e.toString());
	   	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
	    }
	}
	
	/**
	 * 콤보코드(정책그룹) 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getComboPolicyGrpHtml(IRequest request) throws Exception {
		try {
			Map item = request.getMap();
			List lists = getList("mg.su.getComboPolicyGrpHtml", item);
			StringBuffer buffer = new StringBuffer();

			Map map = null;

			//buffer.append("<option value='none'>"+ mm.getMessage("CPIS_0025", request.getUser().getLocale()) +"</option>");
			buffer.append("<option value=''></option>");
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);
				buffer.append("<option value='" + map.get("POLICY_SET_CD") + "'>"
						+ map.get("POLICY_SET_NM") + "</option>");

				// log.debug(
				// "-------------------------------------------------------------------\n"
				// + map.toString());
			}
			//log.info("CustInfo : " + buffer.toString());
			return write(buffer.toString());
		} catch (Exception ex) {
			log.error("-------------------------------------------------------------------\n"
					+ ex.getMessage());
			return write("");
		}
	}
}
