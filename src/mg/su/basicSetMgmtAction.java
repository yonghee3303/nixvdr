package mg.su;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.site.LgChemInterlock;
import mg.site.SangsinInterlock;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.component.mail.IMail;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
 * 업무 그룹명 : mg.su 서브 업무명 : basicSetMgmtAction.java 작성자 : whchoi 작성일 : 2016. 03.
 * 23 설 명 : SU 일반설정
 */
public class basicSetMgmtAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {

		if (request.isMode("selectServer")) { // 서버설정 조회
			return selectServer(request);
		} else if (request.isMode("checkServer")) { // 서버 설정값 존재 유무 확인
			return checkServer(request);
		} else if (request.isMode("saveServer")) { // 서버 설정 저장
			return saveServer(request);
		} else if (request.isMode("selectPolicySetList")) { // 정책 설정세트 조회
			return selectPolicySetList(request);
		} else if (request.isMode("saveAuth")) { // 정책코드 설정 저장
			return saveAuth(request);
		} else if (request.isMode("selectBizAdminList")) { // 사업정 및 관리자 조회
			return selectBizAdminList(request);
		} else if (request.isMode("saveBizAdmin")) { // 사업정 및 관리자 저장
			return saveBizAdmin(request);
		}
		
		else {
			return write(null);
		}
	}

	private IResponse selectServer(IRequest request) {
		
		log.info("--selectServer------------------------" + request.getMap().toString());
		JSONObject result = new JSONObject();
		JSONArray jIpList = new JSONArray();
		JSONObject jIp = null;
		
		try {
			Map item = (Map)getItem("mg.su.selectServerInfo", null);
			Iterator it = item.keySet().iterator();
			
			while(it.hasNext()) {
				String key = (String)it.next();
				if(key.equals("IP_RANGE_ADDR")) {
					if(item.get("IP_RANGE_ADDR") != null && !"".equals(item.get("IP_RANGE_ADDR"))) {
						String ipAddrs = this.getStringWithNullCheck(item, "IP_RANGE_ADDR");
						String[] ipList = ipAddrs.split("\\|");
						for(String ip:ipList) {
							jIp = new JSONObject();
							String[] ipRange = ip.split("\\~");
							jIp.put("IP_ADDR_OP", ipRange[0]);
							jIp.put("IP_ADDR_ED", ipRange[1]);
							jIpList.add(jIp);
						}
					}
					result.put("IP_RANGE_ADDR", jIpList);
				} else {
					result.put(key, item.get(key));
				}
			}
			result.put("errcode", "0");
			result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
		} catch (Exception e) {
			log.error("------------selectServer---------------------------------------------------\n" + e.toString());
			result.put("errcode", "-1");
			result.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
		}
		return write(result.toString());
	}

	private IResponse checkServer(IRequest request) {
		log.info("Secure USB Server Check");
		JSONObject result = new JSONObject();

		result.put("errcode", "0");
		result.put("errmsg", "Success.");

		return write(result.toString());
	}

	private IResponse saveServer(IRequest request) {

		log.info("--saveServer------------------------"	+ request.getMap().toString());
		JSONObject result = new JSONObject();

		try {
			tx.begin();
			Map itm = request.getMap();
			itm.put("LOGIN_ID", request.getUser().getId());

			// SU Server 등록
			if (Integer.parseInt(getItem("mg.su.countServer", itm).toString()) > 0) {
				updateObject("mg.su.updateServer", itm);
				log.info("SU서버 업데이트 완료");
			} else {
				createObject("mg.su.insertServer", itm);
				log.info("SU서버 등록 완료");
			}

			result.put("errcode", "0");
			result.put("errmsg",
					mm.getMessage("COMG_1002", request.getUser().getLocale()));
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveServer---------------------------------------------------\n"	+ e.toString());
			result.put("errcode", "-1");
			result.put("errmsg",
					mm.getMessage("COMG_1007", request.getUser().getLocale()));
		}
		return write(result.toString());
	}
	
	private IResponse selectPolicySetList(IRequest request) {
		
		log.info("--selectAuth------------------------" + request.getMap().toString());
		JSONObject result = new JSONObject();
		JSONObject jAuthObj = new JSONObject();
		//JSONArray jAuthList = new JSONArray();
		JSONObject jAuth = null;
		
		try {
			List lists = getList("mg.su.selectPolicySetList", null);
			Map map = null;
			
			for(int i=0; i<lists.size(); i++) {
				jAuth = new JSONObject();
				map = (Map)lists.get(i);
				Iterator it = map.keySet().iterator();
				while(it.hasNext()) {
					String key = (String)it.next();
					jAuth.put(key, map.get(key));
				}
				jAuth.put("STS", "");
				jAuthObj.put(jAuth.get("POLICY_SET_CD"), jAuth);
				//jAuthList.add(jAuth);
				log.info("----------selectPolicySetList------------------\n" + jAuthObj.toString());
			}
			result.put("policySet", jAuthObj);
			result.put("errcode", "0");
			result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
		} catch (Exception e) {
			log.error("------------selectAuth---------------------------------------------------\n" + e.toString());
			result.put("errcode", "-1");
			result.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
		}
		return write(result.toString());
	}
	
	//SU 정책코드에 관련된 모든 사항을 저장한다 (신규, 수정, 삭제)
	private IResponse saveAuth(IRequest request) {

		log.info("--saveAuth------------------------" + request.getMap().toString());
		JSONObject result = new JSONObject();

		try {
			tx.begin();
			Map itm = request.getMap();
			
			JSONParser jsonParser = new JSONParser();
			JSONObject authList = (JSONObject) jsonParser.parse(java.net.URLDecoder.decode((String)itm.get("authList"), "utf-8")); 
		
			// SU Auth 등록
			Iterator setIt = authList.keySet().iterator();
			//정책코드 세트 호출
			while(setIt.hasNext()) {
				Map authSet = new HashMap();
				String setCd = (String)setIt.next();
				JSONObject jAuth = (JSONObject)authList.get(setCd);

				Iterator authIt = jAuth.keySet().iterator();
				//정책코드 세트 호출
				while(authIt.hasNext()) {
					String key = (String)authIt.next();
					authSet.put(key, jAuth.get(key));
				}
				
				if(jAuth.get("STS").equals("SAVE")) {
					if (Integer.parseInt(getItem("mg.su.countAuth", authSet).toString()) > 0) {
						updateObject("mg.su.updateAuth", authSet);
						log.info("SU 정책세트 업데이트 : " + authSet.toString());
					} else {
						createObject("mg.su.insertAuth", authSet);
						log.info("SU 정책세트 신규 등록 : " + authSet.toString());
					}
				} else if(jAuth.get("STS").equals("DEL")) {
					deleteObject("mg.su.deleteAuth", authSet);
					log.info("SU 정책세트 삭제 : " + authSet.toString());
				}		
			}

			result.put("errcode", "0");
			result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveAuth---------------------------------------------------\n"	+ e.toString());
			result.put("errcode", "-1");
			result.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		}
		return write(result.toString());
	}
	
	private IResponse selectBizAdminList(IRequest request) {
		
		log.info("--selectBizAdminList------------------------" + request.getMap().toString());
		JSONObject result = new JSONObject();
		JSONObject jBiz = new JSONObject();
		JSONObject jAdminObj = new JSONObject();
		JSONArray jAdminList = null;
		JSONObject jAdmin = null;
		
		
		try {
			List bizLists = getList("mg.su.selectBizList"+getDBName( pm.getString("component.sql.database")), null);
			Map map = null;
			Map smap = null;
			
			for(int i=0; i<bizLists.size(); i++) {
				jAdminList = new JSONArray();
				
				map = (Map)bizLists.get(i);
				
				String deptCd = (String)map.get("DEPT_CD");
				String deptNm = (String)map.get("DEPT_NM");
				jBiz.put(deptCd, deptNm);
				
				List adminLists = getList("mg.su.selectAdminList"+getDBName( pm.getString("component.sql.database")), map);
				
				for(int j=0; j<adminLists.size(); j++) {
					jAdmin = new JSONObject();
					smap = (Map)adminLists.get(j);
					Iterator it = smap.keySet().iterator();
					while(it.hasNext()) {
						String key = (String)it.next();
						jAdmin.put(key, (String)smap.get(key));
					}
					jAdmin.put("STS", "");
					jAdminList.add(jAdmin);
				}
				jAdminObj.put(deptCd, jAdminList);
			}
			result.put("bizList", jBiz);
			result.put("adminList", jAdminObj);
			result.put("errcode", "0");
			result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
		} catch (Exception e) {
			log.error("------------selectBizAdminList---------------------------------------------------\n" + e.toString());
			result.put("errcode", "-1");
			result.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
		}
		return write(result.toString());
	}
	
	//SU 정책코드에 관련된 모든 사항을 저장한다 (신규, 수정, 삭제)
	private IResponse saveBizAdmin(IRequest request) {

		log.info("--saveBizAdmin------------------------" + request.getMap().toString());
		JSONObject result = new JSONObject();

		try {
			tx.begin();
			Map itm = request.getMap();
			
			String deptStr = java.net.URLDecoder.decode((String)itm.get("deptList"), "utf-8");
			String[] deptList = deptStr.split("\\|");
			
			JSONParser jsonParser = new JSONParser(); 
			JSONObject adminList = (JSONObject) jsonParser.parse(java.net.URLDecoder.decode((String)itm.get("adminList"), "utf-8"));
			JSONArray jDeptAdminList = null;
			JSONObject jAdmin = null;
			
			//부서코드 삭제
			if(deptStr.length() != 0) {
				deptStr = "";
				for(int i=0; i<deptList.length; i++) {
					deptStr += "'"+deptList[i]+"',";
				}
				deptStr = deptStr.substring(0, deptStr.length()-1);
				deleteObject("mg.su.deleteDept", deptStr);
				log.info("사업장 정보 : " + deptStr);
			}
			//신규사업장 등록
			for(int i=0; i<deptList.length; i++) {
				Map bizMap = new HashMap();
				bizMap.put("LOGIN_ID", request.getUser().getId());
				bizMap.put("DEPT_CD", deptList[i]);
				bizMap.put("dbName", pm.getString("component.sql.database"));
				jDeptAdminList = (JSONArray)adminList.get(deptList[i]);
				//사업장내 사용자 정보 추출
				for(int j=0; j<jDeptAdminList.size(); j++) {
					jAdmin = (JSONObject)jDeptAdminList.get(j);
					bizMap.put("ADMIN_ID", jAdmin.get("ADMIN_ID"));
					String sts = (String)jAdmin.get("STS");
					if(sts.equals("SAVE")) {
						if (Integer.parseInt(getItem("mg.su.countAdmin", bizMap).toString()) == 0) {
							//신규 사업장 번호 할당
							//Map bizCd = (Map)getItem("mg.su.getBizCd", null);
							//bizMap.put("BIZ_CD", bizCd.get("BIZ_CD"));
							//createObject("mg.su.insertAdmin", bizMap);
							createObject("mg.su.insertAdmin2", bizMap);
							log.info("SU 관리자 신규 등록 : " + bizMap.toString());
						}
					} else if(sts.equals("DEL")) {
						deleteObject("mg.su.deleteAdmin", bizMap);
						log.info("SU 관리자 삭제 : " + bizMap.toString());
					}  
				}
			}

			result.put("errcode", "0");
			result.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveBizAdmin---------------------------------------------------\n"	+ e.toString());
			result.put("errcode", "-1");
			result.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		}
		return write(result.toString());
	}
}
