package mg.su;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.SeedEncryptUtil;

public class checkListAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		if(request.isMode("selectCountUsb")) {
            return getCheckList(request);
		 } else {
			 return write(null);
		 }
		
	}
	
	private IResponse getCheckList(IRequest request) {
		
		JSONObject obj = new JSONObject();
	
		try {
			Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
			//생성가능한 USB
        	Map map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
			String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

			String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
			int limitUsb = Integer.parseInt(plainLicense);
			String limitUsbCount=String.valueOf(limitUsb);
			obj.put("limitCnt", limitUsbCount);
        	map.clear();
        	
        	//등록된 모든 USB
	    	 map = (Map)getItem("mg.su.selectAllUsbCnt",smap);
	    	obj.put("allCnt", this.getStringWithNullCheck(map, "CNT"));
	    	map.clear();
	    	
	    	//보유 중
	    	map = (Map)getItem("mg.su.selectHaveUsbCnt",smap);
			obj.put("haveCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//대여 중 
			map = (Map)getItem("mg.su.selectRentUsbCnt",smap);
			obj.put("rentCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//반납일 초과
			map = (Map)getItem("mg.su.selectReturnDtUsbCnt",smap);
			obj.put("returnDtCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//반납일 초과
			map = (Map)getItem("mg.su.selectExpireDtUsbCnt",smap);
			obj.put("expireDtCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//분실
			map = (Map)getItem("mg.su.selectLostUsbCnt",smap);
			obj.put("lostCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//보유 중 분실
			map = (Map)getItem("mg.su.selectLostImportUsbCnt",smap);
			obj.put("lostImportCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//반출 중 분실
			map = (Map)getItem("mg.su.selectLostExportUsbCnt",smap);
			obj.put("lostExportCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//사용금지
			map = (Map)getItem("mg.su.selectBanUsbCnt",smap);
			obj.put("banCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//고장
			map = (Map)getItem("mg.su.selectMalUsbCnt",smap);
			obj.put("malCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
	    	return write(obj.toString());
		
		}catch(Exception e) {
       	 	log.error("---CheckList Action 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	    	
	}
	
}