package mg.su;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class standbyListAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		
		 if(request.isMode("selectStandbyList")) {   /** 전체 조회    */
	            return getStandbyList(request);
		 } else if(request.isMode("selectStandbyObj")) {   /** 단일 객체 조회    */
	            return getStandbyObj(request);
		 } else {
			 return write(null);
		 }	
	}

	private IResponse getStandbyList(IRequest request) {
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists = getList("mg.su.selectStandbyList",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
        
	}
	
	private IResponse getStandbyObj(IRequest request) {
		
		JSONObject obj = new JSONObject();
        Map map = null;
        
        try{
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        
        	map = (Map)getItem("mg.su.selectStandbyList",smap);
        	Iterator it = map.keySet().iterator();
        	
        	while(it.hasNext()) {
        		String key = (String)it.next();        		
        		obj.put(key, this.getStringWithNullCheck(map, key));
        	}
        	obj.put("errcode", "0");
        	obj.put("errmsg", mm.getMessage("COMG_1002", request.getUser().getLocale()));
	        //log.info("obj :: " + obj.toString());
	        
        	return write(obj.toString());
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	obj.put("errcode", "1");
            obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
            
            return write(obj.toString());
        }
        
	}

}
