package mg.su;

import java.util.Map;

import org.json.simple.JSONObject;



import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;


public class detailInfoAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		
		  if(request.isMode("selectApplyPolicy")) {   /** 상세 정보 조회    */
	            return getApplyPolicy(request);
		 } else {
			 return write(null);
		 }
	}
	
	//하단 폼에 설정된 정책들을 표시하기 위한...
	private IResponse getApplyPolicy(IRequest request) {
		// TODO Auto-generated method stub
		try {
   		 
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
	    	Map map = (Map)getItem("mg.su.selectApplyPolicy",smap);
	    	
	    		// Creation of Json Response 
	    		 JSONObject obj = new JSONObject();
	    		obj.put("MOD_STOP", this.getStringWithNullCheck(map, "MOD_STOP"));
	    		obj.put("RETURN_YMD", this.getStringWithNullCheck(map, "RETURN_YMD"));
	    		obj.put("USB_NM", this.getStringWithNullCheck(map, "USB_NM"));
	    		obj.put("USB_AUTH_CL_CD", this.getStringWithNullCheck(map, "USB_AUTH_CL_CD"));
	    		
	    		obj.put("IS_EXPIRE_DT", this.getStringWithNullCheck(map, "IS_EXPIRE_DT"));
	    		obj.put("EXPIRE_YMD", this.getStringWithNullCheck(map, "EXPIRE_YMD"));
	    		obj.put("IS_EXPIRE_INIT", this.getStringWithNullCheck(map, "IS_EXPIRE_INIT"));
	    		
	    		obj.put("IS_READCOUNT", this.getStringWithNullCheck(map, "IS_READCOUNT"));
	    		obj.put("READCOUNT", this.getStringWithNullCheck(map, "READCOUNT"));
	    		obj.put("IS_READ_INIT", this.getStringWithNullCheck(map, "IS_READ_INIT"));
	    		
	    		obj.put("NO_SAVE", this.getStringWithNullCheck(map, "NO_SAVE"));
	    		obj.put("NO_COPY", this.getStringWithNullCheck(map, "NO_COPY"));
	    		obj.put("NO_PRINT", this.getStringWithNullCheck(map, "NO_PRINT"));
	    		
	    		obj.put("WAIT_TIME", this.getStringWithNullCheck(map, "WAIT_TIME"));
	    		obj.put("IS_WAIT_CLOSE", this.getStringWithNullCheck(map, "IS_WAIT_CLOSE"));
	    		
	    		obj.put("IS_LOGIN_FAIL_INIT", this.getStringWithNullCheck(map, "IS_LOGIN_FAIL_INIT"));
	    		obj.put("IS_LOGIN_FAIL_COUNT", this.getStringWithNullCheck(map, "IS_LOGIN_FAIL_COUNT"));
	    		
	    		obj.put("IS_MODIFIED_USER_AUTH", this.getStringWithNullCheck(map, "IS_MODIFIED_USER_AUTH"));
	    		
	    		return write(obj.toString());
	         } catch(Exception e) {
	       	 	log.error("---SU 적용 정책 불러오기 실패-------------------------\n" + e.toString());
	       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
	         }		
	}
	

}
