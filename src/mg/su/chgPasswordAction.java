package mg.su;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;

public class chgPasswordAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		 if(request.isMode("chkPassword")) { /** 비밀번호 확인 */
	        	return chkPassword(request);                                      
	     }else if(request.isMode("chgPassword")){	/** 일반사용자 비밀번호 변경 */
	        	return chgPassword(request);
	     }else if(request.isMode("chgPasswordAdmin")){	/** 관리자 비밀번호 변경 */
	        	return chgPasswordAdmin(request);
	     }else{ 
	            return write(null);
	     }
		
	}

	private IResponse chkPassword(IRequest request) {
		// TODO Auto-generated method stub
		//비밀번호 확인
		JSONObject obj = new JSONObject();
		
		String usbId = request.getStringParam("USB_ID");
		String rentId = request.getStringParam("USB_CREATE_ID");
		String passwd = request.getStringParam("PASSWORD");
		
		try{
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
	    	Map map = (Map)getItem("mg.su.selectIdPasswd",smap);
	    	String passwd_db = this.getStringWithNullCheck(map, "PASSWD_W");
	    	
	    	if(passwd.equals(passwd_db)){
	    		log.info("비밀번호 일치");
	    		obj.put("chkValue", "true");
	    	}else{
	    		log.info("비밀번호 불일치");
	    		obj.put("chkValue", "false");
	    	}
			//passwd랑 DB에 있는 비밀번호 비교해서 같은지 다른지
			//Json 파라미터에 chkValue 변수에 true/false로 넘겨주셈
			
	    	return write(obj.toString());
		}catch(Exception e){
	    		log.error("---------------------------------------------------------------\n" + e.toString());
				return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
    	}
    	
	}

	private IResponse chgPassword(IRequest request) {
		// TODO Auto-generated method stub
		
		JSONObject retobj = new JSONObject();
		JSONObject obj = new JSONObject();
		
		String usbId = request.getStringParam("USB_ID");
		String rentId = request.getStringParam("USB_CREATE_ID");
		String passwd = request.getStringParam("PASSWORD");
		String userIp = request.getStringParam("IP_ADDR");
		try{
			tx.begin();
			
			//비밀번호 변경
			Map smap = request.getMap();
			
			String dbName = pm.getString("component.sql.database");
			smap.put("dbName", dbName);
			
			if(rentId == null){
		  		//USB에서 요청할 경우 예전 PASSWORD를 확인한다.
		  		Map map = (Map)getItem("mg.su.selectIdPasswd",smap);
		    	String passwd_db = this.getStringWithNullCheck(map, "PASSWD_W");
		    	String oldPass = request.getStringParam("OLD_PASSWORD");
		    	if(oldPass.equals(passwd_db)){
		    		log.info("비밀번호 일치");
		    		
		    	}else{
		    		log.info("비밀번호 불일치");
		    		retobj.put("errcode", "-2");
			        retobj.put("errmsg", "초기 비밀번호가 맞지 않습니다.");
			        return write(retobj.toString());
		    	}
		    	//USB_CREATE_ID를 찾는다. 
		  		map = (Map)getItem("mg.su.selectRentId",smap);
		  		rentId=this.getStringWithNullCheck(map, "USB_CREATE_ID");
		  		smap.put("USB_CREATE_ID", rentId);
			}
			
	     	updateObject("mg.su.chgPasswd", smap);

	     	log.info("비밀번호 변경완료");
	     	
	     	
	     	// 로그쓰기위한 USER_ID 얻어옴
	     	Map map = (Map)getItem("mg.su.selectIdPasswd",smap);
	    	String renderId = this.getStringWithNullCheck(map, "RENDER_ID");
	     	
			//log 기록
			Map item = new HashMap(); 
	          
			item.put("USB_ID", usbId);
			item.put("USB_CREATE_ID",rentId);
			item.put("USER_ID", renderId);
			item.put("IP_ADDR", userIp);
			item.put("WORK_TYPE", "014");         
		  	
	     	 if("mssql".equals(dbName)) {
	     		 log.info("비밀번호 log 기록");
	     		 createObject("mg.su.insertWebLog", item); 
	     	 } else {
	     		 createObject("mg.su.insertWebLog_" + dbName, item); 
	     	 }
			
			
			tx.commit();
			
			retobj.put("errcode", "0");
	        retobj.put("errmsg", "Successful");
			
		}catch(Exception e){
			log.error("---------------------------------------------------------------\n" + e.toString());
			tx.rollback();
			retobj.put("errcode", "-1");
	        retobj.put("errmsg", "비밀번호 변경 중 오류 ");
			
		}
		
		return write(retobj.toString());
	}

	
	private IResponse chgPasswordAdmin(IRequest request) {
		// TODO Auto-generated method stub
		//관리자가 비밀번호 변경한 경우 관리자 이름으로 Log를 써야한다
		JSONObject obj = new JSONObject();
		
		String usbId = request.getStringParam("USB_ID");
		String rentId = request.getStringParam("USB_CREATE_ID");
		String passwd = request.getStringParam("PASSWORD");
		String adminId = request.getStringParam("ADMIN_ID");
		String userIp = request.getStringParam("IP_ADDR");
		String userId = request.getStringParam("USER_ID");
		try{
			tx.begin();
			
			//비밀번호 변경
			Map smap = request.getMap();
			
			String dbName = pm.getString("component.sql.database");
			
	     	updateObject("mg.su.chgPasswd", smap);
	     	
	     	log.info("관리자가 비밀번호 변경완료");
			
	     	//log 기록
			Map item = new HashMap(); 
	          
			item.put("USB_ID", usbId);
			item.put("USB_CREATE_ID",rentId);
			item.put("USER_ID", adminId);
			item.put("IP_ADDR", userIp);
			item.put("WORK_TYPE", "014");
	          
	     	createObject("mg.su.insertWebLog"+getDBName(dbName), item); 
	     	
			tx.commit();
			
			return write("Success");
			
			
		}catch(Exception e){
			log.error("---------------------------------------------------------------\n" + e.toString());
			tx.rollback();
			return write("Fail");
		}
		
	}
	
	
}
