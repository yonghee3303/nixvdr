package mg.su;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mg.base.BaseAction;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.core.base.ComponentRegistry;
import com.core.component.crypto.ICryptoManagement;
import com.core.component.mail.IMailManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
* 업무 그룹명 : mg.cp
* 서브 업무명 : approvalAction.java
* 작성자 : cyLeem
* 작성일 : 2014. 04. 03 
* 설 명 : approvalAction
*/ 
public class approvalAction extends BaseAction{

	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
	private String ERR_Message;
	private String RET_Value;
	// protected IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	/**
     * 반출신청 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("authUser")) {   // 웹인증 + 로그기록
        	return authUser(request);                                      
        }else if(request.isMode("saveDocLog")){ // 로그만 기록
        	return saveDocLog(request);
        }else if(request.isMode("getUSBPolicy")){
        	return getUSBPolicy(request);
        }else if(request.isMode("getIPAddr")){
        	return getIPAddr(request);
        }else { 
            return write(null);
        }
    } 
    
      private IResponse getIPAddr(IRequest request) {
		// TODO Auto-generated method stub
    	  JSONObject result = new JSONObject();
    	  result.put("IP_ADDR",((HttpServletRequest)request.getAdapter(HttpServletRequest.class)).getRemoteAddr());
		return write(result.toString());
	}

	/**
       * 보안 USB 인증 
       * @param request
       * @return
       * @throws Exception
       * http://192.168.1.121:8080/cp/approval.do?mod=authUser&DOC_NO=20140426141043022581&USER_ID=000001&PASSWD=1234
       */
      private IResponse authUser(IRequest request) throws Exception 
      {
    	  log.info("인증Action 메소드 진입"); 	  
    	  try{
    	  JSONObject result = new JSONObject();
    	  
    	  String usbId = request.getStringParam("USB_ID");
    	  String userId = request.getStringParam("USERID");
    	  String passwd = request.getStringParam("PASSWD");
    	  String j_language = request.getStringParam("j_language");
    	  String logData = request.getStringParam("log");
    	  
    	  log.info("USB_ID : "+usbId);
    	  log.info("PASSWD : "+passwd);
    	  log.info("j_language : "+j_language);
    	  
    	  //해당 usbId의 비밀번호를 DB에서 가져와, 현재 passwd 값과 비교한다.
    	  Map smap = request.getMap();
  		  smap.put("dbName", pm.getString("component.sql.database"));
  		
  		  Map map = (Map)getItem("mg.su.selectIdPasswd",smap);
  		  String usbId_db = this.getStringWithNullCheck(map, "USB_ID");
  		  String passwd_db = this.getStringWithNullCheck(map, "PASSWD_W");
  		  String usbCreateId = this.getStringWithNullCheck(map, "USB_CREATE_ID");
  		  String renderId = this.getStringWithNullCheck(map, "RENDER_ID");
  		  String modStop = this.getStringWithNullCheck(map, "MOD_STOP");
  		  String loginMsg = this.getStringWithNullCheck(map, "LOGIN_MSG");
  		  String isApproval = this.getStringWithNullCheck(map, "IS_APPROVAL");
  		  log.info("PASSWD_DB : "+passwd_db);
  		  smap.put("USB_CREATE_ID", usbCreateId);
  		  
  		  Object obj = JSONValue.parse(logData);
  		  JSONObject jData = (JSONObject)obj;
        
  		  Map item = new HashMap(); 
        
  		  item.put("USB_ID", jData.get("USB_ID"));
  		  item.put("USB_CREATE_ID",usbCreateId);
  		 // item.put("WORK_TYPE", jData.get("WORK_TYPE")); //상황에 따라 매번 바뀐다.
  		  item.put("IP_ADDR", ((HttpServletRequest)request.getAdapter(HttpServletRequest.class)).getRemoteAddr());
  		  item.put("IP_REMOTE", jData.get("IP_REMOTE"));
  		  item.put("MAC_ADDR", jData.get("MAC_ADDR"));
  		  item.put("WORKGROUP", jData.get("WORKGROUP"));
  		  item.put("PC_LOGIN_ID", jData.get("PC_LOGIN_ID"));
  		  item.put("USER_ID", renderId);
	  	  item.put("MESSAGE", jData.get("Message"));
	  	 
	  	  Map policyMap = (Map)getItem("mg.su.selectPolicy",smap);//로그처리를 위해 정책을 읽어오고, 이후 그에 따른 errcode를 ocx에게 넘겨주어 처리를 하게 된다.
  		  
		 String userAuth = this.getStringWithNullCheck(policyMap,"USER_AUTH_YN");
	     String runMac = this.getStringWithNullCheck(policyMap, "RUN_MAC_YN");
	  			  
	  	  //1. 해당usbId가 없는 경우
    	  //에러발생 Json으로 에러코드,에러메시지 전송
  		  if(!usbId.equals(usbId_db)){
  			 result.put("errcode", "1");
  			 result.put("errmsg", "해당 USB는 등록되지 않은 USB입니다.");
  			 return write(result.toString());
  		  }
  		  //비밀번호 오류로 인한 로그처리
  		  smap.put("COUNT_CD","004");
    	 // Map logCountMap = (Map)getItem("mg.su.selectLogCount",smap);
    	  int loginFailCount = Integer.parseInt(getItem("mg.su.selectLogCount",smap).toString()); //비밀번호 오류가 몇번있었는 지 얻어온다.
    	  //사용자 인증 시 
    	  if(!"1".equals(userAuth)){ //1 사용자 ID/PASSWORD 인증, 0 비번만 입력해서 인증
    		  if(!renderId.equals(userId)){
    			  log.info("사용자 아이디가 일치하지 않습니다.");
    			  result.put("errcode", "2");
    			  result.put("errmsg", "사용자 아이디가 일치하지 않습니다.");
    			  item.put("WORK_TYPE", "017");
    		  }
    		  try{
				  tx.begin();
				  String dbName = pm.getString("component.sql.database");
				  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 
				  tx.commit();
			  }catch(Exception e){
				  tx.rollback();
				  log.error("사용자ID 입력 오류에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
			  }
        	  return write(result.toString());
    	  }
    	  
  		  if(!passwd.equals(passwd_db)){
    		  log.info("비밀번호가 일치하지 않습니다."); 
    		 if(this.getStringWithNullCheck(policyMap, "IS_LOGIN_FAIL_INIT").equals("1")){
    			 if(Integer.parseInt(this.getStringWithNullCheck(policyMap, "IS_LOGIN_FAIL_COUNT")) <= loginFailCount){
    				  result.put("errcode", "2"); 
    				  result.put("errmsg", "보안USB 로그인 횟수 초과로 인해 USB가 초기화 됩니다.");
    				  item.put("WORK_TYPE", "005");
    			  }
    		  }else{
    			  log.info("로그인 실패 횟수 초과시 초기화하지 않음.");
    			  result.put("errcode", "2");
    			  result.put("errmsg", "보안 USB의 비밀번호가 맞지 않습니다.");
    			  item.put("WORK_TYPE", "004");
    		  }
    		  try{
				  tx.begin();
				  String dbName = pm.getString("component.sql.database");
				  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 

				  tx.commit();
			  }catch(Exception e){
				  tx.rollback();
				  log.error("비밀번호 입력 오류에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
			  }
        	  return write(result.toString());
    	  }
    	  
    	  //사용횟수 초과로 인한 로그기록 
    	  smap.remove("COUNT_CD");
    	  smap.put("COUNT_CD","003");
    	  //아래와 같이 계속 변환하는 이유는 count함수를 써서 어떠한 것도 없을 때 0을 반환하는 것이 아니라 ""로 반환하기 때문에 nullPointExeption이 발생한다. 쿼리에서 그룹함수를 사용할 때는 이러한 문제에 유의해야한다.
    	  int useCount= Integer.parseInt(getItem("mg.su.selectLogCount",smap).toString()); //실행성공 횟수가 몇번있었는지 얻어온다.
    	  if(this.getStringWithNullCheck(policyMap, "IS_READCOUNT").equals("1")){
    		  log.info("사용횟수를 제약하였습니다.");
    		  if(Integer.parseInt(this.getStringWithNullCheck(policyMap, "READCOUNT"))<=useCount){
    			  log.info("사용횟수가 초과하였습니다.");
    			  if(this.getStringWithNullCheck(policyMap, "IS_READ_INIT").equals("1")){//사용횟수 초과시 초기화를 할 것인가
    				  log.info("사용횟수를 초과하여 USB를 초기화 하겠습니다.");
    				  result.put("errcode", "2"); 
    				  result.put("errmsg", "사용횟수 초과로 인해 USB가 초기화됩니다.");
    				  item.put("WORK_TYPE", "007");
    			  }else{
    				  result.put("errcode", "2");
    				  result.put("errmsg", "사용횟수가 초과하였습니다.");
    				  item.put("WORK_TYPE", "010");
    			  }try{
    				  tx.begin();
    				  String dbName = pm.getString("component.sql.database");
    				  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 

    				  tx.commit();
    			  }catch(Exception e){
    				  tx.rollback();
    				  log.error("사용횟수 초과에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
    			  }
            	  return write(result.toString());
    		  }
    	  }
    	  
    	//만료기간 초과로 인한 로그기록 
    	  Map expireMap = (Map)getItem("mg.su.selectExpire",smap);
    	  if(this.getStringWithNullCheck(policyMap, "IS_EXPIRE_DT").equals("1")){
    		  log.info("만료기간을 설정하였습니다.");
    		  if(Double.valueOf(this.getStringWithNullCheck(expireMap, "EXPIRE"))<0){ //0보다 작으면 만료기간이 지난 것이다.
    			  log.info("만료기간이 지났습니다.");
    			  if(this.getStringWithNullCheck(policyMap, "IS_EXPIRE_INIT").equals("1")){//만료기간이 지난 후 초기화 할 것인지에 관한 
    				  log.info("만료기간이 지나 USB를 초기화 합니다.");
    				  result.put("errcode", "2"); 
    				  result.put("errmsg", "만료기간 초과로 USB를 초기화합니다.");
    				  item.put("WORK_TYPE", "006");
    			  }else{																	//만료기간이 지난 후 로그에만 기록 
    				  result.put("errcode", "2");
    				  result.put("errmsg", "만료기간이 초과하였습니다.");
    				  item.put("WORK_TYPE", "009");
    			  }
    			  try{
    				  tx.begin();
    				  String dbName = pm.getString("component.sql.database");
    				  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 

    				  tx.commit();
    			  }catch(Exception e){
    				  tx.rollback();
    				  log.error("만료기간 초과에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
    			  }
            	  return write(result.toString());
    		  }
    	  }
    	  //사용금지가 걸렸을 경우
    	  if("1".equals(modStop)){
    		  log.info("사용금지로 사용을 금합니다.");
    		  result.put("errcode","2");
    		  result.put("errmsg", "이 USB는 관리자에 의해 사용이 금지되었습니다.");
    		  item.put("WORK_TYPE", "013");
    		  try{
				  tx.begin();
				  String dbName = pm.getString("component.sql.database");
				  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 
				  
				  tx.commit();
			  }catch(Exception e){
				  tx.rollback();
				  log.error("사용금지에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
			  }
        	  return write(result.toString());
    	  }
    	  
    	  log.info("웹 인증 성공 - log를 파싱하여 기록합니다.");
    	  
     	 	result.put("CREATETYPE", "1"); //
     	 	result.put("PASSWORD", passwd_db);//
     	 	result.put("MODSTOP",modStop);//-----------추가
     	 	result.put("WEBPASSWORD", "");//---------------------------------없음
     	 	result.put("DRIVEUNIT", "MB");//
    		
    		result.put("CHECKSIZE", "1");//
     	 	
     	 	
     	 	result.put("USBID",  this.getStringWithNullCheck(policyMap, "USB_ID"));//
     	 	result.put("AUTHORITYTYPE", this.getStringWithNullCheck(policyMap, "USB_AUTH_CL_CD"));
     	 	/*
     	 	result.put("ISPROTECTWRITE", this.getStringWithNullCheck(policyMap, "NO_SAVE"));//
     	 	result.put("ISPROTECTCOPY", this.getStringWithNullCheck(policyMap, "NO_COPY"));//
     	 	result.put("ISPROTECTPRINT", this.getStringWithNullCheck(policyMap, "NO_PRINT"));//
     		result.put("ISUSEEXPIREDATE", this.getStringWithNullCheck(policyMap, "IS_EXPIRE_DT"));//
    		result.put("EXPIREDATE", this.getStringWithNullCheck(policyMap, "EXPIRE_YMD"));//
    		result.put("ISKILLATEXPIREDATE", this.getStringWithNullCheck(policyMap, "IS_EXPIRE_INIT"));//
    		result.put("ISCHECKUSECOUNT", this.getStringWithNullCheck(policyMap, "IS_READCOUNT"));//
    		result.put("USECOUNT", this.getStringWithNullCheck(policyMap, "READCOUNT"));//
    		result.put("ISKILLATUSECOUNT", this.getStringWithNullCheck(policyMap, "IS_READ_INIT"));//
    		result.put("ISCHECKSLEEP", this.getStringWithNullCheck(policyMap, "IS_WAIT_CLOSE"));//
    		result.put("SLEEPTIME", this.getStringWithNullCheck(policyMap, "WAIT_TIME"));//
    		result.put("ISCHECKLOGIN", this.getStringWithNullCheck(policyMap, "IS_LOGIN_FAIL_INIT"));//
    		result.put("LOGINCOUNT", this.getStringWithNullCheck(policyMap, "IS_LOGIN_FAIL_COUNT"));//
    		result.put("MACLIST", this.getStringWithNullCheck(policyMap, "MAC_ADDR"));//
    		result.put("USBIMAGE", this.getStringWithNullCheck(policyMap, "USB_ENCODE_CL_CD"));//
    		result.put("DRIVESIZE", this.getStringWithNullCheck(policyMap, "USB_IMAGE_SIZE"));//
    		result.put("ISUSEMACLIST", runMac); //0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
    		*/
    		
    		log.info("지금까지 USB를 사용한 횟수 : " + String.valueOf(useCount));
    		result.put("ACTUSECOUNT", String.valueOf(useCount));//
    		
    		
    		Map prodNomap = (Map)getItem("mg.su.selectProdNo",smap);
    		result.put("PRODNO", this.getStringWithNullCheck(prodNomap, "PRODNO"));
    		result.put("ENCKEY", this.getStringWithNullCheck(prodNomap, "PRODNO").substring(0,5));// ->데이터베이스에서  prodno을 갖고와서 앞에 5자리만...
    		
    		Map baseMap = (Map)getItem("mg.su.selectBaseUrl",smap);
    		result.put("BASEURL", this.getStringWithNullCheck(baseMap, "BASEURL")); //
    		//result.put("BASEURL", "http://localhost:8080");
    		
        	Map companyMap = (Map)getItem("mg.su.selectCompany",smap);
        	result.put("COMPANY", this.getStringWithNullCheck(companyMap, "COMPANY")); //
      
        	Map timeMap = (Map)getItem("mg.su.selectTimeout",smap);
        	result.put("TIMEOUT", this.getStringWithNullCheck(timeMap, "TIMEOUT")); //
    		
        	Map masterMap = (Map)getItem("mg.su.selectUsbNmKey",smap);
    		result.put("USBTITLE", this.getStringWithNullCheck(masterMap, "USB_NM"));//
    		result.put("RANDOMKEY",this.getStringWithNullCheck(masterMap, "USB_KEY"));//
    		
    		
    		result.put("ISKILLWITHOUTWARNING", "0");//
    		result.put("WAITINGTIME", "10");//쓰기 금지일 시 대기시간
    		result.put("ISUSELOGFLAG", "0");
    		result.put("LOGEMAIL", "");//
    		result.put("LOGIN_MSG", loginMsg);
    		result.put("ISALLOWWEBCONTROL", "0");//
    		result.put("ISNOTUSEDATDISCONNECT", "0");//
    		result.put("ISNOTUSEDOUTSIDECOMPANY", "0");//수정
    		result.put("ISONLYUSEDMAC", "0");//
    		result.put("SITE_CREATE", "");//-----------------------
    		result.put("SITE_LOG", "");//
    		result.put("SITE_CONNECT", "");//
    		result.put("SITE_INOUT", "");//
    		result.put("SITE_CHANGEPASSWORD", "");//
    		result.put("IS_APPROVAL", isApproval);//
    		
    		
    		//result.put("ERRORCODE", "1");//---------------------------------------
    		//result.put("ERRORSTRING", "1");//--------------------------------
    		
    		//API에 없는 파라미터들
    		result.put("ISCAHNGEPASSWORD", "0");//
    		result.put("FULLPASSWORD", "");//
    		result.put("ISUSEIMAGEDRIVE", "0");//
    		result.put("ISSETAFP", "0");
    		result.put("ISSETAFPLIST", "1");
    		//result.put("ISUSESHADOWPROCESS", "2");
    		//result.put("SHADOW_INCPROCESS", "3");
    		//result.put("SHADOW_EXCPROCESS", "4");
    		result.put("SHADOW_INCPROCESS", getExcProcess("S001")); //SHADOW_INCPROCESS의 값이 있으면 ISUSESHADOWPROCESS가 1, 없으면 0 
    		log.info("-------------------------------------------------------------------------retval---"+RET_Value);
    		result.put("ISUSESHADOWPROCESS", RET_Value);
    		result.put("NOHIDEPROCESS", getExcProcess("S002")); 
    		result.put("SHADOW_EXCPROCESS", "|");
    		
            try{
	        	  item.put("WORK_TYPE", "003");//정상적으로 인증에 성공하였을 때
	        	  tx.begin();
	        	  String dbName = pm.getString("component.sql.database");
	        	  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 
	      
	   	  	 	  tx.commit();
	  	  	  }catch(Exception e){
	  	  		  tx.rollback();
	  	  		  log.error("웹 인증성공에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
	  	  	  }
    		
            result.put("errcode", "0");
        	result.put("errmsg", "웹 인증이 정상적으로 처리되었습니다.");
        	log.info("웹 인증이 정상적으로 처리되었습니다.--------------------------"+result.toString());
        	return write(result.toString());
    	  }catch(Exception e){
    		  log.error("------------------------------------------------------------------"+e.toString());
    		  return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
    	  }
    	
      }
    	     
      /**
       * 보안 USB 접근로그 저장
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse saveDocLog(IRequest request) throws Exception 
      {
    	  //로그기록
    	  
    	  
    	  //리턴해줄 Json객체 생성
    	  JSONObject result = new JSONObject();
    	  
    	  try{
    		  
	    	  String j_language = request.getStringParam("j_language");
	    	  String logData = request.getStringParam("log");
	    	  
	    	
	    	  //Json타입의 log를 파싱하여 DB에 log를 기록하고, (성공)에러코드,(성공)에러메시지 전송.
	    	  
	    	  Object obj = JSONValue.parse(logData);
	          JSONObject jData = (JSONObject)obj;
	          
	          log.info("saveDocLog :: "+jData.toString());
	          
	    	  Map smap = request.getMap();
	  		  smap.put("dbName", pm.getString("component.sql.database"));
	  		  smap.put("USB_ID",jData.get("USB_ID"));
	  		  
	  		  Map map = (Map)getItem("mg.su.selectRentId",smap);
	  		  String usbCreateId = this.getStringWithNullCheck(map, "USB_CREATE_ID");
	  		  
	          
	          Map item = new HashMap(); 
	          
	          item.put("USB_ID", jData.get("USB_ID"));
	          item.put("USB_CREATE_ID",usbCreateId);
	          item.put("USER_ID", jData.get("USER_ID"));
	          item.put("IP_ADDR",((HttpServletRequest)request.getAdapter(HttpServletRequest.class)).getRemoteAddr());// jData.get("IP_ADDR")
	          item.put("IP_REMOTE", jData.get("IP_REMOTE"));
	          item.put("MAC_ADDR", jData.get("MAC_ADDR"));
	          item.put("WORKGROUP", jData.get("WORKGROUP"));
	          item.put("WORK_TYPE", jData.get("WORK_TYPE")); 
	          item.put("MESSAGE", jData.get("Message"));
	          //item.put("COUNTRY_CD", jData.get("COUNTRY_CD"));
	          item.put("PC_LOGIN_ID", jData.get("PC_LOGIN_ID"));
	          
	          try{
	        	 
	        	  tx.begin();
	        	  String dbName = pm.getString("component.sql.database");
	        	  createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), item); 
	        	  
	        	  //비밀번호 변경 작업이 발생하였을 시 해당 정책의 상태값 N 으로 변경
	        	  if(item.get("WORK_TYPE").equals("014")) {
	        		  item.put("STATUS", "N");
	        		  updateObject("mg.su.usbPassReset", item);
	        		  log.info("비밀번호 변경에 따른 SU_POLICY 상태값 변경");
	        	  }

	   	  	 	  tx.commit();
	  	  	  }catch(Exception e){
	  	  		  tx.rollback();
	  	  		  log.error("웹 인증성공에 관한 로그처리 중-------------------------------------------------------------"+e.toString());
	  	  	  }
	            result.put("errcode", "0");
	        	result.put("errmsg", "정상적으로 로그를 기록하였습니다.");
	    
    	 }catch(Exception e){
    		 log.error("saveUSBLog Exception :: "+e.toString());
    		 result.put("errcode", "-1");
             result.put("errmsg", "로그를 기록하던 중 에러가 발생 하였습니다.");
             
    	 }
    	  return write(result.toString());     
    }
  	private String getExcProcess(String clcd)
  	{
  		 Map smap = new HashMap();
           smap.put("CP_EXC_CL_CD", clcd);
           String retval = "";
           String div = "";
           
           List lists = getList("mg.cp.selectExcPolicy",smap);
           for (int i = 0; i < lists.size(); i++) {
            	Map map =(Map) lists.get(i);
            	retval += div + this.getStringWithNullCheck(map, "EXC_PROCESS");
            	div = "|";
           }
           if(lists.size()<1) RET_Value = "0";
           else RET_Value ="1";
           return retval;
  	}
  	
	/**
     * 보안 USB 정책 송신
     * @param request
     * @return
     * @throws Exception
     * http://192.168.1.121:8080/cp/approval.do?mod=authUser&DOC_NO=20140426141043022581&USER_ID=000001&PASSWD=1234
     * 
     * //DB에서 가져온 값
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
	    	Map map = (Map)getItem("mg.su.selectProdNo",smap);
	    	obj.put("PRODNO", this.getStringWithNullCheck(map, "PRODNO"));
	    	obj.put("ENCKEY", this.getStringWithNullCheck(map, "PRODNO").substring(0,5));// ->데이터베이스에서  prodno을 갖고와서 앞에 5자리만...

	    	map = (Map)getItem("mg.su.selectBaseUrl",smap);
	    	obj.put("BASEURL", this.getStringWithNullCheck(map, "BASEURL")); //
	    	//obj.put("BASEURL","http://localhost:8080");
	    	
	    	map = (Map)getItem("mg.su.selectCompany",smap);
	    	obj.put("COMPANY", this.getStringWithNullCheck(map, "COMPANY")); //
	  
	    	map = (Map)getItem("mg.su.selectTimeout",smap);
	    	obj.put("TIMEOUT", this.getStringWithNullCheck(map, "TIMEOUT")); //
	    
	    	// jsp에서 받은 Json 값 파싱
	    	String policy = request.getStringParam("policy");
	    	Object policyData = JSONValue.parse(policy);
	        JSONObject jPolicyData = (JSONObject)policyData;
	        
	        obj.put("action", "CreateSecureUSB"); //
	        obj.put("CREATETYPE", "1"); 
	        
     */
  	
  	//mg/su/approval.do?mod=getUSBPolicy&USB_ID=&USB_CREATE_ID=
  	private IResponse getUSBPolicy(IRequest request) throws Exception 
    {
	  	  try {
	  		  /*String driver = this.getStringNullCheck(request.getStringParam("DRIVER"));
	  		  String serialNo = this.getStringNullCheck(request.getStringParam("USB_ID"));*/
	  		  log.info("보안 USB 정책 송신."); 	
			  return write(getSuPolicy(request, null));
	  	 } catch(Exception e){
	  		 JSONObject result = new JSONObject();
			 result.put("errcode", "1");
	         result.put("errmsg", "로그를 기록하던 중 에러가 발생 하였습니다." + e.getMessage());
	         return write(result.toString()); 
		 }
    }
    
    public String getSuPolicy(IRequest request, JSONObject jPolicyData)
    {
    	JSONObject result = new JSONObject();
    	JSONObject su_policy = new JSONObject();
    	Map smap = request.getMap();
    	smap.put("dbName", pm.getString("component.sql.database"));
    	try {
	    	// smap.put("dbName", pm.getString("component.sql.database"));
	    	// smap.put("USB_ID",jData.get("USB_ID"));
	    	// USB_ID, USB_CREATE_ID
    		log.info("1");
	    	result.put("action", "CreateSecureUSB"); //
	    	su_policy.put("CREATETYPE", "1");
	    	Map bmap = (Map)getItem("mg.su.selectProdNo",smap);
	    	su_policy.put("PRODNO", this.getStringWithNullCheck(bmap, "PRODNO"));
			su_policy.put("ENCKEY", this.getStringWithNullCheck(bmap, "PRODNO").substring(0,5));// ->데이터베이스에서  prodno을 갖고와서 앞에 5자리만...
			su_policy.put("EXPIRE_CHECK_OPTION", pm.getString("component.usb.expireCheckOpt"));//0:기한 만료, 1: 내부 일 경우 만요일 체크 안함
			//내외부 URL 호출
			bmap = (Map)getItem("mg.su.selectServerInfo",smap);
			if(bmap != null) {
				su_policy.put("BASEURL", this.getStringWithNullCheck(bmap, "INNER_SVR_URL"));	//내부서버 URL
				su_policy.put("INNER_SVR_URL", this.getStringWithNullCheck(bmap, "INNER_SVR_URL"));	//내부서버 URL
				su_policy.put("OUTER_SVR_URL", this.getStringWithNullCheck(bmap, "OUTER_SVR_URL"));	//외부서버 URL
				//su_policy.put("IP_RANGE_ADDR", this.getStringWithNullCheck(bmap, "IP_RANGE_ADDR"));	//IP 허용대역
				
				String ipAddrs = this.getStringWithNullCheck(bmap,"IP_RANGE_ADDR");
				ipAddrs = ipAddrs.replace(",", "~");
				su_policy.put("IP_RANGE_ADDR", ipAddrs);	//IP 허용대역
			} else {
				log.info("selectServerInfo Data : Null");
			}
			
			bmap = (Map)getItem("mg.su.selectCompany",smap);
			su_policy.put("COMPANY", this.getStringWithNullCheck(bmap, "COMPANY")); //
			bmap = (Map)getItem("mg.su.selectTimeout",smap);
			su_policy.put("TIMEOUT", this.getStringWithNullCheck(bmap, "TIMEOUT")); //
			
			Map policyMap = null;
			
			if(jPolicyData == null || jPolicyData.get("DRIVE") == null || "".equals(jPolicyData.get("DRIVE"))) {
				log.info("4-1 :: jPolicyData null");
				policyMap = (Map)getItem("mg.su.selectPolicy_Auth",smap);
				su_policy.put("USB_CREATE_ID",this.getStringWithNullCheck(policyMap,"USB_CREATE_ID"));
				su_policy.put("USB_ID",this.getStringWithNullCheck(policyMap,"USB_ID"));
				su_policy.put("SU_POLICY_GRP_CD",this.getStringWithNullCheck(policyMap,"SU_POLICY_GRP_CD"));
				su_policy.put("POL_CHG_YN", this.getStringWithNullCheck(policyMap,"POL_CHG_YN"));
				su_policy.put("USER_NM", this.getStringWithNullCheck(policyMap,"USER_NM"));
				su_policy.put("USER_ID", this.getStringWithNullCheck(policyMap,"USER_ID"));
				su_policy.put("DEPT_USER", this.getStringWithNullCheck(policyMap,"DEPT_USER"));
			  	tx.begin();
			  	smap.put("POL_CHG_YN", "N");
			  	sm.updateObject("mg.su.polChgYnUpdate",smap);
			  	tx.commit();
			} else {
				log.info("4-1 :: jPolicyData not null");
				smap.put("SU_POLICY_BASIC_CD", pm.getString("component.usb.export.default.policyCd"));
				policyMap = (Map)getItem("mg.su.selectPolicy_Basic",smap);
				DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
				String usbCreateId = formatter.format(new Date()) + getRandomString(6);
				su_policy.put("USB_CREATE_ID",usbCreateId);
				su_policy.put("USB_ID", (String)jPolicyData.get("USB_ID"));
				su_policy.put("SU_POLICY_GRP_CD", (String)jPolicyData.get("SU_POLICY_GRP_CD"));
			}
			log.info("5");
			
			String isApproval = this.getStringWithNullCheck(policyMap,"IS_APPROVAL");
			if("".equals(isApproval)) {
				isApproval = "N";
			}
			su_policy.put("IS_APPROVAL",isApproval);
			su_policy.put("USB_AUTH_CL_CD",this.getStringWithNullCheck(policyMap,"USB_AUTH_CL_CD"));	//Y:비밀번호 초기화 
			su_policy.put("IS_EXPIRE_DT",this.getStringWithNullCheck(policyMap,"IS_EXPIRE_DT"));
			su_policy.put("IS_EXPIRE_INIT",this.getStringWithNullCheck(policyMap,"IS_EXPIRE_INIT"));
			su_policy.put("IS_EXPIRE_STOP",this.getStringWithNullCheck(policyMap,"IS_EXPIRE_STOP"));
			su_policy.put("IS_READCOUNT",this.getStringWithNullCheck(policyMap,"IS_READCOUNT"));
			su_policy.put("IS_READ_INIT",this.getStringWithNullCheck(policyMap,"IS_READ_INIT"));
			su_policy.put("IS_READ_STOP",this.getStringWithNullCheck(policyMap,"IS_READ_STOP"));
			su_policy.put("IS_WAIT_CLOSE",this.getStringWithNullCheck(policyMap,"IS_WAIT_CLOSE"));
			su_policy.put("WAIT_TIME",this.getStringWithNullCheck(policyMap,"WAIT_TIME"));
			su_policy.put("IS_LOGIN_FAIL_INIT",this.getStringWithNullCheck(policyMap,"IS_LOGIN_FAIL_INIT"));
			su_policy.put("IS_LOGIN_FAIL_STOP",this.getStringWithNullCheck(policyMap,"IS_LOGIN_FAIL_STOP"));
			su_policy.put("IS_LOGIN_FAIL_COUNT",this.getStringWithNullCheck(policyMap,"IS_LOGIN_FAIL_COUNT"));
			su_policy.put("MAC_ADDR",this.getStringWithNullCheck(policyMap,"MAC_ADDR").replace("|", "$|+"));
			su_policy.put("USB_ENCODE_CL_CD",this.getStringWithNullCheck(policyMap,"USB_ENCODE_CL_CD"));
			su_policy.put("USB_IMAGE_SIZE",this.getStringWithNullCheck(policyMap,"USB_IMAGE_SIZE"));
			su_policy.put("EXPIRE_YMD",this.getStringWithNullCheck(policyMap,"EXPIRE_YMD"));
			su_policy.put("READCOUNT",this.getStringWithNullCheck(policyMap,"READCOUNT"));
			
			log.info("6");
			
			su_policy.put("WORK_DT",this.getStringWithNullCheck(policyMap,"WORK_DT"));
			su_policy.put("LOGIN_MSG",this.getStringWithNullCheck(policyMap,"LOGIN_MSG"));
			su_policy.put("MOD_STOP",this.getStringWithNullCheck(policyMap,"MOD_STOP"));
			
			log.info("7");
			
		  	result.put("SU_POLICY",su_policy);
		  	
		  	JSONArray resultList = new JSONArray();
		  	List lists = getList("mg.su.selectPolicySet",su_policy);
		  	log.info("list.size()-------------------------------------------------------------------------"+lists.size());
		  	for (int i = 0; i < lists.size(); i++) {
		  		Map map =(Map) lists.get(i);
		  		JSONObject node = new JSONObject();
		  		node.put("SET_ID",this.getStringWithNullCheck(map,"SET_ID"));
		  		node.put("SET_NM",this.getStringWithNullCheck(map,"SET_NM"));
		  		
		  		JSONArray policyList = new JSONArray();
		  		JSONObject subnode = new JSONObject();
		  		subnode.put("NO_SAVE",this.getStringWithNullCheck(map,"NO_SAVE"));
		  		subnode.put("NO_COPY",this.getStringWithNullCheck(map,"NO_COPY"));
		  		subnode.put("NO_PRINT",this.getStringWithNullCheck(map,"NO_PRINT"));
		  		subnode.put("NO_CLIP",this.getStringWithNullCheck(map,"NO_CLIP"));
		  		subnode.put("NO_USE",this.getStringWithNullCheck(map,"NO_USE"));
		  		subnode.put("DEL_YN",this.getStringWithNullCheck(map,"DEL_YN"));
		  		
		  		log.info("list["+i+"] : ===================="+subnode.toString());
		  		
		  		policyList.add(subnode);
		  		node.put("SET_CODE",policyList);
		  		
		  		resultList.add(node);
		  	}
		  	log.info("8");
		  	
		  	if(jPolicyData != null) {
		  		result.put("DRIVE", jPolicyData.get("DRIVE"));
		  	}

		  	result.put("SU_POLICY_SET", resultList);
		  	result.put("SERIALNO", "");
		  	result.put("errcode", "0");
		  	result.put("errmsg", "Success.");
		  	
		  	log.info("policy :: " + result.toString());
		  	
		  	
	  	
    	} catch(Exception e) {
    		result.put("errcode", "-1");
		  	result.put("errmsg", "getSuPolicy Fail.");
    		log.error("----- error :: " + e.getMessage());
    		tx.rollback();
    		
    	}
    	return result.toString();
    }
}
