package mg.su;

import java.util.List;
import java.util.Map;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class logMgmtAction extends BaseAction{

	@Override
	public IResponse run(IRequest request) throws Exception {
		
		if(request.isMode("selectLogMgmt")) {   /** 조회    */
            return getLogMgmt(request);
	 }else {
		 return write(null);
	 }
	}

	private IResponse getLogMgmt(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
       
     	Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.su.selectLogMgmt",request.getMap());
	       	 } else {
	       		 lists = getList("mg.su.selectLogMgmt_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
        
	}

}
