package mg.su;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.SeedEncryptUtil;
import mg.su.approvalAction;


public class enrollUSBAction extends BaseAction {
	
	private String RET_Value;
	
	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(request.isMode("selectEnrollUSB")) {		/** 보안USB 등록 - 기본정책삽입*/
            return getEnrollUSB(request);
		 }else if(request.isMode("createSecureUSB")){ 	/** 보안USB 반출  - 정책 정보를 json 형식으로 ocx에 넘겨주기 위함*/
			 return getCreateSecureUsb(request);
		 }else if(request.isMode("enrollUSB")){			/** 보안USB 등록 - DB에 USB 기본정보 입력 */
			 return saveUsbInfo(request);
		 }else if(request.isMode("savePolicy")){ 		/** 반출할 때 지정한 정책으로 저장 */
			 return savePolicy(request);
		 }else if(request.isMode("collectUSB")){ 		/** 보안USB 회수 */
			 return collectUSB(request);
		 }else if(request.isMode("delUSB")){			/** 보안USB 해제 */
			 return delUSB(request);
		 }else if(request.isMode("selectEnckey")){		/** enckey 조회 */
			 return selectEnckey(request);
		 }else if(request.isMode("checkUSB")){ 			/** USB의 수불 상태 확인 - 페이지 버튼 비활성화를 위한 작업*/
			 return getStatusUSB(request);
		 }else if(request.isMode("setCnt")){ /** 라이선스 갯수 변경 **/
			 return setCountUsb(request);
		 }else {
			 return write(null);
		 }
	}
	private IResponse getStatusUSB(IRequest request) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		log.info("getStatusUSB");
		try {
			Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
			//등록된 모든 USB
        		String temp = (String)getItem("mg.su.checkStatusUSB",smap);
        		log.info("------ USB 반출 여부 : " + temp);
	    		if("03".equals(temp)){ //보유 목록에서 상태값이 03이라면 반출된 USB이고, 아니라면 보유 중인 USB이다.  
	    			obj.put("haveUsb", "false"); 	//반출 USB
	    		}else{
	    			obj.put("haveUsb", "true");		//보유 USB
	    		}
			
	    	return write(obj.toString());
		
		}catch(Exception e) {
       	 	log.error("---CheckList Action 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}
	private IResponse selectEnckey(IRequest request) {
		// TODO Auto-generated method stub
		
		//Enckey를 가져온다.
		try{
			Map smap = request.getMap();
			Map map = (Map)getItem("mg.su.selectProdNo",smap);
			String enckey = this.getStringWithNullCheck(map, "PRODNO").substring(0,5);
			map = (Map)getItem("mg.su.selectVersion",smap);
			String version = this.getStringWithNullCheck(map, "SUVERSION");
			
			JSONObject obj = new JSONObject();
			
			obj.put("ENCKEY", enckey);
			obj.put("VERSION", version);
			log.info("obj.toString()----------------------selectEnckey-------------"+obj.toString());
			return write(obj.toString());
			
		}catch(Exception e){
			log.error("getEnckey-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("Fail");
		}
		
	}
	private IResponse collectUSB(IRequest request) {
		// TODO Auto-generated method stub
		log.info("회수처리 시작");
		//USB회수처리
		try{
			tx.begin();
			Map smap = request.getMap();     	
	     	String dbName = pm.getString("component.sql.database");
	     	Map map = (Map)getItem("mg.su.selectUsbRentId",smap); //USB_CREATE_ID는 생성시점의 시간과 기준 시간의 시점의 차이값으로 max함수를 사용하여 가장 최근의 USB_CREATE_ID를 얻어올 수 있다.
	     	//반출된 적이 없을 때  max함수와 같은 그룹함수를 사용한 쿼리는 결과가 없을 때 null로 반환하지 않고 ""로 반환하기 때문에 이같은 처리를 해주어야한다. 
	     	if(!"".equals(this.getStringWithNullCheck(map, "USB_CREATE_ID").toString())){	
	     		smap.put("USB_CREATE_ID", this.getStringWithNullCheck(map, "USB_CREATE_ID"));
	     		updateObject("mg.su.collectUSBRentMgt", smap); 
	     		smap.put("WORK_TYPE", "015");//회수 시 코드번호는 015
	     		if("mssql".equals(dbName)) {
	     			createObject("mg.su.insertWebLog",smap);
	     		}else{
	     			createObject("mg.su.insertWebLog_"+dbName,smap);
	     		}
	     	}
	     	updateObject("mg.su.collectUSBMaster", smap);
	     	tx.commit(); 
	     	return write("Collect Success");
		}catch(Exception e){
			log.error("collectUSB-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("Collect Fail");
		}
		
		
	}
	//보안USB 라이선스 현재 수량 변경 서비스
	//http://localhost:8080/mg/enrollUSB.do?mod=setCnt&CLNITC=100
	private IResponse setCountUsb(IRequest request) {
		try{
			Map smap = request.getMap();
			String licenseCount = this.getStringWithNullCheck(smap,"CLNITC");
			String paddingCount="";
			
			//다시 암호화 하기 위해서 패딩을 주어야 함..
			for(int i= 0; i<(16-licenseCount.length());i++){
				paddingCount = paddingCount.concat("0");
			}
			paddingCount = paddingCount.concat(licenseCount);
			String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");
	
			smap.put("OTHERDATA1", cipherLicense);
			tx.begin();
			updateObject("mg.su.updateLicense", smap);
			tx.commit();
		}catch(Exception e){
			tx.rollback();
			log.error("setUSBcnt------------------error-----------------------------------------"+e.toString());
		}
		return write(null);
	}
	private IResponse delUSB(IRequest request) {
		// TODO Auto-generated method stub
		//라이센스 USB갯수 정보 확인
		log.info("해제처리 시작");
		//보안USB 해제
		//반출목록 해당usbId의 수불상태를 폐기(06)로 변경
		//보유목록 해당usbId의 수불상태를 폐기(06)로 변경
		Map map;
		Map smap = request.getMap();
		try{
		//---------------------------------------------------------------------------------------라이센스 관련
			map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
			String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

			String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
			int limitUsb = Integer.parseInt(plainLicense);
			log.info("-------------------------------------------해제 전 남은 USB 갯수"+limitUsb);
			limitUsb = limitUsb +1;
			
			String modifyCount=String.valueOf(limitUsb);
			String paddingCount="";
			//다시 암호화 하기 위해서 패딩을 주어야 함..
			for(int i= 0; i<(16-modifyCount.length());i++){
			paddingCount = paddingCount.concat("0");
			}
			paddingCount = paddingCount.concat(modifyCount);
			String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");

			map.clear();
			map.put("OTHERDATA1", cipherLicense);
			updateObject("mg.su.updateLicense", map);
		//-----------------------------------------------------------------------------------------------------------
			tx.begin();
			
			updateObject("mg.su.deleteUSBMaster", smap);
			
			String dbName = pm.getString("component.sql.database");
			map = (Map)getItem("mg.su.selectUsbRentId",smap);
			//반출된 적 없이 바로 해제할 때...
			if(!"".equals(this.getStringWithNullCheck(map, "USB_CREATE_ID").toString())){	
				smap.put("USB_CREATE_ID", this.getStringWithNullCheck(map, "USB_CREATE_ID"));
				updateObject("mg.su.deleteUSBRentMgt", smap);
				smap.put("WORK_TYPE", "016");
				if("mssql".equals(dbName)) {
					createObject("mg.su.insertWebLog",smap);
				}else{
					createObject("mg.su.insertWebLog_"+dbName,smap);
				}
			}
	     	tx.commit(); 
			return write("Delete Success");
			
		}catch(Exception e){
			log.error("DeleteUSB-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("Delete Fail");
			
		}
		 
	}

	
	/**
	 * 최종수정일 : 2016.04.03.
	 * @param request
	 * @return
	 */
	private IResponse savePolicy(IRequest request) {
		
		/*Calendar c = Calendar.getInstance();
		long rentId =c.getTimeInMillis();*/
		
		log.info("반출완료후 정책DB저장");
		String policy = request.getStringParam("policySet");
        JSONObject jPolicyData = (JSONObject)JSONValue.parse(policy);
        
        log.info("savePolicy--------------------------------------------json 값 넣어주기 전" + request.getMap());
        Map map = new HashMap();
        //SU_RENT_MGT
        map.put("USB_ID", jPolicyData.get("USB_ID"));
        map.put("USB_CREATE_ID", jPolicyData.get("USB_CREATE_ID"));
        map.put("RENDER_ID", jPolicyData.get("RENDER_ID"));
        map.put("RENT_YMD", jPolicyData.get("RENT_YMD"));
        map.put("RENT_STS_CD", jPolicyData.get("RENT_STS_CD"));
        map.put("MOD_STOP", jPolicyData.get("MOD_STOP"));
        map.put("USB_NM", jPolicyData.get("USB_NM"));
        map.put("RETURN_YMD", jPolicyData.get("RETURN_YMD"));
        map.put("PASSWD_W", jPolicyData.get("PASSWD_W"));
        map.put("PASSWD_R", jPolicyData.get("PASSWD_R"));
        map.put("IS_MODIFIED_USER_AUTH", jPolicyData.get("IS_MODIFIED_USER_AUTH"));
        map.put("USER_AUTH_YN", jPolicyData.get("USER_AUTH_YN"));
        map.put("RUN_MAC_YN", jPolicyData.get("RUN_MAC_YN"));
        map.put("LOGIN_MSG", jPolicyData.get("LOGIN_MSG"));
        map.put("IS_APPROVAL", jPolicyData.get("IS_APPROVAL"));
        
        //SU_POLICY
        map.put("USB_AUTH_CL_CD", jPolicyData.get("USB_AUTH_CL_CD"));
        map.put("IS_EXPIRE_DT", jPolicyData.get("IS_EXPIRE_DT"));
        map.put("EXPIRE_YMD", jPolicyData.get("EXPIRE_YMD"));
        map.put("IS_EXPIRE_INIT", jPolicyData.get("IS_EXPIRE_INIT"));
        map.put("IS_READCOUNT", jPolicyData.get("IS_READCOUNT"));
        map.put("IS_READ_INIT", jPolicyData.get("IS_READ_INIT"));
        map.put("IS_WAIT_CLOSE", jPolicyData.get("IS_WAIT_CLOSE"));
        map.put("IS_LOGIN_FAIL_INIT", jPolicyData.get("IS_LOGIN_FAIL_INIT"));
        map.put("IS_LOGIN_FAIL_COUNT", jPolicyData.get("IS_LOGIN_FAIL_COUNT"));
        map.put("MAC_ADDR", jPolicyData.get("MAC_ADDR"));
        map.put("USB_ENCODE_CL_CD", jPolicyData.get("USB_ENCODE_CL_CD"));
        
        if(!"".equals(jPolicyData.get("WAIT_TIME"))) map.put("WAIT_TIME", jPolicyData.get("WAIT_TIME"));
        if(!"".equals(jPolicyData.get("READCOUNT"))) map.put("READCOUNT", jPolicyData.get("READCOUNT"));
        if(!"".equals(jPolicyData.get("USB_IMAGE_SIZE"))) map.put("USB_IMAGE_SIZE", jPolicyData.get("USB_IMAGE_SIZE"));
        
        map.put("IS_EXPIRE_STOP", jPolicyData.get("IS_EXPIRE_STOP"));
        map.put("IS_READ_STOP", jPolicyData.get("IS_READ_STOP"));
        map.put("SU_POLICY_BASIC_CD", jPolicyData.get("SU_POLICY_BASIC_CD"));
        map.put("IS_LOGIN_FAIL_STOP", jPolicyData.get("IS_LOGIN_FAIL_STOP"));
        map.put("SU_POLICY_GRP_CD", jPolicyData.get("SU_POLICY_GRP_CD"));
        map.put("USER_ID", request.getUser().getId());
        
        
        log.info("savePolicy---------------------------------------------------map--------"+map.toString());
		try{
			
			tx.begin();
			map.put("RENT_STS_CD", "03");
			map.put("dbName", pm.getString("component.sql.database"));
			map.put("WORK_TYPE", "001"); //보안USB생성 로그를 기록하기 위한 값.
		  	String dbName = pm.getString("component.sql.database");
		  	createObject("mg.su.insertRentUsb"+getDBName(dbName), map);//SU_RENT_MGT에 반출정보 추가
		    createObject("mg.su.insertUsbPolicy"+getDBName(dbName), map);//SU_POLICY에 정책정보 저장
		    
		    //SU_LOG 기록
		    map.put("MAC_ADDR", "");	//대여 신청이므로 별도의 MAC_ADDR 값은 필요 없다.
		    createObject("mg.su.insertSecureUsbLog"+getDBName(dbName), map);//SU_LOG에 로그 기록 

		    log.info("map : " + map.toString());
		    //RENDER_ID
		    updateObject("mg.su.updateUsbMaster", map);//SU_MASTER에 RENDER_ID,RENT_STS_CD 정보 기록
		  	
		    //link 상태값 업데이트
		    if(request.getStringParam("LINK_ID")!=null){
		    	 map.put("LINK_ID", request.getStringParam("LINK_ID"));
		    	 map.put("IN_STS_CD", "02");
				 updateObject("mg.batch.updateSuLinkStatus", map);
				 log.info("SU_LINK 업데이트 : " + map.get("LINK_ID"));
		    }
		    tx.commit();
		    log.info("보안USB 대여 완료"); 
		}catch(Exception e){
			log.error("ERRO : savePolicy-----------------------------------------------------------"+e.toString());
			tx.rollback();
		}
		
		return write("");
	}
	
	private String getExcProcess(String clcd)
  	{
  		 Map smap = new HashMap();
           smap.put("CP_EXC_CL_CD", clcd);
           String retval = "";
           String div = "";
           
           List lists = getList("mg.cp.selectExcPolicy",smap);
           for (int i = 0; i < lists.size(); i++) {
            	Map map =(Map) lists.get(i);
            	retval += div + this.getStringWithNullCheck(map, "EXC_PROCESS");
            	div = "|";
           }
           if(lists.size()<1) RET_Value = "0";
           else RET_Value ="1";
           return retval;
  	}


	/**
	 * 최종수정일 : 2016. 04. 03 
	 * @param request
	 * @return
	 */
	private IResponse getCreateSecureUsb(IRequest request) {
		// TODO Auto-generated method stub

		log.info("보안USB 생성(반출) 메소드");
		//DB에서 가져온 값 + jsp에서 가져온 값 + 초기값 으로 modify에 필요한 Json을 만든다.
		JSONObject obj = new JSONObject();
		
		try{
			// jsp에서 받은 Json 값 파싱
	    	String policy = request.getStringParam("policy");
	        JSONObject jPolicyData = (JSONObject)JSONValue.parse(policy);
	        
			if("Y".equals(pm.getString("component.usb.message.useYn"))){
				// USB root폴더에 사용자에게 안내메세지를 여러개의 파일 제목으로 보여주기 위한...
				writeFile("", jPolicyData.get("DRIVE").toString());
			}
			log.info("--------------------------------------createSecureUSB------------");
		
			approvalAction appAction = new approvalAction();
			//return write(appAction.getSuPolicy(request, jPolicyData.get("DRIVE").toString(), jPolicyData.get("USB_ID").toString(), jPolicyData.get("SU_POLICY_GRP_CD").toString(), ""));
			return write(appAction.getSuPolicy(request, jPolicyData));

		}catch(Exception e){
	    	log.error("---------------------------------------------------------------\n" + e.toString());
			return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
	    }
		
	}
		
	private void writeFile(String fileContent, String rootPath){
		
		String fileName[] = mm.getMessage("SUMG_1190","").split("/n");
		for(int i=0; i<fileName.length; i++){
			File file = new File(rootPath+":"+File.separator+fileName[i]+".txt");
			try{
				OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
				writer.write(fileContent, 0, fileContent.length());                        
				writer.close();
				log.info("------------------------------writeFile");
			}catch(IOException e){
				log.error("-----------------------------writeFile Exception-----"+e.toString());
				e.printStackTrace();
			}
		}
	}

	private IResponse saveUsbInfo(IRequest request) {
		// TODO Auto-generated method stub
		// 상태값 02로 변경 및 추가데이터 등록
		JSONObject result = new JSONObject();
		try{
			tx.begin();
			
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			smap.put("RENT_STS_CD", "02");
			
			if(smap.get("ADMIN_ID")==null) smap.put("ADMIN_ID", request.getUser().getId());
			if(smap.get("REG_ID")==null) smap.put("REG_ID", request.getUser().getId());
			
		  	updateObject("mg.su.updateSecureUsb", smap); 

		  	log.info("--------------------------------------------update SU_MASTER ");
		  	 tx.commit();
		  	 result.put("errmsg","USB 정보 업데이트를 정상적으로 진행하였습니다.");
		  	 result.put("errcode","0");
		  	 
		}catch(Exception e){
			 log.error("--Failed saveUsbInfo !!-----------------------------------------------------\n" + e.toString());
			 tx.rollback();
		  	 result.put("errmsg","USB 정보 업데이트 중 오류가 발생하였습니다.");
		  	 result.put("errcode","-1");
		}
		
		return write(result.toString());
	}


	/** 보안USB 등록 - 기본정책삽입 : 대여가 아닌 생성임, 대여는 getCreateSecureUsb() */
	private IResponse getEnrollUSB(IRequest request) {
		// TODO Auto-generated method stub
		
		/*Calendar c = Calendar.getInstance();
		long USBID =c.getTimeInMillis();*/
		
		// 현재시간을 기준으로 13자리의 숫자 생성 (1/1000초 단위로 구별)
		//long USBID = System.currentTimeMillis();
		String drive = request.getStringParam("drive");
		log.info("등록 - 기본정책 삽입");
		log.info(drive + "드라이브에 기본정책을 삽입합니다");
		Map map;
		Map smap = request.getMap();
		JSONObject obj = new JSONObject();
		JSONObject result = new JSONObject();
		
		try{
			//라이센스 USB갯수 정보 확인
			map = (Map)getItem("mg.su.selectLicense",smap);//DB에서 라이센스 정보를 얻어온다.
			String license = this.getStringWithNullCheck(map,"OTHERDATA1");//암호화된 수량정보

			String plainLicense = SeedEncryptUtil.seedDecrypt(license, "MGUSB");
			int limitUsb = Integer.parseInt(plainLicense);
			
			log.info(this.getStringNullCheck(pm.getString("component.usb.authgroup"))); 
			log.info(this.getStringNullCheck(pm.getString("component.usb.label")));
			//log.info(this.getStringNullCheck(mm.getMessage("SUMG_1190", request.getUser().getLocale())));
			
			
			log.info("-------------------------------------------등록 전 남은 USB 갯수"+limitUsb);
			//라이센스가 부족하면 로그로 알려줌.
			
			if(limitUsb>0){
				limitUsb = limitUsb -1;
				String modifyCount=String.valueOf(limitUsb);
				String paddingCount="";
				//다시 암호화 하기 위해서 패딩을 주어야 함..
				for(int i= 0; i<(16-modifyCount.length());i++){
					paddingCount = paddingCount.concat("0");
				}
				paddingCount = paddingCount.concat(modifyCount);
				String cipherLicense = SeedEncryptUtil.seedEncrypt(paddingCount, "MGUSB");

				map.clear();
				map.put("OTHERDATA1", cipherLicense);
				updateObject("mg.su.updateLicense", map);
			}else{
				log.error("----------------------------------------라이센스가 부족하여 더이상 등록하실 수 없습니다. ");
				obj.put("RESULT", "false");
				obj.put("ERROR", "라이센스에 등록된 USB 생성 갯수가 부족하여 더이상 등록하실 수 없습니다.");
				return write(obj.toString());
			}
			obj.put("RESULT", "true");

			smap.put("dbName", pm.getString("component.sql.database"));
		
	    	map = (Map)getItem("mg.su.selectUsbCount",smap);
	    	String usbCount =Integer.toString(Integer.parseInt(this.getStringWithNullCheck(map, "CNT"))+1); //USB의 총 갯수를 얻어와 하나를 증가시켜준다.
	    	String usbType=request.getStringParam("usbType");
	    	if(usbType==null){
	    		usbType="";
	    	}
	    	String UsbIdFormat = pm.getString("component.usb.default.format")+"_"+usbType+"000000";
	    	String UsbId = UsbIdFormat.substring(0, UsbIdFormat.length()-usbCount.length())+usbCount;//주어진 형태의 스트링에서 얻어온 카운트 스트링의 길이만큼 잘라내고 카운트 스트링을 붙여준다.
	    	obj.put("USB_ID", UsbId);
	    	
	    	//초기 정책 받아오기  	
	    	JSONObject su_policy = new JSONObject();
	    	
	    	// smap.put("dbName", pm.getString("component.sql.database"));
	    	// smap.put("USB_ID",jData.get("USB_ID"));
	    	// USB_ID, USB_CREATE_ID
	    	result.put("action", "CreateSecureUSB"); //
	    	su_policy.put("CREATETYPE", "0");
	    	Map bmap = (Map)getItem("mg.su.selectProdNo",smap);
	    	su_policy.put("PRODNO", this.getStringWithNullCheck(bmap, "PRODNO"));
			su_policy.put("ENCKEY", this.getStringWithNullCheck(bmap, "PRODNO").substring(0,5));// ->데이터베이스에서  prodno을 갖고와서 앞에 5자리만...
			//내외부 URL 호출
			bmap = (Map)getItem("mg.su.selectServerInfo",smap);
			if(bmap != null) {
				su_policy.put("BASEURL", this.getStringWithNullCheck(bmap, "INNER_SVR_URL"));	//내부서버 URL
				su_policy.put("INNER_SVR_URL", this.getStringWithNullCheck(bmap, "INNER_SVR_URL"));	//내부서버 URL
				su_policy.put("OUTER_SVR_URL", this.getStringWithNullCheck(bmap, "OUTER_SVR_URL"));	//외부서버 URL
				//su_policy.put("IP_RANGE_ADDR", this.getStringWithNullCheck(bmap, "IP_RANGE_ADDR"));	//IP 허용대역
				
				String ipAddrs = this.getStringWithNullCheck(bmap,"IP_RANGE_ADDR");
				ipAddrs = ipAddrs.replace(",", "~");
				su_policy.put("IP_RANGE_ADDR", ipAddrs);	//IP 허용대역
			} else {
				log.info("selectServerInfo Data : Null");
			}
			bmap = (Map)getItem("mg.su.selectCompany",smap);
			su_policy.put("COMPANY", this.getStringWithNullCheck(bmap, "COMPANY")); //
			bmap = (Map)getItem("mg.su.selectTimeout",smap);
			su_policy.put("TIMEOUT", this.getStringWithNullCheck(bmap, "TIMEOUT")); //

			//보안USB 기본정책 불러오기
			smap.put("SU_POLICY_BASIC_CD", pm.getString("component.usb.export.default.policyCd"));
			
			Map policyMap = (Map)getItem("mg.su.selectPolicy_Basic",smap);
			/*String isApproval = this.getStringWithNullCheck(policyMap,"IS_APPROVAL");
			if(!"Y".equals(isApproval)) {
				isApproval = "N";
			} else {
				isApproval = "";
			}*/
			su_policy.put("USB_ID",UsbId);
			su_policy.put("IS_APPROVAL",this.getStringWithNullCheck(policyMap,"IS_APPROVAL"));
			su_policy.put("USB_AUTH_CL_CD",this.getStringWithNullCheck(policyMap,"USB_AUTH_CL_CD"));
			su_policy.put("IS_EXPIRE_DT",this.getStringWithNullCheck(policyMap,"IS_EXPIRE_DT"));
			su_policy.put("IS_EXPIRE_INIT",this.getStringWithNullCheck(policyMap,"IS_EXPIRE_INIT"));
			su_policy.put("IS_EXPIRE_STOP",this.getStringWithNullCheck(policyMap,"IS_EXPIRE_STOP"));
			su_policy.put("IS_READCOUNT",this.getStringWithNullCheck(policyMap,"IS_READCOUNT"));
			su_policy.put("IS_READ_INIT",this.getStringWithNullCheck(policyMap,"IS_READ_INIT"));
			su_policy.put("IS_READ_STOP",this.getStringWithNullCheck(policyMap,"IS_READ_STOP"));
			su_policy.put("IS_WAIT_CLOSE",this.getStringWithNullCheck(policyMap,"IS_WAIT_CLOSE"));
			su_policy.put("WAIT_TIME",this.getStringWithNullCheck(policyMap,"WAIT_TIME"));
			su_policy.put("IS_LOGIN_FAIL_INIT",this.getStringWithNullCheck(policyMap,"IS_LOGIN_FAIL_INIT"));
			su_policy.put("IS_LOGIN_FAIL_STOP",this.getStringWithNullCheck(policyMap,"IS_LOGIN_FAIL_STOP"));
			su_policy.put("IS_LOGIN_FAIL_COUNT",this.getStringWithNullCheck(policyMap,"IS_LOGIN_FAIL_COUNT"));
			su_policy.put("MAC_ADDR",this.getStringWithNullCheck(policyMap,"MAC_ADDR"));
			su_policy.put("USB_ENCODE_CL_CD",this.getStringWithNullCheck(policyMap,"USB_ENCODE_CL_CD"));
			su_policy.put("USB_IMAGE_SIZE",this.getStringWithNullCheck(policyMap,"USB_IMAGE_SIZE"));
			su_policy.put("EXPIRE_YMD",this.getStringWithNullCheck(policyMap,"EXPIRE_YMD"));
			su_policy.put("READCOUNT",this.getStringWithNullCheck(policyMap,"READCOUNT"));
			su_policy.put("USB_CREATE_ID",this.getStringWithNullCheck(policyMap,"USB_CREATE_ID"));
			su_policy.put("SU_POLICY_GRP_CD",this.getStringWithNullCheck(policyMap,"SU_POLICY_GRP_CD"));
			su_policy.put("WORK_DT",this.getStringWithNullCheck(policyMap,"WORK_DT"));
		  	result.put("SU_POLICY",su_policy);
		  	
		  	//정책그룹세트 더미값 입력
		  	JSONArray resultList = new JSONArray();
		  	JSONObject node = new JSONObject();
		  	node.put("SET_ID","");
	  		node.put("SET_NM","");
	  		JSONArray policyList = new JSONArray();
	  		JSONObject subnode = new JSONObject();
	  		subnode.put("NO_SAVE","");
	  		subnode.put("NO_COPY","");
	  		subnode.put("NO_PRINT","");
	  		subnode.put("NO_CLIP","");
	  		subnode.put("NO_USE","");
	  		subnode.put("DEL_YN","");
	  		policyList.add(subnode);
	  		node.put("SET_CODE",policyList);
	  		resultList.add(node);
	  		
		  	result.put("SU_POLICY_SET", resultList);
		  	result.put("DRIVE", drive);
		  	result.put("SERIALNO", "");
		  	result.put("errcode", "0");
		  	result.put("errmsg", "Success.");

    	}catch(Exception e){
    		log.error("---------------------------------------------------------------\n" + e.toString());
    		result.put("errcode", "-1");
		  	result.put("errmsg", mm.getMessage("CPMG_1002", request.getUser().getLocale()));
    	}

		//---------USB ID 중복방지---------
		try{
			tx.begin();
			
			Map param = request.getMap();
			param.put("USB_ID", obj.get("USB_ID"));
			param.put("USB_TYPE_CD", request.getStringParam("usbType"));
			log.info(obj.get("USB_ID") + "임시등록");
		    createObject("mg.su.insertTmpUSBID", param); 
		   
		  	log.info("--------------------------------------------insert temp USBID ");
		  	 tx.commit();
		}catch(Exception e){
			 log.error("--Failed save temp USBID-----------------------------------------------------\n" + e.toString());
			 tx.rollback();
		}		
		//------------------------------------
		
		log.info("--------------------------------getEnrollUSB----------------"+obj.toString());		
		log.info("UsbInfo :: " + result.toString());
		return write(result.toString());
	}

}
