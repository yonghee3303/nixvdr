package mg.inter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.SeedEncryptUtil;

public class hRInterlockAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		if(request.isMode("updateUser")) {
            return updateUser(request);
		 }else if(request.isMode("updateDept")){
			return updateDept(request); 
		 }else if(request.isMode("updateCode")){
			 return updateCode(request);
		 }else if(request.isMode("updateApprover")){
			 return updateApprover(request);
		 }else {
			 return write(null);
		 }
	}
	
	private IResponse updateUser(IRequest request) {
		// TODO Auto-generated method stub
		
		log.info("updateUser-----------------------------------------------------------------------------------------------START");
		String userStr = request.getStringParam("userlist");
		Object userData = JSONValue.parse(userStr);
        JSONObject obj = (JSONObject)userData;
        JSONArray userList = (JSONArray)obj.get("user");
   
        try{
        	for (int i=0; i< userList.size(); i++){
        		JSONObject user = (JSONObject)userList.get(i);
        		//log.info("updateUser----------------------------------------------------------------------------------------"+user.toString());
        		Map item = new HashMap();
        		//Json으로 받은 데이터를  Map형태로 변환
        		item.put("USER_ID", user.get("USER_ID").toString());
        		item.put("USER_NM", user.get("USER_NM").toString());	
        		item.put("EMP_NO", user.get("EMP_NO").toString());	
        		item.put("DEPT_CD", user.get("DEPT_CD").toString());	
        		item.put("POSITION_CD", user.get("POSITION_CD").toString());	
        		item.put("WORK_CD", user.get("WORK_CD").toString());	
        		item.put("EMAIL", user.get("EMAIL").toString());  		
        		item.put("TEL_NO", user.get("TEL_NO").toString());
        		item.put("USER_ENM", user.get("USER_ENM").toString());
        		item.put("LOGIN_ID", "admin");
        		
        		tx.begin();
        		if( Integer.parseInt(sm.getItem("mg.sys.countUser", item).toString() ) > 0) {
        			//log.info("updateUser------------------------------------update---------"+user.get("USER_ID").toString());
        			updateObject("mg.batch.updateUser", item);
        		}else{
        			//log.info("updateUser------------------------------------insert---------"+user.get("USER_ID").toString());
        			createObject("mg.batch.insertUser", item); 
        		}
        		tx.commit();
        		log.info("----------HR_User Infomation Success Update");
        		
        		if("Y".equals(pm.getString("component.hr.myguard.useYn"))){
        			tx.begin();
        			item.put("MGDB", pm.getString("component.hr.myguard.dbName"));
        			//log.info("-------------------------------------------------------------------------------"+item.toString());
        			List lists = getList("mg.batch.getCompanyCode",item);
        			Map map = (Map) lists.get(0);
        			item.put("COMPANYCODE", map.get("COMPANYCODE"));
        			log.info("-------------------------------------------------------------------------------"+item.toString());
        			if(getItem("mg.batch.getUser_MG",item) != null ) {
            			//log.info("updateUser-MG-----------------------------------update---------"+user.get("USER_ID").toString());
            			updateObject("mg.batch.updateUser_MG", item);
            		}else{
            			//log.info("updateUser-MG-----------------------------------insert---------"+user.get("USER_ID").toString());
            			createObject("mg.batch.insertUser_MG", item); 
            		}
        			tx.commit();
        		}
        		
        		log.info("-MG---------HR_User Infomation Success Update");
        	}
        
        	return write("----------HR_User Infomation Success Update");
		}catch(Exception e){
			tx.rollback();
			log.info("updateUser----------------------------------------ERROR--------------------------------------------------"+e.toString());
        	return write("----------HR_User Infomation Fail Update");
		}
	}

	private IResponse updateDept(IRequest request) {
		// TODO Auto-generated method stub
		log.info("updateDept-----------------------------------------------------------------------------------------------START");
		String deptStr = request.getStringParam("deptlist");
		Object deptData = JSONValue.parse(deptStr);
        JSONObject obj = (JSONObject)deptData;
		
        JSONArray deptList = (JSONArray)obj.get("dept");
   
        try{
        	//MG랑 연동 할 때 우선 백업을 해둔다.
        	if("Y".equals(pm.getString("component.hr.myguard.useYn"))){
        		//BACKUP
       	 		//테이블 초기화
        		tx.begin();
       	 		deleteObject("mg.batch.deleteDeptBackup", request.getMap());
       	 		deleteObject("mg.batch.deleteUserBackup", request.getMap());
       	 		//테이블 insert
       	 		createObject("mg.batch.insertDeptBackup", request.getMap());
       	 		createObject("mg.batch.insertUserBackup", request.getMap());	
       	 		tx.commit();
        	}
        	
        	for (int i=0; i< deptList.size(); i++){
        		JSONObject dept = (JSONObject)deptList.get(i);
        		log.info("updateDept----------------------------------------------------------------------------------------"+dept.toString());
        		Map item = new HashMap();
        		//Json으로 받은 데이터를  Map형태로 변환
        		item.put("DEPT_CD", dept.get("DEPT_CD").toString());
        		item.put("DEPT_NM", dept.get("DEPT_NM").toString());
        		item.put("DEPT_ENM", dept.get("DEPT_ENM").toString());
        		if("".equals(dept.get("P_DEPT_CD").toString()) || dept.get("P_DEPT_CD") == null){
        			item.put("P_DEPT_CD", "-1");
        		}else{
        			item.put("P_DEPT_CD", dept.get("P_DEPT_CD").toString());
        		}
        		item.put("DEPT_ORDER", dept.get("DEPT_ORDER").toString());
        		String useYn = "N";
        		if("0".equals(dept.get("USE_YN").toString())){
        			useYn="Y";
        		}
        		item.put("USE_YN", useYn);
        		item.put("LOGIN_ID", "admin");
        		
        		String pDeptNm;
				if(sm.getItem("mg.batch.getPDept",item) != null){
					pDeptNm =sm.getItem("mg.batch.getPDept",item).toString();
					//log.info("-------------------------------------------상위 부서 이름 얻어오기----"+pDeptNm);
				}else{
					pDeptNm = "";
				}
				item.put("P_DEPT_NM", pDeptNm);
        		
        		tx.begin();
        		if(getItem("mg.batch.getDept",item) != null){
		    		//log.info("updateDept------------------------------------update---------"+dept.get("DEPT_CD").toString());
		    		updateObject("mg.batch.updateDept", item);
		    	}else{
		    		//log.info("updateDept------------------------------------insert---------"+dept.get("DEPT_CD").toString());
		    		createObject("mg.batch.insertDept", item);
		    	}
        		tx.commit();
        	
        		//log.info("----------HR_Dept Infomation Success Update");
        		if("Y".equals(pm.getString("component.hr.myguard.useYn"))){
        			tx.begin();
        			//log.info("-------------------------------------------------------------------------------"+item.toString());
        			List lists = getList("mg.batch.getCompanyCode",item);
        			Map map = (Map) lists.get(0);
        			item.put("COMPANYCODE", map.get("COMPANYCODE"));
    	       	 
        			//log.info("변경 및 추가된 부서코드 및 부서명을 추가/수정-------------------------------------");
        			if(getItem("mg.batch.getDept_MG",item) != null ) {
            			//log.info("updateDept-MG-----------------------------------update---------"+dept.get("DEPT_CD").toString());
            			updateObject("mg.batch.updateDept_MG", item);
            		}else{
            			//log.info("updateDept-MG-----------------------------------insert---------"+dept.get("DEPT_CD").toString());
            			createObject("mg.batch.insertDept_MG", item); 
            		}
        			
        		}
        		log.info("-MG---------HR_Dept Infomation Success Update");
        	}
        	if("Y".equals(pm.getString("component.hr.myguard.useYn"))){
        		Map map = new HashMap();
        		deleteObject("mg.batch.deleteTempDept",map); 
        		log.info("updateDept------------------------------------webDB완료 후 tempDept 내용 지우기---------");
        		//자식을 갖는 부서값들을 list로 갖는다. 상위부터 차례대로 넣어야하기 때문에
	       	 	String dbName = pm.getString("component.sql.database");
	       	 	List lists = getList("mg.batch.getParentDept"+getDBName(dbName),map);
	       	 	log.info("updateDept------------------------------------tempDept에 데이터 채우기---------");
	       	 	for (int i = 0; i < lists.size(); i++) {
	       	 		map =(Map) lists.get(i);
	       	 		createObject("mg.batch.insertTempDept", map);
	       	 	}

	       	 	map.clear();
	       	 	//log.info("updateDept-----------------updateTreeDptNull-------------------treecode업데이트 전 ISMANAGER가 'Y', 기존트리코드를 갖는 부서 외에 모든 트리코드 ''로 초기화---------");
	       	 	updateObject("mg.batch.updateTreeDptNull", map);
	       	 	//log.info("updateDept------------------------------------------------------------------------Treecode 업데이트");
	       	 	updateObject("mg.batch.updateTreeDpt", map);
	       	 	//log.info("updateDept-------------------updateTreeUserNull----------------treecode업데이트 전 ISMANAGER가 'Y', 기존트리코드를 갖는 부서 외에 모든 트리코드 ''로 초기화 ''로 초기화---------");
	       	 	updateObject("mg.batch.updateTreeUserNull", map);
	       	 	//log.info("updateDept------------------------------------------------------------------------Treecode 업데이트");
	       	 	updateObject("mg.batch.updateTreeUser", map);
	       	 	
	       	 tx.commit();
        	}
        	return write("----------HR_Dept Infomation Success Update");
		}catch(Exception e){
			tx.rollback();
			log.info("updateDept----------------------------------------ERROR--------------------------------------------------"+e.toString());
        	return write("----------HR_Dept Infomation Fail Update");
		}
	}

	private IResponse updateCode(IRequest request) {
		// TODO Auto-generated method stub
		log.info("updateCode-----------------------------------------------------------------------------------------------START");
		String codeStr = request.getStringParam("codelist");
		Object codeData = JSONValue.parse(codeStr);
        JSONObject obj = (JSONObject)codeData;
        JSONArray codeList = (JSONArray)obj.get("position");
   
        try{
        	for (int i=0; i< codeList.size(); i++){
        		JSONObject code = (JSONObject)codeList.get(i);
        		//log.info("updateCode----------------------------------------------------------------------------------------"+code.toString());
        		Map item = new HashMap();
        		//Json으로 받은 데이터를  Map형태로 변환
        		item.put("P_CD", code.get("P_CD").toString());
        		item.put("CD", code.get("CD").toString());
        		item.put("CD_KEY", code.get("CD_KEY").toString());
        		item.put("CD_NM", code.get("CD_NM").toString());
        		item.put("CD_ORDER", code.get("CD_ORDER").toString());
        		item.put("USE_YN", code.get("USE_YN").toString());
        		item.put("LOGIN_ID", "admin");
        		tx.begin();
        		if(getItem("mg.batch.getCode",item) != null){
		    		//log.info("updateCode------------------------------------update---------"+code.get("CD").toString());
		    		updateObject("mg.batch.updateCode", item);
		    	}else{
		    		//log.info("updateCode------------------------------------insert---------"+code.get("CD").toString());
		    		createObject("mg.batch.insertCode", item);
		    	}
        		tx.commit();
        	}
        
        	return write("----------HR_Code Infomation Success Update");
		}catch(Exception e){
			tx.rollback();
			log.info("updateCode----------------------------------------ERROR--------------------------------------------------"+e.toString());
        	return write("----------HR_Code Infomation Fail Update");
		}
	}

	private IResponse updateApprover(IRequest request) {
		// TODO Auto-generated method stub
			
			String approverStr = request.getStringParam("approverlist");
			Object approverData = JSONValue.parse(approverStr);
	        JSONObject obj = (JSONObject)approverData;
	        JSONArray approverList = (JSONArray)obj.get("approver");
	   
	        try{
	        	for (int i=0; i< approverList.size(); i++){
	        		JSONObject approver = (JSONObject)approverList.get(i);
	        		//log.info("updateApprover----------------------------------------------------------------------------------------"+approver.toString());
	        		Map item = new HashMap();
	        		//Json으로 받은 데이터를  Map형태로 변환
	        		item.put("USER_ID", approver.get("USER_ID").toString());
	        		//log.info("updateApprover----------------------------------------------------------------------------------------"+approver.get("USER_ID").toString());
	        		item.put("APPROVER_ID", approver.get("APPROVER_ID").toString());
	        		//log.info("updateApprover----------------------------------------------------------------------------------------"+approver.get("APPROVER_ID").toString());
	        		item.put("APPROVER_NM", approver.get("APPROVER_NM").toString());
	        		//log.info("updateApprover----------------------------------------------------------------------------------------"+approver.get("APPROVER_NM").toString());
	        		String appMode = approver.get("APPROVER_MOD").toString();    // 추가된 항목 I : 추가 D: 삭제
	        		
	        		log.info("updateApprover--------APPROVER_MOD--------------------------------------------------------------------------------- : " + appMode);
	        		
	        		tx.begin();
	        		if("D".equals(appMode)) {
	        			deleteObject("mg.batch.deleteApprover", item);
	        		} else {
	        			if(getItem("mg.batch.getApproverUser",item) != null){
				    		//log.info("updateApprover------------------------------------update---------"+approver.get("USER_ID").toString());
				    		updateObject("mg.batch.updateApprover", item);
				    	}else{
				    		//log.info("updateApprover------------------------------------insert---------"+approver.get("USER_ID").toString());
				    		createObject("mg.batch.insertApprover", item);
				    	}
	        		}
	        		tx.commit();
	        	}
	        
	        	return write("----------HR_Approver Infomation Success Update");
			}catch(Exception e){
				tx.rollback();
				log.info("updateApprover----------------------------------------ERROR--------------------------------------------------"+e.toString());
	        	return write("----------HR_Approver Infomation Fail Update");
			}
	}
	
}