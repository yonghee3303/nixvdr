package mg.inter;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.core.component.crypto.ICryptoManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.SeedEncryptUtil;
import mg.base.cryptorSHA;

public class mailInterlockAction extends BaseAction  {

	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		if(request.isMode("interMail")) {
            return interfaceMailScreen(request); //메일스크린과 연동하는 서비스 
		 }else{
			 return null;
		 }
	}

	private String urlDecode(Object temp) throws UnsupportedEncodingException {
		String result; 
		
		if(temp==null){
			result="";
		}else{
			result =  java.net.URLDecoder.decode(temp.toString(), "UTF-8");
		}
		
		return result;
	}

	private IResponse interfaceMailScreen(IRequest request){
	//받은 정보에 대한 validation Check를 한다.
	//CP_DOC_SERVICE에 문서번호와 validation 결과 메세지 등 정보를 backup해둔다.
		
        //response 값
		JSONObject retobj = new JSONObject();
		
		//CP_DOC_SERVICE에 insert할 값
		Map service = new HashMap();
		
		//error message
		String errormsg="interfaceMailScreen Success";
		String errorcode="0";
		
		try{
			//request 값
			String paramStr = request.getStringParam("param");
			Object param = JSONValue.parse(paramStr);
	        JSONObject obj = (JSONObject)param;
	        
	        log.info("interfaceMailScreen-----------------------------param-----------------------------------------------------------------------"+paramStr);
	        
			//문서번호 생성
		 	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			String cpDocNo = formatter.format(new Date()) + getRandomString(6);
			ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
			String url = pm.getString("component.site.pp.url")+"/cp/approval.do?mod=down&security=1&PGM_ID=regMgmt&DOC_NO="+java.net.URLEncoder.encode(crypto.encodeMG(cpDocNo),"utf-8") ;
			String senderId =  (String)getItem("mg.cp.getUserIDFromEmail", obj.get("SENDER"));
			
			service.put("DOC_NO", cpDocNo);
			service.put("DOWN_URL", url);
			service.put("SUBJECT", urlDecode(obj.get("SUBJECT")));
			service.put("SENDER_EMAIL", obj.get("SENDER"));
			
			if(senderId == null){
				errormsg = "등록된 사용자가 아닙니다.";
				errorcode="-1";
				retobj.put("ERRCODE", errorcode);
		        retobj.put("ERRMSG", errormsg);
				return write(retobj.toString());
			} 
		
			//파일 관련 validation 체크
			if(obj.get("FILE_LIST")!=null){
				String[] fileList = urlDecode(obj.get("FILE_LIST")).split("\\|");
				String fileNameList="";
				String fileBasePath="";
				service.put("FILE_NUM", fileList.length);
				for(int i=0; i< fileList.length; i++){
					String temp[] = fileList[i].split("/");
					if(i==0){
						fileNameList ="/"+temp[temp.length-2]+"/"+temp[temp.length-1];
						for(int j=0; j <temp.length-2; j++){
							if(j==0) fileBasePath =temp[j]; 
							else fileBasePath +="/"+temp[j];
						}
					}
					else fileNameList +="|/"+temp[temp.length-2]+"/"+temp[temp.length-1];
				}
				service.put("FILE_LIST", fileNameList); //<일련번호>/< 파일명> ex)1/aadfdf.pdf
				service.put("FILE_PATH", pm.getString("component.upload.servicedirectory")+fileBasePath); //베이스 PATH : [폴더명]/<년월>/<일시>/<분>/<메일스크린_세션ID> ex)/201509/0814/59/21441971630_11388/
			}else{
				service.put("FILE_NUM", "");
				service.put("FILE_LIST", "");
				service.put("FILE_PATH", ""); 
				errormsg ="첨부파일이 없습니다.";
				errorcode="-1";
				retobj.put("ERRCODE", errorcode);
		        retobj.put("ERRMSG", errormsg);
				return write(retobj.toString());
			}
			//정책 정보 체크
			if(obj.get("CD") != null){
				service.put("POLICY_NO", obj.get("CD"));
			}else{
				service.put("POLICY_NO", pm.getString("cliptorplus.export.default.policyCd.service"));
			}		
			//수신자가 여러명이기 때문에 |를 구분자로 두어 입력, 15명 이상일 경우 컬럼 사이즈에 의해 오류 발생 
			//수신자 관련 validation 체크
			JSONArray receiverList = (JSONArray)obj.get("RECEIVER_LIST");
			JSONArray resultList = new JSONArray();
			String tmpReceiverEmail="";
			String tmpReceiverPw="";
			
			if(receiverList.size() > 40){
				errormsg ="수신자는 최대 40명까지 입니다.";
				errorcode="-1";
				retobj.put("RECEIVER_LIST", "");
				retobj.put("ERRCODE", errorcode);
		        retobj.put("ERRMSG", errormsg);
				return write(retobj.toString());
			}else{
				Map userMap = new HashMap();
		        userMap.put("dbName", pm.getString("component.sql.database"));
		    	for (int i=0; i< receiverList.size(); i++){
		    		JSONObject tempReceiver = new JSONObject();
		    		JSONObject user = (JSONObject)receiverList.get(i);
		    		String reciverId =user.get("EMAIL").toString();
		    		userMap.put("USER_ID", reciverId);	
		    		userMap.put("EMAIL",reciverId);
		    		
		    		Map tempUser =(Map)getItem("mg.cppp.selectMemberInfo",userMap);
		    		String tempPass="";
			    	if( tempUser!= null){
			    		if(tempUser.get("PIN_NO")==null){
			    			tempPass = getRandomString(4);
			    			tempUser.put("PIN_NO", SeedEncryptUtil.seedEncrypt(tempPass));
			    			updateObject("mg.inter.updatePinNo",tempUser);
			    		}else{
				    		userMap.put("PASSWD", tempUser.get("PIN_NO").toString());
				    		tempPass = SeedEncryptUtil.seedDecrypt(tempUser.get("PIN_NO").toString());
			    		}
			    	}else{
			    		userMap.put("COMPANY_ID",pm.getString("cliptorplus.export.default.companyId"));
			    		userMap.put("USER_NM",pm.getString("cliptorplus.export.default.userNm"));
			    		userMap.put("STAFF_ID", senderId);
			    		tempPass = getRandomString(4);			    		
			    		userMap.put("PIN_NO", SeedEncryptUtil.seedEncrypt(tempPass));
			    		log.info("enrollDocUser-----------------------------------------------------------------신규 수신자 등록 : "+reciverId);
			    		createObject("mg.cppp.insertCompUserStaff", userMap);
			    		
			    	}//if-else
			    	
		    		tempReceiver.put("EMAIL", reciverId);
		    		tempReceiver.put("PASSWD",  tempPass);
		    		resultList.add(tempReceiver);
		    		if(i ==0){
		    			tmpReceiverEmail = reciverId;
		    			tmpReceiverPw = SeedEncryptUtil.seedEncrypt(tempPass);
		    		}else{
		    			tmpReceiverEmail += "|"+reciverId;
		    			tmpReceiverPw += "|"+SeedEncryptUtil.seedEncrypt(tempPass);		    			
		    		}
		    		
		    	}//for
		    	
		    	retobj.put("RECEIVER_LIST", resultList);
		    	service.put("RECEIVER_EMAIL", tmpReceiverEmail); 
				service.put("RECEIVER_PW", tmpReceiverPw);
			}
			
			service.put("REG_ID", senderId);
			if("-1".endsWith(errorcode)) service.put("STS_CD", "03");
			else service.put("STS_CD", "01"); //01: 접수, 02:등록, 03: 오류
			
			//문서 다운로드 URL 생성
			retobj.put("URL", url);
			
			service.put("ERRCODE", errorcode);
			service.put("ERRMSG", errormsg);
			
			createObject("mg.inter.insertCpService", service);
			log.info("interfaceMailScreen------------------------------------------------------------------------------------------------------------"+service.toString());

		}catch(Exception e){
			retobj.put("RECEIVER_LIST", "");
			retobj.put("URL", "");
			service.put("ERRCODE", "-1");
			service.put("ERRMSG", e.toString());
			createObject("mg.inter.insertCpService", service);
			log.info("interfaceMailScreen----------------------------------------------------------------------------------------------------Exception : "+e.toString());
			errorcode="-1";
			errormsg="Exception 발생 : "+e.toString();
		}
		retobj.put("ERRCODE", errorcode);
        retobj.put("ERRMSG", errormsg);
		return write(retobj.toString());
	}

	
}
