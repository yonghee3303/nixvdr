package mg.inter;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

public class EncrytPw {

	public String digest(String input) {

		String encryStr = "";
		try {
			byte[] digestbyte = digest(input, "SHA-512", "UTF-8", true);
			encryStr = new String(digestbyte, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return encryStr;
	}

	public byte[] digest(String message, String algorithm, String charSet, boolean doEncode) {
		byte[] byteMsg = null;
		byte[] digestedMsg = null;
		try {
			byteMsg = message.getBytes(charSet);

			digestedMsg = digest(byteMsg, algorithm, doEncode);
		} catch (UnsupportedEncodingException e) {
		}

		return digestedMsg;
	}

	public byte[] digest(byte[] message, String algorithm, boolean doEncode) {
		byte[] encodedMsg = null;
		byte[] digestedMsg = null;
		try {
			digestedMsg = MessageDigest.getInstance(algorithm).digest(message);
			if (doEncode)
				encodedMsg = new Base64().encode(digestedMsg);
			else
				encodedMsg = digestedMsg;
		} catch (NoSuchAlgorithmException e) {
		}

		return encodedMsg;
	}
}
