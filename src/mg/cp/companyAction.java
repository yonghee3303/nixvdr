package mg.cp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mg.base.BaseAction;
import mg.base.GridData;

import org.json.simple.JSONObject;

import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
* 업무 그룹명 : mg.cp
* 서브 업무명 : companyAction.java
* 작성자 : cyLeem
* 작성일 : 2014. 04. 03 
* 설 명 : companyAction
*/ 
public class companyAction extends BaseAction {
	
	/**
     * 협력업체 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("selectCompany")) {   /** 업체조회 */
            return selectCompany(request);                           
        } else if(request.isMode("excelCompany")) {   /** 업체조회 Excel Download */
            return excelCompany(request);                           
        }  else if(request.isMode("saveCompany")) { /** 업체 정보 저장**/
            return saveCompany(request);                           
        } else if(request.isMode("saveCompanyUseYn")) {  /** 업체 사용여부에 따라 직원 사용여부 일괄 변경**/
            return saveCompanyUseYn(request);                           
        } else if(request.isMode("deleteCompany")) { 
            return deleteCompany(request);                           
        } else if(request.isMode("popupCompany")) { 
            return popupCompany(request);                           
        } else if(request.isMode("selectCompUser")) {   /** 협력업체 직원조회 */
            return selectCompUser(request);                           
        } else if(request.isMode("selectCompUserJson")) {   /** 협력업체 직원조회 Json */
            return selectCompUserJson(request);                           
        } else if(request.isMode("saveCompUser")) { 
            return saveCompUser(request);                           
        } else if(request.isMode("deleteCompUser")) { 
            return deleteCompUser(request);                           
        } else if(request.isMode("popupCompUser")) { 
            return popupCompUser(request);                           
        } else if(request.isMode("selectPersonalUser")) {   /**  개인별 수신자 조회 */
            return selectPersonalUser(request);                           
        } else if(request.isMode("savePersonalUser")) { /**  개인별 수신자 저장 */
            return savePersonalUser(request);                           
        } else if(request.isMode("deletePersonalUser")) {  /**  개인별 수신자 삭제 */
            return deletePersonalUser(request);                           
        } else if(request.isMode("popupPersonalUser")) { /**  개인별 수신자 선택팝업 */
            return popupPersonalUser(request);                           
        } else if(request.isMode("getComboCompanyHtml")){ /**협력사 직원관리에 사이트 콤보 박스 동적 생성 **/
			return getComboCompanyHtml(request);
		} else {
            return write(null);
        }
    } 

	/**
	 * 협력업체 조회 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse selectCompany(IRequest request) throws Exception 
	 {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	            lists = getList("mg.cp.selectCompany",request.getMap());
	       	 } else {
	       		lists = getList("mg.cp.selectCompany_" + dbName, request.getMap());
	       	 }
	       	 
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	 }
	 /**
      * 협력업체 조회  ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelCompany(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
       String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Cooperation Company List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
  	           	       
				 String dbName = pm.getString("component.sql.database");
		       	 List lists = getList("mg.cp.selectCompany"+getDBName(dbName), request.getMap()); 
         
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Cooperation Company List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
     }
         
	 /**
	 * 협력업체 저장
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse saveCompany(IRequest request) throws Exception 
	 {
	        String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	        String id = "";
	        if(request.getUser() == null){
	        	id = "admin";
	        	
	        }
	        else{
				id =request.getUser().getId();
	        }
	        Locale loc = request.getUser() == null ? new Locale("ko") :  request.getUser().getLocale();
	        
	        try {
	        	tx.begin();
		        Map item = null;
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
		         	}
		       	 	item.put("LOGIN_ID",  id);
		      	    
			       	if( Integer.parseInt(getItem("mg.cp.countCompany", item).toString() ) > 0 ) {
			       		updateObject("mg.cp.updateCompany"+getDBName(pm.getString("component.sql.database")), item);
			       	} else {
			       		createObject("mg.cp.insertCompany"+getDBName(pm.getString("component.sql.database")), item); 
			       	}
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------saveCompany---------------------------------\n" + e.toString());
	        	Object[] params = {"회사코드"};
	        	msg = mm.getMessage("COMG_1007", params, loc);
		        flag = "false";
	        }
	        
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	 }
	 /**
		 * 협력업체 사용여부 일괄 저장
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse saveCompanyUseYn(IRequest request) throws Exception 
		 {
		        String ids_info = request.getStringParam("ids");
		        String cols_ids = request.getStringParam("col_ids");
		        String[] cols = cols_ids.split(",");
		        String[] ids = ids_info.split(",");
		        String msg="", flag="";
		        
		        try {
		        	tx.begin();
			        Map item = null;
			        for (int i = 0; i < ids.length; i++) {
			       	 item = new HashMap();
			       	 
			       	 	for(int j=0; j < cols.length; j++ ) {
			       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
				            item.put(cols[j], tmps);     
			         	}
			       	 	item.put("LOGIN_ID",  request.getUser().getId());
			      	    
				       	if( Integer.parseInt(getItem("mg.cp.countCompany", item).toString() ) > 0 ) {
				       		updateObject("mg.cp.updateCompany"+getDBName(pm.getString("component.sql.database")), item);
				       		//협력사 사용여부에 따라, 하위 직원 사용여부 일괄변경 추가
				       		updateObject("mg.cp.updateUseYnAllUser"+getDBName(pm.getString("component.sql.database")), item);
				       	} else {
				       		createObject("mg.cp.insertCompany"+getDBName(pm.getString("component.sql.database")), item); 
				       	}
			        }
			        tx.commit();
			        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
			        flag = "true";
		        } catch (Exception e) {
		        	tx.rollback();
		        	log.error("------------saveCompany---------------------------------\n" + e.toString());
		        	Object[] params = {"회사코드"};
		        	msg = mm.getMessage("COMG_1007", params, request.getUser().getLocale());
			        flag = "false";
		        }
		        
		        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
		        return writeXml(ret);
		 }		 
	 /**
	 * 협력업체 삭제
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse deleteCompany(IRequest request) throws Exception 
	 {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 updateObject("mg.cp.deleteCompany", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	Object[] params = {"회사코드"};
        	msg = mm.getMessage("COMG_1008", params, request.getUser().getLocale());
	        flag = "false";
        }
        
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	 }
	 
	 /**
	 * 협력업체 팝업 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse popupCompany(IRequest request) throws Exception 
	 {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	
        	Map smap = request.getFormattedMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists = getList("mg.cp.popupCompany"+getDBName(pm.getString("component.sql.database")), smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	 }
	 
	 /**
	 * 협력업체 직원조회 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse selectCompUser(IRequest request) throws Exception 
	 {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Locale locale = null;
        String lang = null;

        if(request.getUser() != null) {
        	//userId = s_user.getId();
        	locale = request.getUser().getLocale();
        } else {
        	//userId = request.getParameter("requster");
        	lang = request.getStringParam("j_janguage");	//language
        	
        	if("ko".equals(lang)) {
        		locale = Locale.KOREA;
        	} else if("en".equals(lang)) {
        		locale = Locale.US;
        	} else if("zh".equals(lang)) {
        		locale = Locale.CHINA;
        	} else if("ja".equals(lang)) {
        		locale = Locale.JAPAN;
        	} else {
        		locale = Locale.KOREA;
        	}	
        }
        
        try {
        	Map smap = request.getFormattedMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
        	
	        List lists = getList("mg.cp.selectCompUser"+getDBName(pm.getString("component.sql.database")), smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", locale), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", locale),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	 }
	 
	 /**
		 * 협력업체 직원조회(Json) 
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse selectCompUserJson(IRequest request) throws Exception 
		 {	        
			 Map smap = request.getFormattedMap();
	         smap.put("dbName", pm.getString("component.sql.database"));
	         JSONObject obj = new JSONObject();
	         String col = request.getStringParam("cols");
	         String[] cols = col.split(",");
	        
	        try {
	        	List<JSONObject> jsonLists = new ArrayList<JSONObject>();
		         JSONObject jsonitem = null;

		        List lists = getList("mg.cp.selectCompUser"+getDBName(pm.getString("component.sql.database")), smap);
		        for (int i = 0; i < lists.size(); i++) {
		         	Map map =(Map) lists.get(i);
		         	
		         	jsonitem = new JSONObject();
		         	for(int j=0; j < cols.length; j++ ) {
		         		jsonitem.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
		         	}
		         	jsonLists.add(jsonitem);
		        }   
		         obj.put("list", jsonLists);   
	 	   		 obj.put("errcode", "0");
	 			 obj.put("errmsg", "Success.");
	 			 log.info(obj.toString());
	 	   		 return write(obj.toString()); 
	         } catch(Exception e) {
	        	 log.error("---selectCompUserJson.  Exception Error-------------------------\n" + e.toString());
	     	 	 obj.put("errcode", "-100");
				 obj.put("errmsg", e.getMessage());
	       	 	 return write(obj.toString());
	        }
		 }
	 
	 /**
	 * [개인용] 수신자조회 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse selectPersonalUser(IRequest request) throws Exception 
	 {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
     	Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	Map rmap = request.getMap();
        	/*
        	String requesterId = request.getStringParam("REQUESTER_ID");
													
        	if(requesterId == null || "".equals(requesterId)) {
        		rmap.put("LOGIN_ID", request.getUser().getId());
        	} else {
        		rmap.put("LOGIN_ID", requesterId);
        	}
        	*/
        	rmap.put("LOGIN_ID", request.getUser().getId());
        	rmap.put("dbName", pm.getString("component.sql.database"));
        	
        	log.info("--selectPersonalUser---1-->\n" + rmap.toString());
        	
	        List lists = getList("mg.cp.selectPersonalUser"+getDBName(pm.getString("component.sql.database")),rmap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        
	        log.info("--selectPersonalUser---2-->\n" + gdRes.getGridXmlDatas());

	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	    log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
                
	 }
		 
	 
	 /**
	 * 협력업체 저장
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse saveCompUser(IRequest request) throws Exception 
	 {
	        String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	         
	        try {
	        	tx.begin();
		        Map item = null;
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
		         	}
		       	 	item.put("LOGIN_ID",  request.getUser().getId());
		      	    
			       	if( Integer.parseInt(getItem("mg.cp.countCompUser", item).toString() ) > 0 ) {
			       		updateObject("mg.cp.updateCompUser"+getDBName(pm.getString("component.sql.database")), item);
			       	} else {
			       		createObject("mg.cp.insertCompUser"+getDBName(pm.getString("component.sql.database")), item); 
			       	}
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
	        	Object[] params = {"수신자"};
	        	msg = mm.getMessage("COMG_1007", params, request.getUser().getLocale());
		        flag = "false";
	        }
	        
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	 }
		 
	 
	 /**
	 * 개인별 수신자 저장
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse savePersonalUser(IRequest request) throws Exception 
	 {
	        String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	         
	        try {
	        	tx.begin();
		        Map item = null;
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
		         	}
		       	 	item.put("LOGIN_ID",  request.getUser().getId());
		      	    
			       	if( Integer.parseInt(getItem("mg.cp.countPersonalUser", item).toString() ) > 0 ) {
			       		updateObject("mg.cp.updatePersonalUser", item);
			       	} else {
			       		String dbName = pm.getString("component.sql.database");
				       	 if("mssql".equals(dbName)) {
				       		 createObject("mg.cp.insertPersonalUser"+getDBName(pm.getString("component.sql.database")), item); 
				       	 } else {
				       		 createObject("mg.cp.insertPersonalUser_" + dbName, item); 
				       	 }
			       	}
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
	        	Object[] params = {"수신자"};
	        	msg = mm.getMessage("COMG_1007", params, request.getUser().getLocale());
		        flag = "false";
	        }
	        
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	 }
	 
	 /**
	 * 협력업체 직원 삭제
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse deleteCompUser(IRequest request) throws Exception 
	 {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	deleteObject("mg.cp.deleteCompUser", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	Object[] params = {"수신자"};
        	msg = mm.getMessage("COMG_1007", params, request.getUser().getLocale());
	        flag = "false";
        }
        
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	 }
	 
	 /**
	 * [개인용] 협력업체 직원 삭제
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse deletePersonalUser(IRequest request) throws Exception 
	 {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	item.put("LOGIN_ID", request.getUser().getId());
	       	 
	       	 	if( Integer.parseInt(getItem("mg.cp.deletePersonalUserCheck", item).toString() ) <= 0 ) {
	       	 		deleteObject("mg.cp.deletePersonalUser", item);
	       	 	} else {
		       	 	Object[] params = {this.getStringWithNullCheck(item,"P_USER_ID") };
					msg = mm.getMessage("CPMG_1010",params, request.getUser().getLocale());
	       	 	    tx.rollback();
	       	 	    String ret = responseTranData(msg, "false", "doSave", "doSaveEnd");
	       	 	    return writeXml(ret);
	       	 	}
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	Object[] params = {"수신자"};
        	msg = mm.getMessage("COMG_1008", params, request.getUser().getLocale());
	        flag = "false";
        }
        
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	 }
	 
	 /**
	 * 협력업체 직원 선택 팝업 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse popupCompUser(IRequest request) throws Exception 
	 {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists = getList("mg.cp.popupCompUser"+getDBName(pm.getString("component.sql.database")), smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	 }
	 
	 /**
	 * [개인용] 수신자 선택 팝업 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse popupPersonalUser(IRequest request) throws Exception 
	 {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	
        	Map rmap = request.getMap();
        	rmap.put("LOGIN_ID", request.getUser().getId());
        	rmap.put("dbName", pm.getString("component.sql.database"));
	        List lists = getList("mg.cp.popupPersonalUser"+getDBName(pm.getString("component.sql.database"))
, rmap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	 }
	 
		private IResponse getComboCompanyHtml(IRequest request){
			  try {
	  		    List lists = getList("mg.cp.selectCompany",request.getMap());
	      	    StringBuffer buffer = new StringBuffer();
	              
	              buffer.append("<select name=\"companyCode\" onchange=\"companyChange();\">");
	              buffer.append("<option id='all' value='all'>" +mm.getMessage("GMSG_1041", request.getUser().getLocale()) + "</option>");
	              
	              Map map = null;

	              for (int i = 0; i < lists.size(); i++) {
	              	map =(Map) lists.get(i);
	                  buffer.append("<option id='"+ map.get("COMPANY_ID")+"' value='" + map.get("COMPANY_ID") + "'>" + map.get("COMPANY_NM") + "</option>");
	              }
	          
	              buffer.append("</select>");
	              return writeXml(buffer.toString());
	  	  } catch(Exception ex) {
	  		   log.error( "-------------------------------------------------------------------\n" + ex.getMessage());
	  		   return writeXml("");
	  	  }
		}
}
