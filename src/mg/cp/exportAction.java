package mg.cp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Hashtable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
// import java.nio.file.DirectoryStream;
// import java.file.Files;
// import java.nio.file.Path;
// import java.nio.file.Paths;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import com.core.base.ComponentRegistry;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.component.message.IMessageManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.net.ftp.FTPClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.cryptorSHA;
import mg.cp.approvalAction;
import mg.base.SeedEncryptUtil;

import com.core.component.util.DateUtils;
import com.core.component.compress.ICompressManagement;


/*
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
*/

/**
* 업무 그룹명 : mg.cp
* 서브 업무명 : exportAction.java
* 작성자 : cyLeem
* 작성일 : 2014. 04. 03 
* 설 명 : exportAction
* 최종 수정일 : 2016. 03. 15
* 수정 내용 : 알림 메일 다국어 지원
*/ 

public class exportAction extends BaseAction{
	/**
     * 협력업체 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
    	    	
        if(request.isMode("testId")) {   /** 테스트서비스 */
            return testService(request);                                                 
        } else if(request.isMode("ldap")) {   /** 테스트서비스 */
            return testLdap(request);                                                 
        } else if(request.isMode("fileCopy")) {   /** 테스트서비스 */
            return fileCopy(request);
        }  else if(request.isMode("saveLicense")) {   /** CP License 저장 */
            return saveLicense(request);                                                 
        } else if(request.isMode("getLicense")) {   /** CP License 요청 */
            return getLicense(request);                                                 
        } else if(request.isMode("getCPConfData")) {   /** CPConfData 요청 */
            return getCPConfData(request);                                                 
        } else if(request.isMode("getCPFileList")) {   /** CPFileList 요청 */
            return getCPFileList(request);                                                 
        } else if(request.isMode("setStateCP")) {   /** CP상태 저장 */
            return setStateCP(request);                                                 
        } else if(request.isMode("setProgressCP")) {   /** CP 진행 상태 저장 */
            return setProgressCP(request);                                                 
        } else if(request.isMode("getScheduleJob")) {   /** Schedule Job 요청 */
            return getScheduleJob(request);                                                 
        }else if(request.isMode("getScheduleJob1")) {   /** Schedule Job 요청 */
            return getScheduleJob1(request);                                                 
        }else if(request.isMode("getScheduleJob2")) {   /** Schedule Job 요청 */
            return getScheduleJob2(request);                                                 
        } else if(request.isMode("getRoot")) {   /** 반출문서 목록 조회 -- Root 조회 */
            return getRoot(request);                                                 
        }else if(request.isMode("getRepackingJob")) {   /** Repacking Job 요청 */
            return getRepackingJob(request);                                                 
        }else if(request.isMode("setStateCPRep")) {   /** Repacking 해제 상태 저장 */
        	return setStateCPRep(request);   
        }else {
        	 JSONObject retobj = new JSONObject();
        	 retobj.put("errcode", "-1");
		     retobj.put("errmsg", "not found mod value.");
             return write(retobj.toString());
        }
    } 
    
    private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
    private ICompressManagement compressHandle = (ICompressManagement) ComponentRegistry.lookup(ICompressManagement.class, "com.core.component.ICompressManagement");
        
    /**
     * Repacking 해제 상태 저장
     * @param request
     * @return
     * 최종 수정일 : 2016. 03. 15
     * 수정 내용 : 다국어 지원을 위한 locale 값 추가 (default : en)
     */
    private IResponse setStateCPRep(IRequest request) {
    	// repacking 해제 상태 저장
    	// 스케쥴번호와 상태값을 받아, 해당 스케쥴 상태값 변경
    	log.info("setStateCPRep 수신 데이터 : " + request.getMap().toString());
    	
    	JSONObject retobj = new JSONObject();
    	Locale locale = new Locale("en");
    	String pLocale = request.getStringParam("j_language");
    	if(pLocale != null && !"".equals(pLocale)) {
    		locale = new Locale(pLocale);
    	}
  		 
    	try{    	
	    	String sts_cd = request.getStringParam("cp_sts_cd");
	    	
	    	Map param = request.getMap();
	    	
	    	tx.begin();
	    	if("03".equals(sts_cd)){
	    		// 스케쥴 03으로 변경
	    		createObject("mg.cp.setRepackingSchedule", request.getMap()); 
	    	}else if("04".equals(sts_cd)){
	    		log.info("스케쥴 04로 변경");
	    		// TFILE_UPLOAD에 압축해제 파일 정보 저장
	    		// 문서번호, 파일경로, 파일명, 크기, 등록, 업로드한 수신자 ID
	    		createObject("mg.cp.setRepackingSchedule", request.getMap());
	    	
	    		Map smap = request.getMap();
	    		smap.put("dbName", pm.getString("component.sql.database"));
	    		log.info("smap :: " +smap.toString());
	    		List lists = getList("mg.cp.getRepDocNo"+getDBName(pm.getString("component.sql.database")),smap);
	    		log.info("lists.size() :: "+lists.size()); 
	    		Map map =(Map) lists.get(0);
	    		log.info("가져온 map객체 내용 : " + map.toString());
	    		
	    		String[] exe_file = request.getStringParam("exe_file").split("\\|");
	    		
	    		log.info("파일개수 : " + exe_file.length);
	    		
	    		/*
	    		#j_file_program#,
                #j_file_docId#,
                #j_file_type:VARCHAR:NO_ENTRY#,
                #j_file_path#,
                #j_file_name#,
                #j_file_size#,
                #j_user.id#,
                GETDATE()
	            */
	    		
	    		for (int i = 0; i < exe_file.length; i++) {
		    		param.put("j_file_program", "regMgmt");
		    		param.put("j_file_docId", map.get("DOC_NO"));
		    		param.put("j_file_path", "/" + map.get("REG_DT") + "/mg/Upload.do/" + map.get("DOC_NO"));
		    		param.put("j_file_name", exe_file[i]);
		    		param.put("j_file_size", "");
		    		param.put("j_user_id", map.get("REG_ID"));
		    		//log.info("넣을 Map 객체 내용 : " + param.toString());
		    		createObject("app.upload.createUpload", param);
		    		log.info(exe_file[i] + " 정상 생성");
	    		}
	    		
	    		// 리패킹 해제 완료시 신청자에게 메일 전송
	    		// SCHEDULE의 DOC_NO -> MASTER USER_ID -> SYSUSER E-mail
	    		
	    		//System.out.println("리패킹해제 완료 파라미터 : " + request.getMap().toString());
	    		// sche_no
	    		
	    		if("Y".equals(pm.getString("cliptorplus.repacking.mail"))){
	    			try{
	    				log.info("setStateCPRep-------------------1----------------\n" + param.toString());
		    			//log.info("리패킹해제 완료 메일 전송");
	    				List mailAddr = getList("mg.cp.getRepDecryptEmail", param);
		    			Map smtp = (Map)mailAddr.get(0);
		    			// Map baseMap = (Map)getItem("mg.su.selectBaseUrl",smtp);
		    			log.info("setStateCPRep-------------------2---------------\n" + smtp.toString());
			    			
	    				IMail mail = mailHandle.createHtmlMail();
		       			mail.addTo(this.getStringWithNullCheck(smtp, "TO_EMAIL"));
		       			//mail.setFrom(this.getStringWithNullCheck(smtp, "FROM_EMAIL"));
		       			//시스템 관리자 메일 주소로 변경
		       			String[] managerMail = pm.getString("cliptorplus.mail.manager.mailaddress").split("\\|");
		       			mail.setFrom(managerMail[0]); 
		       			
		       		    mail.setSubject("["+mm.getMessage("MAIL_0004", locale)+"]"+" "+mm.getMessage("MAIL_0031", locale)); //협업문서가 접수되었습니다.
		       			//log.info("setStateCPRep-------------------3----------------\n");
		       		    // 메일 템플릿을 읽어 온다.
		    	 		String fileName;
		    	 		
		    	 		if("ILJIN".equals(pm.getString("component.site.company")) || "SB".equals(pm.getString("component.site.company"))){
		    	 			log.info("------------NoBtn_mail_Template------------");
		    	 			fileName = "config/templates/mail_Template_NoBtn.html";
		    	 		} else {
		    	 			log.info("------------default_mail_Template------------");
		    	 			fileName = "config/templates/mail_Template.html";
		    	 		}
		    	 		 
		    	 		 String mailContext = readFile(fileName);
		    	 		 
		    	 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", locale)+"]");
		    			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0031", locale));
		    			 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.ep.url"));
		    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));
		    	 		 
		    			 //log.info("setStateCPRep-------------------4--------------\n");
		    			 Object[] params2 = {(String)smtp.get("COMPANY_NM"), (String)smtp.get("FROM_NAME")};
		    		     String tmp = mm.getMessage("MAIL_0032", params2, locale);
		    			 //log.info("setStateCPRep-------------------5--------------\n");
		    			 mailContext = mailContext.replace("MAIL_CONTENT",  tmp);
		    	         
		    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", locale));
						 mailContext = mailContext.replace("COMP_NM", (String)smtp.get("COMPANY_NM"));
						 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1008", locale));
						// log.info("setStateCPRep-----------------------------------------------------------------------------------6");
						 mailContext = mailContext.replace("USER_NM", (String)smtp.get("FROM_NAME"));
						 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1009", locale));
						 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				   		 String curDt = formatter.format(new Date());
						 mailContext = mailContext.replace("REQ_DT", curDt);
						 mailContext = mailContext.replace("LINK", pm.getString("component.site.url"));//
						 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006", locale));
						 
		    			 // 메일 발송
		    			 mail.setHtmlMessage(mailContext);
		    	         mail.send();
	    				
		    		}catch(Exception e){
		    			log.error("Send Mail-----------------------------------------------------------"+e.toString());
		    			//return write("Send Mail Fail");
		    		}
	    		}
	    	}else{
	    		createObject("mg.cp.setRepackingSchedule", request.getMap()); 
	    	}
	    	tx.commit();
	    
	    	retobj.put("errcode", "0");
	        retobj.put("errmsg", "Successful Set Repacking Status.");
	    	//log.info("setStateCPRep 리턴값 : " + retobj.toString());
    	}catch(Exception e){
    		log.error("------------setStateCPRep------------------------\n" + e.toString());
    		retobj.put("errcode", "-1");
	        retobj.put("errmsg", "Failed Set Repacking Status.");
	    	//log.info("setStateCPRep 리턴값 : " + retobj.toString());
    	}
    	
		return write(retobj.toString());
	}


	private IResponse getRepackingJob(IRequest request) {
		// TODO Auto-generated method stub
    	JSONObject retobj = new JSONObject();
        try {
        	//요청한 스케쥴 번호에 대한 정보를 전달
        	// repacking 파일 위치,파일명 / 해제할 위치
        	String rowcnt = request.getStringParam("rowcnt");
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        
        	List lists;
	        lists = getList("mg.cp.getRepackingJob"+getDBName(pm.getString("component.sql.database")),smap);

	        	        
	         List<JSONObject> jsonCategories = new ArrayList<JSONObject>();
	         JSONObject jsonCategory = null;
	         
 	         for (int i = 0; i < lists.size(); i++) {
 	          	Map map =(Map) lists.get(i);
 	          	
 	          	String ext_path = "/" + this.getStringWithNullCheck(map,"REG_DT") + "/mg/Upload.do/" + this.getStringWithNullCheck(map,"DOC_NO");
 	          	log.info("EXEPATH : " + this.getStringWithNullCheck(map,"EXEPATH"));
 	          	log.info("SOURCEPATH : " + this.getStringWithNullCheck(map,"SOURCEPATH"));
 	          	log.info("file_path : " + this.getStringWithNullCheck(map,"FILE_PATH"));
 	          	log.info("file_list : " + this.getStringWithNullCheck(map,"FILE_NAME"));
 	          	log.info("ext_path : " + ext_path);
 	          	
 	          	if("FTP".equals(this.getStringWithNullCheck(map,"TRANSTYPE"))){
 	          		//FTP 모드일 경우에는 폴더생성
 	          		log.info("리패킹해제타입 : FTP");
 	          		String ip = pm.getString("cliptorplus.ftp.host");
 	          		int port = Integer.parseInt(pm.getString("cliptorplus.ftp.port"));
 	          		String username = pm.getString("cliptorplus.ftp.username");
 	          		String password = pm.getString("cliptorplus.ftp.password");
 	          		
 	          		log.info(ip + " " + port + " " + username + " " + password);
 	          		
 	          		FTPClient ftp = new FTPClient();
 	          		ftp.setControlEncoding("utf-8");
 	          		
 	          		ftp.connect(ip, port);
 	          		ftp.login(username, password);
 	          		
 	          		int reply = ftp.getReplyCode();
 	          		log.info("FTP reply code : " + reply);
 	          		
 	          		String[] sourcePath = this.getStringWithNullCheck(map,"SOURCEPATH").split("/");
 	          		
 	          		for(int j=1;j<sourcePath.length;j++){
 	          			// copypath 경로로 이동
 	 	          		ftp.changeWorkingDirectory(sourcePath[j]);
 	          		}
 	          			
 	          		//copypath 이하의 경로 생성
 	          		ftp.makeDirectory(this.getStringWithNullCheck(map,"REG_DT"));
 	          		ftp.changeWorkingDirectory(this.getStringWithNullCheck(map,"REG_DT"));
 	          		ftp.makeDirectory("mg");
 	          		ftp.changeWorkingDirectory("mg");
 	          		ftp.makeDirectory("Upload.do");
 	          		ftp.changeWorkingDirectory("Upload.do");
 	          		ftp.makeDirectory(this.getStringWithNullCheck(map,"DOC_NO"));
 	          		
 	          		ftp.disconnect();
 	          	}else{
 	          		log.info("리패킹해제타입 : LOCAL");
 	          	}
 	          	
 	          	jsonCategory = new JSONObject();
 	          	jsonCategory.put("doc_no",  this.getStringWithNullCheck(map,"DOC_NO"));
 	          	jsonCategory.put("file_path",  this.getStringWithNullCheck(map,"FILE_PATH")); // exe 파일 경로
 	          	jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_NAME")); // exe 파일명
 	          	jsonCategory.put("ext_path", ext_path); // 해제할 경로
 	          	
        	    jsonCategories.add(jsonCategory);
        	    
        	    if(rowcnt != null) 
    	    	{
    	    		if(Integer.parseInt(rowcnt) == (i+1) ) break;
    	    	}
 	         }
 	        retobj.put("list", jsonCategories);   
	        retobj.put("errcode", "0");
	        retobj.put("errmsg", "Success get Repacking Job.");
        } catch (Exception e) {
        	log.error("------------getRepackingJob----------------------------------------------------------------\n" + e.toString());
        	retobj.put("errcode", "-1");
	        retobj.put("errmsg", "Failed get Repacking Job.");
        }
        return write(retobj.toString());
	}


	private String valueToStringOrEmpty(Map<String, ?> map, String key) {
        Object value = map.get(key);
        return value == null ? "" : value.toString();
    }
    
	
	private String getExcProcess(String clcd, String cnt)
  	{
  		 Map smap = new HashMap();
           smap.put("CP_EXC_CL_CD", clcd);
           String retval = "";
           String div = "";
           
           List lists = getList("mg.cp.selectExcPolicy",smap);
           for (int i = 0; i < lists.size(); i++) {
            	Map map =(Map) lists.get(i);
            	retval += div + this.getStringWithNullCheck(map, "EXC_PROCESS");
            	div = "|";
           }
           if(lists.size()<1) cnt = "0";
           else cnt ="1";
           return retval;
  	}
    /**  
	 * 테스트 서비스 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	 private IResponse testService(IRequest request) throws Exception 
	 {
		 try {
			 
			 
			    String filePath = "C:/myguardiconOverlay.log"; // 대상 파일
			    FileInputStream fileInputStream=null;
		        
		        File file = new File(filePath);
		        
		        byte[] bFile = new byte[(int) file.length()];
		        
		        try {
		            //convert file into array of bytes
				    fileInputStream = new FileInputStream(file);
				    fileInputStream.read(bFile);
				    fileInputStream.close();

		        }catch(Exception e){
		        	e.printStackTrace();
		        }
			 
			 
				 Map smap = request.getMap();
				 smap.put("id", "A1");
				 smap.put("nm", "TEST DATA");
				 smap.put("data",bFile);
				 createObject("mg.cp.blobTest", smap); 
			 
			 
			 
			 /*
			 List lists = getList("mg.common.codeList",smap);
	         for (int i = 0; i < lists.size(); i++) {
		          	Map map =(Map) lists.get(i);
		          	log.info("-testService-----\n" + this.getStringWithNullCheck(map, "CD_KEY"));
		     }
	         
			 Collections.shuffle(lists);
			 
	         for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	log.info("-testService-----\n" + this.getStringWithNullCheck(map, "CD_KEY"));
	         }
			 */
			 
			 		 
			 /*
			 try {
				 Runtime rt = Runtime.getRuntime();
				 String comm = "C:\\test\\myDecrRun.exe DC1EN C:\\TEST\\123.txt C:\\TEST\\123_change.txt 1234567890ABCDEFGHIJ 7FD58B40-37B5-4087-A66C-25DA8FE1A870";
				 Process proc = rt.exec(comm);
				 log.info("------\n" + proc.toString());
			 }catch(Exception e) {
				 log.error("---Exception Error--------------------------------\n" + e.getMessage());
			 }
			 
			Calendar calendar = new GregorianCalendar(Locale.KOREA);
 			calendar.setTime(new Date());
 			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
 			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)+Integer.parseInt("1"));
 			log.error("---makeAuthPolicy-calendar------1----------------------------------------------\n" + formatter.format(calendar.getTime()));
 			
 			
 			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)+Integer.parseInt("0"));
 			log.error("---makeAuthPolicy-calendar---0-------------------------------------------------\n" + formatter.format(calendar.getTime()));
 						
 			
 			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)+Integer.parseInt("3"));
 			log.error("---makeAuthPolicy-calendar---3-------------------------------------------------\n" + formatter.format(calendar.getTime()));
 			
 			*/
			// 접근 만료 설정
/*
				String tmpDaTE = "2015-11-21";
				log.error("---makeAuthPolicy-tmpDaTE----------------------------------------------------\n" + tmpDaTE);
				
				String expireDt = tmpDaTE.replaceAll("-", "");
				DateFormat formatter;
				// if(expireDt.contains("-") ) {
				//	formatter = new SimpleDateFormat("yyyy-MM-dd");
				//} else {
				formatter = new SimpleDateFormat("yyyyMMdd");
				// }
				
				log.error("---makeAuthPolicy-0-1-----------------------------------------------------\n" + expireDt);

				try {
					String CompDt = DateUtils.addDay(expireDt, +1);
    				Date expDt = formatter.parse(CompDt);
    				log.error("---makeAuthPolicy-CompDt-----------------------------------------------------\n" + CompDt);
    				
					if(expDt.compareTo(new Date()) < 0 ) {				    				
	        			log.error("---makeAuthPolicy--3---------------------------------------------------------\n" + expDt.toString());
					}
					else log.error("---makeAuthPolicy--4---------------------------------------------------------\n" + expDt.toString());
				}catch(Exception e) {
        			log.error("---makeAuthPolicy-------4----------------------------------------------------\n" + e.getMessage());
				}

			 */
			 
			 
			 /*
			 IMail mail = mailHandle.createHtmlMail();
			 mail.setSubject("메일 발송 테스트 본문");
	         mail.setFrom(pm.getString("test.from.email"));

	         log.info("--메일발송 테슽 FROM ---------------->\n" + pm.getString("test.from.email"));
	         
		     mail.addTo(pm.getString("test.to.email"));
		     
		     log.info("--메일발송 테슽 To ---------------->\n" + pm.getString("test.to.email"));
	         	         
	         mail.setTextMessage("메일 발송 테스트 본문 입니다.");
	         mail.send();
			  */
			 
			 /*
			 String encodeData = SeedEncryptUtil.seedEncrypt(request.getStringParam("DES_FILE").toString());
			 
			 log.info("--encodeData---------------->\n" + encodeData);
			 
			 String decodeData = SeedEncryptUtil.seedDecrypt(encodeData);
			 
			 log.info("--decodeData---------------->\n" + decodeData);
			 */
			 /*
			 compressHandle.createCompressFile("zip", "C:/MyFile.zip");
			  compressHandle.addCompressFile("c:/CP_0205.xls");
			  compressHandle.addCompressFile("c:/SYSMENU.XLS");
			 compressHandle.closeCompress();
			 */
			 
			 /*
			 compressHandle.unzip("C:/TEST/UCM Setup.zip", "C:/TEST2/UCM Setup");
			 compressHandle.zip("C:/TEST2/UCM Setup", "C:/TEST2/UCM Setup.zip");
			
			 compressHandle.unzip("C:/TEST/UCM Setup.zip", "C:/TEST2/UCM Setup");			 
			 compressHandle.zip("C:/TEST2/UCM Setup", "C:/TEST2/UCM Setup.zip");
			  */
			   // app.getDocNo("ddd");
			   // approvalAction  app = new approvalAction();
			   // log.info("---------------testService---------------------------------------------------------retval---"+app.getDocNo(request));
				
	    		// String retval = cryptorSHA.JinEncodeSHA256((String)request.getMap().get("PASSWD"));
	    		
	    		// log.info("-------------------------------------------------------------------------retval---"+retval);
	    		
	    		//long retval2 = cryptorSHA.MgEncodeFile( (String)request.getMap().get("SRC_FILE"), (String)request.getMap().get("DES_FILE"), (String)request.getMap().get("PASSWD"));
	    		
	    		//log.info("-------------------------------------------------------------------------Result :---"+ retval2);
	    		
			 /*
			 String en = request.getStringParam("uid");
			 DateFormat formatter = new SimpleDateFormat("MMdd");
			 String mday = formatter.format(new Date());
			 log.error("---------------------------------------------------------------\n" + mday);
			 
			 int cday = Integer.parseInt(mday);
			 int intVal= Integer.parseInt(en,16);
			 
			 int uNum = intVal / cday;
			 String userId = String.valueOf(uNum);
			 */
			 /*
			 String mKey = request.getStringParam("mKey");
			 String j_lang = request.getStringParam("j_lang");
			 
			 request.getUser().getLocale().getLanguage();
			 
			 log.info("----------------------request.getUser().getLocale().getLanguage()--------------------------------------\n" +  request.getUser().getLocale().getLanguage());
			 log.info("----------------------request.getUser().getLocale().getLanguage()--------------------------------------\n" +  request.getUser().getLocale().toString());
			 
			 log.info("----------------------Message--------------------------------------\n" +  mm.getMessage(mKey,j_lang));
			 
			 String tmpMsg = mm.getMessage(mKey, j_lang);
			 */
			  /* JSONObject obj = new JSONObject();
	    	   String msg="";
			// 인증정보 수신
	    	    Map param = new  HashMap();
	    	    param.put("DOC_NO", "DST-20140612-00002");
	    	    param.put("USER_ID", "SU0001aaa");
	    	    	    	    
	    	    try {
					Map map  = (Map)getItem("mg.cp.getUserPolicy", param);
					
					if(map.isEmpty()) {
						log.info( "<" + "---makeAuthPolicy----0-------------------------------------------------------\n Null 이구나...." + ">");
					} else {
						log.info("---makeAuthPolicy----0-------------------------------------------------------\n" + map.toString());
					}
	    	    } catch(Exception e) {
	    	    	msg = mm.getMessage("CPMG_1038", "ko");   // CPMG_1038=수신자 등록이 되어 있지 않은 ID 입니다.
					obj.put("errcode", "-1");
	    			obj.put("errmsg", msg);
	    			log.error("---makeAuthPolicy----0-------------------------------------------------------\n" + msg);
	    			return write(obj.toString());
	    	    }*/
				
				/*
				if(map.isEmpty()) {  // 수신자 등록이 않 되어 있다면
					msg = mm.getMessage("CPMG_1038", "ko");   // CPMG_1038=수신자 등록이 되어 있지 않은 ID 입니다.
					obj.put("errcode", "-1");
	    			obj.put("errmsg", msg);
	    			log.error("---makeAuthPolicy----0-------------------------------------------------------\n" + msg);
	    			return write(obj.toString());
				}
			 */
				/*
				List lists = getList("mg.cp.getAgentList"+getDBName(pm.getString("component.sql.database"))
,request.getMap());
				
				String ip[] = new String[lists.size()];      // 서버당 접속 IP Address
				int port[] = new int[lists.size()];			  // 서버당 접속 Port
				int process_cnt[] = new int[lists.size()];  // 서버당 최대 실행 Agent 수
				int usage_cnt[] = new int[lists.size()];    // 서버당 가용 Agent 수
				int cpu[] =  new int[lists.size()];  		  // 서버당 CPU 점유율
				int server_cnt=0;    							  // 서버 개수
				int exe_process = 0;
				
		 	    for (int i = 0; i < lists.size(); i++) {
	 	          	Map map =(Map) lists.get(i);
	 	          	
	 	          	ip[i] =  this.getStringWithNullCheck(map, "IP_ADDR");
	 	          	port[i] = Integer.parseInt(map.get("PORT").toString());
	 	          	process_cnt[i] = Integer.parseInt(map.get("PROCESS_CNT").toString());
					String server_msg = "";
					 
		            // 소켓을 생성하여 연결을 요청한다.
					log.info("서버에 연결중입니다. 서버IP : " + ip[i] + ", Port : " + port[i]);
		            Socket socket = new Socket(ip[i], port[i]);
		            
		            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		 			// 서버에 데이터를 송신
		 			OutputStream out = socket.getOutputStream();
		 			// 서버에 데이터 송신
		 			out.write("GETDATA".getBytes());
		 			out.flush();
		 					   
		 			// 소켓의 입력스트림을 얻는다.
		 			server_msg = in.readLine().replaceAll("\\#!=", "^");
		 			// GETDATA000#!=1#!=
		            log.info("서버로부터 받은 메세지 : " + server_msg);
		            
		            String[] s_msgs = server_msg.replace("GETDATA", "").split("\\^");

		            cpu[i] = Integer.parseInt(s_msgs[0]);
		            usage_cnt[i] = Integer.parseInt(s_msgs[1]);
		            
		            log.info(" usage_cnt[i] : " +  usage_cnt[i]);
		            
		            server_cnt = i;     
		            
		            in.close();
					out.close();
		            socket.close();
		            		         
		            // 실행 가능한 서버 Agnet Job 도출
		            if(process_cnt[i] - usage_cnt[i] > 0) {
		            	exe_process = process_cnt[i] - usage_cnt[i];
		            }
		            
		            // 서버 상태 로그 기록			        
			        setServerAgentLog(map, usage_cnt[i], cpu[i], "01", "");
		 	    }
		 	    log.info("ROWNUM : " + exe_process);
		 	    		 	    
		 	    // 실행명령 --> EXEsche_no
		 	    if(exe_process > 0) {  // 실행 가능한 서버 Agent가 존재 하면
		 	    
		 	    	Map inmap = new HashMap();
		 	    	inmap.put("ROWNUM", exe_process);
		 	    	
		 	    	// 실행할 스케쥴을 가져 온다.
		 	    	List exeList = getList("mg.cp.getExeSchedule", inmap);
		 	    	for (int j = 0; j < exeList.size(); j++) {
		 	    		if(exe_process >= j) {
			 	          	Map schemap =(Map) exeList.get(j);
			 	          	
			 	          	log.info("getExeSchedule : " +this.getStringWithNullCheck(schemap,"SCHE_NO"));
			 	          	// 실행 서버를 찾아 온다
			 	          	int exe_index=0;
			 	          	if(execCpAgent(ip[exe_index], port[exe_index], "EXEC" + this.getStringWithNullCheck(schemap,"SCHE_NO") ) ) {
			 	          		// 실행 성공이면 스케쥴 상태 변경
			 	          		Map smap = new HashMap();
			 	          		smap.put("sche_no",this.getStringWithNullCheck(schemap,"SCHE_NO"));
			 	          		smap.put("cp_sts_cd", "03");
			 	          		smap.put("job_desc", "Load balancing servers running.");
			 	          		createObject("mg.cp.setStateSchedule", smap); 
			 	          	}
		 	    		}
		 	    	}
		 	    }
		 	    */	 	    
			 
		 	    /*
				String serverIp = "192.168.1.9";
				String server_msg = "";
				 
	            // 소켓을 생성하여 연결을 요청한다.
				log.info("서버에 연결중입니다. 서버IP : " + serverIp + ", Port : 23651");
	            Socket socket = new Socket(serverIp, 23651);
	            
	            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	 			// 서버에 데이터를 송신
	 			OutputStream out = socket.getOutputStream();
	 			// 서버에 데이터 송신
	 			out.write("GETDATA".getBytes());
	 			out.flush();
	 					   
	 			// 소켓의 입력스트림을 얻는다.
	 			server_msg = in.readLine();
	 			// GETDATA000#!=1#!=
	 			
	             
	            // 소켓으로부터 받은 데이터를 출력한다.
	            log.info("서버로부터 받은 메세지 : " + server_msg);
	            
	            in.close();
				out.close();
	            socket.close();
	            log.info("연결이 종료되었습니다.");
				 */
	            return write("return : CP 실행 작업 성공.. ");
	      } catch(Exception e) {
		    	 log.error("---------------------------------------------------------------\n" + e.toString());
		    	 return write( e.toString());
	      }
	 }
	 
	 private boolean execCpAgent(String ip, int port, String sche_no )
	 {
		 String server_msg="";
		 try {
			    log.info("서버에 연결중입니다. 서버IP : " + ip + ", Port : " + port);
	            Socket socket = new Socket(ip, port);
	            
	            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	 			// 서버에 데이터를 송신
	 			OutputStream out = socket.getOutputStream();
	 			// 서버에 데이터 송신
	 			out.write(sche_no.getBytes());
	 			out.flush();
	 					   
	 			// 소켓의 입력스트림을 얻는다.
	 			server_msg = in.readLine();
	 			// GETDATA000#!=1#!=
	            log.info("서버로부터 받은 메세지 : " + server_msg);
	            
	            in.close();
				out.close();
	            socket.close();
	            
	            if("OK".equals(server_msg.substring(0, 2))) {
	            	return true;
	            } else {
	            	 log.error("-----execCpAgent : returned Server Message. ----------------------------------------------------------\n" );
	            	return false;
	            }
		 } catch(Exception ex) {
			   log.error("-----execCpAgent :----------------------------------------------------------\n" + ex.toString());
	    	   return false;
		 }
	 }
	 
	 private int getExeAgent(String[] cpu, String[] process_cnt, String[] usage_cnt)
	 {
		 		 
		 return 1;
	 }

	 
	 // 서버 상태 로그 기록
 	 // (#AGENT_NO#, TO_CHAR(SYSDATE, 'YYYYMMDD'), TO_CHAR(SYSDATE, 'HH24MISS'), #PROCESS#, #CPU#, #AGENT_STS_CD#, #AGENT_MSG#)
	 private boolean setServerAgentLog(Map map, int process, int cpu, String status_cd, String msg)
	 {
		 try {
			    map.put("PROCESS", process);
	            map.put("CPU", cpu);
	            map.put("AGENT_STS_CD", status_cd);
	            map.put("AGENT_MSG", msg);
	            
	            String dbName = pm.getString("component.sql.database");
	            
	            tx.begin();
	            
	            createObject("mg.cp.saveServerAgent"+getDBName(dbName), map); 

		        tx.commit();
		        
		        return true;
		 } catch(Exception ex) {
			 	log.error("------------setServerAgentLog------------\n" + ex.getMessage());
			 	return false;
		 }
	 }
	 
	 /**
		 * 테스트 서비스 
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse testLdap(IRequest request) throws Exception 
		 {
			 try {
				 return write("");
		      } catch(Exception e) {
		    	 log.error("---------------------------------------------------------------\n" + e.toString());
		    	 return write("LDAP 인증 실패");
		      }
		 }
		 
		 /**
		 * 테스트 서비스 
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse fileCopy(IRequest request) throws Exception 
		 {
			 try {
				 
				 /*
				 Path copyFrom = Paths.get("C:/apache-tomcat-5.5.26/webapps/", "ROOT.war");
				 Path copyTo = Paths.get("C:/workspace_tmp/", "1234.war");
				 
				 Files.copy(copyFrom, copyTo, REPLACE_EXISTING);
				 */
				 return write("return : Success");
		      } catch(Exception e) {
		    	 log.error("---------------------------------------------------------------\n" + e.toString());
		    	 return write("FileCopy Fail");
		      }
		 }

		 /**
		 * cpLincence -- 라이센스 저장
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse saveLicense(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
		        	tx.begin();
		       		createObject("mg.cp.saveLincence", request.getMap()); 
			        tx.commit();
			        
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Successful save operation licenses.");
		        } catch (Exception e) {
		        	tx.rollback();
		        	log.error("------------saveLincence----------------------------------------------------------------\n" + e.toString());
		        	
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed save operation licenses.");
		        }
		        return write(retobj.toString());
		 }
		 
		 /**
		 * cpLincence -- 라이센스 조회
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse getLicense(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
			        Map map  = (Map)getItem("mg.cp.getLicense",request.getMap());

			        retobj.put("ProdNo", this.getStringWithNullCheck(map,"PRODNO"));
			        retobj.put("OtherData1", this.getStringWithNullCheck(map,"OTHERDATA1"));
			        retobj.put("OtherData2", this.getStringWithNullCheck(map,"OTHERDATA2"));
			        retobj.put("OtherData3", this.getStringWithNullCheck(map,"OTHERDATA3"));
			        retobj.put("OtherData4", this.getStringWithNullCheck(map,"OTHERDATA4"));
			        
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Success");
		        } catch (Exception e) {
		        	log.error("------------getLicense----------------------------------------------------------------\n" + e.toString());
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed get licenses.");
		        }
		        return write(retobj.toString());
		 }
		 
		 /**
		 * cpLincence -- CP 환경정보 조회
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse getCPConfData(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
		        	String dbName = pm.getString("component.sql.database");
		        	Map map  = (Map)getItem("mg.cp.getCPConfData"+getDBName(dbName),request.getMap());

			        retobj.put("EXEPATH", this.getStringWithNullCheck(map,"EXEPATH")); // REG시 target rootpath
			        retobj.put("BASEURL", this.getStringWithNullCheck(map,"BASEURL"));
			        retobj.put("COMPANY", this.getStringWithNullCheck(map,"COMPANY"));
			        retobj.put("TIMEOUT", this.getStringWithNullCheck(map,"TIMEOUT"));
			        retobj.put("SOURCEPATH", this.getStringWithNullCheck(map,"SOURCEPATH")); // REG시 source rootpath
			        retobj.put("TRANSTYPE", this.getStringWithNullCheck(map,"TRANSTYPE")); // FTP 또는 LOCAL 지정
			        retobj.put("LOADINGIMG", pm.getString("cliptorplus.loading.image")); // 파일 열기 전 이미지 지정(default: 0, LG화학:1)
			        
			        retobj.put("host", pm.getString("cliptorplus.ftp.host"));
			        retobj.put("port", pm.getString("cliptorplus.ftp.port"));
			        retobj.put("username", pm.getString("cliptorplus.ftp.username"));
			        retobj.put("password", pm.getString("cliptorplus.ftp.password"));
			        retobj.put("dataConnectionMode", pm.getString("cliptorplus.ftp.dataConnectionMode"));
			        retobj.put("encoding", pm.getString("cliptorplus.ftp.encoding"));
			        retobj.put("serverType", pm.getString("cliptorplus.ftp.serverType"));
			        retobj.put("bufferSize", pm.getString("cliptorplus.ftp.bufferSize"));
			        retobj.put("copypath", pm.getString("cliptorplus.ftp.copypath"));
			        retobj.put("uploadpath", pm.getString("cliptorplus.ftp.uploadpath"));
			        
			        if("2".equals(pm.getString("component.site.product.version"))){
			        	retobj.put("logServieURL", pm.getString("cliptorplus.log.service.url")); //CPD 파일 파일 로그 쌓기 위함.
			        	retobj.put("logOption", pm.getString("cliptorplus.log.option"));
			       }
			       
			        // DC 여부 및 암호화 시 키값
			        if(pm.getBoolean("diskcryptor.auth.code.use")){
			        	retobj.put("dcType", "1"); //0: DC사용안함, 1: myDecr.dll로 암호화, 2:그외의 암호화 모듈 사용
			        }else{
			        	retobj.put("dcType", "0"); //0: DC사용안함, 1: myDecr.dll로 암호화, 2:그외의 암호화 모듈 사용	
			        }
			        retobj.put("dcKey", pm.getString("diskcryptor.auth.code"));//키값
			        			        
			         List lists = getList("mg.cp.getExcPolicy",request.getMap());
			         List<JSONObject> jsonLists = new ArrayList<JSONObject>();
			         JSONObject jsonitem = null;
			         
			         
		 	         for (int i = 0; i < lists.size(); i++) {
		 	          	map =(Map) lists.get(i);
		 	          	
		 	          	jsonitem = new JSONObject();
		 	          	jsonitem.put("EXC_NO",  this.getStringWithNullCheck(map,"EXC_NO"));
		 	          	jsonitem.put("CP_EXC_CL_CD",this.getStringWithNullCheck(map,"CP_EXC_CL_CD"));
		 	          	jsonitem.put("EXC_PROCESS",this.getStringWithNullCheck(map,"EXC_PROCESS"));
		 	          	jsonitem.put("EXC_WAITING_TM",this.getStringWithNullCheck(map,"EXC_WAITING_TM"));
		 	          	jsonitem.put("EXC_ALLO_FOLDER", getPolicyFolder(map, "allow"));
		 	          	jsonitem.put("EXC_REST_FOLDER",getPolicyFolder(map, "rest"));
		 	          	jsonLists.add(jsonitem);
		 	         }
		 	        retobj.put("list", jsonLists);   
				        
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Success");
		        } catch (Exception e) {
		        	log.error("------------getCPConfData----------------------------------------------------------------\n" + e.toString());
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed getCPConfData.");
		        }
		        return write(retobj.toString());
		 }
		 
		 /**
		 * getCPFileList --  샌드박스의 변경된 파일 리스트를 가져온다.
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse getCPFileList(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
		        	Map map  = (Map)getItem("mg.cp.getCPFileList", request.getMap());

			        retobj.put("FILE_NUM", this.getStringWithNullCheck(map,"FILE_NUM")); // REG시 target rootpath
			        retobj.put("FILE_LIST", this.getStringWithNullCheck(map,"FILE_LIST"));
			        retobj.put("FILE_DEL_YN", this.getStringWithNullCheck(map,"FILE_DEL_YN"));
				        
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Success");
		        } catch (Exception e) {
		        	log.error("------------getCPFileList----------------------------------------------------------------\n" + e.toString());
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed getCPFileList.");
		        }
		        return write(retobj.toString());
		 }
		 
		 private String getPolicyFolder(Map param, String gubun) 
		 {
			 String retval="";
			 String dvide="";
			 try {
				 List lists;
		       	 if("allow".equals(gubun)) {
		       		lists = getList("mg.cp.getExcAlloFolder", param); 
		       	 } else {
		       		lists = getList("mg.cp.getExcRestFolder" , param);
		       	 }
				  
				 for (int i = 0; i < lists.size(); i++) {
				  	Map map =(Map) lists.get(i);	 
				  	retval += dvide + this.getStringWithNullCheck(map, "FOLDER_ROOT") + this.getStringWithNullCheck(map, "FOLDER_NM");
				  	dvide = "|";
				  }
				  return retval;
	          } catch(Exception e) {
	         	  log.error("--------getPolicyFolder--------------------\n" + e.getMessage());
	         	  return retval;
	           }
		 }
		 
		 
		 /** 
		 * cp 진행 상태 저장
		 * @param request
		 * @return
		 * @throws Exception
		 * cp_sts_cd=04
		 * 최종 수정일 : 2016. 03. 14
		 * 수정 내용 : 보안EXE 생성 완료 작업, 해당 구간은 request내 정보는 cp_sts_cd만 들어있다. (locale 등의 사용자 정보 없음.)
		 * 중요 참조 : mg.cp.getDocNoForMail 에러 발생시 쿼리 및 resultClass 타입 확인, 기능 추가로 인한 타입변경 string -> outmap
		 */
		 private IResponse setStateCP(IRequest request) throws Exception 
		 { 
			 log.info("-----setStateCP----전달값 : " + request.getMap().toString());
			 
			    JSONObject retobj = new JSONObject();
			    
			    Locale locale = new Locale("en");
			    String pLocale = request.getStringParam("j_language");
			    if(pLocale != null && !"".equals(pLocale)) {
			    	locale = new Locale(pLocale);
			    }
			    
		        try {
		        	 String sqlname="";
		        	  if("LINK".equals(pm.getString("component.site.iftype"))) {
		        		  sqlname = "mg.cp.setScheduleCP_LINK";
			          } else {
			        	  sqlname = "mg.cp.setStateCP";
			          }
		        	
		        	String sts_cd = request.getStringParam("cp_sts_cd");
		        	Map param = request.getMap();
		        	//log.info("---------1----------");
		        	tx.begin();
		        	if("03".equals(sts_cd)) {
		        		//log.info("---------2----------");
		        		createObject("mg.cp.setSchedule", request.getMap()); 
		        		createObject("mg.cp.setStatus", request.getMap());
		        		//log.info("--------- 2-1 ----------");
		        	} else if("04".equals(sts_cd)) {  // 완료이면
		        		//log.info("---------3----------");
		        		//log.info("setStateCP 받은 파라미터 값 : " + param.toString());
		        		
		        		String exeFiles = request.getStringParam("exe_file");
		        		int fileNum = Integer.parseInt(request.getStringParam("file_num"));
		        		if(fileNum >2) {  // 실행 파일 개수가 1 개 이상이면
		        			//(복수개 일 경우 실행파일명.exe,실행파일명.DATZIP, 실행파일명,DI002)
		        			
		        			String[] tmpFiles = exeFiles.split("\\.");
		        			exeFiles += "|" + tmpFiles[0] + ".DATZIP";
		        			for(int i=2; i < fileNum; i++) {
		        				exeFiles += "|" + tmpFiles[0] + ".D" + String.format("%02d", i);
		        			}
		        		}
		        		param.put("exe_file", exeFiles);
		        		
		        		createObject(sqlname, param);
		        		
		        		sqlname = "mg.cp.setStatusCP_LINK";
		        		createObject(sqlname, param);
		        		//log.info("--------- 3-1 ----------");
		        		
		        		Map docFrom = (Map)getItem("mg.cp.getInClCd", param);
		        		
		        		// 수신자에게 CP다운로드 요청 메일전송
		        		if("Y".equals(pm.getString("cliptorplus.receiver.mail")) && !"S".equals(this.getStringWithNullCheck(docFrom,"IN_CL_CD"))){
			    			try{
				    			//log.info("CP다운로드 요청 메일전송");
			    				param.put("dbName", pm.getString("component.sql.database"));
				    			List mailAddr = getList("mg.cp.getReceiverEmail"+getDBName(pm.getString("component.sql.database")), param);
				    			Map baseMap = (Map)getItem("mg.su.selectBaseUrl",param);
					    		String baseURL =this.getStringWithNullCheck(baseMap, "BASEURL");
				    	     	//log.info("baseURL-----------------------------------"+baseURL);
				    	     	
				    			
				    			//log.info("메일보낼 수신자 수 : " + mailAddr.size());
				    			
				    			for(int i=0;i<mailAddr.size();i++) {
				    				Map smtp = (Map)mailAddr.get(i);
					       			IMail mail = mailHandle.createHtmlMail();
					       			mail.addTo(this.getStringWithNullCheck(smtp, "TO_EMAIL"));
					       			mail.setFrom(this.getStringWithNullCheck(smtp, "FROM_EMAIL"));
					    	    	//mail.setSubject(mm.getMessage("MAIL_0021", locale));
					       		    mail.setSubject("["+mm.getMessage("MAIL_0004", locale)+"]"+" "+mm.getMessage("MAIL_0021", locale)); //보안문서가 도착했습니다.
					       		    Map docNoItem = (Map)getItem("mg.cp.getDocNoForMail", param);
					       		    String docNo_mail = docNoItem.get("DOC_NO").toString();
					       		    String docMsg = docNoItem.get("DOC_MSG").toString();
					       		    //String fileName = "config/templates/mail_Template_Portal.html";
					    	 		//String mailContext = readFile(fileName);					    	 		
					    	 		
					    	 		//메일 템플릿을 읽어 온다.
							         String fileName = "";
							         String mailContext = "";
							         if("Y".equals(pm.getString("cliptorplus.mail.desc"))){
							        	 if("ILJIN".equals(pm.getString("component.site.company"))){
						    	 			log.info("------------Policy_mail_Template------------");
						    	 			fileName = "config/templates/mail_Template_Policy.html";
						    	 			mailContext = readFile(fileName);
						    	 			
						    	 			mailContext = mailContext.replace("EXPIRE_LABEL", mm.getMessage("MAIL_1014", locale));
						    	 			mailContext = mailContext.replace("EXPIRE_DT", (String)smtp.get("EXPIRE_DT"));
											mailContext = mailContext.replace("COUNT_LABEL", mm.getMessage("MAIL_1015", locale));
											mailContext = mailContext.replace("READCOUNT", (String)smtp.get("READCOUNT"));
							        	 } else {
						    	 			fileName = "config/templates/mail_Template_Desc.html";
						    	 			mailContext = readFile(fileName);
							        	 }
							        	 
							        	 mailContext = mailContext.replace("DESC_HIDDEN", "visible");
							        	 mailContext = mailContext.replace("DESC_LABEL", "");
							        	 String temp = docMsg;
							        	 temp = temp.replace("\n", "<br>");
							        	 mailContext = mailContext.replace("DESC_CONTENT", temp);
							         } else {
							        	 fileName = "config/templates/mail_Template.html";
							        	 mailContext = readFile(fileName);
							         }
					    	 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", locale)+"]");
					    			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0021", locale));
					    			 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.pp.url"));
					    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.pp.url") + pm.getString("component.site.image"));
					    			 Object[] params2 = {(String)smtp.get("FROM_NAME"), pm.getString("component.site.company.name")};
					    		     String tmp = mm.getMessage("MAIL_0022", params2, locale);
					    			 mailContext = mailContext.replace("MAIL_CONTENT",  tmp);
					    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", locale));
									 mailContext = mailContext.replace("COMP_NM", pm.getString("component.site.company.name"));
									 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1008", locale));
									 mailContext = mailContext.replace("USER_NM", (String)smtp.get("FROM_NAME"));
									 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1009", locale));
									 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
							   		 String curDt = formatter.format(new Date());
									 mailContext = mailContext.replace("REQ_DT", curDt);
									 //SELECT * FROM CP_DOC_MASTER WHERE DOC_NO = '#DOC_NO#'
									 log.info("수신자에게 CP다운로드 요청 메일전송----------------------------------------------------------------------"+docNo_mail);
									if("Y".equals(pm.getString("cliptorplus.mail.direct.down"))){
										mailContext = mailContext.replace("LINK", pm.getString("component.site.pp.url")+"/cp/approval.do?mod=down&PGM_ID=regMgmt&DOC_NO="+docNo_mail);//파트너 포탈
										mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1007", locale));
									}else{
										mailContext = mailContext.replace("LINK", pm.getString("component.site.pp.url"));//파트너 포탈
										mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006", locale));
										
									}
					    			 // 메일 발송
					    			 mail.setHtmlMessage(mailContext);
					    	         mail.send();
					    	         
				    			}
				    			
				    		}catch(Exception e){
				    			log.error("Send Mail-----------------------------------------------------------"+e.toString());
				    			//return write("Send Mail Fail");
				    		}
			    		}	
		        		//완료인 경우 문서번호+status값 04 http 전송
	 	          		if("Y".equals(pm.getString("cliptorplus.response.http"))){
	 	          			param.toString();
	 	          			String docNo = (String)getItem("mg.cp.getSuccessDocNo", param);
	 	          			log.info("-----------http response use status 04----------" + pm.getString("cliptorplus.response.url") + "?docno=" + docNo + "&status=04");
		 	          		try{
		 	          			URL url = new URL(pm.getString("cliptorplus.response.url") + "?docno=" + docNo + "&status=04");
			 	          		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			 	          		conn.setUseCaches(false);
			 	          		conn.setRequestMethod("GET");
			 	          		//conn.setRequestProperty("docno", docNo);
			 	          		log.info("Response Code : " + conn.getResponseCode()); //코드값 200(성공)
			 	          		
		 	          		}catch(Exception e){
		 	          			log.error("------------http response fail status 04--------------");
		 	          		}
	 	          		}
		        		
		        	} else if("05".equals(sts_cd)){
		        		createObject(sqlname, param);
		        		
		        		  if("LINK".equals(pm.getString("component.site.iftype"))) {
		        			  createObject("mg.cp.setStatusCP_LINK", param);
		        		  }
		        		//오류인 경우 문서번호+status값 05 http 전송
	 	          		if("Y".equals(pm.getString("cliptorplus.response.http"))){
	 	          			param.toString();
	 	          			String docNo = (String)getItem("mg.cp.getSuccessDocNo", param);
	 	          			log.info("-----------http response use status 05----------" + pm.getString("cliptorplus.response.url") + "?docno=" + docNo + "&status=05");
		 	          		try{
		 	          			URL url = new URL(pm.getString("cliptorplus.response.url") + "?docno=" + docNo + "&status=05");
			 	          		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			 	          		conn.setUseCaches(false);
			 	          		conn.setRequestMethod("GET");
			 	          		//conn.setRequestProperty("docno", docNo);
			 	          		log.info("Response Code : " + conn.getResponseCode()); //코드값 200(성공)
		 	          		}catch(Exception e){
		 	          			log.error("------------http response fail status 05--------------");
		 	          		}
	 	          		}
		        		
	 	          		//오류인 경우 담당자에게 메일 발송 
		        		if("Y".equals(pm.getString("cliptorplus.sysmanager.mail"))){
			    			try{
			    				param.put("dbName", pm.getString("component.sql.database"));
				    			List docInfo = getList("mg.cp.getReceiverEmail"+getDBName(pm.getString("component.sql.database")), param); //수신자 아이디/이름, 발신자 아이디/이름, 문서번호 
				    			Map baseMap = (Map)getItem("mg.su.selectBaseUrl",param);
					    		String baseURL =this.getStringWithNullCheck(baseMap, "BASEURL");
				       			String[] managers = pm.getString("cliptorplus.mail.manager.mailaddress").split("\\|");
				       			log.info("메일보낼 수신자 수2 : " + managers.length);
				    			Map smtp =  (Map)docInfo.get(0);
				    			
				       			String sysname="";
				       			if("C".equals(smtp.get("IN_CL_CD"))) sysname="["+mm.getMessage("MAIL_0004", locale)+"]";
				       			else sysname=pm.getString("cliptorplus.mail.sysname.service");
				    			
				    			for(int i=0;i<managers.length;i++) {			    				
					       			IMail mail = mailHandle.createHtmlMail();
					       			log.info("오류 시--------------------------------------메일 수신자 :" + managers[i]);
					       			mail.addTo(managers[i]);
					       			mail.setFrom(managers[i]);
					    	    	//mail.setSubject(mm.getMessage("MAIL_0021", locale));
					       		    mail.setSubject(sysname+ mm.getMessage("MAIL_0071", locale));//보안파일 생성 중 오류가 발생
					    	          // 메일 템플릿을 읽어 온다.
					       		    String fileName = "config/templates/mail_Template.html";
					       		    String mailContext = readFile(fileName);
					       		    
					    	 		 mailContext = mailContext.replace("SYSTEMNAME", sysname+ mm.getMessage("MAIL_0071", locale));
					    			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0072", locale));//보안파일 생성 중 오류가 발생하였습니다.
					    			 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.url"));
					    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));
					    			 
					    			 //Object[] params2 = {(String)smtp.get("FROM_NAME"), (String)smtp.get("USER_ID"), (String)smtp.get("DOC_NO")};
					    			 Object[] params2 = {request.getStringParam("job_desc")};
					    			 String tmp = mm.getMessage("MAIL_0073", params2, locale);//
					    			 mailContext = mailContext.replace("MAIL_CONTENT",  tmp);
					    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1010", locale));
									 mailContext = mailContext.replace("COMP_NM", (String)smtp.get("DOC_NO"));
									 mailContext = mailContext.replace("USER_LABLE",  mm.getMessage("MAIL_1011", locale));
									 mailContext = mailContext.replace("USER_NM", (String)smtp.get("DEPT_NM")+" - "+(String)smtp.get("FROM_NAME")+"("+(String)smtp.get("USER_ID")+") / "+(String)smtp.get("TO_NAME")+"("+(String)smtp.get("TO_EMAIL")+")");
									 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1013", locale));
									 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							   		 String curDt = formatter.format(new Date());
									 mailContext = mailContext.replace("REQ_DT", curDt);
									 mailContext = mailContext.replace("LINK", "");
									 mailContext = mailContext.replace("BUTTONTEXT", "");
					    			  
					    			 // 메일 발송
					    			 mail.setHtmlMessage(mailContext);
					    	         mail.send();					    	         
				    			}
				    			
				    		}catch(Exception e){
				    			log.error("Send Mail-----------------------------------------------------------"+e.toString());
				    			//return write("Send Mail Fail");
				    		}
			    		}	
	 	          		
	 	          		
	 	          		
		        	}else {
		        		createObject(sqlname, param); 
		        	}
			        tx.commit();
			        
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Successful Set Status.");
		        } catch (Exception e) {
		        	tx.rollback();
		        	log.error("------------setStateCP----------------------------------------------------------------\n" + e.toString());
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed Set Status.");
		        }
		        
		        //log.info("리턴되는 값 : " + retobj.toString());
		        return write(retobj.toString());
		 }
		 
		 
		 /**
		 * CP 진행율 저장
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse setProgressCP(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
		        	
		        	if("LINK".equals(pm.getString("component.site.iftype"))) {
	
			        	tx.begin();
			       		createObject("mg.cp.setProgressCP", request.getMap()); 
				        tx.commit();
		        	}
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Successful Set Progress.");
		        } catch (Exception e) {
		        	tx.rollback();
		        	log.error("------------setProgressCP----------------------------------------------------------------\n" + e.toString());
		        	
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed Set Progress.");
		        }
		        return write(retobj.toString());
		 }		
		 
		 /**
		 * 2016/02/15 수정 : DB name 에 따른 쿼리 선택
		 * CP Schedule Job 조회
		 * @param request
		 * @return
		 * @throws Exception
		 */
		 private IResponse getScheduleJob(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
		        	
		        	 String rowcnt = request.getStringParam("rowcnt");
		        	
			         List lists;
			         
			         //2016/02/15 추가 : DB name 구분
			         String dbName = pm.getString("component.sql.database");
			         String cpVersion = pm.getString("component.site.product.version");
			         Map reqMap = request.getMap();
			         reqMap.put("cpVersion", cpVersion);
			        
			        if("LINK".equals(pm.getString("component.site.iftype"))) {
				       	 lists = getList("mg.cp.getScheduleJob_LINK"+getDBName(pm.getString("component.sql.database")),reqMap);
				    } else {
				    	 if("personal".equals(pm.getString("component.export.user"))){
				        	 lists = getList("mg.cp.getScheduleJob"+getDBName(dbName),reqMap);
				         }else{
				        	 lists = getList("mg.cp.getScheduleJob_Public"+getDBName(dbName), reqMap);
				        	 log.info("component.export.user=public 일때 getScheduleJob");
				         }
				    }

			         // File List, UUID, Path 값을 만들어 줘야 한다. FTP로 다운받을 때 필요
			         List<JSONObject> jsonCategories = new ArrayList<JSONObject>();
			         JSONObject jsonCategory = null;
			         log.info("전달할 수신자 수 : " + lists.size());
			         String reg_user_id2 = "";
			         String reg_passwd = "";
			         String reg_mac_list = "";
			         String reg_ip_list="";
			         //추가
			         String reg_url_list="";
			         
			         
			     	Map confMap  = (Map)getItem("mg.cp.getCPConfData"+getDBName(dbName),request.getMap());			         
			         
		 	         for (int i = 0; i < lists.size(); i++) {
		 	          	Map map =(Map) lists.get(i);
		 	          	
		 	          	jsonCategory = new JSONObject();
		 	          	jsonCategory.put("doc_no",  this.getStringWithNullCheck(map,"DOC_NO"));
		 	          	if("2".equals(cpVersion)){
		 	          		jsonCategory.put("cre_type",  this.getStringWithNullCheck(map,"CRE_TYPE"));  //생성타입 E: EXE, D:CPD
		 	          	}
		 	          	jsonCategory.put("sche_no",this.getStringWithNullCheck(map,"SCHE_NO"));
		 	          	jsonCategory.put("cp_sts_cd",this.getStringWithNullCheck(map,"CP_STS_CD"));
		 	          	jsonCategory.put("reg_dt",this.getStringWithNullCheck(map,"REG_DT"));
		 	          	jsonCategory.put("approve_dt",this.getStringWithNullCheck(map,"APPROVE_DT"));
		 	          	jsonCategory.put("job_desc",this.getStringWithNullCheck(map,"JOB_DESC"));
		 	          	jsonCategory.put("doc_title",this.getStringWithNullCheck(map,"DOC_TITLE"));
		 	          	jsonCategory.put("user_id1",this.getStringWithNullCheck(map,"USER_ID1"));
		 	          	jsonCategory.put("authority",this.getStringWithNullCheck(map,"AUTHORITY"));
		 	          	if("FTP".equals(this.getStringWithNullCheck(confMap,"TRANSTYPE"))) jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_NAME_LIST"));
		 	          	else jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_LIST"));
		 	          	jsonCategory.put("file_uuid",this.getStringWithNullCheck(map,"FILE_UUID"));
		 	          	jsonCategory.put("file_path",this.getStringWithNullCheck(map,"FILE_PATH"));
		 	          	jsonCategory.put("file_size",this.getStringWithNullCheck(map,"FILE_SIZE"));
		 	          	jsonCategory.put("file_num",this.getStringWithNullCheck(map,"FILE_NUM"));
		 	          	jsonCategory.put("exe_file",this.getStringWithNullCheck(map,"EXE_FILE"));
		 	          	jsonCategory.put("waiting_time",this.getStringWithNullCheck(map,"WAITING_TIME"));
		 	          	jsonCategory.put("in_process",this.getStringWithNullCheck(map,"IN_PROCESS"));
		 	          	
		 	          	if("2".equals(cpVersion)){
		 	          		jsonCategory.put("user_id1",this.getStringWithNullCheck(map,"USER_ID1"));
		 	          		jsonCategory.put("user_nm1",this.getStringWithNullCheck(map,"USER_NM1"));
		 	          	}

		 	          	
		 	            if("LINK".equals(pm.getString("component.site.iftype"))) {
		 	        	   jsonCategory.put("user_id2",this.getStringWithNullCheck(map,"USER_ID2").replace("|", "$|+")); //LINK방식인 경우 한 행에 수신자데이터가 모두 있음
		 	        	   jsonCategory.put("passwd",this.getStringWithNullCheck(map,"PASSWD").replace("|", "$|+")); //LINK방식인 경우 한 행에 수신자데이터가 모두 있음
		 	        	   jsonCategory.put("mac_list",this.getStringWithNullCheck(map,"MAC_LIST").replace(" ", "").replace("^", "$|+")); //LINK방식인 경우 수신자MAC구분자 ^를 $|+로 변경
		 	        	   jsonCategory.put("EXEPATH", this.getStringWithNullCheck(map,"EXE_PATH"));
				        }else{
				        	//LINK 방식이 아닌경우 - 수신자 ID/PW/MAC 구분자로 묶어서 전송
				        	jsonCategory.put("EXEPATH", this.getStringWithNullCheck(map,"EXE_PATH"));
				        	for (int j = 0; j < lists.size(); j++) {
				        		Map map2 =(Map) lists.get(j);
					        	if(j==(lists.size()-1)){
				 	          		// 마지막 수신자는 구분자 붙이지 않음
				 	          		reg_user_id2 += this.getStringWithNullCheck(map2,"USER_ID2");
				 	          		reg_passwd += this.getStringWithNullCheck(map2,"PASSWD");
				 	          		reg_mac_list += this.getStringWithNullCheck(map2,"MAC_LIST");
				 	          		if("2".equals(cpVersion)) {
				 	          			reg_ip_list = this.getStringWithNullCheck(map2,"IP_LIST").replace(",", "$|+");
				 	          			reg_url_list = this.getStringWithNullCheck(map2,"URL_ATTH_LST").replace("|", "$|+");
				 	          		}else{
				 	          			reg_ip_list += this.getStringWithNullCheck(map2,"IP_LIST");
				 	          		}
				 	          		jsonCategory.put("user_id2",reg_user_id2);
				 	          		jsonCategory.put("passwd",reg_passwd);
				 	          		jsonCategory.put("mac_list",reg_mac_list);
				 	          		jsonCategory.put("ip_list", reg_ip_list);
				 	          		jsonCategory.put("url_list", reg_url_list);
				 	          	}else{
				 	          		reg_user_id2 += this.getStringWithNullCheck(map2,"USER_ID2") + "$|+";
				 	          		reg_passwd += this.getStringWithNullCheck(map2,"PASSWD") + "$|+";
				 	          		reg_mac_list += this.getStringWithNullCheck(map2,"MAC_LIST") + "$|+";
				 	          		reg_ip_list += this.getStringWithNullCheck(map2,"IP_LIST") + "$|+";
				 	          	}
				        	}
				        	
				        	//jsonCategory.put("user_id2",this.getStringWithNullCheck(map,"USER_ID2"));
				        	//jsonCategory.put("passwd",this.getStringWithNullCheck(map,"PASSWD"));
				        	//jsonCategory.put("mac_list",this.getStringWithNullCheck(map,"MAC_LIST"));
				        	
				        }
		 	          	
		 	          	//jsonCategory.put("passwd",this.getStringWithNullCheck(map,"PASSWD"));
		 	          	//jsonCategory.put("passwd",this.getStringWithNullCheck(map,"PASSWD").replace("|", new Character((char)65).toString()));
		 	          
		 	          	jsonCategory.put("policy",this.getStringWithNullCheck(map,"POLICY"));
		 	          	jsonCategory.put("policy_doc",this.getStringWithNullCheck(map,"POLICY_DOC"));
		 	          	jsonCategory.put("mod_stop",this.getStringWithNullCheck(map,"MOD_STOP"));
		 	          	jsonCategory.put("read_count",this.getStringWithNullCheck(map,"READ_COUNT"));
		 	          	jsonCategory.put("user_nm",this.getStringWithNullCheck(map,"USER_NM"));
		 	          	jsonCategory.put("message",this.getStringWithNullCheck(map,"MESSAGE"));
		 	          	jsonCategory.put("expire_dt",this.getStringWithNullCheck(map,"EXPIRE_DT"));
		 	          	
		 	          	//추가
		 	          	jsonCategory.put("dcKey", pm.getString("diskcryptor.auth.code"));//키값
		 	          	
		 	          	//메세지프로퍼티 전달
		 	          	String inType =  this.getStringWithNullCheck(map,"IN_CL_CD");
		 	          	jsonCategory.put("intype",inType);
		 	          	
		 	          	java.util.Locale ko_lo = new java.util.Locale("ko");
		 	          	java.util.Locale en_lo = new java.util.Locale("en");
		 	          //	java.util.Locale ja_lo = new java.util.Locale("ja");
		 	          	java.util.Locale zh_lo = new java.util.Locale("zh");
		 	          	
		 	          	if("S".equals(inType)) jsonCategory.put("MSG01", mm.getMessage("CPCMG_0001",ko_lo)+"{}[]"+mm.getMessage("CPCMG_0001",en_lo)+"{}[]"+mm.getMessage("CPCMG_0001",zh_lo)+"{}[]");
		 	          	else jsonCategory.put("MSG01", mm.getMessage("CPCMG_0003",ko_lo)+"{}[]"+mm.getMessage("CPCMG_0003",en_lo)+"{}[]"+mm.getMessage("CPCMG_0003",zh_lo)+"{}[]");
		 	          	jsonCategory.put("MSG02", mm.getMessage("CPCMG_0002",ko_lo)+"{}[]"+mm.getMessage("CPCMG_0002",en_lo)+"{}[]"+mm.getMessage("CPCMG_0002",zh_lo)+"{}[]");
		 	          	
		        	    jsonCategories.add(jsonCategory);
		        	    
		        	    if(rowcnt != null) 
	        	    	{
	        	    		if(Integer.parseInt(rowcnt) == (i+1) ) break;
	        	    	}
		 	         }
		 	        retobj.put("list", jsonCategories);   
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Success get Schedule Job.");
		        } catch (Exception e) {
		        	log.error("------------getScheduleJob----------------------------------------------------------------\n" + e.toString());
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed get Schedule Job.");
		        }
		        log.info("getSchedualJob 리턴값 : " + retobj.toString());
		        return write(retobj.toString());
		 }
		 private IResponse getScheduleJob1(IRequest request) throws Exception 
		 { 
			    JSONObject retobj = new JSONObject();
		        try {
		        	
		        	 String rowcnt = request.getStringParam("rowcnt");
		        	
			         List lists;
			         
			         //2016/03/07 추가 : DB name 구분
			         String dbName = pm.getString("component.sql.database");
			         
			         if(dbName.equals("mssql")){
			        	if("LINK".equals(pm.getString("component.site.iftype"))) {
				        	 lists = getList("mg.cp.getScheduleJob_LINK"+getDBName(pm.getString("component.sql.database")),request.getMap());
				         } else {
				        	 if("personal".equals(pm.getString("component.export.user"))){
				        		 lists = getList("mg.cp.getScheduleJob"+getDBName(pm.getString("component.sql.database")),request.getMap());
				        	 }else{
				        		 lists = getList("mg.cp.getScheduleJob_Public"+getDBName(pm.getString("component.sql.database"))
, request.getMap());
				        		 log.info("component.export.user=public 일때 getScheduleJob");
				        	 }
				         }
			        } else{
			        	if("LINK".equals(pm.getString("component.site.iftype"))) {
			        		lists = getList("mg.cp.getScheduleJob_LINK"+getDBName(pm.getString("component.sql.database")),request.getMap());
				         } else {
				        	 if("personal".equals(pm.getString("component.export.user"))){
				        		 lists = getList("mg.cp.getScheduleJob_" + dbName,request.getMap());
				        	 }else{
				        		 lists = getList("mg.cp.getScheduleJob_Public_" + dbName, request.getMap());
				        		 log.info("component.export.user=public 일때 getScheduleJob");
				        	 }
				         }
			        }
			         
			         // File List, UUID, Path 값을 만들어 줘야 한다. FTP로 다운받을 때 필요

			         List<JSONObject> jsonCategories = new ArrayList<JSONObject>();
			         JSONObject jsonCategory = null;
			         log.info("전달할 수신자 수 : " + lists.size());
			         String reg_user_id2 = "";
			         String reg_passwd = "";
			         String reg_mac_list = "";
			         String reg_ip_list="";
		 	         for (int i = 0; i < lists.size(); i++) {
		 	          	Map map =(Map) lists.get(i);
		 	          	
		 	          	jsonCategory = new JSONObject();
		 	          	jsonCategory.put("doc_no",  this.getStringWithNullCheck(map,"DOC_NO"));
		 	          	jsonCategory.put("sche_no",this.getStringWithNullCheck(map,"SCHE_NO"));
		 	          	jsonCategory.put("cp_sts_cd",this.getStringWithNullCheck(map,"CP_STS_CD"));
		 	          	jsonCategory.put("reg_dt",this.getStringWithNullCheck(map,"REG_DT"));
		 	          	jsonCategory.put("approve_dt",this.getStringWithNullCheck(map,"APPROVE_DT"));
		 	          	jsonCategory.put("job_desc",this.getStringWithNullCheck(map,"JOB_DESC"));
		 	          	jsonCategory.put("doc_title",this.getStringWithNullCheck(map,"DOC_TITLE"));
		 	          	jsonCategory.put("user_id1",this.getStringWithNullCheck(map,"USER_ID1"));
		 	          	jsonCategory.put("authority",this.getStringWithNullCheck(map,"AUTHORITY"));
		 	          	jsonCategory.put("file_uuid",this.getStringWithNullCheck(map,"FILE_UUID"));
		 	          	jsonCategory.put("file_path",this.getStringWithNullCheck(map,"FILE_PATH"));
		 	          	jsonCategory.put("file_size",this.getStringWithNullCheck(map,"FILE_SIZE"));
		 	          	jsonCategory.put("file_num",this.getStringWithNullCheck(map,"FILE_NUM"));
		 	          	jsonCategory.put("exe_file",this.getStringWithNullCheck(map,"EXE_FILE"));
		 	          	jsonCategory.put("waiting_time",this.getStringWithNullCheck(map,"WAITING_TIME"));
		 	          	jsonCategory.put("in_process",this.getStringWithNullCheck(map,"IN_PROCESS"));

		 	            if("LINK".equals(pm.getString("component.site.iftype"))) {
		 	        	   jsonCategory.put("user_id2",this.getStringWithNullCheck(map,"USER_ID2").replace("|", "$|+")); //LINK방식인 경우 한 행에 수신자데이터가 모두 있음
		 	        	   jsonCategory.put("passwd",this.getStringWithNullCheck(map,"PASSWD").replace("|", "$|+")); //LINK방식인 경우 한 행에 수신자데이터가 모두 있음
		 	        	   jsonCategory.put("mac_list",this.getStringWithNullCheck(map,"MAC_LIST").replace(" ", "").replace("^", "$|+")); //LINK방식인 경우 수신자MAC구분자 ^를 $|+로 변경
		 	        	   jsonCategory.put("EXEPATH", this.getStringWithNullCheck(map,"EXE_PATH"));
				        }else{
				        	//LINK 방식이 아닌경우 - 수신자 ID/PW/MAC 구분자로 묶어서 전송
				        	jsonCategory.put("EXEPATH", this.getStringWithNullCheck(map,"EXE_PATH"));
				        	for (int j = 0; j < lists.size(); j++) {
				        		Map map2 =(Map) lists.get(j);
		
					        	if(j==(lists.size()-1)){
				 	          		// 마지막 수신자는 구분자 붙이지 않음
				 	          		reg_user_id2 += this.getStringWithNullCheck(map2,"USER_ID2");
				 	          		reg_passwd += this.getStringWithNullCheck(map2,"PASSWD");
				 	          		reg_mac_list += this.getStringWithNullCheck(map2,"MAC_LIST");
				 	          		reg_ip_list += this.getStringWithNullCheck(map2,"IP_LIST");
				 	          		//log.info(reg_user_id2);
				 	          		//log.info(reg_passwd);
				 	          		//log.info(reg_mac_list);
				 	          		jsonCategory.put("user_id2",reg_user_id2);
				 	          		jsonCategory.put("passwd",reg_passwd);
				 	          		jsonCategory.put("mac_list",reg_mac_list);
				 	          		jsonCategory.put("ip_list", reg_ip_list);
				 	          	}else{
				 	          		reg_user_id2 += this.getStringWithNullCheck(map2,"USER_ID2") + "$|+";
				 	          		reg_passwd += this.getStringWithNullCheck(map2,"PASSWD") + "$|+";
				 	          		reg_mac_list += this.getStringWithNullCheck(map2,"MAC_LIST") + "$|+";
				 	          		reg_ip_list += this.getStringWithNullCheck(map2,"IP_LIST") + "$|+";
				 	          	}
				        	}
				        	
				        }
		 	          	
		 	          	jsonCategory.put("policy",this.getStringWithNullCheck(map,"POLICY"));
		 	          	jsonCategory.put("policy_doc",this.getStringWithNullCheck(map,"POLICY_DOC"));
		 	          	jsonCategory.put("mod_stop",this.getStringWithNullCheck(map,"MOD_STOP"));
		 	          	jsonCategory.put("read_count",this.getStringWithNullCheck(map,"READ_COUNT"));
		 	          	jsonCategory.put("user_nm",this.getStringWithNullCheck(map,"USER_NM"));
		 	          	//jsonCategory.put("mac_list",this.getStringWithNullCheck(map,"MAC_LIST"));
		 	         
		 	          	jsonCategory.put("message",this.getStringWithNullCheck(map,"MESSAGE"));
		 	          	jsonCategory.put("expire_dt",this.getStringWithNullCheck(map,"EXPIRE_DT"));

		 	          	//메세지프로퍼티 전달
		 	          	String inType =  this.getStringWithNullCheck(map,"IN_CL_CD");

		 	          	
		 	          	java.util.Locale ko_lo = new java.util.Locale("ko");
		 	          	java.util.Locale en_lo = new java.util.Locale("en");
		 	          	//java.util.Locale ja_lo = new java.util.Locale("ja");
		 	          	java.util.Locale zh_lo = new java.util.Locale("zh");
		 	          	
		 	          	jsonCategory.put("intype",inType);
		 	          	if("S".equals(inType)) jsonCategory.put("MSG01", mm.getMessage("CPCMG_0001",ko_lo)+"{}[]"+mm.getMessage("CPCMG_0001",en_lo)+"{}[]"+mm.getMessage("CPCMG_0001",zh_lo)+"{}[]");
		 	          	else jsonCategory.put("MSG01", mm.getMessage("CPCMG_0003",ko_lo)+"{}[]"+mm.getMessage("CPCMG_0003",en_lo)+"{}[]"+mm.getMessage("CPCMG_0003",zh_lo)+"{}[]");
		 	          	jsonCategory.put("MSG02", mm.getMessage("CPCMG_0002",ko_lo)+"{}[]"+mm.getMessage("CPCMG_0002",en_lo)+"{}[]"+mm.getMessage("CPCMG_0002",zh_lo)+"{}[]");
		 	          	
		 	          	log.info("------------------중국어 -------------------"+mm.getMessage("CPCMG_0002",zh_lo));
		 	          	
		        	    jsonCategories.add(jsonCategory);
		        	    
		        	    if(rowcnt != null) 
	        	    	{
	        	    		if(Integer.parseInt(rowcnt) == (i+1) ) break;
	        	    	}
		 	         }
		 	        retobj.put("list", jsonCategories);   
			        retobj.put("errcode", "0");
			        retobj.put("errmsg", "Success get Schedule Job.");
		        } catch (Exception e) {
		        	log.error("------------getScheduleJob----------------------------------------------------------------\n" + e.toString());
		        	retobj.put("errcode", "-1");
			        retobj.put("errmsg", "Failed get Schedule Job.");
		        }
		        log.info("getSchedualJob 리턴값 : " + retobj.toString());
		        return write(retobj.toString());
		 }
		 /**
			 * CP Schedule Job 조회
			 * @param request
			 * @return
			 * @throws Exception
			 */
			 private IResponse getScheduleJob2(IRequest request) throws Exception 
			 { 
				    JSONObject retobj = new JSONObject();
			        try {
			        	
			        	 String rowcnt = request.getStringParam("rowcnt");
			        	
				         List lists;
				         
				         //2016/03/07 추가 : DB name 구분
				         String dbName = pm.getString("component.sql.database");
				         
				         if(dbName.equals("mssql")){
				        	if("LINK".equals(pm.getString("component.site.iftype"))) {
					        	 lists = getList("mg.cp.getScheduleJob_LINK"+getDBName(pm.getString("component.sql.database")),request.getMap());
					         } else {
					        	 if("personal".equals(pm.getString("component.export.user"))){
					        		 lists = getList("mg.cp.getScheduleJob"+getDBName(pm.getString("component.sql.database")),request.getMap());
					        	 }else{
					        		 lists = getList("mg.cp.getScheduleJob_Public"+getDBName(pm.getString("component.sql.database"))
, request.getMap());
					        		 log.info("component.export.user=public 일때 getScheduleJob");
					        	 }
					         }
				        } else{
				        	if("LINK".equals(pm.getString("component.site.iftype"))) {
				        		lists = getList("mg.cp.getScheduleJob_LINK"+getDBName(pm.getString("component.sql.database")),request.getMap());
					         } else {
					        	 if("personal".equals(pm.getString("component.export.user"))){
					        		 lists = getList("mg.cp.getScheduleJob_" + dbName,request.getMap());
					        	 }else{
					        		 lists = getList("mg.cp.getScheduleJob_Public_" + dbName, request.getMap());
					        		 log.info("component.export.user=public 일때 getScheduleJob");
					        	 }
					         }
				        }
				         
				         // File List, UUID, Path 값을 만들어 줘야 한다. FTP로 다운받을 때 필요
				        
				         
				         List<JSONObject> jsonCategories = new ArrayList<JSONObject>();
				         JSONObject jsonCategory = null;
				         log.info("전달할 수신자 수 : " + lists.size());
				         String reg_user_id2 = "";
				         String reg_passwd = "";
				         String reg_mac_list = "";
				         String reg_ip_list="";
			 	         for (int i = 0; i < lists.size(); i++) {
			 	          	Map map =(Map) lists.get(i);
			 	          	jsonCategory = new JSONObject();
			 	          	
			 	          	/*if("LGE".equals(pm.getString("component.site.company")))*/ 
			 	          	Map confMap  = (Map)getItem("mg.cp.getCPConfData"+getDBName(dbName),request.getMap());			    
			 	          	if("FTP".equals(this.getStringWithNullCheck(confMap,"TRANSTYPE"))) 
			 	          		jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_NAME_LIST"));
			 	          	else jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_LIST"));
			 	         
			        	    jsonCategories.add(jsonCategory);
			        	    
			        	    if(rowcnt != null) 
		        	    	{
		        	    		if(Integer.parseInt(rowcnt) == (i+1) ) break;
		        	    	}
			 	         }
			 	        retobj.put("list", jsonCategories);   
				        retobj.put("errcode", "0");
				        retobj.put("errmsg", "Success get Schedule Job.");
			        } catch (Exception e) {
			        	log.error("------------getScheduleJob----------------------------------------------------------------\n" + e.toString());
			        	retobj.put("errcode", "-1");
				        retobj.put("errmsg", "Failed get Schedule Job.");
			        }
			        log.info("getSchedualJob 리턴값 : " + retobj.toString());
			        return write(retobj.toString());
			 }
		 /**
			 * MyGuard 반출 : Root 폴더 조회 
			 * @param request
			 * @return
			 * @throws Exception
			 */
			 private IResponse getRoot(IRequest request) throws Exception 
			 { 
				    JSONObject retobj = new JSONObject();
			        try {
			        	
			        	 String rowcnt = request.getStringParam("rowcnt");
			        	
				         List lists = getList("mg.cp.getScheduleJob"+getDBName(pm.getString("component.sql.database")),request.getMap());
				         		        
				         List<JSONObject> jsonCategories = new ArrayList<JSONObject>();
				         JSONObject jsonCategory = null;
				         
			 	         for (int i = 0; i < lists.size(); i++) {
			 	          	Map map =(Map) lists.get(i);
			 	          	
			 	          	jsonCategory = new JSONObject();
			 	          	jsonCategory.put("doc_no",  this.getStringWithNullCheck(map,"DOC_NO"));
			 	          	jsonCategory.put("sche_no",this.getStringWithNullCheck(map,"SCHE_NO"));
			 	          	jsonCategory.put("cp_sts_cd",this.getStringWithNullCheck(map,"CP_STS_CD"));
			 	          	jsonCategory.put("reg_dt",this.getStringWithNullCheck(map,"REG_DT"));
			 	          	jsonCategory.put("approve_dt",this.getStringWithNullCheck(map,"APPROVE_DT"));
			 	          	jsonCategory.put("job_desc",this.getStringWithNullCheck(map,"JOB_DESC"));
			 	          	jsonCategory.put("doc_title",this.getStringWithNullCheck(map,"DOC_TITLE"));
			 	          	jsonCategory.put("user_idasuser_id1",this.getStringWithNullCheck(map,"USER_IDASUSER_ID1"));
			 	          	jsonCategory.put("authority",this.getStringWithNullCheck(map,"AUTHORITY"));
			 	          	jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_LIST"));
			 	          	jsonCategory.put("file_list",this.getStringWithNullCheck(map,"FILE_LIST"));
			 	          	jsonCategory.put("file_uuid",this.getStringWithNullCheck(map,"FILE_UUID"));
			 	          	jsonCategory.put("file_path",this.getStringWithNullCheck(map,"FILE_PATH"));
			 	          	jsonCategory.put("file_size",this.getStringWithNullCheck(map,"FILE_SIZE"));
			 	          	jsonCategory.put("file_num",this.getStringWithNullCheck(map,"FILE_NUM"));
			 	          	jsonCategory.put("exe_file",this.getStringWithNullCheck(map,"EXE_FILE"));
			 	          	jsonCategory.put("waiting_time",this.getStringWithNullCheck(map,"WAITING_TIME"));
			 	          	jsonCategory.put("in_process",this.getStringWithNullCheck(map,"IN_PROCESS"));
			 	          	jsonCategory.put("user_idasuser_id2",this.getStringWithNullCheck(map,"USER_IDASUSER_ID2"));
			 	          	jsonCategory.put("passwd",this.getStringWithNullCheck(map,"PASSWD"));
			 	          	jsonCategory.put("policy",this.getStringWithNullCheck(map,"POLICY"));
			 	          	jsonCategory.put("policy_doc",this.getStringWithNullCheck(map,"POLICY_DOC"));
			 	          	jsonCategory.put("mod_stop",this.getStringWithNullCheck(map,"MOD_STOP"));
			 	          	jsonCategory.put("read_count",this.getStringWithNullCheck(map,"READ_COUNT"));
			 	          	jsonCategory.put("user_nm",this.getStringWithNullCheck(map,"USER_NM"));
			 	          	jsonCategory.put("mac_list",this.getStringWithNullCheck(map,"MAC_LIST"));
			 	          	jsonCategory.put("message",this.getStringWithNullCheck(map,"MESSAGE"));
			 	          	jsonCategory.put("expire_dt",this.getStringWithNullCheck(map,"EXPIRE_DT"));

			        	    jsonCategories.add(jsonCategory);
			        	    
			        	    if(rowcnt != null) 
		        	    	{
		        	    		if(Integer.parseInt(rowcnt) == (i+1) ) break;
		        	    	}
			 	         }
			 	        retobj.put("list", jsonCategories);   
				        retobj.put("errcode", "0");
				        retobj.put("errmsg", "Success get Schedule Job.");
			        } catch (Exception e) {
			        	log.error("------------getScheduleJob----------------------------------------------------------------\n" + e.toString());
			        	retobj.put("errcode", "-1");
				        retobj.put("errmsg", "Failed get Schedule Job.");
			        }
			        return write(retobj.toString());
			 }
		 
			 
}
