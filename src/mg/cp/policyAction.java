package mg.cp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class policyAction extends BaseAction{

	/**
     * 반출신청 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("selectExcPolicy")) {   /** CP 예외정책 관리 조회 */
            return selectExcPolicy(request);                                             
        } else  if(request.isMode("saveExcPolicy")) {   /** CP 예외정책 등록 */
            return saveExcPolicy(request);                                             
        } else if(request.isMode("deleteExcPolicy")) {   /** CP 예외정책 삭제 */
            return deleteExcPolicy(request);                                             
        } else if(request.isMode("selectAlloFolder")) {   /** CP 예외정책 허용폴더 조회 */
            return selectAlloFolder(request);                                             
        } else  if(request.isMode("saveAlloFolder")) {   /** CP 예외정책 허용폴더등록 */
            return saveAlloFolder(request);                                             
        } else if(request.isMode("deleteAlloFolder")) {   /** CP 예외정책 허용폴더 삭제 */
            return deleteAlloFolder(request);                                             
        } else if(request.isMode("selectRestFolder")) {   /** CP 예외정책 제한폴더 조회 */
            return selectRestFolder(request);                                              
        } else  if(request.isMode("saveRestFolder")) {   /** CP 예외정책 제한폴더등록 */
            return saveRestFolder(request);                                             
        } else if(request.isMode("deleteRestFolder")) {   /** CP 예외정책 제한폴더 삭제 */
            return deleteRestFolder(request);                                             
        } 
        
        if(request.isMode("selectExcPolicyJson")) {   /** CP 예외정책 관리 조회 */
            return selectExcPolicyJson(request);                                             
        } else  if(request.isMode("saveExcPolicyJson")) {   /** CP 예외정책 등록 */
            return saveExcPolicyJson(request);                                             
        } else if(request.isMode("deleteExcPolicyJson")) {   /** CP 예외정책 삭제 */
            return deleteExcPolicyJson(request);                                             
        } else if(request.isMode("selectAlloFolderJson")) {   /** CP 예외정책 허용폴더 조회 */
            return selectAlloFolderJson(request);                                             
        } else  if(request.isMode("saveAlloFolderJson")) {   /** CP 예외정책 허용폴더등록 */
            return saveAlloFolderJson(request);                                             
        } else if(request.isMode("deleteAlloFolderJson")) {   /** CP 예외정책 허용폴더 삭제 */
            return deleteAlloFolderJson(request);                                             
        } else if(request.isMode("selectRestFolderJson")) {   /** CP 예외정책 제한폴더 조회 */
            return selectRestFolderJson(request);                                             
        } else  if(request.isMode("saveRestFolderJson")) {   /** CP 예외정책 제한폴더등록 */
            return saveRestFolderJson(request);                                             
        } else if(request.isMode("deleteRestFolderJson")) {   /** CP 예외정책 제한폴더 삭제 */
            return deleteRestFolderJson(request);                                             
        } 
        else { 
            return write(null);
        }
    } 
    
    /**
     * CP 예외정책 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectExcPolicy(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
       	
       	  try {
 	         Map smap = request.getMap();
 	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 	         smap.put("excValue", "E%");
 	         
 	         List lists = getList("mg.cp.selectExcPolicy",smap);
 	         for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	 
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	         }
 	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 log.error("---selectExcPolicy-----------------------------------------------------------\n" + e.toString());
          	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
     }
           
	  /**
	   * CP 예외정책 저장 
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  private IResponse saveExcPolicy(IRequest request) throws Exception 
	  {
	      String ids_info = request.getStringParam("ids");
	      String cols_ids = request.getStringParam("col_ids");
	      String[] cols = cols_ids.split(",");
	      String[] ids = ids_info.split(",");
	      String msg="", flag="";
	 
	      try {
	      	tx.begin();
	      	
	      	 Map item = null;
	         for (int i = 0; i < ids.length; i++) {
	        	 item = new HashMap();
	        	 
	        	 for(int j=0; j < cols.length; j++ ) {
	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	          	}
	        	item.put("LOGIN_ID",  request.getUser().getId() );
	        	
	        	if(item.get("EXC_WAITING_TM") == null || "".equals(item.get("EXC_WAITING_TM").toString()) ) {
	        		item.put("EXC_WAITING_TM", 0);
	        	}
	       	    
	        	String dbName = pm.getString("component.sql.database");
	            // CP 예외정책 Master 등록
	            if( Integer.parseInt(getItem("mg.cp.countExcPolicy", item).toString() ) > 0 ) {
	           		updateObject("mg.cp.updateExcPolicy"+getDBName(pm.getString("component.sql.database")), item);
	           	} else {
	           		createObject("mg.cp.insertExcPolicy"+getDBName(dbName), item); 
	           	}
	         } 	
	         tx.commit();
	        
	         msg = "COMG_1002";      // mm.getMessage("CPMG_1004", request.getUser().getLocale());
	         flag = "true";
	      } catch (Exception e) {
	      	tx.rollback();
	      	log.error("------------saveExcPolicy---------------------------------------------------\n" + e.toString());
	      	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
	      }
	      String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
	      return writeXml(ret);
	  }
 
	  /*
	   * CP 예외정책 삭제
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  private IResponse deleteExcPolicy(IRequest request) throws Exception 
	  {
	      String ids_info = request.getStringParam("ids");
	      String cols_ids = request.getStringParam("col_ids");
	      String[] cols = cols_ids.split(",");
	      String[] ids = ids_info.split(",");
	      String msg="", flag="";
	       
	      try {
	      	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	    item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	
	       	 	// 허용폴더 삭제
	       	 	deleteObject("mg.cp.deleteAlloFolder", item); 
	       	 	// 제한폴더 삭제
	       	 	deleteObject("mg.cp.deleteRestFolder", item); 
	       	 	// 마스터 삭제
	       	 	deleteObject("mg.cp.deleteExcPolicy", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
	      } catch (Exception e) {
	      	tx.rollback();
	      	log.error("------------deleteExcPolicy-------------------------------------\n" + e.toString());
	      	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
	      }
	      
	      String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	      return writeXml(ret);
	  }
  
	  /**
     * CP 예외정책 허용폴더 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectAlloFolder(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
       	
       	  try {
 	         Map smap = request.getMap();
 	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 	         
 	         List lists = getList("mg.cp.selectAlloFolder",smap);
 	         for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	 
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	         }
 	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 log.error("---selectExcPolicy-----------------------------------------------------------\n" + e.toString());
          	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
     }
           
	  /**
	   * CP 예외정책 허용폴더 저장 
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  private IResponse saveAlloFolder(IRequest request) throws Exception 
	  {
	      String ids_info = request.getStringParam("ids");
	      String cols_ids = request.getStringParam("col_ids");
	      String[] cols = cols_ids.split(",");
	      String[] ids = ids_info.split(",");
	      String msg="", flag="";
	 
	      try {
	      	tx.begin();
	      	
	      	 Map item = null;
	         for (int i = 0; i < ids.length; i++) {
	        	 item = new HashMap();
	        	 
	        	 for(int j=0; j < cols.length; j++ ) {
	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	          	}
	        	item.put("LOGIN_ID",  request.getUser().getId());
	       	    
	        	if(item.get("FOLDER_NO") == null || "".equals(item.get("FOLDER_NO").toString()) ) {
	        		item.put("FOLDER_NO", 0);
	        	}
	            // CP 예외정책 Master 등록
	            if( Integer.parseInt(getItem("mg.cp.countAlloFolder", item).toString() ) > 0 ) {
	           		updateObject("mg.cp.updateAlloFolder", item);
	           	} else {
	           		item.put("dbName", pm.getString("component.sql.database"));
	           		createObject("mg.cp.insertAlloFolder"+getDBName(pm.getString("component.sql.database")), item); 
	           	}
	         } 	
	         tx.commit();
	        
	         msg = "COMG_1002";      // mm.getMessage("CPMG_1004", request.getUser().getLocale());
	         flag = "true";
	      } catch (Exception e) {
	      	tx.rollback();
	      	log.error("------------saveAlloFolder---------------------------------------------------\n" + e.toString());
	      	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
	      }
	      String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
	      return writeXml(ret);
	  }
 
	  /*
	   * CP 예외정책 허용폴더 삭제
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  private IResponse deleteAlloFolder(IRequest request) throws Exception 
	  {
	      String ids_info = request.getStringParam("ids");
	      String cols_ids = request.getStringParam("col_ids");
	      String[] cols = cols_ids.split(",");
	      String[] ids = ids_info.split(",");
	      String msg="", flag="";
	       
	      try {
	      	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	    item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	
	       	 	// 허용폴더 삭제
	       	 	deleteObject("mg.cp.deleteAlloFolder", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
	      } catch (Exception e) {
	      	tx.rollback();
	      	log.error("------------deleteAlloFolder-------------------------------------\n" + e.toString());
	      	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
	      }
	      
	      String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	      return writeXml(ret);
	  }
   
	  
	  /**
     * CP 예외정책 제한폴더 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectRestFolder(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
       	
       	  try {
 	         Map smap = request.getMap();
 	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 	         
 	         List lists = getList("mg.cp.selectRestFolder",smap);
 	         for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	 
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	         }
 	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 log.error("---selectExcPolicy-----------------------------------------------------------\n" + e.toString());
          	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
     }
	  
	  /**
	   * CP 예외정책 제한폴더 저장 
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  private IResponse saveRestFolder(IRequest request) throws Exception 
	  {
	      String ids_info = request.getStringParam("ids");
	      String cols_ids = request.getStringParam("col_ids");
	      String[] cols = cols_ids.split(",");
	      String[] ids = ids_info.split(",");
	      String msg="", flag="";
	 
	      try {
	      	tx.begin();
	      	
	      	 Map item = null;
	         for (int i = 0; i < ids.length; i++) {
	        	 item = new HashMap();
	        	 
	        	 for(int j=0; j < cols.length; j++ ) {
	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	          	}
	        	item.put("LOGIN_ID",  request.getUser().getId() );
	       	    
	        	if(item.get("FOLDER_NO") == null || "".equals(item.get("FOLDER_NO").toString()) ) {
	        		item.put("FOLDER_NO", 0);
	        	}
	        	
	            // CP 예외정책 Master 등록
	            if( Integer.parseInt(getItem("mg.cp.countRestFolder", item).toString() ) > 0 ) {
	           		updateObject("mg.cp.updateRestFolder", item);
	           	} else {
	           		item.put("dbName", pm.getString("component.sql.database"));
	           		createObject("mg.cp.insertRestFolder"+getDBName(pm.getString("component.sql.database")), item); 
	           	}
	         } 	
	         tx.commit();
	        
	         msg = "COMG_1002";      // mm.getMessage("CPMG_1004", request.getUser().getLocale());
	         flag = "true";
	      } catch (Exception e) {
	      	tx.rollback();
	      	log.error("------------saveRestFolder---------------------------------------------------\n" + e.toString());
	      	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
	      }
	      String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
	      return writeXml(ret);
	  }
 
	  /*
	   * CP 예외정책 제한폴더 삭제
	   * @param request
	   * @return
	   * @throws Exception
	   */
	  private IResponse deleteRestFolder(IRequest request) throws Exception 
	  {
	      String ids_info = request.getStringParam("ids");
	      String cols_ids = request.getStringParam("col_ids");
	      String[] cols = cols_ids.split(",");
	      String[] ids = ids_info.split(",");
	      String msg="", flag="";
	       
	      try {
	      	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	    item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	// 제한폴더 삭제
	       	 	deleteObject("mg.cp.deleteRestFolder", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
	      } catch (Exception e) {
	      	tx.rollback();
	      	log.error("------------deleteRestFolder-------------------------------------\n" + e.toString());
	      	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
	      }
	      
	      String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	      return writeXml(ret);
	  }
	  
	  //---------------------------------------------------------------------------------------------------------------------------
	  
	  /**
	     * CP 예외정책 조회 
	     * @param request
	     * @return
	     * @throws Exception
	     */
	     private IResponse selectExcPolicyJson(IRequest request) throws Exception 
	     {
	    	 JSONObject obj = new  JSONObject();
	    	 JSONArray jArray = new JSONArray();
	       	  String col = request.getStringParam("grid_col_id");
	       	  String[] cols = col.split(",");
	       	  
	       	  Map map = null;
	           GridData gdRes = new GridData();
	       	
	       	  try {
	 	         Map smap = request.getMap();
	 	       //  smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	 	        smap.put("LOCALE", "ko");
	 	         smap.put("excValue", "E%");
	 	        JSONObject listdata;
	 	         List lists = getList("mg.cp.selectExcPolicy",smap);
	 	         for (int i = 0; i < lists.size(); i++) {
	 	          	map =(Map) lists.get(i);
	 		       	listdata = new JSONObject();
	 		       	listdata.put("ROW_ID", String.valueOf(i+1));
	 	          	for(int j=0; j < cols.length; j++ ) {
	 	          		//gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	 	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	 	          	}
	 	           jArray.add(listdata);
	 	         }
	 	        obj.put("data", jArray);
				 obj.put("errmsg", "success");
				 obj.put("errcode", "0");
				 return write(obj.toString());
	 	     } catch(Exception e) {
	 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
	 			 obj.put("errcode", "-1");
	 	    	 return write(obj.toString());
	 	     }
	     }
	           
		  /**
		   * CP 예외정책 저장 
		   * @param request
		   * @return
		   * @throws Exception
		   */
		  private IResponse saveExcPolicyJson(IRequest request) throws Exception 
		  {
			  JSONObject obj = new  JSONObject();
		    	JSONArray jArray = new JSONArray();
			  String ids_info = request.getStringParam("ids");
		      String cols_ids = request.getStringParam("col_ids");
		      String[] cols = cols_ids.split(",");
		      String[] ids = ids_info.split(",");
		      String msg="", flag="";
		 
		      try {
		    	  Map item = null;
		      	 JSONObject listdata = new  JSONObject();
		         for (int i = 0; i < ids.length; i++) {
		        	 item = new HashMap();
		        	 listdata = new JSONObject();
		        	 listdata.put("ROW_ID", String.valueOf(i+1));
		        	 
		        	 for(int j=0; j < cols.length; j++ ) {
		        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		 	             item.put(cols[j], tmps);     
		 	            listdata.put(cols[j], tmps);     
		          	}
		        	 
		        	item.put("LOGIN_ID",  request.getUser().getId());
		        	
		        	if(null != item.get("EXC_WAITING_TM")) item.put("EXC_WAITING_TM", 0);
		      

		            if( Integer.parseInt(getItem("mg.cp.countExcPolicy", item).toString() ) > 0 ) {
		           		updateObject("mg.cp.updateExcPolicy"+getDBName(pm.getString("component.sql.database")), item);
		           	} else {
		           		createObject("mg.cp.insertExcPolicy"+getDBName(pm.getString("component.sql.database")), item); 
		           	}
		       	      jArray.add(listdata);
			        }
			         obj.put("data", jArray);
					 obj.put("errmsg", "success");
					 obj.put("errcode", "0");
					  log.info("------saveExcPolicy-=----result------ > " + obj.toString());
					 return write(obj.toString());
			     } catch(Exception e) {
			    	 log.error("---저장된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
					 obj.put("errmsg", mm.getMessage("COMG_1008", request.getUser().getLocale()));
					 obj.put("errcode", "-1");
			    	 return write(obj.toString());
			     }	
		    }
		  /*
		   * CP 예외정책 삭제
		   * @param request
		   * @return
		   * @throws Exception
		   */
		  private IResponse deleteExcPolicyJson(IRequest request) throws Exception 
		  {
			  JSONObject obj = new  JSONObject();
		    	 JSONArray jArray = new JSONArray();
		      String ids_info = request.getStringParam("ids");
		      String cols_ids = request.getStringParam("col_ids");
		      String[] cols = cols_ids.split(",");
		      String[] ids = ids_info.split(",");
		      String msg="", flag="";
		       
		      try {
		   //   	tx.begin();
		        Map item = null;	 
		        JSONObject listdata ;
		        System.out.println(obj.toString()+"d");
		        for (int i = 0; i < ids.length; i++) {
		       	    item = new HashMap();
		        	 listdata = new JSONObject();
		        	 listdata.put("ROW_ID", String.valueOf(i+1));
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
			            listdata.put(cols[j], tmps);     
		         	} 
		       	 	// 허용폴더 삭제
		       	 	deleteObject("mg.cp.deleteAlloFolder", item); 
		       	 	// 제한폴더 삭제
		       	 	deleteObject("mg.cp.deleteRestFolder", item); 
		       	 	// 마스터 삭제
		       	 	deleteObject("mg.cp.deleteExcPolicy", item); 
		       	 jArray.add(listdata);
		        }
		         obj.put("data", jArray);
				 obj.put("errmsg", "success");
				 obj.put("errcode", "0");
				 System.out.println(obj.toString()+"d");
				  log.info("------deleteExcPolicy-=----result------ > " + obj.toString());
				 return write(obj.toString());
		     } catch(Exception e) {
		    	 System.out.println(obj.toString()+"d");
		    	 log.error("---삭제된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
				 obj.put("errmsg", mm.getMessage("COMG_1008", request.getUser().getLocale()));
				 obj.put("errcode", "-1");
		    	 return write(obj.toString());
		     }	
	}
	  
		  /**
		     * CP 예외정책 허용폴더 조회 
		     * @param request
		     * @return
		     * @throws Exception
		     */
		     private IResponse selectAlloFolderJson(IRequest request) throws Exception 
		     {
		    	 JSONObject obj = new JSONObject();
		    	 JSONArray jArray = new JSONArray();
		       	  String col = request.getStringParam("grid_col_id");
		       	  String[] cols = col.split(",");
		       	  
		       	  Map map = null;
		        //   GridData gdRes = new GridData();
		       	
		       	  try {
		 	         Map smap = request.getMap();
		 	       //  smap.put("LOCALE", request.getUser().getLocale().getLanguage());
		 	        smap.put("LOCALE", "ko");
		 	        JSONObject listdata;
		 	         List lists = getList("mg.cp.selectAlloFolderJson",smap);
		 	         for (int i = 0; i < lists.size(); i++) {
		 	          	map =(Map) lists.get(i);
		 	          	listdata = new JSONObject();
		 	          	listdata.put("ROW_ID", String.valueOf(i+1));
		 	          	for(int j=0; j < cols.length; j++ ) {
		 	          		//gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
		 	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]));     
		 	          	}jArray.add(listdata);
		 	         }
		 	          obj.put("data", jArray);
					 obj.put("errmsg", "success");
					 obj.put("errcode", "0");
					 return write(obj.toString());
		 	     } catch(Exception e) {
		 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
		 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		 			 obj.put("errcode", "-1");
		 	    	 return write(obj.toString());
		 	     }
		     }
		           
			  /**
			   * CP 예외정책 허용폴더 저장 
			   * @param request
			   * @return
			   * @throws Exception
			   */
			  private IResponse saveAlloFolderJson(IRequest request) throws Exception 
			  {
				  JSONObject obj = new JSONObject();
				  JSONArray jArray = new JSONArray();
			      String ids_info = request.getStringParam("ids");
			      String EXC_NO_info = request.getStringParam("EXC_NO");
			      String cols_ids = request.getStringParam("col_ids");
			      String[] cols = cols_ids.split(",");
			      String[] ids = ids_info.split(",");
			      String msg="", flag="";
			 
			      try {
			      //	tx.begin();
			      	JSONObject listdata;
			      	 Map item = null;
			      	System.out.println("어디까지 들어오니?1");
			         for (int i = 0; i < ids.length; i++) {
			        	 item = new HashMap();
			        	 listdata = new JSONObject();
			        	 listdata.put("ROW_ID", String.valueOf(i+1));
			        	 
			        	 if(item.get("EXC_NO") == null || "".equals(item.get("EXC_NO").toString()) ) {
				        		item.put("EXC_NO", EXC_NO_info);
				        	}
			        	 
//			        	 item.put("EXC_NO", EXC_NO_info);
//			        	 listdata.put("EXC_NO", EXC_NO_info);
			        	 System.out.println("어디까지 들어오니?2");
			        	 for(int j=0; j < cols.length; j++ ) {
			        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			 	             item.put(cols[j], tmps);     
			 	            listdata.put(cols[j], tmps);     
			 	   
			          	}
			        	//item.put("LOGIN_ID",  request.getUser().getId());
			        	item.put("LOGIN_ID",  "admin");
			       	    
			        	if(item.get("FOLDER_NO") == null || "".equals(item.get("FOLDER_NO").toString()) ) {
			        		item.put("FOLDER_NO", 0);
			        	}
			        	
			        	System.out.println("어디까지 들어오니?3");
			     
			            // CP 예외정책 Master 등록
			            if( Integer.parseInt(getItem("mg.cp.countAlloFolder", item).toString() ) > 0 ) {
			           		updateObject("mg.cp.updateAlloFolder", item);
			           	} else {
			           		item.put("dbName", pm.getString("component.sql.database"));
			           		createObject("mg.cp.insertAlloFolder"+getDBName(pm.getString("component.sql.database")), item); 
			           	}
			        	jArray.add(listdata);
		 	         }
		 	          obj.put("data", jArray);
					 obj.put("errmsg", "success");
					 obj.put("errcode", "0");
					 log.info("---saveAlloFolderJson-----------------------------------------------------------\n" + obj.toString());
					 return write(obj.toString());
		 	     } catch(Exception e) {
		 	    	 log.error("---저장된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
		 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		 			 obj.put("errcode", "-1");
		 	    	 return write(obj.toString());
		 	     }
			  }
		 
			  /*
			   * CP 예외정책 허용폴더 삭제
			   * @param request
			   * @return
			   * @throws Exception
			   */
			  private IResponse deleteAlloFolderJson(IRequest request) throws Exception 
			  {
				  JSONObject obj = new JSONObject();
				  JSONArray jArray = new JSONArray();
			      String ids_info = request.getStringParam("ids");
			      String cols_ids = request.getStringParam("col_ids");
			      String[] cols = cols_ids.split(",");
			      String[] ids = ids_info.split(",");
			      String msg="", flag="";
			       
			      try {
			    	  JSONObject listdata;
			        Map item = null;
			        for (int i = 0; i < ids.length; i++) {
			       	    item = new HashMap();
			       	 listdata = new JSONObject();
			       	 listdata.put("ROW_ID", String.valueOf(i+1));
			       	 	for(int j=0; j < cols.length; j++ ) {
			       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
				            item.put(cols[j], tmps);     
				            listdata.put(cols[j], tmps);     
			         	} 
			       	 	
			       	 	// 허용폴더 삭제
			       	 	deleteObject("mg.cp.deleteAlloFolder", item); 
			       	     jArray.add(listdata);
			        }
			    	
		 	          obj.put("data", jArray);
					 obj.put("errmsg", "success");
					 obj.put("errcode", "0");
					 log.info("---deleteAlloFolderJson-----------------------------------------------------------\n" + obj.toString());
					 return write(obj.toString());
		 	     } catch(Exception e) {
		 	    	 log.error("---삭제된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
		 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		 			 obj.put("errcode", "-1");
		 			 return write(obj.toString());
			  }
			}
		   
			  
			  /**
		     * CP 예외정책 제한폴더 조회 
		     * @param request
		     * @return
		     * @throws Exception
		     */
		     private IResponse selectRestFolderJson(IRequest request) throws Exception 
		     {
		    	 JSONObject obj = new JSONObject();
		    	 JSONArray jArray = new JSONArray();
		       	  String col = request.getStringParam("grid_col_id");
		       	  String[] cols = col.split(",");
		       	  
		       	  Map map = null;
		           GridData gdRes = new GridData();
		       	
		       	  try {
		       		 JSONObject listdata;
		 	         Map smap = request.getMap();
		 	       //  smap.put("LOCALE", request.getUser().getLocale().getLanguage());
		 	        smap.put("LOCALE", "ko");
		 	         List lists = getList("mg.cp.selectRestFolderJson",smap);
		 	         for (int i = 0; i < lists.size(); i++) {
		 	          	map =(Map) lists.get(i);
		 	          	listdata = new JSONObject();
		 	          	listdata.put("ROW_ID", String.valueOf(i+1));
		 	          	for(int j=0; j < cols.length; j++ ) {
		 	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
		 	          	}
		 	      	 jArray.add(listdata);
		 	        }
		 	        obj.put("data", jArray);
					 obj.put("errmsg", "success");
					 obj.put("errcode", "0");
					 return write(obj.toString());
		 	     } catch(Exception e) {
		 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
		 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		 			 obj.put("errcode", "-1");
		 	    	 return write(obj.toString());
		 	     }
		   }
			  
			  /**
			   * CP 예외정책 제한폴더 저장 
			   * @param request
			   * @return
			   * @throws Exception
			   */
			  private IResponse saveRestFolderJson(IRequest request) throws Exception 
			  {
				  JSONObject obj = new JSONObject();
			    	 JSONArray jArray = new JSONArray();
			      String ids_info = request.getStringParam("ids");
			      String cols_ids = request.getStringParam("col_ids");
			      String[] cols = cols_ids.split(",");
			      String[] ids = ids_info.split(",");
			      String msg="", flag="";
			 
			      try {
			      	//tx.begin();
			      	JSONObject listdata;
			      	 Map item = null;
			         for (int i = 0; i < ids.length; i++) {
			        	 item = new HashMap();
			        		listdata = new JSONObject();
			 	          	listdata.put("ROW_ID", String.valueOf(i+1));
			        	 for(int j=0; j < cols.length; j++ ) {
			        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			 	             item.put(cols[j], tmps);     
			 	            listdata.put(cols[j], tmps);    
			          	}
//			        	item.put("LOGIN_ID",  request.getUser().getId() );
			        	 item.put("LOGIN_ID",  "admin" );
			        	if(item.get("FOLDER_NO") == null || "".equals(item.get("FOLDER_NO").toString()) ) {
			        		item.put("FOLDER_NO", 0);
			        	}
			        	
			            // CP 예외정책 Master 등록
			            if( Integer.parseInt(getItem("mg.cp.countRestFolder", item).toString() ) > 0 ) {
			           		updateObject("mg.cp.updateRestFolder", item);
			           	} else {
			           		item.put("dbName", pm.getString("component.sql.database"));
			           		createObject("mg.cp.insertRestFolder"+getDBName(pm.getString("component.sql.database")), item); 
			           	}
			            jArray.add(listdata);
			 	        }
			 	        obj.put("data", jArray);
						 obj.put("errmsg", "success");
						 obj.put("errcode", "0");
						 return write(obj.toString());
			 	     } catch(Exception e) {
			 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
			 			 obj.put("errcode", "-1");
			 	    	 return write(obj.toString());
			 	     }
			  }
		 
			  /*
			   * CP 예외정책 제한폴더 삭제
			   * @param request
			   * @return
			   * @throws Exception
			   */
			  private IResponse deleteRestFolderJson(IRequest request) throws Exception 
			  {
				  JSONObject obj = new JSONObject();
			    	 JSONArray jArray = new JSONArray();
			      String ids_info = request.getStringParam("ids");
			      String cols_ids = request.getStringParam("col_ids");
			      String[] cols = cols_ids.split(",");
			      String[] ids = ids_info.split(",");
			       
			      try {
			    	JSONObject listdata;
			        Map item = null;
			        for (int i = 0; i < ids.length; i++) {
			       	    item = new HashMap();
			       		listdata = new JSONObject();
		 	          	listdata.put("ROW_ID", String.valueOf(i+1));
			       	 	for(int j=0; j < cols.length; j++ ) {
			       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
				            item.put(cols[j], tmps);     
				            listdata.put(cols[j], tmps);   
			         	} 
			       	 	// 제한폴더 삭제
			       	 	deleteObject("mg.cp.deleteRestFolder", item); 
			       	 jArray.add(listdata);
		 	        }
		 	        obj.put("data", jArray);
					 obj.put("errmsg", "success");
					 obj.put("errcode", "0");
					 return write(obj.toString());
		 	     } catch(Exception e) {
		 	    	 log.error("---삭제된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
		 			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
		 			 obj.put("errcode", "-1");
		 	    	 return write(obj.toString());
		 	     }
			 }
		
}
