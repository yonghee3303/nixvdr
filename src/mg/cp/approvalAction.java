package mg.cp;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.io.File;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.InvalidNameException;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.compress.ICompressManagement;
import com.core.component.crypto.ICryptoManagement;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.base.ComponentRegistry;
import com.core.component.util.DateUtils;
import com.core.component.util.FileUtils;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.SeedEncryptUtil;
import mg.base.ZipFileUtil;
import mg.base.cryptorSHA;
import mg.vf.AgentAction;
import mg.site.IljinInterlock;
//상신브레이크 결재연동
import mg.site.SangsinInterlock;
//LG화학 결재연동
import mg.site.LgChemInterlock;

//LG전자 인증을 위한 
import mg.inter.EncrytPw;
/**
* 업무 그룹명 : mg.cp
* 서브 업무명 : approvalAction.java
* 작성자 : cyLeem
* 작성일 : 2014. 04. 03 
* 설 명 : approvalAction
* 최종 수정일 : 2016. 03. 16
* 변경 내용 : 일진글로벌 반출 신청시 결재연동 처리 (saveRequest)
*/ 
public class approvalAction extends BaseAction{

	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	private String ERR_Message;
	// protected IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	/**
     * 반출신청 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("selectDocList")) {   /** 반출신청 현황조회 */
            return selectDocList(request);                           
        } else if(request.isMode("getCpDoc")) { 
            return getCpDoc(request);                           
        } else if(request.isMode("saveRequest")) {  /* 반출신청 등록 */
            return saveRequest(request);                           
        } else if(request.isMode("changePolicy")) {  /* 반출문서 정책 수정 */
            return changePolicy(request);                           
        } else if(request.isMode("changePolicySingle")){
        	return changePolicySingle(request);
        }else if(request.isMode("deleteDocUser")) { 
            return deleteDocUser(request);                           
        } else if(request.isMode("selectDocUser")) { // 수신자 목록 조회
            return selectDocUser(request);                           
        } else if(request.isMode("deleteDoc")) { 
            return deleteDocMaster(request);                           
        } else if(request.isMode("getCpDocInfo")) { 
            return getCpDocInfo(request);                           
        } else if(request.isMode("selectApproval")) { // 승인 대상 목록 조회
            return selectApproval(request);                           
        }  else if(request.isMode("selectDocAdmin")) { // 반출 내역 조회 (관리자 반출관리
            return selectDocAdmin(request);                           
        }  else if(request.isMode("selectDocAdminAll")) { // 반출 내역 조회 - 편집저장 파일까지 모두 조회 
            return selectDocAdminAll(request);                           
        }else if(request.isMode("excelDocAdmin")) { // 반출 내역 Excel Download(관리자 반출관리
            return excelDocAdmin(request);                           
        } else if(request.isMode("saveApproval")) { // 반출문서 승인
            return saveApproval(request);                           
        } else if(request.isMode("singleApproval")) { // 반출문서 승인 - 단일건
            return singleApproval(request);                           
        } else if(request.isMode("selectDocLog")) { // 반출 문서 로그 조회
            return selectDocLog(request);                           
        } else if(request.isMode("excelDocLog")) { // 반출 문서 로그  excelDownload
            return excelDocLog(request);                           
        } else if(request.isMode("authUser")) { // 반출 문서 열람인증
            return authUser(request);                           
        } else if(request.isMode("repacking")) { // Repacking 시도
            return repackingDoc(request);                           
        } else if(request.isMode("saveDocLog")) { // CP 실행 로그 저장
            return saveDocLog(request);                           
        } else if(request.isMode("exeDownList")) { // 반출파일 다운로드
            return exeDownList(request);                           
        } else if(request.isMode("exeDownList_CPCOM")) { // 반출파일 다운로드
            return exeDownList_CPCOM(request);                           
        } else if(request.isMode("saveRepScheduel")){ //모든 파일을 업로드하고 적용을 누르면 스케줄로 등록
        	return saveRepSchedule(request);
        } else if(request.isMode("down")) { 
            return exeDownload(request);                           
        } else if(request.isMode("testDate")) { 
            return testDate(request);                           
        } else if(request.isMode("selectRepackingList")){ //CP Community 사이트 - 리패킹목록 리스트 가져오기
        	return getRepackingList(request);
        } else if(request.isMode("selectDetailDocInfo")){
        	return getDetailDocInfo(request);
        } else if(request.isMode("cntDocList")){ // CP Community 메인 페이지 수신문서 개수, 업로드 대기 개수 가져오기
        	return cntDocList(request);
        } else if(request.isMode("selectRepList")){ // 리패킹 목록 가져오기
        	return selectRepList(request);
        } else if(request.isMode("getUserApprover")){ //승인자 ID를 얻어온다. 
        	return getUserApprover(request);
        } else if(request.isMode("getPolicyGrp")){ //CP_POLICY_GRP에서 정책내용을 얻어온다.
        	return getPolicyGrp(request);
        } else if(request.isMode("selectApprovalFinishList")){ //승인완료 목록 가져오기
        	return selectApprovalFinishList(request);
        } else if(request.isMode("selectDocNoRead")){ //수신자가 읽지않은 목록을 조회
        	return selectDocNoRead(request);
        } else if(request.isMode("selectRequestFinishList")){
        	return selectRequestFinishList(request); //승인자가 승인한 모든 목록
        } else if (request.isMode("getUserByProperties")) {
			return getUserByProperties(request); // 사용자 정보 취득
		} else if (request.isMode("getToken")) {
			return getToken(request); // 사용자 토큰 정보 취득 (일진 글로벌 전용)
		} else if (request.isMode("getComboGrpHtml")) {
			return getComboGrpHtml(request); // 선택적 검색을 위한 SELECT 태그 호출
		} else if (request.isMode("selectDocStats")) {
			return getDocStats(request); // 사용자별 반출 건수 조회 
		} else if(request.isMode("excelDocStats")) { // 사용자별 반출 건수 Excel Download
            return excelDocStats(request);                           
        }else if(request.isMode("setCpDocSatate")){
        	return setCpDocSatate(request);
        }
		else { 	
            return write(null);
        }
    } 
    
    private IResponse setCpDocSatate(IRequest request) {
    	JSONObject result = new JSONObject();
    	try {
	   		 //LG화학에서 exe 재 생성할 수 있도록 state 변경 요청들어옴. 
	    	String dbName = pm.getString("component.sql.database");
	
	    	tx.begin();
	      	updateObject("mg.cp.updateSTS"+getDBName(dbName), request.getMap());
	      	tx.commit();
	      	result.put("errmsg", "정상적으로 변경되었습니다.");
	   		result.put("errcode", "0");
	   		return write(result.toString());
        } catch(Exception e) {
        	tx.rollback();
        	result.put("errmsg", "상태값 변경 중 오류가 발생하였습니다.");
       		result.put("errcode", "-1");
      	 	return write(result.toString());
        }
	}

	private ICompressManagement compressHandle = (ICompressManagement) ComponentRegistry.lookup(ICompressManagement.class, "com.core.component.ICompressManagement");
    
    private IResponse selectRequestFinishList(IRequest request) {
		// TODO Auto-generated method stub
    	String col = request.getStringParam("grid_col_id");
   	  	String[] cols = col.split(",");
   	  
   	  	Map map = null;
       GridData gdRes = new GridData();
   	
   	  try {
	         Map smap = request.getMap();
	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	        
	       	 String dbName = pm.getString("component.sql.database");
	       	 List lists = getList("mg.cp.selectRequestFinishList"+getDBName(dbName),smap);
	
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
       } catch(Exception e) {
      	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
      	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
       }
	}


	private IResponse selectDocNoRead(IRequest request) {
		// TODO Auto-generated method stub
    	String col = request.getStringParam("grid_col_id");
   	  String[] cols = col.split(",");
   	  
   	  Map map = null;
       GridData gdRes = new GridData();
   	
   	  try {
	         Map smap = request.getMap();
	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	         
	       	 String dbName = pm.getString("component.sql.database");
	       	 List lists;
	       	 
	       	 if("ILJIN".equals(pm.getString("component.site.company"))) {
	       		lists = getList("mg.site.selectDocNoRead"+getDBName(dbName),smap);	//일진글로벌용 컬럼 추가
	       	 } else {
	       		lists = getList("mg.cp.selectDocNoRead"+getDBName(dbName) ,smap);
	       	 }
	  
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
       } catch(Exception e) {
      	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
      	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
       }
	}


	private IResponse selectApprovalFinishList(IRequest request) {
		// TODO Auto-generated method stub
    	String col = request.getStringParam("grid_col_id");
     	  String[] cols = col.split(",");
     	  
     	  Map map = null;
         GridData gdRes = new GridData();
     	
     	  try {
	         Map smap = request.getMap();
	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	         
	       	 String dbName = pm.getString("component.sql.database");
	       	 List lists = getList("mg.cp.selectApprovalFinishList"+getDBName(dbName),smap);
	       
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	 
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
	}


	private IResponse getPolicyGrp(IRequest request) {
		// TODO Auto-generated method stub
		try {
	       	Map map = request.getMap();
	       	map.put("dbName", pm.getString("component.sql.database"));
	       	
	       	String dbName = pm.getString("component.sql.database");
	       	
	       	log.info("---mapmapmapmap-------------------------\n" + map.toString());
	       	
	       	Map item  = (Map)getItem("mg.cp.getPolicyGrp"+getDBName(dbName), map);
   		
	       	// Creation of Json Response 
	       	JSONObject obj = new JSONObject(); 
	       	   	
	       	obj.put("POLICY", this.getStringWithNullCheck(item, "POLICY"));
	       	obj.put("POLICY_DOC", this.getStringWithNullCheck(item, "POLICY_DOC"));
	       	obj.put("POLICY_DISP", this.getStringWithNullCheck(item, "POLICY_DISP"));
	       	obj.put("EXPIRE_DATE", this.getStringWithNullCheck(item, "EXPIRE_DATE"));
	       	obj.put("READ_COUNT", this.getStringWithNullCheck(item, "READ_COUNT"));
	       	obj.put("IS_CHANGE", this.getStringWithNullCheck(item, "IS_CHANGE"));
	       	obj.put("POLICY_NM", this.getStringWithNullCheck(item, "POLICY_NM"));
	       	obj.put("WATERMARK_YN", this.getStringWithNullCheck(item, "WATERMARK_YN"));
	       	obj.put("VDI_YN", this.getStringWithNullCheck(item, "VDI_YN"));
	       	obj.put("OUTDEVICE", this.getStringWithNullCheck(item, "OUTDEVICE"));
	     	obj.put("MAC_YN", this.getStringWithNullCheck(item, "MAC_YN"));
	    	obj.put("POLICY_CL_CD", this.getStringWithNullCheck(item, "POLICY_CL_CD"));
	    	obj.put("USEREDIT_CD", this.getStringWithNullCheck(item, "USEREDIT_CD"));
	    	
	       	//log.info("--------------------------------------"+obj.toString());
	       	return write(obj.toString());
        } catch(Exception e) {
        	log.error("---getPolicyGrp-------------------------\n" + e.toString());
      	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
        }
	}


	private IResponse getUserApprover(IRequest request) {
		// TODO Auto-generated method stub
    	try {	
	       	String dbName = pm.getString("component.sql.database");
	       	Map map;//  = (Map)getItem("mg.cp.getApproverId", request.getMap());
	       	JSONObject obj = new JSONObject();
	       	List lists = getList("mg.cp.getApproverId",request.getMap()); //다중결재선때문에 수정, 하지만 우선순위 컬럼이 테이블에 추가될 필요가 있을 듯....
	       	// Creation of Json Response 
	         for (int i = 0; i < lists.size(); i++) {
	        	 map =(Map) lists.get(i); 
	        	 obj.put("APPROVER_ID", this.getStringWithNullCheck(map, "APPROVER_ID"));
	        	 obj.put("APPROVER_NM", this.getStringWithNullCheck(map, "APPROVER_NM"));
	        	 
	         }
	       	 log.info("getUserApprover--------------------------------------"+obj.toString());
	       	return write(obj.toString());
        } catch(Exception e) {
        	log.error("---getUserApprover-------------------------\n" + e.toString());
      	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
        }
	}


	private IResponse selectRepList(IRequest request) {
		// TODO Auto-generated method stub
    	String col = request.getStringParam("grid_col_id");
     	  String[] cols = col.split(",");
     	  Map map = null;
         GridData gdRes = new GridData();
         
     	  try {
	         Map smap = request.getMap();
	         log.info("request.getMap() :: "+request.getMap()); 
	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
	        

	       	 String dbName = pm.getString("component.sql.database");
	       	 List lists = getList("mg.cp.selectRepList"+getDBName(dbName),smap);
	      
	       	 log.info(lists.toString());
	       	 
	       	 
	         for (int i = 0; i < lists.size(); i++) {
	        	 
	          	map =(Map) lists.get(i);
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }
	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
	}


	private IResponse cntDocList(IRequest request) {
		// TODO Auto-generated method stub
    	// CP Community 메인 페이지 수신문서 개수, 업로드 대기 개수, 최근 로그인 시간 가져오기 (로그인 사용자)
    	
    	JSONObject obj = new JSONObject();
    	
    	Map param = request.getMap();
    	param.put("LOGIN_ID",request.getStringParam("userId"));
    	param.put("LOCALE",request.getStringParam("locale"));
    	//log.info(request.getStringParam("userId"));
    	//log.info(request.getStringParam("locale"));
    	
    	String dbName = pm.getString("component.sql.database");
    	
    	String downloadCnt = "";
    	String uploadCnt =  "";
    	String recentLogin =  "";
    	
    	downloadCnt = (String)getItem("mg.cp.exeDownComCnt"+getDBName(dbName), param);
    	uploadCnt = (String)getItem("mg.cp.exeUploadComCnt"+getDBName(dbName), param);
    	recentLogin = (String)getItem("mg.cp.recentLogin"+getDBName(dbName), param);
    	/*if("mssql".equals(dbName)){
    		downloadCnt = (String)getItem("mg.cp.exeDownComCnt", param);
        	uploadCnt = (String)getItem("mg.cp.exeUploadComCnt"+getDBName(dbName), param);
        	recentLogin = (String)getItem("mg.cp.recentLogin"+getDBName(dbName), param);
    	}else{
    		downloadCnt = (String)getItem("mg.cp.exeDownComCnt_" + dbName , param);
        	uploadCnt = (String)getItem("mg.cp.exeUploadComCnt_" + dbName, param);
        	recentLogin = (String)getItem("mg.cp.recentLogin_" + dbName, param);
    	}*/
    	
    	
    	log.info("다운로드 개수 : " + downloadCnt);
    	log.info("업로드대기 개수 : " + uploadCnt);
    	log.info("최근 로그인 : " + recentLogin);
    	obj.put("downCnt", downloadCnt);
    	obj.put("uploadCnt", uploadCnt);
    	obj.put("recentLogin"+getDBName(dbName), recentLogin);
    	//String downCnt = (String)getItem("mg.cp.getPersonalPassword");
    	
		return write(obj.toString());
	}


	private IResponse getDetailDocInfo(IRequest request) {
		// TODO Auto-generated method stub
    	Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		smap.put("CheckDay", pm.getInt("cliptorplus.download.period.day"));
		try{
    	
			String dbName = pm.getString("component.sql.database");
			Map map = (Map)getItem("mg.cp.selectDetailDocInfo"+getDBName(dbName),smap);
    	
    		// Creation of Json Response 
    		 JSONObject obj = new JSONObject();
    		obj.put("USER_NM", this.getStringWithNullCheck(map, "USER_NM"));
    		obj.put("APPROVE_DT", this.getStringWithNullCheck(map, "APPROVE_DT"));
    		obj.put("FILE_NUM", this.getStringWithNullCheck(map, "FILE_NUM"));
    		obj.put("READ_COUNT", this.getStringWithNullCheck(map, "READ_COUNT")); 
    		obj.put("APPROVE_YMD", this.getStringWithNullCheck(map, "APPROVE_YMD"));
    		obj.put("PERIOD", "false");
    		 if(pm.getBoolean("cliptorplus.download.period.check")) {                    // Exe Download 기간체크 옵션이면
  	        	 /* if( Integer.parseInt(getItem("mg.cp.exePeriodChek"+getDBName(dbName), smap).toString() ) > 0 ) {*/
  	        		obj.put("PERIOD", this.getStringWithNullCheck(map, "PERIOD"));
  	        	  //}
  	          }
    		 log.info("getDetailDocInfo---------------------------------------------------------------------------"+obj.toString());
    		 return write(obj.toString());
         } catch(Exception e) {
       	 	log.error("---getDetailDocInfo 불러오기 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}


	private IResponse saveRepSchedule(IRequest request) {
		// TODO Auto-generated method stub
    	Map itm = new HashMap();
        DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String scheNo = formatter.format(new Date()) + getRandomString(6);
		try{
			itm.put("DOC_NO", request.getStringParam("docId"));
			itm.put("SCHE_NO", scheNo);
			itm.put("CP_STS_CD", "02");
			itm.put("EXE_RULE", "REPACKING");
			log.info(itm.toString());
			String dbName = pm.getString("component.sql.database");
			createObject("mg.cp.saveJobSchedule_Repacking"+getDBName(dbName), itm);
			log.info("Repacking Job Schedule 등록 완료");
			return write("Repacking 등록");
		}catch(Exception e){
			log.error("------------------------------------------------------------"+e.toString());
			return write("Repacking 등록실패");
		}
 		
	}


	private IResponse getRepackingList(IRequest request) {
		// TODO Auto-generated method stub
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try{
        	 List lists;
	       	 Map smap = request.getMap();
	       	 smap.put("dbName", pm.getString("component.sql.database"));
	       	 smap.put("LOCALE", request.getUser().getLocale().getLanguage());
        	 
	       	String dbName = pm.getString("component.sql.database");
	       	 lists = getList("mg.cp.selectRepackingList"+getDBName(dbName),smap);
        	 
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}


	/*
     * CP 번호 생성 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse testDate(IRequest request) throws Exception 
     {
   	    try {
   	    	
   	    	/*
   	    	log.info("------------testDate-------request.getUser().getLocale()--------\n" + request.getUser().getLocale());
   	    	
   	    	Locale locale = new java.util.Locale("ko");
   	    	log.info("------------testDate-------request.getUser().getLocale()--------\n" + locale.toString());
   	    	
   	    	String tmpMsg = mm.getMessage("CPMG_1007", locale);
   	        log.info("------------Message-------\n" + tmpMsg);
   	        
   	        Object[] params = {"문서제목 입니다. "};
   	        tmpMsg = mm.getMessage("CPMG_1007", params, locale);
	        log.info("------------Message2-------\n" + tmpMsg);
	        */
	        
	        String expireDt = request.getStringParam("dt");
			DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
   		  	String curDt = formatter.format(new Date());
   		  	
   		    //  log.info("------------Message3-------\n" +  formatter.parse(expireDt).toString());
   		    // log.info("------------Message3-------\n" +  curDt.toString());
   		    String CompDt = DateUtils.addDay(expireDt, +1);
   		    Date expDt = formatter.parse(CompDt);
   		   
   		    
   		    log.info("------------Message3-------\n" +  expDt.toString());
   		    
			if(expDt.compareTo(new Date()) < 0 ) { 
				  log.info("-----------넘었나------\n");
			} else { 
				  log.info("-----------같은날 ??------\n");
			} 

	        	        
   	        // MessageFormat Mform = new MessageFormat(tmpMsg);   	    	
   	    	// Object[] testArgs = {new Long(3), "MyDisk"};

	         // String temp = replace(temp, "$codeName$", mm.getMessage("CPMG_2110", request.getUser().getLocale()));
   	    	
   	    	/*
			String expireDt = request.getStringParam("EXPIRE_DT");
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
   		  	// String curDt = formatter.format(new Date());
   		  	Date expDt = formatter.parse("2014-04-14");
   		  	try {
		   		 IMail mail = mailHandle.createHtmlMail();
		         mail.setFrom("genetic@itemsystem.com");
		         mail.addTo("genetic@itmsystem.com");
		         mail.setSubject("멍청한 메일을 보낸다.");
		         mail.setTextMessage("메일을 보내주니 좋냐 ?\n이 탱구리야");
		         mail.send();
   		  	}catch(Exception e) {
   		  	   log.info("--Mail 발송실패--\n" + e.toString());
   		  	}	   		   		    
   		  	
			if(expDt.compareTo(new Date()) < 0 ) {

    			return write("date : 현재날자 가 크다 : " + expDt);
			}
			   		  	
			*/
			return write("Locale : " + request.getUser().getLocale());
       } catch(Exception e) {
    	    log.error("---------------------------------------------------------------\n" + e.toString());
      	 	return write("CP 번호 생성 실패");
       }
     }
     
     public String getDocNo(String docname)
     {
    	 return ("DDDDFFF");
     }
    
    /**
     * CP 번호 생성 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getCpDoc(IRequest request) throws Exception 
     {
   	    try {
   		  
   	    	DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
   		  	String cpDocNo = formatter.format(new Date()) + getRandomString(6);
   		 	return write(cpDocNo);
       } catch(Exception e) {
    	    log.error("------CP 번호 생성 실패-------------------------------------------------------\n" + e.toString());
      	 	return write(mm.getMessage("CPMG_1001", request.getUser().getLocale()));
       }
     }
     
     /**
      * CP 문서정보 Return 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse getCpDocInfo(IRequest request) throws Exception 
      {
    	 try {
    		 
	       	 String dbName = pm.getString("component.sql.database");

	       	 Map map  = (Map)getItem("mg.cp.getCpDocInfo"+getDBName(dbName),request.getMap());
  		
    		// Creation of Json Response 
    		JSONObject obj = new JSONObject();
    		obj.put("DOC_NO", this.getStringWithNullCheck(map, "DOC_NO"));
    		obj.put("DOC_TITLE", this.getStringWithNullCheck(map, "DOC_TITLE"));
    		obj.put("P_DOC_NO", this.getStringWithNullCheck(map, "P_DOC_NO"));
    		obj.put("USER_ID", this.getStringWithNullCheck(map, "USER_ID"));
    		obj.put("USER_NM", this.getStringWithNullCheck(map, "USER_NM"));
    		obj.put("DEPT_NM", this.getStringWithNullCheck(map, "DEPT_NM"));
    		obj.put("FUNCTION_NM", this.getStringWithNullCheck(map, "POSITION_NM"));
    		obj.put("POSITION_NM", this.getStringWithNullCheck(map, "FUNCTION_NM"));
    		obj.put("CREATOR_EMAIL", this.getStringWithNullCheck(map, "CREATOR_EMAIL"));
    		obj.put("APPROVER_ID", this.getStringWithNullCheck(map, "APPROVER_ID"));
    		obj.put("APPROVER_NM", this.getStringWithNullCheck(map, "APPROVER_NM"));
    		obj.put("AUTHORITY", this.getStringWithNullCheck(map, "AUTHORITY"));
    		obj.put("IN_PROCESS", this.getStringWithNullCheck(map, "IN_PROCESS"));
    		obj.put("WAITING_TIME", this.getStringWithNullCheck(map, "WAITING_TIME"));
    		obj.put("EXE_RULE", this.getStringWithNullCheck(map, "EXE_RULE"));
    		obj.put("EXE_FILE", this.getStringWithNullCheck(map, "EXE_FILE"));
    		obj.put("OUT_CL_CD", this.getStringWithNullCheck(map, "OUT_CL_CD"));
    		obj.put("FILE_LIST", this.getStringWithNullCheck(map, "FILE_LIST"));
    		obj.put("DOC_MSG", this.getStringWithNullCheck(map, "DOC_MSG"));
    		obj.put("CP_STS_CD", this.getStringWithNullCheck(map, "CP_STS_CD"));
    		
    		return write(obj.toString());
         } catch(Exception e) {
       	 	log.error("---CP 문서정보 조회 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
      }
     
     
    
    /**
     * 현황 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectDocList(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
       	
       	  try {
       		  Map smap = request.getMap();
       		  smap.put("LOCALE", request.getUser().getLocale().getLanguage());
       		  
       		  String dbName = pm.getString("component.sql.database");
       		  List lists;
       		  
       		  if("ILJIN".equals(pm.getString("component.site.company"))) {
       			  lists = getList("mg.site.selectDocList"+getDBName(dbName),smap);	//일진글로벌용 컬럼 추가
       		  } else {
       			  lists = getList("mg.cp.selectDocList"+getDBName(dbName),smap);
       		  }
	    
 	         for (int i = 0; i < lists.size(); i++) {
 	          	map =(Map) lists.get(i);
 	          	 
 	          	for(int j=0; j < cols.length; j++ ) {
 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	          	}
 	         }
 	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
          	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
     }
           
  /**
   * 반출신청 저장 
   * @param request
   * @return
   * @throws Exception
   * 최종 수정일 : 2016. 03. 16
   * 변경 내용 : 일진글로벌 반출신청시 결재연동 처리
   */
  private IResponse saveRequest(IRequest request) throws Exception 
  {
      String ids_info = request.getStringParam("ids");
      String cols_ids = request.getStringParam("col_ids");
      String[] cols = cols_ids.split(",");
      String[] ids = ids_info.split(",");
      String msg="", flag="";
      JSONParser parser = new JSONParser();
      JSONObject s_user = null;
      Locale locale = null;
      
      // log.info("--saveRequest------------------------" + request.getMap().toString());
       
      try {
      	tx.begin();
      	
      	//사용자 정보 추출
      	if(request.getUser() != null) {
      		s_user = new JSONObject();
      		
      		Map params = request.getUser().getProperties();
      		Iterator it = params.keySet().iterator();
      		
      		while(it.hasNext()) {
      			String key = (String)it.next();
      			s_user.put(key, params.get(key));
      		}
      		locale = request.getUser().getLocale();
      		
      	} else {
      		s_user = (JSONObject)parser.parse(request.getStringParam("userInfo"));
      		String lang = request.getStringParam("j_janguage");		//language
    		
    		if("ko".equals(lang)) {
    			locale = Locale.KOREA;
    		} else if("en".equals(lang)) {
    			locale = Locale.US;
    		} else if("zh".equals(lang)) {
    			locale = Locale.CHINA;
    		} else if("ja".equals(lang)) {
    			locale = Locale.JAPAN;
    		} else {
    			locale = Locale.KOREA;
    		}
      	}
      	
      	Map itm = request.getMap();
      	itm.put("LOGIN_ID",  s_user.get("USER_ID"));
      	
      	String approver1_id ="";
      	String approver2_id = "";
      	
   		//POST 방식으로 전달 받기 위해 수정 
   		itm.put("FILE_LIST",  java.net.URLDecoder.decode(request.getStringParam(ids[0] + "_FILE_LIST"), "utf-8"));
        
   		itm.put("cpVersion", pm.getString("component.site.product.version"));
   		
   		// Doc Master 등록
        if( Integer.parseInt(getItem("mg.cp.countDocMaster", itm).toString() ) > 0 ) {
        	String dbName = pm.getString("component.sql.database");
        	updateObject("mg.cp.updateDocMaster"+getDBName(dbName), itm);
       	} else {
       		//1. CP_DOC_MASTER 반출 문서 등록
       		itm.put("IN_CL_CD", "C");
 
       		String dbName = pm.getString("component.sql.database");
       		createObject("mg.cp.insertDocMaster"+getDBName(dbName), itm); 
       		log.info("Master 등록 완료");
       		
       		//반출 신청후 결재 시스템을 통해 승인자를 선택할 경우 (일진) 
       		if(!"Y".equals(pm.getString("cliptorplus.export.request.noApprover"))){
       			//2. 승인자 정보 추출
           		if("personal".equals(pm.getString("component.export.approver"))) {
    		       	Map approverMap= new HashMap();//  = (Map)getItem("mg.cp.getApproverId", request.getMap());
    		       	approverMap.put("USER_ID",  s_user.get("USER_ID"));
    		       	List lists = getList("mg.cp.getApproverId",approverMap);
    		         for (int i = 0; i < lists.size(); i++) {
    		        	 approverMap =(Map) lists.get(i); 
    		        	 if(i==0){
    		        		 approver1_id = this.getStringWithNullCheck(approverMap, "APPROVER_ID");
    		        	 }else{
    		        		 approver2_id= this.getStringWithNullCheck(approverMap, "APPROVER_ID");
    		        	 }
    		         }
       			}else{
       				approver1_id = (String)s_user.get("MANAGER_ID");
    	   			itm.put("P_DEPT_CD", s_user.get("P_DEPT_CD"));
    	   			Object result = getItem("mg.cp.getManagerID", itm);
    	   	      	approver2_id =	(result == null) ? "" : result.toString();
       			}
       			// 반출자와 승인자가 동일하면 승인자로 설정 하지 않음
           		
           		if(approver2_id.equals(s_user.get("USER_ID"))){
           			//log.info("두번째 승인자의 아이디로 자신의 아이디가 있을 경우 순서 변경");
           			String tempId = approver1_id;	
           			approver1_id=approver2_id;
           			approver2_id=tempId;	
           		}
       			if(approver1_id.equals(s_user.get("USER_ID")) && !"Y".equals(s_user.get("SELF_EXP_YN"))) {
       				approver1_id = "sameApprover";
           		}else if("Y".equals(s_user.get("SELF_EXP_YN"))){
           			approver1_id = (String)s_user.get("USER_ID");
           		}
       			log.info("approver1_id / approver2_id---------------------------------------------------------------"+approver1_id+" / "+approver2_id+"--------------------"); 		
           		itm.put("APPROVER1_ID", approver1_id);
           		itm.put("APPROVER2_ID", approver2_id);
           		
           		//3. CP_DOC_APPROVER 내 승인자 정보 등록
           		log.info("--saveRequest---insertDocApprover---------------------\n" + itm.toString());
           		createObject("mg.cp.insertDocApprover", itm); 
       		}
       		
       		//4. CP_JOB_SCHEDULE 상태코드등록
            DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    		String scheNo = formatter.format(new Date()) + getRandomString(6);
       	 	itm.put("SCHE_NO", scheNo);
       	 	log.info("--saveRequest---1");
       	 	if(!"Y".equals(s_user.get("SELF_EXP_YN"))) itm.put("CP_STS_CD", "01");
       	 	else itm.put("CP_STS_CD", "02");
       	 	
     		log.info(itm.toString());
     		
     		createObject("mg.cp.saveJobSchedule"+getDBName(dbName), itm);
     		log.info("Job Schedule 등록 완료");
       	}
                
        // 무인증 이면 수신자 등록 안함.
        // if(!"5".equals(this.getStringWithNullCheck(itm,"AUTHORITY"))) {
        
        // Doc User 등록
        for (int i = 0; i < ids.length; i++) {
       	 	Map item = new HashMap();
       	 
       	 	for(int j=0; j < cols.length; j++ ) {
       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	            item.put(cols[j], tmps);     
         	}
       	 	item.put("LOGIN_ID", s_user.get("USER_ID"));
       	 	item.put("DOC_NO",  itm.get("DOC_NO"));
       	 	
       	 	log.info("--saveRequest--- Doc User 등록------\n" + item.toString());
       	 	if("".equals(item.get("SEQ").toString())) {
       	 		item.put("SEQ", 0);
       	 	}

	       	if( Integer.parseInt(getItem("mg.cp.countDocUser", item).toString() ) > 0 ) {
	       		String dbName = pm.getString("component.sql.database");
	       		updateObject("mg.cp.updateDocUser"+getDBName(dbName), item);
	       	} else {
	       		item.put("dbName", pm.getString("component.sql.database"));
	       		item.put("PASSWD", ""); // 신청/승인 프로세스에서는 DOC_USER 비밀번호 없음
	       		
	       		String dbName = pm.getString("component.sql.database");
	       		createObject("mg.cp.insertDocUser"+getDBName(dbName), item);
	       	}
        }
        // }
        
        // 원문반출이고 반출함 설정이 있으면 반출함을 등록 한다.
        String authority = this.getStringWithNullCheck(itm, "AUTHORITY");
       	if("Y".equals(pm.getString("cliptorplus.vf.export")) && "6".equals(authority)) {
       		// 반출함 등록을 처리 해야 한다. by cyLim, 2015/06/05
       	}
        
        if("Y".equals(pm.getString("cliptorplus.request.mail")) && !"Y".equals(s_user.get("SELF_EXP_YN")) 
        		&& !"Y".equals(pm.getString("cliptorplus.export.request.noApprover"))) {
        	
        	// 신청시 메일발송 옵션 체크 시
	 		// 승인자에게 메일 발송
	 		try {
		   		 IMail mail = mailHandle.createHtmlMail();
		         mail.setFrom(this.getStringWithNullCheck(itm,"CREATOR_EMAIL"));
		         
		         itm.put("APPROVER_ID",  approver1_id);
		         // getItemData("mg.cp.getApproverEmail", itm)
		         log.info("------------saveUser------APPROVER_ID 1--\n" + itm.toString());
		         String tmpApprover = getItemData("mg.cp.getApproverEmail", itm);
		         log.info("------------APPROVER_ID 1--\n" + tmpApprover);
		         if(tmpApprover != null && !"".equals(tmpApprover)) {
		        	 mail.addTo(tmpApprover);   // 상위자
		         }
		         itm.put("APPROVER_ID",  approver2_id);
		         log.info("------------saveUser------APPROVER_ID 2--\n" + itm.toString());
		         tmpApprover = getItemData("mg.cp.getApproverEmail", itm);   // 차상위자
		         log.info("------------APPROVER_ID 2--\n" + tmpApprover);
		         if(tmpApprover != null && !"".equals(tmpApprover)) {
		        	 mail.addTo(tmpApprover);   // 상위자
		         }
		        // mail.setSubject(mm.getMessage("MAIL_0001", locale));
		         mail.setSubject("["+mm.getMessage("MAIL_0004", locale)+"]"+" "+mm.getMessage("MAIL_0001", locale)); //보안문서 반출요청
		         
		         //메일 템플릿을 읽어 온다.
		         String fileName = "";
		         String mailContext = "";
		         
		         if("Y".equals(pm.getString("cliptorplus.mail.desc"))){
		        	 fileName = "config/templates/mail_Template_Desc.html";
		        	 mailContext = readFile(fileName); 
		        	 
		        	 mailContext = mailContext.replace("DESC_HIDDEN", "visible");
		        	 mailContext = mailContext.replace("DESC_LABEL", "");
		        	 String temp = request.getMap().get("DOC_MSG").toString();
		        	 temp = temp.replace("\n", "<br>");
		        	 mailContext = mailContext.replace("DESC_CONTENT", temp);
		         } else {
		        	 fileName = "config/templates/mail_Template.html";
		        	 mailContext = readFile(fileName);
		         }
		        
		 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", locale)+"]");
				 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0001", locale));
				 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.ep.url"));
    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));

    			 Object[] params = {s_user.get("USER_NM")};
    			 mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0002",params, locale));
    			 
    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", locale));
				 mailContext = mailContext.replace("COMP_NM", pm.getString("component.site.company.name"));
				 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1001", locale));
		         // 본문 삽입
		         if("Y".equals(pm.getString("cliptorplus.mail.desc"))){
		        	 mailContext = mailContext.replace("DESC_HIDDEN", "visible");
		        	 mailContext = mailContext.replace("DESC_LABEL", "");
		        	 String temp = request.getMap().get("DOC_MSG").toString();
		        	 temp = temp.replace("\n", "<br>");
		        	 mailContext = mailContext.replace("DESC_CONTENT", temp);
		         } else {
		        	 mailContext = mailContext.replace("DESC_HIDDEN", "none");
		         }
				 String tmp="";
 				// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------2"+s_user.get("POSITION_NM"));
				 if(s_user.get("POSITION_NM") == null){
					// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------2-1");
					 tmp = s_user.get("USER_NM")+" / "+s_user.get("FUNCTION_NM")+" / "+s_user.get("DEPT_NM");
				 }else{
					 tmp = s_user.get("USER_NM")+" / "+s_user.get("POSITION_NM")+" / "+s_user.get("DEPT_NM");
				 }
				// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------3");
				 mailContext = mailContext.replace("USER_NM", tmp);
				 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1002", locale));
				 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		   		 String curDt = formatter.format(new Date());
				 mailContext = mailContext.replace("REQ_DT", curDt);
		         
				 if("LGCHEM".equals(pm.getString("component.site.company"))){
					 mailContext = mailContext.replace("LINK", pm.getString("component.site.url")+"/Login_hidden.jsp?docNo="+itm.get("DOC_NO"));//파트너 포탈
				 }else{
					 mailContext = mailContext.replace("LINK", pm.getString("component.site.ep.url"));
				 }
				 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1005", locale));
				 
				// 메일 발송
				 mail.setHtmlMessage(mailContext);
		         mail.send();
		         
		  	}catch(Exception e) {
		  	     log.info("--Mail 발송실패--\n" + e.toString());
		  	}
        }
        msg = mm.getMessage("COMG_1002", locale);      // mm.getMessage("CPMG_1004", locale);
        flag = "true";
        String result="";
		//LG화학 자동전자결재 연동//신청(기안)처리
        // s_user.get("USER_ID")로  사번을 찾아야됨.
        if("LGCHEM".equals(pm.getString("component.site.company"))) {
        		itm.put("USER_ID", s_user.get("USER_ID"));
        		Map map = (Map)getItem("mg.cp.getApproverEmpNo", itm);
        		LgChemInterlock lgChem = new LgChemInterlock();
        		result = lgChem.ESB_R(request.getStringParam("DOC_NO"), request.getStringParam("DOC_TITLE"),this.getStringWithNullCheck(map, "EMP_NO"), this.getStringWithNullCheck(map, "APPROVER_ID"));
        		 //0000:성공 9999:실패
        } else if("SB".equals(pm.getString("component.site.company"))){
   	 		//상신브레이크 결재연동 승인/반려처리
        	log.info("상신브레이크 결재연동------------------------saveRequest");
        	SangsinInterlock sb = new SangsinInterlock();
   	 		result = sb.saveRequest_SB(request);
   	 		//1:성공 0:실패
        } else if("ILJIN".equals(pm.getString("component.site.company"))){
        	//일진글로벌 결재연동
        	log.info("일진글로벌 결재연동------------------------saveRequest");
        	IljinInterlock iljin = new IljinInterlock();
        	result = iljin.saveRequest(request, ids, cols);
        }
        
        if("9999".equals(result) || "0".equals(result)){
        	tx.rollback();
	      	msg = mm.getMessage("CPMG_1003", locale);
	        flag = "false";
        }else{
            tx.commit();	
        }      
        
      }catch (Exception e) {
      	tx.rollback();
      	log.error("------------saveRequest---------------------------------------------------\n" + e.toString());
      	msg = mm.getMessage("CPMG_1003", locale);
        flag = "false";
      }
      String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
      return writeXml(ret);
  }
  
  /**
   * 반출문서 정책 일괄 수정
   * @param request
   * @return
   * @throws Exception
   */
  private IResponse changePolicy(IRequest request) throws Exception 
  {
      log.info("--changePolicy------------------------" + request.getMap().toString());
       
      try {
	      	tx.begin();
	      	String dbName = pm.getString("component.sql.database");
	  		updateObject("mg.cp.updateDocUserBatch"+getDBName(dbName), request.getMap());
	        tx.commit();
	        
	        return write("Success");
      } catch (Exception e) {
      	    tx.rollback();
	      	log.error("------------changePolicy---------------------------------------------------\n" + e.toString());
	      	return write(mm.getMessage("CPMG_1003", request.getUser().getLocale()));
      }
  }
  /**
   * 반출문서 정책 단건 수정
   * @param request
   * @return
   * @throws Exception
   */
  private IResponse changePolicySingle(IRequest request) throws Exception 
  {
      log.info("--changePolicySingle------------------------" + request.getMap().toString());
       
      try {
	      	tx.begin();
	      	String dbName = pm.getString("component.sql.database");
	  		updateObject("mg.cp.updateDocUserSingle"+getDBName(dbName), request.getMap());
	        tx.commit();
	        
	        return write("Success");
      } catch (Exception e) {
      	    tx.rollback();
	      	log.error("------------changePolicySingle---------------------------------------------------\n" + e.toString());
	      	return write(mm.getMessage("CPMG_1003", request.getUser().getLocale()));
      }
  }
  /*
   * 반출 사용자 정보 삭제
   * @param request
   * @return
   * @throws Exception
   */
  private IResponse deleteDocUser(IRequest request) throws Exception 
  {
      String ids_info = request.getStringParam("ids");
      String cols_ids = request.getStringParam("col_ids");
      String[] cols = cols_ids.split(",");
      String[] ids = ids_info.split(",");
      String msg="", flag="";
       
      try {
      	tx.begin();
        Map item = null;
        for (int i = 0; i < ids.length; i++) {
       	 item = new HashMap();
       	 
       	 	for(int j=0; j < cols.length; j++ ) {
       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	            item.put(cols[j], tmps);     
         	} 
       	 	item.put("DOC_NO",  request.getMap().get("DOC_NO"));
       	 	deleteObject("mg.cp.deleteDocUser", item); 
        }
        tx.commit();
        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
        flag = "true";
      } catch (Exception e) {
      	tx.rollback();
      	log.error("------------deleteDocUser-------------------------------------\n" + e.toString());
      	msg = mm.getMessage("CPMG_1004", request.getUser().getLocale());
        flag = "false";
      }
      
      String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
      return writeXml(ret);
  }
  
  /**
   * 반출 사용자 조회 
   * @param request
   * @return
   * @throws Exception
   */
   private IResponse selectDocUser(IRequest request) throws Exception 
   {
	   String col = request.getStringParam("grid_col_id");
	   String[] cols = col.split(",");
       Map map = null;
       GridData gdRes = new GridData();
       
       try {

	       	 String dbName = pm.getString("component.sql.database");
	       	List lists;
	       	 if("2".equals(request.getStringParam("POLICY_CL_CD")) && "Y".equals(pm.getString("cliptorplus.vf.export"))) {
	       		 lists = getList("mg.cp.selectVFDocUser"+getDBName(dbName),request.getMap());
	       	 }else {
	       		 lists = getList("mg.cp.selectDocUser"+getDBName(dbName),request.getMap());
	       	 }
	           
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
       } catch(Exception e) {
      	 	log.error("----------selectDocUser---------\n" + e.toString());
      	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
       }
   } 
       
   /**
    * 반출신청 삭제
    * @param request
    * @return
    * @throws Exception
    */
   private IResponse deleteDocMaster(IRequest request) throws Exception 
   {
       String ids_info = request.getStringParam("ids");
       String cols_ids = request.getStringParam("col_ids");
       String[] cols = cols_ids.split(",");
       String[] ids = ids_info.split(",");
       String msg="", flag="";
        
       try {
	       	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	    item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	// 1. Schedule 삭제
	       	 	deleteObject("mg.cp.deleteSchedule", item);
	       	 	// 2. 수신자 삭제
	       	 	deleteObject("mg.cp.deleteDocUser", item);
	       	 	// 2. 승인자 삭제
	       	 	deleteObject("mg.cp.deleteApprover", item);
	       	 	// 4. 문서마스터
	       	 	deleteObject("mg.cp.deleteDoc", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
       } catch (Exception e) {
	       	tx.rollback();
	       	log.error("------------deleteDocMaster-----------------------------------\n" + e.getMessage());
	       	msg = mm.getMessage("CPMG_1004", request.getUser().getLocale());
	        flag = "false";
       }
       String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
       return writeXml(ret);
   }
       
   /**
    * 결재승인 조회 	
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectApproval(IRequest request) throws Exception 
    {
      	  String col = request.getStringParam("grid_col_id");
      	  String[] cols = col.split(",");
      	  
      	  Map map = null;
          GridData gdRes = new GridData();
      	
      	  try {
      		  	 Map smap = request.getMap();
      		  	 smap.put("LOCALE", request.getUser().getLocale().getLanguage());
				 
	 	       	 String dbName = pm.getString("component.sql.database");

	 	       	 List lists = getList("mg.cp.selectApproval"+getDBName(dbName),smap);
	 	      
				  	
				 for (int i = 0; i < lists.size(); i++) {
				  	map =(Map) lists.get(i);
				  	 
				  	for(int j=0; j < cols.length; j++ ) {
				  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
				  	}
				  }
				return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	 	log.error("--------selectApproval----------------\n" + e.toString());
         	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
    }
    
    /**
     * 반출내역 조회(관리자 기능)	
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectDocAdmin(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
       	
       	  try {
       		  	 Map smap = request.getMap();
       		  	 smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 				 
	 	       	 String dbName = pm.getString("component.sql.database");
	 	       	 List lists = getList("mg.cp.selectDocAdmin"+getDBName(dbName),smap);
			  	
 				 for (int i = 0; i < lists.size(); i++) {
 				  	map =(Map) lists.get(i);
 				  	 
 				  	for(int j=0; j < cols.length; j++ ) {
 				  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 				  	}
 				  }
 				return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 	log.error("--------selectApproval----------------\n" + e.toString());
          	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
            }
     }
     /**
      * 반출내역 조회(관리자 기능)	
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse selectDocAdminAll(IRequest request) throws Exception 
      {
        	  String col = request.getStringParam("grid_col_id");
        	  String[] cols = col.split(",");
        	  
        	  Map map = null;
            GridData gdRes = new GridData();
        	
        	  try {
        		  	 Map smap = request.getMap();
        		  	 smap.put("LOCALE", request.getUser().getLocale().getLanguage());
  				 
 	 	       	 String dbName = pm.getString("component.sql.database");
 	 	       	 List lists = getList("mg.cp.selectDocAdminAll"+getDBName(dbName),smap);
 			  	
  				 for (int i = 0; i < lists.size(); i++) {
  				  	map =(Map) lists.get(i);
  				  	 
  				  	for(int j=0; j < cols.length; j++ ) {
  				  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  				  	}
  				  }
  				return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
            } catch(Exception e) {
           	 	log.error("--------selectApprovalAll----------------\n" + e.toString());
           	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
             }
      }
     /**
      * 반출내역 조회(관리자 기능)  ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelDocAdmin(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
       String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Export Document List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
  	           	       
				 String dbName = pm.getString("component.sql.database");
		       	 List lists = getList("mg.cp.selectDocAdmin"+getDBName(dbName), request.getMap()); 
         
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Export Document List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
     }
    /**
     * 반출 문서 승인
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveApproval(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String approvalYn = request.getStringParam("APPROVAL_YN");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
        	
        	String dbName = pm.getString("component.sql.database");
        	Map userMap;
        	if("mssql".equals(dbName)){
        		userMap= (Map)getItem("app.user.getUserByProperties", request.getUser().getId().toString());
        	}else{
        		userMap= (Map)getItem("app.user.getUserByProperties_" + dbName, request.getUser().getId().toString());
        	}
	        for (int i = 0; i < ids.length; i++) {
	       	 	Map item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	}
	       	 	
	       	    item.put("APPROVAL_YN", approvalYn);
	       	    msg=comSingleApproval(item, request.getUser().getLocale(), userMap);
	       	 	       
		        flag = "true";
        		String reuslt ="";
	       	    //LG화학 결제 연동 승인/반려 처리
	       	 	if("LGCHEM".equals(pm.getString("component.site.company"))) {
	       	 		item.put("USER_ID", request.getUser().getId());
	       	 		Map remap = (Map)getItem("mg.cp.getApproverEmpNo", item);
	       	 	LgChemInterlock lgChem = new LgChemInterlock();
	       	 		if("Y".equals(approvalYn)){
	       	 			reuslt = lgChem.ESB_F(this.getStringWithNullCheck(item, "DOC_NO"), this.getStringWithNullCheck(remap, "EMP_NO")); //문서번호, 승인자번호
	       	 			//0000:성공 9999:실패
	       	 		}else{
		       	 		reuslt = lgChem.ESB_C(this.getStringWithNullCheck(item, "DOC_NO"), this.getStringWithNullCheck(remap, "EMP_NO")); //문서번호, 반려자번호
		        		 //0000:성공 9999:실패 					
	       	 		}
	       	 		if("9999".equals(reuslt)){
	       	 			tx.rollback();
	       	 			msg = mm.getMessage("CPMG_1003", request.getUser().getLocale());
	       	 			flag = "false";
	       	 		}    
	       	 	} 	 		
	        }
	        tx.commit();
        }catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveApproval------------------------\n" + e.toString());
        	msg = mm.getMessage("CPMG_1005", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    /**
     * 반출 문서 승인 - 단일건
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse singleApproval(IRequest request) throws Exception 
    {
        String msg="";
        try {
        	tx.begin();
        	
        	//--- 사용자의 정보를 찾아옴
        	String dbName = pm.getString("component.sql.database");
        	Map userMap = (Map)getItem("app.user.getUserByProperties"+getDBName(dbName), request.getUser().getId().toString());
        	//-------------------------------------------------
        	msg=comSingleApproval(request.getMap(), request.getUser().getLocale(), userMap);
        	
	        tx.commit();
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------singleApproval------------------------\n" + e.toString());
        	msg =  mm.getMessage("CPMG_1005", request.getUser().getLocale());
        }
        return  write(msg);
    }
    
    public String comSingleApproval(Map map, Locale locale, Map userMap)
    {
    	String approvalYn =  this.getStringWithNullCheck(map,"APPROVAL_YN");
        String msg="";
        try {
        	//tx.begin();
	       	msg = exportApproval(map, approvalYn, locale, userMap);
	       	
	       	String authority = this.getStringWithNullCheck(map, "AUTHORITY");
	       	log.info("-----------------------------------------comSingleApproval------------------------authority: "+authority);
	       	if("Y".equals(pm.getString("cliptorplus.vf.export")) && "6".equals(authority)) {
		       	// 원문인 경우 반출함 상태 저장
		       	// apvReason, isApproval, 
		       	map.put("isApproval", approvalYn);
		       	map.put("apvReason",  this.getStringWithNullCheck(map, "APPROVAL_MSG"));
		       	AgentAction agentAction = new AgentAction();
		       	agentAction.setExportStat(map);
	       	}
	       // tx.commit();
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------singleApproval------------------------\n" + e.toString());
        	msg =  mm.getMessage("CPMG_1005",locale);
        }
       return msg;
    }
    
    
    private String compressFile(Map map){
		 String isError="0"; 
    	try{

		    String dbName = pm.getString("component.sql.database");       	
		    Map item  = (Map)getItem("mg.cp.getCpDocInfo"+getDBName(dbName),map);

	       	//log.info("-------------------------------"+this.getStringWithNullCheck(item, "FILE_LIST"));
           	 String[] fileList =  this.getStringWithNullCheck(item, "FILE_LIST").split("\\|");
           	 String docNo = this.getStringWithNullCheck(item, "DOC_NO");
           	 String fileName = this.getStringWithNullCheck(item, "EXE_FILE");
           	 //log.info("--------------docNo : "+docNo+" -----------------------------------------fileName : "+fileName+" ----------------------------fileList : " +fileList.toString());
           	 
           	 File temp = new File(pm.getString("component.upload.exedirectory")+"/"+docNo.substring(0,8)+"/"+docNo);
           	 if(!temp.exists()){
           		 temp.mkdirs();
           	 }
           	 
    		 compressHandle.createCompressFile("zip", pm.getString("component.upload.exedirectory")+"/"+docNo.substring(0,8)+"/"+docNo+"/"+fileName);
    		// log.info("-------------------------------------------------------ZIP생성 경로 :"+pm.getString("component.upload.exedirectory")+"/"+docNo.substring(0,8)+"/"+docNo+"/"+fileName);
    		 for(int i=0; i<fileList.length; i++){ 
    		//	 log.info("fileList["+i+"]-------------------------------------------------"+fileList[i]);
    			 compressHandle.addCompressFile(fileList[i]);
    		 }
			 compressHandle.closeCompress();
		 }catch(Exception e){
			 isError="1";
			 log.info("compressFile------------------------------------------------------------------------"+e.toString());
		 }
		 return isError;
    }
    
    public String exportApproval(Map item, String approvalYn, Locale locale, Map userMap )
    {
    	String tmpSubject = "";  
    	String msg = "";
    	String tmpWord= "";
   	 	if("Y".equals(approvalYn)) {  // 반출승인이면
   	 		item.put("CP_STS_CD", "02");  // 승인시 상태 코드를 02 : 대기상태로 만든다.
   	 		tmpSubject= mm.getMessage("CPMG_2120", locale);
   	 		
   	 		// 원문반출 추가 2015/04/28 by cylim
   	 		String authority = this.getStringWithNullCheck(item, "AUTHORITY");
	 	    log.info("--- exportApproval 원문반출 : authority --------\n" +authority);
	 	    
	 		if("6".equals(authority)) { 
   	 		    item.put("CP_STS_CD", "04");  // 승인시 상태 코드를 04 : 완료 상태로 만든다.
	 		
   	 		    //원문 반출 시 zip파일로 압축하여 저장 압축만들때 오류 생기면, state 05로 바꿈
   	 		    if("Y".equals(pm.getString("cliptorplus.zip.export"))){
   	 		    	String result =compressFile(item);
   	 		    	log.info("----------------------------------------------원문반출 zip파일 생성 결과 :"+result);
   	 		    	if("1".equals(result)) item.put("CP_STS_CD", "05");//오류
   	 		    }
	 		
	 		}
   	 	
   	 	    tmpWord = mm.getMessage("CPMG_1108", locale);   // 승인
   	 	    msg = mm.getMessage("CPMG_1030", locale);
   	 	} else {
   	 		item.put("CP_STS_CD", "07"); // 반려이면
   	 		tmpSubject= mm.getMessage("CPMG_2130", locale);
   	 		msg = mm.getMessage("CPMG_1031", locale);
   	 		tmpWord = mm.getMessage("CPMG_1107", locale);   //  반려
   	 	}
   	 	// 문서상태 변경
   	 	String dbName = pm.getString("component.sql.database");
   	 	updateObject("mg.cp.updateSTS"+getDBName(dbName), item); 
   	 	
   	 	// 승인자 등록 --> 현재 승인한 사용자가 승인자 임.
   	 	item.put("APPROVER_ID", this.getStringWithNullCheck(userMap, "USER_ID")); 
   	 	updateObject("mg.cp.updateDocApprover", item);
   	 	
   	 	// 신청자에게 승인 메일 발송
   	    if("Y".equals(pm.getString("cliptorplus.request.mail"))) {
	 		try {
	
	 			 Map mmap = (Map)getItem("mg.cp.getRequesterEmail", item);
	 			 
		   		 IMail mail = mailHandle.createHtmlMail();
		         mail.setFrom(this.getStringWithNullCheck(userMap,"EMAIL"));  //승인자의 E-Mail
		         mail.addTo(this.getStringWithNullCheck(mmap,"CREATOR_EMAIL"));   // 상위자
		         log.info("from----------"+this.getStringWithNullCheck(userMap,"EMAIL"));
		         log.info("to------------"+this.getStringWithNullCheck(mmap,"CREATOR_EMAIL"));
		         
		          // 메일 템플릿을 읽어 온다.
		         String fileName = "";
		         String mailContext = "";
		         if("Y".equals(pm.getString("cliptorplus.mail.desc"))){
		        	 fileName = "config/templates/mail_Template_Desc.html";
		        	 mailContext = readFile(fileName); 
		        	 
		        	 mailContext = mailContext.replace("DESC_HIDDEN", "visible");
		        	 mailContext = mailContext.replace("DESC_LABEL", "");
		        	 
		        	 String temp = item.get("DOC_MSG").toString();
		        	 temp = temp.replace("\n", "<br>");
		        	 mailContext = mailContext.replace("DESC_CONTENT", temp);
		         } else {
		        	 fileName = "config/templates/mail_Template.html";
		        	 mailContext = readFile(fileName);
		         }
		 		 
		 		Object[] params = {tmpWord};
		 		mail.setSubject("["+mm.getMessage("MAIL_0004", locale)+"]"+" "+mm.getMessage("MAIL_0011", params,locale));
		 		
     	 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", locale)+"]");
				 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0011", params, locale));
				 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.ep.url"));
    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));
    			 
    			 mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0012", params, locale));
    			 
    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", locale));
				 mailContext = mailContext.replace("COMP_NM", pm.getString("component.site.company.name"));
				 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1003", locale));
				 String tmp="";
 				//log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------2"+request.getUser().getProperty("POSITION_NM"));
				 if(this.getStringWithNullCheck(userMap,"POSITION_NM")== null){
					log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------2-1");
					 tmp = this.getStringWithNullCheck(userMap,"USER_NM")+" / "+this.getStringWithNullCheck(userMap,"FUNCTION_NM")+" / "+this.getStringWithNullCheck(userMap,"DEPT_NM");
				 }else{
					 tmp = this.getStringWithNullCheck(userMap,"USER_NM")+" / "+this.getStringWithNullCheck(userMap,"POSITION_NM")+" / "+this.getStringWithNullCheck(userMap,"DEPT_NM");
				 }
				// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------3");
				 mailContext = mailContext.replace("USER_NM", tmp);
				 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1004", locale));
				 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		   		 String curDt = formatter.format(new Date());
				 mailContext = mailContext.replace("REQ_DT", curDt);
				 
		    	 mailContext = mailContext.replace("LINK", pm.getString("component.site.ep.url"));//협업 포탈
				 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006", locale));
				  
				 // 메일 발송
				 mail.setHtmlMessage(mailContext);
		         mail.send();

		         return msg;
		  	} catch(Exception e) {
		  	     log.error("--Failed saveApproval !!--\n" + e.toString());
		  	     return msg;
		  	}
   	    }
   	    return msg;
    }
    /*
     * private String exportApproval(IRequest request, Map item, String approvalYn)
    {
    	String tmpSubject = "";  
    	String msg = "";
   	 	if("Y".equals(approvalYn)) {  // 반출승인이면
   	 		item.put("CP_STS_CD", "02");  // 승인시 상태 코드를 02 : 대기상태로 만든다.
   	 		tmpSubject= mm.getMessage("CPMG_2120", request.getUser().getLocale());
   	 	    msg = mm.getMessage("CPMG_1030", request.getUser().getLocale());
   	 	} else {
   	 		item.put("CP_STS_CD", "07"); // 반려이면
   	 		tmpSubject= mm.getMessage("CPMG_2130", request.getUser().getLocale());
   	 		msg = mm.getMessage("CPMG_1031", request.getUser().getLocale());
   	 	}
   	 	// 문서상태 변경
   	 	String dbName = pm.getString("component.sql.database");
   	 	updateObject("mg.cp.updateSTS"+getDBName(dbName), item); 
   	 	
   	 	// 승인자 등록 --> 현재 승인한 사용자가 승인자 임.
   	 	item.put("APPROVER_ID", request.getUser().getId()); 
   	 	updateObject("mg.cp.updateDocApprover", item);
   	 	
   	 	// 신청자에게 승인 메일 발송
 		try {

 			 Map mmap = (Map)getItem("mg.cp.getRequesterEmail", item);
 			 
	   		 IMail mail = mailHandle.createHtmlMail();
	         mail.setFrom(request.getUser().getProperty("EMAIL").toString());  //승인자의 E-Mail
	         mail.addTo(this.getStringWithNullCheck(mmap,"CREATOR_EMAIL"));   // 상위자
	         mail.setSubject(tmpSubject);
	         
	          // 메일 템플릿을 읽어 온다.
	 		  String fileName = "config/templates/approval.htm";
			  String mailContext = readFile(fileName).replace("MAILSUBJECT", tmpSubject);
			  
	         
	         String tmpURL = "<" + this.getStringWithNullCheck(mmap,"BASEURL") + "/cp/approval.do?mod=down&DOC_NO=" + this.getStringWithNullCheck(item,"DOC_NO") + ">";
	         Object[] params = {this.getStringWithNullCheck(item,"DOC_MSG"), this.getStringWithNullCheck(item,"APPROVAL_MSG"), tmpURL};
	         String tmpContext = mm.getMessage("CPMG_2121", params, request.getUser().getLocale());
	         String suEmail="";
	         
	         if("Y".equals(approvalYn)) {  // 반출승인이면
	        	 String dbName = pm.getString("component.sql.database");
	        	 String expireDt="";
		       	 if("mssql".equals(dbName)) {
		       		expireDt = getItem("mg.cp.getExportDt", item).toString();   // ~ 일 까지 Download 가능합니다.
		       	 } else {
		       		expireDt = getItem("mg.cp.getExportDt_" + dbName, item).toString();   // ~ 일 까지 Download 가능합니다.
		       	 }
		       	 Object[] params2={expireDt}; 
	        	 tmpContext += "\n" + mm.getMessage("CPMG_1035", params2, request.getUser().getLocale()); 
	        	 
    	         List lists = getList("mg.cp.getMailList", item);
    	         Map smap = null;
				 for (int k = 0; k < lists.size(); k++) {
					 smap =(Map) lists.get(k);
				  	 tmpContext += "\n\n" + this.getStringWithNullCheck(smap,"P_USER_NM");
				  	 tmpContext += "\n" + this.getStringWithNullCheck(smap,"P_USER_ID") + "/" + this.getStringWithNullCheck(smap,"PASSWD");
				  	 tmpContext += "\n" + this.getStringWithNullCheck(smap,"EMAIL");
				  	 suEmail += this.getStringWithNullCheck(smap,"EMAIL")  + ";";
				  }
	         } else {
	        	 tmpContext += "\n\n" +mm.getMessage("CPMG_1031", request.getUser().getLocale());  // 반려 되었습니다.
	         }
	         tmpContext += "\n\n\n" + suEmail;
	         
	         log.debug("------------Message--CPMG_2121-----\n" + tmpContext);
				
	         mail.setTextMessage(tmpContext);
	         mail.send();
	         return msg;
	  	} catch(Exception e) {
	  	     log.error("--Failed saveApproval !!--\n" + e.toString());
	  	     return msg;
	  	}
    }
     */
     
    /**
     * 반출문서 로그 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectDocLog(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
          GridData gdRes = new GridData();
       	
       	  try {
			 String dbName = pm.getString("component.sql.database");
			 
			 Map smap = request.getMap();
			 smap.put("ISPRIVATE",pm.getString("component.private.security"));

			 List lists = getList("mg.cp.selectDocLog"+getDBName(dbName), smap); 

			 for (int i = 0; i < lists.size(); i++) {
			  	map =(Map) lists.get(i);
			  	 
			  	for(int j=0; j < cols.length; j++ ) {
			  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
			  	}
			  }
			  return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	  log.error("--------selectDocLog--------------------\n" + e.toString());
         	  return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
     }
     
     /**
      * Doc Log ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelDocLog(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
       String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Export Document Log");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
  	           	       
				 String dbName = pm.getString("component.sql.database");
				 Map smap = request.getMap();
				 smap.put("ISPRIVATE",pm.getString("component.private.security"));
		       	 List lists = getList("mg.cp.selectDocLog"+getDBName(dbName), smap); 
         
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Export Document Log_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
     }
         
         
     /**
      * CP 문서정보 Repacking 인증
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse repackingDoc(IRequest request) throws Exception 
      {
   		 JSONObject obj = new JSONObject();
   		 String logData = request.getStringParam("log");
   		 String remoteIp ="";
   		 String msg = "";
   		 
   		 Locale locale = new Locale("en");
   		 String pLocale = request.getStringParam("j_language");
   		 if(pLocale != null && !"".equals(pLocale)) {
   			 locale = new Locale(pLocale);
   		 }
   		
    	 try {
    		String password  = ""; // (String)getItem("mg.cp.getPassword",request.getMap());
    		
    		// 개인별 수신자 관리
     		if("personal".equals(pm.getString("component.export.user"))) {
     			password  = (String)getItem("mg.cp.getPersonalPassword",request.getMap());
     		}else if("interface".equals(pm.getString("component.export.user"))){
     			
     			/*
     			//DB에 저장된 수신자 아이디 리스트
     			String r_user_id = (String)getItem("mg.cp.getInterfaceUser",request.getMap());
     			String[] rUserIds = r_user_id.split("\\|");
     			
     			//DB에 저장된 수신자 비밀번호 리스트
     			String r_user_pw = (String)getItem("mg.cp.getInterfacePassword",request.getMap());
     			String[] rUserPws = r_user_pw.split("\\|");
     			
     			for(int i=0; i< rUserIds.length; i++) { 
     				if(rUserIds[i].equals(request.getStringParam("USER_ID"))){
     					//log.info("일치하는 수신자 아이디 : " + rUserIds[i]);
     					password = rUserPws[i];
     					break;
     				}
     			}
     			*/
     			if("Y".equals(pm.getString("component.interface.password"))){
	     			Map param = new HashMap();
	     			param.put("DOC_NO", request.getStringParam("DOC_NO")); // 전달받은 문서번호
	     			param.put("R_USER_ID", request.getStringParam("USER_ID")); // 전달받은 수신자ID
	     			
	     			log.info("검색된 신청자ID" + (String)getItem("mg.cp.getInterfaceUser",request.getMap()));
	     			
	     			param.put("USER_ID", (String)getItem("mg.cp.getInterfaceUser",request.getMap())); // 전달받은 문서번호로 신청자 ID 검색
	     			
	     			password = (String)getItem("mg.cp.getInterfacePassword", param); // 신청자ID와 수신자ID로 비밀번호 검색
	     			//log.info("검색된 수신자 Password : " + password);
     			}else{
     				Map param = new HashMap();
	     			param.put("DOC_NO", request.getStringParam("DOC_NO")); // 전달받은 문서번호
	     			param.put("R_USER_ID", request.getStringParam("USER_ID")); // 전달받은 수신자ID
	     			
     				password = (String)getItem("mg.cp.getInterfacePassword",param);
     				//log.info("검색된 수신자 Password : " + password);
     			}
     			
     		}
     		
     		else {
     			password  = (String)getItem("mg.cp.getPassword",request.getMap());
     		}
    		log.info("---getPassword---new----------------------\n" + password);
    		
    		
    		HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
    		remoteIp = httpRequest.getRemoteAddr();
    		
    		if("".equals(password)) {
    			msg = mm.getMessage("COMG_1003", locale);
    			obj.put("errcode", "-1");
    			obj.put("errmsg", msg);
    			getLogParseData(logData, remoteIp, "LOGIN_REPACK_FAIL", msg);
    			
    		} else if(password.equals(request.getMap().get("PASSWD"))) {
    			// 인증정보 수신
    			String dbName = pm.getString("component.sql.database");
    			Map map = null;
    			if("mssql".equals(dbName)){
    				map  = (Map)getItem("mg.cp.getUserPolicy",request.getMap());
    			}else{
    				map  = (Map)getItem("mg.cp.getUserPolicy_" + dbName ,request.getMap());
    			}
    			
    			log.info("---repackingDoc------1-------------------\n");
    			// 접근금지
    			String modStop = this.getStringWithNullCheck(map, "MOD_STOP");
    			if("1".equals(modStop)) {
    				msg = mm.getMessage("CPMG_1006", locale);
    				obj.put("errcode", "-1");
        			obj.put("errmsg", msg);
        			getLogParseData(logData, remoteIp, "LOGIN_REPACK_FAIL", msg);
        			return write(obj.toString());
    			}
    			
    			log.info("---repackingDoc------2-------------------\n" + map.toString());
    			// 접근 만료 설정
    			String policy = this.getStringWithNullCheck(map, "POLICY");
    			String expireDt = this.getStringWithNullCheck(map, "EXPIRE_DT");
    			DateFormat formatter ;
				if(expireDt.contains("-") ) {
					formatter = new SimpleDateFormat("yyyy-MM-dd");
				} else {
					formatter = new SimpleDateFormat("yyyyMMdd");
				}
    			
				log.info("---repackingDoc------2.0-------------------\n" + policy);
    			Object[] params = {expireDt};
    			msg = mm.getMessage("CPMG_1007", params, locale);       		  	
       		  	if("1".equals(policy.substring(1, 2))) {   // 만료기간 체크
       		  	log.info("---repackingDoc------2.1-------------------\n" + policy);
	    			if(expireDt == null || "".equals(expireDt)) {
	    				obj.put("errcode", "-1");
	        			obj.put("errmsg", msg);
	        			getLogParseData(logData, remoteIp, "LOGIN_REPACK_FAIL", msg);
	        			return write(obj.toString());
	    			} else {
	    				log.info("---repackingDoc------2.5-------------------\n" + expireDt);
	    				Date expDt = formatter.parse(expireDt);
						if(expDt.compareTo(new Date()) < 0 ) {
		    				obj.put("errcode", "-1");
		        			obj.put("errmsg", msg);
		        			getLogParseData(logData, remoteIp, "LOGIN_REPACK_FAIL", msg);
		        			return write(obj.toString());
						}
	    			}    			    		
       		  	}
       		  	
       		    log.info("---repackingDoc------3-------------------\n");
       		 
       		  	// Repacking 문서번호 취득
       		  	DateFormat docFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
    		  	String cpDocNo = docFormatter.format(new Date()) + getRandomString(6);
       		  	
    		    // Repacking 등록
    		  	Map itm = request.getMap();
    		  	if(itm.get("RE_DOC_NO") == null){
    		  		log.info("Repacking DocNo가 null----------------------------------------------------------------------------");
    		  		itm.put("RE_DOC_NO", cpDocNo);
    		  	}else{
    		  		cpDocNo=request.getStringParam("RE_DOC_NO");
    		  	}

    		  	if("2".equals(pm.getString("component.site.product.version"))){
	    		  	 Object fileInfo = JSONValue.parse(logData);
				     JSONObject jData = (JSONObject)fileInfo;
					 itm.put("EXE_FILE", jData.get("FILENAME"));
    		  	}
    		  	//log.info("리패킹 파일 개수 : " + request.getStringParam("FILE_LIST").split("\\|").length);
    		  	itm.put("FILE_NUM", request.getStringParam("FILE_LIST").split("\\|").length); //파일 리스트 split 하여 개수 저장
    		  	
           		createObject("mg.cp.saveRepackingDoc"+getDBName(dbName), itm);
                 		  	
       		  	// Repacking 사용자 등록 - 수신자가 다수일 경우 리패킹문서 수신자도 다수로 생성하여야 함
           		
           		//상위문서 수신자 수를 가져온다
           		int userCnt = (Integer)getItem("mg.cp.getUserCnt",request.getMap());
           		
           		//log.info("상위문서의 수신자 수 : " + userCnt);
           		           		
           		//상위문서 수신자 수만큼 리패킹문서 수신자를 생성한다
           		for(int i=1;i<=userCnt;i++){
           			itm.put("SEQ", i);
           			//log.info("itm Map객체 내용 : " + itm.toString());
           			//Map userInfo = (Map)getItem("mg.cp.getUserInfo",itm);
           			//userInfo.put("RE_DOC_NO", cpDocNo);
           			
           			createObject("mg.cp.saveRepackingUser"+getDBName(dbName), itm);
           		}
           		
           		
           		
           	    log.info("---repackingDoc------4-------------------\n");
    			
           		// Repacking 문서 상태 등록  -- Repacking 문서의 상태를 신규로 등록한다.
           		
    		  	String scheNo = docFormatter.format(new Date()) + getRandomString(6);
    		  	itm.put("SCHE_NO", scheNo);
    		  	itm.put("DOC_NO", cpDocNo); // Repacking 문서번호
	       	 	itm.put("CP_STS_CD", "06");
	       	 	createObject("mg.cp.saveJobSchedule"+getDBName(dbName), itm); 
	       	 			
              	// 재배포 문서번호 Return
        		obj.put("DOC_NO", cpDocNo);
        		obj.put("errcode", "0");
    			obj.put("errmsg", "Success");
    			
    			getLogParseData(logData, remoteIp, "LOGIN_REPACK", "Success");
    		} else {
    			msg = mm.getMessage("COMG_1004", locale);
    			obj.put("errcode", "-1");
    			obj.put("errmsg", msg);
    			getLogParseData(logData, remoteIp, "LOGIN_REPACK_FAIL", msg);
    		}   
    		return write(obj.toString());
    		    		
         } catch(Exception e) {
       	 	log.error("---repackingDoc-------------------------\n" + e.getMessage());
       	    msg = mm.getMessage("COMG_1005", locale);
       	 	obj.put("errcode", "-1");
			obj.put("errmsg", msg + "\n" + e.toString());
			getLogParseData(logData, remoteIp, "LOGIN_REPACK_FAIL", msg);
			return write(obj.toString());
         }
      }
      
      private String getJsonItemValue(String jsonData, String KeyName)
      {
    	  try {
	          Object obj = JSONValue.parse(jsonData);
	          JSONObject jData = (JSONObject)obj;
	          return (String)jData.get(KeyName);
    	  } catch(Exception e) {
    		  log.error("---getJsonItemValue : Key : " + KeyName + "----------\n" + jsonData + "\n"+ e.getMessage());
    		  return "";
    	  }
      }
      
      private boolean getLogParseData(String logData, String RemoteIp, String WorkType, String Msg)
      {
    	  try{
				 Map item = new HashMap();
			        
			     Object obj = JSONValue.parse(logData);
			     JSONObject jData = (JSONObject)obj;
			      
			     item.put("DOC_NO", jData.get("DOC_NO"));
			     item.put("USER_ID", jData.get("USER_ID"));
			     item.put("USER_NM", getItemData("mg.cp.getExportUserNm", item));
			  	 item.put("IP_ADDR", jData.get("IP_ADDR"));
			  	 item.put("IP_REMOTE", RemoteIp);
			  	 item.put("MAC_ADDR", jData.get("MAC_ADDR"));
			  	 item.put("WORKGROUP", jData.get("WORKGROUP"));
			  	 item.put("WORK_TYPE", WorkType);
			  	 item.put("MESSAGE", Msg);
			  	 item.put("COUNTRY_CD", jData.get("COUNTRY_CD"));
			  	 item.put("PC_LOGIN_ID", jData.get("PC_LOGIN_ID"));
			     item.put("ISPRIVATE", pm.getString("component.private.security"));
			     if("2".equals(pm.getString("component.site.product.version"))){
			    	 item.put("FILE_NM", jData.get("FIELNAME"));
			     }
			     
			     //20160704:  상신브레이크 반출함 상세 로그내역 저장을 위해 추가
			     item.put("LOG_CL_CD", "C");
			     item.put("OUT_UNIT_CD", "00");//반출 시점 로그가 아님. 05 WEB
			     item.put("OUT_URL","");//
			     
			  	String dbName = pm.getString("component.sql.database");
			  	createObject("mg.cp.saveDocLog"+getDBName(dbName), item); 
			  	return true;
    	  }catch(Exception e){
    		  log.error("-----------------getLogParseData----------------------------------------------"+e.toString());
    		  return false;
    	  }
      	
      }
                  
      private  boolean authLdap(String userId, String passwd, String ErrMsg, Locale locale, String logData, String remoteIp)
      {
    	    try {
    	    	// LDAP Context
    	    	String ldapDomain = pm.getString("cliptorplus.LDAP.domain");
    	  	    String ldapPath = pm.getString("cliptorplus.LDAP.path");
    	  	    String ldapFilter = pm.getString("cliptorplus.LDAP.filter");
    	  	    boolean isSuccess=false;
  	  	    
    	  	    log.info("------------ldapDomain path----------\n" + ldapDomain + " : " + ldapPath);
  	  	    
  	  	    // String securityPrincipal = "uid=" + uid + "," + providerUrl.substring(providerUrl.lastIndexOf("/") + 1);
    	    	Hashtable<String, String> properties = new Hashtable<String, String>();
    	    	properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    	    	properties.put(Context.PROVIDER_URL, ldapDomain);
    	    	properties.put(Context.SECURITY_AUTHENTICATION, "simple");
    	    	properties.put(Context.SECURITY_PRINCIPAL, ldapPath + userId + ldapFilter);
    	    	properties.put(Context.SECURITY_CREDENTIALS, passwd);
    	    	
    	    	try {
  				DirContext ctx = new InitialDirContext(properties);
  				log.debug("------Ldap : DirContext Success  \n");
  	            ctx.close();
  	            ERR_Message = "Ldap Login Success ";
  	            
  	          // COMG_1003=등록되지 않은 수신자 입니다.
    		  // COMG_1004=비밀번호가 맞지 않습니다.
    		  // COMG_1005=비밀번호 인증시 오류가 발생 되었습니다.
  	            
  	  	  	} catch(AuthenticationException e) {
  	  	  		ERR_Message = mm.getMessage("COMG_1004", locale);
  	  	  		log.error("---authLdap---\n" + ERR_Message);
  	  	  		getLogParseData(logData, remoteIp, "LOGIN_FAIL", ERR_Message);
  	  	  		return false;
  	  	  	} catch(InvalidNameException e) {
  		  	  	// ErrMsg = "-Ldap Fail : User not Found."+ e.toString();
  	  	  		ERR_Message  = mm.getMessage("COMG_1003", locale);
  	  	  		log.error("---authLdap---\n" + ERR_Message);
  	  	  	    getLogParseData(logData, remoteIp, "LOGIN_FAIL", ERR_Message);
  	  	  		return false;
  	  	  	} catch(NamingException e) {
  		  	  	// ErrMsg = "-Ldap Fail : NamingException."+ e.toString();
  	  	  		ERR_Message  = mm.getMessage("COMG_1005", locale);
  	  	  		log.error("---authLdap---\n" + ERR_Message);
  	  	  	    getLogParseData(logData, remoteIp, "LOGIN_FAIL", ERR_Message);
  	  	  		return false;
  	  	  	} 
    	    return true;
        } catch(Exception e) {
     	    log.error("-Fail CP Authentication--------------------------------------------------------------\n" + e.toString());
     	    ERR_Message  = mm.getMessage("COMG_1005", locale);
       	 	return false;
        }
      }
      
      /**
       * CP 문서 열람 인증 
       * @param request
       * @return
       * @throws Exception
       * http://192.168.1.121:8080/cp/approval.do?mod=authUser&DOC_NO=20140426141043022581&USER_ID=000001&PASSWD=1234
       */
      private IResponse authUser(IRequest request) throws Exception 
      {
    	  // Creation of Json Response 
		 JSONObject obj = new JSONObject();
		 String logData = request.getStringParam("log");
		 String msg="";
		 
		 Locale locale = new Locale("en");
		 String pLocale = request.getStringParam("j_language");
		 if(pLocale != null && !"".equals(pLocale)) {
			 locale = new Locale(pLocale);
		 }
		    		
    	 try {
    		HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
    		String remoteIp = httpRequest.getRemoteAddr();
    		
    		// 작성자 여부 확인 
    		if(pm.getBoolean("cliptorplus.requester.auth")) {   // 작성자 본인이 확인 가능할 수 있도록 
	    		if( Integer.parseInt(getItem("mg.cp.confirmDocMaster", request.getMap()).toString() ) > 0 ) {
	    			log.debug("---confirmDocMaster----------------------------------\n YES");
	    			return write(makeAuthPolicy(request.getMap(), logData, remoteIp, true, locale));
	            }
    		}
    		
    		String password="";
    		String ExportAuthectication = pm.getString("cliptorplus.export.authentication");
    		
    		if("ldap".equals(ExportAuthectication)) {
    			String authMsg="";
    			ERR_Message = "";
    			String userId = request.getStringParam("USER_ID");
    			String passWd = request.getStringParam("PASSWD");
    			if(authLdap(userId, passWd, authMsg, locale, logData, remoteIp)) {
    				Map subMap = request.getMap();
    				log.info("------authLdap : true-------subMap----123--------------------------------------------------\n" + subMap.toString());
    				return write(makeAuthPolicy(subMap, logData, remoteIp, false, locale));
    			} else {
    				obj.put("errcode", "-1");
    				// 20160704. CPD 버전 errocode2 추가, 기존 errmsg의 문구로 exe가 판단하여 보냄, 사이트 마다 달라질 수 있어 코드를 주고 받기로 변경 
    				obj.put("errcode2", "LDAP_ERROR"); //LADP 인증시 오류 코드 
    	 			obj.put("errmsg", ERR_Message);   //비밀번호 인증시 오류가 발생 되었습니다
    	 			return write(obj.toString());
    			}
    			
    		} else {  			
    			// CP 내 인증처리

    	     		if("personal".equals(pm.getString("component.export.user"))) {
            			// 개인별 수신자 관리
    	     			password  = (String)getItem("mg.cp.getPersonalPassword",request.getMap());
    	     			log.info("---getPassword----personal--------------------------------------------------\n" + password);
    	     			
    	     			if("".equals(password)) {
        	     			msg = mm.getMessage("COMG_1003", locale);
        	     			obj.put("errcode", "-1"); // COMG_1003
        	     			obj.put("errcode2", "USER_NULL");//등록되지 않은 수신자입니다.
        	     			obj.put("errmsg", msg);
        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
        	     			log.error("---authUser----------------------------------------------------------\n" + msg);  
        	     		} else if(password.equals(request.getMap().get("PASSWD"))) { 
        	     			return write(makeAuthPolicy(request.getMap(), logData, remoteIp, false, locale));
        	     		} else {
        	     			msg = mm.getMessage("COMG_1004", locale);
        	     			obj.put("errcode", "-1");
        	     			obj.put("errcode2", "PASSWD_WRONG");//비밀번호가 맞지 않습니다.
        	     			obj.put("errmsg", msg);
        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
        	     		}
    	     			
    	     		}else if("interface".equals(pm.getString("component.export.user"))) {

	     				// --------------------------------cryptorSHA 암호화 인증 (MD5)------------------------------------------------
    	     			/*
    	     			//DB에 저장된 수신자 아이디 리스트
    	     			String r_user_id = (String)getItem("mg.cp.getInterfaceUser",request.getMap());
    	     			String[] rUserIds = r_user_id.split("\\|");
    	     			
    	     			//DB에 저장된 수신자 비밀번호 리스트
    	     			String r_user_pw = (String)getItem("mg.cp.getInterfacePassword",request.getMap());
    	     			String[] rUserPws = r_user_pw.split("\\|");
    	     			
    	     			for(int i=0; i< rUserIds.length; i++) { 
    	     				if(rUserIds[i].equals(request.getStringParam("USER_ID"))){
    	     					//log.info("일치하는 수신자 아이디 : " + rUserIds[i]);
    	     					password = rUserPws[i];
    	     					break;
    	     				}
    	     			}*/
    	     			if("Y".equals(pm.getString("component.interface.password"))){
    	     				// SYSPERSONALUSER에 수신자 비밀번호 관리시
	    	     			Map param = new HashMap();
	    	     			param.put("DOC_NO", request.getStringParam("DOC_NO")); // 전달받은 문서번호
	    	     			param.put("R_USER_ID", request.getStringParam("USER_ID")); // 전달받은 수신자ID
	    	     			
	    	     			//log.info("검색된 신청자ID" + (String)getItem("mg.cp.getInterfaceSysUser",request.getMap()));
	    	     			
	    	     			param.put("USER_ID", (String)getItem("mg.cp.getInterfaceSysUser",request.getMap())); // 전달받은 문서번호로 신청자 ID 검색
	    	     			
	    	     			password = (String)getItem("mg.cp.getInterfaceSysPassword", param); // 신청자ID와 수신자ID로 비밀번호 검색
	    	     			//log.info("검색된 수신자 Password : " + password);
    	     			}else{
    	     				Map param = new HashMap();
	    	     			param.put("DOC_NO", request.getStringParam("DOC_NO")); // 전달받은 문서번호
	    	     			param.put("R_USER_ID", request.getStringParam("USER_ID")); // 전달받은 수신자ID
	    	     			
	    	     			if(!"LGE".equals(pm.getString("component.site.company"))){
	    	     				password = (String)getItem("mg.cp.getInterfacePassword",param);
	    	     				if(password == null){
	    	     					password="";
	    	     				}
	    	     			}else{
	    	     				password = (String)getItem("mg.cp.getInterfacePassword_LGE",param);
	    	     				log.info("----------------LGE---------password---------"+password);
	    	     				if(password == null){
	    	     					password="";
	    	     				}
	    	     			}//log.info("검색된 수신자 Password : " + password);
    	     			}
    	     			
    	     			if("".equals(password)) {
    	     				// DB에 저장된 비밀번호 반환값이 없는경우
        	     			msg = mm.getMessage("COMG_1003", locale);
        	     			obj.put("errcode", "-1"); // COMG_1003
        	     			obj.put("errcode2", "USER_NULL");//등록되지 않은 수신자입니다.
        	     			obj.put("errmsg", msg);
        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
        	     			log.error("---authUser----------------------------------------------------------\n" + msg);  
        	     		}else{
	    	     			    	     			
	    	     			//인증요청한 암호화 비밀번호
	    	     			String encryptPassword = (String)request.getMap().get("PASSWD");
	    	     			log.info("request-----------------PASSWD : "+encryptPassword);
	    	     			// 인증요청한 일반 비밀번호를 암호화한다
	    	     			//byte[] salt = Base64.decodeBase64(key);
	    	     			//String encryptPassword = Sha256Util.getEncrypt(rqPassword, salt);
	    	     			//log.info("인증요청 비밀번호 : " + rqPassword);
	    	     			
	    	     			//String testPassword = cryptorSHA.JinEncodeSHA256("@#$%%!!@!@#~!~!%^%^^&*%^&%^$%^#%%#$%#$@$^%^%^&%*%$#~~!@!@#!@$#!@#$#$");
	    	     			//log.info("특수문자 비밀번호 테스트 : " + testPassword);
	    	     			if("LGE".equals(pm.getString("component.site.company"))){
	    	     				ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	    	     				//encryptPassword =java.net.URLDecoder.decode(encryptPassword, "utf-8");
	    	     				//log.info("URLDecoder------------------------------------------------------PASSWD : "+encryptPassword);
	    	     				encryptPassword = crypto.decodeMG(encryptPassword);
	    	     				log.info("decodeMG----------------------------------------------------------"+encryptPassword.trim());
	    	     				EncrytPw encryptEncrytPw = new EncrytPw();
	    	     				encryptPassword = encryptEncrytPw.digest(encryptPassword.trim()); //decode 시 공백 하나가 붙어서 나옴. 따라서 trim으로 공백 제거 처리
	    	     			}
	    	     			
	    	     			//비밀번호 비교
	    	     			if(password.equals(encryptPassword)){
	    	     				//비밀번호 일치
	    	     				log.info("------PASSWD True-----");
	    	     				return write(makeAuthPolicy(request.getMap(), logData, remoteIp, false, locale));
	    	     			}else{
	    	     				log.info("------PASSWD False-----");
	    	     				//비밀번호 불일치
	    	     				msg = mm.getMessage("COMG_1004", locale);
	        	     			obj.put("errcode", "-1");
	        	     			obj.put("errcode2", "PASSWD_WRONG");//비밀번호가 맞지 않습니다.
	        	     			obj.put("errmsg", msg);
	        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
	    	     			}
        	     		}
    	     			
	     				// -------------------------------------------------------------------------------------------------------
    	     			
    	     			
    	     			
    	     			
    	     			/*
        	     			// --------------------------------SHA256 키값 사용 암호화 인증--------------------------------------
        	     			
        	     			//DB에 저장된 수신자 아이디 리스트
        	     			String r_user_id = (String)getItem("mg.cp.getInterfaceUser",request.getMap());
        	     			String[] rUserIds = r_user_id.split("\\|");
        	     			
        	     			//DB에 저장된 수신자 비밀번호 리스트
        	     			String r_user_pw = (String)getItem("mg.cp.getInterfacePassword",request.getMap());
        	     			String[] rUserPws = r_user_pw.split("\\|");
        	     			
        	     			//DB에 저장된 키값 리스트
        	     			String pw_key = (String)getItem("mg.cp.getInterfaceKey",request.getMap());
        	     			String[] pwKey = pw_key.split("\\|");
        	     			
        	     			String key = "";
        	     			
        	     			//인증요청한 수신자 아이디를 찾으면 해당 비밀번호/키값을가져온다.
        	     			for(int i=0; i< rUserIds.length; i++) { 
        	     				if(rUserIds[i].equals(request.getStringParam("USER_ID"))){
        	     					//log.info("일치하는 수신자 아이디 : " + rUserIds[i]);
        	     					password = rUserPws[i];
        	     					key = pwKey[i];
        	     					break;
        	     				}
        	     			}

        	     			
        	     			if("".equals(password)) {
        	     				// DB에 저장된 비밀번호 반환값이 없는경우
            	     			msg = mm.getMessage("COMG_1003", locale);
            	     			obj.put("errcode", "-1"); // COMG_1003
            	     			obj.put("errmsg", msg);
            	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
            	     			log.error("---authUser----------------------------------------------------------\n" + msg);  
            	     		}else{
    	    	     			    	     			
    	    	     			//인증요청한 일반 비밀번호
    	    	     			String rqPassword = (String)request.getMap().get("PASSWD");
    	
    	    	     			// 키값으로 인증요청한 일반 비밀번호를 암호화한다
    	    	     			byte[] salt = Base64.decodeBase64(key);
    	    	     			String encryptPassword = Sha256Util.getEncrypt(rqPassword, salt);
    	    	     			
    	    	     			//비밀번호 비교
    	    	     			if(password.equals(encryptPassword)){
    	    	     				//비밀번호 일치
    	    	     				log.info("------비밀번호 일치-----");
    	    	     				return write(makeAuthPolicy(request.getMap(), logData, remoteIp, false, locale));
    	    	     			}else{
    	    	     				log.info("------비밀번호 불일치-----");
    	    	     				//비밀번호 불일치
    	    	     				msg = mm.getMessage("COMG_1004", locale);
    	        	     			obj.put("errcode", "-1");
    	        	     			obj.put("errmsg", msg);
    	        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
    	    	     			}
            	     		}
        	     			// -------------------------------------------------------------------------------------------------------
        	   			*/
    	     			
    	     				
    	     			/*
    	     			// --------------------------------암호화하지 않는 인증------------------------------------------------
        	     			
        	     			//DB에 저장된 수신자 아이디 리스트
        	     			String r_user_id = (String)getItem("mg.cp.getInterfaceUser",request.getMap());
        	     			String[] rUserIds = r_user_id.split("\\|");
        	     			
        	     			//DB에 저장된 수신자 비밀번호 리스트
        	     			String r_user_pw = (String)getItem("mg.cp.getInterfacePassword",request.getMap());
        	     			String[] rUserPws = r_user_pw.split("\\|");
        	     			
        	     			//인증요청한 수신자 아이디를 찾으면 해당 비밀번호를 가져온다.
        	     			for(int i=0; i< rUserIds.length; i++) { 
        	     				if(rUserIds[i].equals(request.getStringParam("USER_ID"))){
        	     					//log.info("일치하는 수신자 아이디 : " + rUserIds[i]);
        	     					password = rUserPws[i];
        	     					break;
        	     				}
        	     			}
        	     			
        	     			//password  = (String)getItem("mg.cp.getInterfacePassword",request.getMap());
        	     			
        	     			log.info("---getPassword----interface--------------------------------------------------\n" + password);
        	     			
        	     			if("".equals(password)) {
            	     			msg = mm.getMessage("COMG_1003", locale);
            	     			obj.put("errcode", "-1"); // COMG_1003
            	     			obj.put("errmsg", msg);
            	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
            	     			log.error("---authUser----------------------------------------------------------\n" + msg);  
            	     		} else if(password.equals(request.getMap().get("PASSWD"))) { 
            	     			return write(makeAuthPolicy(request.getMap(), logData, remoteIp, false, locale));
            	     		} else {
            	     			msg = mm.getMessage("COMG_1004", locale);
            	     			obj.put("errcode", "-1");
            	     			obj.put("errmsg", msg);
            	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
            	     		}
        	     			// -------------------------------------------------------------------------------------------------------
        	     	*/		 
    	     		
    	     		}else{
            			// 회사전체 수신자 관리
    	     			log.info("---public 수신자 처리------pm.getString(\"cliptorplus.auth.key\")------------------------------------------\n" + pm.getString("cliptorplus.auth.key"));
    	     			
    	     			String parampw = request.getStringParam("PASSWD");
    	     			String decodepw = crypto.decodeAES(parampw, pm.getString("cliptorplus.auth.key"));
    	     			String comppw = crypto.encode(decodepw);
    	     			
    	     			// cryptorSHA.JinEncodeSHA256((String)request.getMap().get("PASSWD"));
    	    	     			    	     			
    	     			// prog by cyLim
    	     			// --> iXVDR 사용자는 SYSUSER에서 사용자를 관리 한다.
    	     			if("iXVDR".equals(pm.getString("component.site.product")))  password  = (String)getItem("mg.cp.getPasswordSysuser",request.getMap());    
    	     			else  password  = (String)getItem("mg.cp.getPassword",request.getMap());
    	     				
    	     			if(password == null) password="";
    	     			String pinNo = "";
    	     			
    	     			/*   iXVDR 에서 필요하지 않음
    	     			String pinNo = (String)getItem("mg.cp.getPinNo", request.getMap());
    	     			if(pinNo == null) {
    	     				pinNo="";
    	     			} else{
    	     				pinNo = SeedEncryptUtil.seedDecrypt(pinNo);
    	     				pinNo = cryptorSHA.JinEncodeSHA256(pinNo);
    	     			}
    	     			*/
    	     			
    	     			log.info("---Password / comppw--------------------------------------------------\n" + password+" / " + comppw);
    	     			// log.info("---Password / pinNo--------------------------------------------------\n" + password+" / " +pinNo);
    	     			
    	     			//log.info("인증요청 비밀번호 : " + request.getMap().get("PASSWD"));
    	     			//log.info("암호화요청 비밀번호 : " + cryptorSHA.JinEncodeSHA256((String)request.getMap().get("PASSWD")));
    	     			if("".equals(password) && "".equals(pinNo)) {
    	     				// 등록되지 않은 수신자
        	     			msg = mm.getMessage("COMG_1003", locale);
        	     			obj.put("errcode", "-1"); // COMG_1003
        	     			obj.put("errcode2", "USER_NULL");//등록되지 않은 수신자입니다.
        	     			obj.put("errmsg", msg);
        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
        	     			log.error("---authUser----------------------------------------------------------\n" + msg);  
        	     		} else	if(password.equals(comppw)) {    // prog by cylim  : 비빌번호 암호화 방식 변경 
        	     			// 비밀번호 암호화 비교 일치
        	     			return write(makeAuthPolicy(request.getMap(), logData, remoteIp, false, locale));
        	     		} else if(pinNo.equals(request.getMap().get("PASSWD"))){
        	     			// Pin번호 암호화 비교 일치 
        	     			return write(makeAuthPolicy(request.getMap(), logData, remoteIp, false, locale));
        	     		} else {
        	     			// 비밀번호 암호화 비교 불일치
        	     			msg = mm.getMessage("COMG_1004", locale);
        	     			obj.put("errcode", "-1");
        	     			obj.put("errcode2", "PASSWD_WRONG");//비밀번호가 맞지 않습니다.
        	     			obj.put("errmsg", msg);
        	     			getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
        	     		}	
    	     		}
    		}
    		return write(obj.toString());
    		
         } catch(Exception e) {
       	     msg = mm.getMessage("COMG_1005", locale);
       	     log.error("---authUser  Exception Error --------------------------\n" + e.toString());
       	     obj.put("errcode", "-1");
       	     obj.put("errcode2", "AUTH_ERROR");//사용자 인증중 오류가 발생되었습니다.
			 obj.put("errmsg", msg);   //사용자 인증중 오류가 발생되었습니다.
			 return write(obj.toString());
         }
      }
              
      // isRequester = true --> 작성자 여부 확인
      //
      private String makeAuthPolicy(Map subMap, String logData, String remoteIp, boolean isRequester, Locale locale)
      {
    	   JSONObject obj = new JSONObject();
    	   String msg="";
    	   boolean resultLog;
    	   try {
    		   
    		   if("".equals(logData) || logData == null) {
					msg =  mm.getMessage("CPMG_1048", locale);
					obj.put("errcode", "-1");
					obj.put("errcode2", "LOG_ERROR");//사용자 인증 시도시, 헤당 로그 정보가 존재하지 않습니다.
	    			obj.put("errmsg", msg);
	    			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
	    			log.error("---makeAuthPolicy-----------------------------------------------------------\n" + msg + "\n----resultLog : "+resultLog);
	    			return obj.toString();
				}
    		   
    		   if(isRequester) {
    			   
    			   log.info("---makeAuthPolicy---RequestMap--------------------------------------------------------\n" + subMap.toString() );
    			   
    			    String dbName = pm.getString("component.sql.database");
    			    Map map  = (Map)getItem("mg.cp.getCpDocInfo"+getDBName(dbName),subMap);
    
    		       	 
    			    obj.put("POLICY", "00010");
		    		obj.put("POLICY_DOC", "10011");
		    		obj.put("MOD_STOP", "0");
		    		obj.put("EXPIRE_DT", "");
		    		obj.put("READ_COUNT", "");
		    		obj.put("CUR_COUNT", "0");
		    		obj.put("MESSAGE", "작성자 본인이 문서오픈");
		    		obj.put("MAC_LIST", "");
		    		obj.put("IN_PROCESS", this.getStringWithNullCheck(map,"IN_PROCESS"));
		    		obj.put("WAITING_TIME", this.getStringWithNullCheck(map,"WAITING_TIME"));
		    		obj.put("EXE_RULE", this.getStringWithNullCheck(map,"EXE_RULE"));
		    		obj.put("EXE_FILE", this.getStringWithNullCheck(map,"EXE_FILE"));
		    		obj.put("errcode", "0");
					obj.put("errmsg", "Success");
					
					resultLog = getLogParseData(logData, remoteIp, "LOGIN", "Success [작성자 본인 로그인]");
					return obj.toString();
    		   
    		   } else {
			       // 인증정보 수신
    			   Map map;
    			   String modStop;
    			   log.info("---makeAuthPolicy----map check-------------------------------------------------------\n");
    			   
					try {
						
	    			    String dbName = pm.getString("component.sql.database");
	    			    map  = (Map)getItem("mg.cp.getUserPolicy"+getDBName(dbName),subMap);

						if(map.isEmpty()) {  // 수신자 등록이 않 되어 있다면
							msg = mm.getMessage("CPMG_1038", locale);   // CPMG_1038=수신자 등록이 되어 있지 않은 ID 입니다.
							obj.put("errcode", "-1");
							obj.put("errcode2", "USER_NULL");//등록되어 있지 않은 수신자 ID 입니다.
			    			obj.put("errmsg", msg);
			    			log.info("---makeAuthPolicy----0-------------------------------------------------------\n" + msg);
			    			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
			    			return obj.toString();
						}
					    			 
						// 접근금지
						modStop = this.getStringWithNullCheck(map, "MOD_STOP");
						if("1".equals(modStop)) {
							msg = mm.getMessage("CPMG_1006", locale);
							obj.put("errcode", "-1");
							obj.put("errcode2", "MOD_STOP_Y");//[MOD_STOP = 1] 문서에 대한 열람 금지가 설정되었습니다.
			    			obj.put("errmsg", msg);
			    			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
			    			log.info("---makeAuthPolicy----1-------------------------------------------------------\n" + msg);
			    			return obj.toString();
						}else if("2".equals(modStop)) {
							msg = mm.getMessage("CPMG_1008", locale);
							obj.put("errcode", "-1");
							obj.put("errcode2", "MOD_STOP_D");//[MOD_STOP = 2] 문서에 대한 강제 삭제가 설정되었습니다.
			    			obj.put("errmsg", msg);
			    			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
			    			log.info("---makeAuthPolicy----1-------------------------------------------------------\n" + msg);
			    			return obj.toString();
						}
						
					} catch(Exception e) {
						msg = mm.getMessage("CPMG_1038", locale);   // CPMG_1038=수신자 등록이 되어 있지 않은 ID 입니다.
						obj.put("errcode", "-1");
						obj.put("errcode2", "USER_NULL");//등록되어 있지 않은 수신자 ID 입니다.
		    			obj.put("errmsg", msg);
		    			log.error("---makeAuthPolicy----0-------------------------------------------------------\n" + msg);
		    			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
		    			return obj.toString();
					}
						
					// 접근 만료 설정
					String policy = this.getStringWithNullCheck(map, "POLICY");
					String expireDt = this.getStringWithNullCheck(map, "EXPIRE_DT").replaceAll("-", "");
					log.info("---makeAuthPolicy--- expireDt----------------------------------------------------\n" + expireDt);
					DateFormat formatter;
					// if(expireDt.contains("-") ) {
					//	formatter = new SimpleDateFormat("yyyy-MM-dd");
					//} else {
						formatter = new SimpleDateFormat("yyyyMMdd");
					// }
						
						log.info("---makeAuthPolicy-0-1-----------------------------------------------------\n" + msg);
								   		  				   		
			   		if("1".equals(policy.substring(1, 2))) {   // 만료기간 체크
	    				// msg = "Expiration time has passed.";
		    			if("".equals(expireDt) || expireDt == null) {
		    				Object[] params = {expireDt};
		    				msg = mm.getMessage("CPMG_1007", params, locale );
		    				
		    				obj.put("errcode", "-1");
		    				obj.put("errcode2", "EXPIRE_OVER");//[만기일: {0}] 문서를 열람할 수 있는 기간이 경과되었습니다.
		        			obj.put("errmsg", msg);
		        			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
		        			log.info("---makeAuthPolicy-----2------------------------------------------------------\n" + msg);
		        			return obj.toString();
		    			} else {
		    				Object[] params = {expireDt};
		    				msg = mm.getMessage("CPMG_1007", params, locale);
		    				try {
		    					String CompDt = DateUtils.addDay(expireDt, +1);
		    					log.info("---makeAuthPolicy--CompDt---------------------------------------------------------\n" + CompDt);
			    				Date expDt = formatter.parse(CompDt);
			    				log.info("---makeAuthPolicy--expDt---------------------------------------------------------\n" + expDt.toString());
			    				log.info("---makeAuthPolicy--new Date()---------------------------------------------------------\n" + new Date().toString());
			    				
								if(expDt.compareTo(new Date()) < 0 ) {				    				
				    				obj.put("errcode", "-1");
				    				obj.put("errcode2", "EXPIRE_OVER");//[만기일: {0}] 문서를 열람할 수 있는 기간이 경과되었습니다.
				        			obj.put("errmsg", msg);
				        			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
				        			log.info("---makeAuthPolicy--3---------------------------------------------------------\n" + msg);
				        			return obj.toString();
								}
		    				}catch(Exception e) {
		    					obj.put("errcode", "-1");
		    					obj.put("errcode2", "EXPIRE_OVER");//[만기일: {0}] 문서를 열람할 수 있는 기간이 경과되었습니다.
			        			obj.put("errmsg", msg);
			        			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
			        			log.error("---makeAuthPolicy-------4----------------------------------------------------\n" + msg);
			        			return obj.toString();
		    				}
		    			}    			    		
			   		}
			   		  	
					// 허용 회수 초과 체크
				  	int curCnt = Integer.parseInt(getItem("mg.cp.countDocOpen", subMap).toString());
				  	if("1".equals(policy.substring(2, 3))) {
		    			int maxCnt = Integer.parseInt(this.getStringWithNullCheck(map, "READ_COUNT"));
		    			if(maxCnt > 0 && maxCnt<=curCnt) {
		    				Object[] params = {curCnt};
		    				msg = mm.getMessage("CPMG_1009", params, locale);
		    				// msg = "Eexceeded the allowed number of times.";
		    				obj.put("errcode", "-1");
		    				obj.put("errcode2", "READCNT_OVER");//[실행 횟수 = {0}] 문서에 대한 열람 허용 횟수가 초과되었습니다
		        			obj.put("errmsg", msg);
		        			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
		        			log.error("---makeAuthPolicy-------5----------------------------------------------------\n" + msg);
		        			return obj.toString();
		    			}
				  	}
				  	
				  	// MacAddress 체크
				  	String MacList = this.getStringWithNullCheck(map, "MAC_LIST").trim();
				  	log.info("---makeAuthPolicy-----MacAddress 체크--------------------------------------------------\n" + MacList);
					if(MacList != null && !"".equals(MacList)) {
						String MacAddr = getJsonItemValue(logData,"MAC_ADDR");
					  	if(!MacList.contains(MacAddr)) {  // Mac Address 정책이 있고 허용된 Mac Address가 아니면 어루
					  		msg = mm.getMessage("CPMG_1040", locale);  // 허용된 PC가 아닙니다.
					  		obj.put("errcode", "-1");
					  		obj.put("errcode2", "MAC_DIFF");//보안 문서는 지정된 PC에서만 사용 가능합니다.
		        			obj.put("errmsg", msg);
		        			resultLog = getLogParseData(logData, remoteIp, "LOGIN_FAIL", msg);
		        			log.error("---makeAuthPolicy-------6----------------------------------------------------\n" + msg);
		        			return obj.toString();
					  	}
					}
					log.info("---makeAuthPolicy-------7----------------------------------------------------\n");
					obj.put("POLICY", this.getStringWithNullCheck(map,"POLICY"));
		    		obj.put("POLICY_DOC", this.getStringWithNullCheck(map,"POLICY_DOC"));
		    		obj.put("MOD_STOP", modStop);
		    		obj.put("EXPIRE_DT", this.getStringWithNullCheck(map,"EXPIRE_DT"));
		    		obj.put("READ_COUNT", this.getStringWithNullCheck(map,"READ_COUNT"));
		    		obj.put("CUR_COUNT", String.valueOf(curCnt+1));
		    		obj.put("MESSAGE", this.getStringWithNullCheck(map,"MESSAGE"));
		    		obj.put("MAC_LIST", this.getStringWithNullCheck(map,"MAC_LIST"));
		    		obj.put("IN_PROCESS", this.getStringWithNullCheck(map,"IN_PROCESS"));
		    		obj.put("WAITING_TIME", this.getStringWithNullCheck(map,"WAITING_TIME"));
		    		obj.put("EXE_RULE", this.getStringWithNullCheck(map,"EXE_RULE"));
		    		obj.put("EXE_FILE", this.getStringWithNullCheck(map,"EXE_FILE"));
		    		
		    		//로그 수집을 위해 추가 전달
		    		obj.put("LOG_OPTION", pm.getString("cliptorplus.log.option"));
		    		obj.put("LOG_URL", pm.getString("cliptorplus.log.service.url"));
		    				
		    		obj.put("errcode", "0");
					obj.put("errmsg", "Success");
					// 
					log.info("---makeAuthPolicy-------8----------------------------------------------------\n");
					resultLog = getLogParseData(logData, remoteIp, "LOGIN", "Success");
					return obj.toString();
    		   }
    	   } catch(Exception e) {
    		    msg = "makeAuthPolicy Exception Error : " + e.getMessage();
				obj.put("errcode", "-1");
				obj.put("errcode2", "AUTH_POLICY_ERROR");//정책확인 중 오류 발생
   			    obj.put("errmsg", msg);
   			    
    		    log.error("---makeAuthPolicy Exception Error-----------------------------------------------------------\n" + e.getMessage()+"\n "+ e.toString());
    		   return obj.toString();
    	   }
      }
    	     
      /**
       * 반출 문서 접근로그 저장
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse saveDocLog(IRequest request) throws Exception 
      {
          String logData = request.getStringParam("log");

          Locale locale = new Locale("en");
 		  String pLocale = request.getStringParam("j_language");
 		  if(pLocale != null && !"".equals(pLocale)) {
 			 locale = new Locale(pLocale);
 		  }
 		          
          try {
          	 tx.begin();
          	          	
  	         Object obj = JSONValue.parse(logData);
  	         JSONObject jData = (JSONObject)obj;
  	         HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class); 
   	         
  	    	 Map item = new HashMap();
  	         item.put("DOC_NO", jData.get("DOC_NO"));//
  	         item.put("USER_ID", jData.get("USER_ID"));//
  	         item.put("USER_NM", getItemData("mg.cp.getExportUserNm", item));
  		  	 item.put("IP_ADDR", jData.get("IP_ADDR"));//
  		  	 item.put("IP_REMOTE", httpRequest.getRemoteAddr());
  		  	 item.put("MAC_ADDR", jData.get("MAC_ADDR"));//
  		  	 item.put("WORKGROUP", jData.get("WORKGROUP"));//
  		  	 item.put("WORK_TYPE", jData.get("WORK_TYPE"));//
  		  	 //item.put("MESSAGE", jData.get("MESSAGE"));//?
  		  	 item.put("COUNTRY_CD", jData.get("COUNTRY_CD"));//
  		  	 item.put("PC_LOGIN_ID", jData.get("PC_LOGIN_ID"));//
  		  	 item.put("ISPRIVATE", pm.getString("component.private.security"));
  		  	 
		     //20160704:  상신브레이크 반출함 상세 로그내역 저장을 위해 추가
		     item.put("LOG_CL_CD", "C");
		     item.put("OUT_UNIT_CD", "00");//다운로드 로그가 아님. 05 WEB
		     item.put("OUT_URL","");//
		     
  		  	if("2".equals(pm.getString("component.site.product.version"))){
  		  		if(jData.get("FILENAME") != null || jData.get("ACTIONTIME") != null || jData.get("MESSAGE") != null){
  	  		  		 item.put("MESSAGE", jData.get("MESSAGE"));//
  	  		  		 item.put("FILE_NM",  jData.get("FILENAME"));//
  	  		  		 String actionDate = jData.get("ACTIONTIME").toString();
  	  		  		 item.put("REG_DT", actionDate.substring(0,4)+"-"+actionDate.substring(4,6)+"-"+actionDate.substring(6,8)+" "+actionDate.substring(8,10)+":"+actionDate.substring(10,12)+":"+actionDate.substring(12,14));
  	  		  	 }
  		  	}
  		  	
  	         log.info("saveDocLog------------------------------------------------------------------------------------------------------------"+item.toString());
  	         String dbName = pm.getString("component.sql.database");
  	         createObject("mg.cp.saveDocLog"+getDBName(dbName), item); 
        	
  	         // 재배포 생성 성공이면 --> 상태값을 바꾼다.
  	         if("REPACKING".equals(jData.get("WORK_TYPE").toString())) {
	       	 	item.put("CP_STS_CD", "04");
	       	 	updateObject("mg.cp.updateSTS"+getDBName(dbName), item); 
  	         }
  	         tx.commit();
  	        
  	         JSONObject ret = new JSONObject();
  	         ret.put("errcode", "0");
  	         ret.put("errmsg", mm.getMessage("COMG_1002", locale));
  	         return write(ret.toString());
          } catch (Exception e) {
          	 tx.rollback();
          	 log.error("------------saveDocLog----------------------------------------------------------------\n" + e.toString());
          	 JSONObject ret = new JSONObject();
 	         ret.put("errcode", "-1");;
 	         ret.put("errmsg",mm.getMessage("COMG_1006", locale));
 	         return write(ret.toString());
          }
                    
      }
      
      /**
       * 반출파일 다운로드 
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse exeDownList(IRequest request) throws Exception 
       {
          String col = request.getStringParam("grid_col_id");
          String[] cols = col.split(",");
         	  
          Map map = null;
          GridData gdRes = new GridData();
         	
          try {
        	  Map gmap = request.getMap();
        	  gmap.put("LOGIN_ID", request.getUser().getId());
  	          gmap.put("LOCALE", request.getUser().getLocale().getLanguage());
  	            	      
	       	 String dbName = pm.getString("component.sql.database");
	       	 List lists = getList("mg.cp.exeDownList"+getDBName(dbName), gmap);
		
			 for (int i = 0; i < lists.size(); i++) {
			  	map =(Map) lists.get(i);
			  	 
			  	for(int j=0; j < cols.length; j++ ) {
			  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
			  	}
			  }
			  return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	  log.error("--------exeDownList-----------------------------\n" + e.toString());
         	  return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
       }
       private IResponse exeDownList_CPCOM(IRequest request) throws Exception 
       {
          String col = request.getStringParam("grid_col_id");
          String[] cols = col.split(",");
         	  
          Map map = null;
          GridData gdRes = new GridData();
         	
          try {
        	  Map gmap = request.getMap();
        	  gmap.put("LOGIN_ID", request.getUser().getId());
  	          gmap.put("LOCALE", request.getUser().getLocale().getLanguage());
  	          log.info("exeDownList_CPCOM");  	      
  	         
	       	  String dbName = pm.getString("component.sql.database");
			  List lists = getList("mg.cp.exeDownComList"+getDBName(dbName), gmap);

			 for (int i = 0; i < lists.size(); i++) {
			  	map =(Map) lists.get(i);
			  	 
			  	for(int j=0; j < cols.length; j++ ) {
			  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
			  	}
			  }
			  return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	  log.error("--------exeDownList-----------------------------\n" + e.toString());
         	  return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
       }
           
       private IResponse exeDownload(IRequest request) {
    	   Map map = null;
    	   
    	   try {
    		
    		  String userId;
 			  String userNm;
 			  String userLoc;
    		 //로그인없이 URL로 다운받을 경우
 			 if(request.getUser() == null){
 				 userId =request.getStringParam("USER_ID");
 				 userNm ="urlDownload";
 				 userLoc ="ko";
    	   	 }else{ 
				  userId =request.getUser().getId();
				  userNm=request.getUser().getName();
				  userLoc=request.getUser().getLocale().toString();
			  }
    		 Map smap = request.getMap();
    		 
    		 if(smap.get("security") !=null){
    			 ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
    			 if("1".equals(smap.get("security").toString())){
    				 String result =  crypto.decodeMG(request.getStringParam("DOC_NO"));
    				 smap.put("DOC_NO", java.net.URLDecoder.decode(result.trim(), "utf-8"));
    				 userId = java.net.URLDecoder.decode(crypto.decodeMG(userId).trim(),"utf-8");
    				 log.info("--------------------------------------exeDownload----------------------USER_ID"+userId);
    			 }
    			 log.info("exeDownload----------------------------------------------------------------------doc_no : "+smap.get("DOC_NO") +" / "+request.getStringParam("DOC_NO"));
    		 }
    		 
    		 String dbName = pm.getString("component.sql.database");
   	         String logFileName ="";
   	          smap.put("USER_ID", userId); 
   	          log.info("-----------------------------------------------------------------------------------------------------------------다운로드 호출");
   	          List lists = getList("mg.cp.exeFileInfo"+getDBName(dbName),smap);
   	          //if(request.getStringParam("USER_ID") !=null) { //반출함에서 메일 첨부 URL로 다운 받을 경우, 반출함 원문 반출 시 DocMaster에 저장하지 않기 때문에
   	         log.info("lists.size------------------------------------------------------------------------------------------"+lists.size());
   	          if(lists.size() ==0){ 
   	        	 
   	        	 smap.put("dbName", dbName);
   	        	  lists = getList("mg.cp.vfFileInfo"+getDBName(dbName), smap);
   	          }
	          String downFile = "";
	          String docNo = smap.get("DOC_NO").toString();
	          String worktype = "DOWNLOAD";
	          String logClCd ="C";
			  for (int i = 0; i < 1; i++) {
				 map =(Map) lists.get(i);

			  	 if("6".equals(this.getStringWithNullCheck(map, "AUTHORITY"))) {   // 원문반출이면		
			  		logClCd ="E";	  		
		   	         int downCnt = Integer.parseInt(getItem("mg.cp.downloadCntCheck", smap).toString());

			  		downFile = this.getStringWithNullCheck(map, "EXEPATH") + "/" + docNo.substring(0,8) + "/" + docNo + "/" + this.getStringWithNullCheck(map, "FILE_NAME");
			  		log.info("복호화 하기 전----------------------------------------------");
			  		if("Y".equals(pm.getString("cliptorplus.zip.export"))){ //웹에서 원문반출을 사용할지 여부
				   	      //다운로드 횟수 제한 : 로그에 쓰여진 사용자의 아이디를 카운트하여 횟수 제한을 둠.
				   	     if(getItem("mg.cp.selectReadCnt", smap) != null){
				   	    	 if(downCnt >= Integer.parseInt(getItem("mg.cp.selectReadCnt", smap).toString())){
				   	    		 return closePopupWindow(mm.getMessage("CPMG_1044"));
				   	    	 }  
				   	     }  	  			
			  			 String tmpFileName =this.getStringWithNullCheck(map, "EXE_FILE");
			  			logFileName=tmpFileName;
				  		 String extName = ".zip";
				  		 if(tmpFileName.contains(".")) extName ="";
				  		 downFile = this.getStringWithNullCheck(map, "EXEPATH") + "/" + docNo.substring(0,8) + "/" + docNo + "/" + tmpFileName+ extName;		
				  		 
			  		}else{//반출함에서 원문반출
			  			smap.put("OBJECT_ID",docNo);
			  		   //다운로드 횟수 제한 : 로그에 쓰여진 사용자의 아이디를 카운트하여 횟수 제한을 둠.
			  			smap.put("DOC_NO", getItem("mg.vf.getRequestNo",smap).toString());
			  			
			  			if(request.getStringParam("OUT_MEDIA") ==null){ //메일 첨부 혹은 URL 첨부 일 경우 다수의 사용자에게 반출 될 수 있기 때문에 다운로드 횟수 제한을 두지 않는다.   		
			  				if(getItem("mg.cp.selectReadCnt", smap) != null){
					   	    	 if(downCnt >= Integer.parseInt(getItem("mg.cp.selectReadCnt", smap).toString())){
					   	    		 return closePopupWindow(mm.getMessage("CPMG_1044"));
					   	    	 }  
					   	     }  	  	
			  			} else  worktype = "URLDOWN";
			  			//URL 다운로드 기간 제한 
			  			 if(pm.getBoolean("cliptorplus.original.url.download.period.check") && request.getStringParam("OUT_MEDIA") !=null){
			    	        	smap.put("CheckDay", pm.getInt("cliptorplus.original.url.download.period.day"));
			  	        	  smap.put("dbName", dbName);
			  	        	  if( Integer.parseInt(getItem("mg.cp.urlDownloadPeriodChek"+getDBName(dbName), smap).toString() ) <= 0 ) {
			  	        		  // URL 다운로드 기간 초과
			  	        		  return closePopupWindow( mm.getMessage("CPMG_1051"));
			  	        	  }
			    	     } 
				   	     if(pm.getBoolean("diskcryptor.auth.code.use")) {  // DiskCryptor가 적용된 SITE이면 복호화를 수행한다.
			  			 log.info("복호화 수행");
				   	    	 String fileName = this.getStringWithNullCheck(map, "FILE_NAME");
				   	    	logFileName=fileName;
				   	    	 String srcFile = pm.getString("component.upload.directory") + "/" + this.getStringWithNullCheck(map, "FILE_PATH") + "/" + fileName;
			  	    
				  	           // 1. 대상파일 암호화
				  	           int lastIndex = fileName.lastIndexOf("."); 
				 			   String fileExtend = fileName.substring(lastIndex+1);
				 			   String zipName = fileName.substring(0,lastIndex);
				 			   
				 			   String tempFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/temp";
				 			   FileUtils.makeFolder(tempFile, true);
				 			   downFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/down";
				 			   FileUtils.makeFolder(downFile, true);
				 			   downFile += "/" + zipName + "." + fileExtend;
				 			   String decryptFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/decrypt";
				 			   	 			  
				 			   if("zip".equals(fileExtend.toLowerCase())) {   // 압축파일 이면	 				   
				 				      String zipFile = pm.getString("component.upload.directory") + "/" + this.getStringWithNullCheck(map, "FILE_PATH") + "/" + zipName + "_." + fileExtend;
				 				     
				 				      long retval = cryptorSHA.MgDecodeFile(srcFile, zipFile, pm.getString("diskcryptor.auth.code"));
				 				      log.info("zip일때--------------------------------retval :"+retval);
				 				      // DC 암호화 파일일 경우 복호화 한 파일로 작업하고 암호화된 파일이 아닐 경우 원문으로 작업 한다.
				 				      if(retval != -1)  srcFile = zipFile; 
				 				     ZipFileUtil zipUtil= new ZipFileUtil();
					 				  // 압축해제 
				 				    zipUtil.unZip(srcFile, tempFile);
				 				      // 복호화 임시폴더 생성
					 				  FileUtils.makeFolder(decryptFile, true);
					 				  // 복호화
					 				  FileUtils.dcDecrypt(tempFile, decryptFile, pm.getString("diskcryptor.auth.code"));
					 				  // 복호화 된 파일 다시 압축
					 				 zipUtil.zip(decryptFile, downFile);
					 				  // 2. 임시폴더 삭제
					 				  File decryptFolder = new File(decryptFile);
					 				  FileUtils.deleteFolder(decryptFolder);
					 				  
				 			   } else {
				 				  long retval = cryptorSHA.MgDecodeFile(srcFile, downFile, pm.getString("diskcryptor.auth.code"));
				 				 log.info("zip이 아닐때--------------------------------retval :"+retval);
				 				  if(retval == -1) FileUtils.fileCopy(srcFile, downFile);
				 			   }
				 			   
				 				  // 2. 임시폴더 삭제
				 				  File tempFolder = new File(tempFile);
				 				  FileUtils.deleteFolder(tempFolder);
			  	           }
			  		}
			  	 } else {
			  		 
			  		  if(pm.getBoolean("cliptorplus.download.period.check")) {                    // Exe Download 기간체크 옵션이면
		   	        	  smap.put("CheckDay", pm.getInt("cliptorplus.download.period.day"));
		   	        	  smap.put("dbName", dbName);
		   	        	  if( Integer.parseInt(getItem("mg.cp.exePeriodChek"+getDBName(dbName), smap).toString() ) <= 0 ) {
		   	        		  // 반출기간 초과
		   	        		  return closePopupWindow( mm.getMessage("CPMG_1034"));
		   	        	  }
		   	          }
			  		 
			  		 String status = getItem("mg.cp.getCpScheduleStatus",smap.get("DOC_NO").toString()).toString();
			    	 log.info("exeDownload-------------------------------------------------------------------------"+status);
			    	 if("02".equals(status) || "03".equals(status))  return closePopupWindow(mm.getMessage("CPMG_1049")); //대기, 진행
			    	 else if("05".equals(status))  return closePopupWindow(mm.getMessage("CPMG_1050")); //오류
			  		 
			  		 String tmpFileName =this.getStringWithNullCheck(map, "EXE_FILE");
			  		 String extName = ".exe";
			  		 if(tmpFileName.contains(".")) extName ="";
			  		 downFile = this.getStringWithNullCheck(map, "EXEPATH") + "/" + docNo.substring(0,8) + "/" + docNo + "/" + tmpFileName+ extName;
			  		 logFileName=tmpFileName+extName;
			  	 }
		         log.info("-exeDownload :-\n" + downFile);
		         tx.begin();
		         try{
	  					HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
	  					String locale="";
	  					Map item = new HashMap();
	  					item.put("DOC_NO", smap.get("DOC_NO"));
	  					item.put("USER_ID", userId);
	  					item.put("USER_NM",userNm ); 
	  					item.put("IP_ADDR", httpRequest.getRemoteAddr());
	  					item.put("IP_REMOTE", httpRequest.getRemoteAddr());
	  					item.put("MAC_ADDR", "");
	  					item.put("WORKGROUP", "");
	  					item.put("WORK_TYPE", worktype);
	  					item.put("MESSAGE", "Web Download");
	  					if("ko".equals(userLoc)){ /////////////////////////////////////////
	  						locale="KOR";
	  					}
	  					item.put("COUNTRY_CD",locale);
	  					item.put("PC_LOGIN_ID", "");
	  					item.put("ISPRIVATE", pm.getString("component.private.security"));
	  					
	  				     //20160704:  상신브레이크 반출함 상세 로그내역 저장을 위해 추가
	  				     item.put("LOG_CL_CD", logClCd);
	  				     item.put("OUT_UNIT_CD", "05");//05 WEB
	  				     item.put("OUT_URL","");//
	  				     item.put("FILE_NM", logFileName);
	  				     
		  	        	log.info("exeDownload----------------------------------------------로그---------------------------"+item.toString());	  	   	      

	  					createObject("mg.cp.saveDocLog"+getDBName(dbName), item); 
	  				
				  }catch(Exception logE){
					  	log.error("--------exeDownload---------------------------로그 쓰는 중--\n" + logE.toString());
				  }
			  }

			  //수신자가 exe 다운로드 시 CP_DOC_MASTER의 DOWN_CNT 증가시키기 
			  map.clear();
			  map.put("DOC_ID", smap.get("DOC_NO"));
			  updateObject("mg.common.updateDocDownCnt", map);
			  tx.commit();
			  return downloadFile(downFile);
    	   } catch(Exception e) {
    		  
          	  log.error("--------exeDownload-----------------------------\n" + e.toString());
          	  tx.rollback();
          	  return downloadFile(null);
           }
       }
       
     /**
   	 * NoSession User 사용자 정보 조회 (contextMenu를 사용한 반출 신청시 사용)
   	 * @param request
   	 * @return
   	 * @throws Exception
   	 * 생성일 : 2016. 03. 14
   	 */
   	private IResponse getUserByProperties(IRequest request) {
   		try {
   			String dbName = pm.getString("component.sql.database");
   			Map userMap;
   			if ("mssql".equals(dbName)) {
   				userMap = (Map) getItem("app.user.getUserByProperties",	request.getStringParam("userId"));
   			} else {
   				userMap = (Map) getItem("app.user.getUserByProperties_"	+ dbName, request.getStringParam("userId"));
   			}

   			log.info("------- userMap : " + userMap);

   			JSONObject obj = new JSONObject();

   			// s_user 대체를 위한 init 시 user 데이터 조회, 앞(기존 s_user 속성명, 뒤
   			// getUserByProperties_xxx 에 들어있는 쿼리명) 을 채워주면 된다.
   			obj.put("TODAY", this.getStringWithNullCheck(userMap, "TODAY"));
   			obj.put("FUNCTION_CD", this.getStringWithNullCheck(userMap, "FUNCTION_CD"));
   			obj.put("MANAGER_NM", this.getStringWithNullCheck(userMap, "MANAGER_NM"));
   			obj.put("P_DEPT_CD", this.getStringWithNullCheck(userMap, "P_DEPT_CD"));
   			obj.put("USER_ROLL", this.getStringWithNullCheck(userMap, "USER_ROLL"));
   			obj.put("DEPT_CD", this.getStringWithNullCheck(userMap, "DEPT_CD"));
   			obj.put("USER_NM", this.getStringWithNullCheck(userMap, "USER_NM"));
   			obj.put("WORK_CD", this.getStringWithNullCheck(userMap, "WORK_CD"));
   			obj.put("ID", this.getStringWithNullCheck(userMap, "ID"));
   			obj.put("EMAIL", this.getStringWithNullCheck(userMap, "EMAIL"));
   			obj.put("SELF_EXP_YN", this.getStringWithNullCheck(userMap, "SELF_EXP_YN"));
   			obj.put("NAME", this.getStringWithNullCheck(userMap, "NAME"));
   			obj.put("USER_ID", this.getStringWithNullCheck(userMap, "USER_ID"));
   			obj.put("DEPT_NM", this.getStringWithNullCheck(userMap, "DEPT_NM"));
   			obj.put("TEL_NO", this.getStringWithNullCheck(userMap, "TEL_NO"));
   			obj.put("LAYOUT_URLUSER_TYPE", this.getStringWithNullCheck(userMap, "USER_TYPE"));
   			obj.put("POSITION_NM", this.getStringWithNullCheck(userMap, "POSITION_NM"));
   			obj.put("MANAGER_ID", this.getStringWithNullCheck(userMap, "MANAGER_ID"));
   			obj.put("NEXTMONDAY", this.getStringWithNullCheck(userMap, "NEXTMONDAY"));
   			obj.put("FUNCTION_NM", this.getStringWithNullCheck(userMap, "FUNCTION_NM"));
   			obj.put("APV_REF_YN", this.getStringWithNullCheck(userMap, "APV_REF_YN"));
   			obj.put("FAIL_CNT", this.getStringWithNullCheck(userMap, "FAIL_CNT"));
   			obj.put("POSITION_CD", this.getStringWithNullCheck(userMap, "POSITION_CD"));
   			obj.put("FROMDAY", this.getStringWithNullCheck(userMap, "FROMDAY"));

   			/*
   			 * obj.put("DOC_NO", this.getStringWithNullCheck(userMap, ""));
   			 * obj.put("LAYOUT_HEIGHT", this.getStringWithNullCheck(userMap, ""));
   			 * obj.put("LAYOUT_WIDTH", this.getStringWithNullCheck(userMap, ""));
   			 * obj.put("LAYOUT_GRIDID", this.getStringWithNullCheck(userMap, ""));
   			 * obj.put("LAYOUT", this.getStringWithNullCheck(userMap, ""));
   			 * obj.put("LAYOUT_FORMID", this.getStringWithNullCheck(userMap, ""));
   			 */

   			// log.info("--------------------------------------"+obj.toString());
   			return write(obj.toString());
   		} catch (Exception e) {
   			log.error("---getUserByProperties-------------------------\n" + e.toString());
   			return write(mm.getMessage("CPMG_1002", request.getUser()
   					.getLocale()));
   		}
   	}
   	
   	/**
   	 * NoSession User 토큰 정보 조회 (contextMenu를 사용한 반출 신청시 사용)
   	 * @param request
   	 * @return
   	 * @throws Exception
   	 * 생성일 : 2016. 04. 12
   	 */
   	private IResponse getToken(IRequest request) {
   		try {
   			String dbName = pm.getString("component.sql.database");
   			String sid;
   			Map userMap;
   			if ("mssql".equals(dbName)) {
   				sid = (String)getItem("mg.site.selectIljinToken", request.getStringParam("userId"));
   			} else {
   				sid = (String)getItem("mg.site.selectIljinToken_" + dbName, request.getStringParam("userId"));
   			}

   			log.info("------- token : " + sid);

   			// log.info("--------------------------------------"+obj.toString());
   			return write(sid);
   		} catch (Exception e) {
   			log.error("---getUserByProperties-------------------------\n" + e.toString());
   			return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
   		}
   	}
   	
   	/**
	 * 콤보코드(컬럼정보) 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getComboGrpHtml(IRequest request) throws Exception {
		try {
			List lists = getList("mg.cp.getComboGrpHtml", request.getMap());
			StringBuffer buffer = new StringBuffer();

			//buffer.append("<select id=\'CUST_ID\' name=\'CUST_ID\' class=\'reqInput1\' onchange=\'custGrpChange(this.value);\'>");
			buffer.append("<select id=\'COMBOBOX\' name=\'COMBOBOX\' onchange=\'selGrpChange(this.value);\'>");
			Map map = null;

			//buffer.append("<option value='none'>"+ mm.getMessage("CPIS_0025", request.getUser().getLocale()) +"</option>");
			//buffer.append("<option value=''></option>");
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);
				buffer.append("<option value='" + map.get("COL_ID") + "'>" 
						+ map.get("COL_NM") + "</option>");
				
				// log.debug(
				// "-------------------------------------------------------------------\n"
				// + map.toString());
			}

			buffer.append("</select>");
			//log.info("CustInfo : " + buffer.toString());
			return writeXml(buffer.toString());
		} catch (Exception ex) {
			log.error("-------------------------------------------------------------------\n"
					+ ex.getMessage());
			return writeXml("");
		}
	}
	   /**
     * 사용자별 반출 건수 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getDocStats(IRequest request) throws Exception 
     {
       	  String col = request.getStringParam("grid_col_id");
       	  String[] cols = col.split(",");
       	  
       	  Map map = null;
           GridData gdRes = new GridData();
       	
       	  try {
       		  	 Map smap = request.getMap();
       		  	 smap.put("LOCALE", request.getUser().getLocale().getLanguage());
 				 
	 	       	 List lists = getList("mg.cp.selectDocStats"+getDBName(pm.getString("component.sql.database")),smap);
			  	
 				 for (int i = 0; i < lists.size(); i++) {
 				  	map =(Map) lists.get(i);
 				  	 
 				  	for(int j=0; j < cols.length; j++ ) {
 				  		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 				  	}
 				  }
 				return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	 	log.error("--------selectApproval----------------\n" + e.toString());
          	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
            }
     }
     /**
      * 반출내역 조회(관리자 기능)  ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelDocStats(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
       String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Export Stats List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
  	           	      
		       	 List lists = getList("mg.cp.selectDocStats"+getDBName(pm.getString("component.sql.database")), request.getMap()); 
         
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Export Stats List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
     }
}
