package mg.cppp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.component.parameter.IParameterManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.cryptorSHA;

/**
 * 협력업체 사용자 등록
 * @author Administrator
 * 최종 수정일 : 2016. 03. 17.
 * 변경 내용 : 협력업체 사용자 정보 저장(setCompUser)시 메일링 서비스 오류 수정
 */
public class joiningUserAction extends BaseAction{

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(request.isMode("insertCompUser")) { /** 회원가입 등록을 눌렀을 경우 */
            return setCompUser(request);
		}else if(request.isMode("insertCompany")) { /** 업체 등록을 눌렀을 경우 */ 
			return setCompany(request);
		}else if(request.isMode("checkId")){	/** id 중복체크 */
			return getDuplicationId(request);
		}else if(request.isMode("checkRegNo")){	/** COMPANY_ID 중복체크 */
			return getDuplicationRegNo(request);
		}else if(request.isMode("searchManager")){ /** 담당자 검색 */
			return getManager(request);
		}else if(request.isMode("searchCompany")){ /** 업체 검색*/
			return getCompany(request);
		}else if(request.isMode("searchAllCompany")){ /** 전체 검색 조건으로 검색할 경우*/
			return searchAllCompany(request);
		}else if(request.isMode("selectUserApproval")){ /** 포탈 사용자 회원가입 승인 리스트 */
			return getUserApproval(request);
		}else if(request.isMode("approvalUser")){ /** 포탈 사용자 회원가입 승인 */
			return setApprovalUser(request);
		}else if(request.isMode("returnUser")){ /** 포탈 사용자 회원가입 반려 - 삭제*/
			return delUser(request);
		}else if(request.isMode("searchManagerNoTree")){
			return getManager(request);
		}else if(request.isMode("excelCompany")){ /** 업체 검색 Excel Download*/
			return excelCompany(request);
		}else if(request.isMode("excelAllCompany")){ /** 전체 업체 검색 Excel Download*/
			return excelAllCompany(request);
		}
		else {
			return write(null);
		 }
	}
	
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");

	private IResponse searchAllCompany(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
        
        try{
        	 List lists = getList("mg.cppp.searchAllCompany",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	companyHiding(map);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", "ko"), "true", "doQuery", gdRes.getGridXmlDatas()));
       
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", "ko"),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}
	
     /**
      * 협력사 전체 조회(관리자 기능)  ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelAllCompany(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
       String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");
    	  
    	     Map smap = request.getMap();
		     smap.put("dbName", pm.getString("component.sql.database"));
		     
    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Cooperation Company List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
  	           	      
			
			     List lists = getList("mg.cppp.searchAllCompany",smap);
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	companyHiding(map);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Cooperation Company List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
     }

	private IResponse delUser(IRequest request) {
		// TODO Auto-generated method stub
		 String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	        String locale = request.getUser().getLocale().toString();
	        try {
	        	tx.begin();
		        Map item = null;
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	item.put("STAFF_ID", request.getStringParam("STAFF_ID"));
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			            item.put(cols[j], tmps);     
		         	} 
		       	// log.info("delUser---------------------------------"+item.toString());
		       	 	deleteObject("mg.cppp.deleteUser", item); 
		       		try{
	       				IMail mail = mailHandle.createHtmlMail();
		       			mail.addTo((String)item.get("USER_ID"));
		       			mail.setFrom(request.getUser().getProperty("EMAIL").toString());
		       					
		       			Object[] params = {mm.getMessage("CPMG_1107", request.getUser().getLocale())};
		    	    	//mail.setSubject(mm.getMessage("MAIL_0051", params, request.getUser().getLocale()));
	    		    	mail.setSubject("["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]"+" "+mm.getMessage("MAIL_0051", params, request.getUser().getLocale())); //사용자 반려 완료
	    		    	
		    	          // 메일 템플릿을 읽어 온다.
		    	 		 String fileName = "config/templates/mail_Template_Portal.html";
		    	 		 String mailContext = readFile(fileName);
		    	 		 
		    	 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]");
		    			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0051", params, request.getUser().getLocale()));
	    				 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.pp.url"));
		    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.pp.url") + pm.getString("component.site.image"));
		    			// log.info("사용자 반려 완료 메일 발송------------------------------------------------------------------------------------1");
		    			 mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0054", request.getUser().getLocale()));
		    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", request.getUser().getLocale()));
	    				 mailContext = mailContext.replace("COMP_NM", pm.getString("component.site.company.name"));
	    				 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1003", request.getUser().getLocale()));
	    				// log.info("사용자 반려 완료 메일 발송------------------------------------------------------------------------------------2"+request.getUser().getProperty("POSITION_NM"));
	    				 String tmp;
	    				 if(request.getUser().getProperty("POSITION_NM")== null){
	    					// log.info("사용자 반려 완료 메일 발송------------------------------------------------------------------------------------2-1");
	    					 tmp = request.getUser().getProperty("USER_NM").toString()+" / "+request.getUser().getProperty("FUNCTION_NM").toString()+" / "+request.getUser().getProperty("DEPT_NM").toString();
	    				 }else{
	    					 tmp = request.getUser().getProperty("USER_NM").toString()+" / "+request.getUser().getProperty("POSITION_NM").toString()+" / "+request.getUser().getProperty("DEPT_NM").toString();
	    				 }
	    				// log.info("사용자 반려 완료 메일 발송------------------------------------------------------------------------------------3");
	    				 mailContext = mailContext.replace("USER_NM", tmp);
	    				 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1004", request.getUser().getLocale()));
	    				 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    		   		 String curDt = formatter.format(new Date());
	    				 mailContext = mailContext.replace("REQ_DT", curDt);
	    				// log.info("사용자 반려 완료 메일 발송------------------------------------------------------------------------------------4");
	    				 mailContext = mailContext.replace("LINK", pm.getString("component.site.pp.url"));//파트너 포탈
	    				 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006", request.getUser().getLocale()));
		    			  
		    			 // 메일 발송
		    			 mail.setHtmlMessage(mailContext);
		    	         mail.send();
		       			
		       		}catch(Exception e){
		       			log.error("Send Mail Fail-----------------------------------------------------------"+e.toString());
		       		}
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------delUser----------------------------------------------------------------\n" + e.toString());
	        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
		        flag = "false";
	        }
	                
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	}


	private IResponse setApprovalUser(IRequest request) {
		// TODO Auto-generated method stub
		 String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	        String locale = request.getUser().getLocale().toString();
	        try {
	        	tx.begin();
		        Map item = null;
		       
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 item.put("STAFF_ID", request.getStringParam("STAFF_ID"));
		       	 item.put("dbName", pm.getString("component.sql.database"));
		       	 for(int j=0; j < cols.length; j++ ) {
		       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			             item.put(cols[j], tmps);     
		         	}
		      	    //log.info("setApprovalUser----------------------------------"+item.toString());
				    updateObject("mg.cppp.updateUserApproval", item);
				    //승인 결과 발송
		       		try{
		       			IMail mail = mailHandle.createHtmlMail();
		       			mail.addTo((String)item.get("USER_ID"));
		       			mail.setFrom(request.getUser().getProperty("EMAIL").toString());
		       					
		       			Object[] params = {mm.getMessage("CPMG_1108", request.getUser().getLocale())};
		    	    	//mail.setSubject(mm.getMessage("MAIL_0051", params, request.getUser().getLocale()));
	    		    	mail.setSubject("["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]"+" "+mm.getMessage("MAIL_0051", params, request.getUser().getLocale())); //사용자 승인 완료
	    		    	
		    	          // 메일 템플릿을 읽어 온다.
		    	 		 String fileName = "config/templates/mail_Template_Portal.html";
		    	 		 String mailContext = readFile(fileName);
		    	 		 
		    	 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]");
		    			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0051", params, request.getUser().getLocale()));
	    				 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.pp.url"));
		    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.pp.url") + pm.getString("component.site.pp.image"));
		    			// log.info("사용자 승인 완료 메일 발송------------------------------------------------------------------------------------1");
		    			 Object[] params2 = {(String)item.get("COMPANY_NM"), (String)item.get("USER_NM"), mm.getMessage("CPMG_1108", request.getUser().getLocale())};
		    			 String tmp = mm.getMessage("MAIL_0052", params2, request.getUser().getLocale());
		    			 mailContext = mailContext.replace("MAIL_CONTENT", tmp);
		    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", request.getUser().getLocale()));
	    				 mailContext = mailContext.replace("COMP_NM", pm.getString("component.site.company.name"));
	    				 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1003", request.getUser().getLocale()));
	    				// log.info("사용자 승인 완료 메일 발송------------------------------------------------------------------------------------2"+request.getUser().getProperty("POSITION_NM"));
	    				 if(request.getUser().getProperty("POSITION_NM")== null){
	    					// log.info("사용자 승인 완료 메일 발송------------------------------------------------------------------------------------2-1");
	    					 tmp = request.getUser().getProperty("USER_NM").toString()+" / "+request.getUser().getProperty("FUNCTION_NM").toString()+" / "+request.getUser().getProperty("DEPT_NM").toString();
	    				 }else{
	    					 tmp = request.getUser().getProperty("USER_NM").toString()+" / "+request.getUser().getProperty("POSITION_NM").toString()+" / "+request.getUser().getProperty("DEPT_NM").toString();
	    				 }
	    				// log.info("사용자 승인 완료 메일 발송------------------------------------------------------------------------------------3");
	    				 mailContext = mailContext.replace("USER_NM", tmp);
	    				 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1004", request.getUser().getLocale()));
	    				 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    		   		 String curDt = formatter.format(new Date());
	    				 mailContext = mailContext.replace("REQ_DT", curDt);
	    				// log.info("사용자 승인 완료 메일 발송------------------------------------------------------------------------------------4");
	    				 mailContext = mailContext.replace("LINK", pm.getString("component.site.pp.url"));//파트너 포탈
	    				 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006", request.getUser().getLocale()));
		    			  
		    			 // 메일 발송
		    			 mail.setHtmlMessage(mailContext);
		    	         mail.send();
		       		}catch(Exception e){
		       			log.error("Send Mail Fail-----------------------------------------------------------"+e.toString());
		       		}	   	
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------setApprovalUser----------------------------------------------------------------\n" + e.toString());
	        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
		        flag = "false";
	        }
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	}


	private IResponse getUserApproval(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
        
        try{
        	 List lists = getList("mg.cppp.selectUserApproval",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}


	private IResponse setCompany(IRequest request) {
		// TODO Auto-generated method stub
		try{
			tx.begin();
			Map smap = request.getMap(); 
			Map map = (Map)getItem("mg.cppp.selectComCount",smap);
	    	String count =Integer.toString(Integer.parseInt(this.getStringWithNullCheck(map, "CNT"))+1); 
	    	String companyIdFormat = "COM-0000";
	    	String companyId = companyIdFormat.substring(0, companyIdFormat.length()-count.length())+count;//주어진 형태의 스트링에서 얻어온 카운트 스트링의 길이만큼 잘라내고 카운트 스트링을 붙여준다.
	    	smap.put("COMPANY_ID", companyId);
	    	smap.put("dbName", pm.getString("component.sql.database"));
	     	createObject("mg.cppp.insertCompany",smap);
	     	tx.commit(); 
	    
	     	return write("insertCompany Success");
		}catch(Exception e){
			log.error("setCompany-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("insertCompany Fail");
		}
	}

	private IResponse getDuplicationRegNo(IRequest request) {
		JSONObject obj = new JSONObject();
		
		try{
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
	    	Map map = (Map)getItem("mg.cppp.checkDupliCompany",smap);
	    	String regNo = this.getStringWithNullCheck(map, "REG_NO");
	    	if(this.getStringWithNullCheck(map, "REG_NO") != null){
	    		obj.put("chkValue", "false");
	    	}
	    	return write(obj.toString());
		}catch(NullPointerException e){
    		obj.put("chkValue", "true");
    		return write(obj.toString());
		}catch(Exception e){
	    		log.error("getDuplicationId---------------------------------------------------------------\n" + e.toString());
				return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
    	}
	}

	private IResponse getCompany(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
        
        try{
        	 List lists = getList("mg.cppp.selectCompany",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	companyHiding(map);
	   
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", "ko"), "true", "doQuery", gdRes.getGridXmlDatas()));
       
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", "ko"),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
        
	}
	/**
     * 협력사 조회(관리자 기능)  ExcelDownload
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse excelCompany(IRequest request) throws Exception 
     {
   	  String col = request.getStringParam("grid_col_id");
      String[] cols = col.split(",");
   	  String width = request.getStringParam("grid_col_width");
   	  String[] widths = width.split(",");
   	  String header = request.getStringParam("grid_col_header");
   	  String[] headers = header.split(",");

   	  Map smap = request.getMap();
      smap.put("dbName", pm.getString("component.sql.database"));
   	  try {
   		     // Excel Header 설정
   		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Cooperation Company List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
 	        
		       	 List lists = getList("mg.cppp.selectCompany",smap); 
        
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	companyHiding(map);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
 	         return doc.download(request, "Cooperation Company List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
       } catch(Exception e) {
      	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
      	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
       }
    }
	private IResponse getManager(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
/*        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));*/
        String dbName =pm.getString("component.sql.database");
        try{
        	List lists;
        	if(request.isMode("searchManager")){
        		lists = getList("mg.cppp.selectManager"+getDBName(dbName),request.getMap());
        	}else{
        		lists = getList("mg.cppp.selectManagerNoTree"+getDBName(dbName),request.getMap());
        	}
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", "ko"), "true", "doQuery", gdRes.getGridXmlDatas()));
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", "ko"),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
      
	}

	private IResponse getDuplicationId(IRequest request) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		
		try{
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			
	    	Map map = (Map)getItem("mg.cppp.checkDupliEmail",smap);
	    	String userId = this.getStringWithNullCheck(map, "USER_ID");
	    	if(this.getStringWithNullCheck(map, "USER_ID") != null){
	    		obj.put("chkValue", "false");
	    	}
	    	return write(obj.toString());
		}catch(NullPointerException e){
    		obj.put("chkValue", "true");
    		return write(obj.toString());
		}catch(Exception e){
	    		log.error("getDuplicationId---------------------------------------------------------------\n" + e.toString());
				return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
    	}
	}
	
	/**
	 * 협력업체 사용자 정보 저장
	 * @param request
	 * @return
	 * 최종 수정일 : 2016. 03. 17.
	 * 변경 내용 : 메일링 서비스 오류 수정
	 */
	private IResponse setCompUser(IRequest request) {
		
		Map smap = request.getMap();
		log.info("사용자 등록 정보 : " + smap.toString());
		
		//컴파일 메일링 테스트
		Locale locale;
	    String pLocale = request.getStringParam("j_language");
	    if(pLocale != null) {
			if(pLocale.equals("ko")) {
				locale = Locale.KOREA;
			} else if(pLocale.equals("zh")) {
				locale = Locale.CHINA;
			} else if(pLocale.equals("ja")) {
				locale = Locale.JAPAN;
			} else {
				locale = Locale.US;
			}	
		} else {
			locale = Locale.KOREA;
			pLocale = "ko";
		}
	    
		try{
			tx.begin();
			//수신자 비밀번호 암호화하여 저장
			//if(pm.getBoolean("component.signon.password.encrypted")){
				smap.put("PASSWD", cryptorSHA.JinEncodeSHA256((String)request.getMap().get("PASSWD")));	
			//}
	     	createObject("mg.cppp.insertCompUser",smap);
	     	tx.commit(); 
		}catch(Exception e){
			log.error("setCompUser-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("insertCompUser Fail");
		}
		
	    log.info("로케일 정보");
	    log.info("::" + locale);
		
		try{
			Map map = (Map)getItem("mg.cppp.getSysUser",smap);
	     	//승인요청 mail을 관리자에게 보내야한다.
	    	IMail mail = mailHandle.createHtmlMail();
	    	//mail.setFrom(request.getStringParam("USER_ID")); 
	    	// 시스템 관리자 메일로 변경  		       			
	    	String[] managerMail = pm.getString("cliptorplus.mail.manager.mailaddress").split("\\|");
   			log.info("--------------------------------------------------------------------------------------------------"+managerMail[0]);
	    	mail.setFrom(managerMail[0]);  
	    	mail.addTo(this.getStringWithNullCheck(map,"EMAIL"));
	    	mail.setSubject("["+mm.getMessage("MAIL_0004", locale)+"]"+" "+mm.getMessage("MAIL_0041", locale)); //사용자 등록 요청
	    	
	         // 메일 템플릿을 읽어 온다.
	         String fileName;
	         //상신일 경우 결재 버튼 없는 폼으로 처리
	         if("SB".equals(pm.getString("component.site.company"))){
	        	 log.info("------------NoBtn_mail_Template------------");
	        	 fileName = "config/templates/mail_Template_NoBtn.html";
	         } else {
	        	 log.info("------------default_mail_Template------------");
	        	 fileName = "config/templates/mail_Template.html";
	         }
	 		 String mailContext = readFile(fileName);
	 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", locale)+"]");	 		 //
			 mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0041", locale));//
			 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.url"));
			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));
			 
			 mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0043", locale));
			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", locale));
			 mailContext = mailContext.replace("COMP_NM", request.getStringParam("COMPANY_NM"));
			 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1001", locale));
			 mailContext = mailContext.replace("USER_NM", request.getStringParam("USER_NM"));
			 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1002", locale));
			 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	   		 String curDt = formatter.format(new Date());
			 mailContext = mailContext.replace("REQ_DT", curDt);
			 
			 mailContext = mailContext.replace("LINK", pm.getString("component.site.ep.url"));
			 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1005", locale));

			 // 메일 발송
			 mail.setHtmlMessage(mailContext);
	         mail.send();

		}catch(Exception e){
			log.error("Send Mail-----------------------------------------------------------"+e.toString());
			//return write("Send Mail Fail");
		}
		return write("insertCompUser Success");
	}

	private void companyHiding(Map map) {
		if("Y".equals(pm.getString("component.ui.company.hiding"))) {
     		String regNo = this.getStringWithNullCheck(map, "REG_NO");
     		String ceoNm = this.getStringWithNullCheck(map, "CEO_NM");
     		
     		int regLen = regNo.length();
     		if(regLen > 3) {	
     			//국내 사업자번호는 10자리지만 외국의 경우를 고려 가운데 4자리만 *로 하고 나머지 뒷자리는 보여준다.
     			String temp1 = regNo.substring(0, 3);
     			String temp2 = "";
     			int cnt;
     			
     			if(regLen >= 7) {
     				cnt = 4;
     				temp2 = regNo.substring(7, regLen);
     			} else {
     				cnt = regLen - 3;
     			}
     			for(int j=0; j<cnt; j++) {
     				temp1 += "*";
     			}
     			regNo = temp1 + temp2;
     		}
     		
     		int ceoLen = ceoNm.length();
     		if(ceoLen > 1) {	
     			//fullName 처음과 마지막 글자만 출력
     			String temp1 = ceoNm.substring(0, 1);
     			String temp2 = "";
     			int cnt;
     			
     			if(ceoLen > 2) {
     				cnt = ceoLen - 2;
     				temp2 = ceoNm.substring(ceoLen-1, ceoLen);
     			} else {
     				cnt = ceoLen - 1;
     			}
     			for(int j=0; j<cnt; j++) {
     				temp1 += "*";
     			}
     			ceoNm = temp1 + temp2;
     		}
     		map.put("REG_NO", regNo);
     		map.put("CEO_NM", ceoNm);
     	}
	}
	
}
