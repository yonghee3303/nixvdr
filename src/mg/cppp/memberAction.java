package mg.cppp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.cryptorSHA;

import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;

/**
* 업무 그룹명 : mg.cppp
* 서브 업무명 : memberAction.java
* 작성자 : -
* 작성일 : - 
* 설 명 : memberAction
* 최종 수정일 : 2016. 04. 26
* 변경 내용 : 비밀번호찾기 오류 수정
*/ 
public class memberAction extends BaseAction{
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
	
	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(request.isMode("selectMemberInfo")) { /** 회원 정보 조회 */ 
            return getMemberInfo(request);
		}else if(request.isMode("updateMemberInfo")) { /** 회원 정보 수정 */ 
            return setMemberInfo(request);
		}else if(request.isMode("selectCompanyInfo")) { /** 업체 정보 조회 */ 
            return getCompanyInfo(request);
		}else if(request.isMode("selectUserId")) { /** 아이디 찾기 */ 
            return getUserId(request);
		}else if(request.isMode("selectUserPasswd")) { /** 비밀번호 찾기 */ 
            return getUserPasswd(request);
		}else if(request.isMode("selectSysCompUser")){
			return getUserList(request);
		}else if(request.isMode("selectSysCompUserList")){ /** 업체에 등록된 회원조회 */
			return getUserList_company(request);
		}else if(request.isMode("excelSysCompUserList")){ /** 업체에 등록된 회원조회 */
			return excelUserList_company(request);
		}else if(request.isMode("saveCompUserStaff")){/** sysuser가 업체 사용자를 등록*/
			return saveCompUserStaff(request);
		}else if(request.isMode("selectPasswdValid")){ /** 단방향 암호화 이기 때문에 기존 비밀번호가 맞는지 확인하기 위함.*/
			return getPasswdValid(request);
		}else if(request.isMode("checkPassChangeDate")){ /** 메인페이지에서 패스워드 변경일을 확인하기 위함*/
			return checkPassChange(request);
		}else {
			return write(null);
		 }
	}


	private IResponse checkPassChange(IRequest request) {
		// TODO Auto-generated method stub
		Map smap = request.getMap(); 
		String dbName = pm.getString("component.sql.database");
		JSONObject obj = new JSONObject();
		try{
			
			Map map = (Map)getItem("mg.cppp.selectPassChangeDate"+getDBName(dbName),smap); 
			
			obj.put("DIFF_DATE", this.getStringWithNullCheck(map, "DIFF_DATE"));
			obj.put("RESULT", "false");
    		return write(obj.toString());
         }catch(NullPointerException e){
      		obj.put("RESULT", "true");
     		return write(obj.toString());
         }catch(Exception e) {
       	 	log.error("---getPasswdValid 에러-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}


	private IResponse getPasswdValid(IRequest request) {
		// TODO Auto-generated method stub
		Map smap = request.getMap(); //var arg = "&USER_ID=" + "<%=s_user.getId().toString() %>"+"&PASSWD="+inputOldPass; 
		smap.put("dbName", pm.getString("component.sql.database"));
		JSONObject obj = new JSONObject();
		try{
			smap.put("PASSWD", cryptorSHA.JinEncodeSHA256((String)request.getStringParam("PASSWD")));	
			Map map = (Map)getItem("mg.cppp.selectPasswd",smap);
			obj.put("USER_ID", this.getStringWithNullCheck(map, "USER_ID"));
			obj.put("RESULT", "ture");
    		return write(obj.toString());
         }catch(NullPointerException e){
      		obj.put("RESULT", "false");
     		return write(obj.toString());
         }catch(Exception e) {
       	 	log.error("---getPasswdValid 에러-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}

	private IResponse saveCompUserStaff(IRequest request) {
		// TODO Auto-generated method stub
		 String ids_info = request.getStringParam("ids");
	        String cols_ids = request.getStringParam("col_ids");
	        String[] cols = cols_ids.split(",");
	        String[] ids = ids_info.split(",");
	        String msg="", flag="";
	        String passwd ="";

	        try {
	        	tx.begin();
		        Map item = null;
		       
		        for (int i = 0; i < ids.length; i++) {
		       	 item = new HashMap();
		       	 // id는 입력받은 이메일로...비밀번호는 임시비밀번호 발급해서 메일로 전송
		       	passwd = getRandomString(6);

		       	 for(int j=0; j < cols.length; j++ ) {
		       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			             item.put(cols[j], tmps);
		         }
		       	 item.put("STAFF_ID", request.getStringParam("STAFF_ID"));
		       	 //item.put("COMPANY_ID",request.getStringParam("COMPANY_ID"));
		       	 item.put("dbName", pm.getString("component.sql.database"));
		       	
		       	//if(pm.getBoolean("component.signon.password.encrypted")){
					item.put("PASSWD", cryptorSHA.JinEncodeSHA256(passwd));	
				//}else{
				//	item.put("PASSWD", passwd);
				//}
		      	//log.info("saveCompUserStaff----------------------------------"+item.toString());
		      	if(Integer.parseInt(getItem("mg.cppp.countCompUser", item).toString() ) > 0 ) {
		       		log.info("기존유저 업데이트");
		       		updateObject("mg.cppp.updateCompUserStaff",item);
		       	}else {
		       		log.info("새유저 생성");
		       		createObject("mg.cppp.insertCompUserStaff",item);
		       	}
		      		//임시비밀번호 발송
		       		try{
		       			IMail mail = mailHandle.createHtmlMail();
		       			//mail.setFrom(request.getUser().getProperty("EMAIL").toString());
		       			//시스템 관리자 메일 주소로 발송
		       			String[] managerMail = pm.getString("cliptorplus.mail.manager.mailaddress").split("\\|");
		       			mail.setFrom(managerMail[0]);  
		       			mail.addTo((String)item.get("EMAIL"));
		       			mail.setSubject("["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]"+" "+mm.getMessage("MAIL_0061", request.getUser().getLocale())); //임시 비밀번호가 발급되었습니다.
		        		String fileName = "config/templates/mail_Template_Portal.html";
				 		String mailContext = readFile(fileName);
				 		mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]");
						mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0061" ,request.getUser().getLocale()));
						mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.pp.url"));
		   			    mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image")); 
		   			    
		   			    Object[] params = {passwd};
		   			    mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0062",params, request.getUser().getLocale()));
		   			    mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", request.getUser().getLocale()));
						mailContext = mailContext.replace("COMP_NM", request.getStringParam("COMPANY_NM"));
						mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1001", request.getUser().getLocale()));
						mailContext = mailContext.replace("USER_NM", (String)item.get("USER_NM"));
						mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1002",request.getUser().getLocale()));
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				   		String curDt = formatter.format(new Date());
						mailContext = mailContext.replace("REQ_DT", curDt);
						
						mailContext = mailContext.replace("LINK", pm.getString("component.site.pp.url"));//파트너 포탈
						mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006",request.getUser().getLocale()));
						
		        		mail.setHtmlMessage(mailContext);
		        		mail.send();
		       		}catch(Exception e){
		       			log.error("Send Mail Fail-----------------------------------------------------------"+e.toString());
		       		}	
		        }
		        tx.commit();
		        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		        flag = "true";
	        } catch (Exception e) {
	        	tx.rollback();
	        	log.error("------------saveCompUserStaff----------------------------------------------------------------\n" + e.toString());
	        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
		        flag = "false";
	        }
	        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
	        return writeXml(ret);
	}

	private IResponse getUserList_company(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Map smap = request.getMap();
        String dbName = pm.getString("component.sql.database");
        smap.put("LOCALE", request.getUser().getLocale().getLanguage());

        try{
        	List lists;
        	if("mssql".equals(pm.getString("component.sql.database"))){
        		lists= getList("mg.cppp.selectSysCompUserList",smap);
        	}else{
        		lists=getList("mg.cppp.selectSysCompUserList_"+dbName, smap);
        	}
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	   
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
       
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}
	 /**
     * ExcelDownload
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse excelUserList_company(IRequest request) throws Exception 
     {
   	  String col = request.getStringParam("grid_col_id");
      String[] cols = col.split(",");
   	  String width = request.getStringParam("grid_col_width");
   	  String[] widths = width.split(",");
   	  String header = request.getStringParam("grid_col_header");
   	  String[] headers = header.split(",");

   	  try {
   		     // Excel Header 설정
   		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Cooperation User List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
 	           	       
				 String dbName = pm.getString("component.sql.database");
		       	 List lists = getList("mg.cppp.selectSysCompUserList"+getDBName(dbName), request.getMap()); 
        
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
 	         return doc.download(request, "Cooperation User List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
       } catch(Exception e) {
      	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
      	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
       }
    }
	private IResponse getUserList(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
        String dbName = pm.getString("component.sql.database");
        try{
        	List lists = getList("mg.cppp.selectSysCompUser",smap);
        	
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	   
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
       
        } catch(Exception e){
        	log.error("---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	/*
     * CPPP 비밀번호 찾기 
     * @param request
     * @return
     * @throws Exception
     * 최종 수정일 : 2016. 04. 26.
	 * 변경 내용 : 메일링 서비스 오류 수정 (다국어지원)
     */
	private IResponse getUserPasswd(IRequest request) {
		
		Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		JSONObject obj = new JSONObject();
		String tempPass ="";
		
		Locale locale;
	    String pLocale = request.getStringParam("j_language");
	    if(pLocale != null) {
			if(pLocale.equals("ko")) {
				locale = Locale.KOREA;
			} else if(pLocale.equals("zh")) {
				locale = Locale.CHINA;
			} else if(pLocale.equals("ja")) {
				locale = Locale.JAPAN;
			} else {
				locale = Locale.US;
			}	
		} else {
			locale = Locale.KOREA;
			pLocale = "ko";
		}
	    
	    //log.info("================== getUserPasswd j_language :: " + pLocale + " / Locale :: " + locale.toString());
		
	    try{
			Map map = (Map)getItem("mg.cppp.selectUserPasswd",smap);
			tempPass = this.getStringWithNullCheck(map, "PASSWD");
    	  	tempPass = getRandomString(6);
    		
			smap.put("PASSWD", cryptorSHA.JinEncodeSHA256((String)tempPass));	

    	  	obj.put("TEMP_PASSWD", tempPass);
    	  
    	  	updateObject("mg.cppp.updateTempPass", smap);
    		
    		obj.put("RESULT", "ture");
    		try{
        		IMail mail = mailHandle.createHtmlMail();
        		//mail.setFrom(request.getStringParam("USER_ID"));
        		//시스템 관리자 메일 주소로 지정.
       			String[] managerMail = pm.getString("cliptorplus.mail.manager.mailaddress").split("\\|");
       			mail.setFrom(managerMail[0]);  
        		mail.addTo(request.getStringParam("USER_ID"));
        		mail.setSubject("["+mm.getMessage("MAIL_0004", locale)+"]"+" "+mm.getMessage("MAIL_0061", locale)); //임시 비밀번호가 발급되었습니다.
        		String fileName = "config/templates/mail_Template_Portal.html";
		 		String mailContext = readFile(fileName);
		 		 
		 		mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0004", locale)+"]");
				mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0061", locale));
				mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.pp.url"));
   			    mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));

   			    Object[] params = {tempPass};
   			    mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0062",params, locale));
   			 
   			    mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", locale));
				mailContext = mailContext.replace("COMP_NM", request.getStringParam("COMPANY_NM"));
				mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1001", locale));
				mailContext = mailContext.replace("USER_NM", request.getStringParam("USER_NM"));
				mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1002", locale));
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		   		String curDt = formatter.format(new Date());
				mailContext = mailContext.replace("REQ_DT", curDt);
		         
				mailContext = mailContext.replace("LINK", pm.getString("component.site.pp.url"));//파트너 포탈
				mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1006", locale));
				
        		mail.setHtmlMessage(mailContext);
        		mail.send();
    		}catch(Exception e){
    			log.error("Send Mail Fail-----------------------------------------------------------"+e.toString());
    		}
    		return write(obj.toString());
         }catch(NullPointerException e){
     		obj.put("RESULT", "false");
     		return write(obj.toString());
 		} catch(Exception e) {
       	 	log.error("---getUserPasswd-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", locale));
        }
	}
	
	private IResponse getUserId(IRequest request) {
		// TODO Auto-generated method stub
		Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		JSONObject obj = new JSONObject();
		try{
    	Map map = (Map)getItem("mg.cppp.selectUserId",smap);
    		 
    		obj.put("USER_ID", this.getStringWithNullCheck(map, "USER_ID"));
    		obj.put("RESULT", "ture");
    	
    		return write(obj.toString());
         }catch(NullPointerException e){
     		obj.put("RESULT", "false");
     		return write(obj.toString());
 		} catch(Exception e) {
       	 	log.error("---MemberInfo 불러오기 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}

	private IResponse getCompanyInfo(IRequest request) {
		Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		try{
			log.info("getCompanyInfo 진입");
    	Map map = (Map)getItem("mg.cppp.selectCompanyInfo",smap);
    	
    		// Creation of Json Response 
    		 JSONObject obj = new JSONObject();
  
    		obj.put("COMPANY_ID", this.getStringWithNullCheck(map, "COMPANY_ID"));
    		obj.put("COMPANY_NM", this.getStringWithNullCheck(map, "COMPANY_NM"));
    		obj.put("CEO_NM", this.getStringWithNullCheck(map, "CEO_NM"));
    		obj.put("COMPANY_TELNO", this.getStringWithNullCheck(map, "COMPANY_TELNO"));
    		obj.put("ADDR", this.getStringWithNullCheck(map, "ADDR")); 
    	
    		return write(obj.toString());
         } catch(Exception e) {
       	 	log.error("---CompanyInfo 불러오기 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}

	private IResponse setMemberInfo(IRequest request) {
		// TODO Auto-generated method stub
		
		Map smap = request.getMap();
        JSONObject obj = new JSONObject();
		try{ 
			tx.begin();
			//if(pm.getBoolean("component.signon.password.encrypted")){
				smap.put("PASSWD", cryptorSHA.JinEncodeSHA256((String)request.getMap().get("PASSWD")));	
			//}
			smap.put("dbName", pm.getString("component.sql.database"));
			if("changePass".equals((String)request.getMap().get("callFrom"))){
				updateObject("mg.cppp.updateTempPass", smap);
				log.info("-----------------------------------------------setMemberInfo---비밀번호만 변경");
			}else{
				updateObject("mg.cppp.updateMemberInfo", smap);
				log.info("-----------------------------------------------setMemberInfo---직책 및 전화번호, 비번 변경");
			}
	        tx.commit();
	        obj.put("msg", mm.getMessage("CPCOM_1006", request.getUser().getLocale()));
	        return write(obj.toString());
		}catch(Exception e) {
			tx.rollback();
			obj.put("msg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
			log.error("---회원 정보 수정 실패---------------------------------------------------------------\n" + e.toString());
			return write(obj.toString());
		}
	}

	private IResponse getMemberInfo(IRequest request) {
		// TODO Auto-generated method stub
		Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		try{
    	Map map = (Map)getItem("mg.cppp.selectMemberInfo",smap);
    	
    		// Creation of Json Response 
    		 JSONObject obj = new JSONObject();
    		obj.put("USER_ID", this.getStringWithNullCheck(map, "USER_ID"));
    		obj.put("USER_NM", this.getStringWithNullCheck(map, "USER_NM"));
    		obj.put("USER_ENM", this.getStringWithNullCheck(map, "USER_ENM"));
    		obj.put("COMPANY_NM", this.getStringWithNullCheck(map, "COMPANY_NM"));
    		//obj.put("PASSWD", this.getStringWithNullCheck(map, "PASSWD")); 
    		obj.put("POSITION_NM", this.getStringWithNullCheck(map, "POSITION_NM"));
    		obj.put("WORK_CD", this.getStringWithNullCheck(map, "WORK_CD"));
    		obj.put("TEL_NO", this.getStringWithNullCheck(map, "TEL_NO"));
    		obj.put("EMAIL", this.getStringWithNullCheck(map, "EMAIL"));
    		obj.put("MAC_ADDR", this.getStringWithNullCheck(map, "MAC_ADDR"));
    	
    		return write(obj.toString());
         } catch(Exception e) {
       	 	log.error("---MemberInfo 불러오기 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}
	
}
