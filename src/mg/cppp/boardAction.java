package mg.cppp;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.core.base.ComponentRegistry;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.component.parameter.IParameterManagement;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class boardAction extends BaseAction{

	
	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if(request.isMode("insertBoard")) { /** 게시판 등록 */ 
            return setContent(request);
		}else if(request.isMode("selectNotice")){ /** 공지사항 */
			return getNotice(request);
		}else if(request.isMode("selectBoardContent")){
			return getBoardContent(request);
		}else if(request.isMode("getNewBoardNo")){
			return getNewBoardNo(request);
		}else if(request.isMode("updateBoard")){
			return modifyContent(request);
		}else if(request.isMode("searchAllNotice")){
			return searchAllNotice(request);
		}else if(request.isMode("selectRecentList")){ //커뮤니티 메인화면 공지사항/FAQ 최근 4항목 리스트
			return selectRecentList(request);
		}else if(request.isMode("deleteBoardContent")){ //게시글 삭제
			return delBoardContent(request);
		}else {
			return write(null);
		 }
	}

	private IResponse selectRecentList(IRequest request) {
		// TODO Auto-generated method stub
		//커뮤니티 메인화면 공지사항/FAQ 최근 4항목 리스트
		//log.info("커뮤니티 메인 공지사항/FAQ 최근항목 가져오기");
		
		JSONObject obj = new JSONObject();
		
		Map noticeMap = null;
		Map FAQMap = null;
		
		List listsNotice;
		List listsFAQ;
		
		 Map smap = request.getMap();
		 smap.put("dbName", pm.getString("component.sql.database"));
		if("CP".equals(request.getStringParam("callFrom"))){
			smap.put("BOARD_CLS_CD1", "B");
			smap.put("BOARD_CLS_CD2", "F");
		}else{
			smap.put("BOARD_CLS_CD1", "N");
			smap.put("BOARD_CLS_CD2", "P");
		}
		listsNotice = getList("mg.cppp.selectRecentNotice",smap);
		listsFAQ = getList("mg.cppp.selectRecentFAQ",smap);
    	//log.info("공지사항 최근 항목 : " + listsNotice.toString());
    	//log.info("FAQ 최근 항목 : " + listsFAQ.toString());
		
    	//log.info("공지사항 개수 : " + listsNotice.size());
    	//log.info("FAQ 개수 : " + listsFAQ.size());
    	
    	/*for (int i = 0; i < listsNotice.size(); i++) {
    		noticeMap =(Map) listsNotice.get(i);
    		obj.put("BTITLE" + i, this.getStringWithNullCheck(noticeMap, "BTITLE"));
    		obj.put("REG_DT" + i, this.getStringWithNullCheck(noticeMap, "REG_DT"));
        }
    	
    	for (int i = 0; i < listsFAQ.size(); i++) {
    		FAQMap =(Map) listsFAQ.get(i);
    		obj.put("BTITLE" + (i+4), this.getStringWithNullCheck(FAQMap, "BTITLE"));
    		obj.put("REG_DT" + (i+4), this.getStringWithNullCheck(FAQMap, "REG_DT"));
        }*/
    	
    	for (int i = 0; i < 4; i++) {
    		
    		if(i<listsNotice.size()){
    			noticeMap =(Map) listsNotice.get(i);
    			obj.put("BOARD_NO" + i, this.getStringWithNullCheck(noticeMap, "BOARD_NO"));
        		obj.put("BTITLE" + i, "ㆍ " + this.getStringWithNullCheck(noticeMap, "BTITLE"));
        		obj.put("REG_DT" + i, "[" + this.getStringWithNullCheck(noticeMap, "REG_DATE") + "]");
    		}else{
    			obj.put("BOARD_NO" + i, "");
    			obj.put("BTITLE" + i, "&nbsp");
        		obj.put("REG_DT" + i, "&nbsp");
    		}
    		
        }
    	
    	for (int i = 0; i < 4; i++) {
    		
    		if(i<listsFAQ.size()){
    			FAQMap =(Map) listsFAQ.get(i);
    			obj.put("BOARD_NO" + (i+4), this.getStringWithNullCheck(FAQMap, "BOARD_NO"));
    			obj.put("BTITLE" + (i+4), "ㆍ " + this.getStringWithNullCheck(FAQMap, "BTITLE"));
        		obj.put("REG_DT" + (i+4), "[" + this.getStringWithNullCheck(FAQMap, "REG_DATE") + "]");
    		}else{
    			obj.put("BOARD_NO" + (i+4), "");
    			obj.put("BTITLE" + (i+4), "&nbsp");
        		obj.put("REG_DT" + (i+4), "&nbsp");
    		}
    		
        }

    	log.info("Json 객체 내용 : " + obj.toString());
    	
    	//this.getStringWithNullCheck(noticeMap, "CNT")
    	
    	return write(obj.toString());

	}

	private IResponse searchAllNotice(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        log.info("searchAllNotice");
        Map smap = request.getMap();
       
        smap.put("dbName", pm.getString("component.sql.database"));
        String dbName = pm.getString("component.sql.database");
        try{
        	List lists;
        	if("mssql".equals(dbName)){
        		lists = getList("mg.cppp.selectAllNotice",smap);
        	}else{
        		lists = getList("mg.cppp.selectAllNotice_"+dbName,smap);
        	}
        	 log.info("---------------------------"+smap.toString());
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	   
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
       
        } catch(Exception e){
        	log.error("searchAllNotice---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	private IResponse modifyContent(IRequest request) {
		try{
			tx.begin();
			Map smap = request.getMap(); 
			updateObject("mg.cppp.updateBoard",smap);
			tx.commit(); 
			return write("updateBoard Success");
		}catch(Exception e){
			log.error("modifyContent-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("updateBoard Fail");
	}
}
	
	private IResponse getNewBoardNo(IRequest request) {
		// TODO Auto-generated method stub
		Map smap = request.getMap(); 
		JSONObject obj = new JSONObject();
		try{
			smap.put("dbName", pm.getString("component.sql.database"));
			Map map = (Map)getItem("mg.cppp.getNewBoardNo",smap);
			String count ="";
			if("".equals(this.getStringWithNullCheck(map, "BOARD_NO"))){
				count="1";
			}else{
				count =Integer.toString(Integer.parseInt(this.getStringWithNullCheck(map, "BOARD_NO").substring(1))+1); 
			}
			obj.put("DocNo", count);
			log.info("getNewBoardNo----------------------------------------------------count :"+count);
			return write(obj.toString());
		}catch(Exception e){
			log.error("getNewBoardNo--------------------------------------------------------------------"+e.toString());
			return write(obj.toString());
		}
	}

	private IResponse getBoardContent(IRequest request) {
		// TODO Auto-generated method stub
		Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		try{
    	Map map = (Map)getItem("mg.cppp.selectBoardContent",smap);
    	
    		// Creation of Json Response 
    		 JSONObject obj = new JSONObject();
    		obj.put("BOARD_NO", this.getStringWithNullCheck(map, "BOARD_NO"));
    		obj.put("BOARD_CLS_CD", this.getStringWithNullCheck(map, "BOARD_CLS_CD"));
    		obj.put("BTITLE", this.getStringWithNullCheck(map, "BTITLE"));
    		obj.put("BCONTENT", this.getStringWithNullCheck(map, "BCONTENT"));//HTML 통으로 담는 것
    		obj.put("BTEXT", this.getStringWithNullCheck(map, "BTEXT")); //택스트만 빼서 담는 것
    		obj.put("DEL_YN", this.getStringWithNullCheck(map, "DEL_YN"));
    		obj.put("CHG_ID", this.getStringWithNullCheck(map, "CHG_ID"));
    		obj.put("CHG_DT", this.getStringWithNullCheck(map, "CHG_DT"));
    		String fileList ="";
    		List lists = getList("mg.cppp.getAddFileList",smap);
    		
    		String div = "";
    		for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	fileList += div + this.getStringWithNullCheck(map, "FILE_NAME");
	         	div = "|";
	        }
    		obj.put("FILE_LIST", fileList);
    		return write(obj.toString());
         } catch(Exception e) {
       	 	log.error("---BOARD 불러오기 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
		
	}

	private IResponse setContent(IRequest request) {
		// TODO Auto-generated method stub
		try{
			tx.begin();
			Map smap = request.getMap(); 
			smap.put("dbName", pm.getString("component.sql.database"));
			/*Map map = (Map)getItem("mg.cppp.getNewBoardNo",smap);
			String count ="";
			if("".equals(this.getStringWithNullCheck(map, "CNT"))){
				count="1";
			}else{
				count =Integer.toString(Integer.parseInt(this.getStringWithNullCheck(map, "CNT"))+1); 
			}
			smap.put("BOARD_NO", count);*/
			
	     	createObject("mg.cppp.insertBoard",smap);
	
	     	tx.commit(); 
	    
	     	return write("insertBoard Success");
		}catch(Exception e){
			log.error("setContent-----------------------------------------------------------"+e.toString());
			tx.rollback();
			return write("insertBoard Fail");
		}
	}

	private IResponse getNotice(IRequest request) {
		// TODO Auto-generated method stub
		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
        String dbName = pm.getString("component.sql.database");
        try{
        	List lists;
        	if("mssql".equals(dbName)){
        		lists = getList("mg.cppp.selectNotice",smap);
        	}else{
        		lists = getList("mg.cppp.selectNotice_"+dbName,smap);
        	}
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	   
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
       
        } catch(Exception e){
        	log.error("getNotice---------------------------------------------------------------\n" + e.toString());
        	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}

	private IResponse delBoardContent(IRequest request) {
		Map smap = request.getMap();
		smap.put("dbName", pm.getString("component.sql.database"));
		try{
			deleteObject("mg.cppp.deleteBoardContent",smap);
	    		
	    	return write("success");
		} catch(Exception e) {
       	 	log.error("---BOARD 삭제 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("COMG_1008", request.getUser().getLocale()));
         }
	}
}
