package mg.common;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.SeedEncryptUtil;

public class summaryInfoAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		if(request.isMode("getCountCP")) {
            return getCheckList(request);
		 }else if(request.isMode("getDashBoardExport")){
			return getDashBoardExport(request); 
		 }else if(request.isMode("getDashBoardRepack")){
			 return getDashBoardRepack(request);
		 }else if(request.isMode("getDashBoardApprover")){
			 return getDashBoardApprover(request);
		 }else {
			 return write(null);
		 }
		
	}
	
	private IResponse getDashBoardApprover(IRequest request) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		
		try {
			Map smap = request.getMap();
			smap.put("CHECK_DAY",pm.getInt("cliptorplus.dashboard.period.day"));
			smap.put("dbName", pm.getString("component.sql.database"));
			
			//총 수신된 반출신청 건수
			Map map = (Map)getItem("mg.common.selectAllReciverReqCnt",smap);
			obj.put("allReciverRepCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//총 승인건수
			map = (Map)getItem("mg.common.selectAllApproveReqCnt",smap);
			obj.put("allApproveReqCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
	
			//총 반려건수
			map = (Map)getItem("mg.common.selectAllBanReqCnt",smap);
			obj.put("allBanReqCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//2주가 지난 신청건수
			map = (Map)getItem("mg.common.selectExpireReqCnt",smap);
			obj.put("expireReqCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//최근 2주사이에 승인을 기다리는 건수
			map = (Map)getItem("mg.common.selectWaitReqCnt",smap);
			obj.put("waitReqCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//최근 2주사이의 승인건수
			map = (Map)getItem("mg.common.selectApproveReqCnt",smap);
			obj.put("approvalReqCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
	
	
			return write(obj.toString());

		}catch(Exception e) {
			log.error("---CheckList Action 실패-------------------------\n" + e.toString());
			return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
		}
	}

	private IResponse getDashBoardRepack(IRequest request) {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject();
		
		try {
			Map smap = request.getMap();
			smap.put("CHECK_DAY",pm.getInt("cliptorplus.dashboard.period.day"));
			smap.put("dbName", pm.getString("component.sql.database"));
			
			//총 반입된 문서
			Map map = (Map)getItem("mg.common.selectAllRepCnt",smap);
			obj.put("allRepCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//반입된 문서 중 다운로드를 받지 않은 문서
			map = (Map)getItem("mg.common.selectWaitRepCnt",smap);
			obj.put("waitRepCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
	
			//waitUserCnt : 자신을 담당자로 지정하여 등록한 승인을 기다리는 사용자 수.
			map = (Map)getItem("mg.common.selectWaitUserCnt",smap);
			obj.put("waitUserCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//approvalUserCnt : 나의 협업 사용자 : 내가 승인을 해준 사용자들
			map = (Map)getItem("mg.common.selectApprovedUserCnt",smap);
			obj.put("approvalUserCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
	
	
	return write(obj.toString());

}catch(Exception e) {
	 	log.error("---CheckList Action 실패-------------------------\n" + e.toString());
	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
 }
	}

	private IResponse getDashBoardExport(IRequest request) {
		JSONObject obj = new JSONObject();
		
		try {
			Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
       	
        	//reqCnt : 총 반출 신청 건수
	    	Map map = (Map)getItem("mg.common.selectAllRequestCnt",smap);
	    	obj.put("reqCnt", this.getStringWithNullCheck(map, "CNT"));
	    	map.clear();
	    	
	    	//aprovalCnt : 총 승인건수
	    	map = (Map)getItem("mg.common.selectAllApprovalCnt",smap);
			obj.put("aprovalCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//deniedCnt : 총 반려건수
			map=(Map)getItem("mg.common.selectAllDeniedCnt",smap);
			obj.put("deniedCnt", this.getStringWithNullCheck(map,"CNT"));
			
			//expireCnt : 유효 기한 지난 후 미승인 건수
			//smap.put("CHECK_DAY", pm.getString("cliptorplus.dashboard.period.day"));
			smap.put("CHECK_DAY",pm.getInt("cliptorplus.dashboard.period.day"));
			map=(Map)getItem("mg.common.selectExpireCnt",smap);
			obj.put("expireCnt", this.getStringWithNullCheck(map,"CNT"));
			
			//waitCnt :  유효 기간 내 승인대기 건수
			map = (Map)getItem("mg.common.selectApprovalWaitCnt",smap);
			obj.put("waitCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//finishCnt : 유효 기간 내 승인 완료 건수
			map = (Map)getItem("mg.common.selectApprovalCnt",smap);
			obj.put("finishCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//noReadCnt: 반출 후 수신자가 한번이라도 OPEN을 하지않았을 경우 
			map = (Map)getItem("mg.common.selectNoReadReciverCnt",smap);
			obj.put("noReadCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
	    	return write(obj.toString());
		
		}catch(Exception e) {
       	 	log.error("---CheckList Action 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	}

	private IResponse getCheckList(IRequest request) {
		
		JSONObject obj = new JSONObject();
	
		try {
			
			Map smap = request.getMap();
			smap.put("CHECK_DAY",pm.getInt("cliptorplus.dashboard.period.day"));
			smap.put("dbName", pm.getString("component.sql.database"));

        	//reqCnt : 사용현황-신청건수: 현재까지 모든 신청건수, 즉, 신청,승인,반려 상관없이 자신이 신청했을 경우
	    	Map map = (Map)getItem("mg.common.selectAllRequestCnt",smap);
	    	obj.put("reqCnt", this.getStringWithNullCheck(map, "CNT"));
	    	map.clear();
	    	
	    	//aprovalCnt : 사용현황-승인완료: 신청한 건수 중에서 승인이 된거
	    	map = (Map)getItem("mg.common.selectApprovalCnt",smap);
			obj.put("aprovalCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//waitCnt : 반출-승인대기 : 현재 승인 대기 중인 건수 
			map = (Map)getItem("mg.common.selectApprovalWaitCnt",smap);
			obj.put("waitCnt", this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//반출-읽지않음 : 반출 후 수신자가 한번이라도 OPEN을 하지않았을 경우 
			map = (Map)getItem("mg.common.selectNoReadReciverCnt",smap);
			obj.put("noReadCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//waitRepCnt : 반입문서-읽지 않음
			map = (Map)getItem("mg.common.selectWaitRepCnt",smap);
			obj.put("waitRepCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
					
			//waitUserCnt : 협업포탈 사용자 승인-승인대기 : 자신을 담당자로 지정하여 등록한 승인을 기다리는 사용자 수.
			map = (Map)getItem("mg.common.selectWaitUserCnt",smap);
			obj.put("waitUserCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
			//approvalUserCnt
			map = (Map)getItem("mg.common.selectApprovedUserCnt",smap);
			obj.put("approvalUserCnt",this.getStringWithNullCheck(map,"CNT"));
			map.clear();
			
	    	return write(obj.toString());
		
		}catch(Exception e) {
       	 	log.error("---CheckList Action 실패-------------------------\n" + e.toString());
       	 	return write(mm.getMessage("CPMG_1002", request.getUser().getLocale()));
         }
	    	
	}
	
}