package mg.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.event.ListDataEvent;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;

public class vdrLocAction  extends BaseAction{

	@Override
	public IResponse run(IRequest request) throws Exception {
		 if(request.isMode("getAccess")) {     //엑서스 조회
			  return getAccess(request);                                                  
	      }else if(request.isMode("getDataStatus")) {    //업로드 데이터 현황
			  return getDataStatus(request);                                                  
	      }else if(request.isMode("getSandbox")) {    // 샌드박스 로그 
			  return getSandbox(request);                                                  
	      }else{
	            return write(null);
	        }
	}
	
	/**
	 * 폴더 /문서별 엑서스 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getAccess(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();
		String user_id = (null != request.getUser()) ? request.getUser().getId() : "admin";
		Locale loc = (null != request.getUser()) ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);

		try {

			Map item = request.getMap();
            
			item.put("R_USER_ID", user_id);

			List lists = getList("mg.common.getAccess" + getDBName(pm.getString("component.sql.database")), item);
			log.info("--getAccess--" + lists.toString());
			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();
				listData.put("ROW_ID", String.valueOf(i+1));
				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}

				jArray.add(listData);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			log.info("--getAccess--" + obj.toString());
			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}      
	
	/**
	 * 기간별 업로드 데이터 현황 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getDataStatus(IRequest request) throws Exception {

		JSONObject obj = new JSONObject();
		String user_id = (null != request.getUser()) ? request.getUser().getId() : "admin";
		Locale loc = (null != request.getUser()) ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);

		try {
			
			Map item = request.getMap();
		
			List lists ;

			
			if(item.get("GROUPNM").equals("REG_DT") || item.get("GROUPNM").equals(null)){
				 lists = getList("mg.common.getDateData" + getDBName(pm.getString("component.sql.database")), item);
			}else{
				 lists = getList("mg.common.getIdData" + getDBName(pm.getString("component.sql.database")), item);
			}
			
			JSONArray jArray = new JSONArray();

			for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);

				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();
				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}

				jArray.add(listData);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	}  
	
	/**
	 * 샌드박스 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse getSandbox(IRequest request) throws Exception {

		
		JSONObject obj = new JSONObject();
		String user_id = (null != request.getUser()) ? request.getUser().getId() : "admin";
		Locale loc = (null != request.getUser()) ? request.getUser().getLocale() : new Locale(CpcConstants.DEF_LOC);
		JSONArray jArray = new JSONArray();
		try {
			Map item = request.getMap();
	
			List lists ;
				lists = getList("mg.common.getSandbox" + getDBName(pm.getString("component.sql.database")), item);

				for (int i = 0; i < lists.size(); i++) {
	
					Map map = (Map) lists.get(i);
	
					Iterator<String> keys = map.keySet().iterator();
	
					JSONObject listData = new JSONObject();
					while (keys.hasNext()) {
						String key = keys.next();
						listData.put(key, this.getStringWithNullCheck(map, key));
					}
	
					jArray.add(listData);
				}  
				
			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");

			
			return write(obj.toString());

		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			obj.put("errmsg", mm.getMessage("COMG_1001", loc));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
	} 

}
