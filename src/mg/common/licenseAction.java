package mg.common;

import java.util.HashMap;
import java.util.Map;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.SeedEncryptUtil;

public class licenseAction extends BaseAction {

	@Override
	public IResponse run(IRequest request) throws Exception {
		// TODO Auto-generated method stub
		if (request.isMode("makeProdNo")) { //관리자 제품번호 생성
            return makeProdNo(request);
        }else if(request.isMode("makeTempKey")){ //사용자 임시키 생성
        	return makeTempKey(request);
        }else if(request.isMode("makeAuthKey")){// 관리자 임시키 검사 + 인증키 생성
        	return makeAuthKey(request);
        }else if(request.isMode("chkAuthKey")){ // 사용자 인증키 검사
        	return chkAuthKey(request);
        }else {
        	return write(null);
        }
	}

	
	private IResponse makeProdNo(IRequest request) {
		// TODO Auto-generated method stub
		//제품번호 생성 - 본사 처리
		
		char prodNoArray[] = new char[20];
		
		for (int i = 0; i < 20; i++) {
			
			if(i==0){
				prodNoArray[i] = 'M';
			}else if(i==1){
				prodNoArray[i] = 'G';
			}else if(i==2){
				prodNoArray[i] = 'U';
			}else if(i==3){
				prodNoArray[i] = 'S';
			}else if(i==4){
				prodNoArray[i] = 'B';
			}else{
				prodNoArray[i] = (char) ((Math.random() * 26) + 65);
			}
			
			//System.out.print(tempKeyArray[i]);
		}
		
		String prodNo = new String(prodNoArray,0,prodNoArray.length);
		
		log.info("랜덤으로 생성된 제품번호 : " + prodNo);
		return null;
	}

	private IResponse makeTempKey(IRequest request) {
		// TODO Auto-generated method stub
		//임시키 생성 - 사용자 처리
		
		//본사로부터 입력받은 제품번호 MGUSB-PJKOK-JIBUK-GJGKU
		String prodNo = "MGUSBPJKOKJIBUKGJGKU";
		//사용자가 입력한 수량
		String num = "200";
		StringBuffer tmpNum = new StringBuffer(); //String 편집을 위해 StringBuffer 사용
		tmpNum.append(num);
		
		/*//수량을 5자리로 맞춘다
		if(tmpLicenseNum.length()==1){
			tmpLicenseNum.insert(0, "0000");
		}else if(tmpLicenseNum.length()==2){
			tmpLicenseNum.insert(0, "000");
		}else if(tmpLicenseNum.length()==3){
			tmpLicenseNum.insert(0, "00");
		}else if(tmpLicenseNum.length()==4){
			tmpLicenseNum.insert(0, "0");
		}else{}*/
		
		int numLen = tmpNum.length();
		
		if(numLen < 5) {
			for(int i = numLen; i < 5; i++) {
				tmpNum.insert(0, "0");
			}
		}
		
		
		String licenseNum = tmpNum.toString(); //String으로 변환
		
		log.info("제품번호 : " + prodNo);
		log.info("5자리로 맞춘 수량 : " + licenseNum);
		
		//숫자를 매핑된 문자로 변경
		char chLicenseNumArray[] = licenseNum.toCharArray();
		for(int i = 0 ; i < chLicenseNumArray.length ; i++){
			chLicenseNumArray[i] = (char)((int)chLicenseNumArray[i]+17);
			//log.info("chLicenseNumArray["+i+"] = "+ chLicenseNumArray[i]);
		}
		
		char prodNoArray[] = prodNo.toCharArray();
		
		char tempKeyArray[] = new char[20];
		
		// 8,11,14,16 자리에 수량(문자로변환된)을 넣고, 나머지는 랜덤생성한 문자로 채워 20자리 임시키를 만든다
		for (int i = 0; i < 20; i++) {
			
			if(i==4){
				tempKeyArray[i] = prodNoArray[i];
			}else if(i==6){
				tempKeyArray[i] = prodNoArray[i];
			}else if(i==7){
				tempKeyArray[i] = chLicenseNumArray[0];
			}else if(i==9){
				tempKeyArray[i] = prodNoArray[i];
			}else if(i==10){
				tempKeyArray[i] = chLicenseNumArray[1];
			}else if(i==12){
				tempKeyArray[i] = prodNoArray[i];
			}else if(i==13){
				tempKeyArray[i] = chLicenseNumArray[2];
			}else if(i==15){
				tempKeyArray[i] = prodNoArray[i];
			}else if(i==16){
				tempKeyArray[i] = chLicenseNumArray[3];
			}else if(i==18){
				tempKeyArray[i] = prodNoArray[i];
			}else if(i==19){
				tempKeyArray[i] = chLicenseNumArray[4];
			}
			else{
				tempKeyArray[i] = (char) ((Math.random() * 26) + 65); //랜덤으로 나머지 자리를 채울경우
			}
		}
		
		String tempKey = new String(tempKeyArray,0,tempKeyArray.length);
		
		log.info("생성된 임시키 : " + tempKey );
		
		return null;
	}
	
	
	private IResponse makeAuthKey(IRequest request) {
		// TODO Auto-generated method stub
		//임시키 체크 - 본사에서 사용자의 데이터를 체크하여, 일치할 경우 인증키 생성
		
		//String prodNo = "MGUSBPJKOKJIBUKGJGKU";
		//String tempKey = "AOSBBFJAHKAHBCKGAFKA";
		
		String prodNo = "MGUSBPJKOKJIBUKGJGKU";
		String tempKey = "AOSBBFJAHKAHBCKGAFKA";
		
		String num = "200";
		
		log.info("임시키 : " + tempKey);
		
		//MGUSB-PJKOK-JIBUK-GJGKU
		//AOSBB-FJAHK-AHBCK-GAFKA
		

		char prodNoArray[] = prodNo.toCharArray();
		char tempKeyArray[] = tempKey.toCharArray();
		
		//제품번호가 일치할 경우만 다음 진행
		if(prodNoArray[4]==tempKeyArray[4]&&prodNoArray[6]==tempKeyArray[6]&&prodNoArray[9]==tempKeyArray[9]&&
				prodNoArray[12]==tempKeyArray[12]&&prodNoArray[15]==tempKeyArray[15]&&prodNoArray[18]==tempKeyArray[18]){
			

			char tmpLicenseNum[] = new char[5];
			
			tmpLicenseNum[0] = (char)((int)tempKeyArray[7]-17);
			tmpLicenseNum[1] = (char)((int)tempKeyArray[10]-17);
			tmpLicenseNum[2] = (char)((int)tempKeyArray[13]-17);
			tmpLicenseNum[3] = (char)((int)tempKeyArray[16]-17);
			tmpLicenseNum[4] = (char)((int)tempKeyArray[19]-17);
			
		
			String licenseNum = new String(tmpLicenseNum,0,tmpLicenseNum.length);
			
			log.info("임시키에서 가져온 수량 : " + Integer.parseInt(licenseNum));
			log.info("입력한 수량 : " + Integer.parseInt(num));
			
			//수량이 일치할 경우만 다음 진행
			if(Integer.parseInt(licenseNum)==Integer.parseInt(num)){

				log.info("인증키를 생성합니다.");
				
				//제품번호와 수량을 모두 2자리씩 당겨 생성 - 나머지는 랜덤하게 생성
				
				
				char authKeyArray[] = new char[20];
				
				// 3,5,8,11,14,17 자리에 제품번호 6,9,12,15,18 자리에 수량을 넣고, 나머지는 랜덤생성한 문자로 채워 20자리 인증키를 만든다
				for (int i = 0; i < 20; i++) {
					
					if(i==2){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==4){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==5){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==7){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==8){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==10){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==11){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==13){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==14){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==16){
						authKeyArray[i] = tempKeyArray[i+2];
					}else if(i==17){
						authKeyArray[i] = tempKeyArray[i+2];
					}
					else{
						authKeyArray[i] = (char) ((Math.random() * 26) + 65); //랜덤으로 나머지 자리를 채울경우
					}
				}
				
				String authKey = new String(authKeyArray,0,authKeyArray.length);
				
				log.info("생성된 인증키 : " + authKey);
				
				/*
				 //제품번호 암호화
				String authKey;
				try {
					authKey = SeedEncryptUtil.seedEncrypt(prodNo,"");
					authKey = authKey.toUpperCase();
					log.info("생성된 인증키 : " + authKey);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.info("인증키 생성중 오류발생");
				}
				*/
				
			}else{
				log.info("수량이 일치하지 않습니다.");
			}
		
		}else{
			log.info("제품번호가 일치하지 않습니다.");
		}
		
		return null;
	}
	

	private IResponse chkAuthKey(IRequest request) {
		// TODO Auto-generated method stub
		//인증키 체크 - 사용자
		
		String prodNo = "MGUSBPJKOKJIBUKGJGKU"; //사용자가 입력한 제품번호
		String tempKey = "AOSBBFJAHKAHBCKGAFKA"; //임시키
		String authKey = "BZBWJAKKAYBCUGAHKATP"; //관리자로부터 받아 입력한 인증키
		String num = "200"; //사용자가 입력한 수량
		
		
		
		char tempKeyArray[] = tempKey.toCharArray();
		char authKeyArray[] = authKey.toCharArray();
		
		//인증키와 임시키를 비교하여 정보가 일치할 경우 라이센스 인증
		if(tempKeyArray[4]==authKeyArray[2]&&tempKeyArray[6]==authKeyArray[4]&&tempKeyArray[7]==authKeyArray[5]&&
				tempKeyArray[9]==authKeyArray[7]&&tempKeyArray[10]==authKeyArray[8]&&tempKeyArray[12]==authKeyArray[10]&&
				tempKeyArray[13]==authKeyArray[11]&&tempKeyArray[15]==authKeyArray[13]&&tempKeyArray[16]==authKeyArray[14]&&
				tempKeyArray[18]==authKeyArray[16]&&tempKeyArray[19]==authKeyArray[17]){
			
			log.info("인증 성공. 제품정보를 DB에 암호화하여 저장합니다.");
			
			// 제품번호, 수량(암호화) DB 저장 
			try {
			
				/*//제품번호20자리를 각각 16자리 / 4자리(+0으로 12자리)로 만들어 2번 암호화를 한다.
				String encryptKey = SeedEncryptUtil.seedEncrypt(prodNo,"");
				
				log.info("제품번호 String 암호화문 : " + encryptKey);
				
				String decryptKey = SeedEncryptUtil.seedDecrypt(encryptKey,"");
				
				log.info("제품번호 String 복호화문 : " + decryptKey);
				log.info("제품번호 평문 : " + prodNo);*/
				
				//수량을 16자리로 맞춘다
				StringBuffer tmpNum = new StringBuffer(); //String 편집을 위해 StringBuffer 사용
				tmpNum.append(num);
				
				int numLen = num.length();
				
				if(numLen < 16) {
					for(int i = numLen; i < 16; i++) {
						tmpNum.insert(0, "0");
					}
				}
				
				num = tmpNum.toString(); //String으로 변환
				
				log.info("16자리로 맞춘 수량 : " + num);
				
				String encryptNum = SeedEncryptUtil.seedEncrypt(num,"MGUSB");
				
				log.info("수량 String 암호화문 : " + encryptNum);
				
				//String decryptNum = SeedEncryptUtil.seedDecrypt(encryptNum,"MGUSB");
				
				//log.info("수량 String 복호화문 : " + decryptNum);
				
				
	            tx.begin();
	            
	            Map param = new HashMap();
	            param.put("ProdNo", prodNo);
	            param.put("OtherData1", encryptNum);
	            param.put("OtherData2", "");
	            param.put("OtherData3", "");
	            param.put("OtherData4", "");
	            param.put("CompanyCode", "");
	            
	            createObject("mg.common.regLicense", param);
	            
	            log.info("라이센스 DB저장 완료");
		       	
	            tx.commit();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				tx.rollback();
				e.printStackTrace();
				log.info("암호화중 오류발생");
			}
		
			
		}else{
			log.info("유효하지 않은 인증키 입니다.");
		}
		
		
		return null;
	}


	

}
