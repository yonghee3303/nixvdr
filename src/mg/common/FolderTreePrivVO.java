package mg.common;

import java.util.List;

public class FolderTreePrivVO extends TreeVO{
	private String ACES;
	private String FAD;
	private String RED;
	private String WRT;
	private String PRT;
	private String DLT;
	private String DOWN;
	public String getACES() {
		return ACES;
	}
	public void setACES(String aCES) {
		ACES = aCES;
	}
	public String getFAD() {
		return FAD;
	}
	public void setFAD(String fAD) {
		FAD = fAD;
	}
	public String getRED() {
		return RED;
	}
	public void setRED(String rED) {
		RED = rED;
	}
	public String getWRT() {
		return WRT;
	}
	public void setWRT(String wRT) {
		WRT = wRT;
	}
	public String getPRT() {
		return PRT;
	}
	public void setPRT(String pRT) {
		PRT = pRT;
	}
	public String getDLT() {
		return DLT;
	}
	public void setDLT(String dLT) {
		DLT = dLT;
	}
	public String getDOWN() {
		return DOWN;
	}
	public void setDOWN(String dOWN) {
		DOWN = dOWN;
	}
}