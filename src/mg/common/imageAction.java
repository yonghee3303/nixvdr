package mg.common;

import java.util.List;
import java.util.Map;

import mg.base.BaseAction;

import com.core.component.upload.IFileUploadManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class imageAction extends BaseAction{
	/** 파일 업로드를 위한 정의 */
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);

	public IResponse run(IRequest request) throws Exception {
  
        if (request.isMode("tx_image")) {
            return createUpload(request);
        } else {
        	return write(null);
        }
    }  
    
    private IResponse createUpload(IRequest request) {
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String Id = request.getStringParam("id");
        String filePath = "/upload" + request.getStringParam("j_file");

        Map map = request.getMap();
        log.error("--------fum.create Image Upload--------------------------------------------------\n" + map.toString());
        try {
           
	        fum.createUpload(progId, docId, request);
	        
	        log.info("upload Image File Info--->" + filePath);
	        // msg = "<script> parent.insertFileName('" + filePath + "'); </script>";
    	} catch (Exception e) {
            log.info("upload Image File Info--->" + e);
            // msg = "<script> parent.insertFileName('FAIL'); </script>";
        }
 
        return sendRedirect("/ext/smartEditor/imgupload.jsp?id=" + Id + "&imageurl=" + filePath);
    }
}
