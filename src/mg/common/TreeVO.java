package mg.common;

import java.util.List;

public class TreeVO {
    private String key;
    private String title;
    private String parent;
    private boolean isFolder;
    private List children;
    private Object tag = null;
    private String ACES; 
    private String FAD; 
    private String RED; 
    private String WRT; 
    private String PRT; 
    private String DLT; 
    private String DOWN;
    
    public Object getTag() {
		return tag;
	}

	public void setTag(Object tag) {
		this.tag = tag;
	}

	public String getKey() {
        return key;
    }
    
    public void setKey(String key) {
        this.key = key;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getParent() {
        return parent;
    }
    
    public void setParent(String parent) {
        this.parent = parent;
    }
    
    public List getChildren() {
        return children;
    }
    
    public void setChildren(List children) {
        this.children = children;
    }
    
    public boolean getIsFolder() {
        return isFolder;
    }
    
    public void setIsFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }

	public String getACES() {
		return ACES;
	}

	public void setACES(String aCES) {
		ACES = aCES;
	}

	public String getFAD() {
		return FAD;
	}

	public void setFAD(String fAD) {
		FAD = fAD;
	}

	public String getRED() {
		return RED;
	}

	public void setRED(String rED) {
		RED = rED;
	}

	public String getWRT() {
		return WRT;
	}

	public void setWRT(String wRT) {
		WRT = wRT;
	}

	public String getPRT() {
		return PRT;
	}

	public void setPRT(String pRT) {
		PRT = pRT;
	}

	public String getDLT() {
		return DLT;
	}

	public void setDLT(String dLT) {
		DLT = dLT;
	}

	public String getDOWN() {
		return DOWN;
	}

	public void setDOWN(String dOWN) {
		DOWN = dOWN;
	}

	public void setFolder(boolean isFolder) {
		this.isFolder = isFolder;
	}
}