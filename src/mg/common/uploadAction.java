package mg.common;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.File;

import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.component.upload.IFileUploadManagement;
import com.core.framework.core.IResponse;
import com.core.framework.core.IRequest;
import com.core.component.variablexml.IVariableXml;


/**
* 업무 그룹명 : mg.common
* 서브 업무명 : 
* 작성자 : cyleem
* 작성일 : 2008. 09. 17
* 설 명 : UploadAction
*/

public class uploadAction extends BaseAction{

	/** 파일 업로드를 위한 정의 */
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);

	public IResponse run(IRequest request) throws Exception {
  
        if (request.isMode("search")) {/*search*/
            return searchUpload(request);
        }else if (request.isMode("searchNimsFolder")) {
            return searchNimsFolder(request);
        }else if (request.isMode("searchNimsFolderAuth")) {
            return searchNimsFolderAuth(request);
        }else if (request.isMode("searchNimsFile")) {
            return searchNimsFile(request);
        }else if (request.isMode("searchRepacking")) {
            return searchUploadRepacking(request);
        } else if (request.isMode("searchHtml")) {
            return searchHtml(request);
        } else if (request.isMode("tx_create")) {
            return createUpload(request);
        } else if (request.isMode("tx_create2")) {
            return createUpload2(request);
        } else if (request.isMode("tx_delete")) {
            return deleteUpload(request);
        } else if (request.isMode("tx_update")) {
            return updateUpload(request);
        } else if (request.isMode("fileDownload")) {
            return fileDownload(request);
        } else if (request.isMode("tx_create_LG")) {
            return createUploadLG(request);
        }  else if (request.isMode("tx_create_VF")) {
            return createUploadVF(request);
        }  else if (request.isMode("tx_create_vdr")) {
            return createUploadVDR(request);
        } else {
        	return write(null);
        }
    }


	private IResponse createUploadLG(IRequest request) {
		// TODO Auto-generated method stub

        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String msg="";
        Map map = request.getMap();
        log.info("--------fum.createUploadLG--###-------------\n" + map.toString());
        try {
        	String userid = request.getUser().getId();
	    	tx.begin(); 
	        fum.createUpload(progId, docId, request);
	        // Upload된 파일명 조회
	        msg = "<script> parent.filelistframe.uploadResult('SUCCESS'); </script>";
	        tx.commit();
    	} catch (Exception e) {
            tx.rollback();
            log.info("uploadFile Info--->" + e);
            msg = "<script> parent.filelistframe.uploadResult('FAIL'); </script>";
        }
        return write(msg);
	}

	private IResponse createUploadVF(IRequest request) 
	{
		JSONObject obj = new JSONObject();
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String userid = request.getStringParam("SECUSER");
        String msg="";
        Map map = request.getMap();
        log.info("--------fum.createUploadVF--###-------------\n" + map.toString());
        try {
        	// String userid = request.getUser().getId();
	    	tx.begin(); 
	        fum.UploadNoUser(progId, docId, userid, request);
	        // Upload된 파일명 조회
	        tx.commit();
	        
	        obj.put("UPLOAD_UUID", docId);
	        obj.put("errcode", "0");
			obj.put("errmsg", "SUCCESS.");
    	} catch (Exception e) {
            tx.rollback();
            obj.put("errcode", "-101");
			obj.put("errmsg", "createUploadVF File Upload Exception Error : .\n" + e.getMessage());
        }
        return write(obj.toString());
	}

	private IResponse createUploadVDR(IRequest request) 
	{
		JSONObject obj = new JSONObject();
        String docId = request.getStringParam("docId");
        log.info("docId = "+docId);
        String progId = request.getStringParam("pgmId");
        log.info("pgmId = "+progId);
        String userid = request.getStringParam("SECUSER");
        log.info("SECUSER = "+userid);
        String msg="";
        Map map = request.getMap();
        log.info("--------fum.createUploadVF--###-------------\n" + map.toString());
        try {
        	// String userid = request.getUser().getId();
	    	tx.begin(); 
	        fum.UploadNoUser(progId, docId, userid, request);
	        // Upload된 파일명 조회
	        tx.commit();
	        
	        obj.put("UPLOAD_UUID", docId);
	        obj.put("errcode", "0");
			obj.put("errmsg", "SUCCESS.");
    	} catch (Exception e) {
            tx.rollback();
            obj.put("errcode", "-101");
			obj.put("errmsg", "createUploadVF File Upload Exception Error : .\n" + e.getMessage());
        }
        return write(obj.toString());
	}

	private IResponse searchUpload(IRequest request) throws Exception 
    {	
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        log.info("select 파일 리스트 조건 : " + request.getMap().toString());
        
        try {
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.common.selectFileUpload",request.getMap());
	       	 } else {
	       		 lists = getList("mg.common.selectFileUpload_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        
	        return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }   
    }
	
	private IResponse searchNimsFolder(IRequest request) throws Exception 
    {	
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        Map item = request.getMap();
        GridData gdRes = new GridData();
        String user_id = request.getUser().getId();
	    String folder_id = request.getStringParam("CHECK_P_ID");
        
        log.info("select 파일 리스트 조건 : " + request.getMap().toString());
        
        try {
        	 List lists;
        	 
        	 log.info("request 완");
 		   	item.put("USER_ID", user_id);
 		   	item.put("FOLDER_ID", folder_id);
 		   log.info("item put 완");
 			String userAuth = getItem("mg.vdr.NimsouGetFolderSetAuth",item).toString();
 			log.info(userAuth);
 			String downAuth = getItem("mg.vdr.NimsouGetDownAuth",item).toString();;
 			String fileAuth = getItem("mg.vdr.NimsouGetFileAuth",item).toString();;
 			
 			String AuthTF = downAuth+fileAuth;
 			
 			log.info(AuthTF);

        	 lists = getList("mg.common.searchNimsFolder",request.getMap());

	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        
	        return writeXml(responseSelData(AuthTF, userAuth, "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }   
    }
	
	private IResponse searchNimsFolderAuth(IRequest request) throws Exception 
    {	
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        Map item = request.getMap();
        GridData gdRes = new GridData();
        String user_id = request.getUser().getId();
	    String folder_id = request.getStringParam("CHECK_P_ID");
        
        log.info("select 파일 리스트 조건 : " + request.getMap().toString());
        
        try {
        	 List lists;
        	 
 		   	item.put("FOLDER_ID", folder_id);
 		   log.info("folder_id : "+folder_id);
        	 lists = getList("mg.common.searchNimsFolderAuth",item);
        	 log.info("조회 리스트 : "+lists);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        
	        return writeXml(responseSelData("조회완료", "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }   
    }
	
	private IResponse searchNimsFile(IRequest request) throws Exception 
    {	
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        log.info("select 파일 리스트 조건 : " + request.getMap().toString());
        
        try {
        	 List lists;
	       	 
        	 lists = getList("mg.common.selectNimsFileUpload",request.getMap());
	       	 
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        
	        return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }   
    }
    
	private IResponse searchUploadRepacking(IRequest request) throws Exception 
    {	
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        log.info("select 파일 리스트 조건 : " + request.getMap().toString());
        
        try {
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.common.selectRepackingFileUpload",request.getMap());
	       	 } else {
	       		 lists = getList("mg.common.selectRepackingFileUpload_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        
	        return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }   
    }
	
    private IResponse searchHtml(IRequest request) throws Exception 
    {	       
        Map map = null;
  
        try {	   
        		String userid = request.getUser().getId();
        	    List lists = getList("mg.common.selectFileUpload",request.getMap());
	            StringBuffer buffer = new StringBuffer();
	
	            buffer.append("<table>");	            
	            for (int i = 0; i < lists.size(); i++) {
	            	map =(Map) lists.get(i);
	            	buffer.append("<tr>");
	            	buffer.append("<td style=\"text-align:left\"><a href=\"javascript:doFiledownLoad('" + map.get("PGM_ID").toString() + "','" + map.get("DOC_ID").toString() + "','" + map.get("FILE_NAME").toString() + "');\">");
	            	buffer.append(map.get("FILE_NAME").toString() + "    [" + map.get("FILE_SIZE").toString() + "]"); 
	                buffer.append("</a></td>");
	                buffer.append("</tr>");
	            }
	            buffer.append("</table>");
	            return write(buffer.toString());
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return write("");
        }
    }
        
    private IResponse createUpload(IRequest request) 
    {	
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String msg="";
        Map map = request.getMap();
        Map item = null;
	    item = new HashMap();
        log.info("--------fum.createUpload--###-------------\n" + map.toString());
        try {
        	String userid = request.getUser().getId();
        	item.put("docId", docId);
    		tx.begin();
	        fum.createUpload(progId, docId, request);
	        // Upload된 파일명 조회
	        msg = "<script> parent.uploadResult('SUCCESS'); </script>";
	        // 파일 올리면 인력 정보에 파일 유무 데이터 추가

	        updateObject("mg.ni.manUpdateFileY", item);	        
	        
	        tx.commit();
	    	
    	} catch (Exception e) {
            tx.rollback();
            log.info("uploadFile Info--->" + e);
            msg = "<script> parent.uploadResult('FAIL'); </script>";
        }
        return write(msg);
    }
    
    private IResponse createUpload2(IRequest request) 
    {	
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String msg="";
        JSONObject obj = new JSONObject();
        Map map = request.getMap();
        Map item = null;
	    item = new HashMap();
        log.info("--------fum.createUpload--###-------------\n" + map.toString());
        try {
        	String userid = request.getStringParam("userId");
        	item.put("docId", docId);
    		tx.begin();
	        // fum.createUpload(progId, docId, request);
	        fum.VFUpload(progId, docId, request);
	        // Upload된 파일명 조회
	        msg = "<script> parent.uploadResult('SUCCESS'); </script>";
	        tx.commit();
	        obj.put("ID", progId);   
		    obj.put("errcode", "0");
		    obj.put("errmsg", "success");
		    return write(obj.toString());
	    	
    	} catch (Exception e) {
            tx.rollback();
            log.info("uploadFile Info--->" + e);
            msg = "<script> parent.uploadResult('FAIL'); </script>";
            obj.put("ID", progId); 
			obj.put("errcode", "-1");
		    obj.put("errmsg", "업로드 실패");
		    return write(obj.toString());
        }
    }

    private IResponse deleteUpload(IRequest request) {
    	String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", retflag;

        try {
        	String userid = request.getUser().getId();
        	tx.begin();
        	Map item = null;
	          for (int i = 0; i < ids.length; i++) {
	         	 item = new HashMap();
	         	 
	         	 for(int j=0; j < cols.length; j++ ) {
	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	           	 }
	         	 item.put("pgmId", request.getMap().get("pgmId").toString());
	         	 item.put("docId", request.getMap().get("docId").toString());
	         	 log.info("----------mg.common.deleteUpload--------------------\n" + item.toString());
	         	 deleteObject("mg.common.deleteUpload", item);
	         	 // 파일 지우면 파일 유무 확인 후 인력정보에 파일유무 데이터 추가
	         	if( Integer.parseInt(getItem("mg.ni.manHasFileYN", item).toString() ) > 0 ) {
		        	updateObject("mg.ni.manUpdateFileY", item);
			   	} else {
			   		updateObject("mg.ni.manUpdateFileN", item);
			   	}
	         	 
	         	 String fileName = (String) item.get("FILE_NAME");
	         	 String filePath = (String) item.get("FILE_PATH");
	         	 
	         	 log.info("======"+fileName+"==="+filePath);
	         	 File file = new File("/home/nimsou"+filePath+"/"+fileName);
	         	 if(file.exists()) {file.delete();}

	          }
	        tx.commit();
	        msg = "삭제 되었습니다.";
	        retflag = "true";
    	} catch (Exception e) {
            tx.rollback();
            msg= "삭제시 오류가 발생 하였습니다.\n" + e;
            retflag= "false";
        } 
    	return writeXml(responseTranData(msg, retflag, "doSave", "doSaveEnd"));
    }
    
    private IResponse nims_fileDelete(IRequest request) {
    	String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", retflag;

        try {
        	String userid = request.getUser().getId();
        	tx.begin();
        	Map item = null;
	          for (int i = 0; i < ids.length; i++) {
	         	 item = new HashMap();
	         	 
	         	 for(int j=0; j < cols.length; j++ ) {
	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	           	 }
	         	 item.put("pgmId", request.getMap().get("pgmId").toString());
	         	 item.put("docId", request.getMap().get("docId").toString());
	         	 log.info("----------mg.common.nimsDeleteUpload--------------------\n" + item.toString());
	         	 deleteObject("mg.common.nimsDeleteUpload", item);
	         	 
	         	 String fileName = (String) item.get("DOC_NM");
	         	 String filePath = (String) item.get("DOC_PATH");
	         	 
	         	 log.info("======"+fileName+"==="+filePath);
	         	 File file = new File("/home/nimsou"+filePath+"/"+fileName);
	         	 if(file.exists()) {file.delete();}

	          }
	        tx.commit();
	        msg = "삭제 되었습니다.";
	        retflag = "true";
    	} catch (Exception e) {
            tx.rollback();
            msg= "삭제시 오류가 발생 하였습니다.\n" + e;
            retflag= "false";
        } 
    	return writeXml(responseTranData(msg, retflag, "doSave", "doSaveEnd"));
    }
    
    private IResponse updateUpload(IRequest request) {
    	
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String msg="";
        try {
        	String userid = request.getUser().getId();
	    	tx.begin();      
	        fum.updateUpload(progId, docId, request);
	        tx.commit();
	        msg = "저장되었습니다.";
    	} catch (Exception e) {
            tx.rollback();
            msg= "저장시 오류가 발생 하였습니다.\n" + e;;
        }
        return write(msg);
    }
	
    private IResponse fileDownload(IRequest request) {
    	
    	log.info("PGM_ID 값 : " + request.getStringParam("PGM_ID"));
    	
    	String rootPath = "";
    	rootPath = getParameter("component.upload.directory");
    	
/*    	if("regMgmt".equals(request.getStringParam("PGM_ID"))){
    		rootPath = getParameter("component.upload.directory");
        	updateObject("mg.common.updateDocDownCnt", request.getMap());
    	}else if("repack".equals(request.getStringParam("PGM_ID"))){
    		rootPath = getParameter("component.upload.exedirectory");
        	updateObject("mg.common.updateDocDownCnt", request.getMap());
    	}else if("boardList".equals(request.getStringParam("PGM_ID"))){
    		rootPath = getParameter("component.upload.directory");
    	}*/
    	String filePath = getItem("mg.common.getFilePath", request.getMap()).toString();
        //String filePath = request.getStringParam("FILE_PATH");
    	String filename = request.getStringParam("FILE_NAME");
        //String filename = request.getStringParam("FILE_NAME");
        String retfile = rootPath + filePath + "/" + filename;
        log.info("----------------retfile---------------------------\n" + retfile);
    	        
        return downloadFile(retfile);
    }
}
