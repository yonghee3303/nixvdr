package mg.common;

/**
* Author: jsyang
* Date: 2019. 05. 22
*/ 
public interface CpcConstants {

	public static final String DEF_LOC = "ko"; // default locale
	public static final String EN_LOC= "en"; 
	public static final String SYSADMIN_CD= "S01"; 
	public static final String TODO = "TODO"; 
	
	
	// Response
	public static final String ERRMSG= "errmsg"; 
	public static final String ERRCODE= "errcode"; 
	public static final String EXCEPTION= "exception"; 
	
	
    public static final String USER = "j_user";
    public static final String USER_NOT_FOUND= "Requested USER_ID does Not exists" ;
	
}
