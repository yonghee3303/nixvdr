package mg.common;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.menu.IMenu;
import com.core.component.menu.IMenuManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;


/**
* 업무 그룹명 : mg.common
* 서브 업무명 : excelDownAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 23 
* 설 명 : 각 화면 Grid Excel 출력
*/ 
public class excelDownAction extends BaseAction {

    /**
     * Excel Download 
     * @param request    
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception { 

        if(request.isMode("excelDown")) {   /**  화면 Grid Download    */
            return excelDownload(request);                           
        } else {  
            return write(null);   
        }
    }  
         
   
    /**
     * Excel Download 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse excelDownload(IRequest request) throws Exception 
    {
         String ymd = request.getUser().getProperty("TODAY").toString();
         String user_id = request.getUser().getId().toString();
         String menu_id = request.getStringParam("formId");
         
         if(!"".equals(menu_id)) {
        	 return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
         }
         
         IMenuManagement mm = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
         IMenu hmenu = mm.getMenu(menu_id);
         String menu_name = hmenu.getName();
    	 String menu_sqlid =  hmenu.getImage();
         
         try {
	            IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	            IExcelDocument doc = excel.createDocument( menu_name);
	            
	            List dList = getList(menu_sqlid, request.getMap());
	            makeExcelReport(menu_id, doc, dList);

	            return doc.download(request, menu_name + ymd + "_" + user_id + ".xls");
         } catch(Exception e) {
        	 	log.error("---------------------------------------------------------------\n" + e.toString());
        	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
         }
         
    }

}