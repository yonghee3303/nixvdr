package mg.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import mg.base.BaseAction;
import mg.base.dbUtil;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import commonIT.secure.SeedEncryptUtil;
import mg.base.AriaEncryptUtil;
import mg.base.Sha256Util;

public class encryptAction extends BaseAction {
    /**
     * 콤보박스 세팅 
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
                
    	if(request.isMode("seedEncode")) {     /** 암호화 */
            return seedEncode(request);                           
        }else if(request.isMode("seedKeyEncode")) {     /** 암호화 */
            return seedKeyEncode(request);                           
        }else if(request.isMode("seedEncodeList")) {     /** 암호화 배열처리  */
            return seedEncodeList(request);                           
        }else if(request.isMode("seedDecode")) {     /** 복호화 */
            return seedDecode(request);                           
        }else if(request.isMode("seedKeyDecode")) {     /** Key 복호화 */
            return seedKeyDecode(request);                           
        }else if(request.isMode("seedDecodeList")) {     /** 복호화 배열처리  */
            return seedDecodeList(request);                           
        }else if(request.isMode("seedDecodeCnt")) {     /**  */
            return seedDecodeCnt(request);                           
        }else if(request.isMode("testbyte")) {     /**  */
            return testbyte(request);                           
        }else if(request.isMode("dataConversion")) {     /**  */
            return dataConversion(request);                           
        }else if(request.isMode("ariaEncode")) {     /** ARIA 암호화 Encode*/
            return ariaEncode(request);                           
        }else if(request.isMode("ariaDecode")) {     /** ARIA 암호화 Decode*/
            return ariaDecode(request);                           
        }else if(request.isMode("sha256")) {     /** SHA256 암호화*/
            return sha256Decode(request);                           
        }else if(request.isMode("shaBatch")) {     /** SHA256 암호화 batch*/
            return sha256Batch(request);                           
        }else if(request.isMode("test")) {     /** http Service test*/
            return httpTest(request);                           
        }else if(request.isMode("tcpip")) {     /** TcpIp test*/
            return tcpipTest(request);                           
        }else{   
            return write(null);
        }
    }  
    
    /**
     * 암호화 테스트
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse seedEncode(IRequest request) throws Exception 
     {
     	String srcword = request.getStringParam("srcword");
     	// String jsoncallback = request.getStringParam("jsoncallback");
         
     	log.error("---------------------------------------seedtest--------------------------------\n" + srcword);
     	String targetword = SeedEncryptUtil.seedEncrypt(srcword, "e-accounting");
     	
     	
     	// String retstring = jsoncallback + "({\"result\":\"" + targetword + "\",\"Message\":\"OK\"})";
         return write(targetword);
     } 
     
     private IResponse seedEncodeList(IRequest request) throws Exception 
     {
     	String srcword = request.getStringParam("srcword");
         String[] encodeList = new String[5];
     	
         for(int i=0; i < 5; i++) {
         	encodeList[i] = srcword;
         }
     	
     	String[] targetword = SeedEncryptUtil.seedEncryptList(encodeList, "e-accounting");
     	
     	for(int i=0; i < 5; i++) {
         	log.error("---------------------------------------encodeList--------------------------------\n" + i + "," + targetword[i]);
         }

         return write(targetword[0]);
     } 
     
     private IResponse seedKeyEncode(IRequest request) throws Exception 
     {
     	String srcword = request.getStringParam("srcword");
     	log.error("---------------------------------------seedtest--------------------------------\n" + srcword);
     	String targetword = SeedEncryptUtil.seedKeyEncrypt(srcword);
         return write(targetword);
     } 
     
     private IResponse seedDecode(IRequest request) throws Exception 
     {
     	String targetword = request.getStringParam("srcword");
     	// String jsoncallback = request.getStringParam("jcallback");

     	log.error("---------------------------------------Decode stest--------------------------------\n" + targetword);
     	
     	String decripword = SeedEncryptUtil.seedDecrypt(targetword,"e-accounting");
     		// SeedEncryptUtil.seedDecrypt(targetword); 
     	// log.error("---------------------------------------decripword--------------------------------\n" + decripword + "," + jsoncallback);
     	
     	// String retstring = jsoncallback + "({\"result\":\"" + decripword + "\",\"Message\":\"OK\"})";
     	
     	// log.error("---------------------------------------retstring--------------------------------\n" + retstring );
         return write(decripword);
     }
     
     private IResponse seedKeyDecode(IRequest request) throws Exception 
     {
     	String targetword = request.getStringParam("srcword");
     	log.error("---------------------------------------Decode stest--------------------------------\n" + targetword);
     	
     	String decripword = SeedEncryptUtil.seedKeyDecrypt(targetword);
     	log.error("---------------------------------------Decode Data--------------------------------\n" + decripword);
         return write(decripword);
     }
     
     private IResponse seedDecodeList(IRequest request) throws Exception 
     {
     	String targetword = request.getStringParam("srcword");
     	String[] targetList = new String[5];
     	
     	// String jsoncallback = request.getStringParam("jcallback");
     	
     	for(int i=0; i < 5; i++) {
         	targetList[i] = targetword;
         }

     	
     	String[] decripword = SeedEncryptUtil.seedDecryptList(targetList,"e-accounting");
     	
     	for(int i=0; i < 5; i++) {
         	log.error("---------------------------------------encodeList--------------------------------\n" + i + "," + decripword[i]);
         }
     		// SeedEncryptUtil.seedDecrypt(targetword);
     	// log.error("---------------------------------------decripword--------------------------------\n" + decripword + "," + jsoncallback);
     	// String retstring = jsoncallback + "({\"result\":\"" + decripword + "\",\"Message\":\"OK\"})";
     	// log.error("---------------------------------------retstring--------------------------------\n" + retstring );
         return write(decripword[0]);
     }
     
     private IResponse seedDecodeCnt(IRequest request) throws Exception 
     {
     	String targetword = request.getStringParam("srcword");
     	String cnt = request.getStringParam("cnt");
     	int loopCnt = Integer.parseInt(cnt);

     	String decripword ="";
     	
     	Calendar now = Calendar.getInstance();
     	log.error("----------------------------Start Time--------------------------------\n" + now.getTimeInMillis());
     	for(int i=0; i < loopCnt; i++) {
     		decripword = SeedEncryptUtil.seedDecrypt(targetword,"e-accounting");
     	}
     	Calendar aft = Calendar.getInstance();
     	
     	float difference = aft.getTimeInMillis()- now.getTimeInMillis();
     	
    
     	// Calendar proctime = aft.getTimeInMillis() - now.getTimeInMillis();
     	
     	log.error("----------------------------Ends Time--------------------------------\n" + aft.getTimeInMillis() );


     	// String retstring = jsoncallback + "({\"result\":\"" + decripword + "\",\"Message\":\"OK\"})";
     	// log.error("---------------------------------------retstring--------------------------------\n" + retstring );
     	
         return write( Float.toString(difference/1000));
     } 
     
     private IResponse testbyte(IRequest request) throws Exception 
     {
     	String srcword = request.getStringParam("srcword");
     	String EncryptData = null;
     	String DecryptData = null;
     	/*
     	byte[] bbs = targetword.getBytes();

     	for(int i=0; i < bbs.length; i++) {
     		log.error("---------------------------------------bbs--------------------------------\n" + bbs[i]);
     	}
     	*/

     	SeedEncryptUtil seedUtil = new SeedEncryptUtil();
     	boolean seedutilflag = seedUtil.setUserKey("ace_e-accounting");
     	
     	if(seedutilflag) {
     		EncryptData = seedUtil.seedEncrypt(srcword);
     		
     		DecryptData = seedUtil.seedDecrypt(EncryptData);
     	}
     	
         return write(EncryptData + "," + DecryptData);
     }
     
            
     // 암호화 Field 데이터 Conversion
     private IResponse dataConversion(IRequest request) throws Exception 
     {
     	String targetword = request.getStringParam("srcword");

     	dbUtil conn = new dbUtil() ;
 		PreparedStatement pstmt = null;
 		ResultSet rs = null;
 		String sql = null;

 		try {
 			conn.DBConnection();
 		    Connection con = conn.getConnection();
 			sql = "select DEPT_CD, DEPT_NM from MDEPT";
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				rs.next();
				
				// int hCnt = dsBatch.getHeaderCount();
				// int mCnt = hCnt + 2;
				
				String dept_cd = rs.getString(1).toString();
				String dept_nm = rs.getString(2).toString();
				
				log.error("--------------------->" + dept_cd + "," + dept_nm);

 			con.commit();
 		} catch (Exception e) {
 			log.error("ERROR", e);
 			throw new RuntimeException("ERROR", e);
 		} finally {
 			try {
 				if (pstmt != null) {
 					pstmt.close();
 				}
 			} catch (Exception ce) {
 			}
 			try {
 				if (conn != null) {
 					conn.close();
 				}
 			} catch (Exception ce) {
 			}
 		}
 		
         return write(targetword);
     }
     
     
     /**
      * ARIA 암호화 테스트
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse ariaEncode(IRequest request) throws Exception 
      {
      	String srcword = request.getStringParam("srcword");
      	// String jsoncallback = request.getStringParam("jsoncallback");
      	
      	byte[] key = new byte[32];
		for( int i = 0; i < key.length; i++ ) {
			key[i] = (byte)i;
		}
      	
      	String targetword = AriaEncryptUtil.encrypt(srcword, key, key.length*8, "UTF-8");
      	log.error("---------------------------------------ariatest----encrypt----------------------------\n" + srcword + " , " + targetword);
      	
         return write(targetword);
      } 
      
      private IResponse ariaDecode(IRequest request) throws Exception 
      {
      	String srcword = request.getStringParam("srcword");
      	// String jsoncallback = request.getStringParam("jsoncallback");

      	byte[] key = new byte[32];
		for( int i = 0; i < key.length; i++ ) {
			key[i] = (byte)i;
		}
      	
      	String targetword = AriaEncryptUtil.decrypt(srcword, key, key.length*8, "UTF-8");
      	
    	log.error("---------------------------------------ariatest-----decrypt---------------------------\n" + srcword + " , " + targetword);
          return write(targetword);
      } 
      
      private IResponse sha256Decode(IRequest request) throws Exception 
      {
      	String srcword = request.getStringParam("srcword");
      	
      	// String targetword = Sha256Util.getSha256Encryption(srcword);
      	
      	String targetword = Sha256Util.encryptSha256(srcword);
      	
    	log.error("---------------------------------------ariatest-----decrypt---------------------------\n" + srcword + " , " + targetword);
        return write(targetword);
      }
      
      private IResponse sha256Batch(IRequest request) throws Exception 
      {
      	Map map = null;
      	List lists = getList("mg.common.selectUserGroup",request.getMap());
        for (int i = 0; i < lists.size(); i++) {
         	map =(Map) lists.get(i);
         	String srcdata = map.get("PWD").toString();
         	map.put("SPWD", Sha256Util.encryptSha256(srcdata));
         	updateObject("mg.common.updateUserGroup", map);
         	
         	
        }
      	
    	// log.error("---------------------------------------ariatest-----decrypt---------------------------\n" + srcword + " , " + targetword);
        return write("OK!!");
      }
      
      private IResponse httpTest(IRequest request) throws Exception 
      {
    	String u_id = request.getStringParam("id");
        String u_pw = request.getStringParam("pwd");
      	
      	URL url;    
      	HttpURLConnection connection = null; 
      	String targetURL = getParameter("component.signon.url");
      	String urlParameters = "companyCd=ACE&loginId=" + u_id + "&pwd=" + u_pw;
      	StringBuffer a_response = new StringBuffer();      	
      	
      	try {      
      		//Create connection      
      		url = new URL(targetURL);      
      		connection = (HttpURLConnection)url.openConnection();      
      		connection.setRequestMethod("POST");      
      		connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");			      
      		connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));      
      		connection.setRequestProperty("Content-Language", "UTF-8");  			      
      		connection.setUseCaches (false);      
      		connection.setDoInput(true);      
      		connection.setDoOutput(true);      
      		
      		//Send request      
      		DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());      
      		wr.writeBytes (urlParameters);      
      		wr.flush ();      
      		wr.close ();
      		
      		//Get Response	      
      		InputStream is = connection.getInputStream();      
      		BufferedReader rd = new BufferedReader(new InputStreamReader(is));      
      		String line;      
      		       
      		while((line = rd.readLine()) != null) {        
      			a_response.append(line);        
      			a_response.append('\r');      }      
      		rd.close();      
      		
      		String rets[] = a_response.toString().split(",");
      		      		log.error("------id,pwd----------------------------->\n" + u_id + "," + u_pw);
      		log.error("-------a_response.toString(----------------------------->\n" + rets[0] + "-------"  + rets[1]);
      		
  		} catch (Exception e) {      
  			e.printStackTrace();      
  			return write("Fail Exception");   
  		} finally {      
  			if(connection != null) {        
  				connection.disconnect();       
  			} 
  			return write(a_response.toString()); 
  		}
      	
  		
  		// http://localhost:8091/cp/Encrypt.do?mod=test&id=1103771&pwd=0421bjww
  		
//  	 http://usim.serviceace.co.kr/cp/Encrypt.do?mod=test&id=1103771&pwd=0421bjww
      	
    	// log.error("---------------------------------------ariatest-----decrypt---------------------------\n" + srcword + " , " + targetword);

      }
      
      private IResponse tcpipTest(IRequest request) throws Exception 
      {
	    	 
    	  String retval="";
    	  		try {

	    		log.error("--Encryption Key----------------------------------------------------------------------------------------\n");
	    		// log.error("Create a connection to the server socket on the server application\n");
	    		  
	  			InetAddress host = InetAddress.getLocalHost();
	  			Socket socket = new Socket(host.getHostName(), 7001); 
	  			
	  			// log.error("Send a message to the client application : " + host.getHostName() + "\n");
	  		
	  			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
	  			oos.writeObject("ace_e-accounting"); 
	  			
	  			// log.error("Read and display the response message sent by server application\n");
	  			
	  			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
	  			String message = (String) ois.readObject();
	  			
	  			log.error("Message: " + message + "\n"); 
	  			retval = message;
	  			ois.close();
	  			oos.close();        
	  		} catch (UnknownHostException e) {
	  			e.printStackTrace();       
	  		} catch (IOException e) {
	  			e.printStackTrace();        
	  		} catch (ClassNotFoundException e) {           
	  			e.printStackTrace();        
	  		}  
    	  
	  		// http://localhost:8091/cp/Encrypt?mod=tcpip
    	  return write(retval);
      }
      
}
