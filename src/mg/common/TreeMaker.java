package mg.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TreeMaker {
	Logger log = Logger.getLogger(this.getClass());

	/**
	 * 부모코드를 이용하여 계층형 트리 구성.
	 * 
	 * @param listview
	 * @return List<TreeVO>
	 */
	public List<TreeVO> makeTreeByHierarchy(List<?> listview) {
		List<TreeVO> rootlist = new ArrayList<TreeVO>();

		for (Integer i = 0; i < listview.size(); i++) {
			TreeVO mtDO = (TreeVO) listview.get(i);

			if (mtDO.getParent() == null 	|| (mtDO.getParent() != null && "-1".equals(mtDO.getParent()))) {
				rootlist.add(mtDO);
				continue;
			}
			for (Integer j = 0; j < listview.size(); j++) {
				TreeVO ptDO = (TreeVO) listview.get(j);
				if (mtDO.getParent().equals(ptDO.getKey())) {
					if (ptDO.getChildren() == null) {
						ptDO.setChildren(new ArrayList<Object>());
					}
					List<TreeVO> list = ptDO.getChildren();
					list.add(mtDO);
					ptDO.setIsFolder(true);
					break;
				}
			}
		}

		return rootlist;
	}

	public List<TreeVO> makeByHierarchy(List<?> listview, String p_id) {
		List<TreeVO> rootlist = new ArrayList<TreeVO>();

		for (Integer i = 0; i < listview.size(); i++) {
			TreeVO mtDO = (TreeVO) listview.get(i);
			
			log.info("------makeByHierarchy-=----mtDO------ > " + p_id + " : " + mtDO.getParent());

			if (mtDO.getParent() == null 	||  p_id.equals(mtDO.getParent()) || (mtDO.getParent() != null && "-1".equals(mtDO.getParent()))) {
				rootlist.add(mtDO);
				continue;
			}
			
			for (Integer j = 0; j < listview.size(); j++) {
				TreeVO ptDO = (TreeVO) listview.get(j);
				if (mtDO.getParent().equals(ptDO.getKey())) {
					if (ptDO.getChildren() == null) {
						ptDO.setChildren(new ArrayList<Object>());
					}
					List<TreeVO> list = ptDO.getChildren();
					list.add(mtDO);
					ptDO.setIsFolder(true);
					break;
				}
			}
		}

		return rootlist;
	}

	
}
