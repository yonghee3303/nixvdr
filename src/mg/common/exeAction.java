package mg.common;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.component.upload.IFileUploadManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

/**
* 업무 그룹명 : mg.common
* 서브 업무명 : 
* 작성자 : cyleem
* 작성일 : 2008. 09. 17
* 설 명 : exeAction
*           실행 파일의 Upload 기능 수행
*/

public class exeAction extends BaseAction{

	/** 파일 업로드를 위한 정의 */
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);

	public IResponse run(IRequest request) throws Exception {
  
        if (request.isMode("search")) {
            return searchUpload(request);
        } else if (request.isMode("searchHtml")) {
            return searchHtml(request);
        } else if (request.isMode("tx_create")) {
            return createUpload(request);
        } else if (request.isMode("tx_delete")) {
            return deleteUpload(request);
        } else if (request.isMode("tx_update")) {
            return updateUpload(request);
        } else if (request.isMode("fileDownload")) {
            return fileDownload(request);
        } else {
        	return write(null);
        }
    }
	
	
	private IResponse searchUpload(IRequest request) throws Exception 
    {	
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
            
        
        try {
        	 List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
        	     lists = getList("mg.common.selectFileUpload",request.getMap());
	       	 } else {
	       		 lists = getList("mg.common.selectFileUpload_" + dbName, request.getMap());
	       	 }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        return writeXml(responseSelData("조회 성공", "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        }   
    }
    
    private IResponse searchHtml(IRequest request) throws Exception 
    {	       
        Map map = null;
  
        try {	   
        		String userid = request.getUser().getId();
        	    List lists = getList("mg.common.selectFileUpload",request.getMap());
	            StringBuffer buffer = new StringBuffer();
	
	            buffer.append("<table>");	            
	            for (int i = 0; i < lists.size(); i++) {
	            	map =(Map) lists.get(i);
	            	buffer.append("<tr>");
	            	buffer.append("<td style=\"text-align:left\"><a href=\"javascript:doFiledownLoad('" + map.get("PGM_ID").toString() + "','" + map.get("DOC_ID").toString() + "','" + map.get("FILE_NAME").toString() + "');\">");
	            	buffer.append(map.get("FILE_NAME").toString() + "    [" + map.get("FILE_SIZE").toString() + "]"); 
	                buffer.append("</a></td>");
	                buffer.append("</tr>");
	            }
	            buffer.append("</table>");
	            return write(buffer.toString());
        } catch(Exception e) {
       	 	log.error("---------------------------------------------------------------\n" + e.toString());
       	 	return write("");
        }
    }
        
    private IResponse createUpload(IRequest request) {
    	
    	
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String msg="";
        Map map = request.getMap();
        log.info("--------fum.createUpload--###-------------\n" + map.toString());
        try {
        	String userid = request.getUser().getId();
	    	tx.begin(); 
	        fum.createUpload(progId, docId, request);
	        // Upload된 파일명 조회
	        msg = "<script> parent.uploadResult('SUCCESS'); </script>";
	        tx.commit();
    	} catch (Exception e) {
            tx.rollback();
            log.info("uploadFile Info--->" + e);
            msg = "<script> parent.uploadResult('FAIL'); </script>";
        }
        return write(msg);
    }

    private IResponse deleteUpload(IRequest request) {
    	String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", retflag;

        try {
        	String userid = request.getUser().getId();
        	tx.begin();
        	Map item = null;
	          for (int i = 0; i < ids.length; i++) {
	         	 item = new HashMap();
	         	 
	         	 for(int j=0; j < cols.length; j++ ) {
	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	           	 }
	         	 item.put("pgmId", request.getMap().get("pgmId").toString());
	         	 item.put("docId", request.getMap().get("docId").toString());
	         	 log.error("----------mg.common.deleteUpload--------------------\n" + item.toString());
	         	 deleteObject("mg.common.deleteUpload", item);
	          }
	        tx.commit();
	        msg = "삭제 되었습니다.";
	        retflag = "true";
    	} catch (Exception e) {
            tx.rollback();
            msg= "삭제시 오류가 발생 하였습니다.\n" + e;
            retflag= "false";
        } 
    	return writeXml(responseTranData(msg, retflag, "doSave", "doSaveEnd"));
    }
    
    private IResponse updateUpload(IRequest request) {
    	
        String docId = request.getStringParam("docId");
        String progId = request.getStringParam("pgmId");
        String msg="";
        try {
        	String userid = request.getUser().getId();
	    	tx.begin();      
	        fum.updateUpload(progId, docId, request);
	        tx.commit();
	        msg = "저장되었습니다.";
    	} catch (Exception e) {
            tx.rollback();
            msg= "저장시 오류가 발생 하였습니다.\n" + e;;
        }
        return write(msg);
    }
	
    private IResponse fileDownload(IRequest request) {
    	
        String rootPath = getParameter("component.upload.directory");
        String filePath = getItem("mg.common.getFilePath", request.getMap()).toString();
        // String filePath = request.getStringParam("FILE_PATH");
        String filename = request.getStringParam("FILE_NAME");
        String retfile = rootPath + filePath + "/" + filename;
        String userid = request.getUser().getId();
        log.error("----------------retfile---------------------------\n" + retfile);
        
        return downloadFile(retfile);
    }

}
