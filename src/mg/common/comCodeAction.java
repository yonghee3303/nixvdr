package mg.common;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.base.ComponentRegistry;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.menu.IMenu;
import com.core.component.menu.IMenuManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
// import ssms.base.SeedEncryptUtil; 
	
/**
* 업무 그룹명 : mg.common
* 서브 업무명 : ComCodeAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 17 
* 설 명 : ComCodeAction
*/ 
public class comCodeAction extends BaseAction {
    /**
     * 콤보박스 세팅 
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
                
        if(request.isMode("grid")) {     /** Dhtmlx Grid에서 Combo를 채우기 위함  */
            return getCodeGrid(request);                                                   
        }else if(request.isMode("menu")) {     /** html combo code return  */
            return getMenuInfo(request);                                                     
        }else if(request.isMode("getUserNm")) {     /** 사번으로 성명 찾기  */
            return getUserNm(request);                                                    
        }else if(request.isMode("getCodeList")) {     /** 코드팝업 조회  */
            return getCodeList(request);                                                    
        }else if(request.isMode("ttt")) {     /**  */
            return getTTT(request);                                                    
        }else{   
            return write(null);
        }
    }  
     
    
    /**
     * 코드 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getCodeGrid(IRequest request) throws Exception 
     {
     	 String field = getParameter("dhtmlx.separator.field");
     	 String line = getParameter("dhtmlx.separator.line");
     	      	 
     	 String p_cd = request.getStringParam("P_CD");
     	 String col_id = request.getStringParam("COL_ID");
     	 String default_val = request.getStringParam("default_val");
     	 
     	 String lang;
     	 Locale locale = null;
     	 
     	 if(request.getUser() != null) {
     		 lang = request.getUser().getLocale().getLanguage();
     	 } else {
     		lang = request.getStringParam("locale");
     		
     		if("ko".equals(lang)) {
     			locale = Locale.KOREA;
     		} else if("en".equals(lang)) {
     			locale = Locale.US;
     		} else {
     			locale = Locale.KOREA;
     		}
     	 }
    
         StringBuffer buffer = new StringBuffer();
         
         ICodeManagement sysCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
		 List sCode = sysCode.getCodes(p_cd);
		
		 buffer.append(col_id + field + default_val + line);
		 for (Iterator all = sCode.iterator(); all.hasNext();) {
			ICode icd = (ICode) all.next();
			if("en".equals(lang)) {
				buffer.append(icd.getCd() + field + icd.getNameSl() + line);
			} else {
				buffer.append(icd.getCd() + field + icd.getName() + line);
			}
		 }
        
         return writeXml(buffer.toString());
     } 
    
    /**
     * 메뉴목록조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getMenuInfo(IRequest request) throws Exception 
     {
     	 String field = getParameter("dhtmlx.separator.field");
     	 String line = getParameter("dhtmlx.separator.line");
     	 
     	 String menu_id = request.getStringParam("MENU_ID");
     	 String p_menu_id = request.getStringParam("P_MENU_ID");
         StringBuffer buffer = new StringBuffer();
         
         IMenuManagement mm = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
         IMenu leftmenu = mm.getMenu(p_menu_id);
         
         if(!"".equals(p_menu_id)) {
				for (int i = 0; i < leftmenu.getChildren().size(); i++) {
					IMenu child = (IMenu) leftmenu.getChildren().get(i);
					
					String formId = child.getId();
					if(formId.equals(menu_id)) {
						buffer.append(child.getTarget() + field + child.getPath() + field + child.getName() + field + child.getId());
						
						// buffer.append("'" + child.getTarget() + "','" + child.getPath() + "','" + child.getName()+ "','" + child.getId() + "'");
						break;
					}
				}
         } 
         
         return writeXml(buffer.toString());
     } 
      
      /**
       * 사번으로 성명명조회
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse getUserNm(IRequest request) throws Exception 
       {
     	 String user_nm = getItem("mg.common.getUserNm", request.getMap()).toString();
         return write(user_nm);
       } 
       
       
       private IResponse getTTT(IRequest request) throws Exception 
       {
     	 String retval="abcdefghojklmpoptqstuvwxyzdddddddfff";
     	 
     	 
         return write(retval);
       } 
       
       
       /**
        * POPUP을 위한 공통코드 조회 
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse getCodeList(IRequest request) throws Exception 
        {
          	  String col = request.getStringParam("grid_col_id");
          	  String[] cols = col.split(",");
          	  
          	  Map map = null;
              GridData gdRes = new GridData();
          	
          	  try {
    	         Map smap = request.getMap();
    	         smap.put("LOCALE", request.getUser().getLocale().getLanguage());
    	         
    	         List lists = getList("mg.common.codeList",smap);
    	         for (int i = 0; i < lists.size(); i++) {
    	          	map =(Map) lists.get(i);
    	          	 
    	          	for(int j=0; j < cols.length; j++ ) {
    	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
    	          	}
    	         }
    	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
              } catch(Exception e) {
             	 log.error("---getCodeList-----------------------------------------------------------\n" + e.toString());
             	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
              }
        }
 
}