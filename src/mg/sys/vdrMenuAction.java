package mg.sys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.base.ComponentRegistry;
import com.core.component.menu.IMenuManagement;
import com.core.component.menu.IMenu;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import mg.common.TreeMaker;
import mg.common.TreeVO;


/**
* : mg.basetem
* : codeAction.java
* : cyLeem
* : 2011. 04. 17 
* : menuAction
*/ 
public class vdrMenuAction extends BaseAction {
    IMenuManagement menu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("select")) {   /** */
            return selectMenu(request);                           
        }else if(request.isMode("save")) {   /** */
            return saveMenu(request);                           
        }else if(request.isMode("delete")) {   /** */
            return deleteMenu(request);                           
        }else if(request.isMode("getTree")) {   /** */
            return getTreeMenu(request);                           
        }else { 
            return write(null);
        }
    }  
     
    private List<TreeVO> makelist(List lists){
		// covert lists to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.size(); i++) {
			Map mp = (Map) lists.get(i); 
			JSONObject jTag= new JSONObject();
			jTag.put("TARGET", (String) mp.get("TARGET")); 
			jTag.put("TYPE", (String) mp.get("TYPE"));
			TreeVO tVO = new TreeVO();
			tVO.setKey((String) mp.get("ID"));
			tVO.setParent((String) mp.get("PARENT_ID"));
			tVO.setTitle((String) mp.get("NAME"));
			tVO.setTag(jTag);
			lst.add(tVO);
		}
		return lst;
    }
    
    /*
    private List<TreeVO> makePlist(IMenu lists){
		// covert IMenu to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.getChildren().size(); i++) {
			IMenu mp = (IMenu) lists.getChildren().get(i);

			TreeVO tVO = new TreeVO();
			tVO.setKey( mp.getId());
			tVO.setParent(mp.getParent().getId());
			tVO.setTitle(mp.getName());
			tVO.setTag(mp.getTarget());
			
			 log.info("------selectMenu-=----mp.getName()------ > " +mp.getName());
			 log.info("------selectMenu-=----tVO------ > " + tVO.toString());
			
			lst.add(tVO);
		}
		return lst;
    }
    */
    
    public List<TreeVO> makeByHierarchy(List<?> listview, String p_id) {
		List<TreeVO> rootlist = new ArrayList<TreeVO>();

		for (Integer i = 0; i < listview.size(); i++) {
			TreeVO mtDO = (TreeVO) listview.get(i);
			
			// log.info("------makeByHierarchy-=----mtDO------ > " + p_id + " : " + mtDO.getParent());

			if (mtDO.getParent() == null 	||  p_id.equals(mtDO.getParent()) || (mtDO.getParent() != null && "-1".equals(mtDO.getParent()))) {
				rootlist.add(mtDO);
				continue;
			}
			
			for (Integer j = 0; j < listview.size(); j++) {
				TreeVO ptDO = (TreeVO) listview.get(j);
				if (mtDO.getParent().equals(ptDO.getKey())) {
					if (ptDO.getChildren() == null) {
						ptDO.setChildren(new ArrayList<Object>());
					}
					List<TreeVO> list = ptDO.getChildren();
					list.add(mtDO);
					ptDO.setIsFolder(true);
					break;
				}
			}
		}

		return rootlist;
	}
    
    /**
    *  Get All menu tree
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getTreeMenu(IRequest request) throws Exception 
    {
        JSONObject obj = new JSONObject();
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
        String MenuId = this.getStringNullCheck(request.getStringParam("MENU_ID"));
        String Lang = this.getStringNullCheck(request.getStringParam("LANG"));
        String dbName = pm.getString("component.sql.database");
        
        // �߰��� �κ�
        HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
        //에러가 났을 때
        if("OPTIONS".equals(httpRequest.getMethod())){
           // To avoid nested transaction
         obj.put("errmsg", mm.getMessage("COMG_1002",loc));
         obj.put("errcode", "0");
           return write(obj.toString());
        }
        
        
	    try {
	    	TreeMaker tm = new TreeMaker();  
			List<TreeVO> lst = null;
			List<TreeVO> treeData  = null;
			
			Map map = request.getMap();
			
			System.out.println(map.toString());

	        if(MenuId !=null && !"".equals(MenuId))  	{
	        	
	        	List menuList = getList("mg.base.getTreeVdrMenu_" + dbName, request.getMap()); 
	        	lst = makelist(menuList);
	        	treeData  = makeByHierarchy(lst, MenuId);
	        } 
	       
	        else {
	        	List menuList = getList("mg.base.getTreeAllVdrMenu_" + dbName, request.getMap()); 
	        	lst = makelist(menuList);
	        	treeData  = tm.makeTreeByHierarchy(lst);
	        }

			obj.put("data", treeData);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			
			log.info("------selectMenu-=----obj------ > " + obj.toString());
	
			ObjectMapper mapper = new ObjectMapper();
			String str = "";
			try {
				str = mapper.writeValueAsString(obj); //document.write(obj['key']+"<br>");?
			} catch (IOException ex) {
				log.error("[getTreeMenu] " + ex.getMessage());
			}
			
			log.info("------selectMenu-=----str------ > " + str); 
			
			return write(str);
			
        } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return write("");
        }
    } 
    
    /**
    * 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectMenu(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
      	Map map = null;
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
    	String dbName = pm.getString("component.sql.database");
   
         
         try {
         	
        	List lists;
   	       	 if("mssql".equals(dbName)) {
   	       		 lists = getList("mg.base.selectVdrMenu",request.getMap());
   	       	 } else {
   	       		 if(Boolean.parseBoolean(request.getStringParam("ADMIN"))) lists = getList("mg.base.selectVdrMenuAdmin_" + dbName, request.getMap()); 
   	       		 else lists = getList("mg.base.selectVdrMenu_" + dbName, request.getMap()); 

   	       	 }
         	
         	 JSONArray jArray = new JSONArray();
      	    
	          for (int i = 0; i < lists.size(); i++) {
	          	 map =(Map) lists.get(i);        	
	          	JSONObject listdata = new JSONObject();
	          	listdata.put("ROW_ID", String.valueOf(i+1));
	          	for(int j=0; j < cols.length; j++ ) {
	          	
	          		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	     	
	          	jArray.add(listdata);
	          }
 			 
          	 obj.put("data", jArray);
 			 obj.put("errmsg", "success");
 			 obj.put("errcode", "0");
 			log.info("------selectSample-=----result------ > " + obj.toString());
 			 return write(obj.toString());
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
    } 

    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveMenu(IRequest request) throws Exception 
    {
    	
    	
    	 JSONObject obj = new JSONObject();
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        JSONArray jArray = new JSONArray();
        String dbName = pm.getString("component.sql.database");
        
        try {
        	
        	tx.begin();
        	
        	Map item = null;
        	
	        JSONObject listdata_save;

	        for (int i = 0; i < ids.length; i++) {
	        	
	        	 item = new HashMap();
	        	 
		       	 listdata_save = new JSONObject();
		       	 listdata_save.put("ROW_ID", String.valueOf(i+1));
		       	 
		       	 for(int j=0; j < cols.length; j++ ) {
		       		 
		       		 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		       		 	
		       		 	listdata_save.put(cols[j], tmps );
		       		 	
			            item.put(cols[j], tmps);     
			            
		         }
	       	 
		      	item.put("LOGIN_ID",  request.getUser().getId());
	       
		       	 if("mssql".equals(dbName)) {
			       	if( Integer.parseInt(getItem("mg.base.countVdrMenu", item).toString() ) > 0 ) updateObject("mg.base.updateVdrMenu", item);
			       	else 																		  createObject("mg.base.insertVdrMenu", item);
			      
		       	 }else{
		       	 	if( Integer.parseInt(getItem("mg.base.countVdrMenu", item).toString() ) > 0 ) updateObject("mg.base.updateVdrMenu_"+dbName, item);
		       		else 																		  createObject("mg.base.insertVdrMenu_"+dbName, item);	
		       	 	
			       	for(String lang : languageList) {
			       	 		  
				       	 item.put("MENU_NM", item.get(lang));
				       	 item.put("LANG", lang);
			       	 		
				       	if( Integer.parseInt(getItem("mg.base.countVdrMenuLang", item).toString() ) > 0 ) updateObject("mg.base.updateVdrMenuLang_"+dbName, item);
			       		else 																			  createObject("mg.base.insertVdrMenuLang_"+dbName, item);
			       	}	
		       	 }
	       	  
		       	 jArray.add(listdata_save);
	        }  	
	        
	         tx.commit();
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 return write(obj.toString());
	     } catch(Exception e) {
	         tx.rollback();
	    	 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    }
    
    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteMenu(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	JSONArray jArray = new JSONArray();
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String dbName = pm.getString("component.sql.database");
       
        
        try {
        	
           	tx.begin();
	        Map item = null;
	        JSONObject listdata_delete;
	        
	        for (int i = 0; i < ids.length; i++) {
		       	 	item = new HashMap();
			       	listdata_delete = new JSONObject();
			       	listdata_delete.put("ROW_ID", String.valueOf(i+1));
			       	
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		       	 		listdata_delete.put(cols[j], tmps);     
		       			item.put(cols[j], tmps);    
		         	}
		       	 	
		       	 	deleteObject("mg.base.deleteVdrMenu_"+dbName, item);
		       	 	deleteObject("mg.base.deleteVdrMenuLang_"+dbName, item);
		         	jArray.add(listdata_delete);
	        }
	        
	         tx.commit();
	         
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 return write(obj.toString());
	     } catch(Exception e) {
	    	 tx.rollback();
	    	 obj.put("errmsg", mm.getMessage("COMG_1008", request.getUser().getLocale()));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    }
    
}