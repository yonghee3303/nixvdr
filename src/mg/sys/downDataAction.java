package mg.sys;

import java.io.File;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;


/**
* : mg.basetem
* : codeAction.java
* : cyLeem
* : 2011. 04. 17 
* : 
*/ 

public class downDataAction extends BaseAction {
	
	/**
     * 코드관리
     * @param request
     * @return
     * @throws Exception
     */
	
    @Override
	public IResponse run(IRequest request) throws Exception {

        if(request.isMode("selectData")) {   		 /** 자료 조회  **/
            return selectData(request);                           
        } else if(request.isMode("uploadData")) {     /** 자료 업로드 **/
            return uploadData(request);                           
        } else {    
            return write(null); 
        }
    }    
    /**
    * 자료 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectData(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();

    	JSONArray jArray = new JSONArray();
                        
        try{
          	
        	List<Map> lists = (null != request.getMap().get("TYPE"))? getList("mg.sys.selectDownDataAll" + getDBName(pm.getString("component.sql.database")), request.getMap()) : getList("mg.sys.selectDownData" + getDBName(pm.getString("component.sql.database")), request.getMap());
          	
    		for(Map map : lists) {
    							
				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();
				
				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}
				
				jArray.add(listData);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			return write(obj.toString());
		} catch (Exception e) {
			System.out.println(e);
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
    } 
    /**
     * 자료 업로드
     * @param request
     * @return
     * @throws Exception
     */    
     private IResponse uploadData(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
    	 
    	 Map map = new HashMap();
         
         try {
        	 
        	  tx.begin();
        	   
        	  //자료 업로드
        	  HttpServletRequest httpRequest =  (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
        	  
        	  String downFileLoc = httpRequest.getSession().getServletContext().getRealPath("/downloadFile");
        	  
        	  DiskFileUpload upload = new DiskFileUpload();
              upload.setHeaderEncoding("UTF-8");
              upload.setSizeMax(1024 * 10000);

              List <FileItem> items = upload.parseRequest(httpRequest);
              
              for(FileItem item : items) {      		  
   	        	 if (true == item.isFormField()) {
   	        		map.put(item.getFieldName(), item.getString());
   	        	 }
   	          }
        	  
        	  for(FileItem item : items) {
        		  
  	        	 if (item.isFormField() == false) {
  	        		
  	                String separator = File.separator;
  	                
  	                int index =  item.getName().lastIndexOf(separator);
  	                
  	                String fileName;
  	                
  	                if("UM".equals(map.get("TYPE"))) 	  fileName = "UserManual.pptx";
  	                else if("SB".equals(map.get("TYPE"))) fileName = "SandBox.exe";
  	                else 								  fileName = item.getName().substring(index  + 1);	
  	                
  	                File uploadFile = new File(downFileLoc +  separator + fileName);
  	                
  	                item.write(uploadFile);
  	                
  	        	 }
  	          }
        	  
        	  //자료 기록 저장        	 
	    	  String version = ((null != map.get("PATCH_VERSION").toString())? String.format("%.1f", Double.parseDouble(map.get("PATCH_VERSION").toString())+ 0.1): "0.1");
	    	  map.put("PATCH_VERSION", version);
	    	  
	    	  updateObject("mg.sys.insertDownData" + getDBName(pm.getString("component.sql.database")), map); 
    	 
        	  tx.commit();
			  obj.put("errmsg", "success");
			  obj.put("errcode", "0");
			  return write(obj.toString());

		} catch (Exception e) {
			 System.out.println(e);
			 tx.rollback();
			 obj.put("errmsg", mm.getMessage("COMG_1001"));
			 obj.put("errcode", "-1");
			 return write(obj.toString());
		}
     } 
}



