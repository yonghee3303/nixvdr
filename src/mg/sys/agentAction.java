package mg.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mg.base.BaseAction;
import mg.base.GridData;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class agentAction extends BaseAction {
    /**
     * 에이전트 등록 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("select")) {   /** 조회 */
            return selectAgent(request);                           
        }else if(request.isMode("save")) {   /** 저장 */
            return saveAgent(request);                           
        }else if(request.isMode("delete")) {   /** 삭제        */
            return deleteAgent(request);                           
        }else { 
            return write(null);
        }
    }  
     
    
    /**
    * 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectAgent(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	
        	List lists;
        	String dbName = pm.getString("component.sql.database");
        	
        	if(dbName.equals("mssql")){
        		lists = getList("mg.sys.selectAgent",request.getMap());
        	}else{
        		lists = getList("mg.sys.selectAgent_" + dbName, request.getMap());
        	}
        	
        	
	        
	        
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
                
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 

    
    /**
     * 저장 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveAgent(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        String dbName = pm.getString("component.sql.database");
	        
	        
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("LOGIN_ID",  request.getUser().getId() );
	      	    
	       	 if(dbName.equals("mssql")){
	       	 	
		       	if( Integer.parseInt(getItem("mg.sys.countAgent", item).toString() ) > 0 ) 
		       	{
		       		updateObject("mg.sys.updateAgent", item);
		       	} else 
		       	{
		       		createObject("mg.sys.insertAgent", item);
		       	}
	       	 }else{
	       		if( Integer.parseInt(getItem("mg.sys.countAgent", item).toString() ) > 0 ) 
		       	{
		       		updateObject("mg.sys.updateAgent_"+ dbName, item);
		       	} else 
		       	{
		       		createObject("mg.sys.insertAgent_"+ dbName, item);
		       	} 
	       	 }
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
       	    log.error("---------------------------------------------------------------\n" + e.toString());
            return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    
    
    /**
     * 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteAgent(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.sys.deleteAgent", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
           log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    

}
