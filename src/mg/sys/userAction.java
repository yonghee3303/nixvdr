package mg.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
* 업무 그룹명 : mg.sys
* 서브 업무명 : userAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 17 
* 설 명 : userAction
*/ 
public class userAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("selectUser")) {   /** 조회    */
            return getUserList(request);                           
        } else if(request.isMode("save")) {   /** 저장 */
            return saveUser(request);                           
        }  else if(request.isMode("savePW")) {   /** 비밀번호변경 */
            return savePW(request);     
        }  else if(request.isMode("personalSavePW")) {   /** 개인비밀번호변경 */
            return personalSavePW(request);  
        }  else if(request.isMode("personalCheckPW")) {   /** 개인비밀번호 변경중 기존 비밀번호 확인 */
            return personalCheckPW(request); 
        } else if(request.isMode("usageLogList")) {   /** 사용 로그 조회 */
            return usageLogList(request);                           
        } else if(request.isMode("delete")) {   /** 사용자 삭제 */
            return deleteUser(request);                           
        } else if(request.isMode("excelDownUser")) {   /** 사용자 Barcode List Down */
            return excelDownUser(request);                           
        } else if(request.isMode("excelDownSale")) {   /** 매출구분 Barcode List Down */
            return excelDownSale(request);                           
        }  else if(request.isMode("excelUsageLogList")) {   /** 매출구분 Barcode List Down */
            return excelUsageLogList(request);                           
        }  else if(request.isMode("selectUserApprover")) {   /** 개인 결재선 조회 */
            return getUserApproverList(request);                           
        } else if(request.isMode("saveUserApprover")) {   /** 개인 결재선 저장 */
            return saveUserApprover(request);                           
        } else if(request.isMode("deleteUserApprover")) {   /** 개인 결재선 삭제 */
            return deleteUserApprover(request);                           
        }  
        
        else if(request.isMode("selectUserJson")) {   /** 조회    */
            return getUserListJson(request);                           
        }
        else { 
            return write(null);
        } 
    }  
     
    
    private IResponse deleteUserApprover(IRequest request) {

        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	deleteObject("mg.sys.deleteUserApprover", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------deleteUserApprover----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }
                
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	}


	private IResponse saveUserApprover(IRequest request) {

        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 item.put("dbName",pm.getString("component.sql.database"));
	       	 	if( Integer.parseInt(getItem("mg.sys.countUserApprover", item).toString() ) == 0 ) {
			       		createObject("mg.sys.insertUserApprover", item);
			    }
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUserApprover----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
	}


	private IResponse getUserApproverList(IRequest request) {
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists = getList("mg.sys.selectUserApprover",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-------selectUserApprover--------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
	}


	/**
    * 사용자 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getUserList(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        
        try {
        	// SKT보안조치 - SQL Injection (권한이 없는 사용자가 부서코드 없이 조회 했을 경우 조회 안되도록 조치함)
        	/*
        	if(request.getUser().isNotUserInRole("S1") && request.getUser().isNotUserInRole("S2") ) {
        	   String dept_cd = request.getMap().get("DEPT_CD").toString(); 
        	   if("".equals(dept_cd) || dept_cd == null){
        		  return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
        	   }
        	}
        	*/
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        if(dbName.equals("mssql")){
	        	lists = getList("mg.sys.selectUser",smap);
	        }else{
	        	lists = getList("mg.sys.selectUser_"+dbName,smap);
	        }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
	/**
    * ����� ��ȸ (in json)
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getUserListJson(IRequest request) throws Exception 
    {
     	JSONObject obj = new JSONObject();
     	JSONArray JArray = new JSONArray();
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        
        try {
        	Map smap = request.getMap();
        	List lists;
        	smap.put("dbName", pm.getString("component.sql.database"));
        	if(dbName.equals("mssql")){
	        	lists = getList("mg.sys.selectUser",smap);
	        }else{
	        	lists = getList("mg.sys.selectUser_"+dbName,smap);
	        }
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	JSONObject listdata= new JSONObject();
		       	listdata.put("ROW_ID", String.valueOf(i+1));
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         		listdata.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	         	JArray.add(listdata);
	        }    
	        obj.put("data", JArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			  log.info("------selectUser-=----result------ > " + obj.toString());
			 return write(obj.toString());
	     } catch(Exception e) {
	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    }     

	/*       String msg = mm.getMessage("COMG_1002" , 
//	        		Constants.DEF_LOC
	        		request.getUser().getLocale()
	        		);
	        String resStr = responseSelData(msg
	        		, "true"
	        		, "doQuery"
	        		, gdRes.getGridXmlDatas()); 
	        org.json.JSONObject jsonObject  = XML.toJSONObject(resStr);
			jsonObject.put("errmsg", msg);
			jsonObject.put("errcode", "0");
	        return write(jsonObject.toString());
	        		
        } catch(Exception e) {
	       String msg = mm.getMessage("COMG_1001"
	        		, CpcConstants.EN_LOC
	        		);
       	   log.error("---------------------------------------------------------------\n" + e.toString());
	        org.json.JSONObject jsonObject  = new org.json.JSONObject();
			jsonObject.put("errmsg", msg );
			String exMsg = e.getMessage() != null ? e.getMessage()  : e.toString();
			jsonObject.put("exception", exMsg);
			jsonObject.put("errcode", "-1");
	        return write(jsonObject.toString());
        }
    } */
    
    /**
     * 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveUser(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("LOGIN_ID",  request.getUser().getId());
	       	 	if(pm.getBoolean("component.signon.password.encrypted")) {
	       	 	    item.put("PASSWD",crypto.encode(this.getStringWithNullCheck(item,"PASSWD")));
	       	 	} else {
	       	 		item.put("PASSWD", "");
	       	 	}
	      	    
	       	 	if( !"".equals(item.get("ROLL_GRP_CD")) ) {
	       	 		log.info("------------saveUser-------item-----------\n" + item.toString());
			       	if( Integer.parseInt(getItem("mg.sys.countUser", item).toString() ) > 0 ) {
			       		log.info("기존유저 업데이트");
			       		if(dbName.equals("mssql")){
			       			updateObject("mg.sys.updateUser", item);
			       		}else{
			       			updateObject("mg.sys.updateUser_"+dbName, item);
			       		}
		       			// 권한등록
			       		updateObject("mg.sys.deleteAuth", item);
			       		if(dbName.equals("mssql")){
			       			createObject("mg.sys.insertAuth", item);
			       		}else{
			       			createObject("mg.sys.insertAuth_"+dbName, item);
			       		}
			       	} else {
			       		log.info("새유저 생성");
			       		if(dbName.equals("mssql")){
			       			createObject("mg.sys.insertUser", item); 
			       		}else{
			       			createObject("mg.sys.insertUser_"+dbName, item); 
			       		}
			       			// 권한등록
			       		updateObject("mg.sys.deleteAuth", item);
			       		if(dbName.equals("mssql")){
			       			createObject("mg.sys.insertAuth", item);
			       		}else{
			       			createObject("mg.sys.insertAuth_"+dbName, item);
			       		}
			       	}
	       	 	}
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    /**
     * 비밀번호 변경
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse savePW(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("LOGIN_ID",  request.getUser().getId());

	       	    if(pm.getBoolean("component.signon.password.encrypted")) {
	       	 	    item.put("PASSWD",crypto.encode(this.getStringWithNullCheck(item,"PASSWD")));
	       	 	}
	       	    log.info(item.toString());
	       	    /* else {
	       	 		item.put("PASSWD", "");
	       	 	}*/
	       	 if(dbName.equals("mssql")){
			    updateObject("mg.sys.updatePasswd", item);
	       	 }else{
	       		updateObject("mg.sys.updatePasswd_"+dbName, item);
	       	 }
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------savePW----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    /**
     * 개인비밀번호 변경
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse personalSavePW(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        int preP = 0;
        String dbName = pm.getString("component.sql.database");
        
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("USER_ID",  request.getUser().getId());

	       	    if(pm.getBoolean("component.signon.password.encrypted")) {
	       	 	    item.put("PASSWD",crypto.encode(this.getStringWithNullCheck(item,"PASSWD")));
	       	 	}
	       	    log.info(item.toString());
	       	    
	       		updateObject("mg.sys.updatePersonalPasswd_"+dbName, item);

	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------savePW----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");

        return writeXml(ret);
    }
    
    /**
     * 개인비밀번호 변경중 기존 비밀번호 확인
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse personalCheckPW(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="", flag2="";
        int preP = 0;
        String dbName = pm.getString("component.sql.database");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("USER_ID",  request.getUser().getId());

	       	    if(pm.getBoolean("component.signon.password.encrypted")) {
	       	 	    item.put("PRE_PASS",crypto.encode(this.getStringWithNullCheck(item,"PRE_PASS")));
	       	 	}
	       	    log.info(item.toString());
	       	    
	       		if(Integer.parseInt(getItem("mg.sys.checkPrePWD", item).toString()) == 0 ) {
	       			preP = 1;
	       			flag2 = "noMatch";
	       		}
	        }
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "match";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------savePW----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        String ret2 = responseTranData(msg,flag2, "doSave", "doSaveEnd");
        if(preP == 1) {
        	return writeXml(ret2);
        } else {
        	return writeXml(ret);
        }
    }
    
    /**
     * 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteUser(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		            item.put(cols[j], tmps);     
	         	} 
	       	 	deleteObject("mg.sys.deleteUser", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }
                
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    /**
     * 사용 로그조회
     * @param request
     * @return 
     * @throws Exception
     */
     private IResponse usageLogList(IRequest request) throws Exception 
     {
      	 String col = request.getStringParam("grid_col_id");
      	 String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();
         int i = 0;
         
         
         try {
        	 List lists;
        	 String dbName = pm.getString("component.sql.database");
        	 if("mssql".equals(dbName)) {
        		 lists = getList("mg.sys.usageLogList",request.getMap());
        	 } else {
        		 lists = getList("mg.sys.usageLogList_" + dbName, request.getMap());
        	 }
        	 
	         for (i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	}
	         }  
	         return writeXml(responseSelData( i + "건 조회 성공", "true", "doQuery", gdRes.getGridXmlDatas()));
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseSelData("조회된 데이터가 없습니다.","false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 
         
     /**
      * Excel Download 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelUsageLogList(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
       String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("Usage Log List");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
  	           	       
				 String dbName = pm.getString("component.sql.database");
		       	 List lists= getList("mg.sys.usageLogList"+getDBName(dbName), request.getMap()); 
		       	
	  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "Usage Log List_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
        	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
        }
     }
     /**
      * Excel Download 
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse excelDownUser(IRequest request) throws Exception 
     {
          String dept_nm = request.getStringParam("DEPT_NM");
          String dbName = pm.getString("component.sql.database");     
          int[] col_width = new int[3];
          for(int i=0; i<3; i++) {
        	  col_width[i] = 100 * 40;
          }
  
          try {
 	            IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
 	            IExcelDocument doc = excel.createDocument( dept_nm + " 바코드 리스트");
 	            doc.setColumnsWidth(col_width);
 	            
 	            List lists;
 	           if(dbName.equals("mssql")){
 	        	   lists = getList("mg.sys.selectUser",request.getMap());
 	           }else {
 	        	  lists = getList("mg.sys.selectUser_"+dbName,request.getMap());
 	           }
 	           
	 	        for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	doc.addAlignedCell(this.getStringWithNullCheck(map,"USER_ID"), CellStyleFactory.ALIGN_CENTER );
	 	         	doc.addAlignedCell(this.getStringWithNullCheck(map,"USER_NM"), CellStyleFactory.ALIGN_CENTER );
	 	         	String temp =  "*" + this.getStringWithNullCheck(map,"USER_ID").toString() + "*"; 	     
	 	         	doc.addAlignedCell(temp, CellStyleFactory.ALIGN_CENTER,CellStyleFactory.VERTICAL_CENTER, "Free 3 of 9 Extended");
	 	         	
	 	         }

 	            return doc.download(request, "barcode_" + dept_nm + ".xls");
          } catch(Exception e) {
         	 	log.error("---------------------------------------------------------------\n" + e.toString());
         	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
          }
          
     }
     
     /**
      * Excel Download Salecode 
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse excelDownSale(IRequest request) throws Exception 
     {              
          int[] col_width = new int[3];
          for(int i=0; i<3; i++) {
        	  col_width[i] = 100 * 40;
          }
          col_width[2] = 140 * 40;
  
          try {
 	            IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
 	            IExcelDocument doc = excel.createDocument( "매출구분코드");
 	            doc.setColumnsWidth(col_width);
 	            
 	            List lists = getList("mg.sys.selectBarcodeSale",request.getMap());
 	             	            
	 	        for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	doc.addAlignedCell(this.getStringWithNullCheck(map,"CD_KEY"), CellStyleFactory.ALIGN_CENTER );
	 	         	doc.addAlignedCell(this.getStringWithNullCheck(map,"CD_NM"), CellStyleFactory.ALIGN_CENTER );
	 	         	String temp =  "*" + this.getStringWithNullCheck(map,"CD_KEY").toString() + "*"; 	     
	 	         	doc.addAlignedCell(temp, CellStyleFactory.ALIGN_CENTER,CellStyleFactory.VERTICAL_CENTER, "Free 3 of 9 Extended");
	 	         	
	 	         }

 	            return doc.download(request, "barcode_매출구분코드.xls");
          } catch(Exception e) {
         	 	log.error("---------------------------------------------------------------\n" + e.toString());
         	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
          }
          
     }
}