package mg.sys;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;

import com.core.component.cache.IRefreshManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;


/**
* 업무 그룹명 : mg.basetem
* 서브 업무명 : boardAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 17 
* 설 명 : 게시판 관리 프로그램. 
*/ 
public class boardAction extends BaseAction {
	
	/**
     * 코드관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {

        if(request.isMode("boardList")) {   		 /** 게시판 리스트조회       */
            return boardList(request);                           
        } else if(request.isMode("boardSave")) {     /** 게시판 저장            */
            return boardSave(request);                           
        } else if(request.isMode("boardDelete")) {   /** 게시판 삭제          */
            return boardDelete(request);                           
        }else {    
            return write(null); 
        }
    }    
    /**
    * 게시판 조회 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse boardList(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();

    	JSONArray jArray = new JSONArray();
                        
        try{
          	
        	Map paramMap = request.getMap();
        	List lists; 
        	
        	if(null != paramMap.get("P_CD"))  lists = getList("mg.base.boardListAdmin" + getDBName(pm.getString("component.sql.database")), paramMap);
        	else 							  lists = getList("mg.base.boardList" + getDBName(pm.getString("component.sql.database")), paramMap);
        	
        	
          	
    		for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);
				
				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();
				listData.put("ROW_ID", String.valueOf(i+1));
				
				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}
				
				jArray.add(listData);
			}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			return write(obj.toString());
		} catch (Exception e) {
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
    } 
    /**
     * 저장 
     * @param request
     * @return
     * @throws Exception
     */    
     private IResponse boardSave(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
     	 JSONArray jArray = new JSONArray();
     	 String LANG = (null == request.getStringParam("LANG"))? "KO" : request.getStringParam("LANG");
     	 String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         Map item = request.getMap();
         
         
         try {

        	 JSONObject listData = new JSONObject();
	         for(int j=0; j < cols.length; j++ ) {
	        	String tmps = request.getStringParam(cols[j]);

	        	item.put(cols[j], tmps);     
		        
	        	listData.put(cols[j], tmps);
	         }
	         
	         	 
	         item.put("LOGIN_ID",  request.getUser().getId());
	         item.put("BTITLE", item.get("BTITLE").toString().replaceAll("┗", ""));
	         
	         if(null != item.get("question")) {
	        	 
		    	 String BCONTENT = mm.getMessage("CPMG_1173", LANG) +" : "+request.getUser().getName() +"("+item.get("LOGIN_ID").toString() +") <br><br>" + item.get("BCONTENT").toString();	 
	        	 System.out.println(BCONTENT);
		    	 this.sendMail("", BCONTENT, BCONTENT, 7, LANG);
	     
	         }else {
	        	 if( Integer.parseInt(getItem("mg.base.countBoard", item).toString() ) > 0 ) {
	 	        	if(pm.getString("component.sql.database").equals("mssql")) createObject("mg.base.updateBoard", item);
	 	        	else													   createObject("mg.base.updateBoard_"+pm.getString("component.sql.database"), item);
	 	        } else {
	 	        	item.put("BOARD_NO", item.get("BOARD_CLS_CD").toString() + DateUtils.getCurrentDate("yyyymmddhhmmss") + makeRamdomNumber(10)); 
	 	        	if(pm.getString("component.sql.database").equals("mssql")) createObject("mg.base.insertBoard", item);
	 	        	else													   createObject("mg.base.insertBoard_"+pm.getString("component.sql.database"), item);
	 	        } 
	         }
	       	         	 
	        

			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			return write(obj.toString());

		} catch (Exception e) {
			System.out.println(e);
			obj.put("errmsg", mm.getMessage("COMG_1001"));
			obj.put("errcode", "-1");
			return write(obj.toString());
		}
     } 
      	
     /**
      * 삭제 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse boardDelete(IRequest request) throws Exception 
      {
          String ids_info = request.getStringParam("ids");
          String cols_ids = request.getStringParam("col_ids");
          String[] cols = cols_ids.split(",");
          String[] ids = ids_info.split(",");
          
          try {
	          Map item = null;
	          for (int i = 0; i < ids.length; i++) {
	         	 item = new HashMap();
	         	 
	         	 for(int j=0; j < cols.length; j++ ) {
	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	           	}
	         	deleteObject("mg.base.deleteBoard", item);
	          }
	          return writeXml(responseTranData("삭제 되었습니다.", "true", "doSave", "doSaveEnd"));
          } catch(Exception e) {
         	 log.error("---------------------------------------------------------------\n" + e.toString());
         	 return writeXml(responseTranData("삭제중 오류로 작업 취소 되었습니다.\n관리자에게 문의 바랍니다..","false", "doSave", "doSaveEnd"));
          }
      }
}