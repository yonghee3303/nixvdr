package mg.sys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.base.ComponentRegistry;
import com.core.component.menu.IMenuManagement;
import com.core.component.menu.IMenu;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import mg.common.TreeMaker;
import mg.common.TreeVO;


/**
* : mg.basetem
* : codeAction.java
* : cyLeem
* : 2011. 04. 17 
* : menuAction
*/ 
public class menuAction extends BaseAction {
    IMenuManagement menu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("select")) {   /** */
            return selectMenu(request);     
        }else if(request.isMode("save")) {   /** */
            return saveMenu(request);
        }else if(request.isMode("nimsouSelectMenu")) {   /** */
            return nimsouSelectMenu(request);                           
        }else if(request.isMode("delete")) {   /** */
            return deleteMenu(request);                           
        }else if(request.isMode("getTree")) {   /**  VDR 메뉴 조회*/
            return getTreeMenu(request);   
        }else if(request.isMode("getContextMenu")) {   /** Context Menu 조회*/
            return getContextMenu(request); 
        }else { 
            return write(null);
        }
    }  
     
    // TODO: Move to service layer
    private List<TreeVO> makelist(List lists){
		// covert lists to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.size(); i++) {
			Map mp = (Map) lists.get(i);
			JSONObject jTag= new JSONObject();
			jTag.put("TARGET", (String) mp.get("TARGET"));
			jTag.put("TYPE", (String) mp.get("TYPE"));
			TreeVO tVO = new TreeVO();
			tVO.setKey((String) mp.get("ID"));
			tVO.setParent((String) mp.get("PARENT_ID"));
			tVO.setTitle((String) mp.get("NAME"));
			tVO.setTag(jTag);
			lst.add(tVO);
		}
		return lst;
    }
    
    /*
    private List<TreeVO> makePlist(IMenu lists){
		// covert IMenu to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.getChildren().size(); i++) {
			IMenu mp = (IMenu) lists.getChildren().get(i);

			TreeVO tVO = new TreeVO();
			tVO.setKey( mp.getId());
			tVO.setParent(mp.getParent().getId());
			tVO.setTitle(mp.getName());
			tVO.setTag(mp.getTarget());
			
			 log.info("------selectMenu-=----mp.getName()------ > " +mp.getName());
			 log.info("------selectMenu-=----tVO------ > " + tVO.toString());
			
			lst.add(tVO);
		}
		return lst;
    }
    */
    
    public List<TreeVO> makeByHierarchy(List<?> listview, String p_id) {
		List<TreeVO> rootlist = new ArrayList<TreeVO>();

		for (Integer i = 0; i < listview.size(); i++) {
			TreeVO mtDO = (TreeVO) listview.get(i);
			
			// log.info("------makeByHierarchy-=----mtDO------ > " + p_id + " : " + mtDO.getParent());

			if (mtDO.getParent() == null 	||  p_id.equals(mtDO.getParent()) || (mtDO.getParent() != null && "-1".equals(mtDO.getParent()))) {
				rootlist.add(mtDO);
				continue;
			}
			
			for (Integer j = 0; j < listview.size(); j++) {
				TreeVO ptDO = (TreeVO) listview.get(j);
				if (mtDO.getParent().equals(ptDO.getKey())) {
					if (ptDO.getChildren() == null) {
						ptDO.setChildren(new ArrayList<Object>());
					}
					List<TreeVO> list = ptDO.getChildren();
					list.add(mtDO);
					ptDO.setIsFolder(true);
					break;
				}
			}
		}

		return rootlist;
	}
    
    /**
    *  Get All menu tree
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getTreeMenu(IRequest request) throws Exception 
    {
        JSONObject obj = new JSONObject();
        GridData gdRes = new GridData();
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
        String MenuId = this.getStringNullCheck(request.getStringParam("MENU_ID"));
        String dbName = pm.getString("component.sql.database");
        
	    try {
	    	TreeMaker tm = new TreeMaker();
			List<TreeVO> lst = null;
			List<TreeVO> treeData  = null;

	        if(MenuId !=null && !"".equals(MenuId))  	{
	        	List menuList = getList("mg.base.getTreeVdrMenu_" + dbName, request.getMap()); 	   
	        	log.info("------getSubMenus-=----obj------ > " + menuList.toString());
	        	
	        	lst = makelist(menuList);
	        	treeData  = makeByHierarchy(lst, MenuId);
	        }
	        else {
	        	List menuList = menu.getAllMenus();
	        	log.info("------getAllMenus-=----obj------ > " + menuList.toString());
	        	lst = makelist(menuList);
	        	treeData  = tm.makeTreeByHierarchy(lst);
	        }

			obj.put("data", treeData);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			
			log.info("------selectMenu-=----obj------ > " + obj.toString());
	
			ObjectMapper mapper = new ObjectMapper();
			String str = "";
			try {
				str = mapper.writeValueAsString(obj);
			} catch (IOException ex) {
				log.error("[getTreeMenu] " + ex.getMessage());
			}
			
			log.info("------selectMenu-=----str------ > " + str);
			
			return write(str);
			
        } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return write("");
        }
    } 
    
    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getContextMenu(IRequest request) throws Exception 
     {
    	 JSONObject obj = new JSONObject();
/*      	 String col = request.getStringParam("grid_col_id");
      	 String[] cols = col.split(",");*/
         Map map = request.getMap();
         GridData gdRes = new GridData();
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
         		
         log.info("------getContextMenu-------Locale------ > " + map.toString());
         
         try {
         	
         	 List lists;
 	       	 String dbName = pm.getString("component.sql.database");
 	       	 if("mssql".equals(dbName)) {
 	       		lists = getList("mg.base.selectMenu", map);
 	       	 } else {
 	       		lists = getList("mg.base.selectMenu_" + dbName, map);
 	       	 }

 	       	JSONObject subnode = new JSONObject();
 	         for (int i = 0; i < lists.size(); i++) {
 	         	Map item = (Map) lists.get(i);
 	         	JSONObject subitem = new JSONObject();

/* 	         	for(int j=0; j < cols.length; j++ ) {
 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(item,cols[j]) );
 	         	}*/
 	         	subitem.put("MENU_NM", this.getStringWithNullCheck(item,"MENU_NM"));
 	         	subitem.put("MENU_ORDER", this.getStringWithNullCheck(item,"MENU_ORDER"));
 	         	
 	         	subnode.put(this.getStringWithNullCheck(item,"MENU_URL"), subitem);
 	        }
            
 	        obj.put("data", subnode);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			
			return write(obj.toString());
 	        
         } catch(Exception e) {
        	   log.error("-------getContextMenu-------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001",loc),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 
    
    /**
    * 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectMenu(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = request.getMap();
        GridData gdRes = new GridData();
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
        		
        log.info("------selectMenu-------Locale------ > " + map.toString());
        
        try {
        	
        	List lists;
	       	 String dbName = pm.getString("component.sql.database");
	       	 if("mssql".equals(dbName)) {
	       		lists = getList("mg.base.selectMenu", map);
	       	 } else {
	       		lists = getList("mg.base.selectMenu_" + dbName, map);
	       	 }
	       
	       	
	        for (int i = 0; i < lists.size(); i++) {
	         	Map item = (Map) lists.get(i);
	         	JSONObject data= new JSONObject();
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(item,cols[j]) );
	         	}
	        }
                
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", loc), "true", "doQuery", gdRes.getGridXmlDatas())); 
	        
        } catch(Exception e) {
       	   log.error("-------selectMenu-------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001",loc),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse nimsouSelectMenu(IRequest request) throws Exception 
     {
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = request.getMap();
         GridData gdRes = new GridData();
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
         		
         log.info("------selectMenu-------Locale------ > " + map.toString());
         
         try {
         	
         	List lists;
 	       	lists = getList("mg.base.NimsouSelectMenu", map);

 	        for (int i = 0; i < lists.size(); i++) {
 	         	Map item = (Map) lists.get(i);
 	         	JSONObject data= new JSONObject();
 	         	
 	         	for(int j=0; j < cols.length; j++ ) {
 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(item,cols[j]) );
 	         	}
 	        }

 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", loc), "true", "doQuery", gdRes.getGridXmlDatas())); 
 	        
         } catch(Exception e) {
        	   log.error("-------selectMenu-------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001",loc),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 

    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveMenu(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        String dbName = pm.getString("component.sql.database");
	        
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
		       	 for(int j=0; j < cols.length; j++ ) {
		       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			         item.put(cols[j], tmps);     
	         	}
	       	 	item.put("LOGIN_ID",  request.getUser().getId() );
	      	    
	       	 	
	       	 String cnt = getItem("mg.base.countMenu", item).toString();
	       	 int TCNT = Integer.parseInt(getItem("mg.base.countMenu", item).toString() );
	       	 
	       	 /*
	       	 item.put("MENU_SIZE", 0);
	       	 item.put("MENU_ORDER", Integer.parseInt(item.get("MENU_ORDER").toString()));
	       	 */
	       	 log.info("----saveMenu--------map" + item.toString() );
	       	 	
	       	 if("mssql".equals(dbName)) {
		       	if( TCNT > 0 ) {
		       		updateObject("mg.base.updateMenu", item);
		       	} else {
		       		createObject("mg.base.insertMenu", item);
		       	}
	       	 }else{
	       	 	if( TCNT > 0 ) {
		       		updateObject("mg.base.updateMenu_"+dbName, item);
		       	} else {
		       		createObject("mg.base.insertMenu_"+dbName, item);
		       	}
	       	 }

	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
       	    log.error("---------------------------------------------------------------\n" + e.toString());
            return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    
    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteMenu(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.base.deleteMenu", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
           log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    
}