package mg.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.stringUtil;
import mg.common.CpcConstants;

import com.core.base.ComponentRegistry;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;
import com.core.component.user.IUser;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

public class pgmAction extends BaseAction {
    /**
     * 메뉴 등록 관리
     * @param request
     * @return
     * @throws Exception	
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
    	if(request.isMode("select")) {   /** 조회 */
            return selectPgm(request);                           
        }else if(request.isMode("getPgmJson")) {   /** Json포멧조회 */
            return getPgmJson(request);                           
        }else if(request.isMode("getPgmJsonForTreeGrid")) {   /** Json포멧조회(TreeGrid) */
            return getPgmJsonTreeGrid(request);                           
        }else if(request.isMode("save")) {   /** 저장 */
            return savePgm(request);                           
        }else if(request.isMode("delete")) {   /** 삭제        */
            return deletePgm(request);                           
        }else if(request.isMode("selectJson")) {   /** 조회 */
            return selectPgmJson(request);                                                   
        }else if(request.isMode("saveJson")) {   /** 저장 */
            return savePgmJson(request);                           
        }else if(request.isMode("deleteJson")) {   /** 삭제        */
            return deletePgmJson(request);                           
        }else { 
            return write(null);
        }
    }  
     
    
    /**
    * 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectPgm(IRequest request) throws Exception 
    {
    	 JSONObject obj = new JSONObject();
    	IUser user= request.getUser();
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        JSONArray jArray = new JSONArray();
        
        Locale loc = request.getUser() != null 
        		?request.getUser().getLocale()
				:new Locale(CpcConstants.DEF_LOC);
        try {
        	
        	 List lists;
        	 String dbName = pm.getString("component.sql.database");
        	 if("mssql".equals(dbName)) {       	
        		 lists = getList("mg.sys.selectPgm",request.getMap());
        	 } else {
        		 lists = getList("mg.sys.selectPgm_" + dbName, request.getMap());
        	 }
        	 
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
                
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", loc), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", loc),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    public enum conType {
    	ch,ro,ron,edtxt,edn,coro,dhxCalendar,img;
        public static conType compare(String str) {
               try {
                      return valueOf(str.toLowerCase());
               } catch (Exception ex) {
                      return ro;
               }
        }
    }
    
    public enum propId{
    	key, dummy;
        public static propId compare(String str) {
               try {
                      return valueOf(str.toLowerCase());
               } catch (Exception ex) {
                      return dummy;
               }
        }
    }
    /**
     * json data 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getPgmJson(IRequest request) throws Exception 
     {
         Map map = null;
         GridData gdRes = new GridData();
         JSONObject obj = new JSONObject();
		 String getBgColor= request.getStringParam("getBgColor");
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
         String colType[] = {"ch","ro","ron","edtxt","edn","coro","dhxCalendar","img"};
         
        // try {
         	
        	 Map param = request.getMap();
        	 
        	 
         	 List lists;
         	 String dbName = pm.getString("component.sql.database");
         	 if("mssql".equals(dbName)) {       	
         		 lists = getList("mg.sys.selectPgm", param);
         	 } else {
         		 lists = getList("mg.sys.selectPgm_" + dbName, param);
         	 }
         	 
         	JSONArray jArray = new JSONArray();
         	
 	        for (int i = 0; i < lists.size(); i++) {
 	         	map =(Map) lists.get(i);
 	         	JSONObject data= new JSONObject();
 	         	
 	         	data.put("headerName",  this.getStringWithNullCheck(map, "COL_NM"));
 	         	data.put("field", this.getStringWithNullCheck(map,"COL_ID"));
 	         	data.put("width", this.getStringWithNullCheck(map,"COL_WIDTH"));      	
 	         	data.put("hide", "Y".equals(this.getStringWithNullCheck(map,"COL_HIDDEN_YN")) ? true : false );
 	         	String sTmp = this.getStringWithNullCheck(map,"COL_SORT");
 	         	boolean isSort = ("na".equals(sTmp) || "NULL".equals(sTmp)) ? false : true;
 	         	String cTmp = "";
 	         	if(isSort) 	cTmp = "int".equals(sTmp) ? "numericComparator" : "charComparator";  	
 	         	if(!isSort) data.put("sort", isSort);
// 	         	data.put("comparator", cTmp); // 프론트에서 해당 코드가 제대로 작동하지 않아서 일단 주석 처리
 	         	
 	         	// cell Style
 	         	JSONObject cellStyle= new JSONObject();
 	         	if(getBgColor == null || "true".equals(getBgColor)){
				  cellStyle.put("backgroundColor", this.getStringWithNullCheck(map,"COL_COLOR"));
 	         	}
 	         	cellStyle.put("textAlign", this.getStringWithNullCheck(map,"COL_ALIGN"));
 	         	data.put("cellStyle", cellStyle);
 	         	
 	         	String ctype = this.getStringWithNullCheck(map,"COL_CL_CD");
 	         	switch(conType.compare(ctype))
 	         	{
 	         	case ch:
 	         		data.put("checkboxSelection", "Y".equals(this.getStringWithNullCheck(map,"COL_HIDDEN_YN")) ? false : true );
 	         		data.put("editable", false);
 	         		data.put("maxWidth", this.getStringWithNullCheck(map,"COL_MAX_LEN"));
 	         		break;
 	         	case ro:
 	         		data.put("editable", false);
 	         		break;
 	         	case ron:
 	         		data.put("editable", false);
 	         		break;
 	         	case edtxt:
 	         		data.put("editable", true);
 	         		break;
 	         	case edn:
 	         		data.put("editable", true);
 	         		break;
 	         	case coro:
 	         		data.put("editable", true);
 	         		data.put("cellEditor", "agSelectCellEditor");
 	         		String p_cd = this.getStringWithNullCheck(map,"COL_COMBO_ID");
 	         		JSONArray jCode = new JSONArray();
 	         		
 	         		ICodeManagement  code = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
 	         		
 	         		param.put("CD", map.get("COL_COMBO_ID"));
	 	   	    	
 	         		List sCode = getList("mg.base.getChildrenCode_"+dbName, param);
 	         		
 	         		for(int j = 0; j < sCode.size(); j++) {
 	         			
 	         			JSONObject codedata = new JSONObject();
 	         			
 	         			Map codeMap = (Map) sCode.get(j);
 	         			
 	         			codedata.put("text", (String) codeMap.get("NAME"));
 	         			codedata.put("value", (String) codeMap.get("CD_KEY"));
 	         			
 	         			jCode.add(codedata);
 	         		}
 	         	
		 	   	    data.put("cellEditorParams", jCode);
		 	   	    
 	         		break;
 	         	case dhxCalendar:
 	         		data.put("editable", true);
 	         		data.put("cellEditor", "datePicker");
 	         		break;
 	         	case img:
 	         		data.put("editable", false);
 	         		data.put("cellRenderer",  "imgRenderer");
 	         		break;
 	         	default:
 	         		break;
 	         	}
 	         	jArray.add(data);
 	        } 
 	        
 	       obj.put("data", jArray);
		   obj.put("errmsg", "success");
		   obj.put("errcode", "0");
                 
 	        return write(obj.toString());
        /* } catch(Exception e) {
        	   log.error("---------------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", loc),"false", "doQuery", gdRes.getGridXmlDatas()));
         }*/
     } 
    
    /**
     * json data 조회(TreeGrid)
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse getPgmJsonTreeGrid(IRequest request) throws Exception 
     {
         GridData gdRes = new GridData();
         JSONObject obj = new JSONObject();
         Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
         
         try {
         	
        	 Map param = request.getMap();
         	 List lists;
         	 String dbName = pm.getString("component.sql.database");
         	 
         	 
         	 if("mssql".equals(dbName)) {       	
         		 lists = getList("mg.sys.selectPgm",param);
         	 } else {
         		 lists = getList("mg.sys.selectPgm_" + dbName, param);
         	 }
         	JSONArray jArray = new JSONArray();
         	
 	        for (int i = 0; i < lists.size(); i++) {
 	         	Map map =(Map) lists.get(i);
 	         	JSONObject data= new JSONObject();

 	         	String colId = this.getStringWithNullCheck(map,"COL_ID");
 	         	data.put("prop", colId);
 	         	data.put("minWidth", this.getStringWithNullCheck(map,"COL_WIDTH")+"px");
 	         	switch(propId.compare(colId)) {
 	         	
 	         	case key:
				  data.put("label", "" ); // key should be no title. 
				  data.put("type", "template" );
				  data.put("template", colId);
				  break;
 	         	default:
				  data.put("label",  this.getStringWithNullCheck(map, "ko".equals(loc.toString()) ? "COL_NM" : "COL_ENM") );
				  break;
 	         	}
 	         	jArray.add(data);
 	        } 
 	        
 	       obj.put("data", jArray);
		   obj.put("errmsg", "success");
		   obj.put("errcode", "0");
                 
 	        return write(obj.toString());
         } catch(Exception e) {
        	   log.error("---------------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", loc),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 
    /**
     * 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse savePgm(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String dbName = pm.getString("component.sql.database");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 item = new HashMap();
	       	 
	       	 for(int j=0; j < cols.length; j++ ) {
	       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	item.put("LOGIN_ID",  request.getUser().getId() );
	      	    
	       	 	
	       	 	if(dbName.equals("mssql")){
			       	if( Integer.parseInt(getItem("mg.sys.countPgm", item).toString() ) > 0 ) 
			       	{
			       		updateObject("mg.sys.updatePgm", item);
			       	} else 
			       	{
			       		createObject("mg.sys.insertPgm", item);
			       	}
	       	 	}else{
		       	 	if( Integer.parseInt(getItem("mg.sys.countPgm", item).toString() ) > 0 ) 
			       	{
			       		updateObject("mg.sys.updatePgm_"+dbName, item);
			       	} else 
			       	{
			       		createObject("mg.sys.insertPgm_"+dbName, item);
			       	}
	       	 	}
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
       	    log.error("---------------------------------------------------------------\n" + e.toString());
            return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    
    
    /**
     * 삭제 	
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deletePgm(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        
        try {
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	 	item = new HashMap();
	       	 
	       	 	for(int j=0; j < cols.length; j++ ) {
	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.sys.deletePgm", item);
	        }
	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
        } catch(Exception e) {
           log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
        }
    }
    /**
     * 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectPgmJson(IRequest request) throws Exception 
     {
     	 JSONObject obj = new JSONObject();
     	IUser user= request.getUser();
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
        // GridData gdRes = new GridData();
         JSONArray jArray = new JSONArray();
         
         Locale loc = request.getUser() != null 
         		?request.getUser().getLocale()
 				:new Locale(CpcConstants.DEF_LOC);
         try {
         	
         	 List lists;
         	 String dbName = pm.getString("component.sql.database");
         	 JSONObject listdata_select;
         	 
         	 if("mssql".equals(dbName)) {       	
         		 	lists = getList("mg.sys.selectPgm",request.getMap());
         	 } else {
	         		if(Boolean.parseBoolean(request.getStringParam("ADMIN"))) lists = getList("mg.sys.selectPgmAdmin_" + dbName, request.getMap()); 
	  	       		else lists = getList("mg.sys.selectPgm_" + dbName, request.getMap()); 
         	 }
         	 
 	        for (int i = 0; i < lists.size(); i++) {
 	         	map =(Map) lists.get(i);
 	         	listdata_select = new JSONObject();
 	         	listdata_select.put("ROW_ID", String.valueOf(i+1));
 	         	
 	         	for(int j=0; j < cols.length; j++ ) {
 	         		listdata_select.put(cols[j], this.getStringWithNullCheck(map,cols[j]));
 	         	}
 	         	
 	         	jArray.add(listdata_select);
 	        } 
 	      	 obj.put("data", jArray);
 	    	 obj.put("errmsg", "success");
 	    	 obj.put("errcode", "0");
 			 return write(obj.toString());
 	     } catch(Exception e) {
 	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
 			 obj.put("errcode", "-1");
 	    	 return write(obj.toString());
 	     }
     }
    
    
    /**
     * Json 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse savePgmJson(IRequest request) throws Exception 
    {
    	JSONObject obj = new JSONObject();
    	JSONArray jArray = new JSONArray();
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String dbName = pm.getString("component.sql.database");
        
        try {
        	
        	tx.begin();
        	
	        Map item = null;
	       	
	        JSONObject listdata_save;
	        
	       	for (int i = 0; i < ids.length; i++) {
	       		
	       		item = new HashMap();
	       		
	       		listdata_save = new JSONObject();
	       		listdata_save.put("ROW_ID", String.valueOf(i + 1));
	       		
	       		for(int j = 0; j < cols.length; j++) {
	       			
	       			String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	       			
	       			item.put(cols[j], tmps);
	       			
	       			listdata_save.put(cols[j], tmps);
	       		}
	       		
	       		item.put("LOGIN_ID",  request.getUser().getId());
	       		
	       		if(dbName.equals("mssql")){
	       			if(Integer.parseInt(getItem("mg.sys.countPgm", item).toString()) > 0) updateObject("mg.sys.updatePgm", item);
	       			else																  createObject("mg.sys.insertPgm", item);
	       		}else{
	       			
	       			if(Integer.parseInt(getItem("mg.sys.countPgm", item).toString()) > 0) updateObject("mg.sys.updatePgm_"+dbName, item);
	       			else												  				  createObject("mg.sys.insertPgm_"+dbName, item);
	       			
	       			for(String lang : languageList){
	       				
	       				item.put("COL_NM", item.get(lang));
	       				item.put("LANG", lang);
	       				
		       			if(Integer.parseInt(getItem("mg.sys.countPgmLang", item).toString()) > 0) updateObject("mg.sys.updatePgmLang_"+dbName, item);
		       			else												  				  	  createObject("mg.sys.insertPgmLang_"+dbName, item);	       				
	       			}
	       		}
	       		
	       		jArray.add(listdata_save);
	        }
	       	
	       	tx.commit();
	       	
	       	obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			return write(obj.toString());
			
	     } catch(Exception e) {
	    	 tx.rollback();
	    	 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    }
    
    /**
     * Json 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deletePgmJson(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        JSONObject obj = new JSONObject();
  	    JSONArray jArray = new JSONArray();
  	   String dbName = pm.getString("component.sql.database");
        
        try {
        	
        	tx.begin();
        	
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
		       	 	item = new HashMap();
		       	 	JSONObject listdata_delete = new JSONObject();
		       	 	listdata_delete.put("ROW_ID", String.valueOf(i+1)); 
		       	 	
		       	 	for(int j=0; j < cols.length; j++ ) {
		       	 		
		       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			             item.put(cols[j], tmps);     
			             listdata_delete.put(cols[j], tmps );
			             
		       	 	}
	       	 	
	       	 	deleteObject("mg.sys.deletePgm_"+dbName, item);
	       	 	deleteObject("mg.sys.deletePgmLang_"+dbName, item);	       	 	
	       	 	jArray.add(listdata_delete);
	        }  	
	        
	         tx.commit();
	         obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 return write(obj.toString());
	     } catch(Exception e) {
	    	 tx.rollback();
	    	 obj.put("errmsg", mm.getMessage("COMG_1007", request.getUser().getLocale()));
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
    } //delete

}
