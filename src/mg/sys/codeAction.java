package mg.sys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.base.ComponentRegistry;
import com.core.component.cache.IRefreshManagement;
import com.core.component.code.ICode;
import com.core.component.code.ICodeManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import mg.common.TreeMaker;
import mg.common.TreeVO;

import java.util.ArrayList;

/**
* : mg.basetem
* : codeAction.java
* : cyLeem
* : 2011. 04. 17 
* : ComCodeAction
*/ 
public class codeAction extends BaseAction {
	ICodeManagement code = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	protected IRefreshManagement sync = (IRefreshManagement) getComponent(IRefreshManagement.class);
	
    private static final Log LOG = LogFactory.getLog("code.class");  

    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {

        if(request.isMode("select")) {   /** */
            return selectCode(request);                           
        }else if(request.isMode("getTree")) {   /** */
            return getTreeCode(request);                           
        }else if(request.isMode("save")) {   /** */
            return saveCode(request);                           
        }else if(request.isMode("delete")) {   /** */
            return deleteCode(request);                           
        }else if(request.isMode("selectJson")) {   /** */
            return selectCodeJson(request);                           
        }else if(request.isMode("getTreeJson")) {   /** */
            return getTreeCodeJson(request);                           
        }else if(request.isMode("saveJson")) {   /** */
            return saveCodeJson(request);                           
        }else if(request.isMode("deleteJson")) {   /** */
            return deleteCodeJson(request);                           
        }else { 
            return write(null);
        }
    }  
    
    private List<TreeVO> makelist(List lists){
		// covert lists to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.size(); i++) {
			Map mp = (Map) lists.get(i); 
			TreeVO tVO = new TreeVO();
			tVO.setKey((String) mp.get("ID"));
			tVO.setParent((String) mp.get("PARENT_ID"));
			tVO.setTitle((String) mp.get("NAME"));
			lst.add(tVO);
		}
		return lst;
    }
    
    public List<TreeVO> makeByHierarchy(List<?> listview, String p_id) {
  		List<TreeVO> rootlist = new ArrayList<TreeVO>();

  		for (Integer i = 0; i < listview.size(); i++) {
  			TreeVO mtDO = (TreeVO) listview.get(i);
  			
  			// log.info("------makeByHierarchy-=----mtDO------ > " + p_id + " : " + mtDO.getParent());

  			if (mtDO.getParent() == null 	||  p_id.equals(mtDO.getParent()) || (mtDO.getParent() != null && "-1".equals(mtDO.getParent()))) {
  				rootlist.add(mtDO);
  				continue;
  			}
  			
  			for (Integer j = 0; j < listview.size(); j++) {
  				TreeVO ptDO = (TreeVO) listview.get(j);
  				if (mtDO.getParent().equals(ptDO.getKey())) {
  					if (ptDO.getChildren() == null) {
  						ptDO.setChildren(new ArrayList<Object>());
  					}
  					List<TreeVO> list = ptDO.getChildren();
  					list.add(mtDO);
  					ptDO.setIsFolder(true);
  					break;
  				}
  			}
  		}

  		return rootlist;
  	}

    /**
    * 
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse selectCode(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        Map smap =request.getMap();
        smap.put("dbName", dbName);
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
        try {
	        List lists;
	        
	        if(dbName.equals("mssql")){
	        	lists= getList("mg.base.selectCode",smap);
	        }else{
	        	lists= getList("mg.base.selectCode_"+dbName,smap);
	        }
	        
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }
	        log.info("------selectCode-=----Locale------ > " + gdRes.toString());        
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", loc), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", loc),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    
    /**
   

    /**
    *  Get Code tree
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse getTreeCode(IRequest request) throws Exception 
    {
        JSONObject obj = new JSONObject();
        // GridData gdRes = new GridData();
        Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
        String dbName = pm.getString("component.sql.database");
                
        log.info("------getTreeCode-=----Locale------ > " + loc.toString());        
        String p_cd = request.getStringParam("P_CD");
        String a_cd = request.getStringParam("A_CD");
        String sel = request.getStringParam("SEL");
        
	    try {
	    	ICodeManagement  code = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	    	
	    	System.out.println(request.getMap());
	    	
	    	List sCode = (null != request.getStringParam("LANG"))? getList("mg.base.getChildrenCode_"+dbName,request.getMap()) : code.getCodes(p_cd);
	    	
	    	JSONArray jArray = new JSONArray();

	    	if(!"N".equals(a_cd)) {
	    		JSONObject data= new JSONObject();
	    		if("all".equals(a_cd)) {
	    			data.put("text", "전체");
					data.put("value", "");
	    		} else {
	    			data.put("text", "");
					data.put("value", "");
	    		}
	    		jArray.add(data);
	    	}
	    	
	    	
	  		for(int j = 0; j < sCode.size(); j++) {
      			
	  			JSONObject data= new JSONObject();
      			
      			Map codeMap = (Map) sCode.get(j);
      			
      			System.out.println(codeMap);
      			
      			data.put("text", (String) codeMap.get("NAME"));
      			data.put("value", (String) codeMap.get("CD_KEY"));
      			
      			jArray.add(data);
      		}
	    	
//    		for (Iterator all = sCode.iterator(); all.hasNext();) {
//				ICode icd = (ICode) all.next();
//				JSONObject data= new JSONObject();
//				String tmpCd = icd.getCd();
//				String tmpNm = icd.getName();
//				if(!"ko".equals(loc.toString())) tmpNm = icd.getNameSl();  // 영문명
//				
//				data.put("text", tmpNm);
//				data.put("value", tmpCd);
//				
//				jArray.add(data);
//    		}

			obj.put("data", jArray);
			obj.put("errmsg", "success");
			obj.put("errcode", "0");
			if(!"".equals(sel)) {
				obj.put("selected", sel);
			}
	
			ObjectMapper mapper = new ObjectMapper();
			String str="";
			
			try {
				str = mapper.writeValueAsString(obj);
				
			} catch (IOException ex) {
				log.error("[getTreeCode] " + ex.getMessage());
			}
			log.info("------getTreeCode-=----str------ > " + str); 
			return write(str);
        } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return write("");
        }
    }
    

    /**
     * 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse saveCode(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String dbName = pm.getString("component.sql.database");
         try {
	         Map item = null;
	         for (int i = 0; i < ids.length; i++) {
	        	 item = new HashMap();
	        	 
	        	 for(int j=0; j < cols.length; j++ ) {
	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		             item.put(cols[j], tmps);     
	          	}
	        	item.put("LOGIN_ID",  request.getUser().getId() );
	       	    
	        	
	        	if(dbName.equals("mssql")){
		        	if( Integer.parseInt(getItem("mg.base.countCode", item).toString() ) > 0 ) {
		        		updateObject("mg.base.updateCode", item);
		        	} else {
		        		createObject("mg.base.insertCode", item);
		        	}
	        	}else{
	        		if( Integer.parseInt(getItem("mg.base.countCode", item).toString() ) > 0 ) {
		        		updateObject("mg.base.updateCode_"+dbName, item);
		        	} else {
		        		createObject("mg.base.insertCode_"+dbName, item);
		        	}
	        	}
	         }
	     	log.info("------selectCode-=----str------ > " + item.toString()); 
	         return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
         }
     }
    
     /**
      * 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse deleteCode(IRequest request) throws Exception 
      {
          String ids_info = request.getStringParam("ids");
          String cols_ids = request.getStringParam("col_ids");
          String[] cols = cols_ids.split(",");
          String[] ids = ids_info.split(",");
          
          try {
	          Map item = null;
	          for (int i = 0; i < ids.length; i++) {
	         	 item = new HashMap();
	         	 
	         	 for(int j=0; j < cols.length; j++ ) {
	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	           	}
	         	deleteObject("mg.base.deleteCode", item);
	          }
	     
	          return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
          } catch(Exception e) {
         	 log.error("---------------------------------------------------------------\n" + e.toString());
         	 return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
          }
      }
      
      /** json 조회
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse selectCodeJson(IRequest request) throws Exception 
      {
          JSONObject obj = new JSONObject();
          JSONArray jArray = new JSONArray();
          String dbName = pm.getString("component.sql.database");
          Map smap =request.getMap();
          smap.put("dbName", dbName);
          Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC);
          try {
  	        List lists;
  	        
  	        if(dbName.equals("mssql")){
  	        	 lists= getList("mg.base.selectCode",smap);
  	        }else{
  	        	 if(Boolean.parseBoolean(request.getStringParam("ADMIN"))) lists = getList("mg.base.selectCodeAdmin_" + dbName, smap); 
   	       		 else lists = getList("mg.base.selectCode_" + dbName, smap); 
  	        }
  	        
    		for (int i = 0; i < lists.size(); i++) {

				Map map = (Map) lists.get(i);
				
				Iterator<String> keys = map.keySet().iterator();

				JSONObject listData = new JSONObject();
				listData.put("ROW_ID", String.valueOf(i+1));
				while (keys.hasNext()) {
					String key = keys.next();
					listData.put(key, this.getStringWithNullCheck(map, key));
				}
				
				jArray.add(listData);
			}
    		      
  	    	 obj.put("data", jArray);
  			 obj.put("errmsg", "success");
  			 obj.put("errcode", "0");
  			 return write(obj.toString());
  	     } catch(Exception e) {
  	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
  			 obj.put("errmsg", mm.getMessage("COMG_1001", loc));
  			 obj.put("errcode", "-1");
  	    	 return write(obj.toString());
  	     }
     } 
      
      /**
       *  Get Code tree
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse getTreeCodeJson(IRequest request) throws Exception 
       {
           JSONObject obj = new JSONObject();
           Locale loc = request.getUser() != null ? request.getUser().getLocale(): new Locale(CpcConstants.DEF_LOC); 
                   
           log.info("------getTreeCode-=----Locale------ > " + loc.toString());        
           String p_cd = request.getStringParam("P_CD");
           String a_cd = request.getStringParam("A_CD");
           String sel = request.getStringParam("SEL");
           String dbName = pm.getString("component.sql.database");
         
           try {
   	    	TreeMaker tm = new TreeMaker();  
   			List<TreeVO> lst = null;
   			List<TreeVO> treeData  = null;
   		  Map item = null;
           
   			 if(p_cd !=null && !"".equals(p_cd))  	{
   				List codeList = getList("mg.base.getChildrenCode_"+dbName,request.getMap());
   	        	lst = makelist(codeList);
   	        	treeData  = makeByHierarchy(lst,p_cd);
   	        } 
   	       
   	        else {
   	        	List codeList = getList("mg.base.getTreeAllCode_"+dbName,request.getMap());
   	        	lst = makelist(codeList);
   	        	treeData  = tm.makeTreeByHierarchy(lst);
   	        }
   			

   			obj.put("data", treeData);
   			obj.put("errmsg", "success");
   			obj.put("errcode", "0");
   			
   			log.info("------selectCode-=----obj------ > " + obj.toString());
   	
   			ObjectMapper mapper = new ObjectMapper();
   			String str = "";
   			try {
   				str = mapper.writeValueAsString(obj); //document.write(obj['key']+"<br>");?
   			} catch (IOException ex) {
   				log.error("[getTreeCode] " + ex.getMessage());
   			}
   			
   			log.info("------selectCode-=----str------ > " + str); 
   			
   			return write(str);
   			
           } catch(Exception e) {
           	 log.error("---------------------------------------------------------------\n" + e.toString());
           	 return write("");
           }
       } 

      /**
       * 
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse saveCodeJson(IRequest request) throws Exception 
       {
    	   JSONObject obj = new JSONObject();
    	   JSONArray jArray = new JSONArray();
           String ids_info = request.getStringParam("ids");
           String cols_ids = request.getStringParam("col_ids");
           String[] cols = cols_ids.split(",");
           String[] ids = ids_info.split(",");
           String dbName = pm.getString("component.sql.database");
         
           
           try {
        	   	 tx.begin();
	  	         Map item = null;
	  	         JSONObject listdata;
	  	         
	  	         for (int i = 0; i < ids.length; i++) {
	  	        	 
		  	        	item = new HashMap();
		  	        	listdata = new JSONObject();
		             	listdata.put("ROW_ID", String.valueOf(i+1));
		             	
		  	        	 for(int j=0; j < cols.length; j++ ) {
		  	        		
		  	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		  	        		
		  		             item.put(cols[j], tmps); 
		  		             
		  		             listdata.put(cols[j], tmps);
		  	          	}
		  	        	 
		  	        	item.put("LOGIN_ID",  request.getUser().getId());
	  	        	
		  	        	if(dbName.equals("mssql")){
		  		        	if( Integer.parseInt(getItem("mg.base.countCode", item).toString() ) > 0 ) updateObject("mg.base.updateCode", item);
		  		        	else 																	   createObject("mg.base.insertCode", item);
		  	        	}else{
		  	        		if( Integer.parseInt(getItem("mg.base.countCode", item).toString() ) > 0 )  updateObject("mg.base.updateCode_"+dbName, item);
		  	        		else 																	    createObject("mg.base.insertCode_"+dbName, item);	
			  	        	
		  	        		for(String lang : languageList) {
		  	        			
							       	 item.put("CD_NM", item.get(lang));
							       	 item.put("LANG", lang);							       	 		
							       	 				
							       	if( Integer.parseInt(getItem("mg.base.countCodeLang", item).toString() ) > 0 )  updateObject("mg.base.updateCodeLang_"+dbName, item);
							       	else 																			createObject("mg.base.insertCodeLang_"+dbName, item);
			  	        	}	
		  	        	} 
	  	        	jArray.add(listdata);
	  	         }   
	  	         
	  	         tx.commit();
		    	 obj.put("data", jArray);
				 obj.put("errmsg", "success");
				 obj.put("errcode", "0");
				 return write(obj.toString());
		   
           } catch(Exception e) {	
        	     log.error(e);
        	   	 tx.rollback();
        	     obj.put("errmsg", e.toString());
				 obj.put("errcode", "-1");
		    	 return write(obj.toString());		   
           }
       }
      
       /**
        * 
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse deleteCodeJson(IRequest request) throws Exception 
        {
            String ids_info = request.getStringParam("ids");
            String cols_ids = request.getStringParam("col_ids");
            String[] cols = cols_ids.split(",");
            String[] ids = ids_info.split(",");
            JSONObject obj = new JSONObject();
     	   JSONArray jArray = new JSONArray();
     	  String dbName = pm.getString("component.sql.database");
            
            try {
            
             tx.begin();
            	
  	          Map item = null;
  	          
  	          for (int i = 0; i < ids.length; i++) {
	  	         	 item = new HashMap();
	  	         	 JSONObject listdata = new JSONObject();
	  	         	 listdata.put("ROW_ID", String.valueOf(i+1));
	  	         	
	  	         	 for(int j=0; j < cols.length; j++ ) {
	  	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	  	 	             item.put(cols[j], tmps);     
	  	 	             listdata.put(cols[j], tmps);     
	  	           	}
	  	         	  
	  	         	deleteObject("mg.base.deleteCode_"+dbName, item);
	  	         	deleteObject("mg.base.deleteCodeLang_"+dbName, item);
  	          }
  	     
  	         tx.commit();     
	    	 obj.put("data", jArray);
			 obj.put("errmsg", "success");
			 obj.put("errcode", "0");
			 return write(obj.toString());
	     } catch(Exception e) {
	    	 tx.rollback();
	    	 log.error(e);
	    	 obj.put("errmsg", e.toString());
			 obj.put("errcode", "-1");
	    	 return write(obj.toString());
	     }
      }


      
      private boolean refreshComponent()
      {
    	  boolean ret = true;
    	 //  String[] targets = request.getStringParams("target");
          List destinations = new ArrayList();
          for (int i = 0; i < 1; i++) {
              destinations.add("");
          }
                    
          String interfaceName = "c_interface";
          String moduleId = "moduleId";
          List results = null;
          
          try {
              results = sync.synchronize(interfaceName, moduleId, destinations);
          } catch (Exception e) {
              ret = false;
          }  
       	log.info("------getTreeCode-=----str------ > " + ret); 
          return ret;
      }
      
      
}