package mg.sys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import mg.common.TreeMaker;
import mg.common.TreeVO;

/**
* 업무 그룹명 : mg.basetem
* 서브 업무명 : authAction.java
* 작성자 : cyLeem
* 작성일 : 2011. 04. 17 
* 설 명 : ComCodeAction
*/ 
public class authAction extends BaseAction {
    /**
     * 권한관리 컴포넌트 
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
          //xml
        if(request.isMode("selectAuthMenu")) {   /**  메뉴별 권한 조회 */
            return selectAuthMenu(request);                           
        } else if(request.isMode("saveAuthMenu")) {   /** 메뉴별 권한 저장 */
            return saveAuthMenu(request);                           
        } else if(request.isMode("deleteAuthMenu")) {   /** 메뉴별 권한 삭제 */
            return deleteAuthMenu(request);                           
        } else if(request.isMode("userAuthList")) {   /** 사용자별 권한 리스트 조회 */
            return userAuthList(request);                           
        } else if(request.isMode("saveAuthUser")) {   /** 사용자별 권한 저장 */
            return saveAuthUser(request);                           
        } else if(request.isMode("deleteAuthUser")) {   /** 사용자별 권한 삭제 */
            return deleteAuthUser(request);                           
        }
        
        //json
        else if(request.isMode("selectAuthMenuJson")) {   /**  메뉴별 권한 조회 */
            return selectAuthMenuJson(request);                           
        } else if(request.isMode("saveAuthMenuJson")) {   /** 메뉴별 권한 저장 */
            return saveAuthMenuJson(request);                           
        } else if(request.isMode("deleteAuthMenuJson")) {   /** 메뉴별 권한 삭제 */
            return deleteAuthMenuJson(request);                           
        } else if(request.isMode("userAuthListJson")) {   /** 사용자별 권한 리스트 조회 */
            return userAuthListJson(request);                           
        } else if(request.isMode("saveAuthUserJson")) {   /** 사용자별 권한 저장 */
            return saveAuthUserJson(request);                           
        } else if(request.isMode("deleteAuthUserJson")) {   /** 사용자별 권한 삭제 */
            return deleteAuthUserJson(request);                           
        } else { 
            return write(null);
        }
    }  
    
    private List<TreeVO> makelist(List lists){
		// covert lists to List<TreeVO>
		List<TreeVO> lst = new ArrayList<TreeVO>();
		for (int i = 0; i < lists.size(); i++) {
			Map mp = (Map) lists.get(i); 
			TreeVO tVO = new TreeVO();
			tVO.setKey((String) mp.get("ID"));
			tVO.setParent((String) mp.get("PARENT_ID"));
			tVO.setTitle((String) mp.get("NAME"));
			lst.add(tVO);
		}
		return lst;
    }
    
    public List<TreeVO> makeByHierarchy(List<?> listview, String p_id) {
  		List<TreeVO> rootlist = new ArrayList<TreeVO>();

  		for (Integer i = 0; i < listview.size(); i++) {
  			TreeVO mtDO = (TreeVO) listview.get(i);
  			
  			// log.info("------makeByHierarchy-=----mtDO------ > " + p_id + " : " + mtDO.getParent());

  			if (mtDO.getParent() == null 	||  p_id.equals(mtDO.getParent()) || (mtDO.getParent() != null && "-1".equals(mtDO.getParent()))) {
  				rootlist.add(mtDO);
  				continue;
  			}
  			
  			for (Integer j = 0; j < listview.size(); j++) {
  				TreeVO ptDO = (TreeVO) listview.get(j);
  				if (mtDO.getParent().equals(ptDO.getKey())) {
  					if (ptDO.getChildren() == null) {
  						ptDO.setChildren(new ArrayList<Object>());
  					}
  					List<TreeVO> list = ptDO.getChildren();
  					list.add(mtDO);
  					ptDO.setIsFolder(true);
  					break;
  				}
  			}
  		}

  		return rootlist;
  	}
     
    
    /**
     * 사용자 조회 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectAuthMenu(IRequest request) throws Exception 
     {
      	 String col = request.getStringParam("grid_col_id");
      	 String[] cols = col.split(",");
         Map map = null; 
         GridData gdRes = new GridData();
         
         try {
	         List lists = getList("mg.sys.selectAuthMenu",request.getMap());
	         for (int i = 0; i < lists.size(); i++) {
	          	map =(Map) lists.get(i);
	          	
	          	for(int j=0; j < cols.length; j++ ) {
	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	          	} 
	         }
		     return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 
     
     /**
      * 저장 
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse saveAuthMenu(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String dbName = pm.getString("component.sql.database");
         
         try {
	         Map item = null;
	         for (int i = 0; i < ids.length; i++) {
	        	 item = new HashMap();
	        	 for(int j=0; j < cols.length; j++ ) {
	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	          	}
	        	item.put("LOGIN_ID",  request.getUser().getId() );
	       	    
	        	if( Integer.parseInt(getItem("mg.sys.countAuthMenu", item).toString() ) > 0 ) {
		       		// updateObject("mg.sys.updateAuthMenu", item); 
		       	} else {
		       		if(dbName.equals("mssql")){
		       			createObject("mg.sys.insertAuthMenu", item);
		       		}
		       		else{
		       			createObject("mg.sys.insertAuthMenu_"+dbName, item);
		       		}
		       	}
	         }
	         return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
         } catch(Exception e) {
        	 log.error("---------------------------------------------------------------\n" + e.toString());
        	 return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
         }
     }
     
     /**
      * 삭제 
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse deleteAuthMenu(IRequest request) throws Exception 
      {
          String ids_info = request.getStringParam("ids");
          String cols_ids = request.getStringParam("col_ids");
          String[] cols = cols_ids.split(",");
          String[] ids = ids_info.split(",");
          
          try{
	          Map item = null;
	          for (int i = 0; i < ids.length; i++) {
	        	  item = new HashMap();
	         	 for(int j=0; j < cols.length; j++ ) {
	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	 	             item.put(cols[j], tmps);     
	           	}
	         	deleteObject("mg.sys.deleteAuthMenu", item);
	          }
	          return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
          } catch(Exception e) {
         	 log.error("---------------------------------------------------------------\n" + e.toString());
         	 return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
          }
      }
      
      /**
       * 사용자별 권한 리스트  조회 
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse userAuthList(IRequest request) throws Exception 
       {
           String col = request.getStringParam("grid_col_id");
           String[] cols = col.split(",");
           Map map = null; 
           GridData gdRes = new GridData();
           
           try {
	           List lists = getList("mg.sys.userAuthList",request.getMap());
	           for (int i = 0; i < lists.size(); i++) {
	            	map =(Map) lists.get(i);
	            	
	            	for(int j=0; j < cols.length; j++ ) {
	            		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	            	} 
	           }
	           return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	   log.error("---------------------------------------------------------------\n" + e.toString());
          	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
       } 
       
       /**
        * 사용자별 권한 저장 
        * @param request
        * @return
        * @throws Exception
        */
       private IResponse saveAuthUser(IRequest request) throws Exception 
       {
           String ids_info = request.getStringParam("ids");
           String cols_ids = request.getStringParam("col_ids");
           String[] cols = cols_ids.split(",");
           String[] ids = ids_info.split(",");
           
           try {
	           Map item = null;
	           
	           for (int i = 0; i < ids.length; i++) {
	          	 item = new HashMap();
	          	 for(int j=0; j < cols.length; j++ ) {
	          		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	   	             item.put(cols[j], tmps);     
	             }
	          	 item.put("LOGIN_ID",  request.getUser().getId() );
	          	 
	          	 if( Integer.parseInt(getItem("mg.sys.countAuthUser", item).toString() ) > 0 ) {
	  	       		// updateObject("mg.sys.updateAuthUser", item); 
	  	       	 } else {
	  	       		createObject("mg.sys.insertAuthUser", item);
	  	       	 }
	           }
	          
	           return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
           } catch(Exception e) {
          	 log.error("---------------------------------------------------------------\n" + e.toString());
          	 return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
           }
       }
       
       /*
        * 삭제 
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse deleteAuthUser(IRequest request) throws Exception 
        {
            String ids_info = request.getStringParam("ids");
            String cols_ids = request.getStringParam("col_ids");
            String[] cols = cols_ids.split(",");
            String[] ids = ids_info.split(",");
            
            try {
	            Map item = null;
	            for (int i = 0; i < ids.length; i++) {
	           	 	item = new HashMap();
	           	 JSONObject list = new JSONObject();
	    	      	list.put("ROW_ID", String.valueOf(i+1));
	           	 	for(int j=0; j < cols.length; j++ ) {
	           	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	           	 		item.put(cols[j], tmps);
	             	}
	           	 	deleteObject("mg.sys.deleteAuthUser", item);
	            }
	            return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
            } catch(Exception e) {
           	 log.error("---------------------------------------------------------------\n" + e.toString());
           	 return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
            }
        }
        
        /**
         * 사용자 조회 
         * @param request
         * @return
         * @throws Exception
         */
         private IResponse selectAuthMenuJson(IRequest request) throws Exception 
         {
        	 JSONObject obj = new JSONObject();
        	 JSONArray jArray = new JSONArray();
          	 String col = request.getStringParam("grid_col_id");
          	 String[] cols = col.split(",");
             Map map = null; 
             
             try {
    	         List lists = getList("mg.sys.selectAuthMenu",request.getMap());
    	         for (int i = 0; i < lists.size(); i++) {
    	          	map =(Map) lists.get(i);
    	          	 JSONObject list = new JSONObject();
    	    	      	list.put("ROW_ID", String.valueOf(i+1));
    	          	for(int j=0; j < cols.length; j++ ) {
    	          		list.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
    	          	} 
    	           	jArray.add(list);
   	          }
    			 
             	 obj.put("data", jArray);
    			 obj.put("errmsg", "success");
    			 obj.put("errcode", "0");
    			//  log.info("------selectSample-=----result------ > " + obj.toString());
    			 return write(obj.toString());
    	     } catch(Exception e) {
    	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
    			 obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
    			 obj.put("errcode", "-1");
    	    	 return write(obj.toString());
    	     }
        }
         
         /**
          * 저장 
          * @param request
          * @return
          * @throws Exception
          */
         private IResponse saveAuthMenuJson(IRequest request) throws Exception 
         {
        	 JSONObject obj = new JSONObject();
        	 JSONArray jArray = new JSONArray();
             String ids_info = request.getStringParam("ids");
             String cols_ids = request.getStringParam("col_ids");
             String[] cols = cols_ids.split(",");
             String[] ids = ids_info.split(",");
             String dbName = pm.getString("component.sql.database");
             
             try {
    	         Map item = null;
    	         for (int i = 0; i < ids.length; i++) {
    	        	 item = new HashMap();
    	        	 JSONObject list = new JSONObject();
 	    	      	list.put("ROW_ID", String.valueOf(i+1));
 	    	      	
    	        	 for(int j=0; j < cols.length; j++ ) {
    	        		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
    	 	             item.put(cols[j], tmps);   
    	 	            list.put(cols[j], tmps );
    	          	}
    	        	//item.put("LOGIN_ID",  request.getUser().getId() );
    	        	 item.put("LOGIN_ID",  "admin" );
    	       	    
    	        	if( Integer.parseInt(getItem("mg.sys.countAuthMenu", item).toString() ) > 0 ) {
    		       		// updateObject("mg.sys.updateAuthMenu", item); 
    	        		updateObject("mg.sys.updateAuthMenu"+ getDBName(dbName), item);
    		       	} else {
    		           createObject("mg.sys.insertAuthMenu"+ getDBName(dbName), item);
    		       	}
    	           	jArray.add(list);
      	          }
       			 
                	 obj.put("data", jArray);
       			 obj.put("errmsg", "success");
       			 obj.put("errcode", "0");
       			 log.info("------saveAuthMenuJson-=----result------ > " + obj.toString());
       			 return write(obj.toString());
       	     } catch(Exception e) {
       	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       			 obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
       			 obj.put("errcode", "-1");
       	    	 return write(obj.toString());
       	     }
         }
         
         /**
          * 삭제 
          * @param request
          * @return
          * @throws Exception
          */
          private IResponse deleteAuthMenuJson(IRequest request) throws Exception 
          {
        	  JSONObject obj = new JSONObject();
         	 JSONArray jArray = new JSONArray();
              String ids_info = request.getStringParam("ids");
              String cols_ids = request.getStringParam("col_ids");
              String[] cols = cols_ids.split(",");
              String[] ids = ids_info.split(",");
              
              try{
    	          Map item = null;
    	          for (int i = 0; i < ids.length; i++) {
    	         	 item = new HashMap();
    	         	JSONObject list = new JSONObject();
	    	      	list.put("ROW_ID", String.valueOf(i+1));
    	         	 for(int j=0; j < cols.length; j++ ) {
    	         		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
    	 	             item.put(cols[j], tmps);     
    	 	             list.put(cols[i],tmps);
    	           	}
    	         	deleteObject("mg.sys.deleteAuthMenu", item);
    	           	jArray.add(list);
       	          }
        			 
                 	 obj.put("data", jArray);
        			 obj.put("errmsg", "success");
        			 obj.put("errcode", "0");
        			//  log.info("------selectSample-=----result------ > " + obj.toString());
        			 return write(obj.toString());
        	     } catch(Exception e) {
        	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
        			 obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
        			 obj.put("errcode", "-1");
        	    	 return write(obj.toString());
        	     }
          }
          
          /**
           * 사용자별 권한 리스트  조회 
           * @param request
           * @return
           * @throws Exception
           */
           private IResponse userAuthListJson(IRequest request) throws Exception 
           {
        	   JSONObject obj = new JSONObject();
          	 JSONArray jArray = new JSONArray();
               String col = request.getStringParam("grid_col_id");
               String[] cols = col.split(",");
               Map map = null; 
               
               try {
    	           List lists = getList("mg.sys.userAuthList",request.getMap());
    	           for (int i = 0; i < lists.size(); i++) {
    	            	map =(Map) lists.get(i);
    	            	JSONObject list = new JSONObject();
    	    	      	list.put("ROW_ID", String.valueOf(i+1));
    	            	for(int j=0; j < cols.length; j++ ) {
    	            		list.put(cols[j], this.getStringWithNullCheck(map,cols[j]) );
    	            	} 
    	               	jArray.add(list);
    	   	          }
    	    			 
    	             	 obj.put("data", jArray);
    	    			 obj.put("errmsg", "success");
    	    			 obj.put("errcode", "0");
    	    			//  log.info("------selectSample-=----result------ > " + obj.toString());
    	    			 return write(obj.toString());
    	    	     } catch(Exception e) {
    	    	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
    	    			 obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
    	    			 obj.put("errcode", "-1");
    	    	    	 return write(obj.toString());
    	    	     }
           } 
           
           /**
            * 사용자별 권한 저장 
            * @param request
            * @return
            * @throws Exception
            */
           private IResponse saveAuthUserJson(IRequest request) throws Exception 
           {
        	   JSONObject obj = new JSONObject();
            	 JSONArray jArray = new JSONArray();
               String ids_info = request.getStringParam("ids");
               String cols_ids = request.getStringParam("col_ids");
               String[] cols = cols_ids.split(",");
               String[] ids = ids_info.split(",");
               
               try {
    	           Map item = null;
    	           String dbName = pm.getString("component.sql.database");
    	           for (int i = 0; i < ids.length; i++) {
    	          	 item = new HashMap();
    	          	JSONObject list = new JSONObject();
	    	      	list.put("ROW_ID", String.valueOf(i+1));
    	          	 for(int j=0; j < cols.length; j++ ) {
    	          		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
    	   	             item.put(cols[j], tmps); 
    	   	             list.put(cols[i], tmps);
    	             }
    	          	// item.put("LOGIN_ID",  request.getUser().getId() );
    	          	 item.put("LOGIN_ID",  "admin" );
    	         	    
    	          	 if( Integer.parseInt(getItem("mg.sys.countAuthUser", item).toString() ) > 0 ) {
				       	updateObject("mg.sys.updateAuthUser"+getDBName(dbName), item);
    	  	       	 } else {
    	  	       	if(dbName.equals("mssql")){
		       			createObject("mg.sys.insertAuthUser", item);
		       		}
		       		else{
		       			createObject("mg.sys.insertAuthUser_"+dbName, item);
		       		}
    	  	       	 }
    	          	jArray.add(list);
 	   	          }
 	    			 
 	             	 obj.put("data", jArray);
 	    			 obj.put("errmsg", "success");
 	    			 obj.put("errcode", "0");
 	    			 log.info("------saveAuthUserJson-=----result------ > " + obj.toString());
 	    			 return write(obj.toString());
 	    	     } catch(Exception e) {
 	    	    	 log.error("---저장된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
 	    			 obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
 	    			 obj.put("errcode", "-1");
 	    	    	 return write(obj.toString());
 	    	     }
           }
           
           /*
            * 삭제 
            * @param request
            * @return
            * @throws Exception
            */
            private IResponse deleteAuthUserJson(IRequest request) throws Exception 
            {
            	 JSONObject obj = new JSONObject();
            	 JSONArray jArray = new JSONArray();
                String ids_info = request.getStringParam("ids");
                String cols_ids = request.getStringParam("col_ids");
                String[] cols = cols_ids.split(",");
                String[] ids = ids_info.split(",");
                
                try {
    	            Map item = null;
    	            for (int i = 0; i < ids.length; i++) {
    	           	 	item = new HashMap();
    	           	 JSONObject list = new JSONObject();
 	    	      	list.put("ROW_ID", String.valueOf(i+1));
    	           	 	for(int j=0; j < cols.length; j++ ) {
    	           	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
    	           	 		item.put(cols[j], tmps);
    	           	 		list.put(cols[j], tmps);
    	             	}
    	           	 	deleteObject("mg.sys.deleteAuthUser", item);
    	           	 	jArray.add(list);
  	   	          }
  	    			 
  	             	 obj.put("data", jArray);
  	    			 obj.put("errmsg", "success");
  	    			 obj.put("errcode", "0");
  	    			//  log.info("------selectSample-=----result------ > " + obj.toString());
  	    			 return write(obj.toString());
  	    	     } catch(Exception e) {
  	    	    	 log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
  	    			 obj.put("errmsg", mm.getMessage("COMG_1001", request.getUser().getLocale()));
  	    			 obj.put("errcode", "-1");
  	    	    	 return write(obj.toString());
  	    	     }
            }
            
           
            
        
}