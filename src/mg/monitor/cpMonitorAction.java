package mg.monitor;

import java.util.List;
import java.util.Map;

import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.BaseAction;
import mg.base.GridData;

public class cpMonitorAction extends BaseAction {	

	/**
     * CP 모니터 관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("usageCpServer")) {   /** CP 서버 모니터링 */
            return usageCpServer(request);                           
        } else  if(request.isMode("excelusageCpServer")) {   /** Agent 설치 현황 ExcelDownload */
            return excelusageCpServer(request);                           
        } else   if(request.isMode("workCpAgent")) {   /** Agent 작업 현황 모니터링 */
            return workCpAgent(request);                           
        } else  if(request.isMode("excelworkCpAgent")) {   /** Agent 작업 현황 ExcelDownload */
            return excelworkCpAgent(request);                           
        } else { 
            return write(null);
        }
    }
    
    /**
     * CP Server 모니터링 
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse usageCpServer(IRequest request) throws Exception 
     {
	   	  String col = request.getStringParam("grid_col_id");
	   	  log.info("Agent 운영 현황 그리드 컬럼 : " + col);
	   	  String[] cols = col.split(",");
	   	  
	   	  Map map = null;
	      GridData gdRes = new GridData();
	
	   	  try {
	 	         Map smap = request.getMap();
	 	         List lists = getList("mg.monitor.usageCpServer",smap);
	 	         
	 	         log.info("--------------------------------\n" + lists.toString());
	 	         
	 	         for (int i = 0; i < lists.size(); i++) {
	 	          	map =(Map) lists.get(i);
	 	          	 
	 	          	for(int j=0; j < cols.length; j++ ) {
	 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	 	          	}
	 	         }
	 	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
	       } catch(Exception e) {
	      	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
	      	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
	       }
    }
     
     /**
      * CP 서버 모니터링 ExcelDownload
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse excelusageCpServer(IRequest request) throws Exception 
      {
    	  String col = request.getStringParam("grid_col_id");
         String[] cols = col.split(",");
    	  String width = request.getStringParam("grid_col_width");
    	  String[] widths = width.split(",");
    	  String header = request.getStringParam("grid_col_header");
    	  String[] headers = header.split(",");

    	  try {
    		     // Excel Header 설정
    		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
	             IExcelDocument doc = excel.createDocument("CP 서버 가동 현황");
	       		 int[] col_width = new int[col.length()];
	             for(int i=0; i< cols.length; i++) {
	            	 doc.addTitleCell(headers[i]);
	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
	             }
	             doc.setColumnsWidth(col_width);
	     
  	         Map smap = request.getMap();
	         List lists = getList("mg.monitor.usageCpServer",smap);

  	         for (int i=0; i < lists.size(); i++) {
	 	         	Map map =(Map) lists.get(i);
	 	         	doc.nextRow();
	 	         	
	 	         	for(int j=0; j < cols.length; j++) {
	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
	 	         	}
	 	         }
  	         return doc.download(request, "CP 서버 가동 현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
        } catch(Exception e) {
       	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
       	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. 관리자에게 문의 바랍니다.");
        }
     }
      
      /**
       * Agent 작업 현황 모니터링
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse workCpAgent(IRequest request) throws Exception 
       {
  	   	  String col = request.getStringParam("grid_col_id");
  	   	  String[] cols = col.split(",");
  	   	log.info("Agent 작업 현황 그리드 컬럼 : " + col);
  	   	  
  	   	  Map map = null;
  	      GridData gdRes = new GridData();
  	
  	   	  try {

	 	       	String dbName = pm.getString("component.sql.database");
	 	       	List lists = getList("mg.monitor.workCpAgent"+getDBName(dbName),request.getMap());
 	 	         
  	 	         log.info("--------------------------------\n" + lists.toString());
  	 	         
  	 	         for (int i = 0; i < lists.size(); i++) {
  	 	          	map =(Map) lists.get(i);
  	 	          	 
  	 	          	for(int j=0; j < cols.length; j++ ) {
  	 	          		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	 	          	}
  	 	         }
  	 	         return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
  	       } catch(Exception e) {
  	      	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
  	      	 	return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
  	       }
      }
       
       /**
        * CP 서버 모니터링 ExcelDownload
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse excelworkCpAgent(IRequest request) throws Exception 
        {
      	  String col = request.getStringParam("grid_col_id");
           String[] cols = col.split(",");
      	  String width = request.getStringParam("grid_col_width");
      	  String[] widths = width.split(",");
      	  String header = request.getStringParam("grid_col_header");
      	  String[] headers = header.split(",");

      	  try {
      		     // Excel Header 설정
      		  	 IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
  	             IExcelDocument doc = excel.createDocument("CP서버 작업진행 현황");
  	       		 int[] col_width = new int[col.length()];
  	             for(int i=0; i< cols.length; i++) {
  	            	 doc.addTitleCell(headers[i]);
  	            	 col_width[i] = Integer.parseInt(widths[i]) * 40;
  	             }
  	             doc.setColumnsWidth(col_width);
  	     
    	         Map smap = request.getMap();
  	             List lists = getList("mg.monitor.excelworkCpAgent",smap);

    	         for (int i=0; i < lists.size(); i++) {
  	 	         	Map map =(Map) lists.get(i);
  	 	         	doc.nextRow();
  	 	         	
  	 	         	for(int j=0; j < cols.length; j++) {
  	 	         		doc.addAlignedCell(this.getStringWithNullCheck(map,cols[j]), CellStyleFactory.ALIGN_LEFT);
  	 	         	}
  	 	         }
    	         return doc.download(request, "CP서버 작업진행 현황_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
          } catch(Exception e) {
         	 	log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
         	    return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. 관리자에게 문의 바랍니다.");
          }
       }
    
}
