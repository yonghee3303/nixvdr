package mg.ni;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class manAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("manSelectUser")) {   /** 인력 조회    */
            return manSelectUser(request);                           
        } else if(request.isMode("saveNewMan")) {	/**인력 신규 저장	*/
        	return saveNewMan(request);
        } else if(request.isMode("saveNewManMi")) {	/**인력 신규 저장 - 마이그레이션	*/
        	return saveNewManMi(request);
        } else if(request.isMode("searchManId")) {	/**이름주고 ID 리턴	*/
        	return searchManId(request);
        } else if(request.isMode("saveMan")) {   /** 인력 저장 */
            return saveMan(request);                                                   
        } else if(request.isMode("deleteMan")) {   /** 인력 삭제 */
            return deleteMan(request);                                                     
        } else if(request.isMode("recentSituSelectUser")) {   /** 인재 근황 조회  */
            return recentSituSelectUser(request);                                                     
        } else if(request.isMode("saveNewRecentSitu")) {   /** 인재 근황 신규 저장 */
            return saveNewRecentSitu(request);                                                     
        } else if(request.isMode("saveRecentSitu")) {   /** 인재 근황 저장 */
            return saveRecentSitu(request);                                                     
        } else if(request.isMode("deleteRecentSitu")) {   /** 인재 근황 삭제 */
            return deleteRecentSitu(request);                                                     
        } else if(request.isMode("manPop")) {   /** 인재 팝업 */
            return manPop(request);                                                     
        }
        else { 
        	JSONObject retobj = new JSONObject();
      	     retobj.put("errcode", "-1");
		     retobj.put("errmsg", "not found mod value.");
            return write(null);
        } 
    }  

	/**
    * 인력 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse manSelectUser(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
        	String strTech = request.getStringParam("TECH_CL_CD");
          	if(strTech!="") {
          		String[] strT = strTech.split(",");
	          	//System.out.println(strS);
	          	          	
	          	smap.put("dbName", pm.getString("component.sql.database"));
	          	if(strT.length > 0) {
	          	smap.put("TECH_CL_CD", strT);
	          	}
          	}
	        List lists;
	        lists = getList("mg.ni.manSelectUser_"+dbName,smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    
    public String getRanIdNum() {
    	int randomNum = (int)(Math.floor((Math.random() * 89999)+10000));
    	
		String ranNum = Integer.toString(randomNum);
		
		return ranNum;
	}

	public String getTodayNum() {
		DecimalFormat df = new DecimalFormat("00");
		  Calendar currentCal = Calendar.getInstance();
		  
		  currentCal.add(currentCal.DATE, 0);
		  
		  String year = Integer.toString(currentCal.get(Calendar.YEAR));
		  String month = df.format(currentCal.get(Calendar.MONTH)+1);
		  String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));
		
		
		String todayNum = year + month + day;
		
		return todayNum;
	}
    
    
    /**
     * 인력 신규 저장 - 마이그레이션
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveNewManMi(IRequest request) throws Exception 
    {
    	String msg="", flag="";
	    String dbName = pm.getString("component.sql.database");
	    JSONObject obj = new JSONObject();
	    Map itemm = null;
	    itemm = new HashMap();
	    int checkOver = 0;
	    String checkRanResult = null;
	    
	    while(checkOver == 0) {
	    	String ranIdNum		= getRanIdNum();
	    	String todayNum		= getTodayNum();
	    	String ranId			= todayNum + ranIdNum;
	    	String mRanId			= "M" + ranId;
	    	log.info(mRanId+" 랜덤ID");
	    	checkRanResult = mRanId.substring(0, 14); 
	    	log.info(checkRanResult+" 자른랜덤ID");
		    itemm.put("MAN_IDC", checkRanResult);
		    log.info("ID중복체크 아이템 put");
		   	if( Integer.parseInt(getItem("mg.ni.manIdOverCountUser", itemm).toString() ) > 0 ) {
		   		checkOver = 0;
		   		log.info("중복된ID==========================");
		   	} else {
		   		checkOver = 1;
		   	}
	   	}
	    
	    String ranResult = checkRanResult;
	   	
    try {
    	tx.begin();
        Map item = null;
        
    item = new HashMap();

   	String man_id = request.getStringParam("MAN_ID");
   	String agency_id = request.getStringParam("AGENCY_ID");
   	String indus_cl_cd = request.getStringParam("INDUS_CL_CD");
   	String man_nm = request.getStringParam("MAN_NM");
   	String cur_ym = request.getStringParam("CUR_YM");
   	String addr = request.getStringParam("ADDR");
   	String tel_no = request.getStringParam("TEL_NO");
   	String email = request.getStringParam("EMAIL");
   	String edu_cl_cd = request.getStringParam("EDU_CL_CD");
   	String licen_ym = request.getStringParam("LICEN_YM");
   	String qual_cl_cd = request.getStringParam("QUAL_CL_CD");
   	String major_desc = request.getStringParam("MAJOR_DESC");
   	String tech_cl_cd = request.getStringParam("TECH_CL_CD");
   	String skill_etc = request.getStringParam("SKILL_ETC");
   	String cont_cl_cd = request.getStringParam("CONT_CL_CD");
   	String reg_no = request.getStringParam("REG_NO");

   	item.put("MAN_ID", ranResult);
   	item.put("AGENCY_ID", agency_id);
   	item.put("INDUS_CL_CD", indus_cl_cd);
   	item.put("MAN_NM", man_nm);
   	item.put("CUR_YM", cur_ym);
   	item.put("ADDR", addr);
   	item.put("TEL_NO", tel_no);
   	item.put("EMAIL", email);
   	item.put("EDU_CL_CD", edu_cl_cd);
   	item.put("LICEN_YM", licen_ym);
   	item.put("QUAL_CL_CD", qual_cl_cd);
   	item.put("MAJOR_DESC", major_desc);
   	item.put("TECH_CL_CD", tech_cl_cd);
   	item.put("SKILL_ETC", skill_etc);
   	item.put("CONT_CL_CD", cont_cl_cd);
   	item.put("REG_NO", reg_no);
    
   	item.put("LOGIN_ID",  request.getUser().getId());
   	
	log.info("새유저 생성");
	createObject("mg.ni.manInsertUser_"+dbName, item); 

	String strTech = tech_cl_cd;
	String strManId = ranResult;
  	if(strTech!="") {
  		String[] strT = strTech.split(",");
      	//System.out.println(strS);
  		log.info("strT Split");
  		
  		Map item2 = new HashMap();
  		item2.put("dbName", pm.getString("component.sql.database"));
      	if(strT.length > 0) {
      	/*item2.put("TECH_CL_CDA", strT);*/
      	item2.put("MAN_IDA", ranResult);
      	log.info("item2에 strT 넣음 : "+strManId);
      	}
      	
  		int seq = 0;
  		
	  		for(int k = 0; k < strT.length; k++) {
	  			seq += 1;
	  			item2.put("TECH_CL_CDA", strT[k]);
	  			item2.put("SEQ", seq);
	      		log.info("item2에 seq, Tech 넣음 : "+seq+strT[k]);
	      		createObject("mg.ni.manInsertTech_"+dbName, item2);
	      		log.info("보유기술 sql");
	  			}
	  	}
		  	tx.commit();
		    obj.put("ID", man_nm);   
		    obj.put("errcode", "0");
		    obj.put("errmsg", "success");
		    return write(obj.toString());
		} catch (Exception e) {
			tx.rollback();
			log.info(e);
			log.info("ID를 찾을수 없습니다");
			obj.put("ID", " "); 
			obj.put("errcode", "-100");
		    obj.put("errmsg", "ID를 찾을수 없습니다");
			return write(obj.toString());
		}
	}
	    
    
    /**
     * 이름주고 ID 리턴
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse searchManId(IRequest request) throws Exception 
    {
    String msg="", flag="";
    String dbName = pm.getString("component.sql.database");
    JSONObject obj = new JSONObject();
    	try {
		    	tx.begin();
		        Map item = null;
		        
			    item = new HashMap();
			
			   	String man_nm = request.getStringParam("MAN_NM");
			   	
			   	item.put("MAN_NM", man_nm);
			    
				String getMan_id = getItem("mg.ni.searchManId", item).toString();
				
			    tx.commit();
			    obj.put("ID", getMan_id);   
			    obj.put("errcode", "0");
			    obj.put("errmsg", "success");
			    return write(obj.toString());
			} catch (Exception e) {
				tx.rollback();
				obj.put("ID", " "); 
				obj.put("errcode", "-100");
			    obj.put("errmsg", "ID를 찾을수 없습니다");
				return write(obj.toString());
			}
		}
    
    
    
    /**
     * 인력 신규 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveNewMan(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);
		        }
		       	
	       	item.put("LOGIN_ID",  request.getUser().getId());
			log.info("새유저 생성");
			createObject("mg.ni.manInsertUser_"+dbName, item); 
			
			String strTech = request.getStringParam("TECH_CL_CD");
			String strManId = request.getStringParam("MAN_ID");
          	if(strTech!="") {
          		String[] strT = strTech.split(",");
	          	//System.out.println(strS);
          		log.info("strT Split");
          		
          		Map item2 = new HashMap();
          		item2.put("dbName", pm.getString("component.sql.database"));
	          	if(strT.length > 0) {
	          	/*item2.put("TECH_CL_CDA", strT);*/
	          	item2.put("MAN_IDA", strManId);
	          	log.info("item2에 strT 넣음 : "+strManId);
	          	}
	          	
          		int seq = 0;
          		
          		for(int k = 0; k < strT.length; k++) {
          			seq += 1;
          			item2.put("TECH_CL_CDA", strT[k]);
          			item2.put("SEQ", seq);
              		log.info("item2에 seq, Tech 넣음 : "+seq+strT[k]);
              		createObject("mg.ni.manInsertTech_"+dbName, item2);
              		log.info("보유기술 sql");
          		}
          	}
			
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    

   
    /**
     * 인력 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveMan(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);     
		        }
	       	item.put("LOGIN_ID",  request.getUser().getId());
	       	log.info("기존유저 업데이트");
			updateObject("mg.ni.manUpdateUser_"+dbName, item);
			deleteObject("mg.ni.manDelTech", item);
			
			String strTech = request.getStringParam("TECH_CL_CD");
			String strManId = request.getStringParam("MAN_ID");

          	if(strTech!="") {
          		String[] strT = strTech.split(",");
	          	//System.out.println(strS);
          		log.info("strT Split");
          		
          		Map item2 = new HashMap();
          		item2.put("dbName", pm.getString("component.sql.database"));
	          	if(strT.length > 0) {
	          	/*item2.put("TECH_CL_CDA", strT);*/
	          	item2.put("MAN_IDA", strManId);
	          	log.info("item2에 strT 넣음 : "+strManId);
	          	}
	          	
          		int seq = 0;
          		
          		for(int k = 0; k < strT.length; k++) {
          			seq += 1;
          			item2.put("TECH_CL_CDA", strT[k]);
          			item2.put("SEQ", seq);
              		log.info("item2에 seq, Tech 넣음 : "+seq+strT[k]);
              		createObject("mg.ni.manInsertTech_"+dbName, item2);
              		log.info("보유기술 sql");
          		}
          	}
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }

    /**
     * 인력 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteMan(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
	       		for(int j=0; j < cols.length; j++ ) {
	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		        item.put(cols[j], tmps);     
	         	}
	       	 	deleteObject("mg.ni.manDeleteUser", item); 
	       	 	deleteObject("mg.ni.manDelTech", item);
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }     
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
  
    
    
    
    
    
    
    
	/**
     * 인재 근황 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse recentSituSelectUser(IRequest request) throws Exception 
     {
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();
         String dbName = pm.getString("component.sql.database");
         
         try {
         	Map smap = request.getMap();
         	smap.put("dbName", pm.getString("component.sql.database"));
 	        List lists;
 	        lists = getList("mg.ni.recentSituSelectUser_"+dbName,smap);
 	        for (int i = 0; i < lists.size(); i++) {
 	         	map =(Map) lists.get(i);
 	         	for(int j=0; j < cols.length; j++ ) {
 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	         	}
 	        }   
 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	   log.error("---------------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 
     
     /**
      * 인재 근황 신규 저장
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse saveNewRecentSitu(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
         String dbName = pm.getString("component.sql.database");
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	item = new HashMap();
 		       	for(int j=0; j < cols.length; j++ ) {
 		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 			    item.put(cols[j], tmps);     
 		        }
 	       	item.put("LOGIN_ID",  request.getUser().getId());
 			log.info("새유저 생성");
 			createObject("mg.ni.recentSituInsertUser_"+dbName, item); 
 	        }
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
 	        flag = "false";
         }
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }
     
    
     /**
      * 인재 근황 저장
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse saveRecentSitu(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
         String dbName = pm.getString("component.sql.database");
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	item = new HashMap();
 		       	for(int j=0; j < cols.length; j++ ) {
 		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 			    item.put(cols[j], tmps);     
 		        }
 	       	item.put("LOGIN_ID",  request.getUser().getId());
 	       	log.info("기존유저 업데이트");
 			updateObject("mg.ni.recentSituUpdateUser_"+dbName, item);
 	        }
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
 	        flag = "false";
         }
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }

     /**
      * 인재 근황 삭제
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse deleteRecentSitu(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	item = new HashMap();
 	       		for(int j=0; j < cols.length; j++ ) {
 	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		        item.put(cols[j], tmps);     
 	         	}
 	       	 	deleteObject("mg.ni.recentSituDeleteUser", item); 
 	        }
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 	        flag = "false";
         }     
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }
     
     
     private IResponse manPop(IRequest request) throws Exception 
     {
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();
         String dbName = pm.getString("component.sql.database");
         try {
         	Map smap = request.getMap();
         	smap.put("dbName", pm.getString("component.sql.database"));
 	        List lists;
 	        lists = getList("mg.ni.manSelectPop",smap);
 	        for (int i = 0; i < lists.size(); i++) {
 	        map =(Map) lists.get(i);
 	         	for(int j=0; j < cols.length; j++ ) {
 	         	gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	         	}
 	        } 
 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	   log.error("---------------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     } 
    
    
    
}