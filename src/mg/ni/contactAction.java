package mg.ni;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.logging.log4j.*;
import org.apache.commons.*;
import org.apache.commons.io.output.*;
import org.apache.xmlbeans.XmlException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class contactAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);

	@Override
	public IResponse run(IRequest request) throws Exception {

		if (request.isMode("contractSelectUser")) { /** 조회 */
			return contractSelectUser(request);
		} else if (request.isMode("contractSave")) { /** 저장 */
			return contractSave(request);
		} else if (request.isMode("contractSaveMi")) { /** 저장 - 마이그레이션 */
			return contractSaveMi(request);
		} else if (request.isMode("contractDelete")) { /** 삭제 */
			return contractDelete(request);
		} else if (request.isMode("selectUnitPrice")) { /** 계약세부내용 조회 */
			return selectUnitPrice(request);
		} else if (request.isMode("saveUnitPriceMi")) { /** 계약세부내용 수정 마이그레이션 */
			return saveUnitPriceMi(request);
		} else if (request.isMode("saveUnitPrice")) { /** 계약세부내용 수정 */
			return saveUnitPrice(request);
		} else if (request.isMode("deleteUnitPrice")) { /** 계약세부내용 삭제 */
			return deleteUnitPrice(request);
		} else if (request.isMode("contractSelect")) { /** 계약 조회 */
			return contractSelect(request);
		} else if (request.isMode("contractSelectMon")) { /** 계약 조회(월) */
			return contractSelectMon(request);
		} else if (request.isMode("contractSelectProj")) { /** 계약 조회(프로젝트) */
			return contractSelectProj(request);
		} else if (request.isMode("excelContractMonthSelectList")) { /** 계약 조회(월) 엑셀 다운 */
			return excelContractMonthSelectList(request);
		} else if (request.isMode("excelContractSelectList")) { /** 계약 계약관리 엑셀 다운 */
			return excelContractSelectList(request);
		} else if (request.isMode("unitPaySelect")) { /** 인력급여지급 조회 */
			return unitPaySelect(request);
		} else if (request.isMode("excelUnitPaySelect")) { /** 인력급여지급 엑셀 다운 */
			return excelUnitPaySelect(request);
		} else {
			return write(null);
		}
	}

	/**
	 * 사용자 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractSelectUser(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			List lists;
			lists = getList("mg.ni.contractSelectUser_" + dbName, smap);
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true",
					"doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false",
					"doQuery", gdRes.getGridXmlDatas()));
		}
	}
   
    
	public String getRanIdNum() {
		int randomNum = (int) (Math.floor((Math.random() * 89999) + 10000));

		String ranNum = Integer.toString(randomNum);

		return ranNum;
	}

	public String getTodayNum() {
		DecimalFormat df = new DecimalFormat("00");
		Calendar currentCal = Calendar.getInstance();

		currentCal.add(currentCal.DATE, 0);

		String year = Integer.toString(currentCal.get(Calendar.YEAR));
		String month = df.format(currentCal.get(Calendar.MONTH) + 1);
		String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));

		String todayNum = year + month + day;

		return todayNum;
	}
    
    
	/**
	 * 저장 마이그레이션
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractSaveMi(IRequest request) throws Exception {
		String ranIdNum = getRanIdNum();
		String todayNum = getTodayNum();
		String ranId = todayNum + ranIdNum;
		String mRanId = "T" + ranId;
		log.info(mRanId);
		String ranResult = mRanId.substring(0, 14);

		String msg = "", flag = "";
		String dbName = pm.getString("component.sql.database");
		JSONObject obj = new JSONObject();
		try {
			tx.begin();
			Map item = null;

			item = new HashMap();

			String cont_no = request.getStringParam("CONT_NO");
			String man_id = request.getStringParam("MAN_ID");
			String start_ymd = request.getStringParam("START_YMD");
			String end_ymd = request.getStringParam("END_YMD");
			String cont_cl_cd = request.getStringParam("CONT_CL_CD");
			String cont_amt = request.getStringParam("CONT_AMT");
			String pay_cl_cd = request.getStringParam("PAY_CL_CD");
			String pay_day = request.getStringParam("PAY_DAY");
			String cont_etc = request.getStringParam("CONT_ETC");
			String proj_no = request.getStringParam("PROJ_ID");

			item.put("CONT_RNO", ranResult);
			item.put("MAN_ID", man_id);
			item.put("START_YMD", start_ymd);
			item.put("END_YMD", end_ymd);
			item.put("CONT_CL_CD", cont_cl_cd);
			item.put("CONT_AMT", cont_amt);
			item.put("PAY_CL_CD", pay_cl_cd);
			item.put("PAY_DAY", pay_day);
			item.put("CONT_ETC", cont_etc);
			item.put("PROJ_ID", proj_no);

			item.put("LOGIN_ID", request.getUser().getId());
			log.info("생성");
			createObject("mg.ni.contractInsert_" + dbName, item);

			tx.commit();
			obj.put("ID", man_id);
			obj.put("errcode", "0");
			obj.put("errmsg", "success");
			return write(obj.toString());
		} catch (Exception e) {
			tx.rollback();
			log.info("생성실패");
			log.info(e);
			obj.put("ID", " ");
			obj.put("errcode", "-100");
			obj.put("errmsg", "데이터 업로드 실패");
			return write(obj.toString());
		}
	}
    
	/**
	 * 저장
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractSave(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		String msg = "", flag = "";
		String dbName = pm.getString("component.sql.database");
		log.info("접속");
		try {
			tx.begin();
			Map item = null;

			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();

				for (int j = 0; j < cols.length; j++) {
					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				if (Integer.parseInt(getItem("mg.ni.contractCountUser", item).toString()) > 0) {
					item.put("LOGIN_ID", request.getUser().getId());
					log.info("업데이트");
					updateObject("mg.ni.contractSave_" + dbName, item);
					if (Integer.parseInt(getItem("mg.ni.unitPriceCountUser", item).toString()) == 0) {
						log.info("계약세부에 추가");
						createObject("mg.ni.contractInsertUnitPrice_" + dbName, item);
					}
					log.info("인재풀 데이터도 업데이트");
					updateObject("mg.ni.contractSaveToMan_" + dbName, item);

				} else {
					String cont_rno = request.getStringParam("CONT_RNO");
					item.put("CONT_RNO", cont_rno);
					item.put("LOGIN_ID", request.getUser().getId());
					log.info("생성");
					createObject("mg.ni.contractInsert_" + dbName, item);
					log.info("계약세부에 추가");
					createObject("mg.ni.insertUnitPrice_" + dbName, item);
					log.info("인재풀 데이터 업데이트");
					updateObject("mg.ni.contractSaveToMan_" + dbName, item);
				}
			}
			tx.commit();
			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
			flag = "true";
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveUser----------------------------------------------------------------\n"
					+ e.toString());
			msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
			flag = "false";
		}
		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
		return writeXml(ret);
	}

	/**
	 * 삭제
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractDelete(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		String msg = "", flag = "";
		try {
			tx.begin();
			Map item = null;
			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();
				for (int j = 0; j < cols.length; j++) {
					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				deleteObject("mg.ni.contractDelete", item);
				updateObject("mg.ni.contractDelToMan", item);

			}
			tx.commit();
			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
			flag = "true";
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveUser----------------------------------------------------------------\n"
					+ e.toString());
			msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
			flag = "false";
		}
		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
		return writeXml(ret);
	}
    
    
	/**
	 * 계약세부내용 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse selectUnitPrice(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			List lists;
			lists = getList("mg.ni.selectUnitPrice_" + dbName, smap);
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true",
					"doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false",
					"doQuery", gdRes.getGridXmlDatas()));
		}
	}
     
	/**
	 * 계약세부내용 저장 마이그레이션
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse saveUnitPriceMi(IRequest request) throws Exception {
		String msg = "", flag = "";
		String dbName = pm.getString("component.sql.database");
		JSONObject obj = new JSONObject();
		try {
			tx.begin();
			Map item = null;

			item = new HashMap();

			String man_id = request.getStringParam("MAN_ID");
			String pay_prc = request.getStringParam("PAY_PRC");
			String insur_prc = request.getStringParam("INSUR_PRC");
			String retire_prc = request.getStringParam("RETIRE_PRC");
			String m_exp_prc = request.getStringParam("M_EXP_PRC");
			String trip_prc = request.getStringParam("TRIP_PRC");
			String y_incen_prc = request.getStringParam("Y_INCEN_PRC");
			String y_exp_prc = request.getStringParam("Y_EXP_PRC");
			String proj_id = request.getStringParam("PROJ_ID");

			item.put("MAN_ID", man_id);
			item.put("PAY_PRC", pay_prc);
			item.put("INSUR_PRC", insur_prc);
			item.put("RETIRE_PRC", retire_prc);
			item.put("M_EXP_PRC", m_exp_prc);
			item.put("TRIP_PRC", trip_prc);
			item.put("Y_INCEN_PRC", y_incen_prc);
			item.put("Y_EXP_PRC", y_exp_prc);
			item.put("PROJ_ID", proj_id);

			item.put("LOGIN_ID", request.getUser().getId());

			createObject("mg.ni.insertUnitPrice_" + dbName, item);

			tx.commit();
			obj.put("ID", man_id);
			obj.put("errcode", "0");
			obj.put("errmsg", "success");
			return write(obj.toString());
		} catch (Exception e) {
			log.info("업로드 실패");
			log.info(e);
			obj.put("ID", " ");
			obj.put("errcode", "-100");
			obj.put("errmsg", "업로드 실패");
			return write(obj.toString());
		}
	}
     
	/**
	 * 계약세부내용 저장
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse saveUnitPrice(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		String msg = "", flag = "";
		String dbName = pm.getString("component.sql.database");
		try {
			tx.begin();
			Map item = null;

			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();

				for (int j = 0; j < cols.length; j++) {
					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				if (Integer.parseInt(getItem("mg.ni.unitPriceCountUser", item).toString()) > 0) {
					item.put("LOGIN_ID", request.getUser().getId());
					log.info("업데이트");
					updateObject("mg.ni.saveUnitPrice_" + dbName, item);
					updateObject("mg.ni.unitPriceToManSave_" + dbName, item);
					// 맨파워 업데이트, 원가관리 업데이트 ==== 원가관리 카운트 유저
				} else {
					String cont_rno = request.getStringParam("CONT_RNO");
					item.put("CONT_RNO", cont_rno);
					item.put("LOGIN_ID", request.getUser().getId());
					log.info("생성");
					createObject("mg.ni.insertUnitPrice_" + dbName, item);
					updateObject("mg.ni.unitPriceToManSave_" + dbName, item);
					// 맨파워 업데이트, 원가관리 생성
				}
			}
			tx.commit();
			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
			flag = "true";
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveUser----------------------------------------------------------------\n"
					+ e.toString());
			msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
			flag = "false";
		}
		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
		return writeXml(ret);
	}

	/**
	 * 계약세부내용 삭제
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse deleteUnitPrice(IRequest request) throws Exception {
		String ids_info = request.getStringParam("ids");
		String cols_ids = request.getStringParam("col_ids");
		String[] cols = cols_ids.split(",");
		String[] ids = ids_info.split(",");
		String msg = "", flag = "";
		try {
			tx.begin();
			Map item = null;
			for (int i = 0; i < ids.length; i++) {
				item = new HashMap();
				for (int j = 0; j < cols.length; j++) {
					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
					item.put(cols[j], tmps);
				}
				deleteObject("mg.ni.deleteUnitPrice", item);

			}
			tx.commit();
			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
			flag = "true";
		} catch (Exception e) {
			tx.rollback();
			log.error("------------saveUser----------------------------------------------------------------\n"
					+ e.toString());
			msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
			flag = "false";
		}
		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
		return writeXml(ret);
	}

	/**
	 * 계약 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractSelect(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			List lists;
			lists = getList("mg.ni.contractSelect_" + dbName, smap);
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true",
					"doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false",
					"doQuery", gdRes.getGridXmlDatas()));
		}
	}
      
	/**
	 * 계약 조회(월)
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractSelectMon(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));

			List lists;
			lists = getList("mg.ni.contractSelectMon_" + dbName, smap);

			String cont_no = request.getStringParam("CONT_NO");

			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true",
					"doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false",
					"doQuery", gdRes.getGridXmlDatas()));
		}
	}
       
	/**
	 * 계약 조회(프로젝트)
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse contractSelectProj(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));

			List lists;
			lists = getList("mg.ni.contractSelectProj_" + dbName, smap);

			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true",
					"doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false",
					"doQuery", gdRes.getGridXmlDatas()));
		}
	}
       
       
	/**
	 * Excel Download
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse excelContractSelectListAA(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		String width = request.getStringParam("grid_col_width");
		String[] widths = width.split(",");
		String header = request.getStringParam("grid_col_header");
		String[] headers = header.split(",");

		try {
			// Excel Header 설정
			IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
			IExcelDocument doc = excel.createDocument("계약조회 리스트");
			int[] col_width = new int[col.length()];

			doc.addTitleCell("구분");
			col_width[0] = Integer.parseInt("44") * 40;
			doc.addTitleCell("순번");
			col_width[1] = Integer.parseInt("51") * 40;
			doc.addTitleCell("성명");
			col_width[2] = Integer.parseInt("119") * 40;
			doc.addTitleCell("직위");
			col_width[3] = Integer.parseInt("45") * 40;
			doc.addTitleCell("고객사");
			col_width[4] = Integer.parseInt("272") * 40;
			doc.addTitleCell("투입일");
			col_width[5] = Integer.parseInt("159") * 40;
			doc.addTitleCell("대상금액");
			col_width[6] = Integer.parseInt("120") * 40;
			doc.addTitleCell("공제액(4대보험,소득세,주민세)");
			col_width[7] = Integer.parseInt("252") * 40;
			doc.addTitleCell("공제액(3.3%)");
			col_width[8] = Integer.parseInt("111") * 40;
			doc.addTitleCell("지급액(회사VAT 포함)" + "(10일)");
			col_width[9] = Integer.parseInt("220") * 40;
			doc.addTitleCell("지급액(회사VAT 포함)" + "(21일)");
			col_width[10] = Integer.parseInt("220") * 40;
			doc.addTitleCell("지급액(회사VAT 포함)" + "(31일)");
			col_width[11] = Integer.parseInt("220") * 40;
			doc.addTitleCell("연말정산");
			col_width[12] = Integer.parseInt("149") * 40;
			doc.addTitleCell("근로자 휴가지원");
			col_width[13] = Integer.parseInt("234") * 40;
			doc.addTitleCell("건강보험 정산");
			col_width[14] = Integer.parseInt("129") * 40;
			doc.addTitleCell("요양보험 정산");
			col_width[15] = Integer.parseInt("129") * 40;
			doc.addTitleCell("고용보험 정산");
			col_width[16] = Integer.parseInt("129") * 40;
			doc.addTitleCell("건강보험 연말정산");
			col_width[17] = Integer.parseInt("106") * 40;
			doc.addTitleCell("합계");
			col_width[18] = Integer.parseInt("156") * 40;
			doc.addTitleCell("대상금액 - 합계");
			col_width[19] = Integer.parseInt("122") * 40;
			doc.addTitleCell("미지급");
			col_width[20] = Integer.parseInt("324") * 40;
			doc.addTitleCell("지급일");
			col_width[21] = Integer.parseInt("122") * 40;
			doc.addTitleCell("상태");
			col_width[22] = Integer.parseInt("79") * 40;
			doc.addTitleCell("비고");
			col_width[23] = Integer.parseInt("151") * 40;

			doc.setColumnsWidth(col_width);

			Map smap = request.getMap();
			List lists = getList("mg.ni.contractSelectMonExcelDownAA", smap);
			int total_EXP_PRC = 0;
			int EXP_PRCI = 0;

			for (int i = 0; i < lists.size(); i++) {
				Map map = (Map) lists.get(i);
				int num = i + 1;
				doc.nextRow();
				doc.addAlignedCell(this.getStringWithNullCheck(map, "A"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(num, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "D"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "COMP_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "YMD"), CellStyleFactory.ALIGN_CENTER);
				String EXP_PRC = this.getStringWithNullCheck(map, "EXP_PRC");
				EXP_PRC = EXP_PRC.substring(0, EXP_PRC.length() - 5);
				EXP_PRCI = Integer.parseInt(EXP_PRC);
				doc.addNumericCell(EXP_PRCI);
				total_EXP_PRC += Integer.parseInt(EXP_PRC);
			}
			int listSize = lists.size() + 3;
			doc.nextRow();
			doc.addSpan(1, 6);
			doc.addAlignedCell(("합계"), CellStyleFactory.ALIGN_CENTER);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addNumericCell(total_EXP_PRC);
			// doc.addAlignedCell(("=SUM(G4:G"+listSize+")"), CellStyleFactory.ALIGN_RIGHT);

			return doc.download(request, "계약조회 리스트_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
		}
	}
        
        
	/**
	 * Excel Download
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse excelContractMonthSelectList(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		String width = request.getStringParam("grid_col_width");
		String[] widths = width.split(",");
		String header = request.getStringParam("grid_col_header");
		String[] headers = header.split(",");

		try {
			// Excel Header 설정
			IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
			IExcelDocument doc = excel.createDocument("계약조회 리스트");
			int[] col_width = new int[col.length()];

			doc.addTitleCell("순번");
			col_width[0] = Integer.parseInt("51") * 40;
			doc.addTitleCell("성명");
			col_width[1] = Integer.parseInt("110") * 40;
			doc.addTitleCell("직위");
			col_width[2] = Integer.parseInt("45") * 40;
			doc.addTitleCell("구분");
			col_width[3] = Integer.parseInt("45") * 40;
			doc.addTitleCell("프로젝트명");
			col_width[4] = Integer.parseInt("270") * 40;
			doc.addTitleCell("수행사");
			col_width[5] = Integer.parseInt("270") * 40;
			doc.addTitleCell("담당자");
			col_width[6] = Integer.parseInt("110") * 40;
			doc.addTitleCell("소싱");
			col_width[7] = Integer.parseInt("110") * 40;
			doc.addTitleCell("투입일");
			col_width[8] = Integer.parseInt("159") * 40;
			doc.addTitleCell("종료일");
			col_width[9] = Integer.parseInt("159") * 40;
			doc.addTitleCell("계약금액");
			col_width[10] = Integer.parseInt("140") * 40;
			doc.addTitleCell("급여");
			col_width[11] = Integer.parseInt("140") * 40;
			doc.addTitleCell("4대보험");
			col_width[12] = Integer.parseInt("140") * 40;
			doc.addTitleCell("퇴직금");
			col_width[13] = Integer.parseInt("140") * 40;
			doc.addTitleCell("비용 (월)");
			col_width[14] = Integer.parseInt("140") * 40;
			doc.addTitleCell("출장비");
			col_width[15] = Integer.parseInt("140") * 40;
			doc.addTitleCell("인센티브 (월)");
			col_width[16] = Integer.parseInt("140") * 40;
			doc.addTitleCell("비용 (년)");
			col_width[17] = Integer.parseInt("140") * 40;
			doc.addTitleCell("지급일");
			col_width[18] = Integer.parseInt("100") * 40;
			doc.addTitleCell("월 이익");
			col_width[19] = Integer.parseInt("140") * 40;
//			doc.addTitleCell("월 매출");
//			col_width[19] = Integer.parseInt("140") * 40;
			doc.addTitleCell("월 비용");
			col_width[20] = Integer.parseInt("140") * 40;
			
			

			doc.setColumnsWidth(col_width);

			Map smap = request.getMap();
			List lists = getList("mg.ni.contractSelectMonExcelDownA", smap);
			int total_CONT_AMT = 0;
			int CONT_AMTI = 0;
			int total_PAY_PRC = 0;
			int PAY_PRCI = 0;
			int PAY_PRC_SI = 0;
			int total_INSUR_PRC = 0;
			int INSUR_PRCI = 0;
			int total_RETIRE_PRC = 0;
			int RETIRE_PRCI = 0;
			int total_M_EXP_PRC = 0;
			int M_EXP_PRCI = 0;
			int total_TRIP_PRC = 0;
			int TRIP_PRCI = 0;
			int total_Y_INCEN_PRC = 0;
			int Y_INCEN_PRCI = 0;
			int total_Y_EXP_PRC = 0;
			int Y_EXP_PRCI = 0;
			int total_BENEFIT = 0;
			int total_EXP_PRC = 0;

			for (int i = 0; i < lists.size(); i++) {
				Map map = (Map) lists.get(i);
				int num = i + 1;
				doc.nextRow();
				doc.addAlignedCell(num, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "D"), CellStyleFactory.ALIGN_LEFT);
				String CONT_CL_CD = this.getStringWithNullCheck(map, "CONT_CL_CD");
				if (CONT_CL_CD.equals("1")) {
					CONT_CL_CD = "자사";
				} else if (CONT_CL_CD.equals("2")) {
					CONT_CL_CD = "프리";
				} else if (CONT_CL_CD.equals("3")) {
					CONT_CL_CD = "외주";
				}
				doc.addAlignedCell(CONT_CL_CD, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "PROJ_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "COMP_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "USER_ID"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "RECOMMENDER"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "YMD"), CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "ENDYMD"), CellStyleFactory.ALIGN_CENTER);

				String CONT_AMT = this.getStringWithNullCheck(map, "CONT_AMT");
				// CONT_AMT = CONT_AMT.substring(0, CONT_AMT.length()-5);
				CONT_AMTI = Integer.parseInt(CONT_AMT);
				doc.addNumericCell(CONT_AMTI);
				total_CONT_AMT += Integer.parseInt(CONT_AMT);

				String PAY_PRC = this.getStringWithNullCheck(map, "PAY_PRC");
				String PAY_PRC_S = this.getStringWithNullCheck(map, "PAY_PRC_S");
				// PAY_PRC = PAY_PRC.substring(0, PAY_PRC.length()-5);
				PAY_PRCI = Integer.parseInt(PAY_PRC);
				PAY_PRC_SI = Integer.parseInt(PAY_PRC_S);
				
				if(PAY_PRC_SI == 0) {
				doc.addNumericCell(PAY_PRCI);
				} else {
					doc.addNumericCell(PAY_PRC_SI);
				}
				
				if(PAY_PRC_SI == 0) {
				total_PAY_PRC += PAY_PRCI;
				} else {
					total_PAY_PRC += PAY_PRC_SI;
				}
				
				
				String INSUR_PRC = this.getStringWithNullCheck(map, "INSUR_PRC");
				// INSUR_PRC = INSUR_PRC.substring(0, INSUR_PRC.length()-5);
				INSUR_PRCI = Integer.parseInt(INSUR_PRC);
				doc.addNumericCell(INSUR_PRCI);
				total_INSUR_PRC += Integer.parseInt(INSUR_PRC);

				String RETIRE_PRC = this.getStringWithNullCheck(map, "RETIRE_PRC");
				// RETIRE_PRC = RETIRE_PRC.substring(0, RETIRE_PRC.length()-5);
				RETIRE_PRCI = Integer.parseInt(RETIRE_PRC);
				doc.addNumericCell(RETIRE_PRCI);
				total_RETIRE_PRC += Integer.parseInt(RETIRE_PRC);

				String M_EXP_PRC = this.getStringWithNullCheck(map, "M_EXP_PRC");
				// M_EXP_PRC = M_EXP_PRC.substring(0, M_EXP_PRC.length()-5);
				M_EXP_PRCI = Integer.parseInt(M_EXP_PRC);
				doc.addNumericCell(M_EXP_PRCI);
				total_M_EXP_PRC += Integer.parseInt(M_EXP_PRC);

				String TRIP_PRC = this.getStringWithNullCheck(map, "TRIP_PRC");
				// TRIP_PRC = TRIP_PRC.substring(0, TRIP_PRC.length()-5);
				TRIP_PRCI = Integer.parseInt(TRIP_PRC);
				doc.addNumericCell(TRIP_PRCI);
				total_TRIP_PRC += Integer.parseInt(TRIP_PRC);

				String Y_INCEN_PRC = this.getStringWithNullCheck(map, "Y_INCEN_PRC");
				// Y_INCEN_PRC = Y_INCEN_PRC.substring(0, Y_INCEN_PRC.length()-5);
				Y_INCEN_PRCI = Integer.parseInt(Y_INCEN_PRC);
				doc.addNumericCell(Y_INCEN_PRCI);
				total_Y_INCEN_PRC += Integer.parseInt(Y_INCEN_PRC);

				String Y_EXP_PRC = this.getStringWithNullCheck(map, "Y_EXP_PRC");
				// Y_EXP_PRC = Y_EXP_PRC.substring(0, Y_EXP_PRC.length()-5);
				Y_EXP_PRCI = Integer.parseInt(Y_EXP_PRC);
				doc.addNumericCell(Y_EXP_PRCI);
				total_Y_EXP_PRC += Integer.parseInt(Y_EXP_PRC);

				String PAY_CL_CD = this.getStringWithNullCheck(map, "PAY_CL_CD");
				if (PAY_CL_CD.equals("T")) {
					PAY_CL_CD = "당월";
				} else if (PAY_CL_CD.equals("N")) {
					PAY_CL_CD = "익월";
				}
				String PAY_DAY = this.getStringWithNullCheck(map, "PAY_DAY");
				String PP = PAY_CL_CD + " " + PAY_DAY;
				doc.addAlignedCell(PP, CellStyleFactory.ALIGN_CENTER);

				int BENEFIT = CONT_AMTI - (PAY_PRCI + PAY_PRC_SI + INSUR_PRCI + RETIRE_PRCI + M_EXP_PRCI + TRIP_PRCI + Y_INCEN_PRCI
						+ (Y_EXP_PRCI / 12));
				int EXP_PRC = PAY_PRCI + PAY_PRC_SI + INSUR_PRCI + RETIRE_PRCI + M_EXP_PRCI + TRIP_PRCI + Y_INCEN_PRCI
						+ (Y_EXP_PRCI / 12);
				doc.addNumericCell(BENEFIT);
//				doc.addNumericCell(CONT_AMTI);
				doc.addNumericCell(EXP_PRC);
				total_BENEFIT += BENEFIT;
				total_EXP_PRC += EXP_PRC;
			}
			int listSize = lists.size() + 3;
			doc.nextRow();
			doc.addSpan(1, 8);
			doc.addAlignedCell(("합계"), CellStyleFactory.ALIGN_CENTER);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addNumericCell(total_CONT_AMT);
			doc.addNumericCell(total_PAY_PRC);
			doc.addNumericCell(total_INSUR_PRC);
			doc.addNumericCell(total_RETIRE_PRC);
			doc.addNumericCell(total_M_EXP_PRC);
			doc.addNumericCell(total_TRIP_PRC);
			doc.addNumericCell(total_Y_INCEN_PRC);
			doc.addNumericCell(total_Y_EXP_PRC);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addNumericCell(total_BENEFIT);
//			doc.addNumericCell(total_CONT_AMT);
			doc.addNumericCell(total_EXP_PRC);

			// doc.addAlignedCell(("=SUM(G4:G"+listSize+")"), CellStyleFactory.ALIGN_RIGHT);

			return doc.download(request, "계약조회 리스트_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
		}
	}
       
	/**
	 * Excel Download
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse excelContractSelectList(IRequest request) throws Exception {

		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		String width = request.getStringParam("grid_col_width");
		String[] widths = width.split(",");
		String header = request.getStringParam("grid_col_header");
		String[] headers = header.split(",");

		try {
			// Excel Header 설정
			IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
			IExcelDocument doc = excel.createDocument("계약관리 리스트");
			int[] col_width = new int[col.length()];

			doc.addTitleCell("순번");
			col_width[0] = Integer.parseInt("51") * 40;
			doc.addTitleCell("계약번호");
			col_width[1] = Integer.parseInt("160") * 40;
			doc.addTitleCell("인재ID");
			col_width[2] = Integer.parseInt("160") * 40;
			doc.addTitleCell("성명");
			col_width[3] = Integer.parseInt("60") * 40;
			doc.addTitleCell("계약구분");
			col_width[4] = Integer.parseInt("60") * 40;
			doc.addTitleCell("프로젝트명");
			col_width[5] = Integer.parseInt("200") * 40;
			doc.addTitleCell("수행사");
			col_width[6] = Integer.parseInt("150") * 40;
			doc.addTitleCell("소속사");
			col_width[7] = Integer.parseInt("150") * 40;
			doc.addTitleCell("시작일");
			col_width[8] = Integer.parseInt("130") * 40;
			doc.addTitleCell("종료일");
			col_width[9] = Integer.parseInt("130") * 40;
			doc.addTitleCell("지급구분");
			col_width[10] = Integer.parseInt("60") * 40;
			doc.addTitleCell("지급일");
			col_width[11] = Integer.parseInt("60") * 40;
			doc.addTitleCell("계약금액");
			col_width[12] = Integer.parseInt("110") * 40;
			doc.addTitleCell("비고");
			col_width[13] = Integer.parseInt("200") * 40;

			doc.setColumnsWidth(col_width);

			Map smap = request.getMap();
			List lists = getList("mg.ni.contractSelectExcelDownA", smap);
			int CONT_AMTI = 0;
			int total_CONT_AMT = 0;

			for (int i = 0; i < lists.size(); i++) {
				Map map = (Map) lists.get(i);
				int num = i + 1;
				doc.nextRow();
				doc.addAlignedCell(num, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "CONT_NO"), CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_ID"), CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_NM"), CellStyleFactory.ALIGN_LEFT);
				String CONT_CL_CD = this.getStringWithNullCheck(map, "CONT_CL_CD");
				if (CONT_CL_CD.equals("1")) {
					CONT_CL_CD = "자사";
				} else if (CONT_CL_CD.equals("2")) {
					CONT_CL_CD = "프리";
				} else if (CONT_CL_CD.equals("3")) {
					CONT_CL_CD = "외주";
				}
				doc.addAlignedCell(CONT_CL_CD, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "PROJ_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "COMP_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "AGENCY_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "START_YMD"), CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "END_YMD"), CellStyleFactory.ALIGN_CENTER);
				String PAY_CL_CD = this.getStringWithNullCheck(map, "PAY_CL_CD");
				if (PAY_CL_CD.equals("T")) {
					PAY_CL_CD = "당월";
				} else if (PAY_CL_CD.equals("N")) {
					PAY_CL_CD = "익월";
				}
				doc.addAlignedCell(PAY_CL_CD, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "PAY_DAY"), CellStyleFactory.ALIGN_RIGHT);
				String CONT_AMT = this.getStringWithNullCheck(map, "CONT_AMT");
				CONT_AMTI = Integer.parseInt(CONT_AMT);
				doc.addNumericCell(CONT_AMTI);
				total_CONT_AMT += Integer.parseInt(CONT_AMT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "CONT_ETC"), CellStyleFactory.ALIGN_LEFT);
			}
			int listSize = lists.size() + 3;
			doc.nextRow();
			doc.addSpan(1, 11);
			doc.addAlignedCell(("합계"), CellStyleFactory.ALIGN_CENTER);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
			doc.addNumericCell(total_CONT_AMT);

			// doc.addAlignedCell(("=SUM(G4:G"+listSize+")"), CellStyleFactory.ALIGN_RIGHT);

			return doc.download(request, "계약관리 리스트_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
		}
	}
        
	/**
	 * 인력급여지급 조회
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse unitPaySelect(IRequest request) throws Exception {
		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		Map map = null;
		GridData gdRes = new GridData();
		String dbName = pm.getString("component.sql.database");

		try {
			Map smap = request.getMap();
			smap.put("dbName", pm.getString("component.sql.database"));
			List lists;
			lists = getList("mg.ni.unitPaySelect_" + dbName, smap);
			for (int i = 0; i < lists.size(); i++) {
				map = (Map) lists.get(i);

				for (int j = 0; j < cols.length; j++) {
					gdRes.addValue(cols[j], this.getStringWithNullCheck(map, cols[j]));
				}
			}
			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true",
					"doQuery", gdRes.getGridXmlDatas()));
		} catch (Exception e) {
			log.error("---------------------------------------------------------------\n" + e.toString());
			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false",
					"doQuery", gdRes.getGridXmlDatas()));
		}
	}

	/**
	 * 인력급여지급 Excel Download
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	private IResponse excelUnitPaySelect(IRequest request) throws Exception {

		String col = request.getStringParam("grid_col_id");
		String[] cols = col.split(",");
		String width = request.getStringParam("grid_col_width");
		String[] widths = width.split(",");
		String header = request.getStringParam("grid_col_header");
		String[] headers = header.split(",");

		try {
			String pay_day = request.getStringParam("PAY_DAY");
			String now_ym = request.getStringParam("START_YM").substring(0, 6);
			
			// Excel Header 설정
			IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
			IExcelDocument doc = excel.createDocument("인력급여지급");
			int[] col_width = new int[col.length()];

			doc.addTitleCell("순번");
			col_width[0] = Integer.parseInt("51") * 40;
			doc.addTitleCell("계약구분");
			col_width[1] = Integer.parseInt("60") * 40;
			doc.addTitleCell("성명");
			col_width[2] = Integer.parseInt("60") * 40;
			doc.addTitleCell("인재프로젝트명");
			col_width[3] = Integer.parseInt("200") * 40;
			doc.addTitleCell("시작일");
			col_width[4] = Integer.parseInt("130") * 40;
			doc.addTitleCell("종료일");
			col_width[5] = Integer.parseInt("130") * 40;
			doc.addTitleCell("지급구분");
			col_width[6] = Integer.parseInt("60") * 40;
			doc.addTitleCell("지급일");
			col_width[7] = Integer.parseInt("60") * 40;
			doc.addTitleCell("계약금액");
			col_width[8] = Integer.parseInt("110") * 40;
			doc.addTitleCell("근로소득");
			col_width[9] = Integer.parseInt("110") * 40;
			doc.addTitleCell("사업소득");
			col_width[10] = Integer.parseInt("110") * 40;
			doc.addTitleCell("지급형태");
			col_width[11] = Integer.parseInt("60") * 40;
			doc.addTitleCell("업체명");
			col_width[12] = Integer.parseInt("120") * 40;

			doc.setColumnsWidth(col_width);

			Map smap = request.getMap();
			List lists = getList("mg.ni.unitPaySelect_mysql", smap);
			int CONT_AMTI = 0;
			int PAY_PRCI = 0;
			int PAY_PRC_SI = 0;

			for (int i = 0; i < lists.size(); i++) {
				Map map = (Map) lists.get(i);
				int num = i + 1;
				doc.nextRow();
				doc.addAlignedCell(num, CellStyleFactory.ALIGN_CENTER);
				String CONT_CL_CD = this.getStringWithNullCheck(map, "CONT_CL_CD");
				if (CONT_CL_CD.equals("1")) {
					CONT_CL_CD = "자사";
				} else if (CONT_CL_CD.equals("2")) {
					CONT_CL_CD = "프리";
				} else if (CONT_CL_CD.equals("3")) {
					CONT_CL_CD = "외주";
				}
				doc.addAlignedCell(CONT_CL_CD, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "PROJ_NM"), CellStyleFactory.ALIGN_LEFT);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "START_YMD"), CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "END_YMD"), CellStyleFactory.ALIGN_CENTER);
				String PAY_CL_CD = this.getStringWithNullCheck(map, "PAY_CL_CD");
				if (PAY_CL_CD.equals("T")) {
					PAY_CL_CD = "당월";
				} else if (PAY_CL_CD.equals("N")) {
					PAY_CL_CD = "익월";
				}
				doc.addAlignedCell(PAY_CL_CD, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "PAY_DAY"), CellStyleFactory.ALIGN_RIGHT);
				String CONT_AMT = this.getStringWithNullCheck(map, "CONT_AMT");
				CONT_AMTI = Integer.parseInt(CONT_AMT);
				doc.addNumericCell(CONT_AMTI);
				String PAY_PRC = this.getStringWithNullCheck(map, "PAY_PRC");
				String PAY_PRC_S = this.getStringWithNullCheck(map, "PAY_PRC_S");
				PAY_PRCI = Integer.parseInt(PAY_PRC);
				PAY_PRC_SI = Integer.parseInt(PAY_PRC_S);
				doc.addNumericCell(PAY_PRCI);
				doc.addNumericCell(PAY_PRC_SI);
				
				String PAY_FO_CD = this.getStringWithNullCheck(map, "PAY_FO_CD");
				if (PAY_FO_CD.equals("CO")) {
					PAY_FO_CD = "자사";
				} else if (PAY_FO_CD.equals("OT")) {
					PAY_FO_CD = "타사";
				}
				doc.addAlignedCell(PAY_FO_CD, CellStyleFactory.ALIGN_CENTER);
				doc.addAlignedCell(this.getStringWithNullCheck(map, "PAY_COMP"), CellStyleFactory.ALIGN_LEFT);
			}

			return doc.download(request, "인력급여지급 " + now_ym + " (" + pay_day + "일)_" + DateUtils.getCurrentDate("yyyyMMdd") + ".xls");
		} catch (Exception e) {
			log.error("---조회된 데이터가 없습니다.-----------------------------------------------------------\n" + e.toString());
			return closePopupWindow("엑셀 파일 생성중 오류 발생 되었습니다. \n관리자에게 문의 바랍니다.");
		}
	}

	public Workbook getWorkbook(String filename, String version) {
		try (FileInputStream stream = new FileInputStream(filename)) {
			if ("xls".equals(version)) {
				return (Workbook) new HSSFWorkbook(stream);
			} else if ("xlsx".equals(version)) {
				return new XSSFWorkbook(stream);
			}
			throw new NoClassDefFoundError();
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
	
	// Sheet로 부터 Row를 취득, 생성하기
	public Row getRow(Sheet sheet, int rownum) {
		Row row = sheet.getRow(rownum);
		if (row == null) {
			row = sheet.createRow(rownum);
		}
		return row;
	}
	
	// Row로 부터 Cell를 취득, 생성하기
	public Cell getCell(Row row, int cellnum) {
		Cell cell = row.getCell(cellnum);
		if (cell == null) {
			cell = row.createCell(cellnum);
		}
		return cell;
	}

	public Cell getCell(Sheet sheet, int rownum, int cellnum) {
		Row row = getRow(sheet, rownum);
		return getCell(row, cellnum);
	}

	public void writeExcel(Workbook workbook, String filepath) {
		try (FileOutputStream stream = new FileOutputStream(filepath)) {
			workbook.write(stream);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
  
}