package mg.ni;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class itotarperAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("itoTargetStSalSelToSel")) {   /** 매출기준목표 조회화면 조회 */
            return itoTargetStSalSelToSel(request);
        } else if(request.isMode("itoTargetStSalSelToMod")) {	/** 매출기준목표 수정화면 조회 */
        	return itoTargetStSalSelToMod(request);
        } else if(request.isMode("itoTargetStSalSave")) {   /** 매출기준목표 수정화면 저장 */
            return itoTargetStSalSave(request);
        } else if(request.isMode("itoTargetStConSelToSel")) {   /** 계약기준목표 조회화면 조회 */
            return itoTargetStConSelToSel(request);
        } else if(request.isMode("itoTargetStConSelToMod")) {   /** 계약기준목표 수정화면 조회 */
            return itoTargetStConSelToMod(request);
        } else if(request.isMode("itoTargetStConSave")) {   /** 계약기준목표 수정화면 저장 */
            return itoTargetStConSave(request);
        } else if(request.isMode("itoTargetStConDelete")) {   /** 계약기준목표 수정화면 삭제 */
            return itoTargetStConDelete(request);
        } else if(request.isMode("itoActiveManageSelToManager")) {   /** 활동관리 담당자별 조회 */
            return itoActiveManageSelToManager(request);
        } else if(request.isMode("itoActiveManageSelToDay")) {   /** 활동관리 일자별 조회 */
            return itoActiveManageSelToDay(request);
        } else if(request.isMode("itoActiveManageModSel")) {   /** 활동관리 수정 조회 */
            return itoActiveManageModSel(request);
        } else if(request.isMode("itoActiveManageSave")) {   /** 활동관리 저장 */
            return itoActiveManageSave(request);
        } else if(request.isMode("itoActiveManageDelete")) {   /** 활동관리 삭제 */
            return itoActiveManageDelete(request);
        } else if(request.isMode("itoActiveManageCustomerSel")) {   /** 활동 고객사관리 조회 */
            return itoActiveManageCustomerSel(request);
        } else if(request.isMode("itoActiveManageCustomerSave")) {   /** 활동 고객사관리 저장 */
            return itoActiveManageCustomerSave(request);
        } else if(request.isMode("itoActiveManageCustomerDelete")) {   /** 활동 고객사관리 삭제 */
            return itoActiveManageCustomerDelete(request);
        } else if(request.isMode("customerPopupSel")) {   /** 활동 고객사 팝업 조회 */
            return customerPopupSel(request);
        } else if(request.isMode("ItoSalesStMaSeltoPerf")) {   /** 매출목표실적 조회화면 실적 조회 */
            return ItoSalesStMaSeltoPerf(request);
        } else if(request.isMode("ItoSalesStMaSeltoDiff")) {   /** 매출목표실적 조회화면 차이 조회 */
            return ItoSalesStMaSeltoDiff(request);
        } else if(request.isMode("ItoSalesStMaResultSel")) {	/** 매출목표달성도 조회 */
        	return ItoSalesStMaResultSel(request);
        } else if(request.isMode("ItoSalesStMaSave")) {   /** 매출목표실적 수정화면 저장 ========= 없는기능*/
            return ItoSalesStMaSave(request);
        } else if(request.isMode("itoInputCurSituSel")) {   /** 투입현황 조회 */
            return itoInputCurSituSel(request);
        } else if(request.isMode("itoInputCurSituSelToMod")) {   /** 투입현황 수정화면 조회 */
            return itoInputCurSituSelToMod(request);
        } else if(request.isMode("itoInputCurSituSave")) {   /** 투입현황 수정화면 저장 */
            return itoInputCurSituSave(request);
        } else if(request.isMode("itoTargetStSalSelToSelExcel")) {   /**매출기준목표 조회화면 엑셀다운 */
            return itoTargetStSalSelToSelExcel(request);
        } else { 
            return write(null);
        } 
    }  

	/**
    * 매출기준목표 조회화면 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse itoTargetStSalSelToSel(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
	        lists = getList("mg.ni.itoTargetStSalSelToSel",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------itoTargetStSalSelToSel----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    }
    
    /**
     * 매출기준목표 수정화면 조회
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse itoTargetStSalSelToMod(IRequest request) throws Exception 
    {
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
	        String target_year = request.getStringParam("NOW_YEAR");
	        smap.put("TARGET_YEAR", target_year);
	        
	        if (Integer.parseInt(getItem("mg.ni.itoTargetStSalSelToModToYearExist", smap).toString()) > 0) {
	        	lists = getList("mg.ni.itoTargetStSalSelToModY",smap);
	        } else {
	        	lists = getList("mg.ni.itoTargetStSalSelToModN",smap);
	        }
	        
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------itoTargetStSalSelToMod----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    }
    
   
    /**
     * 매출기준목표 수정화면 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse itoTargetStSalSave(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
       
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
		       	item = new HashMap();
		       	
		        String target_year = request.getStringParam("NOW_YEAR");
		       	item.put("TARGET_YEAR", target_year);
		       	String target_month = request.getStringParam(ids[i] + "_TARGET_MONTH");
		       	item.put("TARGET_MONTH", target_month);
		       	
		       	for(int j=0; j < cols.length; j++ ) {
		       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		       		
			    	item.put(cols[j], tmps);
		        }
		       	
		       	String manpower_son = request.getStringParam(ids[i] + "_" + "MANPOWER_SON").replaceAll("[^\\.0-9]", "");
		       	item.put("MANPOWER_SON", manpower_son);

		       	String sal_amount_son = request.getStringParam(ids[i] + "_" + "SAL_AMOUNT_SON").replaceAll("[^\\.0-9]", "");
		       	item.put("SAL_AMOUNT_SON", sal_amount_son);
		       	String sal_profit_son = request.getStringParam(ids[i] + "_" + "SAL_PROFIT_SON").replaceAll("[^\\.0-9]", "");
		       	item.put("SAL_PROFIT_SON", sal_profit_son);
		       	String manpower_gwa = request.getStringParam(ids[i] + "_" + "MANPOWER_GWA").replaceAll("[^\\.0-9]", "");
		       	item.put("MANPOWER_GWA", manpower_gwa);

		       	String sal_amount_gwa = request.getStringParam(ids[i] + "_" + "SAL_AMOUNT_GWA").replaceAll("[^\\.0-9]", "");
		       	item.put("SAL_AMOUNT_GWA", sal_amount_gwa);
		       	String sal_profit_gwa = request.getStringParam(ids[i] + "_" + "SAL_PROFIT_GWA").replaceAll("[^\\.0-9]", "");
		       	item.put("SAL_PROFIT_GWA", sal_profit_gwa);
		       	String manpower_go = request.getStringParam(ids[i] + "_" + "MANPOWER_GO").replaceAll("[^\\.0-9]", "");
		       	item.put("MANPOWER_GO", manpower_go);

		       	String sal_amount_go = request.getStringParam(ids[i] + "_" + "SAL_AMOUNT_GO").replaceAll("[^\\.0-9]", "");
		       	item.put("SAL_AMOUNT_GO", sal_amount_go);
		       	String sal_profit_go = request.getStringParam(ids[i] + "_" + "SAL_PROFIT_GO").replaceAll("[^\\.0-9]", "");
		       	item.put("SAL_PROFIT_GO", sal_profit_go);
		       	
		       	if (Integer.parseInt(getItem("mg.ni.itoTargetStSalSelToModToDataExistSon", item).toString()) > 0) {
		       	} else {
		       		for (int aa = 1; aa < 13; aa++) {
		       			item.put("FORCE_INPUT_MONTH", aa);
		       			
		       			String ordernum_head = "0";
		       			
		       			if(aa < 10) {
		       				ordernum_head += aa;
		       			} else {
		       				ordernum_head = "";
		       				ordernum_head += aa;
		       			}
		       			
		       			item.put("ORDER_CD", ordernum_head + "00");
		       			
		       			createObject("mg.ni.itoTargetStSalInsertSon", item);
		       		}
		       	}
		       	updateObject("mg.ni.itoTargetStSalUpdateSon", item);
		       	
		       	if (Integer.parseInt(getItem("mg.ni.itoTargetStSalSelToModToDataExistGwa", item).toString()) > 0) {
		       	} else {
		       		for (int aa = 1; aa < 13; aa++) {
		       			item.put("FORCE_INPUT_MONTH", aa);
		       			
		       			String ordernum_head = "0";
		       			
		       			if(aa < 10) {
		       				ordernum_head += aa;
		       			} else {
		       				ordernum_head = "";
		       				ordernum_head += aa;
		       			}
		       			
		       			item.put("ORDER_CD", ordernum_head + "00");
		       			
		       			createObject("mg.ni.itoTargetStSalInsertGwa", item);
		       		}
		       	}
		       	updateObject("mg.ni.itoTargetStSalUpdateGwa", item);
		       	
		       	if (Integer.parseInt(getItem("mg.ni.itoTargetStSalSelToModToDataExistGo", item).toString()) > 0) {
		       	} else {
		       		for (int aa = 1; aa < 13; aa++) {
		       			item.put("FORCE_INPUT_MONTH", aa);
		       			
		       			String ordernum_head = "0";
		       			
		       			if(aa < 10) {
		       				ordernum_head += aa;
		       			} else {
		       				ordernum_head = "";
		       				ordernum_head += aa;
		       			}
		       			
		       			item.put("ORDER_CD", ordernum_head + "00");
		       			
		       			createObject("mg.ni.itoTargetStSalInsertGo", item);
		       		}
		       	}
		       	updateObject("mg.ni.itoTargetStSalUpdateGo", item);
	        }
	        
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------itoTargetStSalSave----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    /**
     * 계약기준목표 조회화면 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse itoTargetStConSelToSel(IRequest request) throws Exception 
     {
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();

         try {
         	Map smap = request.getMap();
 	        List lists;
 	        
 	        String target_year = request.getStringParam("NOW_YEAR");
	        smap.put("TARGET_YEAR", target_year);
	        String user_id = request.getStringParam("USER_ID");
	        if(!user_id.equals("all")) {
	        	smap.put("USER_ID", user_id);
	        	
	        	lists = getList("mg.ni.itoTargetStConSelToSelSpeci",smap);
	 	        for (int i = 0; i < lists.size(); i++) {
	 	         	map =(Map) lists.get(i);
	 	         	for(int j=0; j < cols.length; j++ ) {
	 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	 	         	}
	 	        }
	        } else {
	        	lists = getList("mg.ni.itoTargetStConSelToSelAll",smap);
	 	        for (int i = 0; i < lists.size(); i++) {
	 	         	map =(Map) lists.get(i);
	 	         	for(int j=0; j < cols.length; j++ ) {
	 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	 	         	}
	 	        }
	        }
 	        
 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	   log.error("-----------------itoTargetStConSelToSel----------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     }
     
     /**
      * 계약기준목표 수정화면 조회
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse itoTargetStConSelToMod(IRequest request) throws Exception 
     {
     	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();

         try {
         	Map smap = request.getMap();
 	        List lists;
 	        
 	        String target_year = request.getStringParam("NOW_YEAR");
 	        smap.put("TARGET_YEAR", target_year);
 	        int last_year = Integer.parseInt(target_year)-1;
 	        smap.put("LAST_YEAR", last_year);
 	        
 	        String user_id = request.getStringParam("USER_ID");
	        if(!user_id.equals("all")) {
	        	smap.put("USER_ID", user_id);
	        }
 	        
 	        if (Integer.parseInt(getItem("mg.ni.itoTargetStConSelToModToYearExist", smap).toString()) > 0) {
 	        	lists = getList("mg.ni.itoTargetStConSelToModY",smap);
 	        } else {
 	        	lists = getList("mg.ni.itoTargetStConSelToModN",smap);
 	        }
 	        
 	        for (int i = 0; i < lists.size(); i++) {
 	         	map =(Map) lists.get(i);
 	         	for(int j=0; j < cols.length; j++ ) {
 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	         	}
 	        }   
 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	   log.error("-----------------itoTargetStConSelToMod----------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     }
     
     /**
      * 계약기준목표 수정화면 저장
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse itoTargetStConSave(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
        
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 		       	item = new HashMap();
 		       	
 		        String target_year = request.getStringParam("NOW_YEAR");
 		       	item.put("TARGET_YEAR", target_year);
 		       	
 		       	String user_id = request.getStringParam("USER_ID");
 		        if(!user_id.equals("all")) {
 		        	item.put("USER_ID", user_id);
 		        }
 		       	
 		       	for(int j=0; j < cols.length; j++ ) {
 		       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		       		
 			    	item.put(cols[j], tmps);
 		        }
 		       	
 		       	if (Integer.parseInt(getItem("mg.ni.itoTargetStConSelToModToDataExist", item).toString()) > 0) {
 		       		updateObject("mg.ni.itoTargetStConUpdate", item);
 		       	} else {
 		       		createObject("mg.ni.itoTargetStConInsert", item);
 		       	}
 	        }
 	        
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------itoTargetStConSave----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
 	        flag = "false";
         }
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }
     
     /**
 	 * 계약기준목표 수정화면 삭제
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoTargetStConDelete(IRequest request) throws Exception {
 		String ids_info = request.getStringParam("ids");
 		String cols_ids = request.getStringParam("col_ids");
 		String[] cols = cols_ids.split(",");
 		String[] ids = ids_info.split(",");
 		String msg = "", flag = "";
 		try {
 			tx.begin();
 			Map item = null;
 			for (int i = 0; i < ids.length; i++) {
 				item = new HashMap();
 				
 				String target_year = request.getStringParam("NOW_YEAR");
 		       	item.put("TARGET_YEAR", target_year);
 		       	
 		       	String user_id = request.getStringParam("USER_ID");
		        if(!user_id.equals("all")) {
		        	item.put("USER_ID", user_id);
		        }
 		       	
 				for (int j = 0; j < cols.length; j++) {
 					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 					item.put(cols[j], tmps);
 				}
 				deleteObject("mg.ni.itoTargetStConDelete", item);
 			}
 			tx.commit();
 			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 			flag = "true";
 		} catch (Exception e) {
 			tx.rollback();
 			log.error("------------itoTargetStConDelete----------------------------------------------------------------\n"
 					+ e.toString());
 			msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 			flag = "false";
 		}
 		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
 		return writeXml(ret);
 	}
 	
 	/**
 	 * 활동관리 담당자별 조회
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageSelToManager(IRequest request) throws Exception {
 		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
	        String user_id = request.getStringParam("USER_ID");
	        smap.put("USER_ID", user_id);
	        
	        lists = getList("mg.ni.itoActiveManageSelToManager",smap);
	        
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------itoTargetStSalSelToMod----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
 	}
 	
 	/**
 	 * 활동관리 일자별 조회
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageSelToDay(IRequest request) throws Exception {
 		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
	        String start_ymd = request.getStringParam("START_YMD");
	        smap.put("START_YMD", start_ymd);
	        String end_ymd = request.getStringParam("END_YMD");
	        smap.put("END_YMD", end_ymd);
	        
	        lists = getList("mg.ni.itoActiveManageSelToDay",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------itoActiveManageSelToDay----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
 	}
 	
 	/**
 	 * 활동관리 수정 조회
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageModSel(IRequest request) throws Exception {
 		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
	        lists = getList("mg.ni.itoActiveManageModSel",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------itoActiveManageModSel----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
 	}
 	
 	/**
 	 * 활동관리 저장
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageSave(IRequest request) throws Exception {
 		String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
       
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
		       	item = new HashMap();
		       	
		       	for(int j=0; j < cols.length; j++ ) {
		       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		       		
			    	item.put(cols[j], tmps);
		        }
		       	
		       	if (Integer.parseInt(getItem("mg.ni.itoActiveManageExist", item).toString()) > 0) {
		       		updateObject("mg.ni.itoActiveManageUpdate", item);
		       	} else {
		       		createObject("mg.ni.itoActiveManageInsert", item);
		       	}
	        }

	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------itoTargetStConSave----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
 	}
 	
 	/**
 	 * 활동관리 삭제
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageDelete(IRequest request) throws Exception {
 		String ids_info = request.getStringParam("ids");
 		String cols_ids = request.getStringParam("col_ids");
 		String[] cols = cols_ids.split(",");
 		String[] ids = ids_info.split(",");
 		String msg = "", flag = "";
 		try {
 			tx.begin();
 			Map item = null;
 			for (int i = 0; i < ids.length; i++) {
 				item = new HashMap();
 				
 				for (int j = 0; j < cols.length; j++) {
 					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 					item.put(cols[j], tmps);
 				}
 				deleteObject("mg.ni.itoActiveManageDelete", item);
 			}
 			tx.commit();
 			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 			flag = "true";
 		} catch (Exception e) {
 			tx.rollback();
 			log.error("------------itoTargetStConDelete----------------------------------------------------------------\n"
 					+ e.toString());
 			msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 			flag = "false";
 		}
 		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
 		return writeXml(ret);
 	}
 	
 	/**
 	 * 활동 고객사관리 조회
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageCustomerSel(IRequest request) throws Exception {
 		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
			/*
			 * String s_customer_nm = request.getStringParam("S_CUSTOMER_NM");
			 * smap.put("S_CUSTOMER_NM", s_customer_nm);
			 */
	        
	        lists = getList("mg.ni.itoActiveManageCustomerSel",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------itoActiveManageCustomerSel----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
 	}
 	
 	/**
 	 * 활동 고객사관리 저장
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageCustomerSave(IRequest request) throws Exception {
 		String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
       
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
		       	item = new HashMap();
		       	
		       	for(int j=0; j < cols.length; j++ ) {
		       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		       		
			    	item.put(cols[j], tmps);
		        }
		       	
		       	if (Integer.parseInt(getItem("mg.ni.itoActiveManageCustomerExist", item).toString()) > 0) {
		       		updateObject("mg.ni.itoActiveManageCustomerUpdate", item);
		       	} else {
		       		createObject("mg.ni.itoActiveManageCustomerInsert", item);
		       	}
	        }
	        
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------itoTargetStConSave----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
 	}
 	
 	/**
 	 * 활동 고객사관리 삭제
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse itoActiveManageCustomerDelete(IRequest request) throws Exception {
 		String ids_info = request.getStringParam("ids");
 		String cols_ids = request.getStringParam("col_ids");
 		String[] cols = cols_ids.split(",");
 		String[] ids = ids_info.split(",");
 		String msg = "", flag = "";
 		try {
 			tx.begin();
 			Map item = null;
 			for (int i = 0; i < ids.length; i++) {
 				item = new HashMap();
 				
 				for (int j = 0; j < cols.length; j++) {
 					String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 					item.put(cols[j], tmps);
 				}
 				deleteObject("mg.ni.itoActiveManageCustomerDelete", item);
 			}
 			tx.commit();
 			msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 			flag = "true";
 		} catch (Exception e) {
 			tx.rollback();
 			log.error("------------itoTargetStConDelete----------------------------------------------------------------\n"
 					+ e.toString());
 			msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 			flag = "false";
 		}
 		String ret = responseTranData(msg, flag, "doSave", "doSaveEnd");
 		return writeXml(ret);
 	}
 	
 	/**
 	 * 활동 고객사 팝업 조회
 	 * 
 	 * @param request
 	 * @return
 	 * @throws Exception
 	 */
 	private IResponse customerPopupSel(IRequest request) throws Exception {
 		String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();

        try {
        	Map smap = request.getMap();
	        List lists;
	        
	        String customer_cd = request.getStringParam("S_CUSTOMER_CD");
			smap.put("S_CUSTOMER_CD", customer_cd);
			String customer_nm = request.getStringParam("S_CUSTOMER_NM");
			smap.put("S_CUSTOMER_NM", customer_nm);
	        
	        lists = getList("mg.ni.customerPopupSel",smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("-----------------customerPopupSel----------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
 	}
 	
	/**
     * 매출목표실적 (유지기준) 조회화면 실적 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse ItoSalesStMaSeltoPerf(IRequest request) throws Exception 
     {
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();

         try {
         	Map smap = request.getMap();
 	        List lists;
 	        
 	        String now_year = request.getStringParam("NOW_YEAR");
			smap.put("NOW_YEAR", now_year);
			String select_type = request.getStringParam("SELECT_TYPE");

			if(select_type.equals("admit")) {
				lists = getList("mg.ni.ItoSalesStMaSeltoPerfAdmit",smap);
			}
			else {
				lists = getList("mg.ni.ItoSalesStMaSeltoPerfCalcu",smap);
			}

 	        for (int i = 0; i < lists.size(); i++) {
 	         	map =(Map) lists.get(i);
 	         	for(int j=0; j < cols.length; j++ ) {
 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	         	}
 	        }   
 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	   log.error("-----------------ItoSalesStMaSeltoPerf----------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     }
     
     /**
      * 매출목표실적 (유지기준) 조회화면 차이 조회
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse ItoSalesStMaSeltoDiff(IRequest request) throws Exception 
      {
       	String col = request.getStringParam("grid_col_id");
       	String[] cols = col.split(",");
          Map map = null;
          GridData gdRes = new GridData();

          try {
          	Map smap = request.getMap();
  	        List lists;
  	        
  	        String now_year = request.getStringParam("NOW_YEAR");
			smap.put("NOW_YEAR", now_year);
			String select_type = request.getStringParam("SELECT_TYPE");

			if(select_type.equals("admit")) {
				lists = getList("mg.ni.ItoSalesStMaSeltoDiffAdmit",smap);
			} else {
				lists = getList("mg.ni.ItoSalesStMaSeltoDiffCalcu",smap);
			}

  	        for (int i = 0; i < lists.size(); i++) {
  	         	map =(Map) lists.get(i);
  	         	for(int j=0; j < cols.length; j++ ) {
  	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	         	}
  	        }   
  	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	   log.error("-----------------ItoSalesStMaSeltoDiff----------------------------------------------\n" + e.toString());
         	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
      }
      
      /**
       * 매출목표달성도 조회
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse ItoSalesStMaResultSel(IRequest request) throws Exception 
      {
      	String col = request.getStringParam("grid_col_id");
       	String[] cols = col.split(",");
          Map map = null;
          GridData gdRes = new GridData();

          try {
          	Map smap = request.getMap();
  	        List lists;
  	        
  	        String now_year = request.getStringParam("NOW_YEAR");
  	        smap.put("NOW_YEAR", now_year);
  	        String select_type = request.getStringParam("SEL_TYPE");
  	        smap.put("SEL_TYPE", select_type);
  	        
  	        if(select_type.equals("ito")) {
  	  	        lists = getList("mg.ni.ItoSalesStMaResultSelITO",smap);
			} else if(select_type.equals("son")) {
				lists = getList("mg.ni.ItoSalesStMaResultSelSon",smap);
			} else if(select_type.equals("gwa")) {
				lists = getList("mg.ni.ItoSalesStMaResultSelGwa",smap);
			} else {
				lists = getList("mg.ni.ItoSalesStMaResultSelGo",smap);
			}
  	        
  	        for (int i = 0; i < lists.size(); i++) {
  	         	map =(Map) lists.get(i);
  	         	for(int j=0; j < cols.length; j++ ) {
  	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	         	}
  	        }   
  	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	   log.error("-----------------ItoSalesStMaSeltoMod----------------------------------------------\n" + e.toString());
         	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
      }
      
     
      /**
       * 매출목표실적 수정화면 저장
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse ItoSalesStMaSave(IRequest request) throws Exception 
      {
          String ids_info = request.getStringParam("ids");
          String cols_ids = request.getStringParam("col_ids");
          String[] cols = cols_ids.split(",");
          String[] ids = ids_info.split(",");
          String msg="", flag="";
         
          try {
          	tx.begin();
  	        Map item = null;
  	        for (int i = 0; i < ids.length; i++) {
  		       	item = new HashMap();
  		       	
  		        String target_year = request.getStringParam("NOW_YEAR");
  		       	item.put("TARGET_YEAR", target_year);
  		       	String target_month = request.getStringParam(ids[i] + "_TARGET_MONTH");
  		       	item.put("TARGET_MONTH", target_month);
  		       	String select_type = request.getStringParam("SELECT_TYPE");
  		       	item.put("SELECT_TYPE", select_type);
  		       	
  		       	for(int j=0; j < cols.length; j++ ) {
  		       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
  		       		
  			    	item.put(cols[j], tmps);
  		        }
  		       	
  		       	String manpower_son = request.getStringParam(ids[i] + "_" + "MANPOWER_SON").replaceAll("[^\\.0-9]", "");
  		       	item.put("MANPOWER_SON", manpower_son);

  		       	String sal_amount_son = request.getStringParam(ids[i] + "_" + "SAL_AMOUNT_SON").replaceAll("[^\\.0-9]", "");
  		       	item.put("SAL_AMOUNT_SON", sal_amount_son);
  		       	String sal_profit_son = request.getStringParam(ids[i] + "_" + "SAL_PROFIT_SON").replaceAll("[^\\.0-9]", "");
  		       	item.put("SAL_PROFIT_SON", sal_profit_son);
  		       	String manpower_gwa = request.getStringParam(ids[i] + "_" + "MANPOWER_GWA").replaceAll("[^\\.0-9]", "");
  		       	item.put("MANPOWER_GWA", manpower_gwa);

  		       	String sal_amount_gwa = request.getStringParam(ids[i] + "_" + "SAL_AMOUNT_GWA").replaceAll("[^\\.0-9]", "");
  		       	item.put("SAL_AMOUNT_GWA", sal_amount_gwa);
  		       	String sal_profit_gwa = request.getStringParam(ids[i] + "_" + "SAL_PROFIT_GWA").replaceAll("[^\\.0-9]", "");
  		       	item.put("SAL_PROFIT_GWA", sal_profit_gwa);
  		       	String manpower_go = request.getStringParam(ids[i] + "_" + "MANPOWER_GO").replaceAll("[^\\.0-9]", "");
  		       	item.put("MANPOWER_GO", manpower_go);

  		       	String sal_amount_go = request.getStringParam(ids[i] + "_" + "SAL_AMOUNT_GO").replaceAll("[^\\.0-9]", "");
  		       	item.put("SAL_AMOUNT_GO", sal_amount_go);
  		       	String sal_profit_go = request.getStringParam(ids[i] + "_" + "SAL_PROFIT_GO").replaceAll("[^\\.0-9]", "");
  		       	item.put("SAL_PROFIT_GO", sal_profit_go);
  		       	
  		       	if (Integer.parseInt(getItem("mg.ni.itoSalesStMaSelToModToDataExistSon", item).toString()) > 0) {
  		       	} else {
  		       		for (int aa = 1; aa < 13; aa++) {
  		       			item.put("FORCE_INPUT_MONTH", aa);
  		       			
  		       			String ordernum_head = "0";
  		       			
  		       			if(aa < 10) {
  		       				ordernum_head += aa;
  		       			} else {
  		       				ordernum_head = "";
  		       				ordernum_head += aa;
  		       			}
  		       			
  		       			item.put("ORDER_CD", ordernum_head + "00");
  		       			
  		       			createObject("mg.ni.itoSalesStMaInsertSon", item);
  		       		}
  		       	}
  		       	updateObject("mg.ni.itoSalesStMaUpdateSon", item);
  		       	
  		       	if (Integer.parseInt(getItem("mg.ni.itoSalesStMaSelToModToDataExistGwa", item).toString()) > 0) {
  		       	} else {
  		       		for (int aa = 1; aa < 13; aa++) {
  		       			item.put("FORCE_INPUT_MONTH", aa);
  		       			
  		       			String ordernum_head = "0";
  		       			
  		       			if(aa < 10) {
  		       				ordernum_head += aa;
  		       			} else {
  		       				ordernum_head = "";
  		       				ordernum_head += aa;
  		       			}
  		       			
  		       			item.put("ORDER_CD", ordernum_head + "00");
  		       			
  		       			createObject("mg.ni.itoSalesStMaInsertGwa", item);
  		       		}
  		       	}
  		       	updateObject("mg.ni.itoSalesStMaUpdateGwa", item);
  		       	
  		       	if (Integer.parseInt(getItem("mg.ni.itoSalesStMaSelToModToDataExistGo", item).toString()) > 0) {
  		       	} else {
  		       		for (int aa = 1; aa < 13; aa++) {
  		       			item.put("FORCE_INPUT_MONTH", aa);
  		       			
  		       			String ordernum_head = "0";
  		       			
  		       			if(aa < 10) {
  		       				ordernum_head += aa;
  		       			} else {
  		       				ordernum_head = "";
  		       				ordernum_head += aa;
  		       			}
  		       			
  		       			item.put("ORDER_CD", ordernum_head + "00");
  		       			
  		       			createObject("mg.ni.itoSalesStMaInsertGo", item);
  		       		}
  		       	}
  		       	updateObject("mg.ni.itoSalesStMaUpdateGo", item);
  	        }
  	        
  	        tx.commit();
  	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
  	        flag = "true";
          } catch (Exception e) {
          	tx.rollback();
          	log.error("------------ItoSalesStMaSave----------------------------------------------------------------\n" + e.toString());
          	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
  	        flag = "false";
          }
          String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
          return writeXml(ret);
      }
      
      /**
       * 투입현황 조회
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse itoInputCurSituSel(IRequest request) throws Exception 
       {
        	String col = request.getStringParam("grid_col_id");
        	String[] cols = col.split(",");
           Map map = null;
           GridData gdRes = new GridData();

           try {
           	Map smap = request.getMap();
   	        List lists;
   	        
 			String target_year = request.getStringParam("NOW_YEAR");
 			int next_year = Integer.parseInt(target_year)+1;
 			String select_type = request.getStringParam("SELECT_TYPE");
 			
   	        smap.put("TARGET_START_YEAR", target_year + "0101");
   	        smap.put("TARGET_END_YEAR", target_year + "1231");
   	        smap.put("TARGET_NEXT_START_YEAR", next_year + "0101");
   	        smap.put("TARGET_NEXT_END_YEAR", next_year + "1231");
	        smap.put("TARGET_YEAR", target_year);
	        String user_id = request.getStringParam("USER_ID");
	        if(!user_id.equals("all")) {
	        	smap.put("USER_ID", user_id);
	        	 if(select_type.equals("admit")) {
	        		lists = getList("mg.ni.itoInputCurSituSelSpeciAdm",smap);
		 	        for (int i = 0; i < lists.size(); i++) {
		 	         	map =(Map) lists.get(i);
		 	         	for(int j=0; j < cols.length; j++ ) {
		 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
		 	         	}
		 	        }
	        	 } else {
	        		lists = getList("mg.ni.itoInputCurSituSelSpeciCal",smap);
		 	        for (int i = 0; i < lists.size(); i++) {
		 	         	map =(Map) lists.get(i);
		 	         	for(int j=0; j < cols.length; j++ ) {
		 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
		 	         	}
		 	        }
	        	 }
	        } else {
	        	lists = getList("mg.ni.itoInputCurSituSelAll",smap);
	 	        for (int i = 0; i < lists.size(); i++) {
	 	         	map =(Map) lists.get(i);
	 	         	for(int j=0; j < cols.length; j++ ) {
	 	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	 	         	}
	 	        }
	        }
	        
   	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	   log.error("-----------------itoInputCurSituSel----------------------------------------------\n" + e.toString());
          	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
       }
       
       /**
        * 투입현황 수정화면 조회
        * @param request
        * @return
        * @throws Exception
        */
       private IResponse itoInputCurSituSelToMod(IRequest request) throws Exception 
       {
       	String col = request.getStringParam("grid_col_id");
        	String[] cols = col.split(",");
           Map map = null;
           GridData gdRes = new GridData();

           try {
           	Map smap = request.getMap();
   	        List lists;
   	        
   	        String target_year = request.getStringParam("NOW_YEAR");
   	        int next_year = Integer.parseInt(target_year)+1;
   	        smap.put("TARGET_START_YEAR", target_year + "0101");
   	        smap.put("TARGET_END_YEAR", target_year + "1231");
   	        smap.put("TARGET_NEXT_START_YEAR", next_year + "0101");
   	        smap.put("TARGET_NEXT_END_YEAR", next_year + "1231");
   	        
   	  	    lists = getList("mg.ni.itoInputCurSituSelToMod",smap);

   	        for (int i = 0; i < lists.size(); i++) {
   	         	map =(Map) lists.get(i);
   	         	for(int j=0; j < cols.length; j++ ) {
   	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
   	         	}
   	        }
   	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	   log.error("-----------------itoInputCurSituSelToMod----------------------------------------------\n" + e.toString());
          	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
       }
       
      
       /**
        * 투입현황 수정화면 저장
        * @param request
        * @return
        * @throws Exception
        */
       private IResponse itoInputCurSituSave(IRequest request) throws Exception 
       {
           String ids_info = request.getStringParam("ids");
           String cols_ids = request.getStringParam("col_ids");
           String[] cols = cols_ids.split(",");
           String[] ids = ids_info.split(",");
           String msg="", flag="";
          
           try {
           	tx.begin();
   	        Map item = null;
   	        for (int i = 0; i < ids.length; i++) {
   		       	item = new HashMap();
   		       	
   		        String target_year = request.getStringParam("NOW_YEAR");
   		       	item.put("TARGET_YEAR", target_year);
   		       	item.put("TARGET_START_YEAR", target_year + "0101");
   		       	item.put("TARGET_END_YEAR", target_year + "1231");
   		       	String manager = request.getStringParam(ids[i] + "_MANAGER");
   		       	String recommander = request.getStringParam(ids[i] + "_RECOMMANDER");
   		       	
	   		    String st_ymd = request.getStringParam(ids[i] + "_START_YMD").replaceAll("[^\\.0-9]", "");

	   		    int in_st_ymd = Integer.parseInt(st_ymd);
	   		    int target_year_jan = Integer.parseInt(target_year+"0101");
		   		int target_year_apr = Integer.parseInt(target_year+"0401");
		   		int target_year_jul = Integer.parseInt(target_year+"0701");
		   		int target_year_oct = Integer.parseInt(target_year+"1001");	   		    

   		       	for(int j=0; j < cols.length; j++ ) {
   		       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
   		       		
   			    	item.put(cols[j], tmps);
   		        }
   		       	
   		     if(in_st_ymd < target_year_jan) {
	   		    	item.put("RU_CD", "0000");
	   		    } else if(in_st_ymd < target_year_apr) {
	   		    	item.put("RU_CD", "0100");
	   		    } else if(in_st_ymd < target_year_jul) {
	   		    	item.put("RU_CD", "0200");
	   		    } else if(in_st_ymd < target_year_oct) {
	   		    	item.put("RU_CD", "0300");
	   		    } else {
	   		    	item.put("RU_CD", "0400");
	   		    }
   		       	
   		       	if(manager.equals(recommander)) {
		       		item.put("CLASSIFIC_CD", "1010");
		       	} else {
		       		item.put("CLASSIFIC_CD", "0505");
		       	}
   		       	
   		       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituDataExist", item).toString()) > 0) {
   		       		updateObject("mg.ni.itoInputCurSituUpdate", item);
   		       	} else {
   		       		createObject("mg.ni.itoInputCurSituInsert", item);
   		       	}
   		       	
   		       	
   		       	
   		       	//수정화면 저장할때 새로만든 테이블에 한꺼번에 저장
   		       	//아래처럼 기존 년,월 데이터 유무 확인 후 작업
   		       	//for문으로 1~12월 강제 루프
   		       	//투입인력, MM, 매출금액, 매출이익 계산 후 새테이블에 insert << 해당월인 사람만 구해서 투입인력으로 계산. 해당월 MM 구해서 금액, 이익 계산
   		       	//업데이트할때 금액 다시 읽어와서 업데이트
   	        
	   	     
	   	     
	   	    //인정구간=================================================================================================
         	
         	if(manager.equals(recommander)) {
	       		
         		item.put("RESULT_CLASSIFIC_CD", "1010");
         		
	       		if(manager.equals("bskosk")) {
		       		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistOwnAdmit", item).toString()) > 0) {
		     			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			updateObject("mg.ni.itoInputCurSituResultUpdateOwnAdmit", item);
		     			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertOwnAdmit", item);
			       		}
			       	}
	       		}
	       		
	       		if(manager.equals("s1024323")) {
	         		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistSonAdmit", item).toString()) > 0) {
	         			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;

				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateSonAdmit", item);
	         			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;

				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertSonAdmit", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("redeyesss")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGwaAdmit", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGwaAdmit", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertGwaAdmit", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("daeho")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGoAdmit", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGoAdmit", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertGoAdmit", item);
			       		}
			       	}
	       		}
	       	} else {
	       		item.put("RESULT_CLASSIFIC_CD", "1010");
	       		
	       		if(manager.equals("bskosk") || recommander.equals("bskosk")) {
		       		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistOwnAdmit", item).toString()) > 0) {
		     			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateOwnAdmit", item);
		     			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertOwnAdmit", item);
			       		}
			       	}
	       		}
	       		
	       		if(manager.equals("s1024323") || recommander.equals("s1024323")) {
	         		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistSonAdmit", item).toString()) > 0) {
	         			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateSonAdmit", item);
	         			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertSonAdmit", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("redeyesss") || recommander.equals("redeyesss")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGwaAdmit", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGwaAdmit", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertGwaAdmit", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("daeho") || recommander.equals("daeho")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGoAdmit", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGoAdmit", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableAdmit",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertGoAdmit", item);
			       		}
			       	}
	       		}
	       	}
         	
         	//정산구간========================================================================================================================
         	
         	if(manager.equals(recommander)) {
	       		
         		item.put("RESULT_CLASSIFIC_CD", "0505");
         		
	       		if(manager.equals("bskosk")) {
		       		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistOwnCalcu", item).toString()) > 0) {
		     			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateOwnCalcu", item);
		     			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertOwnCalcu", item);
			       		}
			       	}
	       		}
	       		
	       		if(manager.equals("s1024323")) {
	         		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistSonCalcu", item).toString()) > 0) {
	         			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateSonCalcu", item);
	         			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			createObject("mg.ni.itoInputCurSituResultInsertSonCalcu", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("redeyesss")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGwaCalcu", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGwaCalcu", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			createObject("mg.ni.itoInputCurSituResultInsertGwaCalcu", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("daeho")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGoCalcu", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGoCalcu", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			createObject("mg.ni.itoInputCurSituResultInsertGoCalcu", item);
			       		}
			       	}
	       		}
	       	} else {
	       		item.put("RESULT_CLASSIFIC_CD", "0505");
	       		if(manager.equals("bskosk") || recommander.equals("bskosk")) {
		       		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistOwnCalcu", item).toString()) > 0) {
		     			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateOwnCalcu", item);
		     			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "bskosk");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			createObject("mg.ni.itoInputCurSituResultInsertOwnCalcu", item);
			       		}
			       	}
	       		}
	       		
	       		if(manager.equals("s1024323") || recommander.equals("s1024323")) {
	         		if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistSonCalcu", item).toString()) > 0) {
	         			for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateSonCalcu", item);
	         			}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "s1024323");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			createObject("mg.ni.itoInputCurSituResultInsertSonCalcu", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("redeyesss") || recommander.equals("redeyesss")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGwaCalcu", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGwaCalcu", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "redeyesss");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			createObject("mg.ni.itoInputCurSituResultInsertGwaCalcu", item);
			       		}
			       	}
	       		}
			       	
	       		if(manager.equals("daeho") || recommander.equals("daeho")) {
			       	if (Integer.parseInt(getItem("mg.ni.itoInputCurSituSaveExistGoCalcu", item).toString()) > 0) {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));
			       			
			       			updateObject("mg.ni.itoInputCurSituResultUpdateGoCalcu", item);
			       		}
			       	} else {
			       		for (int aa = 1; aa < 13; aa++) {
			       			item.put("FORCE_INPUT_MONTH", aa);
			       			
			       			String ordernum_head = "0";
			       			
			       			if(aa < 10) {
			       				ordernum_head += aa;
			       			} else {
			       				ordernum_head = "";
			       				ordernum_head += aa;
			       			}
			       			
			       			item.put("ORDER_CD", ordernum_head + "00");
			       			item.put("THIS_YM", target_year + ordernum_head);
			       			item.put("THIS_START_YMD", target_year + ordernum_head + "01");
			       			item.put("USER_ID", "daeho");
			       			
			       			Map map = null;
			   	   	     	List lists;
			       			
				       		lists = getList("mg.ni.itoInputCurSituSelToResultTableCalcu",item);
				 	        
				          	map =(Map) lists.get(0);
				          	
				          	item.put("RESULT_SAL_AMOUNT", this.getStringWithNullCheck(map,"RESULT_SAL_AMOUNT"));
				          	item.put("RESULT_SALPROFIT", this.getStringWithNullCheck(map,"RESULT_SALPROFIT"));
				          	item.put("RESULT_MANPOWER", this.getStringWithNullCheck(map,"RESULT_MANPOWER"));
				          	item.put("RESULT_MANMONTH", this.getStringWithNullCheck(map,"RESULT_MANMONTH"));

			       			createObject("mg.ni.itoInputCurSituResultInsertGoCalcu", item);
			       		}
			       	}
	       		}
	       	}
		       
	        
   		       	
   		       	
   		       	
   	        }
   	        
   	        tx.commit();
   	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
   	        flag = "true";
           } catch (Exception e) {
           	tx.rollback();
           	log.error("------------itoInputCurSituSave----------------------------------------------------------------\n" + e.toString());
           	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
   	        flag = "false";
           }
           String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
           return writeXml(ret);
       }
       
       /**
        * 매출기준목표 조회화면 엑셀다운
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse itoTargetStSalSelToSelExcel(IRequest request) throws Exception 
        {
         	String col = request.getStringParam("grid_col_id");
         	String[] cols = col.split(",");
            Map map = null;
            GridData gdRes = new GridData();

            try {
            	// Excel Header 설정
    			IExcelManagement excel = (IExcelManagement) getComponent(IExcelManagement.class);
    			IExcelDocument doc = excel.createDocument("계약조회 리스트");
    			int[] col_width = new int[col.length()];
    			short mca = 0;
    			short mcb = 1;

    			doc.addTitleCell("순번");
    			doc.mergeCell(0, mca, 0, mcb);
    			col_width[0] = Integer.parseInt("51") * 40;
    			doc.addSpan(2, 5);
    			doc.addTitleCell("성명");
    			col_width[1] = Integer.parseInt("110") * 40;
    			doc.addTitleCell("직위");
    			col_width[2] = Integer.parseInt("45") * 40;
    			doc.addTitleCell("구분");
    			col_width[3] = Integer.parseInt("45") * 40;
    			doc.addTitleCell("프로젝트명");
    			col_width[4] = Integer.parseInt("270") * 40;
    			doc.addTitleCell("수행사");
    			col_width[5] = Integer.parseInt("270") * 40;
    			doc.addTitleCell("담당자");
    			col_width[6] = Integer.parseInt("110") * 40;
    			doc.addTitleCell("소싱");
    			col_width[7] = Integer.parseInt("110") * 40;
    			doc.addTitleCell("투입일");
    			col_width[8] = Integer.parseInt("159") * 40;
    			doc.addTitleCell("종료일");
    			col_width[9] = Integer.parseInt("159") * 40;
    			doc.addTitleCell("계약금액");
    			col_width[10] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("급여");
    			col_width[11] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("4대보험");
    			col_width[12] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("퇴직금");
    			col_width[13] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("비용 (월)");
    			col_width[14] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("출장비");
    			col_width[15] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("인센티브 (월)");
    			col_width[16] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("비용 (년)");
    			col_width[17] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("지급일");
    			col_width[18] = Integer.parseInt("100") * 40;
    			doc.addTitleCell("월 이익");
    			col_width[19] = Integer.parseInt("140") * 40;
//    			doc.addTitleCell("월 매출");
//    			col_width[19] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("월 비용");
    			col_width[20] = Integer.parseInt("140") * 40;

    			doc.setColumnsWidth(col_width);
    			
    			doc.nextRow();
            	
    			doc.addTitleCell("순번");
    			col_width[0] = Integer.parseInt("51") * 40;
    			doc.addTitleCell("성명");
    			col_width[1] = Integer.parseInt("110") * 40;
    			doc.addTitleCell("직위");
    			col_width[2] = Integer.parseInt("45") * 40;
    			doc.addTitleCell("구분");
    			col_width[3] = Integer.parseInt("45") * 40;
    			doc.addTitleCell("프로젝트명");
    			col_width[4] = Integer.parseInt("270") * 40;
    			doc.addTitleCell("수행사");
    			col_width[5] = Integer.parseInt("270") * 40;
    			doc.addTitleCell("담당자");
    			col_width[6] = Integer.parseInt("110") * 40;
    			doc.addTitleCell("소싱");
    			col_width[7] = Integer.parseInt("110") * 40;
    			doc.addTitleCell("투입일");
    			col_width[8] = Integer.parseInt("159") * 40;
    			doc.addTitleCell("종료일");
    			col_width[9] = Integer.parseInt("159") * 40;
    			doc.addTitleCell("계약금액");
    			col_width[10] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("급여");
    			col_width[11] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("4대보험");
    			col_width[12] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("퇴직금");
    			col_width[13] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("비용 (월)");
    			col_width[14] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("출장비");
    			col_width[15] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("인센티브 (월)");
    			col_width[16] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("비용 (년)");
    			col_width[17] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("지급일");
    			col_width[18] = Integer.parseInt("100") * 40;
    			doc.addTitleCell("월 이익");
    			col_width[19] = Integer.parseInt("140") * 40;
//    			doc.addTitleCell("월 매출");
//    			col_width[19] = Integer.parseInt("140") * 40;
    			doc.addTitleCell("월 비용");
    			col_width[20] = Integer.parseInt("140") * 40;
    			
            	Map smap = request.getMap();
    	        List lists;
    	        lists = getList("mg.ni.itoTargetStSalSelToSel",smap);
    	        
    	        int total_CONT_AMT = 0;
    			int CONT_AMTI = 0;
    			int total_PAY_PRC = 0;
    			int PAY_PRCI = 0;
    			int PAY_PRC_SI = 0;
    			int total_INSUR_PRC = 0;
    			int INSUR_PRCI = 0;
    			int total_RETIRE_PRC = 0;
    			int RETIRE_PRCI = 0;
    			int total_M_EXP_PRC = 0;
    			int M_EXP_PRCI = 0;
    			int total_TRIP_PRC = 0;
    			int TRIP_PRCI = 0;
    			int total_Y_INCEN_PRC = 0;
    			int Y_INCEN_PRCI = 0;
    			int total_Y_EXP_PRC = 0;
    			int Y_EXP_PRCI = 0;
    			int total_BENEFIT = 0;
    			int total_EXP_PRC = 0;
    	        
    	        for (int i = 0; i < lists.size(); i++) {
    	         	map =(Map) lists.get(i);

    	         	doc.nextRow();
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_NM"), CellStyleFactory.ALIGN_CENTER);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "MAN_NM"), CellStyleFactory.ALIGN_LEFT);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "D"), CellStyleFactory.ALIGN_LEFT);
    				String CONT_CL_CD = this.getStringWithNullCheck(map, "CONT_CL_CD");
    				if (CONT_CL_CD.equals("1")) {
    					CONT_CL_CD = "자사";
    				} else if (CONT_CL_CD.equals("2")) {
    					CONT_CL_CD = "프리";
    				} else if (CONT_CL_CD.equals("3")) {
    					CONT_CL_CD = "외주";
    				}
    				doc.addAlignedCell(CONT_CL_CD, CellStyleFactory.ALIGN_CENTER);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "PROJ_NM"), CellStyleFactory.ALIGN_LEFT);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "COMP_NM"), CellStyleFactory.ALIGN_LEFT);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "USER_ID"), CellStyleFactory.ALIGN_LEFT);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "RECOMMENDER"), CellStyleFactory.ALIGN_LEFT);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "YMD"), CellStyleFactory.ALIGN_CENTER);
    				doc.addAlignedCell(this.getStringWithNullCheck(map, "ENDYMD"), CellStyleFactory.ALIGN_CENTER);

    				String CONT_AMT = this.getStringWithNullCheck(map, "CONT_AMT");
    				// CONT_AMT = CONT_AMT.substring(0, CONT_AMT.length()-5);
    				CONT_AMTI = Integer.parseInt(CONT_AMT);
    				doc.addNumericCell(CONT_AMTI);
    				total_CONT_AMT += Integer.parseInt(CONT_AMT);

    				String PAY_PRC = this.getStringWithNullCheck(map, "PAY_PRC");
    				String PAY_PRC_S = this.getStringWithNullCheck(map, "PAY_PRC_S");
    				// PAY_PRC = PAY_PRC.substring(0, PAY_PRC.length()-5);
    				PAY_PRCI = Integer.parseInt(PAY_PRC);
    				PAY_PRC_SI = Integer.parseInt(PAY_PRC_S);
    				
    				if(PAY_PRC_SI == 0) {
    				doc.addNumericCell(PAY_PRCI);
    				} else {
    					doc.addNumericCell(PAY_PRC_SI);
    				}
    				
    				if(PAY_PRC_SI == 0) {
    				total_PAY_PRC += PAY_PRCI;
    				} else {
    					total_PAY_PRC += PAY_PRC_SI;
    				}
    				
    				
    				String INSUR_PRC = this.getStringWithNullCheck(map, "INSUR_PRC");
    				// INSUR_PRC = INSUR_PRC.substring(0, INSUR_PRC.length()-5);
    				INSUR_PRCI = Integer.parseInt(INSUR_PRC);
    				doc.addNumericCell(INSUR_PRCI);
    				total_INSUR_PRC += Integer.parseInt(INSUR_PRC);

    				String RETIRE_PRC = this.getStringWithNullCheck(map, "RETIRE_PRC");
    				// RETIRE_PRC = RETIRE_PRC.substring(0, RETIRE_PRC.length()-5);
    				RETIRE_PRCI = Integer.parseInt(RETIRE_PRC);
    				doc.addNumericCell(RETIRE_PRCI);
    				total_RETIRE_PRC += Integer.parseInt(RETIRE_PRC);

    				String M_EXP_PRC = this.getStringWithNullCheck(map, "M_EXP_PRC");
    				// M_EXP_PRC = M_EXP_PRC.substring(0, M_EXP_PRC.length()-5);
    				M_EXP_PRCI = Integer.parseInt(M_EXP_PRC);
    				doc.addNumericCell(M_EXP_PRCI);
    				total_M_EXP_PRC += Integer.parseInt(M_EXP_PRC);

    				String TRIP_PRC = this.getStringWithNullCheck(map, "TRIP_PRC");
    				// TRIP_PRC = TRIP_PRC.substring(0, TRIP_PRC.length()-5);
    				TRIP_PRCI = Integer.parseInt(TRIP_PRC);
    				doc.addNumericCell(TRIP_PRCI);
    				total_TRIP_PRC += Integer.parseInt(TRIP_PRC);

    				String Y_INCEN_PRC = this.getStringWithNullCheck(map, "Y_INCEN_PRC");
    				// Y_INCEN_PRC = Y_INCEN_PRC.substring(0, Y_INCEN_PRC.length()-5);
    				Y_INCEN_PRCI = Integer.parseInt(Y_INCEN_PRC);
    				doc.addNumericCell(Y_INCEN_PRCI);
    				total_Y_INCEN_PRC += Integer.parseInt(Y_INCEN_PRC);

    				String Y_EXP_PRC = this.getStringWithNullCheck(map, "Y_EXP_PRC");
    				// Y_EXP_PRC = Y_EXP_PRC.substring(0, Y_EXP_PRC.length()-5);
    				Y_EXP_PRCI = Integer.parseInt(Y_EXP_PRC);
    				doc.addNumericCell(Y_EXP_PRCI);
    				total_Y_EXP_PRC += Integer.parseInt(Y_EXP_PRC);

    				String PAY_CL_CD = this.getStringWithNullCheck(map, "PAY_CL_CD");
    				if (PAY_CL_CD.equals("T")) {
    					PAY_CL_CD = "당월";
    				} else if (PAY_CL_CD.equals("N")) {
    					PAY_CL_CD = "익월";
    				}
    				String PAY_DAY = this.getStringWithNullCheck(map, "PAY_DAY");
    				String PP = PAY_CL_CD + " " + PAY_DAY;
    				doc.addAlignedCell(PP, CellStyleFactory.ALIGN_CENTER);

    				int BENEFIT = CONT_AMTI - (PAY_PRCI + PAY_PRC_SI + INSUR_PRCI + RETIRE_PRCI + M_EXP_PRCI + TRIP_PRCI + Y_INCEN_PRCI
    						+ (Y_EXP_PRCI / 12));
    				int EXP_PRC = PAY_PRCI + PAY_PRC_SI + INSUR_PRCI + RETIRE_PRCI + M_EXP_PRCI + TRIP_PRCI + Y_INCEN_PRCI
    						+ (Y_EXP_PRCI / 12);
    				doc.addNumericCell(BENEFIT);
//    				doc.addNumericCell(CONT_AMTI);
    				doc.addNumericCell(EXP_PRC);
    				total_BENEFIT += BENEFIT;
    				total_EXP_PRC += EXP_PRC;
    	        }
    	        
    	        doc.nextRow();
    			doc.addSpan(1, 8);
    			doc.addAlignedCell(("합계"), CellStyleFactory.ALIGN_CENTER);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addNumericCell(total_CONT_AMT);
    			doc.addNumericCell(total_PAY_PRC);
    			doc.addNumericCell(total_INSUR_PRC);
    			doc.addNumericCell(total_RETIRE_PRC);
    			doc.addNumericCell(total_M_EXP_PRC);
    			doc.addNumericCell(total_TRIP_PRC);
    			doc.addNumericCell(total_Y_INCEN_PRC);
    			doc.addNumericCell(total_Y_EXP_PRC);
    			doc.addAlignedCell((""), CellStyleFactory.ALIGN_LEFT);
    			doc.addNumericCell(total_BENEFIT);
//    			doc.addNumericCell(total_CONT_AMT);
    			doc.addNumericCell(total_EXP_PRC);
    			
    	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
            } catch(Exception e) {
           	   log.error("-----------------itoTargetStSalSelToSel----------------------------------------------\n" + e.toString());
           	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
            }
        }
}