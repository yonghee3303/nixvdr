package mg.ni;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class projAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("projSelect")) {   /**프로젝트 조회*/
            return projSelect(request);
        } else if(request.isMode("searchProjId")) {	/**프로젝트 ID주면 이름 리턴*/
        	return searchProjId(request);
        } else if(request.isMode("saveNewProj")) {	/**프로젝트 신규 저장*/
        	return saveNewProj(request);
        } else if(request.isMode("saveNewProjMi")) {	/**프로젝트 신규 저장 - 마이그레이션*/
        	return saveNewProjMi(request);
        } else if(request.isMode("saveProj")) {   /**프로젝트 저장 */
            return saveProj(request);
        } else if(request.isMode("deleteProj")) {   /**프로젝트 삭제 */
            return deleteProj(request);
        } else if(request.isMode("compPop")) {   /**수행사 팝업 */
            return compPop(request);
        } else if(request.isMode("reqMemList")) {   /**필요인원 조회 */
            return reqMemList(request);
        } else if(request.isMode("saveReqMemList")) {   /**필요인원 저장 */
            return saveReqMemList(request);
        } else if(request.isMode("deleteReqMemList")) {   /**필요인원 삭제 */
            return deleteReqMemList(request);
        } else if(request.isMode("selectMainProProj")) {   /**메인 진행중 프로젝트 조회 */
            return selectMainProProj(request);
        } else if(request.isMode("selectMainContract")) {   /**메인 계약대상인력 조회 */
            return selectMainContract(request);
        } else if(request.isMode("selectMainInterview")) {   /**메인 인재근황 조회 */
            return selectMainInterview(request);
        }
        else { 
            return write(null);
        } 
    }  

	/**
    * 프로젝트 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse projSelect(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.projSelect_"+dbName,smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    public String getRanIdNum() {
    	int randomNum = (int)(Math.floor((Math.random() * 89999)+10000));
    	
		String ranNum = Integer.toString(randomNum);
		
		return ranNum;
	}

	public String getTodayNum() {
		DecimalFormat df = new DecimalFormat("00");
		  Calendar currentCal = Calendar.getInstance();
		  
		  currentCal.add(currentCal.DATE, 0);
		  
		  String year = Integer.toString(currentCal.get(Calendar.YEAR));
		  String month = df.format(currentCal.get(Calendar.MONTH)+1);
		  String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));
		
		
		String todayNum = year + month + day;
		
		return todayNum;
	}
	
	/**
     * 프로젝트명 주고 ID 리턴
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse searchProjId(IRequest request) throws Exception 
    {
    String msg="", flag="";
    String dbName = pm.getString("component.sql.database");
    JSONObject obj = new JSONObject();
	    try {
	    	tx.begin();
	        Map item = null;
	        
		    item = new HashMap();
		
		   	String proj_nm = request.getStringParam("PROJ_NM");
		   	
		   	item.put("PROJ_NM", proj_nm);
		    
			String getProj_id = getItem("mg.ni.searchProjId", item).toString();
			
		    tx.commit();
		    obj.put("ID", getProj_id);   
		    obj.put("errcode", "0");
		    obj.put("errmsg", "success");
		    return write(obj.toString());
		} catch (Exception e) {
			tx.rollback();
			log.info(e);
			log.info("ID를 찾을수 없습니다");
			obj.put("ID", " ");  
			obj.put("errcode", "-100");
		    obj.put("errmsg", "ID를 찾을수 없습니다");
			return write(obj.toString());
		}
	}
    
    /**
     * 프로젝트 신규 저장 마이그레이션
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveNewProjMi(IRequest request) throws Exception 
    {
    	String ranIdNum		= getRanIdNum();
    	String todayNum		= getTodayNum();
    	String ranId			= todayNum + ranIdNum;
    	String mRanId			= "P" + ranId;
    	log.info(mRanId);
    	String ranResult = mRanId.substring(0, 14); 
    	
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        JSONObject obj = new JSONObject();
        try {
        	tx.begin();
	        Map item = null;
	       	item = new HashMap();

	       	String proj_no = request.getStringParam("PROJ_NO");
	       	String proj_nm = request.getStringParam("PROJ_NM");
	       	String comp_id = request.getStringParam("COMP_ID");
	       	String start_ymd = request.getStringParam("START_YMD");
	       	String end_ymd = request.getStringParam("END_YMD");
	       	String pj_user_nm = request.getStringParam("PJ_USER_NM");
	       	String pj_tel_no = request.getStringParam("PJ_TEL_NO");
	       	String indus_cl_cd = request.getStringParam("INDUS_CL_CD");
	       	String proj_cl_cd = request.getStringParam("PROJ_CL_CD");
	       	String essen_tech_desc = request.getStringParam("ESSEN_TECH_DESC");
	       	String pj_remark = request.getStringParam("PJ_REMARK");
	       	String order_ymd = request.getStringParam("ORDER_YMD");
	       	String pj_etc = request.getStringParam("PJ_ETC");
	       	String pj_cl_cd = request.getStringParam("PJ_CL_CD");
	       	String pj_area_cd = request.getStringParam("PJ_AREA_CD");
	       	String pj_area_sub = request.getStringParam("PJ_AREA_SUB");
	       	String user_id = request.getStringParam("USER_ID");

	       	item.put("PROJ_NO", ranResult);
	       	item.put("PROJ_NM", proj_nm);
	       	item.put("COMP_ID", comp_id);
	       	item.put("START_YMD", start_ymd);
	       	item.put("END_YMD", end_ymd);
	       	item.put("PJ_USER_NM", pj_user_nm);
	       	item.put("PJ_TEL_NO", pj_tel_no);
	       	item.put("INDUS_CL_CD", indus_cl_cd);
	       	item.put("PROJ_CL_CD", proj_cl_cd);
	       	item.put("ESSEN_TECH_DESC", essen_tech_desc);
	       	item.put("PJ_REMARK", pj_remark);
	       	item.put("ORDER_YMD", order_ymd);
	       	item.put("PJ_ETC", pj_etc);
	       	item.put("PJ_CL_CD", pj_cl_cd);
	       	item.put("PJ_AREA_CD", pj_area_cd);
	       	item.put("PJ_AREA_SUB", pj_area_sub);
	       	item.put("USER_ID", user_id);
	       	
	       	item.put("LOGIN_ID",  request.getUser().getId());
	       	log.info("새프로젝트 생성");
	       	createObject("mg.ni.projNewInsert_"+dbName, item); 

	        tx.commit();
	        obj.put("ID", proj_nm);   
		    obj.put("errcode", "0");
		    obj.put("errmsg", "success");
		    return write(obj.toString());
        } catch (Exception e) {
        	tx.rollback();
        	log.info("생성실패");
        	log.info(e);
        	obj.put("ID", " "); 
			obj.put("errcode", "-100");
		    obj.put("errmsg", "프로젝트 생성 실패");
		    return write(obj.toString());
        }
    }
    
    /**
     * 프로젝트 신규 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveNewProj(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);     
		        }
		       	
		       	if(request.getStringParam(ids[i] + "_PROJ_IMP").equals("")||request.getStringParam(ids[i] + "_PROJ_IMP").equals(null)) {
		       		item.put("PROJ_IMP", 0);
	       		}
		    	
		    	if(request.getStringParam(ids[i] + "_PROJ_EMER").equals("")||request.getStringParam(ids[i] + "_PROJ_EMER").equals(null)) {
		    		item.put("PROJ_EMER", 0);
	       		}
		       	
		       	item.put("LOGIN_ID",  request.getUser().getId());
		       	log.info("새프로젝트 생성");
		       	createObject("mg.ni.projNewInsert_"+dbName, item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
   
    /**
     * 프로젝트 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveProj(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
	       	for(int j=0; j < cols.length; j++ ) {
	       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
	       		
		    	item.put(cols[j], tmps);    
	        }
	       	
	       	if(request.getStringParam(ids[i] + "_PROJ_IMP").equals("")||request.getStringParam(ids[i] + "_PROJ_IMP").equals(null)) {
	       		item.put("PROJ_IMP", 0);
       		}
	    	
	    	if(request.getStringParam(ids[i] + "_PROJ_EMER").equals("")||request.getStringParam(ids[i] + "_PROJ_EMER").equals(null)) {
	    		item.put("PROJ_EMER", 0);
       		}
	       	
	       	item.put("LOGIN_ID",  request.getUser().getId());
			log.info("기존프로젝트 업데이트");
			updateObject("mg.ni.projUpdate_"+dbName, item);
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }

    /**
     * 프로젝트 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteProj(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
	       		for(int j=0; j < cols.length; j++ ) {
	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		        item.put(cols[j], tmps);     
	         	} 
	       	deleteObject("mg.ni.projDelete", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    private IResponse compPop(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.compSelectPop",smap);
	        for (int i = 0; i < lists.size(); i++) {
	        map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         	gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    }
    
    /**
     * 필요인원 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse reqMemList(IRequest request) throws Exception 
     {
      	String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
         
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
 	    List lists;

 	    lists = getList("mg.ni.selectReqMemList_"+dbName,smap);

 	    for (int i = 0; i < lists.size(); i++) {
 	        map =(Map) lists.get(i);
 	         	
 	        for(int j=0; j < cols.length; j++ ) {
 	        	gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	        }
 	    }   
 	    return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
     } 
     
     /**
      * 필요인원 저장
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse saveReqMemList(IRequest request) throws Exception 
     {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
	        	tx.begin();
	        	Map item = null;
	        	for (int i = 0; i < ids.length; i++) {
		 	       	item = new HashMap();
		 	       	 
		 	       	for(int j=0; j < cols.length; j++ ) {
			 	       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			 		    item.put(cols[j], tmps);     
			 	    }
			
			 	    if( Integer.parseInt(getItem("mg.ni.countReqMem", item).toString() ) > 0 ) {
			 	        log.info("기존필요인원 업데이트");
			 			updateObject("mg.ni.updateReqMem_"+dbName, item);
			 		} else {
			 			log.info("새필요인원 생성");
			 			createObject("mg.ni.insertReqMem_"+dbName, item); 
			 	    }
	        	}
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
 	        flag = "false";
         }
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }
     
     /**
      * 필요인원 삭제
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse deleteReqMemList(IRequest request) throws Exception 
     {
         String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
          
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	        	item = new HashMap();
 	       	 
 	       	 	for(int j=0; j < cols.length; j++ ) {
 	       	 		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		            item.put(cols[j], tmps);     
 	         	} 
 	       	 	deleteObject("mg.ni.deleteReqMem", item); 
 	        }
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 	        flag = "false";
         }
                 
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     }
     
     /**
      * 메인 진행중 프로젝트 조회
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse selectMainProProj(IRequest request) throws Exception 
      {
       	String col = request.getStringParam("grid_col_id");
       	String[] cols = col.split(",");
          Map map = null;
          GridData gdRes = new GridData();
          String dbName = pm.getString("component.sql.database");
          try {
          	Map smap = request.getMap();
          	smap.put("dbName", pm.getString("component.sql.database"));
          	smap.put("LOGIN_ID",  request.getUser().getId());
  	        List lists;
  	        lists = getList("mg.ni.selectMainProProj_"+dbName,smap);
  	        for (int i = 0; i < lists.size(); i++) {
  	         	map =(Map) lists.get(i);
  	         	for(int j=0; j < cols.length; j++ ) {
  	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	         	}
  	        }   
  	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	   log.error("---------------------------------------------------------------\n" + e.toString());
         	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
      } 
      
      /**
       * 메인 계약대상인력 조회
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse selectMainContract(IRequest request) throws Exception 
       {
        	String col = request.getStringParam("grid_col_id");
        	String[] cols = col.split(",");
           Map map = null;
           GridData gdRes = new GridData();
           String dbName = pm.getString("component.sql.database");
           try {
           	Map smap = request.getMap();
           	smap.put("dbName", pm.getString("component.sql.database"));
           	smap.put("LOGIN_ID",  request.getUser().getId());
   	        List lists;
   	        lists = getList("mg.ni.selectMainContract_"+dbName,smap);
   	        for (int i = 0; i < lists.size(); i++) {
   	         	map =(Map) lists.get(i);
   	         	for(int j=0; j < cols.length; j++ ) {
   	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
   	         	}
   	        }   
   	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
           } catch(Exception e) {
          	   log.error("---------------------------------------------------------------\n" + e.toString());
          	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
           }
       } 
       
       /**
        * 메인 인재근황 조회
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse selectMainInterview(IRequest request) throws Exception 
        {
         	String col = request.getStringParam("grid_col_id");
         	String[] cols = col.split(",");
            Map map = null;
            GridData gdRes = new GridData();
            String dbName = pm.getString("component.sql.database");
            try {
            	Map smap = request.getMap();
            	smap.put("dbName", pm.getString("component.sql.database"));
            	smap.put("LOGIN_ID",  request.getUser().getId());
    	        List lists;
    	        lists = getList("mg.ni.selectMainInterview_"+dbName,smap);
    	        for (int i = 0; i < lists.size(); i++) {
    	         	map =(Map) lists.get(i);
    	         	for(int j=0; j < cols.length; j++ ) {
    	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
    	         	}
    	        }   
    	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
            } catch(Exception e) {
           	   log.error("---------------------------------------------------------------\n" + e.toString());
           	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
            }
        } 
  
}