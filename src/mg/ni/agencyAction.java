package mg.ni;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class agencyAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("agencySelectUser")) {   /** 조회    */
            return agencySelectUser(request);                           
        } else if(request.isMode("saveNewAgency")) {	/**신규 저장	*/
        	return saveNewAgency(request);
        } else if(request.isMode("saveAgency")) {   /** 저장 */
            return saveAgency(request);                                                   
        } else if(request.isMode("deleteAgency")) {   /** 삭제 */
            return deleteAgency(request);    
        } else if(request.isMode("agencyPop")) {   /** 소속사팝업 */
            return agencyPop(request);   
        }
        else { 
            return write(null);
        } 
    }  

	/**
    * 사용자 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse agencySelectUser(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.agencySelectUser_"+dbName,smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    /**
     * 새로저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveNewAgency(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);     
		        }
	       	item.put("LOGIN_ID",  request.getUser().getId());
	       	log.info("새유저 생성");
	       	createObject("mg.ni.agencyInsertUser_"+dbName, item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
   
    /**
     * 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveAgency(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
	       	for(int j=0; j < cols.length; j++ ) {
	       		String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		    	item.put(cols[j], tmps);     
	        }
	       	item.put("LOGIN_ID",  request.getUser().getId());
			log.info("기존유저 업데이트");
			updateObject("mg.ni.agencyUpdateUser_"+dbName, item);
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }

    /**
     * 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteAgency(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
         
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
	       		for(int j=0; j < cols.length; j++ ) {
	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		        item.put(cols[j], tmps);     
	         	} 
	       	deleteObject("mg.ni.agencyDeleteUser", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    
    private IResponse agencyPop(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.agencySelectPop",smap);
	        for (int i = 0; i < lists.size(); i++) {
	        map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         	gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        } 
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    
  
}