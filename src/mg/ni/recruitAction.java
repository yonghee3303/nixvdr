package mg.ni;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class recruitAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("recruitSelect")) {   /**지원자 조회*/
            return recruitSelect(request);
        } else if(request.isMode("addRecMan")) {	/**추천인력 저장*/
        	return addRecMan(request);
        } else if(request.isMode("selectManList")) {   /**선택인력 조회 */
            return selectManList(request);
        } else if(request.isMode("saveManList")) {   /**선택인력 저장 */
            return saveManList(request);
        } else if(request.isMode("deleteManList")) {   /**선택인력 삭제 */
            return deleteManList(request);
        } else if(request.isMode("selectRecManList")) {   /**추천인력 조회 */
            return selectRecManList(request);
        } else if(request.isMode("projectPop")) {   /**프로젝트 팝업 조회 */
            return projectPop(request);
        } else if(request.isMode("manSearchPop")) {   /**추천인력 검색조회 */
            return manSearchPop(request);
        } else if(request.isMode("confirmManList")) {   /**선택인력 확정 */
            return confirmManList(request);
        }
        else { 
            return write(null);
        } 
    }  

	/**
    * 지원자 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse recruitSelect(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.recruitSelect_"+dbName,smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    /**
     * 추천인력 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse addRecMan(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);     
		        }
	       	item.put("LOGIN_ID",  request.getUser().getId());
		       	if( Integer.parseInt(getItem("mg.ni.selectAddRecMan", item).toString() ) > 0 ) {
		       	//	log.info("인력이 이미 프로젝트에 들어가 있음");
		       		updateObject("mg.ni.updateRecMan_"+dbName, item);
		       		flag = "true";
			       	msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		       	//	flag = "truee";
		       	//	msg = getItem("mg.ni.selectAddRecManProj",item).toString();
		       	} else {
		       		log.info("추천인력 저장");
			       	createObject("mg.ni.addRecMan_"+dbName, item); 
			       	flag = "true";
			       	msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
		       	}
	        }
	        tx.commit();
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
   
    /**
     * 선택인력 조회
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse selectManList(IRequest request) throws Exception 
    {
    	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.selectManList_"+dbName,smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    }

    /**
     * 선택인력 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveManList(IRequest request) throws Exception 
    {
    	String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);     
		        }
	       	item.put("LOGIN_ID",  request.getUser().getId());
	       	log.info("선택인력 저장");
	       	createObject("mg.ni.saveManList_"+dbName, item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    /**
     * 선택인력 삭제
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse deleteManList(IRequest request) throws Exception 
     {
    	 String ids_info = request.getStringParam("ids");
         String cols_ids = request.getStringParam("col_ids");
         String[] cols = cols_ids.split(",");
         String[] ids = ids_info.split(",");
         String msg="", flag="";
          
         try {
         	tx.begin();
 	        Map item = null;
 	        for (int i = 0; i < ids.length; i++) {
 	       	item = new HashMap();
 	       		for(int j=0; j < cols.length; j++ ) {
 	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
 		        item.put(cols[j], tmps);     
 	         	} 
 	       	updateObject("mg.ni.editsaveManList", item);
 	       	deleteObject("mg.ni.deleteManList", item); 
 	        }
 	        tx.commit();
 	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
 	        flag = "true";
         } catch (Exception e) {
         	tx.rollback();
         	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
         	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
 	        flag = "false";
         }
         String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
         return writeXml(ret);
     } 
     
     /**
      * 추천인력 조회
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse selectRecManList(IRequest request) throws Exception 
     {
    	String col = request.getStringParam("grid_col_id");
    	String proj_no = request.getStringParam("PROJ_NO");
    	
       	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        Map smap = request.getMap();
        smap.put("dbName", pm.getString("component.sql.database"));
        smap.put("PROJ_NO", proj_no);
  	    List lists;
  	    lists = getList("mg.ni.selectRecManList_"+dbName,smap);

  	    for (int i = 0; i < lists.size(); i++) {
  	        map =(Map) lists.get(i);
  	         	
  	        for(int j=0; j < cols.length; j++ ) {
  	        	gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	        }
  	    }   
  	    return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
     }
     
     /**
      * 프로젝트 팝업 조회
      * @param request
      * @return
      * @throws Exception
      */
     private IResponse projectPop(IRequest request) throws Exception 
     {
    	 String col = request.getStringParam("grid_col_id");
      	String[] cols = col.split(",");
         Map map = null;
         GridData gdRes = new GridData();
         String dbName = pm.getString("component.sql.database");
         try {
         	Map smap = request.getMap();
         	smap.put("dbName", pm.getString("component.sql.database"));
 	        List lists;
 	        lists = getList("mg.ni.projectPop",smap);
 	        for (int i = 0; i < lists.size(); i++) {
 	        map =(Map) lists.get(i);
 	         	for(int j=0; j < cols.length; j++ ) {
 	         	gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
 	         	}
 	        } 
 	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
         } catch(Exception e) {
        	   log.error("---------------------------------------------------------------\n" + e.toString());
        	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
         }
     }
     
     /**
      * 추천인력 검색조회
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse manSearchPop(IRequest request) throws Exception 
      {
       	String col = request.getStringParam("grid_col_id");
       	String[] cols = col.split(",");
          Map map = null;
          GridData gdRes = new GridData();
          String dbName = pm.getString("component.sql.database");
          try {
          	Map smap = request.getMap();
          	
/*          	System.out.println("================smap==============");
          	System.out.println(smap);*/
          	
          	String strTech = request.getStringParam("TECH_CL_CD");
          	if(strTech!="") {
          		String[] strT = strTech.split(",");
	          	//System.out.println(strS);
	          	          	
	          	smap.put("dbName", pm.getString("component.sql.database"));
	          	if(strT.length > 0) {
	          	smap.put("TECH_CL_CD", strT);
	          	}
          	}
          	String strRole = request.getStringParam("ROLE_CL_CD");
          	if(strRole!="") {
          		String[] strR = strRole.split(",");
	          	//System.out.println(strS);
	          	          	
	          	smap.put("dbName", pm.getString("component.sql.database"));
	          	if(strR.length > 0) {
	          	smap.put("ROLE_CL_CD", strR);
	          	}
          	}          	
          	
  	        List lists;
  	        lists = getList("mg.ni.manSearchPopSelect_"+dbName,smap);
  	        for (int i = 0; i < lists.size(); i++) {
  	         	map =(Map) lists.get(i);
  	         	for(int j=0; j < cols.length; j++ ) {
  	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
  	         	}
  	        }   
  	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
          } catch(Exception e) {
         	   log.error("---------------------------------------------------------------\n" + e.toString());
         	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
          }
      } 
      
      
      public String getRanIdNum() {
      	int randomNum = (int)(Math.floor((Math.random() * 89999)+10000));
      	
  		String ranNum = Integer.toString(randomNum);
  		
  		return ranNum;
  	}

  	public String getTodayNum() {
  		DecimalFormat df = new DecimalFormat("00");
  		  Calendar currentCal = Calendar.getInstance();
  		  
  		  currentCal.add(currentCal.DATE, 0);
  		  
  		  String year = Integer.toString(currentCal.get(Calendar.YEAR));
  		  String month = df.format(currentCal.get(Calendar.MONTH)+1);
  		  String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));
  		
  		
  		String todayNum = year + month + day;
  		
  		return todayNum;
  	}
      
      /**
       * 선택인력 확정
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse confirmManList(IRequest request) throws Exception 
      {
	      String ids_info = request.getStringParam("ids");
          String cols_ids = request.getStringParam("col_ids");
          String[] cols = cols_ids.split(",");
          String[] ids = ids_info.split(",");
          String msg="", flag="";
          String dbName = pm.getString("component.sql.database");
          try {
          	tx.begin();
  	        Map item = null;
  	        for (int i = 0; i < ids.length; i++) {
  	       	item = new HashMap();
  		       	for(int j=0; j < cols.length; j++ ) {
  		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
  			    item.put(cols[j], tmps);     
  		        }
  		       		/*String proj_no = getItem("mg.ni.searchManId", item).toString();*/
		  		      String ranIdNum		= getRanIdNum();
		  		      String todayNum		= getTodayNum();
		  		      String ranId			= todayNum + ranIdNum;
		  		      String mRanId			= "T" + ranId;
		  		      log.info(mRanId);
  		       		String ranResult = mRanId.substring(0, 14);
		       		String cont_rno = request.getStringParam("CONT_RNO");
		       		item.put("CONT_RNO", ranResult);
		       		item.put("LOGIN_ID",  request.getUser().getId());
		       		//if( Integer.parseInt(getItem("mg.ni.selectOverlap", item).toString() ) == 0 ) {
		       			log.info("생성");
		       			createObject("mg.ni.confirmManList_"+dbName, item);
		       			updateObject("mg.ni.contractSaveToMan_"+dbName, item);
		       			updateObject("mg.ni.confirmsaveManList_"+dbName, item);
		       			flag = "true";
		       		//} else {
		       		//	log.info("이미 계약관리에 등록된 인원");
		       		//	flag = "truee";
		       		//}
  	        }
  	        tx.commit();
  	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
  	        
          } catch (Exception e) {
          	tx.rollback();
          	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
          	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
  	        flag = "false";
          }
          String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
          return writeXml(ret);
      }
  
}