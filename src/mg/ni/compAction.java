package mg.ni;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.core.base.ComponentRegistry;
import com.core.component.excel.IExcelDocument;
import com.core.component.excel.IExcelManagement;
import com.core.component.excel.internal.CellStyleFactory;
import com.core.component.mail.IMailManagement;
import com.core.component.util.DateUtils;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;
import com.core.component.crypto.ICryptoManagement;

import mg.base.BaseAction;
import mg.base.GridData;
import mg.common.CpcConstants;
import org.json.XML; // java-json-schema.jar
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class compAction extends BaseAction {
    /**
     * 사용자 관리
     * @param request
     * @return
     * @throws Exception
     */
	
	private ICryptoManagement crypto = (ICryptoManagement) getComponent(ICryptoManagement.class);
	
    @Override
	public IResponse run(IRequest request) throws Exception {
          
        if(request.isMode("compSelectUser")) {   /** 조회    */
            return compSelectUser(request);                           
        } else if(request.isMode("saveNewComp")) {	/**신규 저장	*/
        	return saveNewComp(request);
        } else if(request.isMode("searchCompId")) {	/**수행사명을 주면 수행사 아이디 리턴	*/
        	return searchCompId(request);
        } else if(request.isMode("saveComp")) {   /** 저장 */
            return saveComp(request);                                                   
        } else if(request.isMode("deleteComp")) {   /** 삭제 */
            return deleteComp(request);                                                     
        }
        else { 
            return write(null);
        } 
    }  

	/**
    * 사용자 조회
    * @param request
    * @return
    * @throws Exception
    */
    private IResponse compSelectUser(IRequest request) throws Exception 
    {
     	String col = request.getStringParam("grid_col_id");
     	String[] cols = col.split(",");
        Map map = null;
        GridData gdRes = new GridData();
        String dbName = pm.getString("component.sql.database");
        
        try {
        	Map smap = request.getMap();
        	smap.put("dbName", pm.getString("component.sql.database"));
	        List lists;
	        lists = getList("mg.ni.compSelectUser_"+dbName,smap);
	        for (int i = 0; i < lists.size(); i++) {
	         	map =(Map) lists.get(i);
	         	for(int j=0; j < cols.length; j++ ) {
	         		gdRes.addValue(cols[j], this.getStringWithNullCheck(map,cols[j]) );
	         	}
	        }   
	        return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas())); 
        } catch(Exception e) {
       	   log.error("---------------------------------------------------------------\n" + e.toString());
       	   return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()),"false", "doQuery", gdRes.getGridXmlDatas()));
        }
    } 
    
    /**
     * 새로저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveNewComp(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	 for(int j=0; j < cols.length; j++ ) {
		       		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			         item.put(cols[j], tmps);     
		         }
	       	item.put("LOGIN_ID",  request.getUser().getId());
	       	log.info("새유저 생성");
			createObject("mg.ni.compInsertUser_"+dbName, item); 	    
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
    
    
    
    /**
     * 수행사명 주고 ID 리턴
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse searchCompId(IRequest request) throws Exception 
    {
    String msg="", flag="";
    String dbName = pm.getString("component.sql.database");
    JSONObject obj = new JSONObject();
	    try {
	    	tx.begin();
	        Map item = null;
	        
		    item = new HashMap();
		
		   	String man_nm = request.getStringParam("COMP_NM");
		   	
		   	item.put("COMP_NM", man_nm);
		    
			String getComp_id = getItem("mg.ni.searchCompId", item).toString();
			
		    tx.commit();
		    obj.put("ID", getComp_id);   
		    obj.put("errcode", "0");
		    obj.put("errmsg", "success");
		    return write(obj.toString());
		} catch (Exception e) {
			tx.rollback();
			obj.put("ID", " ");  
			obj.put("errcode", "-100");
		    obj.put("errmsg", "ID를 찾을수 없습니다");
			return write(obj.toString());
		}
	}
    
   
    /**
     * 저장
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse saveComp(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        String dbName = pm.getString("component.sql.database");
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
		       	for(int j=0; j < cols.length; j++ ) {
		       	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
			    item.put(cols[j], tmps);     
		        }
	       	item.put("LOGIN_ID",  request.getUser().getId());
	       	log.info("기존유저 업데이트");
			updateObject("mg.ni.compUpdateUser_"+dbName, item);
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1007", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }

    /**
     * 삭제
     * @param request
     * @return
     * @throws Exception
     */
    private IResponse deleteComp(IRequest request) throws Exception 
    {
        String ids_info = request.getStringParam("ids");
        String cols_ids = request.getStringParam("col_ids");
        String[] cols = cols_ids.split(",");
        String[] ids = ids_info.split(",");
        String msg="", flag="";
        try {
        	tx.begin();
	        Map item = null;
	        for (int i = 0; i < ids.length; i++) {
	       	item = new HashMap();
	       		for(int j=0; j < cols.length; j++ ) {
	       	 	String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
		        item.put(cols[j], tmps);     
	         	}
	       	deleteObject("mg.ni.compDeleteUser", item); 
	        }
	        tx.commit();
	        msg = mm.getMessage("COMG_1002", request.getUser().getLocale());
	        flag = "true";
        } catch (Exception e) {
        	tx.rollback();
        	log.error("------------saveUser----------------------------------------------------------------\n" + e.toString());
        	msg = mm.getMessage("COMG_1008", request.getUser().getLocale());
	        flag = "false";
        }
        String ret = responseTranData(msg,flag, "doSave", "doSaveEnd");
        return writeXml(ret);
    }
  
}