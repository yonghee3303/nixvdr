package mg.vf;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.File;
import java.nio.file.Files;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.*;

import com.core.base.ComponentRegistry;
import com.core.component.mail.IMail;
import com.core.component.mail.IMailManagement;
import com.core.component.upload.IFileUploadManagement;
import com.core.framework.core.IRequest;
import com.core.framework.core.IResponse;

import mg.base.FileUtils;
import mg.base.BaseAction;
import mg.base.GridData;
import mg.base.cryptorSHAThread;
import mg.base.unZipThread;
import mg.cp.approvalAction;
//import mg.base.cryptorSHA;
import mg.base.ZipFileUtil;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;






//상신브레이크 결재연동
import mg.site.SangsinInterlock;

public class AgentAction extends BaseAction {
	
	private IMailManagement mailHandle = (IMailManagement) ComponentRegistry.lookup(IMailManagement.class, "com.core.component.IMailManagement");
	/** 파일 업로드를 위한 정의 */
	private IFileUploadManagement fum = (IFileUploadManagement) getComponent(IFileUploadManagement.class);
	/* 압축 Utility */
	//private ICompressManagement compressHandle = (ICompressManagement) ComponentRegistry.lookup(ICompressManagement.class, "com.core.component.ICompressManagement");
	
	
	/**
     * 반출함관리
     * @param request
     * @return
     * @throws Exception
     */
    @Override
	public IResponse run(IRequest request) throws Exception {
        
        if(request.isMode("treeList")) {   /** 반출함 Root 조회 */
            return selectTreeList(request); 
        } else if(request.isMode("collectionList")) {   /* 반출폴더 조회 */
            return selectCollection(request);
        } else if(request.isMode("docList")) {    /* 반출문서 조회 */
            return selectDoc(request);
        } else if(request.isMode("appExport")) {   /* 반출신청 */
            return ApplicationExport(request);
        } else if(request.isMode("delExport")) {   /* 반출신청 삭제(취소) */
            return deleteExport(request);
        } else if(request.isMode("downDoc")) {   /* 반출문서 Download */
            return downExportFile(request);
        } else if(request.isMode("uploadDoc")) {   /* 반출문서 Upload */
            return uploadDoc(request);
        } else if(request.isMode("approvalExport")) {   /* 반출승인 */
            return approvalExport(request);
        } else if(request.isMode("getApprover")) {   /* 승인자 정보조회 */
            return getApprover(request);
        } else if(request.isMode("getApprovalDoc")) {   /* 승인대상 문서조회 */
            return getApprovalDoc(request);
        } else if(request.isMode("searchApprover")) {   /* 결재자 검색 */
            return searchApprover(request);
        } else if(request.isMode("docShiftLogin")) {   /* 문서이관로그인 */
            return docShiftLogin(request);
        } else if(request.isMode("checkDocExport")) {   /* 문서  다운로드 체크 */
            return checkDownExportFile(request);
        }  else if(request.isMode("getDocSiteURL")) {   /* 문서 사이트 URL */
            return getDocSiteURL(request);
        } else if(request.isMode("selectConfigList")) {  /* 반출함Config 조회 */
            return selectConfigList(request);
        }else if(request.isMode("saveConfig")) {   /* 반출함Config 저장 */
            return saveConfig(request);
        }else if(request.isMode("deleteConfig")) {   /* 반출함Config 삭제 */
            return deleteConfig(request);
        }else if(request.isMode("testDecode")) {   /* 반출함Config 삭제 */
            return testDecode(request);
        }else { 
        	 JSONObject retobj = new JSONObject();
       	     retobj.put("errcode", "-1");
		     retobj.put("errmsg", "not found mod value.");
            return write(retobj.toString());
        }
    } 
    public  void testUnZip(){

		log.info("----Batch---testUnZip------Start--- ");
		File zipFile =new File("G:/zipTest");
		ZipFileUtil zipUtil = new ZipFileUtil();
		zipUtil.unZip(zipFile, "G:/zipTest/temp");
		log.info("----Batch---testUnZip------End--- ");
	}
	
	public void testDecryptZip(String srcStr, String fileName){
		log.info("testDecryptZip : " + srcStr+"/"+fileName+".zip");
			try{
				ZipFileUtil zipUtil= new ZipFileUtil();
				FileUtils fu = new FileUtils();
				//fu.makeFolder(srcStr+"/temp", true); 
				String zipFile = srcStr+"/" + fileName + "_.zip";
			    cryptorSHAThread th = new cryptorSHAThread(srcStr+"/"+fileName+".zip", zipFile, pm.getString("diskcryptor.auth.code"));
			    th.start();
				th.join();
			     long retval =th.getResultLong();
			     log.info("복호화 다운로드 : retval (0:실패, 1:성공, -1:원문) : "+retval);
			     unZipThread unzip = new unZipThread(srcStr+"/"+fileName+"_.zip",srcStr+"/temp/"+fileName);
			     unzip.start();
			     unzip.join();
				 // 복호화 임시폴더 생성
			    // fu.makeFolder(srcStr+"/decrypt", true); //decrypt폴더를 만든다.
				 // 복호화
				 fu.dcDecrypt(srcStr+"/temp", srcStr+"/decrypt", pm.getString("diskcryptor.auth.code")); //decrypt폴더에 tempFile에 있는 파일들을 복호화 한다. 
				 // 복호화 된 파일 다시 압축
				 zipUtil.zip(srcStr+"/decrypt", srcStr+"/down/"+fileName+".zip");//인코드 UTF-8 지정하여 zip
				 // 2. 임시폴더 삭제
				 //fu.deleteFolder(new File(srcStr+"/decrypt"));
				 //fu.deleteFolder(new File(srcStr+"/temp"));
			}catch(Exception e){
				log.info("testDecryptZip-------Exception : \n "+e.toString());
			}
	}
	
	public void testDecrypt(String srcStr, String fileName, String fileExt){
		log.info("testDecrypt : " + srcStr+"/"+fileName+"."+fileExt);
		cryptorSHAThread th = new cryptorSHAThread(srcStr+"/"+fileName+"."+fileExt, srcStr+"/down/"+fileName+"."+fileExt,  pm.getString("diskcryptor.auth.code"));
		th.start();
		try{
			th.join();
		}catch(Exception e){ 
			log.info("testDecrypt Error -----"+e.toString());
		}
		long retval = th.getResultLong();
		log.info("retval : "+retval);
	}
	private IResponse testDecode(IRequest request) {
		log.info("testDecode : " + request.toString());
		if("zip".equals(request.getStringParam("fileExt"))) testDecryptZip("Z:/MgUpload/test",request.getStringParam("fileName"));
		else testDecrypt("Z:/MgUpload/test",request.getStringParam("fileName"),request.getStringParam("fileExt"));
		log.info("testDecode : End");
		return write("Finish"); 
	}
	/**
     * 반출폴더 Root 조회
     * @param request
     * @return
     * @throws Exception
     */
     private IResponse selectTreeList(IRequest request) throws Exception 
     {
    	 // Creation of Json Response 
    	 JSONObject obj = new JSONObject();
	   	 
    	 try {
   		 
    		 List<JSONObject> jsonLists = new ArrayList<JSONObject>();
	         JSONObject jsonitem = null;
	         
 	         // 1. 반출신청
 	          	jsonitem = new JSONObject();
 	          	jsonitem.put("r_object_type",  "dm_folder");
 	          	jsonitem.put("rid","Request");
 	          	jsonitem.put("r_folder_type","Request");
 	          	jsonitem.put("text",mm.getMessage("VFMG_0001", request.getUser().getLocale()));
 	          	jsonitem.put("r_link_type", "Request");
 	          	jsonitem.put("r_folder_security","LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,PRINT,DELETE,WRITE");
 	          	jsonLists.add(jsonitem);
 	         // 2. 승인요청
 	          	jsonitem = new JSONObject();
 	          	jsonitem.put("r_object_type",  "dm_folder");
 	          	jsonitem.put("rid","Approval");
 	          	jsonitem.put("r_folder_type","Approval");
 	          	jsonitem.put("text",mm.getMessage("VFMG_0003", request.getUser().getLocale()));
 	          	jsonitem.put("r_link_type", "Approval");
 	          	jsonitem.put("r_folder_security","LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,PRINT,DELETE,WRITE");
 	          	jsonLists.add(jsonitem);
 	         // 3. 반출함
 	          	jsonitem = new JSONObject();
 	          	jsonitem.put("r_object_type",  "dm_folder");
 	          	jsonitem.put("rid","Export");
 	          	jsonitem.put("r_folder_type","Export");
 	          	jsonitem.put("text",mm.getMessage("VFMG_0002", request.getUser().getLocale()));
 	          	jsonitem.put("r_link_type", "Export");
 	          	jsonitem.put("r_folder_security","LOCALTOECM,ECMTOECM,DELETE,WRITE");
 	          	jsonLists.add(jsonitem);
 	       
 	        obj.put("list", jsonLists); 
	   		obj.put("errcode", "0");
			obj.put("errmsg", "Tree List query success.");
	   		return write(obj.toString());
        } catch(Exception e) {
      	 	log.error("---Export Query failed ROOT folder. -------------------------\n" + e.toString());
      	 	obj.put("errcode", "-100");
			obj.put("errmsg", e.toString());
      	 	return write(obj.toString());
        }
     }
     
     
     /**
      * 반출폴더 조회
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse selectCollection(IRequest request) throws Exception 
      {
 	   	 JSONObject obj = new JSONObject();
 	   	 
     	 try {
    		 Map remap = request.getMap();
    		 String dbName = pm.getString("component.sql.database");	 
    		 remap.put("LOGIN_ID", request.getUser().getId());
     		 List lists;
 	       	 String folderType = request.getStringParam("linkId");
 	         String rid = request.getStringParam("rid");
 	       	 String security_val = "";
 	         	        
	       	 if("Request".equals(folderType)) {  // 반출신청 이면
	       		 security_val = "LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,WRITE";
	       		 if("Request".equals(rid) ) {
	       			 remap.put("rid","");
	       		 }
	 	       	 lists = getList("mg.vf.selectCollectionRequest" + getDBName(dbName),remap);
 	       	 } else  if("Export".equals(folderType)) { // 반출함 이면
 	       		security_val = "LOCALTOECM,ECMTOECM,WRITE";
 	       		if("Export".equals(rid) ) {
	       			 remap.put("rid","");
	       		 }
 	       		lists = getList("mg.vf.selectCollectionExport" + getDBName(dbName),remap);
 	       	 } else  if("Approval".equals(folderType)) {  // 승인함 이면
 	       		security_val = "LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,DELETE,WRITE";
 	       		if("Approval".equals(rid) ) {
 	       			remap.put("rid","");
 	       			lists = getList("mg.vf.selectCollectionUser" + getDBName(dbName) ,remap);
 	       		} else {
 	       			if("U_".equals(rid.substring(0, 2))) {  // 승인함 User 이면
 	       				remap.put("rid","");
 	       				remap.put("createrId", rid.substring(2));
 	       			} 
 	       			lists = getList("mg.vf.selectCollectionApproval"+ getDBName(dbName),remap);
 	       		}
 	       	 } else   {  
 	       		security_val = "LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,PRINT,DELETE,WRITE";
 	       		lists = getList("mg.vf.selectCollectionExport"+ getDBName(dbName),remap);
 	       	 }
	       	 //log.info("selectCollection-------------------------------------------------------------------------------------------------");
	         List<JSONObject> jsonLists = new ArrayList<JSONObject>();
	         JSONObject jsonitem = null;
	         // 
	         for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	
	          	jsonitem = new JSONObject();
 	          	jsonitem.put("r_object_type",  "dm_folder");
 	          	
 	          	jsonitem.put("r_object_id", this.getStringWithNullCheck(map, "COLLECTION_ID"));
 	          	jsonitem.put("r_folder_type",folderType);
 	          	jsonitem.put("r_object_type","dm_folder");
 	          	jsonitem.put("r_link_type", folderType);
 	          	jsonitem.put("rid", this.getStringWithNullCheck(map, "COLLECTION_ID"));
 	          	jsonitem.put("r_content_size","0");
 	          	jsonitem.put("text",this.getStringWithNullCheck(map, "COLLECTION_NM"));
 	          	jsonitem.put("r_sts_nm", this.getStringWithNullCheck(map, "REQ_STS_NM"));
 	          	jsonitem.put("create_dt", this.getStringWithNullCheck(map, "CREATE_DT"));
 	          	jsonitem.put("requester_nm", this.getStringWithNullCheck(map, "CREATOR_NM"));
 	          	jsonitem.put("approval_dt", this.getStringWithNullCheck(map, "R_DT"));
 	          	//log.info("selectCollection-------------------------------------------------------------------------------------------------"+this.getStringWithNullCheck(map, "R_REASON").replace("\r\n", " "));
 	          	jsonitem.put("approval_reason", this.getStringWithNullCheck(map, "R_REASON").replace("\r\n", " "));
 	          	jsonitem.put("approver_nm", this.getStringWithNullCheck(map, "RU_NM"));
 	          	jsonitem.put("ex_nm", this.getStringWithNullCheck(map, "EX_CL_NM"));
 	          	jsonitem.put("r_folder_security",security_val);
	          	jsonLists.add(jsonitem);
	         }
	         obj.put("list", jsonLists);   
 	   		 obj.put("errcode", "0");
 			 obj.put("errmsg", "Collection List query success.");
 			 //log.info("selectCollection--------------------------------------------------------------------------------------------obj.toString()-"+obj.toString());
 	   		 return write(obj.toString());
 	   		
         } catch(Exception e) {
        	 log.error("---Collection List folder lookup failure. -------------------------\n" + e.toString());
     	 	 obj.put("errcode", "-100");
			 obj.put("errmsg", "The Session is not valid. You are not logged in or session expired.\n" + e.toString());
       	 	 return write(obj.toString());
         }
      }
     
      
      /**
       * 반출문서 조회
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse selectDoc(IRequest request) throws Exception 
       {
  	   	 JSONObject obj = new JSONObject();
  	   	 
      	 try {
      		 Map remap = request.getMap();
      		 String dbName =  pm.getString("component.sql.database");
      		 remap.put("LOGIN_ID", request.getUser().getId());
      		 //log.info("----------------------------------------------------------------LOGIN_ID :  "+request.getUser().getId());
    		 List lists;
	       	 String folderType = request.getStringParam("linkId");
	       	 String security_val = "";
	       	 	        
	         if("Request".equals(folderType)) {  // 신청함 이면
	        	security_val = "LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,DELETE,WRITE";
	 	       	lists = getList("mg.vf.selectDocRequest" + getDBName(dbName),remap);
	       	 } else  if("Export".equals(folderType)) { // 반출함 이면  --> 기간/회수 체크 추가
	       		security_val = "LOCALTOECM,ECMTOECM,WRITE";
	       		lists = getList("mg.vf.selectDocExport" + getDBName(dbName),remap);
	       	 } else  if("Approval".equals(folderType)) {  // 승인함 이면
	       		 security_val = "LOCALSAVE,LOCALTOECM,ECMTOECM,ATTACH,DELETE,WRITE";
	       		 lists = getList("mg.vf.selectDocApproval" + getDBName(dbName) ,remap);
	       	 } else   {  
	       		security_val = "LOCALSAVE,LOCALTOECM,ECMTOECM,WRITE";
	       		lists = getList("mg.vf.selectDocExport" + getDBName(dbName) ,remap);
	       	 }
	         //log.info("selectDoc----------------------------------------1 : "+lists.size());
	         List<JSONObject> jsonLists = new ArrayList<JSONObject>();
	         JSONObject jsonitem = null;
	         // 
	         for (int i = 0; i < lists.size(); i++) {
	          	Map map =(Map) lists.get(i);
	          	//log.info("selectDoc----------------------------------------1"+map.toString());
	          	jsonitem = new JSONObject();
	          	jsonitem.put("r_object_type",  "di_doc");
	          	jsonitem.put("r_object_id", this.getStringWithNullCheck(map, "DOC_ID"));
	          	jsonitem.put("object_name",this.getStringWithNullCheck(map, "DOC_NM") + "." + this.getStringWithNullCheck(map, "DOC_EXTENSION"));
	          	jsonitem.put("r_folder_type",folderType);
	          	jsonitem.put("r_link_type", folderType);
	          	jsonitem.put("r_lock_owner", "");
	          	jsonitem.put("r_lock_type", "");
	          	jsonitem.put("owner_name", this.getStringWithNullCheck(map, "CREATOR_NM"));
	          	jsonitem.put("r_content_size",this.getStringWithNullCheck(map, "DOC_SIZE"));
	          	jsonitem.put("r_sts_nm", this.getStringWithNullCheck(map, "REQ_STS_NM"));
	        	jsonitem.put("create_dt", this.getStringWithNullCheck(map, "CREATE_DT"));
	          	jsonitem.put("requester_nm", this.getStringWithNullCheck(map, "CREATOR_NM"));
	          	jsonitem.put("r_doc_id", this.getStringWithNullCheck(map, "COLLECTION_ID"));
	          	jsonitem.put("approval_dt", this.getStringWithNullCheck(map, "R_DT"));
	        	//log.info("selectDoc----------------------------------------2---------------"+this.getStringWithNullCheck(map, "R_REASON").replace("\r\n", " "));
	          	jsonitem.put("approval_reason", this.getStringWithNullCheck(map, "R_REASON").replace("\r\n", " "));
	          	
	          	jsonitem.put("approver_nm", this.getStringWithNullCheck(map, "RU_NM"));
	          	jsonitem.put("ex_nm", this.getStringWithNullCheck(map, "EX_CL_NM"));
	          	jsonitem.put("r_doc_security",security_val);
	          	jsonLists.add(jsonitem);
	         }
	         obj.put("list", jsonLists);   
  	   			         
	        obj.put("totalCount", String.valueOf(lists.size()) );
  	   		obj.put("errcode", "0");
  			obj.put("errmsg", "Export Documents query success.");
  	   		return write(obj.toString());
  	   		
          } catch(Exception e) {
        	 	log.error("---Export Documents folder lookup failure. -------------------------\n" + e.toString());
        	 	obj.put("errcode", "-100");
  			    obj.put("errmsg", "The Session is not valid. You are not logged in or session expired.\n" + e.toString());
        	 	return write(obj.toString());
          }
       }
       
       /**
        * 반출문서 Upload
        * @param request
        * @return
        * @throws Exception
        */
       private IResponse uploadDoc(IRequest request) {
       	
    	   JSONObject obj = new JSONObject();
     	   
    	   String docId = request.getStringParam("docId");
    	   String pgmId =  request.getStringParam("pgmId");
           String msg="";
           Map map = request.getMap();
           log.info("--------fum.createUpload--###-------------\n" + map.toString());
           try {
	           	String userid = request.getUser().getId();
	   	    	tx.begin(); 
	   	        fum.VFUpload(pgmId, docId, request);
	   	        tx.commit();
	   	        
	   	        // 성공시 FileName Response
	   	        obj.put("errcode", "0");
	  			obj.put("errmsg", "File Upload success.");
	  			obj.put("UPLOAD_UUID", docId);
	       	} catch (Exception e) {
	            tx.rollback();
	            obj.put("errcode", "-1");
	  			obj.put("errmsg", "Fail File Upload \n" + e.getMessage());
           }
           return write(obj.toString());
       }
     
     /**
      * 반출신청 등록
      *   EX_COLLECTION --> 문서제목 으로 폴더 생성
      *   EX_DOC --> (Upload된 파일 수 만큼) 등록된 문서등록 (복수)
      *   EX_APPROVER --> 선택된 결재자 등록
      *   EX_REQUEST --> 결재신청 등록
      * @param request
      * @return
      * @throws Exception
      */
      private IResponse ApplicationExport(IRequest request) throws Exception 
      {
     	 // Creation of Json Response 
 	   	 JSONObject obj = new JSONObject();
 	   	 JSONParser parser = new JSONParser();
 	   	
     	 try {
     		 tx.begin();
     		 String jsonData = request.getStringParam("fileRegParam");
     		 JSONObject jObj = (JSONObject)parser.parse(jsonData);
     		 
     		 Map map = new HashMap();
     		 DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
             String collectionId = "C" + formatter.format(new Date()) + getRandomString(5);
             
             // 폴더생성 (Collection)
     		 map.put("COLLECTION_ID", collectionId);
     		 map.put("P_COLLECTION_ID", "ROOT");
     		 String docTitle = jObj.get("docTitle").toString();
     		 map.put("hasCollectionNm", docTitle);
     		 int sameCollectionCnt =getList("mg.vf.selectCollectionExport" + getDBName(pm.getString("component.sql.database")),map).size();
     		 log.info("docTitle  :: collectionId :" +collectionId+"  sameCollectionCnt:: "+sameCollectionCnt);
     		 if(sameCollectionCnt > 0){
     			log.info("docTitle duplicate :: collectionId :" +collectionId+"  sameCollectionCnt:: "+sameCollectionCnt);
     			 docTitle +="_"+sameCollectionCnt; // docTitle 현재 반출함에 있는 거랑 중복(대/소문자 구분 없이) 안되도록 체크하고, 수정해야됨.
     		 }
     		 map.put("COLLECTION_NM", docTitle);
     		 map.put("CREATOR_ID", request.getUser().getId());
     		 map.put("OWNER_ORG_CD", request.getUser().getProperty("DEPT_CD"));
     		 map.put("COLLECTION_STYLE", "Request");
     		 map.put("COLLECTION_SECURITY", "LOCALSAVE,ATTACH,PRINT,DELETE,WRITE");   //LOCALSAVE,ATTACH,PRINT,DELETE,WRITE,READ
     		 createObject("mg.vf.insertCollection", map);
     		 
     		 log.info("ApplicationExport ---Collection--exe파일추가--\n" + map.toString());
     		 
      		// deleteObject("mg.vf.deleteSiteURL", map);
      		//사이트등록
     		 if(!"2".equals(jObj.get("REQ_CL_CD").toString())){
	      		JSONArray jURL = (JSONArray)jObj.get("listURL"); 
	      	    for (int i = 0; i < jURL.size(); i++) {
	      		    JSONObject docs = (JSONObject)jURL.get(i);
	      	        Map item = new HashMap();
	      	        item.put("COLLECTION_ID", collectionId);
	      	        item.put("EX_CONF_CL_CD", docs.get("EX_CONF_CL_CD").toString());
	      	        item.put("CONF_KEY", docs.get("CONF_KEY").toString());
	      	        item.put("CONF_VAL", docs.get("CONF_VAL").toString());
	      	        log.info("ApplicationExport ---insertSiteURL----\n" + item.toString());
	      	        
	      	        createObject("mg.vf.insertSiteURL", item);
	      	    } 
      	    }

     		// 결재신청등록
     		String requesterId = "R" + formatter.format(new Date()) + getRandomString(5);
     		map.put("REQUEST_NO", requesterId);
     		map.put("REQUEST_TITLE", docTitle);
     		map.put("REQUEST_REASON", jObj.get("reqReason").toString());
     		map.put("REQ_CL_CD", jObj.get("REQ_CL_CD").toString());  // 1.일반문서, 2.보안문서(CP)
     		map.put("EXE_FILE",  getStringNullCheck(jObj.get("exeFile").toString()));
     		if(("Y".equals(jObj.get("selfApproval").toString()))) {   //자가결재 이면
     			map.put("REQ_STS_CD", "3");  // 승인
     			map.put("APPROVER_ID", request.getUser().getId());  // 결재자
     			map.put("APPROVAL_REASON", "Self Approval !");  // 의견
     		 }else if("Y".equals(request.getUser().getProperty("SELF_EXP_YN").toString())){
     			map.put("REQ_STS_CD", "3");  // 승인
     			map.put("APPROVER_ID", request.getUser().getId());  // 결재자
     			map.put("APPROVAL_REASON", jObj.get("reqReason").toString());  // 의견
     		 }else {
     			map.put("REQ_STS_CD", "1");  // 신청
     			map.put("APPROVER_ID", "");  // 결재자
     			map.put("APPROVAL_REASON", jObj.get("reqReason").toString());  // 의견
     		}
     		createObject("mg.vf.insertRequest", map);
     		     		
     		log.info("ApplicationExport ---insertRequest----\n" + map.toString());
     		
     		//승인자 등록
     		JSONArray jApprover = (JSONArray)jObj.get("approverlist"); 
     	    for (int i = 0; i < jApprover.size(); i++) {
     		    JSONObject docs = (JSONObject)jApprover.get(i);
     	        Map item = new HashMap();
     	        item.put("REQUEST_NO", requesterId);
     	        item.put("APPROVER_ID", docs.get("approverId").toString());
     	        item.put("APPROVER_NM", docs.get("approverNm").toString());
     	        item.put("APPROVER_EMAIL", docs.get("email").toString());
     	        item.put("APPROVER_TEL", docs.get("tel").toString());
     	        createObject("mg.vf.insertApprover", item);
     	        
     	        log.info("ApplicationExport ---insertApprover----\n" + item.toString());
     	    } 
     	    
     		// 문서등록
     	 	JSONArray jArray = (JSONArray)jObj.get("fileList"); 
     	    for (int i = 0; i < jArray.size(); i++) {
     		    JSONObject docs = (JSONObject)jArray.get(i);
     	        Map item = new HashMap();
     	        item.put("DOC_ID", docs.get("UPLOAD_UUID").toString());
     	        item.put("DOC_NM", docs.get("fileName").toString());
     	        item.put("DOC_EXTENSION", docs.get("fileExt").toString());
     	        item.put("DOC_SIZE", docs.get("fileSize").toString());
     	        item.put("CREATOR_ID", request.getUser().getId());
     	        item.put("OWNER_ORG_CD", request.getUser().getProperty("DEPT_CD"));
     	        item.put("COLLECTION_ID", collectionId);
     	        // 문서등록
     	        createObject("mg.vf.insertDoc", item);
     	        // log.info("ApplicationExport ---insertDoc----\n" + map.toString());
     	    } 
     	    
     	    
     	   //  if("2".equals(jObj.get("REQ_CL_CD").toString())) {  // EXE 반출 등록
     	    	if(!saveCP(request, map, jObj)) {
     	    		 tx.rollback();
     	       	 	 log.error("---ApplicationExport :  CP 생성 실패-------------------------\n");
     	       	 	 obj.put("errcode", "-1");
     	 			 obj.put("errmsg", "Export registration document fails");
     	       	 	 return write(obj.toString());
     	    	}
     	    // }
     	    tx.commit();
     		  	   		
 	   		obj.put("errcode", "0");
 			obj.put("errmsg", "Export registration Success.");
 			log.info("ApplicationExport-----------------\n " + obj.toString());
 	   		return write(obj.toString());
 	   		
         } catch(Exception e) {
        	 tx.rollback();
       	 	 log.error("---ApplicationExport :  Export registration document fails-------------------------\n" + e.toString());
       	 	 obj.put("errcode", "-1");
 			 obj.put("errmsg", "Export registration document fails (Exception Error)");
       	 	 return write(obj.toString());
         }
      }
      
      /* 
       * CP 문서 생성 등록,
       */
      private boolean saveCP(IRequest request, Map master, JSONObject jObj)
      {
      
    	  try {
    		  	Map map = new HashMap();
    		  	map.put("LOGIN_ID", this.getStringWithNullCheck(master, "CREATOR_ID"));
    		  	map.put("DOC_NO", this.getStringWithNullCheck(master, "REQUEST_NO"));
    		  	map.put("DOC_TITLE", this.getStringWithNullCheck(master, "REQUEST_TITLE"));
    		  	map.put("WAITING_TIME", "10");
    		  	map.put("CREATOR_EMAIL", request.getUser().getProperty("EMAIL"));
    		  	map.put("USER_ID", this.getStringWithNullCheck(master, "CREATOR_ID"));
    		  	map.put("P_DOC_NO", ""); 
    		  	map.put("EXE_FILE", this.getStringWithNullCheck(master, "EXE_FILE"));
    		  	map.put("IN_PROCESS", ""); 
    		  	map.put("EXE_RULE", "VF EXPORT"); 
    		  	if("1".equals(jObj.get("REQ_CL_CD").toString())) {  // 원문반출
    		  		map.put("AUTHORITY", "6"); 
	       		} else if("2".equals(jObj.get("REQ_CL_CD").toString())) {  // EXE 반출 등록
	       			map.put("AUTHORITY", "4"); 
	       		}
    		  	map.put("OUT_CL_CD", "01"); 
    		  	map.put("APPROVER_ID", this.getStringWithNullCheck(master, "APPROVER_ID"));
    		  	map.put("DOC_MSG", this.getStringWithNullCheck(master, "REQUEST_REASON"));
    		  	map.put("IN_CL_CD", "C");
    		  	
    		  	int x;
    		  	String filepath="", dip="";
    		  	JSONArray jArray = (JSONArray)jObj.get("fileList"); 
    		  	for (x = 0; x < jArray.size(); x++) {
         		    JSONObject docs = (JSONObject)jArray.get(x);
         	        Map item = new HashMap();
         	        item.put("DOC_NO", docs.get("UPLOAD_UUID").toString());
         	        item.put("PGM_ID", "VFExport");
         	        Map Fmap  =  (Map)getItem("mg.vf.fileUploadPath", item);
         	        filepath +=  dip + pm.getString("component.upload.directory") + this.getStringWithNullCheck(Fmap, "FILE_PATH") + "/" + this.getStringWithNullCheck(Fmap, "FILE_NAME");
         	        dip = "|";
         	       
         	        // 반출등록된 문서를 CP에서 참조 하기 위하여 CP DOC_NO로  Mapping 등록 해준다.
         	        Fmap.put("DOC_ID", this.getStringWithNullCheck(master, "REQUEST_NO"));
         	        createObject("mg.vf.insertUploadMappingFile", Fmap); 
         	    } 
    			map.put("FILE_NUM", "" + x); 
    		  	map.put("FILE_LIST", filepath); 
    		  	
    		  	// Doc Master 등록
	       		createObject("mg.cp.insertDocMaster", map); 
	       		log.info("-----------------------\nVF Export Master 등록 완료 !!");

    	      	String approver1_id ="";
    	      	String approver2_id = "";
    			if("personal".equals(pm.getString("component.export.approver"))) {
    		       	Map approverMap= new HashMap();//  = (Map)getItem("mg.cp.getApproverId", request.getMap());
    		       	approverMap.put("USER_ID",  request.getUser().getId());
    		       	List lists = getList("mg.cp.getApproverId",approverMap);
    		         for (int i = 0; i < lists.size(); i++) {
    		        	 approverMap =(Map) lists.get(i); 
    		        	 if(i==0){
    		        		 approver1_id = this.getStringWithNullCheck(approverMap, "APPROVER_ID");
    		        	 }else{
    		        		 approver2_id= this.getStringWithNullCheck(approverMap, "APPROVER_ID");
    		        	 }
    		         }
       			}else{
       				approver1_id = request.getUser().getProperty("MANAGER_ID").toString();
    	   			map.put("P_DEPT_CD",  request.getUser().getProperty("P_DEPT_CD").toString());
    	   			Object result = getItem("mg.cp.getManagerID", map);
    	   	      	approver2_id =	(result == null) ? "" : result.toString();
       			}
           		if(approver2_id.equals(request.getUser().getId())){
           			//log.info("두번째 승인자의 아이디로 자신의 아이디가 있을 경우 순서 변경");
           			String tempId = approver1_id;	
           			approver1_id=approver2_id;
           			approver2_id=tempId;	
           		}
       			if(approver1_id.equals(request.getUser().getId()) && !"Y".equals(request.getUser().getProperty("SELF_EXP_YN").toString())) {
	       			approver1_id = "sameApprover";
	       		} else if("Y".equals(request.getUser().getProperty("SELF_EXP_YN").toString())){
	       			approver1_id = request.getUser().getId();
	       		}
       			log.info("approver1_id / approver2_id---------------------------------------------------------------"+approver1_id+" / "+approver2_id+"--------------------"); 		
	       		map.put("APPROVER1_ID", approver1_id);
	       		map.put("APPROVER2_ID", approver2_id);
	       		log.info("--saveCP---insertDocApprover---------------------\n" + map.toString());
	       		createObject("mg.cp.insertDocApprover", map); 
	       			       		
	       		// 신규등록 이면 상태코드등록
	            DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	            
	    		String scheNo = formatter.format(new Date()) + getRandomString(6);
	       	 	map.put("SCHE_NO", scheNo);
	       	 	
	       	 	if("Y".equals(request.getUser().getProperty("SELF_EXP_YN").toString())){
	       	 		if("6".equals(map.get("AUTHORITY"))) map.put("CP_STS_CD", "04");
	       	 		else if("4".equals(map.get("AUTHORITY"))) map.put("CP_STS_CD", "02");	
	       	 	}
	       	 	else map.put("CP_STS_CD", "01");
	       	 	
	     		log.info(map.toString());
	     		createObject("mg.cp.saveJobSchedule", map);
	     		log.info("Job Schedule 등록 완료");
		     	
		     	if("2".equals(jObj.get("REQ_CL_CD").toString())) {  // EXE 반출 등록
	    	        // Doc User 등록
		     		JSONArray docUser = (JSONArray)jObj.get("docuserlist"); 
	    		  	for (int i = 0; i < docUser.size(); i++) {
	         		    JSONObject docs = (JSONObject)docUser.get(i);
	         		    
	         	        Map item = new HashMap();
	         	        item.put("LOGIN_ID",  request.getUser().getId());
	   	       	 	    item.put("DOC_NO",  map.get("DOC_NO"));
	   	       	 	    item.put("SEQ", i);
	   	       	 	    item.put("COMPANY_ID", docs.get("COMPANY_ID").toString());
	   	       	 	    item.put("USER_ID", docs.get("USER_ID").toString());
	   	       	 	    item.put("USER_NM", docs.get("USER_NM").toString());
	   	       	 	    item.put("POLICY", docs.get("POLICY").toString());
	   	       	 	    item.put("POLICY_DOC", docs.get("POLICY_DOC").toString());
	   	       	 	    item.put("MOD_STOP", "");
	   	       	 	    item.put("EXPIRE_DT", docs.get("EXPIRE_DT").toString());
	   	       	 	    item.put("READ_COUNT", docs.get("READ_COUNT").toString());
	   	       	 	    item.put("MESSAGE", docs.get("MESSAGE").toString());
	   	       	 	    item.put("MAC_LIST", "");
	   	       	 	    item.put("PASSWD", docs.get("PASSWD").toString());
	   	       	 	    item.put("POLICY_NO", docs.get("POLICY_NO").toString());
	   	       	 	    item.put("POLICY_DISP", docs.get("POLICY_DISP").toString());
	   	       	 	    item.put("OUTDEVICE", "");
		   	       	 	item.put("dbName", pm.getString("component.sql.database"));
		   	       	 	log.info("--saveCP---insertDocUser--EXE------------------\n" + item.toString());
			       		createObject("mg.cp.insertDocUser", item);
	         	    } 
	       		} else {   //원문반출
	       			Map item = new HashMap();
         	        item.put("LOGIN_ID",  request.getUser().getId());
   	       	 	    item.put("DOC_NO",  map.get("DOC_NO"));
   	       	 	    item.put("SEQ", 0);
   	       	 	    item.put("COMPANY_ID", "COM-001");
   	       	 	    item.put("USER_ID",request.getUser().getId());
   	       	 	    item.put("POLICY", "");
   	       	 	    item.put("POLICY_DOC", "");
   	       	 	    item.put("MOD_STOP", "");
   	       	 	    if("Y".equals(request.getUser().getProperty("SELF_EXP_YN").toString())){
   	       	 	    	//정책정보 가져오기
   	       	 	    	item.put("CD", jObj.get("POLICY_NO").toString());
   	       	 	    	Map policyMap = (Map)sm.getItem("mg.master.getSingleCpGrp",item);
   	       	 	    	Calendar calendar = new GregorianCalendar(Locale.KOREA);
   	       	 	    	calendar.setTime(new Date());
   	       	 	    	DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
   	       	 	    	calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)+Integer.parseInt(policyMap.get("EXPIRE_DAY").toString()));
   	       	 	    	item.put("EXPIRE_DT", formatter2.format(calendar.getTime()));
   	       	 	    }
   	       	 	    else item.put("EXPIRE_DT", jObj.get("EXPIRE_DT").toString());
   	       	 	    item.put("READ_COUNT", jObj.get("READ_COUNT").toString());
   	       	 	    item.put("MESSAGE", "");
   	       	 	    item.put("MAC_LIST", "");
   	       	 	    item.put("PASSWD", "");
   	       	 	    item.put("POLICY_DISP", "");
   	       	 	    item.put("POLICY_NO", jObj.get("POLICY_NO").toString());
   	       	 	    item.put("OUTDEVICE", jObj.get("OUTDEVICE").toString());
   	       	 	    map.put("OUTDEVICE", jObj.get("OUTDEVICE").toString());
	   	       	 	item.put("dbName", pm.getString("component.sql.database"));
	   	       	 	log.info("--saveCP---insertDocUser ---원문------------------\n" + item.toString());
		       		createObject("mg.cp.insertDocUser", item);
	       		}
		     	    	        
    	        if("Y".equals(pm.getString("cliptorplus.request.mail")) && !"Y".equals(request.getUser().getProperty("SELF_EXP_YN").toString())) {
    	        	// 신청시 메일발송 옵션 체크 시
    		 		// 승인자에게 메일 발송
    		 		try {
    		 			 IMail mail = mailHandle.createHtmlMail();
    		 			
    			         mail.setFrom(this.getStringWithNullCheck(map,"CREATOR_EMAIL"));
    			         
    			         map.put("APPROVER_ID",  approver1_id);
    			         log.info("------------saveUser------APPROVER_ID 1--\n" + map.toString());
    			         String tmpApprover = getItemData("mg.cp.getApproverEmail", map);
    			         log.info("------------APPROVER_ID 1--\n" + tmpApprover);
    			         if(tmpApprover != null && !"".equals(tmpApprover) && !"sameApprover".equals(approver1_id)) {
    			        	 mail.addTo(tmpApprover);   // 상위자
    			         }
    			         map.put("APPROVER_ID",  approver2_id);
    			         log.info("------------saveUser------APPROVER_ID 2--\n" + map.toString());
    			         tmpApprover = getItemData("mg.cp.getApproverEmail", map);   // 차상위자
    			         log.info("------------APPROVER_ID 2--\n" + tmpApprover);
    			         if(tmpApprover != null && !"".equals(tmpApprover)) {
    			        	 mail.addTo(tmpApprover);   // 차상위자
    			         }
    			        // mail.setSubject(mm.getMessage("MAIL_0001", request.getUser().getLocale()));
    			         if("2".equals(jObj.get("REQ_CL_CD").toString())) { 
    			        	 mail.setSubject("["+mm.getMessage("MAIL_0004", request.getUser().getLocale())+"]"+" "+mm.getMessage("MAIL_0001", request.getUser().getLocale())); //보안문서 반출요청
    			         }else{
    			        	 mail.setSubject("["+mm.getMessage("MAIL_0083", request.getUser().getLocale())+"]" +" "+mm.getMessage("MAIL_0081", request.getUser().getLocale())); //보안문서 반출요청
    			         }
    			          // 메일 템플릿을 읽어 온다.
    			         String fileName;
    			         //상신일 경우 결재 버튼 없는 폼으로 처리
    			         if("SB".equals(pm.getString("component.site.company"))){
    			        	 log.info("------------NoBtn_mail_Template------------");
    			        	 fileName = "config/templates/mail_Template_NoBtn.html";
    			         } else {
    			        	 log.info("------------default_mail_Template------------");
    			        	 fileName = "config/templates/mail_Template.html";
    			         }
    			 		 String mailContext = readFile(fileName);
    			 		 
    			 		 mailContext = mailContext.replace("SYSTEMNAME", "["+mm.getMessage("MAIL_0083", request.getUser().getLocale())+"]");
    			 		if("2".equals(jObj.get("REQ_CL_CD").toString())) { 
    			 			mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0001", request.getUser().getLocale()));
    			 		}else{
    			 			mailContext = mailContext.replace("MAIL_CAPTION", mm.getMessage("MAIL_0081", request.getUser().getLocale()));
    			 		}
    					 mailContext = mailContext.replace("LOGO_LINK",pm.getString("component.site.ep.url"));
    	    			 mailContext = mailContext.replace("LOGOIMAGE", pm.getString("component.site.url") + pm.getString("component.site.image"));

    	    			 Object[] params = {request.getUser().getName()};
    	    			 if("2".equals(jObj.get("REQ_CL_CD").toString())) { 
    	    				 mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0002",params, request.getUser().getLocale()));
    	    			 }else{
    	    				 mailContext = mailContext.replace("MAIL_CONTENT", mm.getMessage("MAIL_0082",params, request.getUser().getLocale()));	 
    	    			 }
    	    			 mailContext = mailContext.replace("COMPANY", mm.getMessage("MAIL_1000", request.getUser().getLocale()));
    					 mailContext = mailContext.replace("COMP_NM", pm.getString("component.site.company.name"));
    					 mailContext = mailContext.replace("USER_LABLE", mm.getMessage("MAIL_1001", request.getUser().getLocale()));
    					 String tmp="";
    	 				// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------2"+request.getUser().getProperty("POSITION_NM"));
    					 if(request.getUser().getProperty("POSITION_NM")== null){
    						// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------2-1");
    						 tmp = request.getUser().getProperty("USER_NM").toString()+" / "+request.getUser().getProperty("FUNCTION_NM").toString()+" / "+request.getUser().getProperty("DEPT_NM").toString();
    					 }else{
    						 tmp = request.getUser().getProperty("USER_NM").toString()+" / "+request.getUser().getProperty("POSITION_NM").toString()+" / "+request.getUser().getProperty("DEPT_NM").toString();
    					 }
    					// log.info("사용자 신청 완료 메일 발송------------------------------------------------------------------------------------3");
    					 mailContext = mailContext.replace("USER_NM", tmp);
    					 mailContext = mailContext.replace("DATE", mm.getMessage("MAIL_1002", request.getUser().getLocale()));
    					 
    					 formatter = new SimpleDateFormat("yyyy-MM-dd");
    			   		 String curDt = formatter.format(new Date());
    					 mailContext = mailContext.replace("REQ_DT", curDt);
    			         
    					 mailContext = mailContext.replace("LINK", pm.getString("component.site.ep.url"));//파트너 포탈
    					 mailContext = mailContext.replace("BUTTONTEXT", mm.getMessage("MAIL_1005", request.getUser().getLocale()));
    					     					 
    					// 메일 발송
    					 mail.setHtmlMessage(mailContext);
    			         mail.send();
    		 	
    			  	}catch(Exception e) {
    			  	     log.info("---반출함--->CP 문서 생성 : Mail 발송실패--\n" + e.toString());
    			  	}
    	        }
    	        
    	        if("SB".equals(pm.getString("component.site.company"))){
    	   	 		//상신브레이크 결재연동 승인/반려처리
    	        	log.info("상신브레이크 결재연동------------------------saveCP");
    	        	SangsinInterlock sb = new SangsinInterlock();
    	        	String result_SB = sb.saveCP_SB(request, map, (JSONArray)jObj.get("docuserlist"));
    	   	 		if("0".equals(result_SB)){
    	   	 			log.info("상신브레이크 결재연동 오류------------------------saveCP");
    	   	 			return false;
    	   	 		}
    	        }
    	        
    	        return true;
    	  	 } catch (Exception e) {
    	  		   log.info("--CP 문서 생성--\n" + e.getMessage());
    	  		   return false;
    	      }
      }
      
      /**
       *   반출신청 삭제 (취소)
       *   EX_COLLECTION --> 문서제목 으로 폴더 생성
       *   EX_DOC --> (Upload된 파일 수 만큼) 등록된 문서등록 (복수)
       *   EX_APPROVER --> 선택된 결재자 등록
       *   EX_REQUEST --> 결재신청 등록
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse deleteExport(IRequest request) throws Exception 
       {
      	 // Creation of Json Response 
  	   	 JSONObject obj = new JSONObject();
  	   	 JSONParser parser = new JSONParser();
  	   	
      	 try {
      		 tx.begin();
      		 String objType = request.getStringParam("objtype");
      		 Map param = request.getMap();
      		 try {
      			 param.put("LOGIN_ID", request.getUser().getId());
			} catch (Exception e) {
				param.put("LOGIN_ID", "admin");
			}
      		 
      		 if("folder".equals(objType)) {  // Collection 삭제
      			 param.put("COLLECTION_ID", param.get("r_folder_id").toString());
      			 updateObject("mg.vf.cancelRequest", param);
      			
      			//Collection 취소
      			 updateObject("mg.vf.cancelCollection", param);
      			 
      			 //관련 문서 취소
      			 updateObject("mg.vf.cancelDoc", param);
      			 
      		 } else {   // Content 삭제
      			 param.put("DOC_ID", param.get("r_object_id").toString());
      			 updateObject("mg.vf.cancelDoc", param);
      		 }

      	     log.info("deleteExport -------\nSuccess.");
      	     tx.commit();
      		  	   		
  	   		 obj.put("errcode", "0");
  			 obj.put("errmsg", "Delete Export Success.");
  	   		 return write(obj.toString());
  	   		
          } catch(Exception e) {
         	 tx.rollback();
        	 log.error("---deleteExport : Failed to delete the export documents -------------------------\n" + e.toString());
        	 obj.put("errcode", "-1");
  			 obj.put("errmsg", "Failed to delete the export documents");
        	 return write(obj.toString());
          }
       }
       
       private IResponse checkDownExportFile(IRequest request) 
       {
    	   JSONObject obj = new JSONObject();
    	   
    	   try {
	   		     Map smap = request.getMap();
	   		     String dbName = pm.getString("component.sql.database");
	   		     smap.put("OBJECT_ID", request.getStringParam("r_object_id"));
	   		     smap.put("USER_ID", request.getUser().getId());
	   		     String request_no = getItem("mg.vf.getRequestNo",smap).toString();
	   		     smap.put("DOC_NO", request_no);
	   		     
	   		     //log.info("tmpCnt------------------------"+smap.toString());
	   		     	   		   
	   		     //int logDownload = Integer.parseInt(getItem("mg.cp.downloadCntCheck", smap).toString());
	   		     int logDownload = Integer.parseInt(getItem("mg.cp.downCntCheck", smap).toString());
	   		     //log.info("tmpCnt------------------------"+logDownload);
	   		  
	   		     //다운로드 횟수 제한 : 체크 시점에 다운 가능하다면 DOWN_CNT를 증가하여, 다음 요청 시 그 값과 비교 (드래그, 우클릭 후 복사 시 여러 파일을 다운받더라도 단 한번의 카운트로 인식하도록....) 
	   	        
	   	        Map cpmap = (Map)getItem("mg.cp.selectExportDocPolicy", smap);
	   	        int downCnt = Integer.parseInt(this.getStringWithNullCheck(cpmap, "READ_COUNT"));
	   	        obj.put("OUTDEVICE", this.getStringWithNullCheck(cpmap, "OUTDEVICE"));
	   	        obj.put("READCOUNT", downCnt);
	   	     
	   	         //log.info("downCnt------------------------"+downCnt);
	   	         //CP_DOC_USER에 등록된 문서번호는 요청번호, 다운받을 때 받는 번호는 문서번호로 서로 달라 문서번호로 요청번호를 찾아 다운로드 횟수를 갖고온다.
	   	         //log.info("cpmap------------------------"+cpmap.toString());
	   	         	   	         
	   	    	 if(logDownload >= downCnt){
	   	    		 obj.put("errcode", "-1");
		  			 obj.put("errmsg",  mm.getMessage("CPMG_1044", request.getUser().getLocale())); 
		  			return write(obj.toString());
	   	    	 } else {
	   	    		 //download 가능하다고 체크 된 시점에 Download 카운트 증가 
	   	    		  smap.put("DOC_ID", request_no);
	   	    		  updateObject("mg.common.updateDocDownCnt", smap);
			   	     obj.put("errcode", "0");
		  			 obj.put("errmsg", "Download Check Success.");
		  	   		 return write(obj.toString());
	   	    	 }
    	   } catch(Exception e) {
          	  log.error("--------checkDownExportFile-----------------------------\n" + e.toString());
          	  obj.put("errcode", "-1");
 			  obj.put("errmsg", "Failed to download the export documents");
          	  return write(obj.toString());
           }
       }
      
      /**
       * 반출파일 다운로드 
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse downExportFile(IRequest request) 
      {
	   	   try {
		   		   Map smap = request.getMap();
		   		   String dbName = pm.getString("component.sql.database");
		   		   smap.put("DOC_NO", request.getStringParam("r_object_id"));
		   		   smap.put("USER_ID", request.getUser().getId());
	   		   
		   	       smap.put("PGM_ID","VFExport");
	   		        
	  	           Map map = (Map)getItem("mg.vf.exportFile", smap);         
	  	           String fileName = this.getStringWithNullCheck(map, "FILE_NAME");
	  	           String srcFile = pm.getString("component.upload.directory") + "/" + this.getStringWithNullCheck(map, "FILE_PATH") + "/" + fileName;
	  	           String downFile = "";
	  	           if(pm.getBoolean("diskcryptor.auth.code.use")) {  // DiskCryptor가 적용된 SITE이면 복호화를 수행한다.
	  	        	   //log.info("java.library.path : "+System.getProperty("java.library.path"));
		  	           // 1. 대상파일 암호화
		  	           int lastIndex = fileName.lastIndexOf("."); 
		 			   String fileExtend = fileName.substring(lastIndex+1);
		 			   String zipName = fileName.substring(0,lastIndex);
		 			  FileUtils fu = new FileUtils();
		 			  // srcFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/" + fileName;
		 			   String tempFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/temp";
		 			   fu.makeFolder(tempFile, true);
		 			   downFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/down";
		 			   fu.makeFolder(downFile, true);
		 			   downFile += "/" + zipName + "." + fileExtend;
		 			   String decryptFile = pm.getString("component.upload.directory") +  this.getStringWithNullCheck(map, "FILE_PATH") + "/decrypt";
		 			   	 			  
		 			   if("zip".equals(fileExtend.toLowerCase())) {   // 압축파일 이면
		 				   	ZipFileUtil zipUtil= new ZipFileUtil();
		 				    String zipFile = pm.getString("component.upload.directory") + "/" + this.getStringWithNullCheck(map, "FILE_PATH") + "/" + zipName + "_." + fileExtend;
		 				    cryptorSHAThread th = new cryptorSHAThread(srcFile, zipFile, pm.getString("diskcryptor.auth.code"));
		 					try{
		 						th.start();
		 						th.join();
		 						long retval =th.getResultLong();
		 						if(retval != -1)  srcFile = zipFile;  // DC 암호화 파일일 경우 복 화 한 파일로 작업하고 암호화된 파일이 아닐 경우 원문으로 작업 한다.
		 						log.info("복호화 다운로드 : retval (0:실패, 1:성공, -1:원문) : "+retval);
		 						unZipThread unzip = new unZipThread(srcFile,tempFile); //_.zip을 temp폴더에 압축을 푼다.
		 						// 압축해제 
		 						unzip.start();
		 					    unzip.join();
		 					}catch(Exception e){ 
		 						log.info("zip Decrypt & unzip Error -----"+e.toString());
		 					}
		 				     // 복호화 임시폴더 생성
				 			 fu.makeFolder(decryptFile, true); //decrypt폴더를 만든다.
				 			 // 복호화
				 			 fu.dcDecrypt(tempFile, decryptFile, pm.getString("diskcryptor.auth.code")); //decrypt폴더에 tempFile에 있는 파일들을 복호화 한다. 
				 			 // 복호화 된 파일 다시 압축
				 			zipUtil.zip(decryptFile, downFile);//인코드 UTF-8 지정하여 zip
				 			 // 2. 임시폴더 삭제
				 			 File decryptFolder = new File(decryptFile);
				 			 fu.deleteFolder(decryptFolder);
				 				  
		 			   } else {
			 				  //log.info("srcFile : "+srcFile +"     []     downFile : "+downFile);
			 				  cryptorSHAThread th = new cryptorSHAThread(srcFile, downFile, pm.getString("diskcryptor.auth.code"));
			 				  th.start();
			 				  try{
			 						th.join();
			 				  }catch(Exception the){ 
			 						log.info("cryptorSHAThread Decrypt Error -----"+the.toString());
			 				  }
			 				  long retval =th.getResultLong();
			 				     
			 				  // long retval = cryptorSHA.MgDecodeFile(srcFile, downFile, pm.getString("diskcryptor.auth.code")); // 0:실패, 1:성공, -1:원문
			 				  if(retval == -1) fu.fileCopy(srcFile, downFile); 
			 				  log.info("복호화 다운로드 : retval (0:실패, 1:성공, -1:원문) : "+retval);
			 			 
		 			   }
		 			     // 2. 임시폴더 삭제
		 				File tempFolder = new File(tempFile);
		 				fu.deleteFolder(tempFolder);
      
	  	           } else {
		               downFile = pm.getString("component.upload.directory") + "/" + this.getStringWithNullCheck(map, "FILE_PATH") + "/" + fileName;
	  	           }
		           log.info("-복호화 다운로드  :-\n" + downFile);
		           tx.begin();
		           //파일 다운 시 로그 쓰기
					  try{
		  					HttpServletRequest httpRequest = (HttpServletRequest) request.getAdapter(HttpServletRequest.class);
		  					String locale="";
		  					Map item = new HashMap();
		  					
		  					smap.put("OBJECT_ID", request.getStringParam("r_object_id"));
		  					item.put("DOC_NO", getItem("mg.vf.getRequestNo",smap).toString()); //INNER JOIN을 해서 조회하기 때문에 요청번호로 저장해야함. 반출번호로 저장하면 나오지 않는다.
		  					//item.put("DOC_NO", request.getStringParam("r_object_id"));
		  					
		  					item.put("USER_ID", request.getUser().getId());
		  					item.put("USER_NM", request.getUser().getName()); 
		  					item.put("IP_ADDR", httpRequest.getRemoteAddr());
		  					item.put("IP_REMOTE", httpRequest.getRemoteAddr());
		  					item.put("MAC_ADDR", "");
		  					item.put("WORKGROUP", "");
		  					item.put("WORK_TYPE", "DOWNLOAD");
		  					item.put("MESSAGE", "VFExport Download");
		  				
		  					if("ko".equals(request.getUser().getLocale().toString())){ 
		  						locale="KOR";
		  					}
		  					item.put("COUNTRY_CD",locale);
		  					item.put("PC_LOGIN_ID", "");
		  					item.put("ISPRIVATE", pm.getString("component.private.security"));
		  					
		  					//20160704:  상신브레이크 반출함 상세 로그내역 저장을 위해 추가
		  				    item.put("LOG_CL_CD", "E"); //C: CP반출, E: 원문반출
		 			        item.put("OUT_UNIT_CD", request.getStringParam("OUT_UNIT_CD"));//전달받아서 입력해야함. 01:Local, 02:USB, 03:CD, 04:ATTACH
		 			        item.put("OUT_URL",request.getStringParam("OUT_URL"));//04일 경우 전달받아서 입력해야함.
		 			        item.put("FILE_NM",fileName);
		 			        
		 			        log.info("Download----------------------------------------------로그---------------------------"+item.toString());	  	   	      
			  	        	
		  					createObject("mg.cp.saveDocLog"+getDBName(dbName), item); 
		  				
					  }catch(Exception logE){
						  	log.error("--------exeDownload---------------------------로그 쓰는 중--\n" + logE.toString());
					  }
					  
					 /* //수신자 다운로드 시 CP_DOC_MASTER의 DOWN_CNT 증가시키기 
					  map.clear();
					  smap.put("OBJECT_ID", request.getStringParam("r_object_id"));
					  map.put("DOC_ID", getItem("mg.vf.getRequestNo",smap).toString()); 
					  updateObject("mg.common.updateDocDownCnt", map);*/
		           tx.commit();
		           log.info("--------downFile-----------------------------\n"+downFile);
		           return downloadFile(downFile);
	   	   } catch(Exception e) {
         	  log.error("--------exeDownload-----------------------------\n" + e.toString());
         	  tx.rollback();
         	  return downloadFile(null);
          }
      }

      /**
       * 문서별 UPLOAD SITE 통제
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse getDocSiteURL(IRequest request) 
      {
    	  JSONObject obj = new JSONObject();
	   	   try {   		
	   		   /*
	   		    Map smap = request.getMap();		
	   		    smap.put("DOC_NO", request.getStringParam("r_object_id"));
	   		    try {
		   	        Map cpmap = (Map)getItem("mg.cp.selectExportDocPolicy", smap);
		   	        obj.put("OUTDEVICE", this.getStringWithNullCheck(cpmap, "OUTDEVICE"));
	   		    }catch(Exception e) {
	   		    	obj.put("OUTDEVICE", "");
	   		    	log.info("--------OUTDEVICE Exception Error-----------------------------\n"+ e.getMessage());
	   		    }
	   		 log.info("--------getDocSiteURL----1------------------------\n");	   
	   		 */	      
	   		    String retURL="";
		   		List lists = getList("mg.vf.getDocSiteURL",request.getMap());
		   		String div = "";
		        for (int i = 0; i < lists.size(); i++) {
		         	Map map = (Map) lists.get(i);
		         	retURL += div + this.getStringWithNullCheck(map,"CONF_URL");
		         	div = "^";
		         	log.info("--------getDocSiteURL----CONF_VAL-----------------------\n" + i);	   	
		        }  
		        obj.put("URL", retURL);
		        obj.put("errcode", "0");
				obj.put("errmsg", "Success");
		  	 	return write(obj.toString());
	   	   } catch(Exception e) {
	          	log.error("------------getDocSiteURL Exception Error-----------------------\n" + e.toString());
	          	obj.put("errcode", "-1");
				obj.put("errmsg", e.toString());
	    	 	return write(obj.toString());
          }
      }
      
      /**
       * 반출 문서 승인
       * @param request
       * @return
       * @throws Exception
       */
      private IResponse approvalExport(IRequest request) throws Exception 
      {
    	  JSONObject obj = new JSONObject();
          String msg="";
          try {
          	tx.begin();
          	Map map = request.getMap();
          	map.put("DOC_NO", getItemData("mg.vf.getRequestDocNo", map));
          	map.put("USER_ID", request.getUser().getId());
          	
          	log.info("--------------------------approvalExport -MAP--------------------------------\n"  + map.toString());
          	if(setExportStat(map)) {
          		
          		
	          	// CP 승인처리
	            // DOC_NO, AUTHORITY,  APPROVAL_MSG
	          	map.put("AUTHORITY", "6");  // 원문반출
	          	map.put("APPROVAL_MSG", map.get("apvReason").toString());
	          	approvalAction  appAction = new approvalAction();
	          	
	          	// 사용자의 정보를 찾아옴
	          	Map xmap = new HashMap();
	        	xmap.put("value", request.getUser().getId());
	        	String dbName = pm.getString("component.sql.database");
	        	Map userMap = (Map)getItem("app.user.getUserByProperties" + getDBName(dbName), request.getUser().getId().toString());
	          	//------------------------------------------

	        	log.info("--------------------------approvalExport -appAction.exportApprova--------------------------------\n"  + map.toString());
	        	
	          	msg = appAction.exportApproval(map, map.get("isApproval").toString(), request.getUser().getLocale(), userMap );
	          	
	  	        tx.commit();
	  	        
		  	    obj.put("errcode", "0");
				obj.put("errmsg", msg);
		  	 	return write(obj.toString());
          	} else {
          		tx.rollback();
          		obj.put("errcode", "-1");
				obj.put("errmsg", mm.getMessage("CPMG_1005", request.getUser().getLocale()));
		  	 	return write(obj.toString());
          	}
          } catch (Exception e) {
          	tx.rollback();
          	log.error("------------approvalExport------------------------\n" + e.toString());
          	obj.put("errcode", "-1");
			obj.put("errmsg", e.toString());
    	 	return write(obj.toString());
          }
      }
      
      // apvReason, isApproval, 
      public boolean setExportStat(Map map)
      {
    	  boolean result = true;
    	  try {
        	map.put("LOGIN_ID", map.get("USER_ID"));
        	if("Y".equals(map.get("isApproval"))) {
        		map.put("STAT_CD","3");

                //원문 반출 시 마이가드 정책 연동을 할 것인지....
    	 		    if("Y".equals(pm.getString("component.policy.myguard.useYn"))){
    	 		    	String interResult = vfPolicyInterlock(map);
    	 		    	log.info("----------------------------------------------원문반출 마이가드 정책연동 :"+result);
    	 		    	if("1".equals(interResult)) result = false;  //오류
    	 		    } 
    	 		  
    	          	//승인일을 기준으로 만료일 지정
    	          	log.info("----------------------------------------승인일 기준으로 만료일 지정");
    	          	map.put("EXPIRE_DAY", Integer.parseInt(getItem("mg.cp.getExpireDay",map).toString()));
    	           	map.put("dbName", pm.getString("component.sql.database"));	
    	          	updateObject("mg.cp.setExpireDate",map);
    	          	log.info("만료일수---------------"+map.get("EXPIRE_DAY"));
    	 		    		    
        	} else {
        		map.put("STAT_CD","2");
        	}
        	log.info("----------------------------------------------setExportStat :"+map.toString());
        	
        	updateObject("mg.vf.approvalExport", map);
        	
        	log.info("----------------------------------------------setExportStat : Update");
	 		    
        	return result;
    	  } catch (Exception e) {
        	log.error("------------setExportStat------------------------\n" + e.toString());
        	return false;
          }
      }
      
      /**
       * 반출 승인 시 마이가드 정책연동
       * @param map
       * @return
       */
      private String vfPolicyInterlock(Map map){
      	String errorStr="0";
      	try{
         	//MyGuard와 정책연동
         	//ReqFromDate는 현재시간
         	//ReqEndDate는 현재시간이 오전이면 오늘 업무시간까지 , 오후이면 차일 업무시간까지
      	Calendar calendar = new GregorianCalendar(Locale.KOREA);
  		calendar.setTime(new Date());
  		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
  		
  		String fromDate = formatter.format(calendar.getTime());
  		String endDate="";
  		if(Integer.parseInt(fromDate.substring(8, 12)) > 1200){
  			log.info("오후");
  			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)+1, pm.getInt("component.policy.myguard.officeHour"), 00, 00);
  			endDate = formatter.format(calendar.getTime());
  		}else{
  			log.info("오전");
  			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), pm.getInt("component.policy.myguard.officeHour"), 00, 00);
  			endDate = formatter.format(calendar.getTime());
  		}
  		map.put("REQFROMDATE", fromDate);
  		map.put("REQENDDATE",endDate );
  		log.info("-----------------fromDate : "+fromDate+"\n--------------------endDate : "+endDate);
  		String requesterId = getItemData("mg.cp.getRequesterId", map);
  		map.put("USER_ID", requesterId);
  		String outdevice = getItemData("mg.batch.getOutdevice",map); //DOC_NO
  		map.put("OUTDEVICE",outdevice);
  		log.info("------------------------------------------------------requesterId : "+requesterId+"\n-------------------------------------outDevice : "+outdevice);

  		//사용자가 가지고 있던 기존 정책들을 얻어온다.
  		Map authMap = (Map)getItem("mg.batch.getAuthCode", map);
  		log.info("기존 정책 : "+this.getStringWithNullCheck(authMap, "REQAUTHCODEA")+"/"+this.getStringWithNullCheck(authMap, "REQAUTHCODEB")+"/"+this.getStringWithNullCheck(authMap, "REQAUTHCODEC")+"/"+this.getStringWithNullCheck(authMap, "REQAUTHCODED"));
  		map.put("REQAUTHCODEA", this.getStringWithNullCheck(authMap, "REQAUTHCODEA"));
  		map.put("REQAUTHCODEB", this.getStringWithNullCheck(authMap, "REQAUTHCODEB"));
  		map.put("REQAUTHCODEC", this.getStringWithNullCheck(authMap, "REQAUTHCODEC"));
  		map.put("REQAUTHCODED", this.getStringWithNullCheck(authMap, "REQAUTHCODED"));
  		
  		String newPolicy = getItemData("mg.batch.getAuthCodeNew", map);
  		log.info("새로운 정책 코드 : " +newPolicy);
  		if(newPolicy != null){
  			String[] newAuthList = newPolicy.split(",");
  			
			for(int i=0; i<newAuthList.length; i++){
				log.info("newAuthList ---------------\t["+i+"]"+newAuthList[i]);
				String newAuth[] = newAuthList[i].split(":");
				log.info("------------------AuthCode \t"+newAuth[0]+":"+newAuth[1]);
				if("A".equals(newAuth[0])){
					map.put("REQAUTHCODEA", newAuth[1]);
				}else if("B".equals(newAuth[0])){
					map.put("REQAUTHCODEB", newAuth[1]);	
				}else if("C".equals(newAuth[0])){
					map.put("REQAUTHCODEC", newAuth[1]);	
				}else if("D".equals(newAuth[0])){
					map.put("REQAUTHCODED", newAuth[1]);	
				}
			}
  		}

  		log.info("MG 정책연동 중---------------------------------------------------------"+map.toString());
         	createObject("mg.batch.insertPolicy_MG", map);
         	
      	}catch(Exception e){
      		errorStr="1";
      		log.error("MG 정책연동 중 오류-------------------------------------------"+e);
      		
      	}
      	return errorStr;
      }
      
      /**
       * 반출승인 결재자 조회
       * @param request
       * @return
       * @throws Exception
       */
       private IResponse getApprover(IRequest request) throws Exception 
       {
  	   	 JSONObject obj = new JSONObject();
  	   	 
      	 try {
     		 Map remap = request.getMap();
     		 String dbName = pm.getString("component.sql.database");
     		 remap.put("LOGIN_ID", request.getUser().getId());
      		 List lists = getList("mg.vf.getApprover" + getDBName(dbName), remap);
       	          		 
      		 obj.put("requesterNm", request.getUser().getName() + "(" + request.getUser().getId()  + ")" + " / " +  request.getUser().getProperty("DEPT_NM")  );

 	         List<JSONObject> jsonLists = new ArrayList<JSONObject>();
 	         JSONObject jsonitem = null;
 	         // 
 	         for (int i = 0; i < lists.size(); i++) {
 	          	Map map =(Map) lists.get(i);
 	          	
 	          	jsonitem = new JSONObject();
  	          	jsonitem.put("approverId",   this.getStringWithNullCheck(map, "USER_ID"));
  	          	jsonitem.put("approverNm", this.getStringWithNullCheck(map, "USER_NM"));
  	          	jsonitem.put("functionNm", this.getStringWithNullCheck(map, "FUNCTION_NM"));
  	          	jsonitem.put("deptNm",this.getStringWithNullCheck(map, "DEPT_NM"));
  	          	jsonitem.put("email",this.getStringWithNullCheck(map, "EMAIL"));
  	          	jsonitem.put("tel",this.getStringWithNullCheck(map, "TEL_NO"));
 	          	jsonLists.add(jsonitem);
 	         }
 	         obj.put("list", jsonLists);   
  	   		 obj.put("errcode", "0");
  			 obj.put("errmsg", "Select Ssuccess.");
  	   		 return write(obj.toString());
  	   		
          } catch(Exception e) {
        	 	log.error("---getApprover Exception Error -------------------------\n" + e.toString());
        	 	obj.put("errcode", "-1");
  			    obj.put("errmsg", e.toString());
        	 	return write(obj.toString());
          }
       }
       
       /**
        * 반출승인 문서조회
        * @param request
        * @return
        * @throws Exception
        */
        private IResponse getApprovalDoc(IRequest request) throws Exception 
        {
   	   	 JSONObject obj = new JSONObject();
   	   	 
       	 try {
             List lists = getList("mg.vf.getApprovalDoc", request.getMap());

       		 obj.put("requesterNm", request.getUser().getName() + "(" + request.getUser().getId()  + ")" + " / " +  request.getUser().getProperty("DEPT_NM")  );

  	         List<JSONObject> jsonLists = new ArrayList<JSONObject>();
  	         JSONObject jsonitem = null;
  	         // 
  	         for (int i = 0; i < lists.size(); i++) {
  	          	Map map =(Map) lists.get(i);
  	          	
  	          	if(i==0) {
  	          	     obj.put("requesterNm",  this.getStringWithNullCheck(map, "REQUESTER_NM") + "(" +  this.getStringWithNullCheck(map, "REQUESTER_ID") + ")" + " / " +   this.getStringWithNullCheck(map, "DEPT_NM")   );
  	          	     obj.put("REQUEST_TITLE",  this.getStringWithNullCheck(map, "REQUEST_TITLE"));
  	          	     obj.put("REQUEST_REASON",  this.getStringWithNullCheck(map, "REQUEST_REASON"));
  	          	}
  	          	
  	          	jsonitem = new JSONObject();
   	          	jsonitem.put("DOC_ID",   this.getStringWithNullCheck(map, "DOC_ID"));
   	          	jsonitem.put("DOC_NM", this.getStringWithNullCheck(map, "DOC_NM"));
   	          	jsonitem.put("DOC_SIZE", this.getStringWithNullCheck(map, "DOC_SIZE"));
  	          	jsonLists.add(jsonitem);
  	         }
  	         obj.put("list", jsonLists);   
   	   		 obj.put("errcode", "0");
   			 obj.put("errmsg", "Select Success.");
   	   		 return write(obj.toString());
   	   		
           } catch(Exception e) {
         	 	log.error("---getApprover Exception Error -------------------------\n" + e.toString());
         	 	obj.put("errcode", "-1");
   			    obj.put("errmsg", e.toString());
         	 	return write(obj.toString());
           }
        }
        
        /**
         * 결재 승인자 조회
         * @param request
         * @return
         * @throws Exception
         */
         private IResponse searchApprover(IRequest request) throws Exception 
         {
    	   	 JSONObject obj = new JSONObject();
    	   	 JSONParser parser = new JSONParser();
    	   	 
        	 try {
        		 
        		 String jsonData = request.getStringParam("param");
         		 JSONObject pObj = (JSONObject)parser.parse(jsonData);
         		 
        		 
        		 Map param = request.getMap();
        		 String sKey = pObj.get("searchKey").toString();
        		 if("NAME".equals(sKey)) {
        			 param.put("userNm", "%" +  pObj.get("searchKeyword").toString() + "%");
        		 } else if("ID".equals(sKey)) {
        			 param.put("userId", "%" +  pObj.get("searchKeyword").toString() + "%");
        		 } else if("DEPT_NM".equals(sKey)) {
        			 param.put("deptNm", "%" +  pObj.get("searchKeyword").toString() + "%");
        		 }
                 List lists = getList("mg.vf.searchApprover", param);

	   	         List<JSONObject> jsonLists = new ArrayList<JSONObject>();
	   	         JSONObject jsonitem = null;
	   	         // A.USER_ID, A.USER_NM, A.DEPT_CD ,DEPT_NM, C.CD_NM AS FUNCTION_NM, TEL_NO, EMAIL
	   	         for (int i = 0; i < lists.size(); i++) {
	   	          	Map map =(Map) lists.get(i);
	   	          	jsonitem = new JSONObject();
    	          	jsonitem.put("USER_ID",   this.getStringWithNullCheck(map, "USER_ID"));
    	          	jsonitem.put("USER_NM", this.getStringWithNullCheck(map, "USER_NM"));
    	          	jsonitem.put("FUNCTION_NM", this.getStringWithNullCheck(map, "FUNCTION_NM"));
    	          	jsonitem.put("DEPT_NM", this.getStringWithNullCheck(map, "DEPT_NM"));
    	          	jsonitem.put("TEL_NO", this.getStringWithNullCheck(map, "TEL_NO"));
    	          	jsonitem.put("EMAIL", this.getStringWithNullCheck(map, "EMAIL"));
	   	          	jsonLists.add(jsonitem);
	   	         }
	   	         obj.put("list", jsonLists);   
    	   		 obj.put("errcode", "0");
    			 obj.put("errmsg", "Select Success.");
    	   		 return write(obj.toString());
    	   		
            } catch(Exception e) {
          	 	log.error("---getApprover Exception Error -------------------------\n" + e.toString());
          	 	obj.put("errcode", "-1");
    			    obj.put("errmsg", e.toString());
          	 	return write(obj.toString());
            }
         }
         
         
         /**
          * 문서이관 로그인
          * @param request
          * @return
          * @throws Exception
          */
          private IResponse docShiftLogin(IRequest request) throws Exception 
          {
     	   	 JSONObject obj = new JSONObject();
     	   	 
         	 try {
        		String dbName = pm.getString("component.sql.database");
	        	Map map = (Map)getItem("app.user.getUserByProperties" + getDBName(dbName), request.getStringParam("user_id").toString());
        		
	        	log.info("docShiftLogin-------------------------------------------------------\n"+map);
	        	
	        	if (map == null || map.size() == 0) {
	        		 obj.put("user_id", request.getStringParam("user_id"));
	     	   		 obj.put("errcode", "-1");
	     			 obj.put("errmsg", "not found user !!");
	        	 } else {
	        		 obj.put("user_name", map.get("NAME"));
	        		 obj.put("user_id", request.getStringParam("user_id"));
	        		 obj.put("authCode", pm.getString("diskcryptor.auth.code"));
	     	   		 obj.put("errcode", "0");
	     			 obj.put("errmsg", "Success.");
	        	 }
     	   		 return write(obj.toString());
     	   		
             } catch(Exception e) {
           	 	log.error("---getApprover Exception Error -------------------------\n" + e.toString());
           	 	obj.put("errcode", "-1");
     			obj.put("errmsg", e.toString());
           	 	return write(obj.toString());
             }
          }
          
          private IResponse test(IRequest request) throws Exception 
          {
     	   	 JSONObject obj = new JSONObject();
     	   	 
         	 try {
         		 Map remap = request.getMap();
         		 
   	          	log.info("remap.param--------------------- > "+this.getStringWithNullCheck(remap, "PARAM"));
   	          
     	   		//return write(this.getStringWithNullCheck(remap, "PARAM").replace("\\r\\n", " "));
   	         File selectDir = new File(this.getStringWithNullCheck(remap, "PARAM").replace("/", "\\"));
   	         File[] innerFileList = selectDir.listFiles();
   	         String msg="";
   	         if(innerFileList.length==0){
   	        	msg += "하위 파일이 존재 하지 않아 해당 폴더를 삭제 합니다.";
 				if(selectDir.delete()) msg +="\n파일 삭제 성공";
 				else msg +="\n파일 삭제 실패";
 			 }else{
 				msg += "하위파일이 존재합니다.";
 			 }
   	          	
     	   		return write(msg);
             } catch(Exception e) {
           	 	log.error("---Export Documents folder lookup failure. -------------------------\n" + e.toString());
           	 	obj.put("errcode", "-100");
     			    obj.put("errmsg", "The Session is not valid. You are not logged in or session expired.\n" + e.toString());
           	 	return write(obj.toString());
             }
        	 
          }
          
          //반출함 기본 첨부 url CONFIG 삭제
          private IResponse deleteConfig(IRequest request) {
          	String ids_info = request.getStringParam("ids");
              String cols_ids = request.getStringParam("col_ids");
              String[] cols = cols_ids.split(",");
              String[] ids = ids_info.split(",");
              Map item = null;
              
              try {
      	        for (int i = 0; i < ids.length; i++) {
      	       	 	item = new HashMap();
      	       	 
      	       	 	for(int j=0; j < cols.length; j++ ) {
      	       	 		 String tmps = request.getStringParam(ids[i] + "_" + cols[j]);
      		             item.put(cols[j], tmps);     
      	         	}
      	       	 	deleteObject("mg.vf.deleteConfig", item);
      	        }
      	        return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
              } catch(Exception e) {
                 log.error("---------------------------------------------------------------\n" + e.toString()
              		   + "\n---------------- LogMap :: " + item);
             	   return writeXml(responseTranData(mm.getMessage("COMG_1008", request.getUser().getLocale()),"false", "doSave", "doSaveEnd"));
              }
      	}
          //반출함 기본 첨부 url CONFIG 저장
      	private IResponse saveConfig(IRequest request) {
          	String ids_info = request.getStringParam("ids");
      		String cols_ids = request.getStringParam("col_ids");
      		String[] cols = cols_ids.split(",");
      		String[] ids = ids_info.split(",");
      		Map item = null;

      		try {
      			for (int i = 0; i < ids.length; i++) {
      				item = new HashMap();

      				for (int j = 0; j < cols.length; j++) {
      					String tmps = request
      							.getStringParam(ids[i] + "_" + cols[j]);
      					item.put(cols[j], tmps);
      				}
      				if (Integer.parseInt(getItem("mg.vf.countConfig", item).toString()) > 0) {
      					updateObject("mg.vf.updateConfig", item);
      				} else {
      					createObject("mg.vf.insertConfig", item);
      				}
      			}
      			return writeXml(responseTranData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doSave", "doSaveEnd"));
      		} catch (Exception e) {
      			log.error("---------------------------------------------------------------\n" + e.toString()
      					+ "\n---------------- LogMap :: " + item);
      			return writeXml(responseTranData(mm.getMessage("COMG_1007", request.getUser().getLocale()), "false", "doSave", "doSaveEnd"));
      		}
      	}
        //반출함 기본 첨부 url CONFIG 조회
      	private IResponse selectConfigList(IRequest request) {
          	String col = request.getStringParam("grid_col_id");
      		String[] cols = col.split(",");
      		Map map = null;
      		GridData gdRes = new GridData();
      		
      		try {
      			List lists;
      			lists = getList("mg.vf.selectConfigList", request.getMap());
      			for (int i = 0; i < lists.size(); i++) {
      				map = (Map) lists.get(i);

      				for (int j = 0; j < cols.length; j++) {
      					gdRes.addValue(cols[j],
      							this.getStringWithNullCheck(map, cols[j]));
      				}
      			}

      			return writeXml(responseSelData(mm.getMessage("COMG_1002", request.getUser().getLocale()), "true", "doQuery", gdRes.getGridXmlDatas()));
      		} catch (Exception e) {
      			log.error("---------------------------------------------------------------\n" + e.toString()
      					+ "\n---------------- LogMap :: " + request.getMap());
      			return writeXml(responseSelData(mm.getMessage("COMG_1001", request.getUser().getLocale()), "false", "doQuery", gdRes.getGridXmlDatas()));
      		}
      	}

}
