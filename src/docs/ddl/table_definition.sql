CREATE TABLE cp_agent (
	agent_no     	varchar(5)    NOT NULL,
	server_nm    	varchar(50)   NULL,
	ip_addr      	varchar(20)   NULL,
	port         	numeric(10)   NULL,
	use_yn       	varchar(1)    NULL,
	process_cnt  	numeric(3)    NULL,
	chg_id       	varchar(128)  NULL,
	chg_dt         	timestamp     NULL
);

CREATE INDEX cp_agent_xpkcpagent ON cp_agent USING btree (agent_no);

CREATE TABLE cp_agent_sts (
	agent_no    	varchar(5) 		NOT NULL,
	chk_ymd     	varchar(8) 		NOT NULL,
	chk_hms 	    varchar(6) 		NOT NULL,
	exe_process_cnt numeric(3) 		NULL,
	cpu_usage 		numeric(3) 		NULL,
	memory_usage 	numeric(3) 		NULL,
	agent_sts_cd 	varchar(2) 		NULL,
	agent_msg 		varchar(512)	NULL,
	disk_usage 		numeric(3)      NULL
);

CREATE INDEX cp_agent_sts_xpkcpagentsts ON cp_agent_sts USING btree (agent_no, chk_ymd, chk_hms);

CREATE TABLE cp_config (
	cp_conf_cl_cd  varchar(2) 		NOT NULL,
	conf_key 	   varchar(32) 		NOT NULL,
	conf_val 	   varchar(128) 	NULL,
	policy_no 	   varchar(20) 		NOT NULL
);

CREATE INDEX cp_config_xpk_cp_config ON cp_config USING btree (cp_conf_cl_cd, conf_key, policy_no);

CREATE TABLE cp_doc_approver (
	doc_no 		  varchar(20) 	    NOT NULL,
	approver1_id  varchar(128) 	    NULL,
	approver2_id  varchar(128) 	    NULL
);

CREATE INDEX cp_doc_approver_xpk_cp_doc_approver ON cp_doc_approver USING btree (doc_no);

CREATE TABLE cp_doc_link (
	is_policy1 	   varchar(1) 		NULL,
	doc_no 		   varchar(200) 	NOT NULL,
	doc_title 	   varchar(256) 	NULL,
	waiting_time   numeric(5) 		NULL,
	r_user_nm 	   varchar(2000) 	NULL,
	file_num 	   numeric(5) 		NULL,
	file_list      varchar(2000) 	NULL,
	user_id 	   varchar(128) 	NULL,
	p_doc_no  	   varchar(20) 		NULL,
	exe_file 	   varchar(128) 	NULL,
	in_process 	   varchar(1024) 	NULL,
	transfer_date  varchar(8) 		NULL,
	authority 	   bpchar(1) 		NULL,
	mod_stop 	   varchar(1) 		NULL,
	mac_list 	   varchar(20) 		NULL,
	expire_dt 	   varchar(8) 		NULL,
	r_user_id 	   varchar(1024) 	NULL,
	message 	   varchar(1024) 	NULL,
	r_user_pw 	   varchar(256) 	NULL,
	email 		   varchar(128) 	NULL,
	file_uuid 	   varchar(2000) 	NULL,
	file_path 	   varchar(256) 	NULL,
	file_size 	   varchar(512) 	NULL,
	user_nm 	   varchar(128) 	NULL,
	user_dept 	   varchar(128) 	NULL,
	transfer_flag  varchar(1) 		NULL,
	company_nm 	   varchar(1024) 	NULL,
	is_policy2 	   varchar(1) 		NULL,
	is_policy3 	   varchar(1) 		NULL,
	is_policy4 	   varchar(1) 		NULL,
	is_policy5 	   varchar(1) 		NULL,
	is_policy_doc1 varchar(1) 		NULL,
	is_policy_doc2 varchar(1) 		NULL,
	is_policy_doc3 varchar(1) 		NULL,
	is_policy_doc4 varchar(1) 		NULL,
	is_policy_doc5 varchar(1) 		NULL,
	read_count 	   numeric(5) 		NULL,
	"oid" 		   varchar(100) 	NULL,
	exe_path 	   varchar(512) 	NULL
);

CREATE INDEX cp_doc_link_xpk_cp_doc_link ON cp_doc_link USING btree (doc_no);

CREATE TABLE cp_doc_log (
	doc_no 		varchar(20) 		NOT NULL,
	seq 		numeric(6) 			NOT NULL,
	create_ymd  varchar(10) 		NULL,
	ip_addr 	varchar(20)			NULL,
	mac_addr 	varchar(20) 		NULL,
	workgroup   varchar(512) 		NULL,
	user_nm 	varchar(30) 		NULL,
	country_cd 	varchar(10) 		NULL,
	work_type 	varchar(30) 		NULL,
	message 	varchar(1024) 		NULL,
	reg_dt 		timestamp 			NULL,
	user_id 	varchar(128) 		NULL,
	ip_remote 	varchar(20) 		NULL,
	pc_login_id varchar(128) 		NULL,
	file_nm 	varchar(128) 		NULL,
	log_cl_cd 	varchar(1) 			NULL,
	out_unit_cd varchar(2) 			NULL,
	out_url 	varchar(256) 		NULL
);

CREATE INDEX cp_doc_log_xpk_cp_doc_log ON cp_doc_log USING btree (doc_no, seq);

CREATE TABLE cp_doc_master (
	create_dt     timestamp 		NULL,
	doc_no 		  varchar(20) 		NOT NULL,
	doc_title 	  varchar(256) 		NULL,
	waiting_time  int4 				NULL,
	creator_email varchar(128) 		NULL,
	file_num 	  int4 				NULL,
	file_list 	  text 				NULL,
	user_id 	  varchar(128) 		NULL,
	p_doc_no 	  varchar(20) 		NULL,
	exe_file 	  varchar(1024) 	NULL,
	in_process 	  varchar(1024) 	NULL,
	exe_rule 	  varchar(1024) 	NULL,
	authority 	  bpchar(1) 		NULL,
	approver_id   varchar(128) 		NULL,
	modifier_id   varchar(128) 		NULL,
	modified_dt   timestamp 		NULL,
	out_cl_cd 	  varchar(2) 		NULL,
	doc_msg 	  varchar(256) 		NULL,
	approval_msg  varchar(256) 		NULL,
	down_cnt 	  numeric(5) 		NULL,
	use_yn 		  varchar(1) 		NULL,
	in_cl_cd 	  varchar(1) 		NULL,
	file_del_yn   varchar(1) 		NULL,
	cre_type 	  varchar(1) 		NULL,
	room_id 	  varchar(32) 		NULL
);

CREATE INDEX cp_doc_master_xpk_cp_doc_master ON cp_doc_master USING btree (doc_no);

CREATE TABLE cp_doc_service (
	doc_no 	  	 	varchar(20) 	NOT NULL,
	subject   	 	varchar(256) 	NULL,
	sender_email 	varchar(256) 	NULL,
	file_num 	 	numeric(5) 		NULL,
	file_list 	 	varchar(4000) 	NULL,
	errmsg 		 	varchar(256) 	NULL,
	reg_dt 		 	timestamp 		NULL,
	reg_id 		 	varchar(128) 	NULL,
	chg_id 		 	varchar(128) 	NULL,
	receiver_email  varchar(4000)  	NULL,
	file_path 		varchar(256) 	NULL,
	errcode 		varchar(5) 		NULL,
	receiver_pw 	varchar(1000) 	NULL,
	down_url 		varchar(512) 	NULL,
	chg_dt 			timestamp 		NULL,
	sts_cd 			varchar(2) 		NULL,
	policy_no 		varchar(20) 	NULL
);

CREATE INDEX cp_doc_service_xpk_cp_doc_service ON cp_doc_service USING btree (doc_no);

CREATE TABLE cp_doc_user (
	doc_no 		 	varchar(20)    NOT NULL,
	company_id  	varchar(8) 	   NOT NULL,
	user_id 	 	varchar(128)   NOT NULL,
	"policy" 	 	varchar(20)    NULL,
	policy_doc 	 	varchar(20)    NULL,
	mod_stop 	    varchar(1) 	   NULL,
	expire_dt 	    varchar(10)    NULL,
	read_count      int4 		   NULL,
	message      	varchar(1024)  NULL,
	seq          	numeric(3) 	   NOT NULL,
	user_nm      	varchar(40)    NULL,
	mac_list 	 	varchar(1024)  NULL,
	chg_dt 		 	timestamp 	   NULL,
	chg_id 		 	varchar(128)   NULL,
	passwd 		 	varchar(128)   NULL,
	watermark_yn 	varchar(1) 	   NULL,
	vdi_yn 		 	varchar(1) 	   NULL, 
	ip_list 	 	varchar(1024)  NULL,
	policy_disp  	varchar(20)    NULL,
	policy_no    	varchar(20)    NULL,
	outdevice       varchar(32)    NULL,
	url_atth_cl_cd  varchar(1) 	   NULL,
	url_atth_lst    varchar(1024)  NULL
);

CREATE INDEX cp_doc_user_xpk_cp_doc_user ON cp_doc_user USING btree (doc_no, seq);

CREATE TABLE cp_exc_allo_folder (
	exc_no 		   varchar(10) 	  NOT NULL,
	folder_no 	   numeric(5) 	  NOT NULL,
	folder_nm 	   varchar(128)   NULL,
	folder_root    varchar(64) 	  NULL
);

CREATE INDEX cp_exc_allo_folder_xpk_cp_exc_policy ON cp_exc_allo_folder USING btree (exc_no, folder_no);

CREATE TABLE cp_exc_policy (
	exc_no 		    varchar(10)   NOT NULL,
	cp_exc_cl_cd    varchar(5) 	  NULL,
	exc_waiting_tm  numeric(5) 	  NULL,
	chg_id 			varchar(128)  NULL,
	chg_dt 			timestamp 	  NULL,
	exc_process 	varchar(256)  NULL
);

CREATE INDEX cp_exc_policy_xpkcp_exc_policy ON cp_exc_policy USING btree (exc_no);

CREATE TABLE cp_exc_rest_folder (
	exc_no 		varchar(10) 	NOT NULL,
	folder_no   numeric(5) 		NOT NULL,
	folder_nm   varchar(128) 	NULL,
	folder_root varchar(64) 	NULL
);

CREATE INDEX cp_exc_rest_folder_xpkcp_exc_rest_folder ON cp_exc_rest_folder USING btree (exc_no, folder_no);


CREATE TABLE cp_job_schedule (
	doc_no 	     varchar(20) 	NULL,
	sche_no      varchar(20) 	NOT NULL,
	cp_sts_cd    varchar(2)  	NULL,
	reg_dt 		 timestamp 		NULL,
	approve_dt   timestamp 		NULL,
	job_desc 	 varchar(512) 	NULL,
	job_progress numeric(3)  	NULL,
	file_cnt 	 numeric(3) 	NULL
);

CREATE INDEX cp_job_schedule_xpk_cp_job_schedule ON cp_job_schedule USING btree (sche_no);

CREATE TABLE cp_job_status (
	doc_no 		  varchar(200)  NULL,
	sche_no       varchar(20)   NOT NULL,
	cp_doc_status varchar(2) 	NULL,
	job_desc  	  varchar(512) 	NULL,
	job_progress  numeric(3) 	NULL,
	doc_title 	  varchar(256) 	NULL,
	exe_file 	  varchar(1024) NULL,
	file_num 	  numeric(5) 	NULL,
	user_id 	  varchar(128) 	NULL,
	reg_dt 		  timestamp  	NULL
);

CREATE INDEX cp_job_status_xpk_cp_job_status ON cp_job_status USING btree (sche_no);

CREATE TABLE cp_policy_grp (
	policy_no 	   varchar(20)	NOT NULL,
	"policy" 	   varchar(20)	NULL,
	policy_doc     varchar(20) 	NULL,
	policy_disp    varchar(20) 	NULL,
	expire_day     numeric(5) 	NULL,
	read_count     int4 		NULL,
	is_change 	   varchar(1) 	NULL,
	policy_nm 	   varchar(40) 	NULL,
	chg_dt 		   timestamp 	NULL,
	chg_id 		   varchar(128) NULL,
	watermark_yn   varchar(1) 	NULL,
	vdi_yn 		   varchar(1) 	NULL,
	ipaddr 		   varchar(2048)NULL,
	outdevice 	   varchar(32) 	NULL,
	policy_cl_cd   varchar(1) 	NULL,
	use_yn 		   varchar(1) 	NULL,
	is_ipaddr 	   varchar(1) 	NULL,
	mac_yn 		   varchar(1) 	NULL,
	url_atth_cl_cd varchar(1)   NULL,
	useredit_cd    varchar(1) 	NULL
);

CREATE INDEX cp_policy_grp_xpk_cp_policy_grp ON  cp_policy_grp USING btree (policy_no);

CREATE TABLE cp_policy_grp_lang (
	policy_no 	varchar(20) 	NOT NULL,
	policy_nm 	varchar(120) 	NULL,
	lang 		varchar(2) 		NOT NULL
);

CREATE TABLE cp_rep_schedule (
	doc_no 		 varchar(20)    NULL,
	sche_no 	 varchar(20)    NOT NULL,
	cp_sts_cd    varchar(2)  	NULL,
	reg_dt 	     timestamp 		NULL,
	approve_dt   timestamp 		NULL,
	job_desc 	 varchar(512) 	NULL,
	job_progress numeric(3) 	NULL
);

CREATE INDEX cp_rep_schedule_xpk_cp_rep_schedule ON cp_rep_schedule USING btree (sche_no);

CREATE TABLE custbill (
	cust_id     varchar(20)   NOT NULL,
	yyyymm 	    varchar(6) 	  NOT NULL,
	bill_amt    numeric(16)   NULL,
	bill_yn     varchar(1) 	  NULL,
	use_cnt   	numeric(10)   NULL,
	use_size 	numeric(16)   NULL,
	reg_dt 		timestamp 	  NULL,
	reg_id 		varchar(128)  NULL,
	chg_dt 		timestamp 	  NULL,
	chg_id 		varchar(128)  NULL,
	non_pay_yn 	varchar(1)    NULL
);

CREATE TABLE ex_collection (
	collection_id 		varchar(32) NOT NULL,
	collection_nm 		varchar(128) NULL,
	p_collection_id 	varchar(32) NULL,
	collection_desc 	varchar(512) NULL,
	creator_id 			varchar(20) NULL,
	create_dt 			timestamp NULL,
	collection_order 	numeric(5) NULL,
	collection_path 	varchar(512) NULL,
	owner_id 			varchar(20) NULL,
	modifier_id 		varchar(20) NULL,
	modified_dt 		timestamp NULL,
	owner_org_cd 		varchar(20) NULL,
	deleter_id 			varchar(20) NULL,
	deleted_dt 			timestamp NULL,
	is_delete 			varchar(1) NULL,
	collection_style 	bpchar(18) NULL,
	collection_security varchar(128) NULL
);

CREATE INDEX ex_collection_xpkex_collection ON ex_collection USING btree (collection_id);

CREATE TABLE mcode (
	cd 		 varchar(30)  NOT NULL,
	cd_nm 	 varchar(100) NULL,
	p_cd 	 varchar(30)  NULL,
	cd_type  varchar(2)   NULL,
	reg_dt 	 timestamp    NULL,
	reg_id 	 varchar(128) NULL,
	chg_dt 	 timestamp    NULL,
	chg_id   varchar(128) NULL,
	cd_order numeric(5)   NULL,
	use_yn 	 varchar(1)   NULL,
	cd_ref1  varchar(100) NULL,
	cd_ref2  varchar(100) NULL,
	cd_ref3  varchar(100) NULL,
	cd_key   varchar(50)  NULL,
	cd_enm   varchar(100) NULL
);

CREATE INDEX mcode_xpk_mcode ON mcode USING btree (cd);

CREATE TABLE mcode_lang (
	cd 		varchar(30)  NOT NULL,
	cd_nm   varchar(100) NULL,
	lang    varchar(2)   NOT NULL
);

CREATE TABLE mdept (
	dept_cd    varchar(20)  NOT NULL,
	dept_nm    varchar(100) NULL,
	p_dept_cd  varchar(12)  NULL,
	p_dept_nm  varchar(100) NULL,
	child_yn   varchar(1)   NULL,
	manager_id varchar(20)  NULL,
	reg_dt     timestamp    NULL,
	reg_id     varchar(128) NULL,
	chg_dt     timestamp    NULL,
	chg_id     varchar(128) NULL,
	use_yn     varchar(1)   NULL,
	dept_order numeric(5)   NULL,
	start_ymd  varchar(8)   NULL,
	end_ymd    varchar(8)   NULL,
	dept_enm   varchar(100) NULL,
	cust_id    varchar(20)  NULL,
	CONSTRAINT r_102 FOREIGN KEY (cust_id) REFERENCES syscustmst(cust_id)
);

CREATE INDEX mdept_xpk_mdept ON mdept USING btree (dept_cd);
CREATE UNIQUE INDEX ux_mdept ON mdept USING btree (dept_cd);

CREATE TABLE rd_userauth (
	seq 		numeric(10) NOT NULL,
	user_id 	varchar(20) NULL,
	user_nm 	varchar(50) NULL,
	rank_cd  	varchar(20) NULL,
	auth_grp_cd varchar(2)  NULL,
	auth_cd 	varchar(20) NULL,
	dept_cd 	varchar(50) NULL,
	rank_nm 	varchar(50) NULL,
	auth_grp_nm varchar(50) NULL,
	work_cd 	varchar(2)  NULL,
	auth_type 	varchar(2)  NULL
);

CREATE INDEX rd_userauth_xpk_rd_userauth ON rd_userauth USING btree (seq);

CREATE TABLE syscompany (
	addr 		  varchar(100)  NULL,
	reg_dt  	  timestamp 	NULL,
	reg_id  	  varchar(128)  NULL,
	chg_dt  	  timestamp 	NULL,
	chg_id  	  varchar(128)  NULL,
	use_yn  	  varchar(1)    NULL,
	company_id    varchar(8) 	NOT NULL,
	company_nm    varchar(50)   NULL,
	reg_no        varchar(13)   NULL,
	ceo_nm        varchar(50)   NULL,
	ceo_telno     varchar(12)   NULL,
	company_telno varchar(12)   NULL,
	cust_id 	  varchar(20)   NULL
);

CREATE INDEX syscompany_xpk_syscpmpany ON syscompany USING btree (company_id);

CREATE TABLE syscompuser (
	user_id 	varchar(128) 	NOT NULL,
	user_nm 	varchar(40) 	NULL,
	user_enm	varchar(50) 	NULL,
	passwd 		varchar(128) 	NULL,
	position_nm varchar(50) 	NULL,
	work_cd 	varchar(2) 		NULL,
	tel_no 		varchar(14) 	NULL,
	reg_dt 		timestamp 		NULL,
	reg_id 		varchar(128) 	NULL,
	chg_id 		varchar(128) 	NULL,
	company_id  varchar(8) 		NULL,
	chg_dt 		timestamp 		NULL,
	email 		varchar(128)	NULL,
	staff_id  	varchar(20) 	NULL,
	fail_cnt 	numeric(3) 		NULL,
	use_yn 		varchar(1)	 	NULL,
	mac_addr 	varchar(20) 	NULL,
	pin_no 		varchar(128) 	NULL
);

CREATE INDEX syscompuser_xpk_syscompuser ON syscompuser USING btree (user_id);

CREATE TABLE sysconfig (
	config_no 	 varchar(8) 	NOT NULL,
	config_id 	 varchar(64) 	NULL,
	config_val 	 varchar(512) 	NULL,
	config_desc  varchar(256) 	NULL,
	uinstsys_id  varchar(10) 	NOT NULL,
	config_cl_cd varchar(4) 	NOT NULL,
	CONSTRAINT sysconfig_pkey PRIMARY KEY (config_cl_cd, config_no, uinstsys_id)
);

CREATE INDEX sysconfig_xpk_sysconfig ON sysconfig USING btree (config_no, uinstsys_id, config_cl_cd);

CREATE TABLE syscustmst (
	cust_id 	 varchar(20) 	NOT NULL,
	cust_nm   	 varchar(50) 	NULL,
	cust_enm  	 varchar(100) 	NULL,
	addr 		 varchar(128) 	NULL,
	reg_no 		 varchar(13) 	NULL,
	ceo_nm 		 varchar(50) 	NULL,
	ceo_telno 	 varchar(20) 	NULL,
	use_yn 		 varchar(1) 	NULL,
	reg_dt 		 timestamp 		NULL,
	reg_id 		 varchar(128) 	NULL,
	chg_dt 		 timestamp 		NULL,
	chg_id 		 varchar(128) 	NULL,
	use_type 	 varchar(3) 	NULL,
	cust_m_telno varchar(20) 	NULL,
	cust_m_nm 	 bpchar(18) 	NULL,
	bill_type_cd varchar(3) 	NULL,
	CONSTRAINT syscustmst_pk PRIMARY KEY (cust_id)
);

CREATE UNIQUE INDEX ux_syscustmst ON syscustmst USING btree (cust_id);

CREATE TABLE syslangmsg (
	func_id 	varchar(32) 	NOT NULL,
	lang_cl_cd  varchar(2) 		NOT NULL,
	lang_key    varchar(128) 	NOT NULL,
	kr_nm 		varchar(256) 	NULL,
	us_nm 		varchar(256) 	NULL,
	ch_nm 		varchar(256) 	NULL,
	jp_nm 		varchar(256) 	NULL,
	e1_nm 		varchar(256) 	NULL,
	e2_nm 		varchar(256) 	NULL,
	e3_nm 		varchar(256) 	NULL,
	CONSTRAINT syslangmsg_pkey PRIMARY KEY (func_id, lang_cl_cd, lang_key)
);

CREATE INDEX syslangmsg_xpk_syslangmsg ON syslangmsg USING btree (func_id, lang_cl_cd, lang_key);

CREATE TABLE syslog (
	login_dt 	timestamp 		NULL,
	logout_dt 	timestamp 		NULL,
	client_ip 	varchar(15) 	NULL,
	log_status 	numeric(3) 		NULL,
	server_ip 	varchar(15) 	NULL,
	session_id 	varchar(256) 	NOT NULL,
	user_id 	varchar(256) 	NULL,
	server_port numeric(4) 		NULL
);
CREATE INDEX syslog_xpksyslog ON syslog USING btree (session_id);


CREATE TABLE sysmacro (
	macro_id 	varchar(32) 	NOT NULL,
	macro_nm 	varchar(32) 	NULL,
	macro_desc  varchar(256) 	NULL,
	func_id     varchar(32) 	NOT NULL,
	CONSTRAINT sysmacro_pkey PRIMARY KEY (func_id, macro_id)
);

CREATE INDEX sysmacro_xpk_sysmacro ON sysmacro USING btree (macro_id, func_id);

CREATE TABLE sysmain (
	user_id 	 varchar(128) 	NOT NULL,
	main_layout  varchar(50) 	NULL,
	portlets_cnt numeric(3) 	NULL
);

CREATE TABLE sysmainpersonal (
	main_no 	numeric(3) 		NOT NULL,
	user_id 	varchar(128) 	NOT NULL,
	portlets_id varchar(9) 		NULL
);

CREATE TABLE sysmenu (
	menu_id 	varchar(50) 	NOT NULL,
	menu_nm 	varchar(100) 	NULL,
	p_menu_id 	varchar(50) 	NULL,
	menu_order	numeric(4) 		NULL,
	menu_desc 	varchar(256) 	NULL,
	menu_sts 	varchar(10) 	NULL,
	menu_image  varchar(256) 	NULL,
	menu_url 	varchar(256) 	NULL,
	menu_size   numeric(4) 		NULL,
	reg_dt 		timestamp 		NULL,
	reg_id 		varchar(128) 	NULL,
	chg_dt 	 	timestamp 		NULL,
	chg_id 		varchar(128) 	NULL,
	menu_path 	varchar(256) 	NULL,
	use_yn 		varchar(1) 		NULL,
	menu_enm 	varchar(100) 	NULL,
	menu_epath 	varchar(256) 	NULL,
	CONSTRAINT sysmenu_pk PRIMARY KEY (menu_id)
);

CREATE INDEX sysmenu_xpk_sysmenu ON sysmenu USING btree (menu_id);

CREATE TABLE sysmenu_lang (
	menu_id 	varchar(50) 	NOT NULL,
	menu_nm 	varchar(100) 	NULL,
	lang 		varchar(2) 		NOT NULL
);

CREATE TABLE sysmenuroll (
	menu_id 	varchar(50) 	NOT NULL,
	roll_grp_cd varchar(8) 		NOT NULL,
	chg_dt 		timestamp 		NULL,
	chg_id 		varchar(128) 	NULL
);

CREATE INDEX sysmenuroll_xpk_sysmenuroll ON sysmenuroll USING btree (menu_id, roll_grp_cd);

CREATE TABLE syspersonaluser (
	p_user_id 	varchar(20) 	NOT NULL,
	p_user_nm 	varchar(40) 	NULL,
	passwd 		varchar(50) 	NULL,
	position_nm varchar(50) 	NULL,
	tel_no 		varchar(14) 	NULL,
	company_nm  varchar(50) 	NULL,
	email 		varchar(128) 	NULL,
	user_id 	varchar(128) 	NOT NULL
);

CREATE INDEX syspersonaluser_xpk_pcompuser ON syspersonaluser USING btree (user_id, p_user_id);

CREATE TABLE syspgminfo (
	item_cl_cd	  varchar(20) 	NULL,
	col_id 		  varchar(30) 	NULL,
	col_nm 		  varchar(128) 	NULL,
	col_cl_cd 	  varchar(20) 	NULL,
	col_format 	  varchar(50) 	NULL,
	col_width     varchar(10) 	NULL,
	col_max_len   varchar(10) 	NULL,
	col_align 	  varchar(20) 	NULL,
	col_sel_yn    varchar(1) 	NULL,
	col_hidden_yn varchar(1) 	NULL,
	col_order     varchar(5) 	NULL,
	col_color     varchar(20) 	NULL,
	col_combo_id  varchar(100) 	NULL,
	reg_ymd 	  timestamp 	NULL,
	reg_id 		  varchar(128)  NULL,
	chg_ymd 	  timestamp 	NULL,
	chg_id 		  varchar(128)  NULL,
	use_yn 		  varchar(1) 	NULL,
	col_image 	  varchar(128)  NULL,
	col_sort 	  varchar(12)   NULL,
	menu_id 	  varchar(50)   NOT NULL,
	item_id 	  varchar(50)   NOT NULL,
	col_pop_url   varchar(128) 	NULL,
	col_enm 	  varchar(128) 	NULL
);

CREATE INDEX syspgminfo_xpk_syspgminfo ON syspgminfo USING btree (menu_id, item_id);

CREATE TABLE syspgminfo_lang (
	item_id varchar(50) 	NOT NULL,
	col_nm 	varchar(128) 	NULL,
	lang 	varchar(2) 		NOT NULL
);

CREATE TABLE sysportlets (
	portlets_id 	varchar(9) 	 NOT NULL,
	portlets_nm 	varchar(50)  NULL,
	portlets_url 	varchar(256) NULL,
	portlets_height numeric(4)   NULL,
	portlets_enm 	varchar(50)  NULL,
	portlets_if 	varchar(10)  NULL,
	menu_id 		varchar(50)  NULL,
	sytem_cl_cd 	varchar(8)   NULL,
	portlets_width  numeric(4)   NULL
);

CREATE TABLE sysroll (
	roll_id varchar(12) 	NOT NULL,
	roll_nm varchar(100) 	NULL,
	use_yn  varchar(1) 		NULL
);

CREATE INDEX sysroll_xpk_sysroll ON sysroll USING btree (roll_id);

CREATE TABLE system_log (
	system_id 		varchar(32) 	NOT NULL,
	log_level 		varchar(16) 	NOT NULL,
	log_ymd   		varchar(8) 		NOT NULL,
	log__content 	varchar(2000) 	NULL,
	reg_dt 			timestamp 		NULL,
	ip_addr 		varchar(16) 	NULL,
	reg_id 			varchar(32) 	NULL
);

CREATE TABLE sysuser (
	user_id 	varchar(128) 	NOT NULL,
	user_nm 	varchar(40) 	NULL,
	user_enm 	varchar(50) 	NULL,
	dept_cd 	varchar(20) 	NULL,
	passwd 		varchar(50) 	NULL,
	position_cd varchar(12) 	NULL,
	function_cd varchar(12) 	NULL,
	work_cd 	varchar(2) 		NULL,
	entr_dt 	varchar(8)		NULL,
	email 		varchar(256) 	NULL,
	regist_no 	varchar(50) 	NULL,
	tel_no 		varchar(14) 	NULL,
	user_type 	varchar(2) 		NULL,
	reg_dt 		timestamp 		NULL,
	reg_id 		varchar(128) 	NULL,
	chg_id 		varchar(128) 	NULL,
	fail_cnt 	numeric(3) 		NULL,
	company_id  varchar(30) 	NULL,
	chg_dt 		timestamp 		NULL,
	emp_no 		varchar(20) 	NULL,
	self_exp_yn varchar(1) 		NULL,
	apv_ref_yn  varchar(1) 		NULL,
	cust_id 	varchar(20) 	NULL,
	company_nm  bpchar(18) 		NULL,
	p_install   varchar(1) 		NULL,
	jwt 		varchar(512) 	NULL,
	PW_ERROR 	numeric(9) 		NULL 	DEFAULT 0,
	CONSTRAINT sysuser_pk PRIMARY KEY (user_id),
	CONSTRAINT r_100 FOREIGN KEY (cust_id) REFERENCES syscustmst(cust_id),
	CONSTRAINT r_2 FOREIGN KEY (dept_cd) REFERENCES mdept(dept_cd)
);

CREATE TABLE sysuserapprover (
	user_id 	 varchar(128) 	NOT NULL,
	approver_id  varchar(128) 	NOT NULL,
	approver_nm  varchar(50) 	NULL
);

CREATE TABLE sysuserroll (
	user_id 	varchar(128) 	NOT NULL,
	tran_cls_cd varchar(2) 		NULL,
	roll_grp_cd varchar(8) 		NOT NULL,
	chg_dt 		timestamp 		NULL,
	chg_id 		varchar(128) 	NULL
);

CREATE INDEX sysuserroll_xpk_sysuserroll ON sysuserroll USING btree (user_id, roll_grp_cd);

CREATE TABLE tb_cust (
	addr 		varchar(100) 	NULL,
	reg_dt  	timestamp 		NULL,
	reg_id  	varchar(128) 	NULL,
	chg_dt  	timestamp 		NULL,
	chg_id  	varchar(128) 	NULL,
	use_yn  	varchar(1) 		NULL,
	cust_id 	varchar(8) 		NOT NULL,
	cust_nm 	varchar(128) 	NULL,
	reg_no  	varchar(13) 	NULL,
	ceo_nm      varchar(50) 	NULL,
	ceo_telno   varchar(12) 	NULL,
	telno 		varchar(12) 	NULL,
	cust_cl_cd  varchar(2) 		NULL,
	email 		varchar(128) 	NULL,
	cust_grp_cd varchar(2) 		NULL
);

CREATE INDEX tb_cust_xpk_cust ON tb_cust USING btree (cust_id);

CREATE TABLE tb_func (
	func_cd 	varchar(20) 	NOT NULL,
	func_nm 	varchar(128) 	NULL,
	reg_dt 		timestamp 		NULL,
	reg_id 		varchar(128) 	NULL,
	chg_dt 		timestamp 		NULL,
	chg_id 		varchar(128) 	NULL,
	func_cl_cd  varchar(12) 	NULL
);

CREATE TABLE tb_license (
	cust_id 	  varchar(8) 	NOT NULL,
	prod_id 	  varchar(12) 	NOT NULL,
	reg_id 		  varchar(128) 	NULL,
	reg_dt 		  timestamp 	NULL,
	chg_dt 		  timestamp 	NULL,
	chg_id 		  varchar(128) 	NULL,
	license_no    varchar(28) 	NULL,
	auth_key 	  varchar(28) 	NULL,
	issue_ymn 	  bpchar(18) 	NULL,
	issue_cl_cd   varchar(2) 	NULL,
	ma_cl_cd 	  varchar(2) 	NULL,
	user_id 	  varchar(20) 	NULL,
	issue_etc 	  varchar(512) 	NULL,
	prod_ver 	  varchar(16) 	NOT NULL,
	start_ymd 	  bpchar(18) 	NULL,
	end_ymd 	  bpchar(18) 	NULL,
	purc_cnt 	  numeric(8) 	NULL,
	corecpu_cnt   numeric(3) 	NULL,
	instance_cnt  numeric(3) 	NULL,
	user_cnt 	  numeric(8) 	NULL,
	provid_cd_yn  varchar(1) 	NULL,
	license_cl_cd varchar(2) 	NULL,
	use_yn 		  bpchar(18) 	NULL,
	minor_ver 	  varchar(16) 	NULL
);

CREATE TABLE tb_licensecp (
	prodno 		varchar(23)   NOT NULL,
	companycode varchar(5) 	  NOT NULL,
	otherdata1  varchar(2000) NULL,
	otherdata2  varchar(2000) NULL,
	otherdata3  varchar(2000) NULL,
	otherdata4  varchar(2000) NULL
);

CREATE INDEX tb_licensecp_xpktb_licensecp ON tb_licensecp USING btree (prodno, companycode);

CREATE TABLE tb_prod (
	prod_id   varchar(12) 	NOT NULL,
	prod_nm   varchar(128) 	NULL,
	reg_dt 	  timestamp		NULL,
	reg_id 	  varchar(128)  NULL,
	chg_dt 	  timestamp 	NULL,
	chg_id 	  varchar(128)  NULL,
	p_prod_id varchar(12)   NULL,
	prod_ver  varchar(16)   NOT NULL,
	p_prod_nm varchar(128)  NULL
);

CREATE TABLE tb_purchase (
	cust_id  varchar(8)   NOT NULL,
	prod_id  varchar(12)  NOT NULL,
	func_cd  varchar(20)  NOT NULL,
	reg_id   varchar(128) NULL,
	reg_dt   timestamp 	  NULL,
	chg_dt   timestamp 	  NULL,
	chg_id   varchar(128) NULL,
	prod_ver varchar(16)  NOT NULL
);

CREATE TABLE tboard (
	board_no 	 varchar(20) 	NOT NULL,
	btitle 	 	 varchar(256) 	NULL,
	bcontent 	 varchar(2000) 	NULL,
	reg_id 	 	 varchar(20) 	NULL,
	del_yn 	 	 varchar(2) 	NULL,
	board_cls_cd varchar(2) 	NULL,
	reg_dt 		 timestamp 		NULL,
	chg_id 		 varchar(20) 	NULL,
	chg_dt 		 timestamp 		NULL,
	p_board_no	 varchar(15) 	NULL,
	pop_yn 		 varchar(2) 	NULL,
	start_ymd 	 varchar(20) 	NULL,
	end_ymd 	 varchar(20) 	NULL,
	btext 		 varchar(2000)  NULL,
	ctx_cls_cd 	 varchar(2) 	NULL,
	top_yn 		 varchar(2) 	NULL
);

CREATE INDEX tboard_xpk_tboard ON tboard USING btree (board_no);

CREATE TABLE tfile_upload (
	pgm_id 	  	varchar(50) 	NOT NULL,
	doc_id 	  	varchar(128) 	NOT NULL,
	file_type 	varchar(10) 	NULL,
	file_path 	varchar(256) 	NOT NULL,
	file_name 	varchar(256) 	NOT NULL,
	file_size 	numeric(10) 	NULL,
	reg_dt 	  	timestamp 		NULL,
	reg_id 	  	varchar(128) 	NULL,
	file_icon 	varchar(128) 	NULL,
	file_del_yn varchar(1) 		NULL,
	v_key 		varchar(256) 	NULL,
	contents 	text 			NULL
);

CREATE INDEX tfile_upload_xpk_tfile_upload ON tfile_upload USING btree (pgm_id, doc_id, file_path, file_name);

CREATE TABLE vdr_comment (
	cmt_seq 	numeric(5) NOT NULL DEFAULT nextval('vdr_cmt_seq'::regclass),
	cmt_desc 	varchar(256) NULL,
	reg_dt 		timestamp NULL,
	reg_id 		varchar(128) NULL,
	folder_id 	varchar(32) NULL,
	room_id 	varchar(32) NOT NULL,
	doc_id 		varchar(32) NULL
);

CREATE TABLE vdr_config (
	conf_cl_cd  varchar(2) 	 NOT NULL,
	conf_key 	varchar(32)  NOT NULL,
	conf_val 	varchar(128) NULL
);

CREATE INDEX vdr_config_xpk_ex_com_config ON vdr_config USING btree (conf_cl_cd, conf_key);

CREATE TABLE vdr_doc (
	doc_id 			varchar(32) 	NOT NULL,
	doc_nm 			varchar(128) 	NULL,
	doc_extension 	varchar(8) 		NULL,
	doc_size 		numeric(16) 	NULL,
	is_lock 		varchar(1) 		NULL,
	creator_id 		varchar(128) 	NULL,
	is_delete 		varchar(1) 		NULL,
	create_dt 		timestamp 		NULL,
	owner_id 		varchar(128) 	NULL,
	modifier_id 	varchar(128) 	NULL,
	modified_dt 	timestamp 		NULL,
	deleter_id 		varchar(20) 	NULL,
	deleted_dt 		timestamp 		NULL,
	checkouter_id 	varchar(128) 	NULL,
	checkout_dt 	timestamp 		NULL,
	uuid 			varchar(32) 	NULL,
	ver 			varchar(9) 		NULL,
	is_favorites 	varchar(1) 		NULL,
	folder_id 		varchar(32) 	NULL,
	user_auth 		varchar(1) 		NULL DEFAULT 'N'::character varying,
	CONSTRAINT r_130 FOREIGN KEY (folder_id) REFERENCES vdr_folder(folder_id)
);

CREATE UNIQUE INDEX ux_vdr_doc ON vdr_doc USING btree (doc_id);
CREATE INDEX vdr_doc_xpkex_doc ON vdr_doc USING btree (doc_id);

CREATE TABLE vdr_doc_auth (
	r_user_id 	varchar(128) 	NOT NULL,
	room_id 	varchar(32) 	NOT NULL,
	aces 		varchar(1) 		NULL,
	red 		varchar(1) 		NULL,
	wrt 		varchar(1) 		NULL,
	prt 		varchar(1) 		NULL,
	chg_dt 		timestamp 		NULL,
	chg_id 		varchar(128) 	NULL,
	dlt 		varchar(1) 		NULL,
	doc_id 		varchar(32) 	NOT NULL,
	down 		varchar(1) 		NULL,
	"access" 	varchar(1) 		NULL DEFAULT 'N'::character varying,
	"view" 		varchar(1) 		NULL DEFAULT 'N'::character varying,
	edit 		varchar(1) 		NULL DEFAULT 'N'::character varying,
	CONSTRAINT vdr_doc_auth_pkey PRIMARY KEY (doc_id, r_user_id, room_id),
	CONSTRAINT r_123 FOREIGN KEY (r_user_id, room_id) REFERENCES vdr_room_join(r_user_id, room_id),
	CONSTRAINT r_128 FOREIGN KEY (doc_id) REFERENCES vdr_doc(doc_id)
);

CREATE TABLE vdr_doc_hist (
	doc_id 	 		varchar(32) 	NOT NULL,
	ver 	 		varchar(9) 		NOT NULL,
	uuid 	 		varchar(32) 	NULL,
	doc_nm 	 		varchar(128) 	NULL,
	doc_size 		numeric(16) 	NULL,
	doc_extension   varchar(8) 		NULL,
	modifier_id 	varchar(128) 	NULL,
	modified_dt 	timestamp 		NULL,
	CONSTRAINT vdr_doc_hist_pk PRIMARY KEY (doc_id, ver)
);

CREATE TABLE vdr_doc_ver (
	doc_id 	varchar(32) 	NOT NULL,
	ver 	varchar(9) 		NOT NULL,
	uuid 	varchar(32) 	NULL,
	reg_dt  timestamp 		NULL,
	reg_id  varchar(128) 	NULL
);

CREATE UNIQUE INDEX vdr_doc_ver_doc_id_idx ON vdr_doc_ver USING btree (doc_id, ver);

CREATE TABLE vdr_favorites (
	room_id 	varchar(32) 	NOT NULL,
	folder_id 	varchar(32) 	NOT NULL,
	doc_id 		varchar(32) 	NOT NULL,
	r_user_id 	varchar(128)	NOT NULL,
	favorite_yn varchar(1) 		NOT NULL,
	reg_dt 		timestamp 		NOT NULL,
	CONSTRAINT vdr_favorites_pk PRIMARY KEY (room_id, folder_id, doc_id, r_user_id)
);

CREATE TABLE vdr_folder (
	folder_nm 		varchar(128) 	NULL,
	p_folder_id 	varchar(32) 	NULL,
	folder_desc 	varchar(512) 	NULL,
	folder_style 	bpchar(18) 		NULL,
	folder_security varchar(128) 	NULL,
	folder_order 	numeric(5) 		NULL,
	folder_path 	varchar(512) 	NULL,
	creator_id 		varchar(128) 	NULL,
	create_dt 		timestamp 		NULL,
	owner_id 		varchar(128) 	NULL,
	owner_org_cd 	varchar(20) 	NULL,
	modifier_id 	varchar(128) 	NULL,
	modified_dt 	timestamp 		NULL,
	is_delete 		varchar(1) 		NULL,
	deleter_id 		varchar(128) 	NULL,
	deleted_dt 		timestamp 		NULL,
	folder_id 		varchar(32) 	NOT NULL,
	room_id 		varchar(32) 	NULL,
	user_auth 		varchar(1) 		NULL DEFAULT 'N'::character varying,
	CONSTRAINT r_131 FOREIGN KEY (room_id) REFERENCES vdr_room(room_id)
);

CREATE UNIQUE INDEX ux_vdr_folder ON vdr_folder USING btree (folder_id);

CREATE TABLE vdr_folder_auth (
	r_user_id 	varchar(128) 	NOT NULL,
	room_id 	varchar(32) 	NOT NULL,
	aces 		varchar(1) 		NULL,
	fad 		varchar(1) 		NULL,
	red 		varchar(1) 		NULL,
	wrt 		varchar(1) 		NULL,
	prt 		varchar(1) 		NULL,
	chg_dt 		timestamp 		NULL,
	chg_id	 	varchar(128)	NULL,
	dlt 		varchar(1) 		NULL,
	down 		varchar(1) 		NULL,
	folder_id 	varchar(32) 	NOT NULL,
	"access" 	varchar(1) 		NULL DEFAULT 'N'::character varying,
	"view" 		varchar(1) 		NULL DEFAULT 'N'::character varying,
	edit 		varchar(1) 		NULL DEFAULT 'N'::character varying,
	CONSTRAINT vdr_folder_auth_pkey PRIMARY KEY (folder_id, r_user_id, room_id),
	CONSTRAINT r_121 FOREIGN KEY (r_user_id, room_id) REFERENCES vdr_room_join(r_user_id, room_id)
);

CREATE TABLE vdr_log (
	room_id 	varchar(32) 	NOT NULL,
	log_seq 	serial 			NULL,
	log_cl_cd 	varchar(2) 		NULL,
	log_desc 	varchar(256) 	NULL,
	r_user_id 	varchar(128) 	NULL,
	reg_dt 		timestamp 		NULL,
	folder_id 	varchar(32) 	NULL,
	doc_id 		varchar(32) 	NULL
);

CREATE TABLE vdr_room (
	room_id 	varchar(32) 	NOT NULL,
	room_nm 	varchar(128) 	NULL,
	room_cl_cd  varchar(2) 		NULL,
	ctrt_no 	varchar(20) 	NULL,
	ctrt_id 	varchar(128) 	NULL,
	chg_dt 		timestamp 		NULL,
	chg_id 		varchar(128) 	NULL,
	reg_id 		varchar(128) 	NULL,
	reg_dt 		timestamp 		NULL,
	ctrt_dt 	varchar(8) 		NULL,
	room_desc 	varchar(256) 	NULL,
	CONSTRAINT r_113 FOREIGN KEY (ctrt_no) REFERENCES vdr_contract(ctrt_no)
);

CREATE UNIQUE INDEX ux_vdr_room ON vdr_room USING btree (room_id);

CREATE TABLE vdr_room_join (
	r_user_id 	 		varchar(128)	  NOT NULL,
	room_id 	 		varchar(32) 	  NOT NULL,
	creator_id 	 		varchar(128) 	  NULL,
	create_dt 	 		timestamp 		  NULL,
	mail_send_yn 		varchar(1) 	  	  NULL,
	join_yn 	 		varchar(1) 	 	  NULL,
	join_cl_cd 	 		varchar(2) 	  	  NULL,
	join_dt 	 		varchar(8) 	  	  NULL,
	mail_send_cert_id 	varchar(36) 	  NULL,
	"admin" 		    varchar(1) 		  NULL DEFAULT 'N'::character varying,
	CONSTRAINT r_115 FOREIGN KEY (room_id) REFERENCES vdr_room(room_id)
);

CREATE UNIQUE INDEX ux_vdr_room_join ON vdr_room_join USING btree (r_user_id, room_id);


CREATE TABLE vdr_room_user (
	"policy" 	 	varchar(20) 	NULL,
	policy_doc 	 	varchar(20) 	NULL,
	mod_stop 	 	varchar(1) 		NULL,
	expire_dt 	 	varchar(10) 	NULL,
	read_count   	int4 			NULL,
	message 	 	varchar(1024) 	NULL,
	user_nm 	 	varchar(40) 	NULL,
	mac_list 	 	varchar(1024) 	NULL,
	chg_dt 		 	timestamp 		NULL,
	chg_id 		 	varchar(128) 	NULL,
	passwd 		 	varchar(128) 	NULL,
	watermark_yn 	varchar(1) 		NULL,
	vdi_yn 		 	varchar(1) 		NULL,
	ip_list 	 	varchar(1024) 	NULL,
	policy_disp  	varchar(20) 	NULL,
	policy_no 	 	varchar(20) 	NULL,
	outdevice 	 	varchar(32) 	NULL,
	url_atth_cl_cd  varchar(1) 		NULL,
	url_atth_lst 	varchar(1024) 	NULL,
	room_id 		varchar(32) 	NOT NULL,
	r_user_id 		varchar(128) 	NOT NULL,
	"view" 			varchar(1) 		NULL DEFAULT 'N'::character varying,
	edit 			varchar(1) 		NULL DEFAULT 'N'::character varying,
	CONSTRAINT r_124 FOREIGN KEY (r_user_id, room_id) REFERENCES vdr_room_join(r_user_id, room_id)
);

CREATE INDEX vdr_room_user_xpk_cp_doc_user ON vdr_room_user USING btree (room_id, r_user_id);

CREATE TABLE vdr_room_auth_set (
	room_id 	    varchar(32) 	NOT NULL,
	chg_dt 		 	timestamp 		NOT NULL,
	chg_id 		 	varchar(128) 	NOT NULL,	
	ACCESS_SET      varchar(20)     NOT NULL,
	VIEW_SET        varchar(20)     NOT NULL,
	EDIT_SET        varchar(20)	    NOT NULL,
	CONSTRAINT r_115 FOREIGN KEY (room_id) REFERENCES vdr_room(room_id)
);

CREATE TABLE vdr_trash (
	folder_id 	varchar(32) 	NOT NULL,
	doc_id 		varchar(32) 	NOT NULL,
	reg_dt 		timestamp 		NULL,
	reg_id 		varchar(128) 	NULL,
	room_id 	varchar(32) 	NOT NULL,
	del_seq 	numeric(8) 		NOT NULL DEFAULT nextval('seq_trash'::regclass),
	del_cl_cd 	varchar(2) 		NULL,
	CONSTRAINT trash_pk PRIMARY KEY (del_seq)
);

CREATE TABLE vdr_contract_type (
	ctrt_code varchar(2) not null,
	max_user numeric(10,0) null,
	max_room numeric(10,0) null,
	max_storage numeric(20,0) null,
	max_auth varchar(1) null,	
	access_set varchar(20) null,
	view_set varchar(20) null,
	edit_set varchar(20) null,
	price    numeric(10) not null,
    constraint vdr_contract_type_pk primary key (ctrt_code)
);

CREATE SEQUENCE seq_dataroom START 1;

CREATE TABLE vdr_dataroom (
	dataroom_nid numeric(10) NOT NULL DEFAULT nextval('seq_dataroom'),
	patch_version varchar(10) not null,
	module_version varchar(10) null,
	patch_note text null,
	type varchar(2) not null,
    constraint vdr_dataroom_pk primary key (dataroom_nid)
);
