<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="com.core.component.util.WebUtils"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<% if (false) { // IDE를 위한 코드 %>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html><head/><body><div>
<% } %>

<script type="text/javascript"><!--

    var debugElement = "debug-selected";
    
    var debugId = "debug-request";

    function doDebug(element, id) {
        if (debugElement) {
            $(debugElement).className = "";
        }
        debugElement = element;
        debugElement.className = "selected";
        if (debugId) {
            Element.hide(debugId);
        }
        debugId = id;
        Element.show(debugId);
    }

//--></script>

<div id="debug">

<c:set var="target" value="debug-in" scope="request"/>
<c:set var="off" value="true" scope="request"/>

<div id="debug-in" style="display: none;">

<ul class="tab-menu">
    <li id="debug-selected" class="selected"
        onclick="doDebug(this, 'debug-request')">javax.servlet.http.HttpServletRequest
    </li>
    <li onclick="doDebug(this, 'debug-response')">javax.servlet.http.HttpServletResponse
    </li>
    <li onclick="doDebug(this, 'debug-session')">javax.servlet.http.HttpSession
    </li>
    <li onclick="doDebug(this, 'debug-context')">javax.servlet.ServletContext</li>
</ul>

<table id="debug-request">
    <tbody>
        <tr>
            <th>getParameters()</th>
            <td><textarea rows="7" cols="75"><%=WebUtils.getRequestMessage(request) %></textarea></td>
        </tr>
        <tr>
            <th>getAttributes()</th>
            <td><textarea rows="7" cols="75"><%=WebUtils.getRequestAttributeMessage(request) %></textarea></td>
        </tr>
        <tr>
            <th>getHeaders()</th>
            <td><textarea rows="7" cols="75"><%=WebUtils.getRequestHeaderMessage(request) %></textarea></td>
        </tr>
        <tr>
            <th>getCookies()</th>
            <td><textarea rows="7" cols="75"><%=WebUtils.getRequestCookieMessage(request) %></textarea></td>
        </tr>
        <tr>
            <th>getCharacterEncoding()</th>
            <td><%=request.getCharacterEncoding()%></td>
        </tr>
        <tr>
            <th>getContentLength()</th>
            <td><%=request.getContentLength()%></td>
        </tr>
        <tr>
            <th>getContentType()</th>
            <td><%=request.getContentType()%></td>
        </tr>
        <tr>
            <th>getLocale()</th>
            <td><%=request.getLocale()%></td>
        </tr>
        <tr>
            <th>getProtocol()</th>
            <td><%=request.getProtocol()%></td>
        </tr>
        <tr>
            <th>getRemoteAddr()</th>
            <td><%=request.getRemoteAddr()%></td>
        </tr>
        <tr>
            <th>getRemoteHost()</th>
            <td><%=request.getRemoteHost()%></td>
        </tr>
        <tr>
            <th>getScheme()</th>
            <td><%=request.getScheme()%></td>
        </tr>
        <tr>
            <th>getServerName()</th>
            <td><%=request.getServerName()%></td>
        </tr>
        <tr>
            <th>getServerPort()</th>
            <td><%=request.getServerPort()%></td>
        </tr>
        <tr>
            <th>isSecure()</th>
            <td><%=request.isSecure()%></td>
        </tr>
        <tr>
            <th>getRequestURL()</th>
            <td><%=request.getRequestURL()%></td>
        </tr>
        <tr>
            <th>getRequestURI()</th>
            <td><%=request.getRequestURI()%></td>
        </tr>
        <tr>
            <th>getServletPath()</th>
            <td><%=request.getServletPath()%></td>
        </tr>
        <tr>
            <th>getContextPath()</th>
            <td><%=request.getContextPath()%></td>
        </tr>
        <tr>
            <th>getAuthType()</th>
            <td><%=request.getAuthType()%></td>
        </tr>
        <tr>
            <th>getPathInfo()</th>
            <td><%=request.getPathInfo()%></td>
        </tr>
        <tr>
            <th>getMethod()</th>
            <td><%=request.getMethod()%></td>
        </tr>
        <tr>
            <th>getPathTranslated()</th>
            <td><%=request.getPathTranslated()%></td>
        </tr>
        <tr>
            <th>getQueryString()</th>
            <td><%=request.getQueryString()%></td>
        </tr>
        <tr>
            <th>getRemoteUser()</th>
            <td><%=request.getRemoteUser()%></td>
        </tr>
        <tr>
            <th>getRequestedSessionId()</th>
            <td><%=request.getRequestedSessionId()%></td>
        </tr>
        <tr>
            <th>getUserPrincipal()</th>
            <td><%=request.getUserPrincipal()%></td>
        </tr>
    </tbody>
</table>

<table id="debug-response" style="display: none;">
    <tbody>
        <tr>
            <th>getBufferSize()</th>
            <td><%=response.getBufferSize()%></td>
        </tr>
        <tr>
            <th>getCharacterEncoding()</th>
            <td><%=response.getCharacterEncoding()%></td>
        </tr>
        <tr>
            <th>getLocale()</th>
            <td><%=response.getLocale()%></td>
        </tr>
        <tr>
            <th>isCommitted()</th>
            <td><%=response.isCommitted()%></td>
        </tr>
    </tbody>
</table>

<table id="debug-session" style="display: none;">
    <tbody>
        <tr>
            <th>getAttributes()</th>
            <td><textarea rows="7" cols="75"><%=WebUtils.getSessionMessage(session) %></textarea></td>
        </tr>
        <tr>
            <th>getId()</th>
            <td><%=session.getId() %></td>
        </tr>
        <tr>
            <th>getCreationTime()</th>
            <td><%=session.getCreationTime() %></td>
        </tr>
        <tr>
            <th>getLastAccessedTime()</th>
            <td><%=new java.util.Date(session.getLastAccessedTime()) %></td>
        </tr>
        <tr>
            <th>getMaxInactiveInterval()</th>
            <td><%=session.getMaxInactiveInterval() %></td>
        </tr>
        <tr>
            <th>isNew()</th>
            <td><%=session.isNew() %></td>
        </tr>
    </tbody>
</table>

<table id="debug-context" style="display: none;">
    <tbody>
        <tr>
            <th>getServerInfo()</th>
            <td><%=application.getServerInfo()%></td>
        </tr>
        <tr>
            <th>getServletContextName()</th>
            <td><%=application.getServletContextName()%></td>
        </tr>
        <tr>
            <th>getVersion()</th>
            <td><%=application.getMajorVersion() + "." + application.getMinorVersion()%></td>
        </tr>
        <tr>
            <th valign="top">getAttributes()</th>
            <td><textarea rows="7" cols="75"><%=WebUtils.getApplicationAttributeMessage(application) %></textarea></td>
        </tr>
    </tbody>
</table>

</div>

</div>

<%
if (false) { // IDE를 위한 코드  %> </div></body></html> <% } 
%>
