<%@ page contentType = "text/html; charset=UTF-8" %>
<%String contextPath_css = request.getContextPath(); %>
	<link rel="stylesheet" href="<%=contextPath_css %>/ext/css/common.css" type="text/css">
	<link rel="stylesheet" href="<%=contextPath_css %>/ext/css/popup_style.css" type="text/css">
	<link rel="stylesheet" href="<%=contextPath_css %>/ext/css/board.css" type="text/css">

<style>
	.dhx_combo_select {overflow-y: scroll;}
    .grid_hover { background-color:#7FFFD4; font-size:20px; }
    .dhtmlxGrid_selection {
		-moz-opacity: 0.5;
		filter: alpha(opacity = 50);
		background-color:#83abeb;
		opacity:0.5;
	}
	.not_m_line {
        white-space:nowrap; overflow:hidden;
    }
    .dhx_header_cmenu{
        font-family:arial;
		font-size:12px;
		background-color:#ffffff;
		border:2px outset silver;
		z-index:2;
	}
	.dhx_header_cmenu_item{
		white-space:nowrap;
	}
	div.gridbox div.ftr td{
		font-family:arial;font-size:12px;color:black;background-color:#E5F2F8;
		border-color:#000000 #000000 #000000 #000000;
	}
	div.gridbox_light table.hdr td {
		text-align:center;
	}

.se_location { color:#477548; padding-top:5 }
.se_title { font-family:"Arial";color:#000000; padding-top:3px; font-size:14px; font-weight:bold; padding-top:3 }
.se_cell_title { background-image:url(../images/blt_srch.gif); background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#F6F6F6; text-align:left }
.se_cell_title1 { background-repeat:no-repeat;background-position:40%;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:5px;background-color:#F6F6F6; text-align:center; }
.se_cell_title2 { background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#F6F6F6; text-align:left }
.se_cell_title_option { background-image:url(../images/blt_srch.gif); background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#FAFADF; text-align:left }
.se_cell_title_must { background-image:url(../images/blt_srch.gif); background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#FAFAC1; text-align:left }
.se_cell_data { font-family:"Arial";padding-left:2px;background-color:#FFFFFF;color:black }
.se_cell_data_option { font-family:"Arial";padding-left:2px;background-color:#FAFADF;color:black }
.se_cell_data_must { font-family:"Arial";padding-left:2px;background-color:#FAFAC1;color:black }

/* NIMSOU 스타일 */

@font-face {
      src : url("../../fonts/NanumSquare_acB.ttf");
      font-family: "NanumSquareB";
}
@font-face {
      src : url("../../fonts/NanumSquare_acR.ttf");
      font-family: "NanumSquareR";
}
.hideScrollF::-webkit-scrollbar{
    display: none; /* Chrome, Safari, Opera*/
}
.hideScrollF {
    -ms-overflow-style: none; /* IE and Edge */
    scrollbar-width: none; /* Firefox */
}

/* Tahoma */

	.nimsou td > select {font-size: 14px; letter-spacing: 1px; margin: 10px 0px 0px 0px; font-family: Tahoma; border: 1px solid #1E96FF; border-radius: 5px; background-color: #FFFFFF; color: #000000;}
	.nimsou td > input {font-size: 14px; letter-spacing: 1px; margin: 10px 0px 0px 0px; font-family: Tahoma; border: 1px solid #1E96FF; border-radius: 5px; background-color: #FFFFFF; color: #000000;}
	.nimsou td > textarea {font-size: 14px; letter-spacing: 1px; margin: 10px 0px 0px 0px; font-family: Tahoma; border: 1px solid #1E96FF; border-radius: 5px; background-color: #FFFFFF; color: #000000;}
	.nimsou td > a {color: #ee0101;}
	.nimsou td > label {font-size: 14px; letter-spacing: 1px;margin: 10px 0px 0px 0px; font-family: Tahoma;}
	.nimsou td > label > input {font-size: 14px; letter-spacing: 1px;margin: 10px 0px 0px 0px; font-family: Tahoma;}
	.nimsou td {margin: 15px 0px 0px 0px;}
	.fs_large {font-size: 14px; letter-spacing: 1px; padding: 10px 0px 0px 0px; font-family: Tahoma; color: #000000;}
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    	-webkit-appearance: none;
    }
    .input-number-password {
    	-webkit-text-security: disc;
	}

	</style>
