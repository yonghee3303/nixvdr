    <link rel='STYLESHEET' type='text/css' href='/ext/css/style.css'>
	<link rel="STYLESHEET" type="text/css" href="/dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid.css">
	<link rel="STYLESHEET" type="text/css" href="/dhtmlx/dhtmlxGrid/codebase/dhtmlxgrid_skins.css">
	<link rel="STYLESHEET" type="text/css" href="/dhtmlx/dhtmlxGrid/codebase/ext/dhtmlxgrid_pgn_bricks.css">
	<link rel="STYLESHEET" type="text/css" href="/dhtmlx/dhtmlxCalendar/codebase/dhtmlxcalendar.css">
	<link rel="stylesheet" type="text/css" href="/dhtmlx/dhtmlxMenu/codebase/skins/dhtmlxmenu_standard.css">
	<link rel="stylesheet" type="text/css" href="/dhtmlx/dhtmlxWindows/codebase/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="/dhtmlx/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<style>
    .grid_hover { background-color:#7FFFD4; font-size:20px; }
    .dhtmlxGrid_selection {
		-moz-opacity: 0.5;
		filter: alpha(opacity = 50);
		background-color:#83abeb;
		opacity:0.5;
	}
	.not_m_line {
        white-space:nowrap; overflow:hidden;
    }
    .dhx_header_cmenu{
        font-family:arial;
		font-size:12px;
		background-color:#ffffff;
		border:2px outset silver;
		z-index:2;
	}
	.dhx_header_cmenu_item{
		white-space:nowrap;
	}
	div.gridbox div.ftr td{
		font-family:arial;font-size:12px;color:black;background-color:#E5F2F8;
		border-color:#000000 #000000 #000000 #000000;
	}
	div.gridbox_light table.hdr td {
		text-align:center;
	}

.se_location { color:#477548; padding-top:5 }
.se_title { font-family:"Arial";color:#000000; padding-top:3px; font-size:14px; font-weight:bold; padding-top:3 }
.se_cell_title { background-image:url(../images/blt_srch.gif); background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#F6F6F6; text-align:left }
.se_cell_title1 { background-repeat:no-repeat;background-position:40%;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:5px;background-color:#F6F6F6; text-align:center; }
.se_cell_title2 { background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#F6F6F6; text-align:left }
.se_cell_title_option { background-image:url(../images/blt_srch.gif); background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#FAFADF; text-align:left }
.se_cell_title_must { background-image:url(../images/blt_srch.gif); background-repeat:no-repeat;background-position:10px ;font-family:"Arial";font-weight:bold;font-size:12px;color:black;padding-left:25px;background-color:#FAFAC1; text-align:left }
.se_cell_data { font-family:"Arial";padding-left:2px;background-color:#FFFFFF;color:black }
.se_cell_data_option { font-family:"Arial";padding-left:2px;background-color:#FAFADF;color:black }
.se_cell_data_must { font-family:"Arial";padding-left:2px;background-color:#FAFAC1;color:black }

	</style>