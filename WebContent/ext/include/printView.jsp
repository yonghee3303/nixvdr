
<%@ page import="mg.base.DataFormater"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="java.util.*"%>
<%
	IParameterManagement param = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String col_del = param.getString("dhtmlx.separator.field");
	String row_del = param.getString("dhtmlx.separator.line");
	
	DataFormater sf = null;
	String [] header = null;
	StringBuffer exdsb = new StringBuffer();

	try {
		String headText = request.getParameter("headText") == null  ? "" : request.getParameter("headText");
		sf                = (DataFormater)request.getSession().getValue("excel_data");
		String gridColids = request.getParameter("gridColids") == null  ? "" : request.getParameter("gridColids");
		Vector grid_col_vec = null;
		String grid_col_name = "";
		String grid_ex_data  = "";
		
		if(sf != null && sf.getRowCount() > 0) {
			header = StringUtils.StrToArray(headText, col_del);
			grid_col_vec = StringUtils.StrToVector(gridColids,col_del);
			
			for(int j=0; j < grid_col_vec.size(); j++) {
				grid_col_name = (String)grid_col_vec.elementAt(j);
			}
			
			for(int i=0; i < header.length; i++) {
				if(i == 0) {
					exdsb.append("<table border=1 cellspacing=0 cellpadding=2 style=\"border:1px solid #000000;\"><tr><td align=left>"+header[i]+"</td>");
				} else {
					exdsb.append("<td align=left>" + header[i]+"</td>");
				}
			}

			if(header.length > 0) exdsb.append("</tr>");
			
			for(int k=0; k < sf.getRowCount(); k++)
			{
				exdsb.append("<tr>");
				for(int j=0; j < grid_col_vec.size(); j++) {
					grid_col_name = (String)grid_col_vec.elementAt(j);
					grid_ex_data  = sf.getValue(grid_col_name, k);
					exdsb.append("<td align=left>&nbsp;"+grid_ex_data+"</td>");
				}
				exdsb.append("</tr>");
			}
			
			if(sf.getRowCount() > 0) exdsb.append("</table>");

			out.println(exdsb.toString());
		}
	} catch(Exception e) {}
	finally {

	}
%>
<Script language="javascript">
	window.print();
</script>