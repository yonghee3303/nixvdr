<%@ page language="java" pageEncoding="UTF-8"%>
<% String context = request.getContextPath(); %>
<!-- 공통 스타일시트 -->
<link rel="stylesheet" type="text/css" href="<%= context%>/dhtmlx_v403/codebase/dhtmlx_2.css">

<!-- Layout 스타일시트 -->
<link rel="stylesheet" type="text/css" href="<%= context%>/dhtmlx_v403/sources/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_web.css">
<link rel="stylesheet" type="text/css" href="<%= context%>/dhtmlx_v403/sources/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%= context%>/dhtmlx_v403/sources/dhtmlxLayout/codebase/skins/dhtmlxlayout_dhx_terrace.css">

<!-- Grid 스타일시트 -->
<!-- <link rel="stylesheet" type="text/css" href="/dhtmlx_v403/sources/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_terrace.css"> -->
<link rel="stylesheet" href="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css">
<!-- <link rel="stylesheet" href="/dhtmlx_v403/sources/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_web.css"> -->

<!-- Tabbar 스타일시트 -->
<link rel="stylesheet" href="<%= context%>/dhtmlx_v403/sources/dhtmlxTabbar/codebase/skins/dhtmlxtabbar_dhx_skyblue.css">


<!-- Windows 스타일시트 -->
<!-- <link rel="stylesheet" href="/dhtmlx_v403/sources/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_terrace.css"> -->
<link rel="stylesheet" href="<%= context%>/dhtmlx_v403/sources/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_web.css">

<!-- Tabbar 스타일시트 -->
<link rel="stylesheet" href="<%= context%>/dhtmlx_v403/sources/dhtmlxTabbar/codebase/skins/dhtmlxtabbar_dhx_web.css">

<!-- Menu 스타일 시트 -->
<!-- <link rel="stylesheet" href="/dhtmlx_v403/sources/dhtmlxMenu/codebase/skins/dhtmlxmenu_dhx_skyblue.css">
 -->

<!-- 공통 스크립트 -->
<script  src="<%= context%>/dhtmlx_v403/codebase/dhtmlx.js"></script>
<script  src="<%= context%>/dhtmlx_v403/codebase/dhtmlx_deprecated.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCommon/codebase/connector.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCommon/codebase/dhtmlxcommon.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCommon/codebase/dhtmlxcontainer.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCommon/codebase/dhtmlxcore.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCommon/codebase/dhtmlxdataprocessor.js"></script>

<!-- Calendar 스크립트 -->
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCalendar/codebase/dhtmlxcalendar_deprecated.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxCalendar/codebase/ext/dhtmlxcalendar_double.js"></script>

<!-- Tabbar 스크립트 -->
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxTabbar/codebase/dhtmlxtabbar_deprecated.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxTabbar/codebase/dhtmlxtabbar_start.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxTabbar/codebase/dhtmlxtabbar.js"></script>

<!-- Chart 스크립트 -->
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxChart/codebase/dhtmlxchart.js"></script>
<script  src="<%= context%>/dhtmlx_v403/sources/dhtmlxChart/codebase/thirdparty/excanvas/excanvas.js"></script>

<!-- Menu 스크립트 -->
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxMenu/codebase/dhtmlxmenu.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxMenu/codebase/ext/dhtmlxmenu_ext.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxMenu/codebase/ext/dhtmlxmenu_effects.js"></script>

<!-- Tree 스크립트 -->
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/dhtmlxtree.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_attrs.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_dragin.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_ed.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_json.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_kn.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_lf.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_li.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_path.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_rl.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_sb.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_srnd.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_start.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxTree/codebase/ext/dhtmlxtree_xw.js"></script>

<!-- Layout 스크립트 -->
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxLayout/codebase/dhtmlxlayout.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxLayout/codebase/dhtmlxlayout_deprecated.js"></script>


<!-- Grid 스크립트 -->
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/dhtmlxgrid.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_data.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_drag.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_export.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_fast.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter_ext.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_filter.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_form.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_group.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_hextra.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_hmenu.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_json.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_markers.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_math.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_mcol.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_nxml.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_over.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_pgn.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_pivot.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_post.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_rowspan.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_selection.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_splt.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_srnd.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_ssc.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_start.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_undo.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_validation.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_acheck.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_calck.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_clist.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_cntr.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_combo.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_context.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_cor.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_dec.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_grid.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_limit.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_liveedit.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_mro.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_num.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_passw.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_ra_str.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_sub_row.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_time.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_tree.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_wbut.js"></script>


<!-- Windows 스크립트 -->
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxWindows/codebase/dhtmlxwindows_deprecated.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxWindows/codebase/ext/dhtmlxwindows_dnd.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxWindows/codebase/ext/dhtmlxwindows_menu.js"></script>
<script src="<%= context%>/dhtmlx_v403/sources/dhtmlxWindows/codebase/ext/dhtmlxwindows_resize.js"></script>


