<%@ page contentType = "text/html; charset=UTF-8" session="true"%>

<%@ page import="java.util.*"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.pgm.IPgm"%>
<%@ page import="com.core.component.pgm.IPgmManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.util.StringUtils"%>

<%
	IUser g_user = (IUser) session.getAttribute("j_user");
	IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String contextPath_grid = request.getContextPath();
	
	String formId = request.getParameter("formid");
	String grid_obj = request.getParameter("gridid");
		
	String col_del = paramemter.getString("dhtmlx.separator.field"); // CommonUtil.getConfig("sepoa.separator.field");
	String row_del = paramemter.getString("dhtmlx.separator.line");  // CommonUtil.getConfig("sepoa.separator.line");
	String page_count = paramemter.getString("grid.view.rowcount");
	
	IPgmManagement pgm = (IPgmManagement) ComponentRegistry.lookup(IPgmManagement.class);
	List gridcols = pgm.getCols(formId, "grid"); //화면ID, 구분키(프로그램 구분)
	
    String grid_header      = "";
    String grid_init_widths = "";
    String grid_col_align   = "";
    String grid_col_id      = "";
    String grid_col_color   = "";
    String grid_col_sort    = "";
    String grid_col_type    = "";
    String grid_combo_data  = "";
    String grid_num_format  = "";
    String grid_date_format = "";
    String grid_col_visible = "";
    String grid_selected    = "";
    String grid_footer_msg  = "\"Msg,";
    String grid_read_only_col = "";
    String grid_column_type  = "";
    String grid_back_color   = "";
    String grid_bk_color     = "";
    int    grid_read_check  = 0;
    int    grid_footer_cnt  = 0;
    
    //헤더 머지에 사용
    int    grid_head_merge_cnt = 0;
    String grid_attach_header         = "";
	String grid_attach_header_element = "";
	String grid_attach_header_name    = "";
	String grid_attach_header_value   = "";
	
	
	// Grid 전체 정보로 저장 (추
	
	// 화면에 행머지기능을 사용할지 여부의 구분
	boolean isRowsMergeable = false;
	// setHeader init 관련 구분
	boolean isReDrawHeader = false;
	//	 화면에 헤더 머지 기능의 관한 Vector
	Vector<String> dhtmlx_head_merge_cols_vec = new Vector<String>(); 
	//  Grid Back Color
	Vector dhtmlx_back_cols_vec = new Vector();
	//  Grid Read Only Col
	Vector dhtmlx_read_cols_vec = new Vector();
	// MultiGrid 여부
	boolean isMultiGridable = false;
	int dhtmlx_grid_cnt = 0;
	//cell고정기능 사용여부
	boolean isSplitGridable = false;
	int dhtmlx_split_cnt = 0; //고정시킬 cell의 개수.
	
	boolean isSelectScreen 	= false;	// 그리드가 조회용인지 저장용인지 구분하기 위함
	boolean devFlag = true; // param.getString("mouse.right.enable");  // 우측 마우스 사용금지
	boolean devCheckFlag = true;  // param.getString("server.development.flag");
			
	
	for (Iterator all = gridcols.iterator(); all.hasNext();) {
		IPgm cols = (IPgm) all.next();
		
		//grid_header       += cols.getName() + ",";
   		grid_init_widths  += cols.getProperty("COL_WIDTH").toString() + ",";
   		grid_col_align    += cols.getProperty("COL_ALIGN").toString() + ",";
   		grid_col_id       += cols.getProperty("COL_ID").toString() + ",";
   		grid_column_type   = cols.getProperty("COL_CL_CD").toString();
   		//grid_col_color    += cols.getProperty("COL_COLOR").toString() + ",";
   		//grid_col_sort     += cols.getProperty("COL_SORT").toString() + ",";
   		String columnName = "";
   		if("ko".equals(g_user.getLocale().getLanguage())) {
   			columnName = cols.getName();
		} else {
			if(cols.getProperty("NAME_EN") != null) {
				columnName = cols.getProperty("NAME_EN").toString();
			} else {
				columnName = cols.getName();
			}
		}

   		// 한 화면 formId(MENU_ID) 기준으로 Buyer 화면일 경우에는 컬럼이 ReadOnly 이고
		// Supplier 화면일 경우에 edit 일 경우에는 아래의 벡터 클래스에다가 컬럼명을 addElement 하시면 됩니다.
		// 변환되는 컬럼타입 기준은 아래와 같습니다.
		// ed -> ro(EditBox -> ReadOnlyBox),
		// edn, -> ron(NumberEditBox -> NumberReadOnlyBox),
		// dhxCalendar -> ro(CalendarBox -> ReadOnlyBox),
		// txt -> ro(TextBox -> ReadOnlyBox)
		// sepoa_grid_common.jsp 에서 컬럼타입을 변경 시켜 줍니다.
		// 참고로 Vector dhtmlx_read_cols_vec 객체는 ui_common.jsp에서 생성 시켜 줍니다.
		
		// 신코드	코드	TEXT1
		// 03	ch			CheckBox
		// 09	coro		ComboBox
		// 08	dhxCalendar	CalendarBox
		// 06	edn			NumberEditBox
		// 02	edtxt		EditBox
		// 04	img			ImageBox
		// 05	ro			ReadOnlyBox
		// 07	ron			NumberReadOnlyBox
		// 10	sub_row		SubTreeRow
		// 01	txttxt		TextBox
		
		//	String Mcolor = "#FAFAC1";
		//	String Ocolor = "#fdfdd4";
		//	String Rcolor = "#f7f7f7";
		//	String Scolor = "#f9f7b7";
		//	String Lcolor = "#FFFFFF";

				
		grid_col_type += grid_column_type + ",";
		
		//헤더 머지 기능 
		if(dhtmlx_head_merge_cols_vec != null && dhtmlx_head_merge_cols_vec.size() > 0) {
		//System.out.println("dhtmlx_head_merge_cols_vec.size = " + dhtmlx_head_merge_cols_vec.size());
			grid_head_merge_cnt = 0;
			
			for(int mr_depth =0; mr_depth < dhtmlx_head_merge_cols_vec.size(); mr_depth++)
    		{
    			grid_attach_header_element = (String)dhtmlx_head_merge_cols_vec.elementAt(mr_depth);
    			grid_attach_header_name    = grid_attach_header_element.substring(0, grid_attach_header_element.indexOf("="));
    			grid_attach_header_value   = grid_attach_header_element.substring(grid_attach_header_element.indexOf("=") + 1, grid_attach_header_element.length()).toLowerCase();
    			
    			if(grid_attach_header_name.equals(cols.getProperty("COL_ID").toString())) {
    				grid_header += grid_attach_header_value + ",";
   					grid_attach_header += "\""+ columnName +"\",";
    				grid_head_merge_cnt++;
    				break;
    			} 
    		}
    		
    		if(grid_head_merge_cnt == 0) {
    			grid_header += columnName + ",";
    			grid_attach_header += "\"#rspan\",";
    		}
			
		} else {
			grid_header += columnName + ",";
		}
		grid_head_merge_cnt = dhtmlx_head_merge_cols_vec.size();
		
		// alert(cols.getProperty("COL_FORMAT") );
		// String colFormat = StringUtils.Obj  cols.getProperty("COL_FORMAT").toString();
	
		// grid_column_type --> Combo 'SL0400','M210',''
   		if(grid_column_type.equals("coro") &&  cols.getProperty("COL_COMBO_ID") != "" &&  cols.getProperty("COL_COMBO_ID") != null) {
   			String tmpCd = cols.getProperty("COL_COMBO_ID").toString();
   			String tmpColId = cols.getProperty("COL_ID").toString();
   			String tmpval = cols.getPropertyString("COL_FORMAT");
   			grid_combo_data += " doRequestUsingPOST_dhtmlxGrid(grid_obj,\"" + tmpCd + "\", \"" + tmpColId + "\", \"" + tmpval + "\" ); \n";
   		}
		
		// grid_column_type -->  Format 지정
		if((grid_column_type.equals("edn") || grid_column_type.equals("ron")) && cols.getPropertyString("COL_FORMAT").length() > 0) 
		{
			grid_num_format += "grid_obj.setNumberFormat(\"" + cols.getPropertyString("COL_FORMAT") + "\", " + "grid_obj.getColIndexById(\""+cols.getPropertyString("COL_ID")+"\"));";
    	}
    	
    	// grid back Color 지정 --> Cell별 칼러를 넣는다.
    	if(cols.getProperty("COL_COLOR").toString().length() > 0)
   		{
   			grid_col_color    += cols.getProperty("COL_COLOR").toString() + ",";
   		}
   		
   		// grid_col_sort 지정
   		if(grid_column_type.equals("edn") || grid_column_type.equals("ron")) 
   		{   //number
   			grid_col_sort += "int,";
   		}
   		else
   		{
   			if(grid_column_type.equals("ch")) //check box
   			{
   				grid_col_sort += "na,";
   			}
   			else if(cols.getProperty("COL_SORT").toString().length() > 0){
   				grid_col_sort += cols.getProperty("COL_SORT").toString() + ",";
   			}
   			else { 
   				grid_col_sort += "str,";
   			}
   		}
	
    	// grid_column_type -->  Colendar 지정
    	if(grid_column_type.equals("dhxCalendar") && cols.getPropertyString("COL_FORMAT").length() > 0 && grid_date_format.length()== 0)
   		{
   			grid_date_format = "grid_obj.setDateFormat(\"" + cols.getPropertyString("COL_FORMAT") + "\");";
   		}

		// grid Hidden Col 처리
   		if(cols.getProperty("COL_HIDDEN_YN").toString().length() > 0 && cols.getProperty("COL_HIDDEN_YN").toString().equals("Y"))
   		{
   			grid_col_visible +=  "grid_obj.setColumnHidden(" + "grid_obj.getColIndexById(\""+cols.getProperty("COL_ID").toString()+"\")"+", true);";
   		}
		// grid Selected Col 처리
   		if(cols.getProperty("COL_SEL_YN").toString().equals("Y") && grid_selected.length()== 0)
   		{
   			grid_selected = cols.getProperty("COL_ID").toString();
   		}

    	grid_footer_cnt++;
   		if(grid_footer_cnt == 1) grid_footer_msg += "<div id='message' style='height:20'></div>";
   		else if(isSplitGridable && dhtmlx_split_cnt < grid_footer_cnt) continue; //splitAt()기능(셀고정) 사용시 하단 메세지 colspan 수 제어
   		else if(grid_footer_cnt > 2) grid_footer_msg += ",#cspan";
	}

    if(grid_header.length() > 0)      grid_header      = grid_header.substring(0, grid_header.length() - 1);
    if(grid_init_widths.length() > 0) grid_init_widths = grid_init_widths.substring(0, grid_init_widths.length() - 1);
    if(grid_col_align.length() > 0)   grid_col_align   = grid_col_align.substring(0, grid_col_align.length() - 1);
    if(grid_col_id.length() > 0)      grid_col_id      = grid_col_id.substring(0, grid_col_id.length() - 1);
    if(grid_col_color.length() > 0)   grid_col_color   = grid_col_color.substring(0, grid_col_color.length() - 1);
    if(grid_col_sort.length() > 0)    grid_col_sort    = grid_col_sort.substring(0, grid_col_sort.length() - 1);
    if(grid_col_type.length() > 0)    grid_col_type    = grid_col_type.substring(0, grid_col_type.length() - 1);
    if(grid_footer_msg.indexOf("cspan") > 0) grid_footer_msg += "\" ";
%>

<%@ include file="/ext/include/dhtmlx_scripts.jsp"%>

<script language='JavaScript'>
	var dhtmlx_start_row_id = 0;
	var dhtmlx_end_row_id = 0;
	var dhtmlx_cur_pagenumber = 1;
	var dhtmlx_last_row_id = 0;
	var dhtmlx_cur_page_last_idx = 0;
	var dhtmlx_before_row_id = 0;
	var dhtmlx_modal_end_cnt = 0;
	var dhxWins;
	var prg_win;
	var dhxWins_Filter;
	var prg_win_filter;

	//기존 소스
	//function setGridDraw(grid_obj)
	//contextPath 추가 : root
	function setGridDraw(grid_obj, root)
    {	
		//기존소스
		//grid_obj = new dhtmlXGridObject('gridbox');
		//contextPath 추가 : root
		grid_obj = new dhtmlXGridObject('gridbox', root);
		grid_obj.setImagePath("/dhtmlx/dhtmlxGrid/codebase/imgs/");
		grid_obj.setHeader("<%=grid_header%>");
		grid_obj.setInitWidths("<%=grid_init_widths%>");
	    grid_obj.setColAlign("<%=grid_col_align%>");
		grid_obj.setColumnIds("<%=grid_col_id%>");
	    grid_obj.setColumnColor("<%=grid_col_color%>");
		grid_obj.setColSorting("<%=grid_col_sort%>");
		grid_obj.setColTypes("<%=grid_col_type%>");

		// 헤더머지
		<% if (grid_head_merge_cnt > 0) { %>
			grid_obj.attachHeader([<%=grid_attach_header%>]);
		<% } %>
		
		<%=grid_combo_data%>
		
		grid_obj.setColumnMinWidth(50,0);

		MenuObj = new dhtmlXMenuObject(null,"standard");
		MenuObj.setImagePath("/dhtmlx/dhtmlxMenu/codebase/imgs/");
		MenuObj.setIconsPath("/ext/images/");
		MenuObj.renderAsContextMenu();
		MenuObj.setOpenMode("web");
		MenuObj.attachEvent("onClick", doOnMouseRightButtonClick);
		MenuObj.loadXML("/dhtmlx/dhtmlxMenu/dyn_context.xml");
		
		grid_obj.enableContextMenu(MenuObj);
		grid_obj.enableMultiselect(false);
		
		grid_obj.attachEvent("onRowDblClicked",doOnRowSelected);
		// grid_obj.attachEvent("onRowSelect",doOnRowSelect);
		//팝업오픈 원클릭 onRowSelect
		//팝업오픈 더블클릭 onRowDblClicked
		grid_obj.enableEditEvents(true,false,false); // dbl click를 추가한 후 edit event 추가함.click, dbl click, f2key
		grid_obj.attachEvent("onBeforeContextMenu", doOnRightButtonShowMenu);
		grid_obj.attachEvent("onBeforePageChanged", doOnBeforePageChanged);
		grid_obj.attachEvent("onKeyPress", doOnKeyPressed);
		grid_obj.attachEvent("onXLS",doQueryDuring);
		grid_obj.attachEvent("onXLE",doQueryModalEnd);
		grid_obj.attachEvent("onXLE",doQueryEnd);

		grid_obj.setSkin("light");
		
		<% if(!isSelectScreen) { %>
		    grid_obj.attachEvent("onEditCell",doOnCellEdit);
			grid_obj.attachEvent("onEditCell",doOnCellChange);
		<% } %>

	    //grid_obj.enableHeaderMenu();
	    grid_obj.enableUndoRedo();
	    // grid_obj.enableRowsHover(true,'grid_hover');
	    grid_obj.enableBlockSelection();

	    grid_obj.enableAutoSizeSaving();
        grid_obj.enableOrderSaving();
        //grid_obj.loadOrderFromCookie();
	    //grid_obj.loadSizeFromCookie();

	    //grid_obj.enableMultiline(true);
		//grid_obj.enableColumnMove(true);
		//grid_obj.enableMathEditing(true);
		
		//cell고정기능 사용시 컬럼의 위치변경 기능은 사용할 수 없습니다.
		<% if(isSplitGridable) { %>
			grid_obj.splitAt(<%=dhtmlx_split_cnt%>);
		<% }else{ %>
			grid_obj.enableColumnMove(true);
		<% } %>

		//grid_obj.attachFooter(<%=grid_footer_msg%>);
		<%=grid_num_format%>
	    <%=grid_date_format%>
	    <%=grid_col_visible%>

	    // 행머지 기능과 SmartRendering 기능을 같이 사용을 할수가 없어서 아래와 같이 사용하였습니다.
	    <% if(isRowsMergeable) { %>
	    	grid_obj.enableRowspan(true);
	    <% } %>

	    // 조회전용 화면은 SmartRendering 을 합니다.
	   
		<% if(isSelectScreen) { %>
			grid_obj.enableSmartRendering(true);
		<% } %>
	
		// Rendering 기능 적
		// grid_obj.enableSmartRendering(true);
		
		// 각 화면에서 setHeader를 다시 그릴때 사용하는 변수입니다. 다시 그려줄때는 true 로 셋팅합니다.
		<% if(!isReDrawHeader) { %>
		    grid_obj.init();
		<% } %>
		//grid_obj.enableLightMouseNavigation(true);
		
		return grid_obj;
    }
    
	
	function setGridReDraw(grid_obj, gridHeader)
    {   
    	// alert("<%=formId%>");
    	// backSpaceFalse();
    	grid_obj = new dhtmlXGridObject('gridbox');
		grid_obj.setImagePath("/dhtmlx/dhtmlxGrid/codebase/imgs/");
		grid_obj.setHeader(gridHeader);
		grid_obj.setInitWidths("<%=grid_init_widths%>");
	    grid_obj.setColAlign("<%=grid_col_align%>");
		grid_obj.setColumnIds("<%=grid_col_id%>");
	    grid_obj.setColumnColor("<%=grid_col_color%>");
		grid_obj.setColSorting("<%=grid_col_sort%>");
		grid_obj.setColTypes("<%=grid_col_type%>");

		// 헤더머지
		<% if (grid_head_merge_cnt > 0) { %>
			grid_obj.attachHeader([<%=grid_attach_header%>]);
		<% } %>
		
		<%=grid_combo_data%>
		
		grid_obj.setColumnMinWidth(50,0);

		MenuObj = new dhtmlXMenuObject(null,"standard");
		MenuObj.setImagePath("/dhtmlx/dhtmlxMenu/codebase/imgs/");
		MenuObj.setIconsPath("/ext/images/");
		MenuObj.renderAsContextMenu();
		MenuObj.setOpenMode("web");
		MenuObj.attachEvent("onClick", doOnMouseRightButtonClick);
		MenuObj.loadXML("/dhtmlx/dhtmlxMenu/dyn_context.xml");
		
		grid_obj.enableContextMenu(MenuObj);
		grid_obj.enableMultiselect(false);
		
		grid_obj.attachEvent("onRowDblClicked",doOnRowSelected);
		// grid_obj.attachEvent("onRowSelect",doOnRowSelect);
		//팝업오픈 원클릭 onRowSelect
		//팝업오픈 더블클릭 onRowDblClicked
		grid_obj.enableEditEvents(true,false,false); // dbl click를 추가한 후 edit event 추가함.click, dbl click, f2key
		grid_obj.attachEvent("onBeforeContextMenu", doOnRightButtonShowMenu);
		grid_obj.attachEvent("onBeforePageChanged", doOnBeforePageChanged);
		grid_obj.attachEvent("onKeyPress", doOnKeyPressed);
		grid_obj.attachEvent("onXLS",doQueryDuring);
		grid_obj.attachEvent("onXLE",doQueryModalEnd);
		grid_obj.attachEvent("onXLE",doQueryEnd);

		grid_obj.setSkin("light");
		
		<% if(!isSelectScreen) { %>
		    grid_obj.attachEvent("onEditCell",doOnCellEdit);
			grid_obj.attachEvent("onEditCell",doOnCellChange);
		<% } %>

	    //grid_obj.enableHeaderMenu();
	    grid_obj.enableUndoRedo();
	    // grid_obj.enableRowsHover(true,'grid_hover');
	    grid_obj.enableBlockSelection();

	    grid_obj.enableAutoSizeSaving();
        grid_obj.enableOrderSaving();
        //grid_obj.loadOrderFromCookie();
	    //grid_obj.loadSizeFromCookie();

	    //grid_obj.enableMultiline(true);
		//grid_obj.enableColumnMove(true);
		//grid_obj.enableMathEditing(true);
		
		//cell고정기능 사용시 컬럼의 위치변경 기능은 사용할 수 없습니다.
		<% if(isSplitGridable) { %>
			grid_obj.splitAt(<%=dhtmlx_split_cnt%>);
		<% }else{ %>
			grid_obj.enableColumnMove(true);
		<% } %>

		//grid_obj.attachFooter(<%=grid_footer_msg%>);
		<%=grid_num_format%>
	    <%=grid_date_format%>
	    <%=grid_col_visible%>

	    // 행머지 기능과 SmartRendering 기능을 같이 사용을 할수가 없어서 아래와 같이 사용하였습니다
	    <% if(isRowsMergeable) { %>
	     	grid_obj.enableRowspan(true);
	    <% } %>

	    // 조회전용 화면은 SmartRendering 을 합니다.
	   
		<% if(isSelectScreen) { %>
			grid_obj.enableSmartRendering(true);
		<% } %>
	
		// Rendering 기능 적
		// grid_obj.enableSmartRendering(true);
		
		// 각 화면에서 setHeader를 다시 그릴때 사용하는 변수입니다. 다시 그려줄때는 true 로 셋팅합니다.
		<% if(!isReDrawHeader) { %>
		    grid_obj.init();
		<% } %>
		//grid_obj.enableLightMouseNavigation(true);
		
		return grid_obj;
    }

    function doOnKeyPressed(code,ctrl,shift)
    {
    	if(code==67 && ctrl){
	        GridObj.setCSVDelimiter("\t");
			GridObj.copyBlockToClipboard();
	    }
	    return true;
    }

    function doOnMouseRightButtonClick(menuitemId)
    {
	    var data = GridObj.contextID.split("_"); //rowInd_colInd
		var rId  = data[0];
		var cInd = data[1];

        switch(menuitemId) {
        	case "SaveCookie":
            	/*  */
                break;
            case "LoadCookie":
                //GridObj.loadSizeFromCookie();
            	//GridObj.loadOrderFromCookie();
                break;
            case "ClearCookie":
                /* gridOrderWidthClear(GridObj, ' */
                break;
            case "AllCheckLock":
            	if(GridObj.getColType(cInd) == "ch" && <%=isSelectScreen%> == false) {
            		
            		if(GridObj.getRowsNum() > 1000) {
            			alert("데이터 조회 건수가 많습니다. 1000 건 까지만 처리 가능합니다.");
            			break;
            		} else {
            		
            			for(var row = 0; row < GridObj.getRowsNum(); row++)
						{
							//GridObj.cells(row, cInd).setValue("1");
							GridObj.cells(GridObj.getRowId(row), cInd).setValue("1");
							GridObj.cells(GridObj.getRowId(row), cInd).cell.wasChanged = true;
					    }
            		}
            		
            		//GridObj.setCheckedRows(cInd, "true");
            	}
                break;
            case "AllCheckUnLock":
                if(GridObj.getColType(cInd) == "ch" && <%=isSelectScreen%> == false) {
                	if(GridObj.getRowsNum() > 1000) {
            			alert("데이터 조회 건수가 많습니다. 1000 건 까지만 처리 가능합니다.");
            			break;
            		} else {
	                	for(var row = 0; row < GridObj.getRowsNum(); row++)
						{
							GridObj.cells(GridObj.getRowId(row), cInd).setValue("0");
							GridObj.cells(GridObj.getRowId(row), cInd).cell.wasChanged = false;
					    }
					}
            	}
                break;
            case "CellCopy":
            	if(GridObj.getColType(cInd) == "coro") {
            		window.clipboardData.setData("Text", GridObj.cells(rId, cInd).getTitle());
            	} else {
            		window.clipboardData.setData("Text", GridObj.cells(rId, cInd).getValue().toString());
            	}
                break;
            case "ColFrozen":
                //GridObj.splitAt(cInd);
                break;
            case "ExcelSave":
                gridExport(GridObj);
                break;
            case "PrintView":
                PrintView(GridObj);
                break;
            case "Undo":
                GridObj.doUndo();
                break;
            case "Redo":
                GridObj.doRedo();
                break;
            case "Filter":
                doFiltering(GridObj);
                break;
            case "CellDataInit":
                GridObj.cells(rId, cInd).setValue("");
                break;
        }
        //return true;
	}

	function doOnRightButtonShowMenu(rowId,celInd,grid) {
		return true;
	}

	function doOnPageChanged(cur_page, first_page, last_page)
	{
		if(dhxWins != null && prg_win != null && dhxWins.window("prg_win").isModal()) {
			//dhxWins.window("prg_win").setModal(false);
			//dhxWins.window("prg_win").hide();
		}

		var page_cnt = parseInt("<%=page_count%>");
		dhtmlx_start_row_id   = (cur_page - 1) * page_cnt;
		dhtmlx_end_row_id = cur_page * page_cnt;
		dhtmlx_cur_pagenumber = cur_page;

		if(GridObj.getRowsNum() <= <%=page_count%>) {
			dhtmlx_end_row_id = GridObj.getRowsNum();
			dhtmlx_cur_page_last_idx = GridObj.getRowsNum();
		} else {
			dhtmlx_cur_page_last_idx = cur_page * <%=page_count%>;
		}

		if(dhtmlx_end_row_id > GridObj.getRowsNum()) {
			dhtmlx_end_row_id = GridObj.getRowsNum();
		}

		return true;
	}

	function doOnBeforePageChanged(cur_page, page_cnt)
	{
		if(dhxWins != null && prg_win != null && !dhxWins.window("prg_win").isModal()) {
			//dhxWins.window("prg_win").setModal(true);
			//dhxWins.window("prg_win").show();
		}

		return true;
	}


	function gridExport(mygrid)
	{
	    var html="";
	    var headText="";
	    var gridColids="";
	    var numCols = mygrid.getColumnsNum();

	    if(mygrid.getRowsNum() < 1) return;

	    for(i=0; i < numCols; i++)
	    {
	    	if(!mygrid.isColumnHidden(i) && mygrid.getColumnId(i) != "<%=grid_selected%>") {
				if(LRTrim(mygrid.getColumnLabel(i)) == "") {
					headText   = headText + mygrid.getColumnId(i) + "<%=col_del%>";
				} else {
					headText   = headText + mygrid.getColumnLabel(i)+ "<%=col_del%>";
				}
				gridColids = gridColids + mygrid.getColumnId(i)+ "<%=col_del%>";
			}
	    }
	    
	    window.clipboardData.setData("text","");
	    mygrid.gridToClipboard();
	    // var griddata = window.clipboardData.getData("text");
	     
	    // alert(griddata);
		/* 
	    document.formstyle.headText.value=headText;
	    document.formstyle.gridColids.value=gridColids;
	    document.formstyle.csvBuffer.value=griddata;
        document.formstyle.method='GET';
	    document.formstyle.action='/ext/include/csvExport.jsp';
	    document.formstyle.target='WholeHidden';
	    document.formstyle.submit();
	    */
	    
	    
	    openExcel();
	    
	    return;
	}
	
	var excel = null;

function openExcel()
{
	try	{
	
		var objExcel = new ActiveXObject("excel.Application");
		var objWorkbook = objExcel.Workbooks.add();
		var objWorksheet;  							//WorkBook의 WorkSheet 변수
		objExcel.DisplayAlerts = false;
					
		while (objWorkbook.Worksheets.Count > 1) {
			objWorksheet = objWorkbook.Worksheets.Item(objWorkbook.Worksheets.Count);
			objWorksheet.Delete;
		}
	
		if(excel == null)		{	
		 excel = new ActiveXObject("Excel.Application");
		}
		var workbook = excel.Workbooks.Add();

		workbook.Activate();
		var worksheet = workbook.Worksheets("Sheet1");
		worksheet.Activate();

		worksheet.Paste();

		excel.visible=true;

	}catch(exception)	{
	   window.alert("Now you may Paste into an Excel SpreadSheet");
	}
}


	function gridOrderWidthSave(mygrid, screen_id, user_id)
	{
	    var html="";
	    var headText="";
	    var headWidth="";
	    var numCols = mygrid.getColumnsNum();

	    if(mygrid.getRowsNum() < 1) return;

	    for(i=0; i < numCols; i++)
	    {
	    	headText  = headText + mygrid.getColumnId(i)+ "<%=col_del%>";
	    	headWidth = headWidth+ mygrid.getColWidth(i)+ "<%=col_del%>";
	    }

	    headText = headText + "<%=row_del%>";
	    headWidth = headWidth + "<%=row_del%>";

	    document.formstyle.headText.value=headText;
        document.formstyle.headWidth.value=headWidth;
        document.formstyle.screen_id.value=screen_id;
        document.formstyle.user_id.value=user_id;
        document.formstyle.method='POST';
	    document.formstyle.action='/common/gridOrderWidthSave.jsp';
	    document.formstyle.target='WholeHidden';
	    document.formstyle.submit();
	    return;
	}

	function gridOrderWidthClear(mygrid, screen_id, user_id)
	{
		if(mygrid.getRowsNum() < 1) return;

	    document.formstyle.screen_id.value=screen_id;
        document.formstyle.user_id.value=user_id;
        document.formstyle.method='POST';
	    document.formstyle.action='/common/gridOrderWidthClear.jsp';
	    document.formstyle.target='WholeHidden';
	    document.formstyle.submit();
	    return;
	}

	function PrintView(mygrid)
	{
	    var html="";
	    var headText="";
	    var gridColids="";
	    var numCols = mygrid.getColumnsNum();

	    if(mygrid.getRowsNum() < 1) return;

	    for(i=0; i < numCols; i++)
	    {
	    	if(!mygrid.isColumnHidden(i) && mygrid.getColumnId(i) != "<%=grid_selected%>") {
	    		headText   = headText + mygrid.getColumnLabel(i)+ "<%=col_del%>";
	    	    gridColids = gridColids + mygrid.getColumnId(i)+ "<%=col_del%>";
	    	}
	    }
	    document.formstyle.headText.value=headText;
	    document.formstyle.gridColids.value=gridColids;
        document.formstyle.method='POST';
	    document.formstyle.action='/ext/include/printView.jsp';
	    document.formstyle.target='_blank';
	    document.formstyle.submit();
	    return;
	}

	function BwindowWidth() {
		 if (window.innerWidth) {
			 return window.innerWidth;
		 } else if (document.body && document.body.offsetWidth) {
		 	return document.body.offsetWidth;
		 } else {
			 return 0;
		}
	}

	function BwindowHeight() {
		 if (window.innerHeight) {
		 	return window.innerHeight;
		 } else if (document.body && document.body.offsetHeight) {
		 	return document.body.offsetHeight;
		 } else {
		 	return 0;
		}
	}

	function doQueryDuring()
	{
		var dim  = new Array(2);
		var top  = "";
		var left = "";
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;

		if(dhxWins == null) {
        	dhxWins = new dhtmlXWindows();
        	dhxWins.setImagePath("/dthmlx_v403/dhtmlxWindows/codebase/imgs/");
        	//dhxWins.enableAutoViewport(false);
			//dhxWins.setViewport(0, 0, BwindowWidth()-20, 100);
        }
 
		if(prg_win == null) {
			prg_win = dhxWins.createWindow("prg_win", top, left, 180, 73);
			prg_win.setText("Please wait for a moment.");
			prg_win.button("close").hide();
			prg_win.button("minmax1").hide();
			prg_win.button("minmax2").hide();
			prg_win.button("park").hide();
			dhxWins.window("prg_win").setModal(true);
			prg_win.attachURL("<%=contextPath_grid%>/ext/include/progress_ing.htm");
		} else {
			dhxWins.window("prg_win").setModal(true);
		    dhxWins.window("prg_win").show();
		}

		<% if(!isRowsMergeable) { %>
	    	GridObj.enableSmartRendering(true);
	    <% } %>
		return true;
	}

	function doFiltering(mygrid)
	{
		var dim  = new Array(2);
		var width  = "550";
		var height = "120";
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;
		var headText="";
	    var numCols = mygrid.getColumnsNum();
	    var gridColids = "";

	    if(mygrid.getRowsNum() < 1) return;

	    for(i=0; i < numCols; i++)
	    {
	    	if(!mygrid.isColumnHidden(i)) {
	    		headText = headText + mygrid.getColumnLabel(i)+ ",";
	    		gridColids = gridColids + mygrid.getColumnId(i)+ ",";
	    	}
	    }

	    if(headText.length > 0) {
	    	headText = headText.substring(0, headText.length - 1);
	    }

	    if(gridColids.length > 0) {
	    	gridColids = gridColids.substring(0, gridColids.length - 1);
	    }

		var url = "/ext/include/do_filter.jsp?grid_col_id="+gridColids+"&grid_col_lable="+headText;
		CodeSearchCommon(url, 'FiterScreen', left, top, width, height);
	}

	function doFilterSearch(filter_column, filter_word)
	{
		GridObj.filterBy(grid_obj.getColIndexById(filter_column), filter_word);
	}

	function doQueryModalEnd(GridObj, RowCnt)
	{
		var msg = GridObj.getUserData("", "message");
		
		if(dhxWins == null) {
        	dhxWins = new dhtmlXWindows();
        	dhxWins.setImagePath("/dthmlx_v403/dhtmlxWindows/codebase/imgs/");
        }

		if(prg_win == null) {
			var top = BwindowWidth()/2 - 180;
			var left  = BwindowHeight()/2 - 73;
		
			prg_win = dhxWins.createWindow("prg_win", top, left, 180, 73);
			prg_win.setText("Please wait for a moment.");
			prg_win.button("close").hide();
			prg_win.button("minmax1").hide();
			prg_win.button("minmax2").hide();
			prg_win.button("park").hide();
			dhxWins.window("prg_win").setModal(true);
			prg_win.attachURL("<%=contextPath_grid%>/ext/include/progress_ing.htm");
		}

		<% if(!isMultiGridable) { %> 
			dhxWins.window("prg_win").setModal(false);
			dhxWins.window("prg_win").hide();
			dhtmlx_modal_end_cnt++;
		<% } else { %>	
			dhtmlx_modal_end_cnt++;
			
			if(<%=dhtmlx_grid_cnt%> == dhtmlx_modal_end_cnt) {
				dhxWins.window("prg_win").setModal(false);
				dhxWins.window("prg_win").hide();
				dhtmlx_modal_end_cnt = 0;
			}
		<% } %>

		dhtmlx_end_row_id = GridObj.getRowsNum();
	  //  document.getElementById("message").innerHTML = msg + " Rows : " + GridObj.getRowsNum();

		//document.formstyle.csvBuffer.value = GridObj.getUserData("", "excel_data");

		//조회 화면 전용일 경우에는 PageNavigation을 하지 않기 때문에 dhtmlx_end_row_id이 전체행의 값입니다.
		//<% if(isSelectScreen) { %>
		//	    dhtmlx_end_row_id = GridObj.getRowsNum();
		//	    document.getElementById("message").innerHTML = msg + " Rows : " + GridObj.getRowsNum();
		//<% } else { %>
		//	document.getElementById("message").innerHTML = msg;

		//	if(GridObj.getRowsNum() <= <%=page_count%>) {
		//		dhtmlx_end_row_id = GridObj.getRowsNum();
		//	} else {
		//		var page_cnt = parseInt("<%=page_count%>");
		//		dhtmlx_end_row_id = page_cnt;
		//	}
		//<% } %>

		dhtmlx_last_row_id = GridObj.getRowsNum();
		return true;
	}

	function doOnCellEdit(stage,rowId,cellInd)
    {
    	var max_value = GridObj.cells(rowId, cellInd).getValue();

    	//stage = 0 현재상태, 1 = 수정이전상태, 2 = 수정후상태, true 수정후값이 적용되며 false 는 수정이전값으로 적용됩니다.
    	if(stage==0) {
			return true;
		} else if(stage==1) {
			if(GridObj.getColType(cellInd) == "ch" && cellInd != GridObj.getColIndexById("<%=grid_selected%>")) {
				if(max_value == "1") {
					GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("1");
					GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).cell.wasChanged = true;
					return true;
				} else {
					GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("0");
					GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).cell.wasChanged = false;
					return false;
				}
			}
			return true;
		} else if(stage==2) {
			<% if(!isSelectScreen) {
					for (Iterator all = gridcols.iterator(); all.hasNext();) {
						IPgm cols = (IPgm) all.next();
		
			            if(cols.getProperty("ITEM_CL_CD").toString().equals("grid"))
                        {
		    		        grid_column_type = cols.getProperty("COL_CL_CD").toString();

							for(int ro_depth =0; ro_depth < dhtmlx_read_cols_vec.size(); ro_depth++)
				    		{
				    			grid_read_only_col = (String)dhtmlx_read_cols_vec.elementAt(ro_depth);

				    			if(grid_read_only_col != null && grid_read_only_col.indexOf("=") > 0) {
				    				if(grid_read_only_col != null && grid_read_only_col.startsWith(cols.getProperty("COL_ID").toString())) {
				    					grid_column_type   = grid_read_only_col.substring(grid_read_only_col.indexOf("=") + 1, grid_read_only_col.length()).toLowerCase();
				    					break;
				    				}
				    			} else {
					    			if(grid_read_only_col != null && grid_read_only_col.equals(cols.getProperty("COL_ID").toString())) {
					    				if(grid_column_type.equals("edn")) {
					    					grid_column_type = "ron";
					    					break;
					    				} else if(grid_column_type.equals("ed") || grid_column_type.equals("edtxt") || grid_column_type.equals("txt") || grid_column_type.equals("txttxt") || grid_column_type.equals("dhxCalendar")) {
					    					grid_column_type = "ro";
					    					break;
					    				}
					    			}
					    		}
				    		}

			    		    if(grid_column_type.equals("edn") && cols.getProperty("COL_MAX_LEN").toString().length() > 0 && cols.getPropertyString("COL_FORMAT").length() > 0)
		    			    {
		    %>
		    				   if(cellInd == GridObj.getColIndexById("<%=cols.getProperty("COL_ID").toString()%>"))
		    			   	   {
									if(max_value == "0" || isNaN(max_value)) {
								    	<% if(grid_selected != null && grid_selected.length() > 0) { %>
											GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("0");
										<% } %>
										return false;
									} else {
										<% if(grid_selected != null && grid_selected.length() > 0) { %>
											GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("1");
											GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).cell.wasChanged = true;
										<% } %>
										return true;
									}
								}
		    <%
		    		   	   }
			    		   else if((grid_column_type.equals("ed") || grid_column_type.equals("edtxt") || grid_column_type.equals("txt") || 
			    		   			grid_column_type.equals("txttxt")) && cols.getProperty("COL_MAX_LEN").toString().length() > 0 && 
			    		   			cols.getPropertyString("COL_FORMAT").length() == 0)
			    		   {
			    %>
			    			   if(cellInd == GridObj.getColIndexById("<%=cols.getProperty("COL_ID").toString()%>"))
			    			   {
									//19번 Column MaxLength 조회시점에는 불가능하고 수정 시점에만 체크하여 처리할수 있습니다.
									if(getLength(max_value) > <%=cols.getProperty("COL_MAX_LEN").toString()%>) {
										<% if(grid_selected != null && grid_selected.length() > 0) { %>
											alert("칼럼의 최대 입력 길이를 초과했습니다. 입력가능 Bytes : <%=cols.getProperty("COL_MAX_LEN").toString()%>, 입력한 Bytes : " + getLength(max_value));
											GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("0");
										<% } %>
										return false;
									} else {
										<% if(grid_selected != null && grid_selected.length() > 0) { %>
											GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("1");
											GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).cell.wasChanged = true;
										<% } %>
										return true;
									}
							   }
			    <%
			    		   }
		    	   	   }
		    	   }
	    	   }
		    %>

			if(max_value.indexOf("image") == -1) {
			<% if(grid_selected != null && grid_selected.length() > 0) { %>
				GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).setValue("1");
				GridObj.cells(rowId, GridObj.getColIndexById("<%=grid_selected%>")).cell.wasChanged = true;
			<% } %>
			}
			return true;
		}

		return false;
    }
 
 
function master_checkbox(checked){
	
	var start_row = 0;
	var end_row = GridObj.getRowsNum();
	
	if(GridObj._selectionArea == undefined){
	
		//기본 전체 선택
		for(var i = 0; i < GridObj.getRowsNum(); i++) {
			if(checked==true){
				GridObj.cells2(i, GridObj.getColIndexById("SELECTED")).setValue("1");	
			}else if(checked==false){	
				GridObj.cells2(i, GridObj.getColIndexById("SELECTED")).setValue("0");
			}
		}
	
	}else{
		
		start_row 	= GridObj._selectionArea.LeftTopRow;
		end_row 	= GridObj._selectionArea.RightBottomRow;

		for(var i = start_row; i <= end_row; i++) {
			if(checked==true){
				GridObj.cells2(i, GridObj.getColIndexById("SELECTED")).setValue("1");	
			}else if(checked==false){	
				GridObj.cells2(i, GridObj.getColIndexById("SELECTED")).setValue("0");
			}
		}	
 	
	}	
  	  	
  	try{ 
  		doBlockSelected(checked, start_row, end_row);  		
   	}catch(e){
  	}  	
}
 
</script>