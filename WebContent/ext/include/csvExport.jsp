<%@ page contentType="application/vnd.ms-excel; charset=UTF-8" %>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	response.reset();
	response.setHeader("Content-Type", "application/octet-stream;charset=utf-8");
	response.setHeader("Content-Disposition", "attachment; filename=ExcelData.xls");
	response.setHeader("Content-De-scription", "JSP Generated Data");
	response.setHeader("Content-Transfer-Encoding", "UTF-8" );
	
	IParameterManagement paramameter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String col_del = paramameter.getString("dhtmlx.separator.field");
	// String row_del = paramameter.getString("dhtmlx.separator.line"); 
	
	String[] header = null;
	StringBuffer exdsb = new StringBuffer();
	
	try {
		String headText   = request.getParameter("headText") == null  ? "" : request.getParameter("headText");
		// String grid_obj   = request.getParameter("grid_obj") == null  ? "" : request.getParameter("grid_obj");
		String gridColids = request.getParameter("gridColids") == null  ? "" : request.getParameter("gridColids");
		String csvBuffer = request.getParameter("csvBuffer") == null  ? "" : request.getParameter("csvBuffer");
		java.util.Vector grid_col_vec = null;
		String grid_col_name = "";
		String grid_ex_data  = "";
		
		System.out.println("headText"+headText);
		
		String[] rows = csvBuffer.split("\n");
		
		if(rows.length > 0) {
			System.out.println("rows.length~~~~~~~]"+rows.length);
			header =  StringUtils.StrToArray(headText, col_del);
			grid_col_vec = StringUtils.StrToVector(gridColids, col_del);
			
			for(int j=0; j < grid_col_vec.size(); j++) {
				   grid_col_name = (String)grid_col_vec.elementAt(j);
			}
			System.out.println("grid_col_name !!!!!!!~~~~~~~]"+grid_col_name);
			
			for(int i=0; i < header.length; i++) {
			
				if(i == 0) {
					exdsb.append("<table border=1><tr><td align=center>"+header[i]+"</td>");
				} else {
					exdsb.append("<td align=center>" + header[i]+"</td>");
				}
			}
			if(header.length > 0) exdsb.append("</tr>");
			
			System.out.println("exdsb!!!!!!!~~1~~~~~]"+exdsb.toString());
			
			for(int k=0; k< rows.length; k++) {

				exdsb.append("<tr>");
				String[] cols = rows[k].split(",");
				
				for(int j=1; j < cols.length ; j++) {
					// grid_col_name = (String)grid_col_vec.elementAt(j);
					grid_ex_data  = cols[j];
					exdsb.append("<td align=left style=mso-number-format:\\@>"+ grid_ex_data+"</td>");
				}
				exdsb.append("</tr>");
			}
			if(rows.length > 0) exdsb.append("</table>");
			
		}

		//exdsb.toString().getBytes("UTF-8");
		//System.out.println("exdsb!!!!!!!~~~~~~~]"+exdsb.toString());
		
	} catch(Exception e) {
		e.printStackTrace();
		System.out.println("exdsb!!!!!!!~~~~~~~]"+exdsb.toString());
	} finally {
	}
%>
<meta content="application/vnd.ms-excel; charset=UTF-8" name="Content-type">
<%=exdsb.toString()%>

