<%@ page contentType = "text/html; charset=UTF-8" %>
<%@ page import="com.core.component.util.TokenizerUtil"%>

<%@ include file="/ext/include/include_css.jsp"%>
<%
	String grid_col_id    = request.getParameter("grid_col_id");
	String grid_col_lable = request.getParameter("grid_col_lable");
	TokenizerUtil col_ids = new TokenizerUtil(grid_col_id, ",");
	TokenizerUtil col_las = new TokenizerUtil(grid_col_lable, ",");
%>	
<Script language="javascript">
	function doFilter()
	{ //
		var filter_column = document.form.filter_column.value;
		var filter_word   = document.form.filter_word.value;
		window.close();
		opener.doFilterSearch(filter_column, filter_word);
	}
	
	function enableEnterKey() {
		function onkeypress(e) {
			if(window.event.keyCode == 13 && window.event.srcElement.type != "textarea" && window.event.srcElement.type != "button") { // 13 : Enter
				doFilter();
			}
		}
		document.onkeypress = onkeypress;
	}

	enableEnterKey();
</script>
<html>
<form name="form">
<table width="500">
	<tr>
		<td>
		<fieldset style="width:500">
			<legend>Filter</legend>
			Column 
			<select name='filter_column'>
				<%
					while(col_ids.hasMoreElements()) {
						String tmpA = (String)col_ids.nextElement();
						if(!tmpA.equals("SELECTED")) {
				%>	
					<option value="<%=tmpA%>"><%=col_las.nextElement()%></option>
				<%		}
					}
				
				%>
			</select>
			검색어를 입력하여주세요. <!-- 검색어를 입력하여주세요. -->
			<input type="text" name="filter_word" value="" >
		 	<input type="button" name="Search"  id="a11" value="Search" onclick='javascript:doFilter()'><br/><br/> 
		</fieldset>
		</td>
	</tr>
</table>
</form>