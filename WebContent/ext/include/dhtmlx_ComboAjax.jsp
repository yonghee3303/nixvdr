<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	IParameterManagement paramemter2 = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String ajax_col = paramemter2.getString("dhtmlx.separator.field"); // CommonUtil.getConfig("sepoa.separator.field");
	String ajax_row = paramemter2.getString("dhtmlx.separator.line"); 
%>

<script type="text/javascript">

var GridObj;

function doRequestUsingPOST_dhtmlxGrid(grid_obj, code, col_id, default_val) {

	GridObj = grid_obj;
	
	var tmpval = "&P_CD=" + code;
	     tmpval += "&COL_ID=" + col_id;
	     tmpval += "&default_val=" + default_val;
	
	var url = "<%=request.getContextPath()%>/mg/Code.do?mod=grid";

	sendRequest(parseResults_dhtmlxGrid, tmpval,'POST', url , false, false);
}

function parseResults_dhtmlxGrid(oj) {
    var result = oj.responseText;//Servlet에서 뿌린 리턴값
    var field = "<%=ajax_col%>";
    var line = "<%=ajax_row%>";
    var subStr = result.split(line);//Row단위 딜리미터
	var ooption;//option element
	var selectbox_name = "";//select box id값
	var default_value ="";//기본 셋팅값
	var nodePath; //해당 select box Object
	var finalStr =new Array ;
	var combo_data = new Array();
	var combobox;
	
		//alert(result);
	  for(var i=0 ; i < subStr.length ; i++) {

		   if(i==0)
		   {
		   		finalStr = subStr[i].split(field);
		   		selectbox_name = finalStr[0];
		   		default_value = finalStr[1];
		   		combobox = GridObj.getCombo(GridObj.getColIndexById(selectbox_name));
		   }
		   else
		   {
		   		if(i== 1){
		   			combobox.put("", "");
		   			//combobox.addOption([["", ""]]);
		   		}

		   		finalStr = subStr[i].split(field);

		   		if(finalStr[1] != null){
		   			//ooption.text = finalStr[1].replace(/(^\s*)|(\s*$)/g, "");
		   			combobox.put(finalStr[0], finalStr[1].replace(/(^\s*)|(\s*$)/g, ""));
		   			//combobox.addOption([[finalStr[0], finalStr[1].replace(/(^\s*)|(\s*$)/g, "")]]);
		   		} else {
		   			//ooption.text = finalStr[1];
		   			combobox.put(finalStr[0], finalStr[1]);
		   			//combobox.addOption([[finalStr[0], finalStr[1]]]);
		   		}
		   }
	  }//End for
}//End function

</script>
