/* 전역 변수 */
try {
	var IS_NAV = (navigator.appName == "Netscape");
	var IS_IE = (navigator.appName == "Microsoft Internet Explorer");
	
	var IS_WIN = (navigator.userAgent.indexOf("Win") != -1);
	var IS_MAC = (navigator.userAgent.indexOf("Mac") != -1);
	var IS_UNIX = (navigator.userAgent.indexOf("X11") != -1);
} catch(e){
	var IS_NAV = false;
	var IS_IE = true;
	
	var IS_WIN = true;
	var IS_MAC = false;
	var IS_UNIX = false;
}

/**
 * 쿠키에 저장된 값을 반환한다.    
 * @param name 쿠키 이름
 * @return 쿠키 이름에 대한 값을 반환.     없는 경우에는 ""를 반환.
 */
function getCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg) {
            return getCookieVal(j);
        }
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return "";
}


/**
 * 쿠키를 저장한다.
 * @param name 쿠키 이름
 * @param value 쿠키 값
 * @param expires 쿠키의 유효 일
 * @param path
 * @param domain
 * @param secure
 */
function setCookie(name, value, expires, path, domain, secure) {
    if (!path) {
        path = "/";
    }
    document.cookie = name + "=" + escape (value) +
                    ((expires) ? "; expires=" + expires : "") +
                    ((path) ? "; path=" + path : "") +
                    ((domain) ? "; domain=" + domain : "") +
                    ((secure) ? "; secure" : "");
}


/**
 * 쿠키를 삭제한다.
 * @param name 삭제할 쿠키 이름
 * @param path
 * @param domain
 */
function deleteCookie(name, path, domain) {
    if (!path) {
        path = "/";
    }
    if (getCookie(name)) {
        document.cookie = name + "=" +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") + 
            "; expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
}


/**
 * 쿠키를 저장할 때 필요한 적합한 형식의 유효기간을 반환한다.
 * @days 쿠키가 유효할 일 (예를 들어 3 일 동안 유효해야 하면 3을 입력)
 * @hours 쿠키가 유효할 시간 (예를 들어 2 시간 동안 유효해야 하면 2를 입력)
 * @minutes 쿠키가 유효할 분 (예를 들어 30 분 동안 유효해야 하면 30을 입력)
 */
function getExpDate(days, hours, minutes) {
    var expDate = new Date( );
    if (typeof days == "number" && typeof hours == "number" &&
        typeof hours == "number") {
        expDate.setDate(expDate.getDate( ) + parseInt(days));
        expDate.setHours(expDate.getHours( ) + parseInt(hours));
        expDate.setMinutes(expDate.getMinutes( ) +
        parseInt(minutes));
        return expDate.toGMTString( );
    }
}


/**
 * 쿠키 값을 읽을 때 사용하는 보조 함수
 */
function getCookieVal(offset) {
    var endstr = document.cookie.indexOf (";", offset);
    if (endstr == -1) {
        endstr = document.cookie.length;
    }
    return unescape(document.cookie.substring(offset, endstr));
}

/**
 * 입력받을 수 있는 값을 필터링한다.
 * ex : <input type="text" ..... onkeypress="filterKey('[0-9]')"> ; 숫자만 키입력이 가능한 text filed
 * ex : <input type="text" ..... onkeypress="filterKey('[0-9a-zA-Z]')"> ; 영문,숫자만 키입력이 가능한 text filed
 * @param filter : 필터링할 정규표현식 ex) '[0-9]':0~9의 값만 허용, '[a-zA-Z]':알파벳만 허용
 * @return 
 */
function filterInputData(filter) {
    if (filter) {
        var sKey = String.fromCharCode(event.keyCode);
        var re = new RegExp(filter);
        if (!re.test(sKey)) {
            event.returnValue = false;
        }
    }
}



/**
 * 입력 변수에 3 자리마다 콤마(,)를 붙여 반환한다.
 * @param field 콤마를 붙일 값
 */
function formatCommas(numString) {
    var re = /,|\s+/g;
    numString = numString.replace(re, "");

    re = /(-?\d+)(\d{3})/;
    while (re.test(numString)) {
        numString = numString.replace(re, "$1,$2");
    }
    return numString;
}

function stripCommas(numString) {
    var re = /,/g;
    return numString.replace(re, "");
}

/**
 * 텍스트 필드에 입력한 값에 3자리마다 콤마(,)를 붙인다.
 * 텍스트 필드에 아래를 기입한다. onkeyup="toMoney(this)"
 * @param field 텍스트 필드
 */
function toMoney(field) {
    var value = field.value;
    var indexOfPoint = value.indexOf(".");
    if (indexOfPoint == -1) {
        field.value = formatCommas(value);
    } else {
        field.value = formatCommas(value.substring(0, indexOfPoint)) +
                        value.substring(indexOfPoint, value.length);
    }
}


/**
 * 두 날짜 사이의 일수를 계산하여 반환한다.
 * @param date1 문자열 데이터로 '20041012' 형식
 * @param date2 문자열 데이터로 '20041012' 형식
 */
function daysBetween(date1, date2) {
    date1 = new Date(date1.substring(0, 4), date1.substring(4, 6)-1, date1.substring(6,8));
    date2 = new Date(date2.substring(0, 4), date2.substring(4, 6)-1, date2.substring(6,8));
    var DSTAdjust = 0;
    oneMinute = 1000 * 60;
    var oneDay = oneMinute * 60 * 24;
    date1.setHours(0);
    date1.setMinutes(0);
    date1.setSeconds(0);
    date2.setHours(0);
    date2.setMinutes(0);
    date2.setSeconds(0);
    DSTAdjust = (date2.getTimezoneOffset( ) - 
                     date1.getTimezoneOffset( )) * oneMinute;
    var diff = date2.getTime( ) - date1.getTime() - DSTAdjust;
    return Math.ceil(diff/oneDay);
}




function cala_weekday( x_nMonth, x_nDay, x_nYear) 
{ 
		var f = document.cnjform;

        if(x_nMonth >= 3){         
                x_nMonth -= 2; 
        } else { 
                x_nMonth += 10; 
        } 
        if( (x_nMonth == 11) || (x_nMonth == 12) ){ 
                x_nYear--; 
        } 
        var nCentNum = parseInt(x_nYear / 100); 
        var nDYearNum = x_nYear % 100; 
        var g = parseInt(2.6 * x_nMonth - .2); 
        g +=  parseInt(x_nDay + nDYearNum); 
        g += nDYearNum / 4;         
        g = parseInt(g); 
        g += parseInt(nCentNum / 4); 
        g -= parseInt(2 * nCentNum); 
        g %= 7; 
        if(x_nYear >= 1700 && x_nYear <= 1751) { 
                g -= 3; 
        } else { 
                if(x_nYear <= 1699) { 
                        g -= 4; 
                } 
        } 
        if(g < 0){ 
                g += 7; 
        } 
        return g; 
}
