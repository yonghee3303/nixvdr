
var innerHtmlSpan=""; 
var divField= "";
var trans_condion="";

//신규 CP Doc No 를 구한다.
function doAjaxGetCpDocNo() {
	var url = "/cp/approval.do?mod=getCpDoc";
	sendRequest(doAjaxCpDocNoResult, "", 'POST', url , false, false);
}

function doAjaxCpDocNoResult(oj) {
	var docNo = oj.responseText;
	doAjaxCpDocNo(docNo);
}

//신규 CP Doc No 를 구한다.
function doAjaxGetCpDocInfo(docNo) {
	var url = "/cp/approval.do?mod=getCpDocInfo&DOC_NO=" + docNo;
	sendRequest(doAjaxCpDocInfoResult, "", 'POST', url , false, false);
}

function doAjaxCpDocInfoResult(oj) {
	var JsonData = oj.responseText;

	doAjaxCpDocInfo(JsonData);
}

// 사번으로 성명 조회함.
// 
function doAjaxGetUserNm(user_id) {
	var tmpval = "&W_USER_ID=" + user_id;
	var url = "/mg/Code.do?mod=getUserNm";
	
	sendRequest(doAjaxUserNmResult, tmpval,'POST', url , false, false);
}

function doAjaxUserNmResult(oj) {
	var user_nm = oj.responseText;
	doAjaxUserNm(user_nm);
}

// 화면 이동을 위한 함수 
//
function doAjaxMenuTrans(p_menu_id, menu_id, fields, cond) {
	divField = fields;
	trans_condion = cond;
	
	var tmpval = "&P_MENU_ID=" + p_menu_id;
	    tmpval += "&MENU_ID=" + menu_id;
	var url = "/mg/Code.do?mod=menu";
	
	sendRequest(doAjaxMenuTransResult, tmpval,'POST', url , false, false);
}

function doAjaxMenuTransResult(oj) {
 var result = oj.responseText; 
	var subStr = result.split(divField);  //Col 단위 딜리미터
	
	parent.leftFrame.exe_menu(subStr[0] + trans_condion,subStr[1],subStr[2],subStr[3]);
}




