var encoding_status = "UTF-8";
var G_IMG_ICON = '/ext/images/button/bt_search.gif';
var G_MSS1_SELECT = 'The selected data does not exist.';
var G_MSS2_SELECT = 'You cannot select more than one.';
var G_COL1_OPT    = '242|242|242';
var G_COL1_ESS    = '251|251|92';

//contextPath 추가
var offset=location.href.indexOf(location.host)+location.host.length;
var contextPath=location.href.substring(offset,location.href.indexOf('/',offset+1));

//NIMS Function

function popupAccTree(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '거래처선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupAccTree.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupAccTree.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupAgency(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '거래처선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupAgency.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupAgency.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupProjComp(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '수행사선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupProjComp.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupProjComp.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupNewAddBoard(url, title, height, width) {
    //화면 가운데로 배치
    var top = 100;
    var left = 100;

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
    
	var ret = window.open(url, title, 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupProject(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '프로젝트선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupProject.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupProject.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupMan(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '인재선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupMan.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupMan.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupManageCustomer(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '고객사 선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupManageCustomer.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupManageCustomer.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function getRanIdNum() {
	return Math.floor((Math.random() * 89999)+10000);
}

function getTodayNum() {
	let today = new Date();
	
	var year = today.getFullYear();
	var month = today.getMonth() + 1;
	var date = today.getDate();
	
	if (month < 10){
		month = "0"+ month.toString();
	}
	if (date < 10){
		date = "0"+ date.toString();
	}
	
	var todayNum = year.toString() + month.toString() + date.toString();
	
	return todayNum
}

function phoneFomatter(num){

    var formatNum = '';

    if(num.length==11){
            formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
    } else {
        formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
    }
    return formatNum;
}

function busFomatter(num){

    var formatNum = '';

    formatNum = num.replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-$3');
    
    return formatNum;
}

function numberMaxLength(e){
    if(e.value.length > e.maxLength){
        e.value = e.value.slice(0, e.maxLength);
    }
}
























function excelDownload(url, mygrid) {
	var ws = formatWindowProperty1(10, 10, 10, 10);
	var ret = window.open(url, null, ws);
}

function fileDownload(url) {
	var ws = formatWindowProperty1(10, 10, 10, 10);
	var ret = window.open(url, null, ws);
}

function formatWindowProperty(left, top, width, height) {
    if (window.showModalDialog) {
        return "status:no; dialogLeft:" + left + "px; dialogTop:" + top + "px; dialogWidth:" + width + "px; dialogHeight:" + height + "px";
    } else {
        return "modal, left=" + left + "px, top=" + top + ", width=" + width + "px, height=" + height + "px";
    }
}

function formatWindowProperty1(left, top, width, height) {
    return "left=" + left + ", top=" + top + ", width=" + width + ", height=" + height;
}

function popupPolicy(title, url, width, height) {
    //화면 가운데로 배치
	var top = 100;
    var left = 100;
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, title, 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function popupBoard(url, title, height, width) {
    //화면 가운데로 배치
    var top = 100;
    var left = 100;

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, title, 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function excelUpload(title, url, left, top, width, height) {
	if (title == '') title = 'Excel Upload';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
    
	var ret = window.open(url, title, 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

// 첨부파일 관리  POPUP을 띄운다.
function popupAttachFile(title, left, top, width, height, pgmId, docId, authority, callmode,root) {
    if (title == '') title = 'File Upload';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
    
    var url ="";
    
    if(root == false) {
    	url = contextPath;
    }
    
    if(callmode == "write") {
       url += "/mg/common/popupAttachFile.jsp";
    }else if(callmode=="repacking"){
    	url += "/mg/common/popupAttachFile_repacking.jsp";
    	resizable='no';
    }else if(callmode=="readPortal"){
    	url += "/mg/common/popupAttachFileRead_portal.jsp";
    	resizable='no';
    }else if(callmode=="readRepacking"){
    	url += "/mg/common/popupAttachFileSingleRead_portal.jsp";
    	resizable='no';
    }else {
    	url += "/mg/common/popupAttachFileRead.jsp";
    }
    
    if(callmode=="readRepacking"){
    	url +="?formid=POP_0109&gridid=GridObj";
    }else{
    	url += "?formid=POP_0106&gridid=GridObj";
    }
    url += "&pgmId=" + pgmId + "&docId=" + docId + "&authority=" + authority;
    if(callmode=="repacking"){
    	url += "&docTitle="+authority;
    }
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];
	
	var ret = window.open(url, "upload", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}




// 부서 선택 POPUP을 띄운다.
function popupDept(title, left, top, width, height, type, callFunc) {
	return openPopupDept(title, left, top, width, height, type, '', callFunc);
}

function popupDeptAuth(title, left, top, width, height, type, roll,callFunc) {
   return openPopupDept(title, left, top, width, height, type, roll, callFunc);
}

function openPopupDept(title, left, top, width, height, type, roll, callFunc) {
    if (title == '') title = '부서선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url = "/mg/common/popupDept.jsp";
        url += "?formid=POP_0102&gridid=GridObj&type=" + type + "&retfunc=" + callFunc;
    	url += "&roll=" + roll;
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function openPopupCode(title, left, top, width, height, p_cd, callFunc, root) {
    if (title == '') title = '코드선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url="";
    
    if(root == false) {
    	url = contextPath; 
    }
    	url += "/mg/common/popupCode.jsp";
        url += "?formid=POP_0101&gridid=GridObj&p_cd=" + p_cd + "&retfunc=" + callFunc;
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

//  Tree 부서 선택 POPUP을 띄운다.
//기존 소스
//function popupDeptTree(title, left, top, width, height, type, retfunc) {
//contextPath 추가 : root
function popupDeptTree(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '부서선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

//협력사 선택 POPUP을 띄운다.
function popupCompany(title, left, top, width, height, retfunc, root) {
    if (title == '') title = "협력사 선택";
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url ="";
    
    if(root == false) {
    	url = contextPath;
    }
    	url += "/mg/common/popupCompany.jsp";
        url += "?formid=POP_0103&gridid=GridObj&retfunc=" + retfunc;
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

//협력사 선택 POPUP을 띄운다.
function popupCompUser_portal(title, left, top, width, height, exportUser, requster, root) {
    if (title == '') title = "수신자 선택";
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url = "";
    if(!root){
    	url = contextPath;
    }
   
    if(exportUser == "personal") {
    	 url = "/mg/common/popupPersonalUser.jsp";
         url += "?formid=POP_0107&gridid=GridObj&requster=" + requster;
    } else {
    	 url = "/mg/common/popupCompUser_portal.jsp";
         url += "?formid=POP_0104&gridid=GridObj&requster=" + requster;
    }
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}
//협력사 선택 POPUP을 띄운다.
function popupCompUser(title, left, top, width, height, exportUser, requster, root ) {
    if (title == '') title = "수신자 선택";
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url = "";
    if(!root){
    	url = contextPath;
    }
    if(exportUser == "personal") {
    	 url += "/mg/common/popupPersonalUser.jsp";
         url += "?formid=POP_0107&gridid=GridObj&requster=" + requster;
    } else {
    	 url += "/mg/common/popupCompUser.jsp";
         url += "?formid=POP_0104&gridid=GridObj";
    }
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

// 사용자 선택 POPUP을 띄운다.
function popupUser(title, left, top, width, height) {
    if (title == '') title = "사용자선택";
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url = "/mg/common/popupUser.jsp";
        url += "?formid=POP_0105&gridid=GridObj";
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function chkUserAuth(chkComponent, roll)
{
	var userLevel = parseInt(Zero_LTrim(roll.substring(1,3)));
	if(chkComponent == "user") {
		if( userLevel < 5 ) {
			return true;
		} else {
			return false;
		}
	} else if(chkComponent == "dept") {
		if( userLevel < 4 ) {
			return true;
		} else {
			return false;
		}
	}
}

function OnKeyEvent()
{
  var keycode = event.keyCode;
  if(keycode==13) {
  	  doQuery();
  }	
}

function uncheckGrid(grid_obj, rowId)
{
	grid_obj.cells(rowId, grid_obj.getColIndexById("SELECTED")).cell.wasChanged = false;
	grid_obj.cells(rowId, grid_obj.getColIndexById("SELECTED")).setValue("0");
}

function checkGrid(grid_obj, rowId)
{
	grid_obj.cells(rowId, grid_obj.getColIndexById("SELECTED")).cell.wasChanged = true;
	grid_obj.cells(rowId, grid_obj.getColIndexById("SELECTED")).setValue("1");
}


function replace(s, old, replacement)
{
        var i = s.indexOf(old, 0);
        var r = "";
        if(i <= 0)
            return s;
        r = r + s.substring(0, i) + replacement;

        if(i + old.length < s.length)
            r = r + replace(s.substring(i + old.length, s.length), old, replacement);

        return r;
}

    function IsNumber1(num)
    {
        var i;

        if(num.length <= 0)
            return false;

        for(i=0;i<num.length;i++)
        {
            if((num.charAt(i) < '0' || num.charAt(i) > '9') && num.charAt(i) !='.'  )
            return false;
        }

        return true;
    }

    function chkKorea(chkstr)
    {
        var j, lng = 0;

        for (j=0; j<chkstr.length; j++)
        {
            if (chkstr.charCodeAt(j) > 255)
            {
                ++lng;
            }
            ++lng;
        }

        return lng;
    }


    function getLength(chkstr)
    {
        var j, lng = 0;

        for (j=0; j<chkstr.length; j++)
        {
            if (chkstr.charCodeAt(j) > 255)
            {
                ++lng;
                if(encoding_status != null && encoding_status.toUpperCase() == 'UTF-8')
                {
           			++lng
           		}
            }
			++lng;
        }

        return lng;
    }

function getStringLength(chkstr)
{
    var j, lng = 0;

    for (j=0; j<chkstr.length; j++)
    {
        if (chkstr.charCodeAt(j) > 255)
        {
            ++lng;
        }
		++lng;
    }
    return lng;
}

function getSubStrLength(chkstr, max_len)
{
    var j, lng = 0;

    for (j=0; j<chkstr.length; j++)
    {
    	++lng;
        if (chkstr.charCodeAt(j) > 255)
        {
            ++lng;
    		if(encoding_status != null && encoding_status.toUpperCase() == 'UTF-8')
    		{
    			++lng;
			}
            if(max_len < lng){
            	chkstr = chkstr.substring(0, j);
            	return chkstr;
            }
		}else{
		    if(max_len < lng){
            	chkstr = chkstr.substring(0, j);
            	return chkstr;
            }
		}
    }
    return chkstr;
}

	function getLength2Bytes(chkstr)
    {
        var j, lng = 0;

        for (j=0; j<chkstr.length; j++)
        {
            if (chkstr.charCodeAt(j) > 255)
            {
                ++lng;
            }
			++lng;
        }
        return lng;
    }

	function getSubStrLength2Bytes(chkstr, max_len)
	{
	    var j, lng = 0;

	    for (j=0; j<chkstr.length; j++)
	    {
	    	++lng;
	        if (chkstr.charCodeAt(j) > 255)
	        {
	            ++lng;
	            if(max_len < lng){
	            	chkstr = chkstr.substring(0, j);
	            	return chkstr;
	            }
			}else{
			    if(max_len < lng){
	            	chkstr = chkstr.substring(0, j);
	            	return chkstr;
	            }
			}
	    }
   	 return chkstr;
	}


    /*왼쪽 0를 잘라준다
        RETURN : var */
    function Zero_LTrim(str)
    {
        if(str.charAt(0) == "0")
            {str=str.substring(1,str.length); return Zero_LTrim(str);}
        else
            return str;
    }

    /*오른쪽 공백를 잘라준다
        RETURN : var */
    function RTrim(str)
    {
        if(str.charAt(str.length-1) == " ")
            {str=str.substring(0,str.length-1); return RTrim(str);}
        else
            return str;
    }

    /*왼쪽 공백를 잘라준다
        RETURN : var */
    function LTrim(str)
    {
        if(str.charAt(0) == " ")
            {str=str.substring(1,str.length); return LTrim(str);}
        else
            return str;
    }

    /*좌우 공백를 잘라준다
        RETURN : var */
    function LRTrim(str)
    {
        return RTrim(LTrim(str));
    }

/* 일자의 입력 Check 달체크는 나중에 한다.
     RETURN : true */
	function checkDD(str)
	{
		dd = parseInt(str);

		if( str.length == 0)
			return true;
		for(j = 0; j<str.length ; j++){
			if((str.charAt(j) < '0' || str.charAt(j) > '9'))
			return false;
		}

		if( dd > 31)
			return false;
		else
			return true;
	}
	
    /* 윤년 계산 및 입력날짜 Check
        RETURN : true */
    function checkDateMessage(str)
    {
        var yy,mm,dd,ny,nm,nd;
        var arr_d;

        if(str.length != 8)
        {
            return false;
        }

        yy = str.substring(0,4);      // 년, 월, 일을 문자열로 가지고 있는다. 
        mm = str.substring(4,6);
        dd = str.substring(6,8);

        if(mm < '10')   // patch 판  
        {
            mm = mm.substring(1);   // patch 판 
        }

        if(dd < '10')    // patch 판 
        {
            dd = dd.substring(1);        // patch 판 
        }

        ny = parseInt(yy);       // 년, 월, 일을 정수형으로 가지고 있는다. 
        nm = parseInt(mm);
        nd = parseInt(dd);

        if(!(Number(yy)) || (ny < 1000) || (ny>9999))
        {
            alert('Check year.');
            return false;
        }

        if(!(Number(mm)) || (nm < 1) || (nm > 12))
        {
            alert('Check month.');
            return false;
        }

        arr_d = new Array('31','28','31','30','31','30','31','31','30','31','30','31')

        file:    //윤년계산 

        if(((ny % 4 == 0)&&(ny % 100 !=0)) || (ny % 400 == 0)) arr_d[1] = 29;
        if(!(Number(dd)) || (nd < 1) || (nd > arr_d[nm-1]))
        {
            alert('Check day.');
            return false;
        }

        return true;
      }


    /* 문자열 delete
        RETURN : var */

    function deleteChar( str, ch )
    {
        var rt = "";

        for( i = 0; i < str.length; i++ )
        {
            if ( str.charAt( i ) == ch ) continue;
            else    rt += str.charAt( i );
        }
        return rt;
    }

//! E&P
/**
* @desc		현재 날짜에서 주어진 term(월) 만큼을 계산한다.
* @param	sys_date - System Date, terms - term을 두고 싶은 달수.
* @return	date - from_date + to_date. ex)2001012020010220
*/

function getDateCommon(sys_date, terms)
{
	var now = sys_date;                               // System Date 입니다! 
    	var ny = now.substring(0,4);
        var nm = parseInt(now.substring(4,6));
        var nd = now.substring(6,8);
        var arr_d;
        var nm = nm - terms;                            // 현재날짜에서 한달 전입니다. 
        var from_date;
        var to_date = now;
        var date;

        if(nm=="0") {
        	ny = ny-1;
        	nm = "12";
        }

        var mo=(nm<10) ? "0"+nm:nm;

        arr_d = new Array('31','28','31','30','31','30','31','31','30','31','30','31')
        if(((ny % 4 == 0)&&(ny % 100 !=0)) || (ny % 400 == 0))
        arr_d[1] = 29;

        for(var i=0;i<5;i++) {
        	if(nd > arr_d[nm-1]){
        		nd = nd - 1;
        	} else {
        		nd = nd;
        		break;
        	}
        }
        from_date = ny+mo+nd;
        date = from_date + to_date;
        return date;
}


/**
* @desc		현재 날짜에서 주어진 term(일) 만큼을 계산한다.
* @param	sys_date - System Date, terms - term을 두고 싶은 일수.
* @return	date - from_date + to_date. ex)2001012020010125
*/

function getDateCommon2(sys_date, terms)      // Term 을 일자단위로 줌. 
{

	var now = sys_date;                             // System Date 입니다!  
    	var ny = now.substring(0,4);
        var nm = now.substring(4,6);
        var nd = parseInt(now.substring(6,8));
        var arr_d;
        var nd = nd - terms;                           // 현재날짜에서 하루 전입니다.  
        var from_date;
        var to_date = now;
        var date;

        if(nd==0) {
        	nm = nm-1;
        	if(nm == "0") {
        		ny = ny-1;
        		nm = "12";
        		nd = "31";
        	}
        	if(nm == "1") {
        		nm = "01";
        		nd = "31";
        	}
        	if(nm == "2") {
        		nm = "02";
        		nd = "28";
        	}
        	if(nm == "3") {
        		nm = "03";
        		nd = "31";
        	}
        	if(nm == "4") {
        		nm = "04";
        		nd = "30";
        	}
        	if(nm == "5") {
        		nm = "05";
        		nd = "31";
        	}
        	if(nm == "6") {
        		nm = "06";
        		nd = "30";
        	}
        	if(nm == "7") {
        		nm = "07";
        		nd = "31";
        	}
        	if(nm == "8") {
        		nm = "08";
        		nd = "31";
        	}
        	if(nm == "9") {
        		nd = "30";
        	}
        	if(nm == "10") {
        		nd = "31";
        	}
        	if(nm == "11") {
        		nd = "30";
        	}
        }

        var no;

        if(nd<10){
        	no = "0"+nd;
        }else {
        	no = nd;
        }
        var mo = nm;

        arr_d = new Array('31','28','31','30','31','30','31','31','30','31','30','31')
        if(((ny % 4 == 0)&&(ny % 100 !=0)) || (ny % 400 == 0))
        arr_d[1] = 29;

        for(var i=0;i<5;i++) {
        	if(nd > arr_d[nm-1]){
        		no = nd - 1;
        	} else {
        		nd = nd;
        		break;
        	}
        }

        from_date = ny+mo+no;
        date = from_date + to_date;
        return date;
}


/**
* @desc		윤달계산 및 날짜 체크.
* @param	str - 체크하고자 하는 날짜.
* @return	올바른 형식이면 true 리턴.
*/

function checkDateCommon(str)
{
	var yy,mm,dd,ny,nm,nd;
	var arr_d;

        if(str.length != 8)
                return false;

        yy = str.substring(0,4);            // 년, 월, 일을 문자열로 가지고 있는다.  
        mm = str.substring(4,6);
        dd = str.substring(6,8);

        if(mm < '10')                             // patch 판 
                mm = mm.substring(1);             // patch 판 

        if(dd < '10')                             // patch 판 
                dd = dd.substring(1);             // patch 판 

        ny = parseInt(yy);        // 년, 월, 일을 정수형으로 가지고 있는다. 
        nm = parseInt(mm);
        nd = parseInt(dd);

        if(!(Number(yy)) || (ny < 1000) || (ny>9999))
                return false;

        if(!(Number(mm)) || (nm < 1) || (nm > 12))
                return false;

        arr_d = new Array('31','28','31','30','31','30','31','31','30','31','30','31')

          // 윤년계산  
        if(((ny % 4 == 0)&&(ny % 100 !=0)) || (ny % 400 == 0))
        arr_d[1] = 29;

        if(!(Number(dd)) || (nd < 1) || (nd > arr_d[nm-1]))
                return false;

        return true;
}


/**
* @desc		입력값의 오른쪽 공백를 잘라준다
* @param	str - 체크하고자 하는 문자열.
* @return	str - 공백을 제거한 후의 문자열 리턴.
*/

function RTrimCommon(str)
{
	if(str.charAt(str.length-1) == " ") {
		str=str.substring(0,str.length-1); return RTrimCommon(str);
	}
	else {
        	return str;
        }
}


/**
* @desc		입력값의 왼쪽 공백를 잘라준다
* @param	str - 체크하고자 하는 문자열.
* @return	str - 공백을 제거한 후의 문자열 리턴.
*/

function LTrimCommon(str)
{
	if(str.charAt(0) == " ") {
		str=str.substring(1,str.length); return LTrimCommon(str);
	}
	else {
		return str;
	}
 }


/**
* @desc		입력값이 숫자인지를 확인한다.
* @param	val - 체크하고자 하는 숫자.
* @return	값이 숫자이면 true 리턴.
*/

function isNumberCommon(val)
{
	if(val.length < 1)
		return false;

	for(i=0;i<val.length;i++) {
    		abit_dat = val.substring(i,i+1);

    		if(i == 0 && abit_dat == '-') continue;

    		if(abit_dat == '.') {

    		}
    		else {
    			abit = parseInt(val.substring(i,i+1));
		    	if(('0' < abit) || ('9' > abit) || ('.' == abit_dat)) {

		    	}
		    	else {
		    		return false;
		    	}
		}
    	}
    	return true;
}


/**
* @desc		입력값에 한글이 있는지를 확인한다.
* @param	str - 체크하고자 하는 문자열.
* @return	check_flag - 한글이 없으면 true 리턴.
*/

function hangulCheckCommon(str)
{
	var check_flag = true;
	var strValue = str;
	var retCode = 0;

	for (i = 0; i <  strValue.length; i++) {

		var retCode = strValue.charCodeAt(i);
		var retChar = strValue.substr(i,1).toUpperCase();
		retCode = parseInt(retCode);

		if ((retChar <  "0" || retChar  > "9") && (retChar <  "a" || retChar  > "z") && (retChar <  "A" || retChar  > "Z")) {

			check_flag = false;
			break;
		}
	}
	return check_flag;
}


/**
* @desc		들어온 데이타의 값에 공백 체크
* @param	date - String value
* @return	공백이 있으면 false 없으면 true
*/

function isEmpty(data){
    for(var i = 0; i < data.length; i++) {
        if(data.substring(i, i+1) != " ") {
            return false;
        }
    }
    return true;
 }

/*
폼의  값이  빈값인지 체크
*/
function checkEmpty(obj, message)
{
	if(isEmpty(obj.value))
	{
		alert(message);
		obj.focus();
		return false;
	}
	return true;
}

/*
	obj 객체가 chknum보다 큰지 체크
*/
function checkKorea(obj, message, chknum)
{
	if(chkKorea(obj.value) > chknum)
	{
		alert(message);
		obj.focus();
		obj.select();
		return false;
	}
	return true;
}


/**
* @desc		들어온 데이타 값이 있는지 체크
* @param	str - String value
* @return	값이 있으면 true  없으면 false
*/
 function isNull(str)
 {
    return ((str == null || str == "" || str == "<undefined>") ? true:false);
 }

/**
* @desc		들어온 날짜를 체크 한다. (윤년 계산 및 입력날짜 Check)
* @param	str - date 값이 들어온다.  ex)20010120
* @return	들어온 값이 날짜면 true, 아니면 false
*/

function checkDate(str)                 /* 윤년 계산 및 입력날짜 Check */  
{
        var yy,mm,dd,ny,nm,nd;
        var arr_d;

        if(str.length != 8)
                return false;

        yy = str.substring(0,4);           // 년, 월, 일을 문자열로 가지고 있는다.  
        mm = str.substring(4,6);
        dd = str.substring(6,8);

        if(mm < '10')                                     // patch 판  
                mm = mm.substring(1);             // patch 판  

        if(dd < '10')                                     // patch 판  
                dd = dd.substring(1);             // patch 판  

        ny = parseInt(yy);        // 년, 월, 일을 정수형으로 가지고 있는다.  
        nm = parseInt(mm);
        nd = parseInt(dd);

        if(!(Number(yy)) || (ny < 1000) || (ny>9999)) return false;

        if(!(Number(mm)) || (nm < 1) || (nm > 12))
                return false;

        arr_d = new Array('31','28','31','30','31','30','31','31','30','31','30','31')

        //윤년계산
        if(((ny % 4 == 0)&&(ny % 100 !=0)) || (ny % 400 == 0))
        arr_d[1] = 29;

        if(!(Number(dd)) || (nd < 1) || (nd > arr_d[nm-1]))  return false;

        return true;
}

/**
* @desc		 왼쪽 0를 잘라서 return 값으로 넘긴다.
* @param	 str - String
* @return	 왼쪽 0를 잘라서 값으로 넘긴다.
*/

function Zero_LTrim(str)
{
       if(str.charAt(0) == "0")
        {str=str.substring(1,str.length); return Zero_LTrim(str);}
      else
         return str;
}
 
/**
* @desc		String 값이 전화번호 입력인지 체크
* @param	num - number
* @return	맞으면 true , 틀리면 false 값을 return 한다.
*/

function IsTel(num) {
     for(i=0;i<num.length;i++)
        {
              if((num.charAt(i) < '0' || num.charAt(i) > '9') && num.charAt(i) !='-')
              {
                  return false;
                }
        }

      return true;
}

/**
* @desc		현재 날짜에서 주어진 term(월) 만큼을 계산한다.
* @param	num - number
* @return	맞으면 true, 틀리면 false return 한다.
*/

function IsNumber(num)
	{
			var i;
			pcnt = 0;

				 for(i=0;i<num.length;i++)
				{
					if (num.charAt(i) == '.')
					{
						pcnt ++;
					}
				      if((num.charAt(i) < '0' || num.charAt(i) > '9') && (num.charAt(i) != '.' ||  pcnt>1))
				      {
					  return false;
				        }
				}

			      return true;
	}

/**
* @desc		오른쪽 공백를 잘라준다
* @param	str - String
* @return	str - 오른쪽 공백을 제거한 String  vlaue return
*/
function RTrim(str)
{
	if(str.charAt(str.length-1) == " ") {
  		str=str.substring(0,str.length-1);
  		return RTrim(str);
  	}else
	return str;
}

/**
* @desc		왼쪽 공백를 잘라준다.
* @param	str - String
* @return	str - 왼쪽 공백을 제거한 String  vlaue return
*/

function LTrim(str)
{

	if(str.charAt(0) == " "){
		str=str.substring(1,str.length);
		return LTrim(str);
	}else
		return str;
}

/**
* @desc		입력된 값이 Time (hhmmss)맞게 들어왔는지 체크
* @param	str - String
* @return	맞으면 true, 틀리면 false return 한다.
*/

function TimeCheck(str)
{

 var hh,mm,ss;

  if(str.length == 0)
     return true;

  if(IsNumber(str)==false || str.length !=6 )
    return false;

  hh=str.substring(0,2);
  mm=str.substring(2,4);
  ss=str.substring(4,6);

  if(parseInt(hh)<0 || parseInt(hh)>23)
     return false;

  if(parseInt(mm)<0 || parseInt(mm)>59)
     return false;

  if(parseInt(ss)<0 || parseInt(ss)>59)
     return false;

  return true;
}

/*
스크립트 상에서 금액단위별로 끊어서 보여준다..
*/

    function Comma(srcNumber)
    {
        var txtNumber = '' + srcNumber;

        if (isNaN(txtNumber) || txtNumber == "")
        {
            //alert("숫자를 넣어주세요");
            alert("please, Number Format!");
        }
        else
        {
            var rxSplit = new RegExp('([0-9])([0-9][0-9][0-9][,.])');
            var arrNumber = txtNumber.split('.');
            arrNumber[0] += '.';

            do
            {
                arrNumber[0] = arrNumber[0].replace(rxSplit, '$1,$2');
            } while (rxSplit.test(arrNumber[0]));

            if (arrNumber.length > 1)
            {
                return arrNumber.join('');
            }
            else
            {
                return arrNumber[0].split('.')[0];
            }
        }
    }

function CenterWindow(height,width)
{
	var outx = screen.height;
	var outy = screen.width;
	var x = (outx - height)/2;
	var y = (outy - width)/2;
	dim = new Array(2);
	dim[0] = x;
	dim[1] = y;

	return  dim;
}

///////////소수점 포맷/////////////////////////////////////

//소수점이하는 제외한 정수부분

function fixed_number(aa)
{
    var len = aa.length;
    var tmp = "" ;
    var i = 0 ;
    for(i = 0 ; i < len ; i++)
    {
        if(aa.charAt(i) == ".")
        {
            break ;
        }
        tmp +=  aa.charAt(i) ;
    }
    return tmp ;
}


  //정수를 제외한 소수점 이하부분  
function decimal_number(bb, point_pos)
{
    var len = bb.length;
    var tmp = "" ;
    var x = 1;

    for(i = 0 ; i < len ; i++)
    {
        if(bb.charAt(i) == ".")
        {
            i = eval(i) + 1 ;
            for(k = i; i <= len; i++)
            {
                tmp +=  bb.charAt(i);
            }
            break;
        }
    }

    var rtn = Round(tmp, point_pos);
    return rtn;
}

  //반올림을 한다.  
function Round(dd, pnt) {
	var tmp = "";
	if(dd.length == 0) {
		tmp = "0.";
  		//소수점자리가 없는경우  
		for (i = 0; i<eval(pnt); i++) {
			tmp += "0";
		}
	} else if (dd.length < eval(pnt)) {
 		//소수점 자리가 있으나 소수점이하 자리수가 작을 경우  
		tmp = "0."+dd;
		for (i = dd.length; i<eval(pnt); i++) {
			tmp += 0;
		}
	} else if (dd.length == eval(pnt)) {
		//소수점 자리가 있으나 소수점이하 자리수가 같을 경우  
		tmp = "0."+dd;

		for (i = dd.length; i<eval(pnt); i++) {
			tmp += 0;
		}
	} else  if (dd.length > eval(pnt)) {
  		//소수점 자리가 있으나 소수점이하 자리수가 클 경우  
		tmp = "0."+dd;
		tmp = point(tmp, pnt);
	}
	return tmp;
}

  //소수점자리 Fomatter  
function point(data, point_pos) {
	var result;       //결과값 리턴. 
	var x = 1;

	for (var i = 0; i<point_pos; i++) {
		x = eval(x)*10;
	}

	var a = eval(data)*eval(x);
	var result = Math.round(a);
	result = result/x;
	return result;
}

  //소수점 찾기 
function searchDot(input)
{
    var output = IsTrimStr(input);
    var s = "" ;

    for(i=0; i<output.length;i++)
    {
        if(output.charAt(i)==".")
        {
            s = i ;
            return s;
            break;
        }
    }
    return s ;
}

function add_comma(input, num)
{
    var output = "";
    var output1 = "";
    var output2 = "";
    var temp1 = IsTrimStr(input);
      //var temp1 = input;  


    if(temp1 != "") {
        var temp = fixed_number(temp1);

        i = temp.length ;
        var k = i / 3 ;
        var m = i % 3 ;
        var n= 0;
        if(m==0) {
            for(j=0;j<k-1;j++) {
                output1 += temp.substring(n, j*3+3)+",";
                n=j*3+3;
            }
        } else {
            for(j=0;j<k-1;j++)
            {
                output1 += temp.substring(n, j*3+m)+",";
                n=j*3+m;
            }
        }

        output1 += temp.substring(n,temp.length);

        var h = searchDot(temp1);

        if(num != "0") {
            output2 += "." ;
        }

        if(h == "")
        {
            for(p=0; p<num; p++)
            {
                output2 += "0" ;
            }
        } else {
            var temp2 = decimal_number(temp1,num)+"" ;
            temp2 = temp2.substring(1,temp2.length);
            output2 = temp2;
        }
        output = output1 + output2 ;
    } else if(temp1 == "") {
        if(num == "0")
        {
            output += "" ;
        }
        else
        {
            output += "0." ;
        }

        for(p=0; p<num; p++)
        {
            output += "0" ;
        }
    }
    return output;
}

  //콤마(,)  빼기  
function del_comma(input)
{
    var s = 0;

   for(i=0; i<input.length;i++)
    {
        if(input.charAt(i)==",")
        {
            s++ ;
        }
    }

    for(i = 0;i < s; i++)
    {
        input = input.replace(",", "");
    }
    return input;
}

  //공백을 제거하는 함수 
function IsTrimStr(checkStr)
{
    var str = "";
    checkStr = checkStr.toString();
    for (i = 0;i < checkStr.length;i++)
    {
        ch = checkStr.charAt(i);
        if (ch != " ")
        {
            str = str + ch;
        }
    }
    return str;
}

  //한글 입력 못하게 
function chkHangul(val)
{
    var j = 0;

    for (j=0; j<val.length; j++)
    {
        if (val.charCodeAt(j) > 255)
        {
            return false;
            break;
        }
    }
    return true;
}

//소수점 자리 제거
function fomatter(x, point) {
	var y;	  //정수부분  
	var z;    //소수부분  
	if (chkHangul(x)) {
		x = IsTrimStr(x);
 		//document.forms[0].Y.value = fixed_number(x);  

		y = fixed_number(x);	  //정수 

		var num= decimal_number(x, point);
	    num = num.toString();

	    var num1 = num.substring(1, num.length);   //소수점 이하   
		 var num2 = num.substring(0, 1);	  //소수점 이상의 정수부분  

		if (eval(num2) > 0) {
			y = eval(y) + eval(num2);
		}

		var a = y;
		var b = num1;

		if (b.length == 0) {
			b = decimal_number(a, point);
			b = b.substring(1, b.length);     //소수점 이하   --%>
		}

		a = IsTrimStr(a);
		var c = add_comma(a, 0)+b;
		return c;
	} else {
		alert("Input number.");
		return;
	}
}

/*  엔터키 입력시 검색함수 호출 */
function enableEnterKey() {
	function onkeypress(e) {
		if(window.event.keyCode == 13 && window.event.srcElement.type != "textarea" && window.event.srcElement.type != "button") { // 13 : Enter
			if(typeof setQuery == "function")
				setQuery();
			else if(typeof search == "function")
				search();
			else if(typeof Search == "function")
				Search();
			else if(typeof Query == "function")
				Query();
			else if(typeof query == "function")
				query();
			else if(typeof getQuery == "function")
				getQuery();
			else if(typeof doQuery == "function")
				doQuery();
			else if(typeof doSelect == "function")
				doSelect();
		}
	}

	document.onkeypress = onkeypress;
}
enableEnterKey();


function isMail(val){
	if ( val.search(/(^\..*)|(.*\.$)/) != -1 || val.search(/\S+@(\S+)\.(\S+)/) == -1) {
			return false;
		}
	return true;
}

function attach_file(attach_no, module)
{
	var ATTACH_VALUE = LRTrim(attach_no);
	if("" == ATTACH_VALUE) {
		FileAttach(module,'','');
	} else {
		FileAttachChange(module, ATTACH_VALUE);
	}
}

function SaveFileCommon(col)
{
  	//var Rtn = GD_ExcelExport(document.WiseGrid,col);
  	var Rtn = SepoaGridExcelExport(document.WiseGrid,col);

	if(Rtn) {
    /*	if(confirm("'" + Rtn + "' 에 저장된 파일 열기")) {
			window.open(Rtn,"xls","width=800,height=550,menubar=YES,resizable=YES,scrollbars=YES,top=60,left=20");
		}
    */
    	alert("Saved Complete!" );
	}
}

/*
	중복검사
*/
function isDuplicate(wise, bidx, cidx, message)
{
	var iRowCount = document.WiseGrid.GetRowCount();

	var init = iRowCount-1;

	for(var i=init;i>0;i--)
	{
		for(var j=i-1;j>=0;j--)
		{
			if(SepoaGridGetCellValueIndex(document.WiseGrid,i,bidx)=="true")
			{
				//키
				var key1 = "";
				var key2 = "";

				for(var k=0;k<cidx.length;k++)
				{
					key1 = key1 + SepoaGridGetCellValueIndex(document.WiseGrid,i,cidx[k]);
					key2 = key2 + SepoaGridGetCellValueIndex(document.WiseGrid,j,cidx[k]);
				}

				if(key1 == key2)
				{
					alert(message + (i+1) + " Row,  " + (j+1) + " Row");
					return true;
				}

			}
		}
	}
	return false;
}

function getCheckedOneValue(wise, idx, need_idx)
{
	for(var i=0;i<document.WiseGrid.GetRowCount();i++)
	{
		if(SepoaGridGetCellValueIndex(document.WiseGrid,i,idx) == "true")
		{
			return SepoaGridGetCellValueIndex(document.WiseGrid,i,need_idx);
		}
	}
}


/*
	와이즈테이블의 체크된 항목을 지운다.
*/
function deleteWiseTable(wise, idx)
{
	for(var i=document.WiseGrid.GetRowCount()-1;i>=0;i--)
	{
		if(SepoaGridGetCellValueIndex(document.WiseGrid,i,idx) == "true")
		{
			document.WiseGrid.DeleteRow(i);
		}
	}
}


/*
	빈값일 경우 0을 리턴한다.
	eval로  계산을 위해 사용한다.
*/
function getCalculEval(value)
{
	if(value == "")
		return 0;
	return eval(value);
}

function commaPrice(obj, type, number)
{
	if(!isNumberCommon(obj.value))
	{
		obj.value = "";
		return;
	}

	if(type == "add")
		obj.value = add_comma(obj.value,number);
	else
		obj.value = del_comma(obj.value);

}

/*
	와이즈 테이블의 체크된 제일 위 항목을 가져온다.
*/
function getCheckedPos(wise, idx)
{
	for(var i=0;i<document.WiseGrid.GetRowCount();i++)
	{
		if(SepoaGridGetCellValueIndex(document.WiseGrid,i,idx) == "true")
		{
			return i;
		}
	}
	return -1;
}

function floor_number(number, point)
{
	cnj_x = number;
	cnj_y = point;
	return Math.floor(cnj_x * Math.pow(100, cnj_y)) / Math.pow(100, cnj_y);
}

function ceil_number(number, point)
{
	cnj_x = number;
	cnj_y = point;
	return Math.ceil(cnj_x * Math.pow(100, cnj_y)) / Math.pow(100, cnj_y);
}

function round_number(number, point)
{
	cnj_x = number;
	cnj_y = point;
	return Math.round(cnj_x * Math.pow(100, cnj_y)) / Math.pow(100, cnj_y);
}

function copyCell(wise, idx, type)
{


	if(document.WiseGrid.GetRowCount() < 2)
		return;

	var value = SepoaGridGetCellValueIndex(document.WiseGrid,0,idx);
	var text = document.WiseGrid.GetCellValue(document.WiseGrid.GetColHDKey(idx),0);

	if(isEmpty(value))
	{
		for(i=1; i<document.WiseGrid.GetRowCount(); i++)
		{
			value = SepoaGridGetCellValueIndex(document.WiseGrid,i,idx);
			text = document.WiseGrid.GetCellValue(document.WiseGrid.GetColHDKey(idx),i);
			if(!isEmpty(value))
			{
				break;
			}
		}
	}


	if(type == "t_imagetext")
	{
		for(i=0; i<document.WiseGrid.GetRowCount(); i++)
		{
			SepoaGridSetCellValueIndex(document.WiseGrid,i, idx, G_IMG_ICON + "&"+text+"&"+value, "&");
		}
	}
	else
	{
		for(i=0; i<document.WiseGrid.GetRowCount(); i++)
		{
			SepoaGridSetCellValueIndex(document.WiseGrid,i, idx, value);
		}
	}
}

/*
	빈값일 경우 0을 리턴한다.
	float 계산을 위해 사용한다.
*/
function getCalculInt(value)
{
	if(value == "")
		return 0;
	return parseInt(value);
}

/*
	빈값일 경우 0을 리턴한다.
	float 계산을 위해 사용한다.
*/
function getCalculFloat(value)
{
	if(value == "")
		return 0;
	return parseFloat(value);
}

function sendTransactionDelete(GridObj, data_processor, selected, grid_array)
{
	if(grid_array == null)
	{
		alert("grid_array is null.");
		return;
	}
	data_processor.init(GridObj);
	data_processor.enableDataNames(true);
	data_processor.setTransactionMode("POST", true);
	//data_processor.setTransactionMode("GET", true);
	data_processor.defineAction("doSaveEnd", doSaveEnd);
	// data_processor.enableDebug(true);
	data_processor.setUpdateMode("off");
	var row = 0;

	//for(row = dhtmlx_start_row_id; row < dhtmlx_end_row_id; row++)
	for(row = 0; row < grid_array.length; row++)
	{
		//checked = GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById(selected)).getValue();
		checked = GridObj.cells(grid_array[row], GridObj.getColIndexById(selected)).getValue();

		if(checked == "1") {
			//myDataProcessor.setUpdated(GridObj.getRowId(row), true);
			myDataProcessor.setUpdated(grid_array[row], true);
		} else {
			//myDataProcessor.setUpdated(GridObj.getRowId(row), false);
			myDataProcessor.setUpdated(grid_array[row], false);
		}
    }
	myDataProcessor.setUpdateMode("row");
	myDataProcessor.sendData();
	myDataProcessor.setUpdateMode("off");
	doQueryDuring();
}

function sendTransaction(GridObj, data_processor)
{
	data_processor.init(GridObj);
	data_processor.enableDataNames(true);
	// data_processor.setTransactionMode("POST", true);
	data_processor.setTransactionMode("GET", true);
	data_processor.defineAction("doSaveEnd", doSaveEnd);
	// data_processor.enableDebug(true);
	data_processor.setUpdateMode("off");

	myDataProcessor.setUpdateMode("row");
	myDataProcessor.sendData();
	myDataProcessor.setUpdateMode("off");
	doQueryDuring();
}

function sendTransactionGrid(GridObj, data_processor, selected, grid_array)
{
	if(grid_array == null)
	{
		alert("grid_array is null.");
		return;
	}
	data_processor.init(GridObj);
	data_processor.enableDataNames(true);
	data_processor.setTransactionMode("POST", true);
	//data_processor.setTransactionMode("GET", true);
	data_processor.defineAction("doSaveEnd", doSaveEnd);
	// data_processor.enableDebug(true);
	data_processor.setUpdateMode("off");
	var row = 0;

	//for(row = dhtmlx_start_row_id; row < dhtmlx_end_row_id; row++)
	for(row = 0; row < grid_array.length; row++)
	{
		//checked = GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById(selected)).getValue();
		checked = GridObj.cells(grid_array[row], GridObj.getColIndexById(selected)).getValue();

		if(checked == "1") {
			//myDataProcessor.setUpdated(GridObj.getRowId(row), true);
			myDataProcessor.setUpdated(grid_array[row], true);
		} else {
			//myDataProcessor.setUpdated(GridObj.getRowId(row), false);
			myDataProcessor.setUpdated(grid_array[row], false);
		}
    }
	myDataProcessor.setUpdateMode("row");
	myDataProcessor.sendData();
	myDataProcessor.setUpdateMode("off");
	doQueryDuring();
}

function sendTransactionGridPost(GridObj, data_processor, selected, grid_array)
{
	if(grid_array == null)
	{
		alert("grid_array is null.");
		return;
	}
	data_processor.init(GridObj);
	data_processor.enableDataNames(true);
	data_processor.setTransactionMode("POST", true);
	data_processor.defineAction("doSaveEnd", doSaveEnd);
	// data_processor.enableDebug(true);
	data_processor.setUpdateMode("off");
	var row = 0;

	//for(row = dhtmlx_start_row_id; row < dhtmlx_end_row_id; row++)
	for(row = 0; row < grid_array.length; row++)
	{
		//checked = GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById(selected)).getValue();
		checked = GridObj.cells(grid_array[row], GridObj.getColIndexById(selected)).getValue();

		if(checked == "1") {
			//myDataProcessor.setUpdated(GridObj.getRowId(row), true);
			myDataProcessor.setUpdated(grid_array[row], true);
		} else {
			//myDataProcessor.setUpdated(GridObj.getRowId(row), false);
			myDataProcessor.setUpdated(grid_array[row], false);
		}
    }
	myDataProcessor.setUpdateMode("row");
	myDataProcessor.sendData();
	myDataProcessor.setUpdateMode("off");
	doQueryDuring();
}

function GridGetCellValueId(obj, row_id, column_name)
{  	
	if(obj.getColType(obj.getColIndexById(column_name)) == "ch") {
 	    //if(obj.cells2(row_id, 0).getValue() == 0) { 2011.04.04 컬럼 아이디로 체크하지 않고 인덱스로 체크
 	    // if(obj.cells(row_id, obj.getColIndexById(column_name)).getValue() == 0) {
 	    if(obj.cells2(row_id, 0).getValue() == 0) {
			return 'false';
	    } else {
		   	return 'true';
	    }
   	} else {
   		return obj.cells(row_id, obj.getColIndexById(column_name)).getValue();
   	}
}

function getGridChangedRows(gridobj, selected)
{
	// var changed_array = new Array();
	var grid_array = new Array();
	var grid_index = 0;
//	var changed_row = gridobj.getChangedRows();
//	changed_array = changed_row.split(",");

	for(i = 0; i < gridobj.getRowsNum(); i++)
	{	
		if(GridGetCellValueId(gridobj, i, selected) == "true")
		{
			grid_array[grid_index] = i+1;
			grid_index++;
		}
	}

	return grid_array;
}

function getGridChangedRows2(gridobj, selected)
{
	var grid_array = new Array();
	var grid_index = 0;
	
	for(var i = 0; i < GridObj.getRowsNum(); i++)
	{	
		if(GridGetCellValueId(GridObj, i, selected) == "true")
		{   
			grid_array[grid_index] = GridObj.getRowId(i);
			grid_index++;
		}
	}
	return grid_array;
}

function tempFunc(column_name)
{
    var checked_count = 0;
    var checked_row   = -1;

    var grid_array = getGridChangedRows(GridObj, column_name);
    checked_count = grid_array.length;

	if(checked_count == 0)  {
        return -1;
    }

    checked_row = grid_array[0];

    return checked_row;
}

function chkLastOneRow(column_name)
{
    var checked_count = 0;
    var checked_row   = -1;

    var grid_array = getGridChangedRows(GridObj, column_name);
    checked_count = grid_array.length;

	if(checked_count == 0)  {
        return -1;
    }

    checked_row = grid_array[grid_array.length - 1];

    return checked_row;
}
function bizNoCheck(value) {
	var aa = 0;
	aa += parseInt(value.substring(0,1));
	aa += parseInt(value.substring(1,2)) * 3 % 10;
	aa += parseInt(value.substring(2,3)) * 7 % 10;
	aa += parseInt(value.substring(3,4)) * 1 % 10;
	aa += parseInt(value.substring(4,5)) * 3 % 10;
	aa += parseInt(value.substring(5,6)) * 7 % 10;
	aa += parseInt(value.substring(6,7)) * 1 % 10;
	aa += parseInt(value.substring(7,8)) * 3 % 10;
	aa += Math.floor(parseInt(value.substring(8,9)) * 5 / 10);
	aa += parseInt(value.substring(8,9)) * 5 % 10;
	aa += parseInt(value.substring(9,10));
	if(aa % 10 != 0) {
		return false;
	} else {
		return true;
	}
}

function grid_width_resize() {
	var gridbox_value = document.getElementById("gridbox");
	if(gridbox_value != null) {
		document.getElementById("gridbox").style.width = 1+"px";
	}
}

function jQueryCalendar(obj, imgObj) {
	$(function() {
		$( "#"+obj ).datepicker({
			dateFormat: 'yy/mm/dd',
			showOtherMonths: true,
			changeMonth: true,
			changeYear: true,
			
			beforeShow: function(input, inst) { 
				inst.dpDiv.css({marginTop: '0px', marginLeft: '0px'});
			}
		});
	
		$('#'+imgObj).click(function() {$('#'+obj).focus();});
	});
}

function jQueryFromToCalendar(obj1, imgObj1, obj2, imgObj2) {
	$(function() {
		$( "#"+obj1 ).datepicker({
			dateFormat: 'yy/mm/dd',
			showOtherMonths: true,
			changeMonth: true,
			changeYear: true,
			onSelect: function (dateText, inst) {
				var sToDate = jQuery.trim($('#'+obj2).val());
				if (sToDate.length > 0) {
					var iToDate   = parseInt(replace(sToDate, "/", ""));
					var iFromDate = parseInt(replace(dateText, "/", ""));
					if (iFromDate > iToDate) {
						alert("시작일보다 종료일이 과거일 수 없습니다.");
						$('#'+obj1).val(sToDate);
					}
				}
			},
			beforeShow: function(input, inst) { 
				inst.dpDiv.css({marginTop: '0px', marginLeft: '0px'});
			}
		});
		$( "#"+obj2 ).datepicker({
			dateFormat: 'yy/mm/dd',
			showOtherMonths: true,
			changeMonth: true,
			changeYear: true,
			onSelect: function (dateText, inst) {
				var sFromDate = jQuery.trim($('#'+obj1).val());
				if (sFromDate.length > 0) {
					var iFromDate = parseInt(replace(sFromDate, "/", ""));
					var iToDate   = parseInt(replace(dateText, "/", ""));
					
					if (iFromDate > iToDate) {
						alert("시작일보다 종료일이 과거일 수 없습니다.");
						$('#'+obj2).val(sFromDate);
					}
				}
			},
			beforeShow: function(input, inst) { 
				inst.dpDiv.css({marginTop: '0px', marginLeft: '0px'});
			}
		});
	
		$('#'+imgObj1).click(function() {$('#'+obj1).focus();});
		$('#'+imgObj2).click(function() {$('#'+obj2).focus();});
	});
}

function close_popup_insert(obj) {
    try {
	    top.popupWindow.push(obj);
	} catch(e) {
		try {
			parent.top.popupWindow.push(obj);
		} catch(e) {
			try {
				parent.parent.top.popupWindow.push(obj);
			} catch(e) {
				try {
					parent.parent.parent.top.popupWindow.push(obj);
				} catch(e) {}
			}
		}
	}
    try {
	    opener.top.popupWindow.push(obj);
	} catch(e) {
		try {
			opener.parent.top.popupWindow.push(obj);
		} catch(e) {
			try {
				opener.parent.parnet.top.popupWindow.push(obj);
			} catch(e) {
				try {
					opener.parent.parent.parent.top.popupWindow.push(obj);
				} catch(e) {}
			}
		}
	}
    try {
	    opener.opener.top.popupWindow.push(obj);
	} catch(e) {
		try {
			opener.opener.parent.top.popupWindow.push(obj);
		} catch(e) {
			try {
				opener.opener.parent.parent.top.popupWindow.push(obj);
			} catch(e) {
				try {
					opener.opener.parent.parent.parent.top.popupWindow.push(obj);
				} catch(e) {}
			}
		}
	}
}

function close_popup() {
    try {
	    top.popupWindowClose();
	} catch(e) {
		try {
			parent.top.popupWindowClose();
		} catch(e) {
			try {
				parent.parent.top.popupWindowClose();
			} catch(e) {
				try {
					parent.parent.parent.top.popupWindowClose();
				} catch(e) {}
			}
		}
	}
    try {
		opener.top.popupWindowClose();
	} catch(e) {
		try {
			opener.parent.top.popupWindowClose();
		} catch(e) {
			try {
				opener.parent.parnet.top.popupWindowClose();
			} catch(e) {
				try {
					opener.parent.parent.parent.top.popupWindowClose();
				} catch(e) {}
			}
		}
	}
    try {
		opener.opener.top.popupWindowClose();
	} catch(e) {
		try {
			opener.opener.parent.top.popupWindowClose();
		} catch(e) {
			try {
				opener.opener.parent.parent.top.popupWindowClose();
			} catch(e) {
				try {
					opener.opener.parent.parent.parent.top.popupWindowClose();
				} catch(e) {}
			}
		}
	}		
}

function backSpaceFalse() {
	$(document).keydown(function(event){
	  var code;
	  if (event.keyCode) code = event.keyCode;
	  else if (event.which) code = event.which;
	
	  //백스페이스 와 엔터키 안되도록 처리
	  if(code==8 || code==13 )
	  {
	   var targetNode = event.target.nodeName;
	   var readonly = event.target.readOnly;
	   var disabled = event.target.disabled;
	   var type = event.target.type;
	
	   //type이 password나 text인 input와 textarea를 제외한 모든 엘리먼트에서 백스페이스기능을 제한함
	   if(!(((targetNode=="INPUT"&&(type=="text"||type=="password"))||targetNode=="TEXTAREA")&&(!readonly&&!disabled) ) )
	   {
	    if(event.preventDefault)
	     event.preventDefault();
	    else
	     event.returnValue = false;
	   }
	  }
	});
}

//이규현 추가 바코드 스캔 시 일련번호만 잘라오기
function doScan() {
	if(document.form.SERIAL_NO.value.length == 20) {
		document.form.SERIAL_NO.value  = document.form.SERIAL_NO.value.substring(11);
	} 
}
