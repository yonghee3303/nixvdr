  var websocket;
  var isConnection = false;
  
  function WebSocketConnect(){
    //open socket
    if ("WebSocket" in window){
      //websocket = new WebSocket("ws://localhost:9480/MgWebUSB");
    	websocket = new WebSocket("ws://localhost:4116/USB");
      //note this server does nothing but echo what was passed
      //use a more elaborate server for more interesting behavior
      // output.innerHTML = "connecting..." ;

      //attach event handlers
      websocket.onopen = onOpen;
      websocket.onclose = onClose;
      websocket.onmessage = onMessage;
      websocket.onerror = onError;
    } else {
    	alert("사용 중인 브라우저는 웹소켓을 지원하지 않는 브라우저 입니다.");
      //alert("WebSockets not supported on your browser.");
    } // end if
  } // end connect
  
  function sendWsMessage(message) {
	  if(isConnection == false) {
		  //alert("Failed to connect to the WebSockets.");
		  return;
	  }
	  //alert(message);
	  websocket.send(message);
  }
  function closeWs(){
	  websocket.close();
  }
  function websocketServiceDownload() {
	  if(confirm('보안USB 프로그램 미설치 또는 버전 업데이트로 인해 설치가 필요합니다. 설치하시겠습니까?')) {
		  window.location.href = "../../../mg/su/file/MgWebUSBSetup.exe";
		  alert('프로그램 설치 완료 후 새로고침 버튼을 눌어주세요.');
		  //window.open("../../../mg/su/file/MgWebUSB.exe");
	  }
  }