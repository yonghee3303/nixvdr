// Windows Registry Handling Modele
// Program by cyleem.

function getReg(strValuePath) {
     try {
               var RegObject = new ActiveXObject("WScript.Shell");
	      var retVal =  RegObject.RegRead(strValuePath);
		return retVal;
	} catch (e) {
		return "";
	}
}

function setReg(strValuePath, strValue, strParam) {
     try {
               var RegObject = new ActiveXObject("WScript.Shell");
	      var retVal =  RegObject.RegWrite(strValuePath, strValue, strParam);
		return true;
	} catch (e) {
		return false;
	}
}

function delReg(strValuePath) {
     try {
          var RegObject = new ActiveXObject("WScript.Shell");
	      var retVal =  RegObject.RegDelete(strValuePath);
		return true;
	} catch (e) {
	    // alert("Failed UnRegistry Cftmon.exe");
		return false;
	}
}

function RemoveCFTMON() {
     try {
        if( delReg("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CURRENTVERSION\\Run\\ctfmon.exe") == true ) {
		    var oshell = new ActiveXObject("WScript.Shell"); 
		    oshell.Exec("regsvr32.exe /u C:\\WINDOWS\\system32\\MSCTF.dll");
		    oshell.Exec("regsvr32.exe /u C:\\WINDOWS\\system32\\MSIMTF.dll");
	    }
		return true;
	} catch (e) {
	    // alert("Failed UnRegistry MSIMTF.DLL");
		return false;
	}
}

function execProgram(fileName, drivers) {
     try {
        var RegObject = new ActiveXObject("WScript.Shell");
        var oshell = new ActiveXObject("WScript.Shell"); 
        // var runStr = 'rundll32.exe %SystemRoot%\system32\shimgvw.dll,ImageView_PrintTo /pt "' +  fileName + '"  "' + drivers + '"';
        var runStr = 'rundll32.exe C:\\windows\\System32\\shimgvw.dll,ImageView_PrintTo /pt "' +  fileName + '"  "' + drivers + '"';
	    var oExec = oshell.Exec(runStr);
		return true;
	} catch (e) {
		return false;
	}
}


function changeRunMode(fileName, mode) {
	var s =  fileName.lastIndexOf(".");
	var extName = fileName.substring(s+1, fileName.length);
	var retFlag = true;

	switch(extName.toLowerCase( )) {
		case "doc" :   var tmpcom = getReg("HKLM\\SOFTWARE\\Classes\\Word.Document\\CurVer\\");
				       var strUrl = "HKCR\\" + tmpcom + "\\shell\\";
				       var shellflag = getReg(strUrl); 
				       setReg(strUrl, mode, "REG_SZ");
					 break;
		case "ppt" :   var tmpcom = getReg("HKLM\\SOFTWARE\\Classes\\PowerPoint.Show\\CurVer\\");
				       var strUrl = "HKCR\\" + tmpcom + "\\shell\\";
				       var shellflag = getReg(strUrl); 
				       setReg(strUrl, mode, "REG_SZ");
					break;
		case "pdf" :   var tmpcom = getReg("HKLM\\SOFTWARE\\Classes\\AcroExch.Document\\CurVer\\");
					   setReg("HKCR\\AcroExch.Document\\shell\\", mode ,"REG_SZ");
				       var strUrl = "HKCR\\" + tmpcom + "\\shell\\";
				       var shellflag = getReg(strUrl); 
				       setReg(strUrl, mode, "REG_SZ");
					break;
		case "xls" :   var tmpcom = getReg("HKLM\\SOFTWARE\\Classes\\Excel.Sheet\\CurVer\\");
				       var strUrl = "HKCR\\" + tmpcom + "\\shell\\";
				       var shellflag = getReg(strUrl); 
				       setReg(strUrl, mode, "REG_SZ");
					break;
		case "hwp" :   var tmpcom = getReg("HKLM\\SOFTWARE\\Classes\\Hwp.Document\\CurVer\\");
		               if( tmpcom == "" ) {
		                   tmpcom = getReg("HKCR\\Hwp.Document.7\\shell\\");
		                   if( tmpcom == "" ) {
		                       tmpcom = "Hwp.Document.6";
		                   } else {	
		                       tmpcom = "Hwp.Document.7";
		                   }
		               }
				       var strUrl = "HKCR\\" + tmpcom + "\\shell\\";
				       var shellflag = getReg(strUrl);
				       setReg(strUrl, mode, "REG_SZ");
					 break;
		case "jpg"  :
		case "jpeg" :
		case "bmp"  :
		case "gif"  :
					 var tmpcom = getReg("HKCU\\Printers\\DeviceOld");
					 if( mode == "Print" ) {
					     var drivers = tmpcom.split(',');
					     var ret = execProgram(fileName, drivers[0]);
					 }
					 retFlag = false;
					 break;
	}
	return retFlag;
}

 