/**
 * @author LG CNS
 */


var w_width = 0;
var w_height = 0;

$(document).ready(function(){
	menuActive();
	shuttleTree();
});

$(window).load(function(){
	$(window).resize(function(){
		w_height = $(this).height();
		w_width = $(this).width();
		$('.container').css('height', (w_height - $('.header').height()) + "px");
		//96은 LBar의 width + LBar의 좌우여백 + Scroll 영역의 값이다.
		$('.contents').css('width', (w_width - ($('.leftMenu').width() + 96 )) + "px");
	});
	$(window).resize();
});

//left 영역 열고 닫기 
function clsopnleftMenu(){
	
		if($('.leftTop').hasClass('close')) {
		  $('.leftMenu').css('width', '20px');
			$('.leftBg').addClass('closed');
		  	$('.leftTop').removeClass('close');
		 
		  $('.contents').css('width', (w_width - 116 ) + "px");
		  $('.closeOpen').hide();
		  $('.leftUnderBanner').hide();
		  		  
	}else {
		  
		  $('.leftMenu').css('width', '189px');
		  $('.leftTop').addClass('close');
		  $('.leftBg').removeClass('closed');
		  $('.contents').css('width', (w_width - 285 ) + "px");
		  
		  $('.closeOpen').show();
		  $('.leftUnderBanner').show();
 }	
	
}

//헤더영역의 메뉴 클릭 시 호버 영역 및 메가드롭 나타내기
function globalNavigate(target) {
	var item = "." + target + " a";
	var mega = "." + target + " .megaDrop";
	
	$(".hMenu").mouseleave();
	
	$(".hMenu a").removeClass('on');
	$(item).toggleClass('on');
	
	$(".megaDrop").removeClass('Lnodisplay');
	$(".megaDrop").removeClass('on');
	$(mega).addClass('on');	
}

//퀵메뉴 영역 클릭시 레이어팝업 나타내기
function clickBanner(target) {

	var item = "." + target;

	if(target == "qQuick") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qVisited").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSearch").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSms").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qFont").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qLang").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qHelp").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}
	if(target == "qVisited") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qQuick").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSearch").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSms").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qFont").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qLang").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qHelp").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}
	if(target == "qSearch") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qVisited").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qQuick").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSms").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qFont").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qLang").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qHelp").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}	
	if(target == "qSms") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qQuick").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSearch").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qVisited").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qFont").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qLang").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qHelp").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}
	if(target == "qFont") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qVisited").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qQuick").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSms").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSearch").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qLang").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qHelp").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}
	if(target == "qLang") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qQuick").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSearch").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qVisited").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qFont").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSms").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qHelp").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}
	if(target == "qHelp") {
		$(item).toggleClass('on');	
		$(item).parent().children("div.qLayerpop").toggleClass('Lnodisplay');
		$(".qVisited").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qQuick").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSms").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qSearch").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qFont").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
		$(".qLang").removeClass('on').parent().children("div.qLayerpop").addClass('Lnodisplay');
	}
	
}
//즐겨찾기 버튼 클릭시  아이콘 바뀌기
function toggleTitleIcon(target) {
	$(".titleButton li a").removeClass('on');
	$(target).addClass('on');
}

//탭영역 클릭시 선택탭 나타내기 
function switchTabs(target, item) {
	$(".tabArea ul.tab li a").removeClass('selected');
	$(target).addClass('selected');
	var changeTab = "." + item;
	
	$(".tabUnder").addClass('Lnodisplay');
	if($(".tabUnder").hasClass(item)) {
		$(changeTab).removeClass('Lnodisplay');
	}
}

//셔틀 영역의 탭(혹은 탭 안의 탭) 클릭시 선택탭 나타내기
function switchShuttle(target) {
	$(target).parent().parent().children('li').removeClass('on');
	$(target).parent().addClass('on');

}

//레이어팝업에서 X버튼 클릭시 닫기
function closelayerpopup() {
	$(".qLayerpop").addClass('Lnodisplay');
	$(".qBanners a").removeClass('on');
}

//플래너의 탭 클릭시 선택탭 나타내기
function switchCalendar(target) {
	$(".plTop ul li").removeClass('on');
	$(target).parent().addClass('on');
}

//조회조건이 4행 이상인 경우 버튼 클릭시 모두 나타내기
function spreadInquiry() {
	
	var chImage = $(".btnSpread a img");
	var hiddenConds = $('.tableInquiry table tr.hideNshow');
	
	if(hiddenConds.hasClass('Lnodisplay')){
		$(chImage).attr('src', '../images/btn_search_fold.gif');
		$(hiddenConds).removeClass('Lnodisplay');
		
		} else {
		$(chImage).attr('src', '../images/btn_search_spread.gif');
		$(hiddenConds).addClass('Lnodisplay');
	}
}

//트리영역 구현
function shuttleTree(){
	var shuttleTree = $('.shuttleTree');
	var icon_open = '../images/ico/ico_tree_open.gif';
	var icon_close = '../images/ico/ico_tree_close.gif';
	shuttleTree.find('li:has("ul")').prepend('<a href="#" class="control"><img src="' + icon_open + '" /></a> ');
	shuttleTree.find('li:last-child').addClass('end');
		
		$('.control').click(function(){

			var temp_el = $(this).parent().find('>ul');
			if (temp_el.css('display') == 'none') {
				temp_el.slideDown(50);
				$(this).find('img').attr('src', icon_close);
				$(this).next('a').find('span').addClass('on');
				return;
			}
			else {
				temp_el.slideUp(50);
				$(this).find('img').attr('src', icon_open);
				$(this).next('a').find('span').removeClass('on');
				return;
			}
		});
		
}

//슬라이드 메뉴 혹은 트리 메뉴 아이콘 및 영역 바꾸기 
function toggleListType(target) {
	var item = "." + target;
	
	$(".leftType li a").removeClass('on');
	$(item).addClass('on');
	if(target == "list") {
		
		$("#leftMenuBlock").removeClass('treeMenu');
		$("#leftMenuBlock").addClass('slideMenu');
		$(".control").remove();
		$(".treeConArea").addClass('Lnodisplay');
		menuActive();
	} else
	if (target == "tree") {
		$("#leftMenuBlock").removeClass('slideMenu');
		$("#leftMenuBlock").addClass('treeMenu');
		$("#leftMenuBlock").css('min-height', '145px');		
		$(".treeConArea").removeClass('Lnodisplay');
        treemenuActive();
	}
	
}

//트리메뉴 구현
function treemenuActive(){
	var tree_menu = $('.treeMenu');
	var icon_open = '../images/ico/ico_tree_open.gif';
	var icon_close = '../images/ico/ico_tree_close.gif';
	//$.each(tree_menu.find('li:has("ul")'),function(index,item){
	//    if (item.children[0].children[0].attributes[0].value == 'on') {
	//        item.prepend('<a href="#" class="control"><img src="' + icon_close + '" /></a> ');
	//    } else {
	//        item.prepend('<a href="#" class="control"><img src="' + icon_open + '" /></a> ');
	//    }
	//});
	tree_menu.find('li:has("ul")').prepend('<a href="#" class="control"><img src="' + icon_open + '" /></a> ');
	tree_menu.find('li:last-child').addClass('end');
	
	$('.dashAdded').remove();
	
	$(".treeConArea").removeClass('Lnodisplay');	
	$("#all").css('display', 'block');	
	
	
		$('.control').click(function(){

			var temp_el = $(this).parent().find('>ul');
			if (temp_el.css('display') == 'none') {
				temp_el.slideDown(50);
				$(this).find('img').attr('src', icon_close);
				$(this).next('a').addClass('on');
				$(this).next('a').children('span').addClass('on');
				return false;
			}
			else {
				temp_el.slideUp(50);
				$(this).find('img').attr('src', icon_open);
				$(this).next('a').removeClass('on');
				$(this).next('a').children('span').removeClass('on');
				return false;
			}
		});
				
}
//트리메뉴 모두 열거나 모두 닫기
function tree_init(status){
	var tree_menu = $('.treeMenu');
	var icon_open = '../images/ico/ico_tree_open.gif';
	var icon_close = '../images/ico/ico_tree_close.gif';	
	if (status == 'close') {
		tree_menu.find('ul').hide();
		$('.control').find('img').attr('src', icon_open);
		$(".treeMenu .leftDepth1 span").removeClass('on');
		$(".treeMenu .leftDepth1").removeClass('on');
		$(".treeMenu .leftDepth2 span").removeClass('on');
		$(".treeMenu .leftDepth2").removeClass('on');
	}
	else 
		if (status == 'open') {
			tree_menu.find('ul').show();
			$('.control').find('img').attr('src', icon_close);
			$(".treeMenu .leftDepth1 span").addClass('on');
			$(".treeMenu .leftDepth1").addClass('on');
			$(".treeMenu .leftDepth2 span").addClass('on');
			$(".treeMenu .leftDepth2").addClass('on');
	}
}
//트리메뉴 모두 열거나 모두 닫을 때 텍스트 바꾸기
function clickTreeCon() {
	if($(".treeCon span").text() == "ALL OPEN") {
		tree_init('open');
		$(".treeCon span").text('ALL CLOSE');
	}else {
		tree_init('close');
		$(".treeCon span").text('ALL OPEN');
	}
}	


	
//슬라이드 메뉴 구현
function menuActive(){
	
	var appendDash = "<span class=\"dashAdded\">- </span>";
	$('.slideMenu .leftDepth2 span').before(appendDash);
	
		$('.slideMenu .leftDepth1').unbind("click");
		$('.slideMenu .leftDepth2').unbind("click");
		
		
		$('.slideMenu .leftDepth1').click(function(){
			
				$(".slideMenu .2depthArea").addClass('Lnodisplay').css('display', 'none');
				$(".slideMenu .leftDepth1 span").removeClass('on');
				$(".slideMenu .leftDepth1").removeClass('on');
				
				$(".slideMenu .3depthArea").addClass('Lnodisplay').css('display', 'none');
				$(".slideMenu .leftDepth2 span").removeClass('on');
				$(".slideMenu .leftDepth2").removeClass('on');
				
				if ($("#leftMenuBlock").hasClass('slideMenu')) {
					var slide_el = $(this).parent().find('>ul');
					
					if (slide_el.hasClass('Lnodisplay')) {
					
						slide_el.slideDown(100);
						$(slide_el).removeClass('Lnodisplay');
						$(this).addClass('on');
						$(this).children('span').addClass('on');
					}
					else {
						slide_el.slideUp(100);
						$(this).children('span.1depthArea').removeClass('on');
						$(slide_el).addClass('Lnodisplay');
					}
					
				}
					
		});
		
		$('.slideMenu .leftDepth2').click(function(){
			var depth2_el = $(this).parent().find('>ul');
			if (depth2_el.hasClass('Lnodisplay')) {
				depth2_el.slideDown(50);
				$(this).children('span').addClass('on');
				$(this).addClass('on');
				$(depth2_el).removeClass('Lnodisplay');
				return;
			}
			else {
				depth2_el.slideUp(50);
				$(this).children('span').removeClass('on');
				$(depth2_el).addClass('Lnodisplay');
				return;
			}
		});
}


//DatePicker;
/*$(function() {
	$( "#inputDateFrom" ).datepicker({
		defaultDate: "+1w",
		onClose: function( selectedDate ) {
			$( "#inputDateTo" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#inputDateTo" ).datepicker({
		defaultDate: "+1w",
		onClose: function( selectedDate ) {
			$( "#inputDateFrom" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
 */
 
 
 
 
 