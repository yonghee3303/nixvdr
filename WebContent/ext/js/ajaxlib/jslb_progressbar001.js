//--jslb----------------------------------------------------------------------------
// 최신 정보: http://jsgt.org/mt/01/
// 저작권 표시의무 없음. 상업 이용과 개조는 자유. 연락 필요 없음.

////
// jsgt_ProgressBar 프로그래시브 바 오브젝트 
//
// @author     Toshirou Takahashi http://jsgt.org/mt/archives/01/000543.html
// @version    0.01 
// @license    Public Domain 저작권 표시 의무 없음. 상용이용, 개조, 자유. 연락 불요.
// @sample     oj = new jsgt_ProgressBar()           //DIV를 자동 생성하는 경우
// @sample     oj = new jsgt_ProgressBar('nloading') //기존의 DIV 이름으로 지정하는 경우
// @param      id                 프로그래스용 DIV의 ID 이름(생략하면 "_progress")
// @method     oj.prog_start()    프로그래스 시작
// @method     oj.prog_stop()     프로그래스 정지
// @property   oj.div             바를 출력하는 DIV 오브젝트
// @property   oj.div.style       스타일 오브젝트(CSS를 이용할 수 있습니다)
// @property   oj.prog_bar        바의 캐릭터(기본은 '|')
// @property   oj.prog_interval   프로그래스 인터벌(기본은 50 1/1000초 단위) 
// @property   oj.prog_count_max  프로그래스 바 카운터 최대값(기본은 18)
// @return     프로그래스 바 오브젝트의 인스턴스
  // @see        http://jsgt.org/ajax/
  // 

  if(document.getElementsByTagName('BODY').length==0)document.write('<body>')//더미 body 태그

  function jsgt_progressBar(id)
  {
    this.progress =  setProgressBars(id) ;
    function setProgressBars(id)
    {
    // 프로그래스 바를 출력할 div
      if(!id){
        id = "_progress";
        var creprgDIV = document.createElement("DIV") ;
        this.div = document.body.appendChild(creprgDIV)       ;
        this.div.setAttribute("id",id) ;
  
        this.div.style.position ="absolute";
        this.div.style.top ="0px";
        this.div.style.left ="0px";
        this.div.style.width ="0px";
        this.div.style.height ="0px";

      } else {
        this.div = document.getElementById(id)
      }

    // 프로그래스 바용 DIV의 기본값(인스턴스로 재정의 가능합니다)
    this.div.style.color = 'red'  ; //바의 색
    this.div.style.margin = '0px' ; //바의 마진
    this.div.style.padding = '4px'; //바의 패딩

    // 프로그래스 바의 기본값
    this.div.prog_bar= '|';             //바의 문자
    this.div.prog_interval= 50;         //프로그래스 인터벌 1/1000초 단위
    this.div.prog_count =0;             //프로그래스 카운터 초기화
    this.div.prog_count_max =18;        //프로그래스 바 카운터 최대값

    this.div.prog_array= [];             //바의 타이머 ID를 담은 배열

  //프로그래스 시작
      this.prog_start = function ()
      {
    //사이즈를 받아서 표시한다
        this.div.style.height ="12px";
        this.div.style.width ="auto";
        prog_array.unshift(
          setInterval(
            //프로그래스바 출력
            function ()
            {
              if(this.prog_count >= this.prog_count_max){
                this.div.innerHTML = '' ; //초기화
                this.prog_count =0;
              }
              this.div.innerHTML += this.prog_bar ;
              this.prog_count++ ;
            }
            , this.prog_interval 
          )
        )
      }
  
  //프로그래스 정지
      this.prog_stop = function ()
      {
        clearInterval(prog_array[0])
    //정지한 타이머를 삭제
        prog_array.shift()
    //삭제
        this.div.style.width ="0px";
        this.div.style.height ="0px";
        this.div.innerHTML  = '' ;
      }
  
      return this
    }
  
  }

