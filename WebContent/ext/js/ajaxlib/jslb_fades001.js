// @license    저작권 표시의무 없음. 상업 이용과 개조는 자유. 연락 필요 없음.

////
//BG 페이드
//@parem id BG 페이드시키려는 DIV의 ID 이름
//@sample oj = new fadeBgclGry('contents')
//@requires             jslb_ajax038.js        간이 Ajax 라이브러리
//@requires             jslb_progressbar001.js 프로그래스 바 라이브러리
//@requires             jslb_cacheajax001.js   Ajax 캐쉬 라이브러리
function fadeBgclGry(id) {
  
  var div    = "document.getElementById('"+id+"')";
  var count  = 0;
  var tid    = 0;

  //@sample oj.doBgFade('3456789abcde')
  this.doBgFade   = function (f){
    count  = 0;
    clearInterval(tid);
    tid = setInterval( function(){fade(f)} ,30);
  }
   
  function fade(f){ 
    if(!f)var f = "3456789abcdef";
    if(count < f.length){ 
      var b = f.charAt(count);
      setBgcolor( '#'+b+b+b+b+b+b );
      count++;
    } else {
      clearInterval(tid);
    }
  } 
  
  function setBgcolor(color){
    if(document.getElementById)         //e5,e6,n6,n7,m1,o6,o7,s1용
      eval(div).style.backgroundColor =color
  }
}




////
// 불투명 페이드 함수
//
// @sample          fadeOpacity('drgbbs',1,0.7)
//
//   
function fadeOpacity(layName,swt,stopOpacity){
  
  if(!window.fadeOpacity[layName]) //카운터 초기화
    fadeOpacity[layName] =0 

  //페이드 스위치 인수 생략시 초기치(불투명부터 투명으로)
  if(!arguments[1]) swt = -1
  
  //인수 swt가 -1이면 불투명부터 투명으로
  //           1이면 투명부터 불투명으로 페이드한다
  if(swt==-1)        var f  = "9876543210"
  else if(swt==1)    var f  = "0123456789"
  else               var f  = "9876543210"
  
  //정지 불투명도 인수 생략시 초기치
  if(!arguments[2] && swt==-1)   stopOpacity = 0
  else if(!arguments[2] && swt==1) stopOpacity = 10

  //페이드 처리  
  if( fadeOpacity[layName] < f.length-1 ){
  
    //카운터 순서의 문자열을 추출
    var opa = f.charAt(fadeOpacity[layName])/10

    //종료시 불투명도라면 종료
    if( opa == stopOpacity ){
      setOpacity(layName,stopOpacity)  //종료
      fadeOpacity[layName] = 0     //리셋
      return
    }
    // 불투명도 변경을 실행한다
    setOpacity(layName,opa)
    // 카운터를 가산
    fadeOpacity[layName]++
    //--50/1000초 후에 fadeOpacity을 재실행
    setTimeout('fadeOpacity("'+layName+'","'+swt+'","'+stopOpacity+'")',50)
  } else {
    //종료
    setOpacity(layName,stopOpacity)
    //--리셋
    fadeOpacity[layName] = 0

  }

}
function setOpacity(layName,arg) {
  if(window.opera)return
  var ua = navigator.userAgent
  if(ua.indexOf('Safari') !=-1 || ua.indexOf('KHTML') !=-1 ) { 
      document.getElementById(layName).style.opacity = arg
  } else if(document.all) {          //win-e4,win-e5,win-e6
      document.all(layName).style.filter="alpha(opacity=0)"
      document.all(layName).filters.alpha.Opacity  = (arg * 100)
  } else if(ua.indexOf('Gecko')!=-1) //n6,n7,m1
      document.getElementById(layName).style.MozOpacity = arg
}