//==============================================================================
//  SYSTEM      :  잠정판 크로스 프라우저 Ajax용 라이브러리
//  PROGRAM     :  XMLHttpRequest에 의한 송수신을 합니다
//                 open()이 아니고 요청 헤더에 Auth를 하기 위한 별도의 라이브러리.
//                 고급 JavaScript 라이브러리 모음에서 base64 인코딩을 이용하고 있습니다.
//                 http://www.onicos.com/staff/iz/amuse/javascript/expert/
//  FILE NAME   :  ajasql_ajaxXXX_auth2.js
//  CALL FROM   :  Ajax 클라이언트
//  AUTHER      :  Toshirou Takahashi http://jsgt.org/mt/01/
//  SUPPORT URL :  http://jsgt.org/mt/archives/01/000409.html
//  CREATE      :  2005.6.26
//  TEST-URL    :  헤더 http://jsgt.org/ajax/ref/lib/test_head.htm
//  TEST-URL    :  인증   http://jsgt.org/mt/archives/01/000428.html
//  TEST-URL    :  비동기 
//        http://allabout.co.jp/career/javascript/closeup/CU20050615A/index.htm
//  TEST-URL    :  SQL     http://jsgt.org/mt/archives/01/000392.html
//------------------------------------------------------------------------------
// 최신 정보 http://jsgt.org/mt/archives/01/000409.html 
// 저작권 표시의무 없음. 상업 이용과 개조는 자유. 연락 필요 없음.
// 아래에 추가해 주세요.
//
// 

	////
	// XMLHttpRequest 오브젝트 생성
	//
	// @sample           oj = createHttpRequest()
	// @return        XMLHttpRequest 오브젝트(인스턴스)
	//
	function createHttpRequest()
	{
		if(window.ActiveXObject){
			 //Win e4,e5,e6용
			try {
				return new ActiveXObject("Msxml2.XMLHTTP") ;
			} catch (e) {
				try {
					return new ActiveXObject("Microsoft.XMLHTTP") ;
				} catch (e2) {
					return null ;
	 			}
	 		}
		} else if(window.XMLHttpRequest){
			 //Win Mac Linux m1,f1,o8 Mac s1 Linux k3용
			return new XMLHttpRequest() ;
		} else {
			return null ;
		}
	}
	
	////
	// 송신함수
	//
	// @sample           sendRequest_auth2(onloaded,'&prog=1','POST','./about2.php',true,true)
	// @param callback 송수신시에 기동하는 함수 이름
	// @param data	   송신하는 데이터 (&이름1=값1&이름2=값2...)
	// @param method   "POST" 또는 "GET"
	// @param url      요청하는 파일의 URL
	// @param async	   비동기라면 true 동기라면 false
	// @param sload	   수퍼 로드 true로 강제、생략또는 false는 기본
	// @param user	   인증 페이지용 사용자 이름
	// @param password 인증 페이지용 암호
	//
	function sendRequest_auth2(callback,data,method,url,async,sload,user,password)
	{
		//XMLHttpRequest 오브젝트 생성
		var oj = createHttpRequest();
		if( oj == null ) return null;
		
		//강제 로드의 설정
		var sload = (!!sendRequest_auth2.arguments[5])?sload:false;
		if(sload)url=url+"?t="+(new Date()).getTime();
		
		//브라우저 판정
		var ua = navigator.userAgent;
		var safari	= ua.indexOf("Safari")!=-1;
		var konqueror = ua.indexOf("Konqueror")!=-1;
		var mozes	 = ((a=navigator.userAgent.split("Gecko/")[1] )
				?a.split(" ")[0]:0) >= 20011128 ;
		
		//송신 처리
		//opera는 onreadystatechange에 중복 응답이 있을 수 있어 onload가 안전
		//Moz,FireFox는 oj.readyState==3에서도 수신하므로 보통은 onload가 안전
		//Win ie에서는 onload가 동작하지 않는다
		//Konqueror은 onload가 불안정
		//참고 http://jsgt.org/ajax/ref/test/response/responsetext/try1.php
		if(window.opera || safari || mozes){
			oj.onload = function () { callback(oj); }
		} else {
		
			oj.onreadystatechange =function () 
			{
				if ( oj.readyState == 4 ){
					callback(oj);
				}
			}
		}

		//URL 인코딩
		if(method == 'GET') {
			if(data!=""){
				//&과=로 일단 구분해서 encode
				var encdata = '';
				var datas = data.split('&');
				//
				for(i=0;i<datas.length;i++)
				{
					var dataq = datas[i].split('=');
					encdata += '&'+encodeURIComponent(dataq[0])+'='+encodeURIComponent(dataq[1]);
				}
				url=url + encdata;
			}
		}
		
		//open 메소드
		oj.open(method,url,async);


		//base64 Basic 인증
		var b64 = base64encode(user+':'+password)
		oj.setRequestHeader('Authorization','Basic '+b64);
		
		
		//헤더 설정
		if(method == 'POST') {
		
			//이 메소드가 Win Opera8.0에서 에러가 나므로 별도 처리(8.01은 OK)
			if(!window.opera){
				oj.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
			} else {
				if((typeof oj.setRequestHeader) == 'function')
					oj.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
			}
		} 

		//디버그
		//alert("////jslb_ajaxxx.js//// \n data:"+data+" \n method:"+method+" \n url:"+url+" \n async:"+async);
		
		//send 메소드
		oj.send(data);
		

	}

