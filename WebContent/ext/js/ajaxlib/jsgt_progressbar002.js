//--jslb----------------------------------------------------------------------------
// 최신정보   : http://jsgt.org/mt/01/
// Public Domain 저작권 표시 의무 없음. 상용이용, 개조, 자유. 연락 불요.
 
////
// jsgt_ProgressBar 프로그래시브 바 오브젝트 
//
// @author     Toshiro Takahashi 
// @support    http://jsgt.org/mt/archives/01/000743.html
// @version    0.02 
// @license    Public Domain 저작권 표시 의무 없음. 상용이용, 개조, 자유. 연락 불요.
// @sample     oj = new jsgt_ProgressBar()           //DIV를 자동 생성하는 경우
// @sample     oj = new jsgt_ProgressBar('nloading') //기존의 DIV 이름으로 지정하는 경우
// @param      id                 프로그래스용 DIV의 ID 이름(생략하면 "_progress"+(new Date()).getTime())
// @method     oj.prog_start()    프로그래스 시작
// @method     oj.prog_stop()     프로그래스 정지
// @property   oj.div             바를 출력하는 DIV 오브젝트
// @property   oj.div.style       스타일 오브젝트(CSS를 이용할 수 있습니다)
// @property   oj.prog_bar        바의 캐릭터(기본은 '|')
// @property   oj.prog_interval   프로그래스 인터벌(기본은 50 1/1000초 단위) 
// @property   oj.prog_count      프로그래스 바 카운터
// @property   oj.prog_count_max  프로그래스 바 카운터 최대값(기본은 18)
// @return     프로그래스 바 오브젝트의 인스턴스
// 

function jsgt_ProgressBar(id)
{

  this.div        = setProgressDIV(id)
  this.prog_start = prog_start
  this.prog_stop  = prog_stop
  var oj = this.div

  function setProgressDIV(id)
  {

    // 프로그래스 바를 출력할 div
    if(!id){
      id = "_progress"+(new Date()).getTime();//id를 생성;
      if(document.getElementsByTagName('BODY').length==0)
        document.write('<body>')// 더미 body 태그
      var creprgDIV = document.createElement("DIV") ;
      this.div = document.body.appendChild(creprgDIV) ;
      this.div.setAttribute("id",id) ;

      this.div.style.position = "relative";
      this.div.style.top      = "0px";
      this.div.style.left     = "0px";
      this.div.style.width    = "0px";
      this.div.style.height   = "0px";

    } else {
      this.div = document.getElementById(id)
    }

    // 프로그래스 바용 DIV의 기본값(인스턴스로 재정의 가능합니다)
    this.div.style.color = 'red'  ; //바의 색
    this.div.style.margin = '0px' ; //바의 마진
    this.div.style.padding = '4px'; //바의 패딩

    // 프로그래스 바의 기본값
    this.div.prog_bar= '|';             //바의 문자
    this.div.prog_interval= 50;         //프로그래스 인터벌 1/1000초 단위
    this.div.prog_count =0;             //프로그래스 카운터 초기화
    this.div.prog_count_max =18;        //프로그래스 바 카운터 최대값

    this.div.prog_array= [];             //바의 타이머 ID를 담은 배열
    return this.div
  }

  //프로그래스 시작
  function prog_start()
  {
    //사이즈를 받아서 표시한다
    this.div.style.height ="12px";
    this.div.style.width ="auto";

    this.div.prog_array.unshift(
      setInterval(
        function(){ doProguress() }
        , this.div.prog_interval 
      )
    )
  }

  //프로그래스 정지
  function prog_stop()
  {
    clearInterval(this.div.prog_array[0])
    //정지한 타이머를 삭제
    this.div.prog_array.shift()
    //삭제
    this.div.style.width ="0px";
    this.div.style.height ="0px";
    this.div.innerHTML  = '' ;
  }

  //프로그래스 바의 작동
  function doProguress()
  {
    //window.status=oj.id  //체크

    if(oj.prog_count >= oj.prog_count_max||
       oj.prog_count <= 0 ){
            oj.innerHTML = '' ; //초기화
            oj.prog_count =0;
    }
    oj.innerHTML += oj.prog_bar ;
    oj.prog_count++ ;
  }

  return this
}

