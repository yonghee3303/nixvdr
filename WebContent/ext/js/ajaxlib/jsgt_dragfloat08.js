//=====================================================================
//  SYSTEM      :  DragableFloat DIV
//  PROGRAM     : 드랙가능하며, 스크롤 시에는 화면의 고정위치에 정지하는 DIV
//  FILE NAME   :  jsgt_dragfloat.js
//  CALL FROM   :  HTML
//  AUTHER      :  Toshirou Takahashi http://jsgt.org/mt/01/
//  SUPPORT URL :  http://jsgt.org/mt/archives/01/000419.html
//  CREATE      :  2005.7.8
//  UPDATE      :  2005.11.20 v08 선택을 할 수 없게 되는 문제가 있어 document.onmousemove의 return false을 코멘트 아웃
//  UPDATE      :  2005.9.26 v07 floatEnabled boundEnabled setBounds chkBounds追加
//  UPDATE      :  2005.9.22 v06 getTOP getLEFT getMouseX getMouseY를 div의 프로퍼티화
//  UPDATE      :  2005.8.12 v05드랙 시에 ie에서 발생하는 select를 onselectstart로 취소
//  UPDATE      :  2005.8.10 v04드랙 시에 iframe 위의 이벤트를 얻을 수 없음을 다소 수정했습니다
//  UPDATE      :  2005.8.8  dbg_echo()추가
//  UPDATE      :  2005.8.8  body태그가 없을 때를 위해 더미 body출력을 추가　
//  UPDATE      :  2005.7.8 DOCTYPE 표준 모드에 대응
//                   참고 http://otd8.jbbs.livedoor.jp/877597/bbs_tree?base=9322&range=1
//
// 
// 이 소스는 개조도 상업 이용도 자유입니다만,
// 그 자유를 지키기 위해 저작원은 버리지 않습니다.
//---------------------------------------------------------------------
// 高橋登史朗 (Toshirou Takahashi http://jsgt.org/mt/01/) 2005.7.8

/*
//=====================================================================
// 아래 예제

<-- 라이브러리  jsgt_dragfloat.js-->
<script type    = 'text/javascript'
        charset = 'UTF-8'
        src     = 'jsgt_dragfloat.js'></script>
<script type='text/javascript'>
<!--

////
// 동작 개시
//
// 페이지 읽어 들임 시에 동작 개시
//
window.onload = function ()
{
    setDragableFloat() //설정
}

////
// 설정
//
// @syntax oj = dragableFloat("DIV의 ID 이름",초기위치X,초기위치Y)
//
// @sample              div1 = dragableFloat("aaa",100,200) //생성
// @sample              div1.innerHTML="가나다라"         //HTML을 삽입
// @sample              div1.style.backgroundColor='orange' //CSS로 수정
// @sample              doDragableFloat()                   //개시
//    
function setDragableFloat()
{
    //드랙가능한 플로트 DIV를 생성
    div1 = dragableFloat("aaa",100,200)
    div2 = dragableFloat("fff",200,300)
    div3 = dragableFloat("ddd",250,300)
    
    //각 DIV도 HTML을 삽입
    div1.innerHTML="aaaaaaaa"
    div2.innerHTML="fffffff"
    div3.innerHTML="xx"

    //CSS로 수식
    div1.style.backgroundColor ="orange"
    div2.style.fontSize  ="18px"

    //개시
    doDragableFloat()
    
}

//-->
</script>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>



// 예제는 여기까지
//=====================================================================
 */
 





////
// 전역 변수
//
// @var    zcount             모든 드랙 가능한 DIV 중에 현재 zindex 최전면
// @var    clickElement       현재 드랙 중의 DIV의 ID 이름
// @var    canvas             document.body의 DOCTYPE 표준 모드 대응
// @array  dragableFloatId    전 드랙 가능한 DIV의 ID 이름을 할당
//
var zcount = 0          ;
var clickElement = ""   ;
if(document.getElementsByTagName('BODY').length==0)document.write('<body>')//더미 body 태그
var canvas = document[ 'CSS1Compat' == document.compatMode ? 'documentElement' : 'body'];
var dragableFloatId=[]  ;
var recx1,recy1,recx2,recy2,recxOffset,recyOffset


////
// 설정된 모드 드랙 가능한 DIV를 개시
//
//
function doDragableFloat()
{ 

  for(i in dragableFloatId){ 
    var oj = document.getElementById(dragableFloatId[i]) ;
    if(oj.floatEnabled){

      if(!(is.safari || is.khtml))
      {
        //스크롤 시의 동작
        window.onscroll = function(e){
            moveDiv(oj,oj.style.left,oj.style.top);
        }
      } else {
        aaa=setInterval(function(){
          moveDiv(oj,oj.style.left,oj.style.top);
        },100)
      }
    }
  }
}

//모든 드랙 가능한 DIV의 플로트를 시작
function startDragableFloat()
{
    for(i in dragableFloat ){
        var oj = document.getElementById(dragableFloat[i].id) ;
        moveDiv(oj,oj.style.left,oj.style.top);
    }
}

//DIV를 가라앉힌다    
function moveDiv (oj,ofx,ofy)
{
    if(oj.draging)return  ;//드랙 중은 무시
    if(oj.dragcnt == 0 ){ 
        ofx = parseInt(ofx,10) 
        ofy = parseInt(ofx,10) 
        oj.dragcnt++
    } else {//드랙 종료 위치가 오프셋
        ofx = parseInt(oj.pageOffLeft,10) 
        ofy = parseInt(oj.pageOffTop,10) 
    }
    var l = parseInt(canvas.scrollLeft,10) 
    var t = parseInt(canvas.scrollTop,10) 
    oj.style.left = l + ofx+"px"
    oj.style.top  = t + ofy+"px"
}


////
//브라우저 판정
//
// @sample               alert(is.ie)
//
var is = 
{
    ie     : !!document.all ,
    mac45  : navigator.userAgent.indexOf('MSIE 4.5; Mac_PowerPC') != -1 ,
    opera  : !!window.opera ,
    safari : navigator.userAgent.indexOf('Safari') != -1 ,
    khtml  : navigator.userAgent.indexOf('Konqueror') != -1 
}

////
// 드랙 가능한 플로트 DIV 생성
//
// @sample          div1 = dragableFloat("aaa",100,200)
//

function dragableFloat(id,x,y)
{
    if(!!dragableFloatId[id]) return document.getElementById(id)
    
    ////
    // DIV 생성
    // @param  id             DIV의 ID 이름
    //
    this.mkDiv = function (id) 
    {
        var canvas = document[ 'CSS1Compat' == document.compatMode ? 'documentElement' : 'body'];
        var doc   = document                           ; // document 오브젝트
        var body  = doc.body                           ;
        var elem  = doc.createElement("DIV")           ; //DIV 요소를 생성
        var div   = body.appendChild(elem);
            div.setAttribute("id",id)                   ;
            div.style.position = "absolute"           ;
            div.style.left     = x + "px"             ;
            div.style.top      = y + "px"             ;
            div.innerHTML      = ""                   ;
            div.offLeft        = 0                    ;
            div.offTop         = 0                    ;
            div.pageOffLeft    = x-parseInt(canvas.scrollLeft,10)+ "px";
            div.pageOffTop     = y-parseInt(canvas.scrollTop,10) + "px";
            div.dragcnt        = 0                    ;
            div.draging        = false                ;
            div.getTOP         = getTOP               ;
            div.getLEFT        = getLEFT              ;
            div.getMouseX      = getMouseX            ;
            div.getMouseY      = getMouseY            ;
            recx1              = x
            recy1              = y
            div.floatEnabled   = true                 ; //플로트 가능 true|false
            div.boundEnabled   = false                ; //이동 가능 영역 있음 true|false
            div.setBounds      = function (a,b,c,d){
                div.minX=a;div.minY=b;div.maxX=c;div.maxY=d;
                div.boundEnabled = true;
            }
            div.onmouseout     = function (e){ 

                if(!clickElement) return
                selLay=document.getElementById(clickElement);

                //xy에러 시의 유추 추적용 xy세트
                x =  recx2+=recxOffset  
                y =  recy2+=recyOffset  
                dofollow(x,y)
                x =  recx2+=recxOffset  
                y =  recy2+=recyOffset  
                setTimeout('"dofollow('+x+','+y+')"',10)

                //follow(e)
                //dbg.innerHTML += getMouseX(e)+"--"+getMouseY(e)+"<br>"
                div.style.zIndex = zcount++
                return false 
            }
            div.onselectstart  = function (e){ return false }
            div.onmouseover    = function (e){ return false }
            div.onmousedown    = function (e)
            {
                div.draging    = true  ;
                div.dragcnt ++         ;
                selLay=div
                clickElement = selLay.id

                //DIV의 left,top으로부터 포커스 위치까지 오프셋을 갈무리
                if (selLay){    
                    selLay.offLeft = getMouseX(e) - getLEFT(selLay.id)
                    selLay.offTop  = getMouseY(e) - getTOP(selLay.id)
                
                } 
                return false
            }

        dragableFloatId[div.id] = div.id;//window로 등록
        div.index++;
        return div;
    }

    //마우스 이동 시의 동작
    document.onmousemove  = function (e)
    {
        recTimeOffset(e) //rec
        follow(e)
        //return false
    }
    
    //마우스 업(up) 시의 동작
    document.onmouseup  = function (e)
    {
        if(!clickElement) return
        selLay=document.getElementById(clickElement);
        
        //드랙 중임에도 벗어나버린 경우
        follow(e)
        
        //드랙 중지
        selLay.draging   = false ;
        selLay.style.zIndex = zcount++

        //화면 안의 오프셋 left,top 위치를 갈무리
        if (selLay){
            var sl = parseInt(canvas.scrollLeft,10)
            var st = parseInt(canvas.scrollTop,10)
            selLay.pageOffLeft = getLEFT(selLay.id)-sl
            selLay.pageOffTop  = getTOP(selLay.id)-st
        }
        return false
    }

    //드랙 실패 시의 유추 추적
    function follow(e)
    {
        if(!clickElement) return
        selLay=document.getElementById(clickElement);

        //마우스 위치 취득
        var x = getMouseX(e)
        var y = getMouseY(e)

        //xy에러 시의 유추 추적용 xy 세트
        x = (x == -1)? recx2+=recxOffset : x ;
        y = (y == -1)? recy2+=recyOffset : y ;
        if(x == -1 && y == -1)setTimeout('follow('+e+')',100)

        dofollow(x,y)
    }

    //마우스 추적
    function dofollow(x,y)
    {
        if(!clickElement) return
        selLay=document.getElementById(clickElement);
        if(!chkBounds(selLay)){
          return
        } else {
          if(selLay.draging){
            //오프셋을 끌어와 따른다
            movetoX = x - selLay.offLeft
            movetoY = y - selLay.offTop
            selLay.style.left = parseInt(movetoX,10) +"px"
            selLay.style.top  = parseInt(movetoY,10) +"px"
          }
        }
       // window.status = selLay.style.left
    }

    //마우스 위치를 기록
    function recTimeOffset(e)
    {
        if(x == -1 || y == -1)return 
        recx2= recx1
        recy2= recy1
        recx1= getMouseX(e)
        recy1= getMouseY(e)
        recxOffset= recx1 - recx2
        recyOffset= recy1 - recy2
        
    }
    
    //지정 영역 안이 어떤지를 체크
    function chkBounds(oj){

      var layName = oj.id
      if(oj.boundEnabled){
      
        //현재 위치 취득
        var nowX = getLEFT(layName);
        var nowY = getTOP(layName);
        //체크
        if( 
          nowX >= oj.minX &&
          nowY >= oj.minY &&
          nowX <= oj.maxX &&
          nowY <= oj.maxY
        ){
          return true //지정영역 안이면 true
        } else {
          returnPOS(nowX,nowY,oj)
          return false
        }
      } else {
        return true
      }
    }

    //영역 안으로 돌아간다
    function returnPOS(nowX,nowY,oj){
      if(nowX < oj.minX) oj.style.left = oj.minX +"px"
      if(nowY < oj.minY) oj.style.top  = oj.minY +"px"
      if(nowX > oj.maxX) oj.style.left = oj.maxX +"px"
      if(nowY > oj.maxY) oj.style.top  = oj.maxY +"px"
    }

    //마우스 X좌표 취득 
    function getMouseX(e)
    {
        if(document.all)               //e4,e5,e6용
            return canvas.scrollLeft+event.clientX
        else if(document.getElementById)    //n6,n7,m1,o7,s1용
            return e.pageX
    }

    //마우스 Y좌표 취득 
    function getMouseY(e)
    {
        if(document.all)               //e4,e5,e6용
            return canvas.scrollTop+event.clientY
        else if(document.getElementById)    //n6,n7,m1,o7,s1용
            return e.pageY
    }


    //레이어 좌변 X좌표 취득 
    function getLEFT(layName){
        //デバック
        //document.getElementById('aaa').innerHTML+=layName+'<BR>'
        
        if(document.all)                    //e4,e5,e6,o6,o7용
            return document.all(layName).style.pixelLeft
        else if(document.getElementById)    //n6,n7,m1,s1용
            return (document.getElementById(layName).style.left!="")
                ?parseInt(document.getElementById(layName).style.left):""
    }

    //레이어 상변 Y좌표 취득 
    function getTOP(layName){
        if(document.all)                    //e4,e5,e6,o6,o7용
            return document.all(layName).style.pixelTop
        else if(document.getElementById)    //n6,n7,m1,s1용
            return (document.getElementById(layName).style.top!="")
                    ?parseInt(document.getElementById(layName).style.top):""
    }

    //디버그
    function dbg_echo(){
            ////////dbg.innerHTML += selLay.draging+"<br>"
            
        var debugDIV  = document.createElement("DIV")  ; //DIV 요소를 생성
        var dbg   = document.body.appendChild(debugDIV);
            dbg.setAttribute("id","dbg")                   ;
            dbg.style.position = "absolute"           ;
            dbg.style.left     =  "400px"             ;
            dbg.style.top      = "0px"             ;
            dbg.innerHTML      = "dbg"                   ;
            return dbg;
    }  //dbg = dbg_echo()


	function db1(e)
	{
		dbg.innerHTML += getMouseX(e)+"-1000-"+getMouseY(e)+"<br>"
	}
	
	
	return this.mkDiv(id) ;
		
}
		