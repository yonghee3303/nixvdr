//--jsGadget--------------------------------------------------------------------------
// 최신정보   : http://jsgt.org/mt/01/
// Public Domain 저작권 표시 의무 없음. 상용이용, 개조, 자유. 연락 불요.

////
// jsgt_Indicator 인디케이터 오브젝트 
// 
// @author     Toshiro Takahashi 
// @support    http://jsgt.org/mt/archives/01/000747.html
// @download   http://jsgt.org/lib/indicator/
// @version    0.02 jsgt_indicator001.js
// @license    Public Domain 저작권 표시 의무 없음. 상용이용, 개조, 자유. 연락 불요.
// @syntax     oj = new jsgt_Indicator(src[,id])
// @sample     oj = new jsgt_Indicator('img.gif')             //DIV를 자동 생성하는 경우
// @sample     oj = new jsgt_Indicator('img.gif','nloading')  //기존의 DIV 이름으로 지정하는 경우
// @param      id                 인디케이터용 DIV의 ID 이름(생략하면 "_indicator"+(new Date()).getTime())
// @method     oj.indi_start()    인디케이터 시작
// @method     oj.indi_stop()     인디케이터 정지
// @property   oj.div             바를 출력하는 DIV 오브젝트
// @property   oj.div.style       스타일 오브젝트(CSS를 이용할 수 있습니다)
// @return     인디케이터 오브젝트의 인스턴스
//
// @Thanx      AJAX Activity indicators http://mentalized.net/activity-indicators/ 감사합니다. 
// 

function jsgt_Indicator(src,id)
{

  this.div        = setIndicatorDIV(src,id)
  this.indi_start = indi_start
  this.indi_stop  = indi_stop

  function setIndicatorDIV(src,id)
  {

    // 인디케이터를 출력하는 div
    if(!id){
      id = "_indicator"+(new Date()).getTime();//id를 생성;
      if(document.getElementsByTagName('BODY').length==0)
        document.write('<body>')//더미 body 태그
      var creDIV = document.createElement("DIV") ;
      this.div = document.body.appendChild(creDIV) ;
      this.div.setAttribute("id",id) ;

      this.div.style.position = "relative";
      this.div.style.top      = "0px";
      this.div.style.left     = "0px";
      this.div.style.width    = "0px";
      this.div.style.height   = "0px";

    } else {
      this.div = document.getElementById(id)
    }

    // 인디케이터용 DIV의 기본 값(인스턴스로 갱신할 수 있습니다)
    this.div.style.margin  = '0px' ; //바의 마진
    this.div.style.padding = '0px' ; //바의 패딩

    //인디케이터의 그림을 프리로드
    this.div.img = new Image()
    this.div.img.src = src

    //인디케이터의 기본 값
    this.div.indi_bar= '|';             //바의 문자
    this.div.indi_interval= 50;         //인디케이터 인터벌 1/1000초 단위
    this.div.indi_count =0;             //인디케이터의 카운터를 초기화
    this.div.indi_count_max =18;        //인디케이터 카운터의 최대값

    this.div.indi_array= [];             //바ㅢ 타이머 ID를 담은 배열
    return this.div
  }

  //인디케이터 시작
  function indi_start()
  {
    //사이즈를 받아서 표시
    this.div.style.height ="12px";
    this.div.style.width ="auto";
    this.div.innerHTML  = '<img src="'+this.div.img.src+'">' ;
  }

  //인디케이터 정지
  function indi_stop()
  {
    this.div.style.width ="0px";
    this.div.style.height ="0px";
    this.div.innerHTML  = '' ;
  }

  return this
}

