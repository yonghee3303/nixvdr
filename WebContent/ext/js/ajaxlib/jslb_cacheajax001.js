//--jslb----------------------------------------------------------------------------
// 최신 정보   : http://jsgt.org/mt/01/
// 저작권 표시의무 없음. 상업 이용과 개조는 자유. 연락 필요 없음.


////
//Ajax 캐쉬 라이브러리
//  한번 읽어 들인 파일 데이터를 배열로 넣어, 다시 읽어들이지 않고 이용합니다.
//  Ajax에 의한 로딩은, 캐쉬를 무시하는 강제 로딩 설정을 하고 있어서
//  브라우저에 의한 자동 캐쉬가 아닌 프로그램의 명시적인 제어가 가능합니다.
//
//@sample cacheAjaxOj = new cacheAjax('http://jsgt.org/ajax/newmon/tools/php/getfile.php','bodys');
//@param  url           HTTP 요청 주소
//@param  outputDivId   HTTP 응답의 출력 DIV id 이름
//@requires             jslb_ajax038.js 간이 Ajax 라이브러리 http://jsgt.org/mt/archives/01/000409.html
//@requires             jslb_progressbar001.js 프로그래스 바 라이브러리
//@see                  http://jsgt.org/ajax/
//
function cacheAjax(url,outputDivId){

  //
  //@sample cacheAjaxOj.getFile('chapter03.htm',callback)
  //@parem fileName 파일 이름
  //@parem callback 착신시 콜백 함수 이름
  this.getFile     = getFileSwt;

  this.cachenabled = true ;

  var pbarAry  = [];  //프로그래스 바용 배열
  var pageAry  = [];  //페이지 데이터용 배열

  //목자 프리로드
  function preload(fileName,callback){

    //프로그래스 바 인스턴스 생성
    pbarAry[fileName] = new jsgt_progressBar();
    //프로그래스 바 시작
    pbarAry[fileName].progress.prog_start();
    
    //요청 전송
    sendRequest(onloaded_pre,'&file='+fileName,'POST',url,true,true);
    //콜백 처리
    function onloaded_pre(oj){ 
      //수신
      pageAry[fileName] = decodeURIComponent(oj.responseText) ;
      //출력
      writePage(pageAry[fileName],callback);
      //프로그래스 바 정지
      pbarAry[fileName].progress.prog_stop();
    }

  }

  //파일 취득 스위치
  //
  function getFileSwt(fileName,callback){
    //로딩되었나 this.cachenabled==true가 아니라면 로딩
    if(!pageAry[fileName]||!this.cachenabled)preload(fileName,callback);
    else                  writePage(pageAry[fileName],callback);
  }
  
  //페이지 데이터 출력
  function writePage(pagedata,callback){
    if(!pagedata || !document.getElementById(outputDivId))return;
    document.getElementById(outputDivId).innerHTML='<pre>'+pagedata+'</pre>' ;
    if(callback)callback();
  }

}