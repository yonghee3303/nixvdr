/** 
 * Copyright (c) 2005 Genetics. All rights reserved. 
 * 
 * This software is the confidential and proprietary information of SK C&C. 
 * You shall not disclose such Confidential Information and shall use it 
 * only in accordance with the terms of the license agreement you entered into 
 * with SK C&C. 
 */

/**
 * 함수명		:chkDate(date)
 * 내용			:주어진 값이 날짜형식인지를 검사한다.
 * parameter	:날짜값
 * return		:boolean(true : 날짜형식입니다. false:날짜형식이 아닙니다.)
 */
function chkDate(date) {
	var year = date.substr(0,4);	//alert(year);
	var month = date.substr(4,2);	//alert(month);
	var day = date.substr(6,2);		//alert(day);

	if((date).length==8){
		if (year > 1900 && year < 10000){
			if (month > 0 && month < 13){
				if (day < (getLastDay(year,month)+1) && day > 0){
						return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}else {
			return false;
		}
	}else{
		return false;
	}
}

/**
 * 함수명		:getLastDay(year,month)
 * 내용			:주어진 년, 월 값을 입력받아 매월 말일자를 리턴한다.
 * parameter	:년, 월
 * return		:매월 말일자
 */
function getLastDay(year,month) {
	var days;
	if (month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
		days=31;
	else if (month==4 || month==6 || month==9 || month==11)
		days=30;
	else if (month==2)
		days=isLeapYear(year) ? 29 : 28;
	
	return days;
}

function getYearMonth(ymd, minus_val)
{
	var i_year = parseInt(ymd.substring(0,4));
	var i_month = parseInt(Zero_LTrim(ymd.substring(4,6)));
	var tmp_mon ="";
	
	if((i_month + minus_val) <= 0) {
		i_year = i_year - 1;
		i_month = 12 + (i_month + minus_val);
	} else {
		i_month = i_month + minus_val;
	}
	
	if( i_month < 10) {
		tmp_mon = "0" + i_month;
	} else {
		tmp_mon = "" + i_month;
	}
	return  tmp_mon;
}

/**
 * 함수명		: isLeapYear(year)
 * 내용			:주어진 년도가 윤년인지를 검사한다.
 * parameter	:년
 * return		:boolean
 * 4로 나누어지되 100으로 나뉘지 않으면, 100으로 나뉘어도 400으로 나뉘면 
 * 윤년임 (2월29일)
 */
function isLeapYear(year) {
	if (((year % 4)==0 && (year % 100)!=0) || (year % 400)==0)
		return true;
	else
		return false;
}

	
/**
 * 함수명		:daysBetween(date1, date2)
 * 내용			: 두 날짜 사이의 일수를 계산하여 반환한다.
 * parameter	: date1, date2 문자열 데이터로 '20041012' 형식
 * return		:boolean
 */
function daysBetween(date1, date2) {
    date1 = new Date(date1.substring(0, 4), date1.substring(4, 6)-1, date1.substring(6,8));
    date2 = new Date(date2.substring(0, 4), date2.substring(4, 6)-1, date2.substring(6,8));
    var DSTAdjust = 0;
    
    // constants used for our calculations below
    oneMinute = 1000 * 60;
    var oneDay = oneMinute * 60 * 24;
    
    // equalize times in case date objects have them
    date1.setHours(0);
    date1.setMinutes(0);
    date1.setSeconds(0);
    date2.setHours(0);
    date2.setMinutes(0);
    date2.setSeconds(0);

    DSTAdjust = (date2.getTimezoneOffset( ) - 
                 date1.getTimezoneOffset( )) * oneMinute;
 
    var diff = date2.getTime( ) - date1.getTime() - DSTAdjust;
    return Math.ceil(diff/oneDay);
}

function monthsBetween(date1, date2) {

//    fromDate = new Date(date1.substring(0, 4), date1.substring(4, 6)-1, date1.substring(6,8));
//    toDate   = new Date(date2.substring(0, 4), date2.substring(4, 6)-1, date2.substring(6,8));
 // 200808 200809 
    var fromYear = parseInt(date1.substring(0, 4), 10);
    var toYear = parseInt(date2.substring(0, 4), 10);
    var fromMonth = parseInt(date1.substring(4, 6), 10);
    var toMonth = parseInt(date2.substring(4, 6), 10);
    var result =0;
    if(date2.length ==6){
        result += ((toYear - fromYear) * 12);
    	result += (toMonth - fromMonth);
    	return result;
    }
    
    var fromDay = parseInt(date1.substring(6,8), 10);
    var toDay = parseInt(date2.substring(6,8), 10);

    result += ((toYear - fromYear) * 12);
    result += (toMonth - fromMonth);

    if ((toDay - fromDay) > 0) result += 1;

    return result;
}

function getDateCalMonth(m)
{	
     var d = new Date();
     var mon = (d.getMonth() + 1 + m).toString();
     var day = d.getDate().toString(); 
                
     if ( mon.length == 1 ) {
      mon = "0" + mon;
     }
          
     if ( day.length == 1 ) {
      day = "0" + day;
     }     
     return((d.getYear()).toString() + "-" + mon + "-" + day);
} 

function getCalDate(sDay)
{
     var d, s = "";
     var year = "", mon ="", day ="";
     var sMon = 0, sYear = 0;
     d = new Date();
     if( d.getDate() + sDay < 1 ) {
     	 sMon = -1;
         if ( d.getMonth() == 1) sDay = 28 + (d.getDate() + sDay); 
         else sDay = 30 + (d.getDate() + sDay); 
     }
     if( (d.getMonth() + 1 + sMon) < 1 ) {
         sYear = -1;
         sMon = + 11;
     }
     var lastDay = getLastDay((d.getYear() + sYear),(d.getMonth() + 1 + sMon) );
  
     if ( lastDay < (d.getDate() + sDay) ) { sDay = sDay - ((d.getDate() + sDay) - lastDay); }
     
     year = (d.getYear() + sYear).toString();
     mon = (d.getMonth() + 1 + sMon).toString();
     day = (d.getDate() + sDay).toString(); 

     if ( mon.length == 1 ) {
        mon = "0" + mon;
     }
          
     if ( day.length == 1 ) {
        day = "0" + day ;
     }  
          
     s += year;
     s += mon;
     s += day;
     
     return(s);
} 

function getAddDate(ymd, sDay)
{
     var d, s = "";
     var year = "", mon ="", day ="";
     var sMon = 0, sYear = 0;
     d = new Date(ymd);
     
     
     if( d.getDate() + sDay < 1 ) {
     	 sMon = -1;
         if ( d.getMonth() == 1) sDay = 28 + (d.getDate() + sDay); 
         else sDay = 30 + (d.getDate() + sDay); 
     }
     if( (d.getMonth() + 1 + sMon) < 1 ) {
         sYear = -1;
         sMon = + 11;
     }
     var lastDay = getLastDay((d.getYear() + sYear),(d.getMonth() + 1 + sMon) );
  
     if ( lastDay < (d.getDate() + sDay) ) { sDay = sDay - ((d.getDate() + sDay) - lastDay); }
     
     year = (d.getYear() + sYear).toString();
     mon = (d.getMonth() + 1 + sMon).toString();
     day = (d.getDate() + sDay).toString(); 

     if ( mon.length == 1 ) {
        mon = "0" + mon;
     }
          
     if ( day.length == 1 ) {
        day = "0" + day ;
     }  
          
     s += year;
     s += mon;
     s += day;
     
     return(day);
} 

function toDay()
{
     var days, d, s = "";
     var year ="", mon ="", day ="";
     d = new Date();
     mon = (d.getMonth() + 1).toString();
     day = (d.getDate()).toString(); 
     year = (d.getYear()).toString();
     
                
    /*          
	if (mon==1 || mon==3 || mon==5 || mon==7 || mon==8 || mon==10 || mon==12)
		days=31;
	else if (mon==4 || mon==6 || mon==9 || mon==11)
		days=30;
	else if (mon==2)
		days=isLeapYear(year) ? 29 : 28;

	if(day == days) {
		mon = (parseInt(mon) + 1).toString();
		day = "1";
		
		if(parseInt(mon) > 12){
			year = (parseInt(year) + 1).toString();
			mon = "1";
		}
	} else {
		day = (parseInt(day)).toString();
	}
	*/

	 day = (parseInt(day)).toString();
                
     if ( mon.length == 1 ) {
      mon = "0" + mon;
     }
          
     if ( day.length == 1 ) {
      day = "0" + day ;
     }     
          
     s += year + "-";
     s += mon + "-" ;
     s += day;
     return(s);
   } 

   function nextDate(date)
   {
     var days, s = "";
     var year ="", mon ="", day ="";
     year = eval(date.substring(0,4)).toString();
     mon = eval(date.substring(4,6)).toString();
     day = eval(date.substring(6,8)).toString();
                
     // alert(year + "," + date.substring(4,6) + "," + day);
                
	if (mon==1 || mon==3 || mon==5 || mon==7 || mon==8 || mon==10 || mon==12)
		days=31;
	else if (mon==4 || mon==6 || mon==9 || mon==11)
		days=30;
	else if (mon==2)
		days=isLeapYear(year) ? 29 : 28;

	if(day == days) {
		mon = (parseInt(mon) + 1).toString();
		day = "1";
		
		if(parseInt(mon) > 12){
			year = (parseInt(year) + 1).toString();
			mon = "1";
		}
	} else {
		day = (parseInt(day)).toString();
	}

                
     if ( mon.length == 1 ) {
      mon = "0" + mon;
     }
          
     if ( day.length == 1 ) {
      day = "0" + day ;
     }     
     
     s += year ;
     s += mon ;
     s += day;
     return(s);
  } 

	function dateFormat(date,seperator){
		if(seperator == ""){
			seperator = "-";
		}
		
		if (date == "") {
		  	return "";
		  } else if (date.length() == 1){
		 	return date = "0" + date ;
		  } else if (date.length() == 6){
		 	return date.substring(0,4) + seperator + date.substring(4,6);
		  } else if (date.length()== 8) {
		   	return date.substring(0,4) + seperator + date.substring(4,6)  + seperator + date.substring(6,8);
		  } else if (date.length()== 10) {
			   	return date.substring(2);
		  } else {
		   	return date;
		  }
    	return date;
	}
	
	
	
/**
* @desc		현재 날짜에서 주어진 term(일) 만큼을 계산한다.
* @param	sys_date - System Date, terms - term을 두고 싶은 일수.
* @return	date - from_date + to_date. ex)2001012020010125
*/
