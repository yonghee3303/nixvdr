<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%
   String formId = StringUtils.paramReplace(request.getParameter("formid"));
   String grid_obj = StringUtils.paramReplace(request.getParameter("gridid"));
 %>

<html>
<head>
<title>게시판등록</title>
	<link rel="stylesheet" href="/ext/css/common.css" type="text/css">
	<link rel="stylesheet" href="/ext/css/popup_style.css" type="text/css">
	<link rel="stylesheet" href="/ext/css/board.css" type="text/css">


<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>

<link rel="stylesheet" href="/jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="/jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#PUB_YMD" ).datepicker();
			$( "#imgPubDate").click(function() {$( "#PUB_YMD" ).focus();});
		});
	</script>

<script>

function init() 
{
	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>" + "&user_id=&board_cd=none";
	listframe.location.href='/mg/board/boardList.jsp?eventframe=listframe' + argu;
	
	argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&user_id=";
    boardframe.location.href='/ext/smartEditor/SEditor.html?' + argu;
}

function refresh(board_content) 
{	
    boardframe.setHtml(board_content);
}


function OkPopupDept(dept_cd, plant_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}

function getwindowHeight()
{
	return document.body.scrollHeight;
}

function getwindowWidth()
{
	return document.body.scrollWidth;
}

function doUserPop() {
	popupUser("게시자 선택", '', '', 440, 400);
}

function OkPopupUser(userId, userNm) {
	document.form.SEARCH_ID.value = userId;
	document.form.SEARCH_NM.value = userNm;	
}

</script>

</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" hright="100%">
<tr>
   	<td>
	    <%@ include file="/mg/common/milestone.jsp"%>
	  
        <table width="100%" class="board-search">
        <colgroup>
            <col width="12%" />
            <col width="26%" />
            <col width="12%" />
            <col width="24%" />	  
            <col width="80px" />
        </colgroup>
        <tr>
            <td class="tit">게시판구분</td>
            <td>			
				<jsp:include page="/mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0106"/>
					<jsp:param name="tagname" value="BOARD_CLS_CD"/>
				</jsp:include>
            </td>
        	<td class="tit">제목</td>
            <td>
				<input type="text" name="BTITLE"  size="20"  value="" class="text">
			</td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:listframe.doQuery();" class="btn"><span>검색</span></a>
                </div>
            </td>
        </tr>
        <tr>
        <td class="tit">게시일자</td>
            <td>						
				<input type="text" id="PUB_YMD" value="" size="7" class="text">
				<img id="imgPubDate" src="/ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
            </td> 
            <td class="tit">게시자</td>
            <td colspan="4">
				<input type="text" name="SEARCH_ID"  size="6"  value="" class="text" onchange="document.form.SEARCH_NM.value=''">
				<a href="javascript:doUserPop();"><img src="/ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
				<input type="text" name="SEARCH_NM"  size="10" value="" class="text">
			</td> 
        </tr>
		</table>
   </td>
</tr>

<tr>
	<td>
      <table width="100%" border="0">
        <tr>
            <td valign="top">
				<iframe name="listframe" width="100%" height="200px" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
        </tr>
        <tr>
           <td valign="top">
				<iframe name="boardframe" width="100%" height="400px" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
           </td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="boardframe=300"/>
</jsp:include>
</body>
</html>
