<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.util.StringUtils"%>
<% 
   String board_cd = StringUtils.paramReplace(request.getParameter("board_cd"));
   String board_no = StringUtils.paramReplace(request.getParameter("board_no"));
%>

<html>
<head>
<title>답변저장</title>
	<link rel="stylesheet" href="/ext/css/common.css" type="text/css">
	<link rel="stylesheet" href="/ext/css/popup_style.css" type="text/css">
	<link rel="stylesheet" href="/ext/css/board.css" type="text/css">

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

function init() 
{
    var SERVLETURL = "/mg/board.do?mod=getContentPop";
    var argu = "&BOARD_NO=<%=board_no %>";
    sendRequest(doAjaxGetBoard, argu, 'POST', SERVLETURL , false, false);
    
    boardframe.location.href='/ext/smartEditor/SEditor.html?';
    
    document.form.BOARD_CLS_CD.value = "<%=board_cd%>";
}

function doAjaxGetBoard(oj) {
	var bdata = oj.responseText;
	var pdata = bdata.split("~");

	document.getElementById("btitle").innerHTML = pdata[0];
	document.getElementById("REG_DT").innerHTML = pdata[1];
	boardframe.setHtml(pdata[3]);
}

function doSave() {
	var btitle = document.form.BTITLE.value;
	if(btitle == "") {
		alert("답변 제목을 입력해 주십시요");
		return;
	} 
	
	if (confirm("저장하시겠습니까?")) {	// 저장 하시겠습니까?
	    var SERVLETURL = "/mg/board.do?mod=boardSaveAnswer";
	    var boardhtml = replace(parent.boardframe.getHtml(),"&","♨");
	    var argu = "&P_BOARD_NO=<%=board_no %>";
	    	argu += "&BOARD_CLS_CD=<%=board_cd %>";
			argu += "&BTITLE=" + encodeURIComponent(btitle);
	    	argu += "&BCONTENT=" + encodeURIComponent(boardhtml);
	    sendRequest(doAjaxSaveAnswer, "", 'POST', SERVLETURL+argu, false, false);
	}
}

function doAjaxSaveAnswer(oj) {
	var bdata = oj.responseText;
	
	try {
	   opener.doQuery();
	   window.close();
	}catch(exception)	{
	   opener.init();
	   window.close();
	}
	
	
}


</script>

</head>
<body onload="init();">
<form name="form" method="post">
<input type="hidden" id="BOARD_CLS_CD" value="">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>질의 답변 저장</h1>
      <a href="javascript:window.close();"><img src="/ext/images/popup_btn_close.gif" alt="닫기" /></a></td>
  </tr>
</table>

<form>
<table width="100%" border="0" class="p_boardview_1 mgt-15" align="center">
	  <colgroup>
		  <col width="130px" />
		  <col width="420px" />
	  </colgroup>
    <tr>
      <th>질문제목</th>
      <td><div id="btitle"></div></td>
    </tr>
    <tr>
      <th>질문일/질문자</th>
      <td><div id="REG_DT"></div></td>
    </tr>
	<tr>
		<th>답변제목</th>
		<td> <input type="text" name="BTITLE"  size="40"  value="" class="text">
		</td>
	</tr>
</table>
</form>
  <table width="100%" border="0">
  	<tr>
  		<td>
 				<div class="btnarea">
	            <a href="javascript:doSave();" class="btn"><span>저장</span></a>
	        </div>
		</td>
	</tr>
    <tr>
       <td valign="top">
			<iframe name="boardframe" width="100%" height="300px" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
       </td>
    </tr>
  </table>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="boardframe=140"/>
</jsp:include>
</body>
</html>
