<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%

// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj";
IUser s_user = (IUser) session.getAttribute("j_user");
String board_cls_cd = StringUtils.paramReplace(request.getParameter("board_cd"));
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
%>

<html>
<head>
<title>게시판리스트</title>
<%@ include file="/ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="/ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="http://jquery.com/src/jquery-latest.pack.js"></script> 
<script type="text/javascript" src="/jquery/jquery.wresize.js"></script> 
<script type="text/javascript"> 
jQuery( function( $ )  
{ 
    function content_resize()  
    { 
        // parent.boardframe.viewResize(parent.getwindowWidth()-6, parent.getwindowHeight()-370);
    } 
 
    $( window ).wresize( content_resize ); 
    content_resize(); 
} );
</script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "/mg/board.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
	
	var board_no = GridObj.cells(rowId, GridObj.getColIndexById("BOARD_NO")).getValue();
    var SERVLETURL = G_SERVLETURL + "?mod=getContent";
    var argu = "&BOARD_NO=" + board_no;	

    sendRequest(doAjaxGetContent, argu, 'POST', SERVLETURL , false, false);
    
     if( header_name == "FILE_ICON" ) {
		popupAttachFile("첨부파일 등록", '', '', 440, 400,"boardList",board_no,"",<%=pm.getString("component.contextPath.root")%>);
	}
}

function doAjaxGetContent(oj) {
	parent.refresh(oj.responseText);
}



// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {

	var board_cd = parent.document.form.BOARD_CLS_CD.value;
	if(board_cd == "" ) {
		alert("게시구분코드를  선택하신 후 검색하여 주십시오.");
		return;
	}
	
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&BOARD_CLS_CD=" + board_cd;
		argu += "&BTITLE=" + encodeURIComponent(parent.document.form.BTITLE.value);
		argu += "&SEARCH_ID=" + parent.document.form.SEARCH_ID.value;
		argu += "&PUB_YMD=" + parent.document.form.PUB_YMD.value;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=boardList&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "총 <span class='point'> : " + GridObj.getRowsNum()+"</span> 건 ";
	if(status == "false") alert(msg);
	parent.refresh("");
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doAddRow() {

   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	
   	parent.refresh("");
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BOARD_CLS_CD")).setValue(parent.document.form.BOARD_CLS_CD.value);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("DEL_YN")).setValue('N');
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POP_YN")).setValue('N');

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doSave() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length<1) {
		alert("저장하실 게시물을 체크 하십시요.");
		return;
	} else if( grid_array.length > 1) {
		alert("저장은 1건 씩 만 가능합니다.\n 1건만 체크 하십시요.");
		return;
	}
		
	if (confirm("저장하시겠습니까?")) {	// 저장 하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
		var boardhtml = replace(parent.boardframe.getHtml(),"&","♨");
        var SERVLETURL = G_SERVLETURL + "?mod=boardSave&col_ids="+cols_ids+"&b_content=" + encodeURIComponent(boardhtml);	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveAnswer() {
	var board_cd = parent.document.form.BOARD_CLS_CD.value;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length<1) {
		alert("답변 등록 하실 질문을 을 체크 하십시요.");
		return;
	} else if( grid_array.length > 1) {
		alert("답변 등록은 1건 씩 만 가능합니다.\n 1건만 체크 하십시요.");
		return;
	} else if( board_cd != "Q") { 
		alert("QNA 만 답변이 가능 합니다.");
		return;
	} else {
	
		var board_no = GridObj.cells(grid_array[0], GridObj.getColIndexById("BOARD_NO")).getValue();		
		var url = "/mg/board/boardAnswer.jsp?board_no=" + board_no + "&board_cd=" + board_cd;
		popupBoard(url, "Board", 470, 480); 
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	var roll = '<%=s_user.getProperty("USER_ROLL").toString()%>';
	var usr_id = '<%=s_user.getId().toString()%>';
	var userLevel = parseInt(Zero_LTrim(roll.substring(1,3)));
	
	for(var i=0; i < grid_array.length; i++) {
	
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("CHG_ID")).getValue() != usr_id && userLevel > 2) {
			alert("본인이 작성한 질문 및 답변만 삭제 가능 합니다.");
			return;
		}

	}
	
	if (confirm("선택하신 게시물을 삭제하시겠습니까?")) { 
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=boardDelete&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}	
}


function init() {
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<% if(board_cls_cd.equals("none")) { %>
		            	<a href="javascript:doAddRow();" class="btn"><span>행추가</span></a>
		            	<a href="javascript:doSave();" class="btn"><span>저장</span></a>
		            	<a href="javascript:doSaveAnswer();" class="btn"><span>답변등록</span></a>
		            	<a href="javascript:doDelete();" class="btn"><span>삭제</span></a>
		            <%} else if(board_cls_cd.equals("Q")) {%>
		            	<a href="javascript:doAddRow();" class="btn"><span>질문추가</span></a>
		            	<a href="javascript:doSave();" class="btn"><span>저장</span></a>
		            	<a href="javascript:doSaveAnswer();" class="btn"><span>답변등록</span></a>
		            	<a href="javascript:doDelete();" class="btn"><span>삭제</span></a>
		            <%} %>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=30"/>
</jsp:include>
</body>
</html>
