<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>

<html>
<head>

<title>Insert title here</title>

<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;
var cell1;
var cell2;

function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "2E",
	    skin: "dhx_web"
	});
	
	
	cell1 = layout.cells('a');
	
	cell1.setText(leftMenuName);
	
	cell1.attachURL("companyMgmt.jsp", null,  {
		formid: "CP_0401",
		gridid: "<%=grid_obj%>"
	});
	
	cell2 = layout.cells('b');
	
	cell2.hideHeader();
	
	cell2.attachURL("companyUserList.jsp", null,  {
		formid: "CP_0402",
		gridid: "<%=grid_obj%>",
		companyId: "",
		companyNm: "",
		userNm : "",
		init:"1" // 1일 경우 doQuery 안함 
	});
}
function refresh(companyId, companyNm, userNm, init) 
{	
	cell2.attachURL("companyUserList.jsp", null,  {
		formid: "CP_0402",
		gridid: "<%=grid_obj%>",
		companyId: companyId,
		companyNm: companyNm,
		userNm : userNm,
		init:init
	});
	
}
function resize() {
	layout.setSizes();
}

</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        overflow: hidden;
    }
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
</style>

</head>

<body onload="init();" onresize="resize();">

<div id="layoutObj" style="position: relative;"></div>

</body>
</html>