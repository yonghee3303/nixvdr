<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>Agent 설치현황(마스터)</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/cppp/joiningUser.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	var companyId = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_ID")).getValue();
	var companyNm = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_NM")).getValue();
	parent.refresh(companyId,companyNm,"2");
}
function doOnRowSelect(rowId,cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	var companyId = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_ID")).getValue();
	var companyNm = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_NM")).getValue();
	parent.refresh(companyId,companyNm,"2");
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {	
	var selectBox = parent.document.getElementById("searchTerm");
	var term="";
	for(var i=0; i<selectBox.options.length; i++){
		if(selectBox.options[i].selected ==true){
			term = selectBox.options[i].value;
		}
	}
	var argu;
	var mod="searchCompany";
	var con = encodeURIComponent(parent.document.getElementById("con").value);
	
	switch(term){
		case "compNm":
			argu="&COMPANY_NM="+con;
		break;
		
		case "compNo":
			argu="&REG_NO="+con;
		break;
		
		case "ceoNm":
			argu="&CEO_NM="+con;
		break;
		
		default:
			argu="&COMPANY_NM="+con+"&REG_NO="+con+"&CEO_NM="+con;
			mod="searchAllCompany";
		break;
	}

	var grid_col_id     = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod="+mod+"&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);																																									
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	parent.refresh("","","1");
	return true;
}

function doExcelDown()
{	
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");
	
	var selectBox = parent.document.getElementById("searchTerm");
	var term="";
	for(var i=0; i<selectBox.options.length; i++){
		if(selectBox.options[i].selected ==true){
			term = selectBox.options[i].value;
		}
	}
	var argu;
	var mod="excelCompany";
	var con = encodeURIComponent(parent.document.getElementById("con").value);
	
	switch(term){
		case "compNm":
			argu="&COMPANY_NM="+con;
		break;
		
		case "compNo":
			argu="&REG_NO="+con;
		break;
		
		case "ceoNm":
			argu="&CEO_NM="+con;
		break;
		
		default:
			argu="&COMPANY_NM="+con+"&REG_NO="+con+"&CEO_NM="+con;
			mod="excelAllCompany";
		break;
	}
	
		
	fileDownload(G_SERVLETURL+"?mod="+mod+"&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);	
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	//var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function init() {
	setFormDraw();
}

function doAjaxResult(oj) {
	var jData = JSON.parse(oj.responseText);
	document.getElementById("totalCntTD").innerHTML = "총 <span class='point'> : " + jData.TOT_CNT+"</span> 건 ";
	doQuery();
}
function doAddCompany(){
	window.open("/cppp/enrollCompany.jsp",'','width=550px,height=340px,scrollbars=yes,resizable=no');
}
</script>

</head>
<body onload="init();">
<form name="form" method="post">  
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
            	<div class="btnarea">
	            	<%--오류 <a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a> --%>
	            	<a href="javascript:doAddCompany();" class="btn"><span>업체등록</span></a>
	            </div>
	        </td>
        </tr>
        </table>
	</td>  
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" height="80%" style="background-color:white;overflow:hidden"></div>
</body>
</html>
