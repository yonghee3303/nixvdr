<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
  IUser s_user = (IUser) session.getAttribute("j_user"); 
  String companyId = request.getParameter("companyId");
  String companyNm = request.getParameter("companyNm");
  String userNm = request.getParameter("userNm");
  String init = request.getParameter("init");
  if(userNm != null) userNm = java.net.URLDecoder.decode(userNm,"utf-8");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>설치로그조회</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>	
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script type="text/javascript">
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var G_SERVLETURL = "<%=contextPath%>/mg/cppp/member.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedRowId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedRowId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}
function doOnRowSelect(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}
function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery2() 
{

	var grid_col_id = "<%=grid_col_id%>";
	
	var companyId = document.form.COMPANY_ID.value; 
	var userNm = document.form.USER_NM.value;
	var argu = "&COMPANY_ID="+companyId+"&USER_NM="+ encodeURIComponent(userNm);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=selectSysCompUserList&grid_col_id="+grid_col_id+argu);
																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) 
{
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	if(status == "false") alert(msg);
	return true;
}
function doExcelDown()
{	
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");
	
	var companyId = document.form.COMPANY_ID.value; 
	var userNm = document.form.USER_NM.value;
	var argu = "&COMPANY_ID="+companyId+"&USER_NM="+ encodeURIComponent(userNm);

	fileDownload(G_SERVLETURL+"?mod=excelSysCompUserList&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);	
}

function checkMail(strEmail){
	
	 var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	
	 if(!strEmail.match(regExp)){
		return false;
	}else{
		return true;
	}
}
function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	for(var i=0; i < grid_array.length; i++) {

		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("USER_NM")).getValue() == "") {
			alert("Please choose the user.");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("EMAIL")).getValue() == "") {
			alert("Please choose the E-mail");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("POSITION_NM")).getValue() == "") {
			alert("Please choose the position.");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("TEL_NO")).getValue() == "") {
			alert("Please choose the tel.No");
			return;
		}
		if(!checkMail(GridObj.cells(grid_array[i], GridObj.getColIndexById("EMAIL")).getValue())){
			alert("E-mail 형식이 맞지 않습니다.");
			return;
		}
	}
	 
	if (confirm("<%=mm.getMessage("COMG_1009")%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
		var argu ="&STAFF_ID=<%=s_user.getId()%>"; 
		var SERVLETURL = G_SERVLETURL + "?mod=saveCompUserStaff&col_ids="+cols_ids+argu;
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		doQuery2();
	} else {
		alert(messsage);
	}
	return false;
}
function doAddRow() {
  	if(document.form.COMPANY_NM.value==""){
   		alert("회사를 선택해 주시기 바랍니다.");
   		return;
   	}
  	
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
   	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_NM")).setValue(document.form.COMPANY_NM.value);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_ID")).setValue(document.form.COMPANY_ID.value);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function init() {
	
	setFormDraw();
	if("<%=userNm%>"!=null || "<%=userNm%>"!="") document.form.USER_NM.value="<%=userNm%>";
	else document.form.USER_NM.value="";
	loadCombobox();
	if("<%=init%>"=="2") doQuery2();
	
	
}

function loadCombobox() {
	
	var url = "<%=contextPath%>/cp/company.do?mod=getComboCompanyHtml";
	sendRequest(doAjaxResult, "", 'POST', url , false, false);
}

function doAjaxResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var companyCombo = document.getElementById("companyCombo");
	companyCombo.innerHTML = oj.responseText;//jData;

	if("<%=companyId%>"=="null" || "<%=companyId%>"==""){
		document.form.companyCode.value ="all";
		document.form.COMPANY_ID.value="";
	}else{
		document.form.companyCode.value ="<%=companyId%>";
		document.form.COMPANY_ID.value="<%=companyId%>";
	}
		
}

function companyChange(){
	var companyCombo = document.form.companyCode.value;
	
	if(companyCombo =="all"){
		document.form.COMPANY_ID.value="";
		document.form.COMPANY_NM.value="";
	}else{
		document.form.COMPANY_ID.value=companyCombo;
		document.form.COMPANY_NM.value=document.getElementById(companyCombo).innerText;
	}
}
function doQuery(){
	parent.refresh(document.form.COMPANY_ID.value,"", document.form.USER_NM.value,"2");
}
</script>
</head>
<body onload="init();" style="margin: 0px; overflow:hidden;">
<form name="form" method="post"> 
<table style="width:100%;">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
		<td colspan="2">
			<table width="100%" class="board-search" style="table-layout:fixed">
        		<tr>
        			<td>
        				<font id="companyCombo"></font>
						<input type="text" class="text" size="20" name="USER_NM" placeholder="<%=mm.getMessage("SUMG_1203", s_user.getLocale())%>"/>
						<input type="hidden" name="COMPANY_ID" value="">
						<input type="hidden" name="COMPANY_NM" value="">
					</td>
            		<td>
            			<div class="searchbtn">
                			<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                		</div>
            		</td>
        		</tr>
			</table>
		</td>
        </tr>
        <tr> 
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea"><!-- 행추가/수정/ -->
	            	<a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a>
	            	<a href="javascript:doAddRow();" class="btn"><span><%=mm.getMessage("COMG_1011", s_user.getLocale())%></span></a>
	            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" style="background-color:white; width:100%; height:85%;"></div>
</body>
</html>
