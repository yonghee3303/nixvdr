<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   IUser s_user = (IUser) session.getAttribute("j_user");
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

   String contextPath = request.getContextPath();
%>

<html>
<head>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>

<script>

function init() 
{
	var argu = "?formid=CP_0401&gridid=GridObj";
	masterframe.location.href='<%=contextPath%>/mg/company/companyList.jsp' + argu;

}
function doQuery() 
{
	masterframe.doQuery();
}
function refresh(companyId, companyNm,init) 
{		
	parent.refresh(companyId,companyNm, "",init);
	
}

</script>

</head>
<body onload="init();" style="overflow:hidden">
<form name="form" method="post" onsubmit="return false;">
<table width="100%" hright="100%">
<tr>
   	<td>		
	<table width="100%" class="board-search" style="table-layout:fixed">
        <tr>
        	<td>
        		<select id="searchTerm">
					<option value="all">---전체---</option>
					<option value="compNm">업체명</option>
					<option value="compNo">사업자 등록번호</option>
					<option value="ceoNm">대표자</option>
				</select>
				<input type="text" class="text" size="20" id="con"/>
			</td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
	</table>
<!-- 	<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td>
        		<div class="title_num" id="totalCntTD"></div>
        	</td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
      </table> -->
   </td>
</tr>
</table>
<iframe name="masterframe" width="100%" height="85%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
</form>
</body>
</html>
