<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>

<title>포탈 사용자 승인</title>

<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../ext/include/su_grid_common_render.jsp"%>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>

var GridObj 		= {};
var myDataProcessor = {};
var G_SERVLETURL ="/mg/cppp/joiningUser.do";

function init() {
	setFormDraw();
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
}
function setFormDraw(){
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onXLE",doQueryEnd);
}
function doQuery() {
	var userNm = document.getElementById("userNm").value;
	var comNm = document.getElementById("comNm").value;
	var email = document.getElementById("email").value;
	var argu = "&USER_NM="+encodeURIComponent(userNm)+"&COMPANY_NM="+encodeURIComponent(comNm)+"&EMAIL="+email;
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUserApproval&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}
function doQueryEnd(){
	var rowsCount = GridObj.getRowsNum(); //총 갯수
	document.getElementById("listCount").innerHTML = "<b>"+rowsCount+"</b>";
}
function resize() {
	GridObj.setSizes();	
}
function initSearch(){ //초기화 버튼을 눌렀을 때 값들을 다 초기화 시켜준다.
	document.getElementById("userNm").value="";
	document.getElementById("comNm").value="";
	document.getElementById("email").value="";
}
//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("승인하시겠습니까?")) {  
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&STAFF_ID="+"<%=s_user.getId()%>";
		var SERVLETURL = G_SERVLETURL + "?mod=approvalUser&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}
function doReturn(){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
			var argu = "&STAFF_ID="+"<%=s_user.getId()%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=returnUser&col_ids="+cols_ids+argu;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}
function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}
</script>

<style>
div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}   
</style>

</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:5px; overflow: hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
       <table class="board-search" style="table-layout:fixed">
       <!--  <colgroup>
            <col width="80px"/> 
            <col width="200px"/>
            <col width="80px"/>
            <col width="200px"/>  
            <col width="80px"/> 
            <col width="200px"/>
            <col width="180px"/>버튼
        </colgroup> -->
        <tr>
        	<td class="tit">
        		신청자
        	</td>
            <td>
            	<input class="text" type='text' id='userNm' value='' onkeypress="if(event.keyCode==13) {doQuery();}">
            </td>
            <td class="tit">
            	업체명
            </td>
            <td>
            	<input class="text" type='text' id='comNm' value='' onkeypress="if(event.keyCode==13) {doQuery();}">
            </td>
        	<td class="tit">
        		E-mail
        	</td>
        	<td>
        		<input class="text" type='text' id='email' value='' onkeypress="if(event.keyCode==13) {doQuery();}">
        	</td>
			<td><div class="searchbtn">
	            	<%-- <a href="javascript:initSearch();" class="btn"><span><%=mm.getMessage("COMG_1065", s_user.getLocale())%></span></a> --%>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
	            </div>
	    	</td>
        </tr>
		</table>

		<!-- 임시 공간 -->
		<table width="100%" border="0">
       		<tr>
				<td>
				 총 : <font color="red" id="listCount"></font> 건
				</td>
				<td>
					<div class="btnarea" style="float:right; padding-top:11px;">
						<a class="btn" href="javascript:doSave();"><span>승인</span></a>
						<a class="btn" href="javascript:doReturn();"><span>반려</span></a>
					</div>
				</td>
			</tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" style="width:100%; height:90%; background-color:white; overflow:hidden;"></div>
</body>
</html>