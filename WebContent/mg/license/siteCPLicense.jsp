<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CP 라이센스 등록 (사이트)</title>

<%@ include file="../../ext/include/include_css.jsp"%>

</head>

<style>

	.board-search { width:600px; margin-bottom:10px; border:1px solid black;}
	
	.board-search td { height:50px; font-size:12px; text-align:left; border-bottom:0; background:#ffffff;}

	.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(../../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}
	
	 #title {color:white; font-size:13px; font-weight: bold; background:#2589ce;}

	#regist {text-align:right; padding:0, 12px, 0, 0; }

</style>


<body>
<br /><br /><br />
<form>
<center>
<table class="board-search" rules="none" style="frame:border;">
	<tr>
		<td id="title" colspan="2">
			라이센스 등록
		</td>
	</tr>
	<tr>
		<td>
			제품번호 입력
		</td>
		<td>
			<input type="text" size="4" maxlength="5" />-<input type="text" size="4" maxlength="5" />-<input type="text" size="4" maxlength="5" />-<input type="text" size="4" maxlength="5" />
		</td>
	</tr>
	<tr>
		<td>
			사용수량
		</td>
		<td>
			<input type="text" size="4" maxlength="4" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cryptor Plus - CPU 코어수 / 보안USB - 생성가능수량
		</td>
	</tr>
	<tr>
		<td>
			<button>임시키 생성</button>
		</td>
		<td>
			임시키 12345-67890-09876-54321
		</td>
	</tr>
	<tr>
		<td colspan="2">
			마스터 관리자에게 임시키를 전달한 후 인증키를 받아 입력하세요.
		</td>
	</tr>
	<tr>
		<td>
			인증키 입력
		</td>
		<td>
			<input type="text" size="4" maxlength="5" />-<input type="text" size="4" maxlength="5" />-<input type="text" size="4" maxlength="5" />-<input type="text" size="4" maxlength="5" />
		</td>
	</tr>
	<tr>
		<td id="regist" colspan="2">
			<button>제품등록</button>
		</td>
	</tr>
</table>
</center>
</form>


</body>
</html>