<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.organization.IOrganizationManagement"%>
<%@ page import="com.core.component.organization.IOrganization" %>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ include file="../../ext/include/ui_common.jsp"%>

<%
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	IOrganizationManagement Org = (IOrganizationManagement) ComponentRegistry.lookup(IOrganizationManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	 
	/* String eframe = StringUtils.paramReplace(request.getParameter("eventframe")); */
	String eframe = "cell2";
%>

<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
</HEAD>
<BODY>
<script>
function go(dept_cd, p_dept_cd) {
	<%-- parent.<%=eframe%>.deptClick(dept_cd, p_dept_cd); --%>
	parent.<%=eframe%>.getFrame().contentWindow.deptClick(dept_cd, p_dept_cd);
}
//기존 소스
//d = new dTree('d');
//contextPath 추가 : root
d = new dTree('d', <%=pm.getString("component.contextPath.root")%>);
<%
	String deptCode = "";
	
	List Orgmgmt = Org.getAllOrganizations();
		
	for (Iterator all = Orgmgmt.iterator(); all.hasNext();) {
        Map each = (Map) all.next();
        String dept = each.get("ID").toString();
        String deptName = each.get("NAME").toString();
        // if("en".equals(user.getLocale().getLanguage())) {
        // 	deptName = each.get("NAME_EN").toString();
        // }
        String parentDept = each.get("PARENT_ID").toString();
        // deptCode =(String)session.getAttribute("DEPT_NM");
	%>
		d.add("<%=dept%>","<%=parentDept%>","<%=deptName%>","javascript:go('<%=dept%>','<%=parentDept%>')");
	<%
	}
%>
document.write(d);
d.closeAll();
</script>
</BODY>
</HTML>