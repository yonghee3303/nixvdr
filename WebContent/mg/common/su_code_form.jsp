<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ include file="../../ext/include/ui_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<%
	ICodeManagement MCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	 
	/* String eframe = StringUtils.paramReplace(request.getParameter("eventframe")); */
	String eframe = "cell2";
%>
<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
</HEAD>
<BODY onload='build();' style="overflow:hidden;">
	<div id="code_tree"></div>
<script>
var tree = new dhtmlXTreeObject("code_tree","100%","100%",0);
tree.setSkin('dhx_skyblue');
tree.setImagePath("../../dhtmlx_v403/skins/skyblue/imgs/dhxtree_skyblue/");
tree.enableDragAndDrop(false);

tree.setOnClickHandler(function(id){
	parent.<%=eframe%>.getFrame().contentWindow.codeClick(id, tree.getParentId(id));
});

function build(){

	tree.deleteChildItems(0);
	
	<%
		String mcode = "";	
		List sCode = MCode.getAllCodes();
			
		for (Iterator all = sCode.iterator(); all.hasNext();) {
	        Map each = (Map) all.next();	
	        String code = each.get("ID").toString();
	        String codeName = each.get("NAME").toString();
	        String parentCode = each.get("PARENT_ID").toString();
	        if(parentCode.equals("-1")){
		%>
			tree.insertNewChild(0,"<%=code%>","<%=codeName%>");
			<%}else{%>
			tree.insertNewChild("<%= parentCode%>","<%=code%>","<%=codeName%>")
		<% }
		}
	%>
	
	tree.closeAllItems(0);
	
}

</script>
</BODY>
</HTML>