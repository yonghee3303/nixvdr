<%@ page pageEncoding="UTF-8"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%
	String thisWindowPopupFlag ="false";
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IMessageManagement jmm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String parentMenuName="";
	String menuName="";
	IUser P_user = (IUser) session.getAttribute("j_user");
	if(request.getParameter("leftMenuName")!=null){
		if(!StringUtils.isEmpty(request.getParameter("mileStoneName")) && !thisWindowPopupFlag.toLowerCase().equals("true") ) {
			parentMenuName = java.net.URLDecoder.decode(request.getParameter("mileStoneName"),"utf-8");
			menuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
		}
	}else{
		String form = request.getParameter("formid");
		IMenu cmenu = mnu.getMenu(form, P_user);
		String pform = cmenu.getParent().getId();
		IMenu pmenu = mnu.getMenu(pform,P_user);
		parentMenuName =pmenu.getName(P_user.getLocale());
		menuName = cmenu.getName(P_user.getLocale());
		if(request.getParameter("callFrom")!=null){
			menuName=jmm.getMessage("CPMG_1143",P_user.getLocale());
		}
	}
%>
<div class="title">
          <h1><%=menuName %></h1>
          <div class="titleRight">
            <ul class="pageLocation">
              <li><span><a href="#">Home</a></span></li>
              <li><span><a href="#"><%=parentMenuName %></a></span></li>
              <li class="lastLocation"><span><a href="#"><%=menuName %></a></span></li>
            </ul>
          </div>
</div>