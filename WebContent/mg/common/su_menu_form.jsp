<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>

<%@ include file="../../ext/include/ui_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<%
	IMenuManagement menus = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	 
	/* String eframe = StringUtils.paramReplace(request.getParameter("eventframe")); */
	
	String eframe = "cell2";

%>

<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>

</HEAD>
<BODY onload='build();' style="overflow:hidden">
<div >
	<div id="menu_tree"></div>
</div>
<script>
var tree = new dhtmlXTreeObject("menu_tree","100%","100%",0);
tree.setSkin('dhx_skyblue');
tree.setImagePath("../../dhtmlx_v403/skins/skyblue/imgs/dhxtree_skyblue/");
tree.enableDragAndDrop(false);
tree.setOnClickHandler(function(id){
	//alert("id : "+id+" was selected\n menupath : "+tree.getUserData(id,"menuPath"));
	
	<%-- parent.<%=eframe%>.menuClick(id,tree.getParentId(id),tree.getUserData(id,"menuPath")); --%>
	parent.<%=eframe%>.getFrame().contentWindow.menuClick(id,tree.getParentId(id),tree.getUserData(id,"menuPath"));
});

function build(){
		tree.deleteChildItems(0);
		
		 <%
			String mcode = "";
			List menuList = menus.getAllMenus();
			 
			//상위 객체부터 만들어야 하위 메뉴를 붙일 수 있기 때문에 
			for (Iterator all = menuList.iterator(); all.hasNext();) {
		        Map each = (Map) all.next();	
		        String menu_id = each.get("ID").toString();
		        String menu_nm = each.get("NAME").toString();
		        String p_menu_id = each.get("PARENT_ID").toString();
		        String menuPath = each.get("PATH").toString();
		       // String sts = each.get("TYPE").toString();
		        
		        if("-1".equals(p_menu_id)){
			%>
					//void insertNewChild(mixed parentId,mixed id,string text,function actionHandler,string image1,string image2,string image3,string optionStr,mixed children);
					tree.insertNewChild(0,"<%=menu_id%>","<%=menu_nm%>");
					tree.setUserData("<%=menu_id%>","menuPath","<%=menuPath%>");
			<%		
		        }else if("MG".equals(p_menu_id)){	
			%>		
					//void insertNewChild(mixed parentId,mixed id,string text,function actionHandler,string image1,string image2,string image3,string optionStr,mixed children);
					tree.insertNewChild("<%=p_menu_id%>","<%=menu_id%>","<%=menu_nm%>");
					tree.setUserData("<%=menu_id%>","menuPath","<%=menuPath%>"); 
			<%		
		        }
			}
			//만들어진 상위 객체에 하위 메뉴를 붙인다.
			for (Iterator sub = menuList.iterator(); sub.hasNext();) {
		       		Map sub_menu = (Map) sub.next();	
		        	String sub_menu_id = sub_menu.get("ID").toString();
		        	String sub_menu_nm = sub_menu.get("NAME").toString();
		        	String sub_p_menu_id = sub_menu.get("PARENT_ID").toString();
		        	String sub_menuPath = sub_menu.get("PATH").toString();
		        	if(!("MG".equals(sub_p_menu_id) || "MG".equals(sub_menu_id))){
		      %>
		      	tree.insertNewChild("<%=sub_p_menu_id%>","<%=sub_menu_id%>","<%=sub_menu_nm%>");
				tree.setUserData("<%=sub_menu_id%>","menuPath","<%=sub_menuPath%>"); 
		<%
				}
		    }
		%>
		
		tree.closeAllItems(0);	
}
</script>
</BODY>
</HTML>