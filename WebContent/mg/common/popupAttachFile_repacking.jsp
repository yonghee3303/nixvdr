<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>    
<% 
    IUser s_user = (IUser) session.getAttribute("j_user");
	String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
	String docId = StringUtils.paramReplace(request.getParameter("docId"));
	String docTitle = StringUtils.paramReplace(request.getParameter("docTitle"));
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");

%>

<html>
<head>
<title>File Upload</title>
<%-- <%@ include file="../../ext/include/include_css.jsp"%> --%>
<link rel="stylesheet" type="text/css" href="../../ext/css/cooperation_<%=css %>.css" />
<%@ include file="../../ext/include/portal_grid_common.jsp"%><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var queryCount  = 0;

var G_SERVLETURL = "/mg/Exe.do";

////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
function doQueryDuring2()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd2()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
} 
////////////////////////////////////////////////////프로그래스바 처리때문에 
// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	row_id = rowId;
	doFiledownLoad(rowId);
}
function doOnRowSelect(rowId,cellInd){
	
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doFiledownLoad(rowId) {
	row_id = rowId;	
	var filename = GridObj.cells(row_id, GridObj.getColIndexById("FILE_NAME")).getValue();
		
	 var SERVLETURL = G_SERVLETURL + "?mod=fileDownload";	
	 var argu = "&PGM_ID=" + "<%=pgmId %>";
	 	 argu += "&DOC_ID=" + "<%=docId %>";
	     argu += "&FILE_NAME=" + encodeURIComponent(filename);
	 fileDownload(SERVLETURL + argu);
}


// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {

	var grid_col_id = "<%=grid_col_id%>";
	var argu = "&pgmId=<%=pgmId%>&docId=<%=docId%>";

	GridObj.loadXML(G_SERVLETURL+"?mod=search&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
<%-- 	document.getElementById('totalCntTD').innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
 --%>
	if(status == "false") alert(msg);
	return true;
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doDelete(){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("CPMG_1027", s_user.getLocale())%>")) { // 선택하신 반출파일을 삭제하시겠습니까?

		var cols_ids = "<%=grid_col_id%>";
		var argu = "&pgmId=<%=pgmId%>&docId=<%=docId%>";
        var SERVLETURL = G_SERVLETURL + "?mod=tx_delete&col_ids="+cols_ids+argu;	
      
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}	
}

function doOk()
{
	if(GridObj.getRowsNum()>0){
		var url = "/cp/approval.do" + "?mod=saveRepScheduel&docId=<%=docId%>";
		sendRequest(doOkEnd, " " ,"POST", url , false, false);	
	}else{
		alert("업로드할 파일을 추가해주시기 바랍니다.");
	}
}
function doOkEnd(){
	opener.init();
	self.close();
}
function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}
	return false;
}

function doUpload() {
	doQueryDuring2();
	uploadframe.fileUpload();
}

function uploadResult(msg) 
{
	doQueryModalEnd2();
	loadAttachFile();
	if(msg == "SUCCESS") {
		doQuery();
	} else {
		alert("<%=mm.getMessage("CPMG_1029", s_user.getLocale())%>");  // Upload 중 오류가 발생  되었습니다.
	}
}

function loadAttachFile()
{
    var argu = "pgmId=<%=pgmId%>&docId=<%=docId%>&docTitle="+encodeURIComponent("<%=docTitle%>");
    uploadframe.location.href='/mg/common/fileUpload_repacking.jsp?' + argu;
}

function doCancel()
{
	window.close();
}

function init() {
	 loadAttachFile();
	 setFormDraw();
	
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<%-- <table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>리패킹 업로드</h1>
      </td>
  </tr>
</table>
<table width="100%">
<tr>
   	<td>
        <table class="board-search">
        <colgroup>
            <col width="15%" />
            <col width="65%" />
            <col/>            
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1142", s_user.getLocale())%></td>
			<td>
				<iframe name="uploadframe" width="100%" height="22px" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
			<td><div class="btnarea">
				       <a href="javascript:doUpload();" class="btn"><span>Upload</span></a>
				</div>
			</td>
        </tr>
		</table>
		<table width="100%" border="0">
		        <colgroup>
		            <col width="40%" />
		            <col width="60%" />
		        </colgroup>
		        <tr>
		        	<td><div class="title_num" id="totalCntTD"></div></td>
		            <td>
			            <div class="btnarea">
				            	<!-- <a href="javascript:doUpload();" class="btn"><span>Upload</span></a> -->
				            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("COMG_1013", s_user.getLocale())%></span></a>
				            	<a href="javascript:doOk();" class="btn"><span><%=mm.getMessage("COMG_1031", s_user.getLocale())%></span></a>
			            </div>
		            </td>
		        </tr>
		</table>
	</td>
</tr>
</table> --%>
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("CPMG_1202", s_user.getLocale())%></span>
        <a href="javascript:window.close();"><img src="../../ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="body">
        <div class="content2">
            <div>
                <!-- Inquiry Table start -->
                <div class="tableInquiry2">
                    <table>
                    <caption>조회</caption>
                    <colgroup>
                    <col class="col_15p" />
                    <col />
                    <col />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><%=mm.getMessage("CPMG_1200", s_user.getLocale())%></th>
                        <td>
                           <iframe name="uploadframe" width="100%" height="22px" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
                        </td>
                        <td>
                        	<div class="tableBtnSearch3"><button onclick="doUpload()"><span><%=mm.getMessage("CPMG_1135", s_user.getLocale())%></span></button></div>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <!--// Inquiry Table end -->
            </div>
            <div class="subContent">
                <div class="listArea2">
<!--                     <div class="buttonArea">
                        <ul class="btn_crud">
                            <li><a href="#"><span>선택삭제</span></a></li>
                        </ul>
                    </div> -->
                    <div id="gridbox" name="gridbox"  style="width:100%; height:200px; background-color:white;overflow:hidden"></div>
                    <div class="clear"> </div>
                </div>
            </div>
        </div>
        <ul class="btn_crud btn1">
            <li><a class="darken" href="#" onclick="doOk()"><span><%=mm.getMessage("COMG_1031", s_user.getLocale())%></span></a></li>
            <li><a class="none" href="#" onclick="doCancel()"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a></li>
        </ul>
        <div class="clear"> </div>
    </div>
</div>
</form>
</body>
</html>
