<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%
	IUser s_user = (IUser) session.getAttribute("j_user");
	String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
	String docId = StringUtils.paramReplace(request.getParameter("docId"));
	String outClCd = StringUtils.paramReplace(request.getParameter("outClCd"));
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>File Upload</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var G_SERVLETURL = "/mg/Upload.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	row_id = rowId;
}

function doOnRowSelect(rowId,cellInd) {
	row_id = rowId;
	doFiledownLoad(rowId);
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doFiledownLoad(rowId) {
	row_id = rowId;	
	var filename = GridObj.cells(row_id, GridObj.getColIndexById("FILE_NAME")).getValue();
		
	 var SERVLETURL = G_SERVLETURL + "?mod=fileDownload";	
	 var argu = "&PGM_ID=" + "<%=pgmId %>";
	 	 argu += "&DOC_ID=" + "<%=docId %>";
	     argu += "&FILE_NAME=" + encodeURIComponent(filename);
	 fileDownload(SERVLETURL + argu);
}


// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {

	var grid_col_id = "<%=grid_col_id%>";
	var argu = "&pgmId=<%=pgmId%>&docId=<%=docId%>";

	GridObj.loadXML(G_SERVLETURL+"?mod=search&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById('totalCntTD').innerHTML =  "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	if(status == "false") alert(msg);
	return true;
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doDelete() {
	if(!checkRows()) return;	
}

function doOk()
{
	var fileList="";
	var sep="";
	
	for(var row = 0; row < GridObj.getRowsNum(); row++)
	{
		var tmpPath = GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById("FILE_PATH")).getValue() + "/" + GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById("FILE_NAME")).getValue();
		fileList += sep + "<%=paramemter.getString("component.upload.directory")%>" + tmpPath;
		sep = "|";
	}
	opener.OkFileUpload(fileList);
	self.close();
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}
	return false;
}

function doCancel()
{
	window.close();
}

function init() {
	 setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1><%=mm.getMessage("CPMG_1142", s_user.getLocale())%></h1>
      </td>
  </tr>
</table>
<table width="100%">
<tr>
   	<td>
        <table class="board-search">
        <colgroup>
            <col width="20%" />
            <col width="80%" />            
        </colgroup>
		</table>
		<table width="100%" border="0">
		        <colgroup>
		            <col width="40%" />
		            <col width="60%" />
		        </colgroup>
		        <tr>
		        	<td><div class="title_num" id="totalCntTD"></div></td>
		            <td>
			            <div class="btnarea">
			            <a href="javascript:doCancel();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
			            </div>
		            </td>
		        </tr>
		</table>
	</td>
</tr>
</table>
</form>

<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=92"/>
</jsp:include>

</body>
</html>
