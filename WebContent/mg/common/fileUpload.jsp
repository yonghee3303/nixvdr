<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%
String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
String docId = StringUtils.paramReplace(request.getParameter("docId"));

String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path + "/";
 

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Excel Upload</title>
    
	<script type="text/javascript">

		function fileUpload() {
			
			
			var filename = document.Form.j_file.value;
			var lastIndex  = filename.lastIndexOf('.'); 
    		var extension = filename.substring(lastIndex+1, filename.len);
			extension = extension.toLowerCase();
			
			if(extension == "html" || extension == "htm" || extension == "php" || extension == "inc"  || extension == "jsp" || extension == "exe"
			   || extension == "bat" || extension == "cmd") {
				alert("실행 파일은 Upload 금지 파일 입니다.");
			} else if(filename == '' ) {
				alert("Upload 파일을 선택해 주세요.");
			} else {
				
				/* alert("Upload 파일을 선택해 주세요. ----> " + filename); */
				/* document.Form.mod.value = "tx_create"; */
				/* alert("mod --< " + document.Form.mod.value); */
				
				document.Form.submit();
			}
		}
			
	</script>

  </head>
  
  <body>
  <form name="Form" action="/mg/Upload.do?mod=tx_create" method="post" enctype="multipart/form-data">
    
  	<div height="20px">
  		<input type="file" name="j_file" multiple="multiple"/>
  		<button type="submit"></button>
  	</div>
  	
<!--   	<input type="file" name="j_file" onchange="fileUpload(this)" multiple="multiple"/> -->
    <input type="hidden" name="mod" value="tx_create"/>
    <input type="hidden" name="docId" value="<%=docId %>"/>
    <input type="hidden" name="pgmId" value="<%=pgmId %>"/>
     
   </form>

  </body>
</html>
