<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ include file="../../ext/include/ui_common.jsp"%>

<%
	ICodeManagement MCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	 
	String eframe = StringUtils.paramReplace(request.getParameter("eventframe"));
%>


<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
</HEAD>
<BODY>
<script>
function go(code, parentCode) {
	parent.<%=eframe%>.codeClick(code, parentCode);
}

d = new dTree('d');
<%
	String mcode = "";	
	List sCode = MCode.getAllCodes();
		
	for (Iterator all = sCode.iterator(); all.hasNext();) {
        Map each = (Map) all.next();	
        String code = each.get("ID").toString();
        String codeName = each.get("NAME").toString();
        String parentCode = each.get("PARENT_ID").toString();
	%>
		d.add("<%=code%>","<%=parentCode%>","<%=codeName%>","javascript:go('<%=code%>','<%=parentCode%>')");
	<%
	}
%>
document.write(d);
d.closeAll();

</script>
</BODY>
</HTML>