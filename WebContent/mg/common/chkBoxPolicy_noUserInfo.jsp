<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
	String tagname = StringUtils.paramReplace(request.getParameter("tagname"));
	String codeId = StringUtils.paramReplace(request.getParameter("codeId"));
	String lang3 = request.getParameter("locale");
	Locale locale3 = null;
	
	if("ko".equals(lang3)) {
		locale3 = Locale.KOREA;
	} else if("en".equals(lang3)) {
		locale3 = Locale.US;
	} else {
		locale3 = Locale.KOREA;
	}
	if("".equals(lang3) || "null".equals(lang3) || lang3 == null) {
		lang3 = "ko";
	}
	
	String def_val = StringUtils.paramReplace(request.getParameter("def")); 
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	 
	if("".equals(def_val)) {
		def_val = mm.getMessage("COMG_1032", locale3);
	}
%>

<%-- 공통코드 검색조건  Combobox 구현 --%>
<table>
	<tr>
<%
	ICodeManagement MCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	List sCode = MCode.getCodes(codeId);

	for (Iterator all = sCode.iterator(); all.hasNext();) {
		
		ICode icd = (ICode) all.next();
        String code = icd.getId();
        String cdNm = icd.getName();
        if("en".equals(locale3) ) {
        	cdNm = icd.getNameSl();
        }
        String cdKey = icd.getCd();
        
		%>
			<td style="border:0px;"><input type="checkbox" value="<%=cdKey%>" id="<%=tagname%>_<%=cdKey%>" name="<%=tagname%>"><%=cdNm%></td>
		<%
	}
    %>
	</tr>
</table>