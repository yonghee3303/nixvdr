<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>사용자 선택</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="/ext/include/su_grid_common.jsp"%><%-- Dhtmlx Grid용 JSP--%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;


var G_SERVLETURL = "/cp/company.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	// doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	
	var user_id = GridObj.cells(rowId, GridObj.getColIndexById("USER_ID")).getValue();
	var user_nm = GridObj.cells(rowId, GridObj.getColIndexById("USER_NM")).getValue();
	var company_id = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_ID")).getValue();
	var company_nm = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_NM")).getValue();
	var email = GridObj.cells(rowId, GridObj.getColIndexById("EMAIL")).getValue();
	
	opener.OkPopupCompUser(user_id, user_nm, company_id,company_nm, email);
	// window.close();
	self.close();
}

function doOnRowSelect(rowId,cellInd) {
	row_id = rowId;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	row_id = rowId;
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doOk()
{
	if( row_id> 0) {
		doOnRowSelected(row_id, 2);
	} else {
		alert("선택된 정보가 없습니다.");
	}
}

function doCancel() {
	window.close();
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var search_id = document.form.SEARCH_USER.value;
	var search_nm = encodeURIComponent(document.form.SEARCH_NM.value);
	
	var argu = "";
	if(search_id == "1") {
		argu  = "&SEARCH_NM="+search_nm;
	} else if(search_id == "2") {
		argu  = "&EMAIL="+search_nm;
	} else if(search_id == "3") {
		argu  = "&COMPANY_NM="+search_nm;
	}
	
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectCompUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}


function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>
</head>
<body onload="init(); loadCombobox();">
<form name="form" method="post" onsubmit="return false;">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1><%=mm.getMessage("CPMG_1140", s_user.getLocale())%></h1>
    </td>
  </tr>
</table>
<table width="100%">
<tr>
   	<td>
        <table class="board-search">
        <colgroup>
            <col width="25%" />
            <col width="15%" />
            <col width="30%" />
            <col width="80px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1024", s_user.getLocale())%></td>
            <td>			
				<jsp:include page="../../mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0112"/>
					<jsp:param name="tagname" value="SEARCH_USER"/>
				</jsp:include>
            </td>
            <td>
				<input type="text" name="SEARCH_NM" size="20" class="text" onkeypress="if(event.keyCode==13) {doQuery();}">
            </td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doOk();" class="btn"><span><%=mm.getMessage("COMG_1022", s_user.getLocale())%></span></a>
	            	<a href="javascript:doCancel();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=68"/>
</jsp:include>
</body>
</html>
