<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>

<%@ include file="../../ext/include/ui_common.jsp"%>

<%
	IMenuManagement menus = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	 
	String eframe = StringUtils.paramReplace(request.getParameter("eventframe"));

%>

<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
</HEAD>
<BODY>
<script>
function go(menuId, parentId, menuPath) {
	parent.<%=eframe%>.menuClick(menuId, parentId, menuPath);
}

d = new dTree('d');
<%
	String mcode = "";
	List menuList = menus.getAllMenus();
		
	for (Iterator all = menuList.iterator(); all.hasNext();) {
        Map each = (Map) all.next();	
        String menu_id = each.get("ID").toString();
        String menu_nm = each.get("NAME").toString();
        String p_menu_id = each.get("PARENT_ID").toString();
        String menuPath = each.get("PATH").toString();
	%>
		d.add("<%=menu_id%>","<%=p_menu_id%>","<%=menu_nm%>","javascript:go('<%=menu_id%>' , '<%=p_menu_id%>' , '<%=menuPath%>');");
	<%
	}
%>
document.write(d);
d.closeAll();
</script>
</BODY>
</HTML>