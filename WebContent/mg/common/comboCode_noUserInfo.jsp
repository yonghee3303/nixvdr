<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>

<%
	String tagname = StringUtils.paramReplace(request.getParameter("tagname"));
	String codeId = StringUtils.paramReplace(request.getParameter("codeId")); 
	String def_val = StringUtils.paramReplace(request.getParameter("def")); 
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	//IUser c_user = (IUser) session.getAttribute("j_user");
	String lang6 = request.getParameter("locale");
	Locale locale6 = null;
	
	if("ko".equals(lang6)) {
		locale6 = Locale.KOREA;
	} else if("en".equals(lang6)) {
		locale6 = Locale.US;
	} else {
		locale6 = Locale.KOREA;
	}
	if("".equals(lang6) || "null".equals(lang6) || lang6 == null) {
		lang6 = "ko";
	}
	 
	if("".equals(def_val)) {
		def_val = mm.getMessage("COMG_1032", locale6);
	}
%>

<%-- 공통코드 검색조건  Combobox 구현 --%>
<select name="<%=tagname%>">
<%if(def_val != null) {%>
	<option value="<%=def_val%>">--<%=mm.getMessage("COMG_1032", locale6)%>--</option>
<%
   }
	ICodeManagement MCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
	List sCode = MCode.getCodes(codeId);

	for (Iterator all = sCode.iterator(); all.hasNext();) {
		ICode icd = (ICode) all.next();

        String code = icd.getId();
        String cdNm = icd.getName();
        if(!"ko".equals(lang6) ) {
        	cdNm = icd.getNameSl();
        }
        String cdKey = icd.getCd();
		%>
			<option value="<%=cdKey%>"><%=cdNm%></option>
		<%
	}
    %>
</select>