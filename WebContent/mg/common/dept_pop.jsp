<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.organization.IOrganizationManagement"%>
<%@ page import="com.core.component.organization.IOrganization" %>

<%@ include file="../../ext/include/ui_common.jsp"%>

<%
	IOrganizationManagement Org = (IOrganizationManagement) ComponentRegistry.lookup(IOrganizationManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
%>


<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
</HEAD>
<BODY>
<script>
function go(url) {
	// parent.userframe.document.location.href = url;
	alert()
}

d = new dTree('d');
<%
	String deptCode = "";
	
	List Orgmgmt = Org.getAllOrganizations();
		
	for (Iterator all = Orgmgmt.iterator(); all.hasNext();) {
        Map each = (Map) all.next();
        String dept = each.get("ID").toString();
        String deptName = each.get("NAME").toString();
        if("en".equals(user.getLocale().getLanguage())) {
        	deptName = each.get("NAMESL").toString();
        }
        String parentDept = each.get("PARENT_ID").toString();
        // deptCode =(String)session.getAttribute("DEPT_NM");
	%>
		d.add("<%=dept%>","<%=parentDept%>","<%=deptName%>","javascript:go('approval_user_pop.jsp?DEPT=<%=dept%>&DEPT_NAME=<%=deptName%>&USE_FLAG=Y')");
	<%
	}
%>
document.write(d);
d.closeAll();
if('<%=deptCode%>' != '') d.openTo('<%=deptCode%>', true);
</script>
</BODY>
</HTML>