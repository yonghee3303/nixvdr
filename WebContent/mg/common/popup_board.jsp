<%@ page contentType="text/html; charset=UTF-8" session="true" 
         import="com.core.component.util.WebUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%
    IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
    String company = paramemter.getString("component.cp.company");
    String logo_image = ("CP".equals(company)) ? "../../ext/images/logo_serviceace.gif" : "../../ext/images/logo_servicetop.gif";
     
	String board_no = StringUtils.paramReplace(request.getParameter("board_no"));
	String init_flag = StringUtils.paramReplace(request.getParameter("init_flag"));
	String boardClsCd = StringUtils.paramReplace(request.getParameter("boardClsCd"));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="../../ext/include/include_css.jsp"%>
<title>USIM Sales & Management Board</title>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

function init()
{
    var SERVLETURL = "/mg/board.do?mod=getContentPop";
    var argu = "&BOARD_NO=<%=board_no %>";
    sendRequest(doAjaxGetBoard, argu, 'POST', SERVLETURL , false, false);
}

function doAjaxGetBoard(oj) {
	var bdata = oj.responseText;
	var pdata = bdata.split("~");

	document.getElementById("btitle").innerHTML = pdata[0];
	document.getElementById("REG_DT").innerHTML = pdata[1];
	document.getElementById("boardContent").innerHTML = pdata[3];
	
	attachFile();
}

function attachFile()
{    
    var SERVLETURL = "/mg/Upload.do?mod=searchHtml";
    var argu = "&pgmId=boardList&docId=<%=board_no%>";
  
    sendRequest(doAjaxAttachFile, argu, 'POST', SERVLETURL , false, false);
}

function doAjaxAttachFile(oj) {
	document.getElementById("attachFile").innerHTML = oj.responseText;
}

function doFiledownLoad(pgmId, docId, filename) {
	
	 var SERVLETURL = "/cp/Upload.do?mod=fileDownload";	
	 var argu = "&PGM_ID=" + pgmId;
	 	 argu += "&DOC_ID=" + docId;
	     argu += "&FILE_NAME=" + encodeURIComponent(filename);
	 fileDownload(SERVLETURL + argu);
}

// 쿠키 저장 
function notice_setCookie(name, value, expiredays){ 
        var todayDate = new Date(); 
        todayDate.setDate(todayDate.getDate() + expiredays); 
        document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
} 

// 팝업창 닫기 
function notice_closeWin(){ 

        if(document.forms[0].Notice.checked) 
			notice_setCookie("Notice", "done" , 1); // 1=하루동안 공지 창을 열지 않음 

        self.close(); 
} 

// 답변등록화면으로 이동
function doAnswer() {
		
	var url = "/mg/board/boardAnswer.jsp?board_no=<%=board_no %>&board_cd=<%=boardClsCd %>";
	popupBoard(url, "Board", 470, 480); 
}

 </script>
    
</head>
  
<body bgcolor="#f8f9f9" onload="init();" >
<table width="100%" border="0">
  <tr>
    <% if( "B".equals(boardClsCd) ) { %>
    <td class="popup_tit"><h1>NOTICE</h1>
    <% } else if( "Q".equals(boardClsCd)) { %>
    <td class="popup_tit"><h1>Q&A</h1>
    <% } %>
      <a href="javascript:window.close();"><img src="../../ext/images/popup_btn_close.gif" alt="닫기" /></a></td>
  </tr>

</table>

<form>
<table width="100%" border="0" class="p_boardview_1 mgt-15" align="center">
	  <colgroup>
		  <col width="130px" />
		  <col width="420px" />
	  </colgroup>
    <tr>
      <th>제목</th>
      <td><div id="btitle"></div></td>
    </tr>
    <tr>
    	<th>첨부파일</th>
        <td> <div id="attachFile"></div>
        </td>
    </tr>
    <tr>
      <th>작성일/작성자</th>
      <td><div id="REG_DT"></div></td>
    </tr>
    <tr>
        <td colspan="2"> <br>
    		 <div class="notice_list" id="boardContent"></div>
    		 <br>
        </td>
    </tr>
    <% if( "Q".equals(boardClsCd)) { %>
	    <tr>
	  		<td colspan="2">
				<div class="btnarea">
		            <a href="javascript:doAnswer();" class="btn"><span>답변등록</span></a>
		        </div>
			</td>
		</tr>
	<%} %>	
   	<% if(init_flag == "true") { %>
	    <tr>
	    	<td colspan="2">
	
		    		<br>
		    		<INPUT TYPE="checkbox" NAME="Notice" OnClick="notice_closeWin();"> 
		    		<span style="font-size:8pt;">오늘은 이 창을 다시 열지않음</span>
	
	    	</td>
	    </tr>
    <% } %>
</table> 
</form>
</body>
</html>
