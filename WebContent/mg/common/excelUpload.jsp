<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
 
String dept_cd = StringUtils.paramReplace(request.getParameter("DEPT_CD"));
String dept_nm = StringUtils.paramReplace(request.getParameter("DEPT_NM"));
String prod_cd = StringUtils.paramReplace(request.getParameter("PRODUCT_CD"));
String inv_cd = StringUtils.paramReplace(request.getParameter("INV_GRP_CD"));
String inv_nm = StringUtils.paramReplace(request.getParameter("INV_GRP_NM"));
String ins_ymd = StringUtils.paramReplace(request.getParameter("INS_YMD"));
String action = StringUtils.paramReplace(request.getParameter("action"));
String mod = StringUtils.paramReplace(request.getParameter("mod"));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Excel Upload</title>
    <%@ include file="../../ext/include/include_css.jsp"%>
    
	<script type="text/javascript">

	function doUploadExcel() {
	    
	    var progress = document.getElementById("progress");
	    progress.style.visibility = "visible";
	    
		document.Form.submit();
	}
	
	</script>

  </head>
  
  <body>
  <form name="Form" action="<%=action %>" method="post" enctype="multipart/form-data"><div>
  	
    <input type="hidden" name="mod" value="<%=mod %>"/>
    <input type="hidden" name="PRODUCT_CD" value="<%=prod_cd %>"/>
    <input type="hidden" name="DEPT_CD" value="<%=dept_cd %>"/>
    <input type="hidden" name="INS_YMD" value="<%=ins_ymd %>"/>
    <input type="hidden" name="INV_GRP_CD" value="<%=inv_cd %>"/>
    </div>
    <table class="board-search">
        <colgroup>
            <col width="40px" />
            <col width="50px" />
        </colgroup>
        <tr>
        	<td class="tit">입고지점</td>
            <td><%=dept_nm %></td>
        </tr>
        <tr>   
        	<td class="tit">입고구분</td>
            <td><%=inv_nm %></td>
        </tr>
        <tr> 
			<td class="tit">입고일</td>
            <td><%=ins_ymd %></td>
       	</tr>
       	<tr> 
			<td class="tit">File</td>
            <td><input type="file" name= "file" /></td>
       	</tr>
	</table>
		
	<table width="100%" border="0">
        <colgroup>
            <col width="100%" />
        </colgroup>
        <tr>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doUploadExcel();" class="btn"><span>파일Upload</span></a>
	            </div>
            </td>
        </tr>
     </table>
            
   </form>
   <br/><br/>
   <div id="progress" width="100%" style="visibility:hidden;padding-left:10%;"><img src="../../ext/images/progressBar.gif"/></div>

  </body>
</html>
