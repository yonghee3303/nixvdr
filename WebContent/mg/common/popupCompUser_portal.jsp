<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String userId = request.getParameter("requster");
Locale locale5 = null;
String lang5 = null;

if(s_user != null) {
	userId = s_user.getId();
	locale5 = s_user.getLocale();
} else {
	userId = request.getParameter("requster");
	lang5 = request.getParameter("j_janguage");	//language
	
	if("ko".equals(lang5)) {
		locale5 = Locale.KOREA;
	} else if("en".equals(lang5)) {
		locale5 = Locale.US;
	} else if("zh".equals(lang5)) {
		locale5 = Locale.CHINA;
	} else if("ja".equals(lang5)) {
		locale5 = Locale.JAPAN;
	} else {
		locale5 = Locale.KOREA;
	}	
}

IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>

<html>
<head>
<title>사용자 선택</title>
<%-- <%@ include file="../../ext/include/include_css.jsp"%> --%>
<link rel="stylesheet" type="text/css" href="../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../ext/js/lgchem/jquery-ui-1.10.0.js"></script>
<script type="text/javascript" src="../../ext/js/lgchem/common.js"></script>

<%@ include file="../../ext/include/portal_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;


var G_SERVLETURL = "/cp/company.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var user_id = GridObj.cells(rowId, GridObj.getColIndexById("USER_ID")).getValue();
	var user_nm = GridObj.cells(rowId, GridObj.getColIndexById("USER_NM")).getValue();
	var company_id = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_ID")).getValue();
	var company_nm = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_NM")).getValue();
	var email = GridObj.cells(rowId, GridObj.getColIndexById("EMAIL")).getValue();
	var position = GridObj.cells(rowId, GridObj.getColIndexById("POSITION_NM")).getValue();
	var tel_no = GridObj.cells(rowId, GridObj.getColIndexById("TEL_NO")).getValue();
	var macAddr = GridObj.cells(rowId, GridObj.getColIndexById("MAC_ADDR")).getValue();
	opener.OkPopupCompUser("",user_id, user_nm, company_id,company_nm, email, position, tel_no, macAddr);	
	self.close();
}

function doOnRowSelect(rowId,cellInd) {

}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	row_id = rowId;
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doOk()
{	if(!checkRows()) return;
	var user_id;
	var user_nm;
	var company_id;
	var company_nm;
	var email;
	var position;
	var tel_no;
	var macAddr;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	for(var i=0; i < grid_array.length; i++) {
		user_id = GridObj.cells(grid_array[i], GridObj.getColIndexById("USER_ID")).getValue();
		user_nm = GridObj.cells(grid_array[i], GridObj.getColIndexById("USER_NM")).getValue();
		company_id = GridObj.cells(grid_array[i], GridObj.getColIndexById("COMPANY_ID")).getValue();
		company_nm = GridObj.cells(grid_array[i], GridObj.getColIndexById("COMPANY_NM")).getValue();
		email = GridObj.cells(grid_array[i], GridObj.getColIndexById("EMAIL")).getValue();
		position = GridObj.cells(grid_array[i], GridObj.getColIndexById("POSITION_NM")).getValue();
		tel_no = GridObj.cells(grid_array[i], GridObj.getColIndexById("TEL_NO")).getValue();
		macAddr = GridObj.cells(grid_array[i], GridObj.getColIndexById("MAC_ADDR")).getValue();
		opener.OkPopupCompUser("",user_id, user_nm, company_id,company_nm, email, position, tel_no, macAddr);
	}
	self.close(); 
}

function doCancel() {
	self.close();
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var search_id = document.form.SEARCH_USER.value;
	var search_nm = encodeURIComponent(document.form.SEARCH_NM.value);
	
	var argu = "";
	if(search_id == "2") {
		argu  = "&SEARCH_NM="+search_nm;
	} else if(search_id == "3") {
		argu  = "&EMAIL="+search_nm;
	} else if(search_id == "1") {
		argu  = "&COMPANY_NM="+search_nm;
	} else if(search_id == "4"){
		argu = "&STAFF_ID2="+search_nm;
	}
	
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectCompUser&grid_col_id="+grid_col_id+argu+"&STAFF_ID=<%=userId%>&j_janguage=<%=lang5%>");																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", locale5)%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", locale5)%> ";
	if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}
function init() {
	setFormDraw();
	var reciver = opener.document.getElementById("input_exp05").value;
	if(reciver != ""){
		document.form.SEARCH_USER.value="2";
		document.form.SEARCH_NM.value=reciver;
		doQuery();
	}else{
		document.form.SEARCH_USER.value="4";
		document.form.SEARCH_NM.value="<%=userId%>";
		doQuery();
	}	
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>
</head>
<body onload="init(); loadCombobox();">
<form name="form" method="post" onsubmit="return false;">
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("CPMG_1140", locale5)%></span>
        <a href="javascript:window.close();"><img src="../../ext/images/btn_popup_close.png" alt="<%=mm.getMessage("COMG_1016", locale5)%>" /></a>
    </div>
    <div class="body">
        <div class="content2">
            <div>
                <!-- Inquiry Table start -->
                <div class="tableInquiry2">
                    <table>
                    <caption>조회</caption>
                    <colgroup>
                    <col class="col_15p" />
                    <col />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><%=mm.getMessage("COMG_1024", locale5)%></th>
                        <td>
                        	<%if(s_user != null) {%>
                        		<jsp:include page="comboCode.jsp" >
									<jsp:param name="codeId" value="A0111"/>
									<jsp:param name="tagname" value="SEARCH_USER"/>
								</jsp:include>
                        	<%} else { %>
                        		<jsp:include page="comboCode_noUserInfo.jsp" >
									<jsp:param name="codeId" value="A0111"/>
									<jsp:param name="tagname" value="SEARCH_USER"/>
									<jsp:param name="locale" value="<%=lang5%>"/>
								</jsp:include>
                        	<%} %>
                            
                            <input type="text" name="SEARCH_NM" size="20" onkeypress="if(event.keyCode==13) {doQuery();}">
                            <div class="tableBtnSearch3">
                            	<button onclick="doQuery();">
                            		<span><%=mm.getMessage("COMG_1043", locale5)%></span>
                            	</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
                <!--// Inquiry Table end -->
            </div>
            <div class="subContent">
                <div class="listArea2">
                    <div class="listTop">
                    	<div class="title_num" id="totalCntTD"></div>
                    </div>
                 	<div id="gridbox" name="gridbox" width="100%" style="width:100%; height:216px; background-color:white;overflow:hidden"></div>
                   	<div class="clear"> </div>
                </div>
            </div>
        </div>
        <ul class="btn_crud btn1">
			<li><a href="javascript:doOk();" class="darken"><span><%=mm.getMessage("COMG_1022", locale5)%></span></a></li>
            <li><a class="none" href="javascript:doCancel();"><span><%=mm.getMessage("COMG_1023", locale5)%></span></a></li>
        </ul>
        <div class="clear"> </div>
        
                <ul class="btn_crud close2">
			<li><a class="darken" href="#" onclick="goJoinUser();"><span>다음단계</span></a></li>
            <li><a class="none" href="javascript:window.close();"><span>닫기</span></a></li>
        </ul>
        <div class="clear"> </div>
    </div>
</div>
</form>
</html>
