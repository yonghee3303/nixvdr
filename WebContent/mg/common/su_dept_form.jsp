<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.organization.IOrganizationManagement"%>
<%@ page import="com.core.component.organization.IOrganization" %>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ include file="../../ext/include/ui_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<%
	IOrganizationManagement Org = (IOrganizationManagement) ComponentRegistry.lookup(IOrganizationManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	 
	/* String eframe = StringUtils.paramReplace(request.getParameter("eventframe")); */
	String eframe = "cell2";
%>

<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
</HEAD>
<BODY onload='build();' style="overflow:hidden;">
	<div id="dept_tree"></div>
<script>

var tree = new dhtmlXTreeObject("dept_tree","100%","100%",0);
tree.setSkin('dhx_skyblue');
tree.setImagePath("../../dhtmlx_v403/skins/skyblue/imgs/dhxtree_skyblue/");
tree.enableDragAndDrop(false);

tree.setOnClickHandler(function(id){
	parent.<%=eframe%>.getFrame().contentWindow.deptClick(id, tree.getParentId(id));
});

function build(){

	tree.deleteChildItems(0);
	
	<%
	String deptCode = "";
	
	List Orgmgmt = Org.getAllOrganizations();
		
	for (Iterator all = Orgmgmt.iterator(); all.hasNext();) {
        Map each = (Map) all.next();
        String dept = each.get("ID").toString();
        String deptName = each.get("NAME").toString();
        String parentDept = each.get("PARENT_ID").toString();
        if(parentDept.equals("-1")){
	%>
			tree.insertNewChild(0,"<%=dept%>","<%=deptName%>");
	<%}else{%>
			tree.insertNewChild("<%= parentDept%>","<%=dept%>","<%=deptName%>");
		<% }
		}
	%>
	
	tree.closeAllItems(0);
	
}

</script>
</BODY>
</HTML>