<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>    
<%
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String css = pm.getString("component.ui.portal.css");
IUser s_user = (IUser) session.getAttribute("j_user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=mm.getMessage("WTMG_1010", s_user.getLocale())%></title>
<link rel="stylesheet" type="text/css" href="/ext/css/cooperation_<%=css %>.css" />
<script language="JavaScript" src="/ext/js/JUtil.js"></script>
<script>
function closeWindow(){

	if(document.form.noRead.checked){
		   var todayDate = getExpDate(1000,10,10);
           setCookie("IsReadNotice", "1", todayDate, "", "", "");
	}
	window.close();
}
</script>
</head>
<body>
<form name="form" method="post">
<!-- // Window Popup start -->
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("WTMG_1010", s_user.getLocale())%></span>
        <a href="javascript:window.close();"><img src="/ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="body">
        <div class="content" style="width:500px;">
            <p class="provision-txt" style="width:500px;">
            	<%=mm.getMessage("COMG_1071", s_user.getLocale())%>
            </p>
            <div class="provision">
                <dl style="width:500px;">
                <dt>■ <%=mm.getMessage("COMG_1072", s_user.getLocale())%></dt>
                <dd>
                <textarea name="" style="width:480px; height:100px;">
<%=mm.getMessage("COMG_1073", s_user.getLocale())%>

<%=mm.getMessage("COMG_1074", s_user.getLocale())%>
				</textarea>
                </dd>
                <dd class="provision-txt2" style="width:480px;"><input type="checkbox" name="noRead"/>  <span><%=mm.getMessage("COMG_1075", s_user.getLocale())%></span></dd>
                </dl>
            </div>
        </div>
        <ul class="btn_crud close2" style="margin-left:400px">
			<li><a class="none" href=javascript:closeWindow();><span><%=mm.getMessage("CPCOM_1104")%></span></a></li>
        </ul>
        <div class="clear"> </div>
    </div>
</div>
<!-- // Window Popup end -->
</form>
</body>
</html>
