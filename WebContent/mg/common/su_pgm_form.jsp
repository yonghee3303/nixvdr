<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.pgm.IPgm"%>
<%@ page import="com.core.component.pgm.IPgmManagement"%>

<%@ include file="../../ext/include/ui_common.jsp"%>

<%
	IPgmManagement pgm = (IPgmManagement) ComponentRegistry.lookup(IPgmManagement.class);
	
%>


<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/dtree.js"></script>
</HEAD>
<BODY>


<%
	String mcode = "";

	// List dCode = MCode.getCodes("01");
	
	List gridcols = pgm.getCols("SSMS_0405", "grid");
	

	for (Iterator all = gridcols.iterator(); all.hasNext();) {
		// Map each = (Map) all.next();
		IPgm cols = (IPgm) all.next();
		
        String code = cols.getId();
        String codeName = cols.getName();
        String parentCode = cols.getProperty("PARENT_ID").toString() ;
        
	%>
		<%=code%>, <%=parentCode%>, <%=codeName%>, <%=cols.getProperty("COL_ID").toString() %>
	<%
	}
%>

</BODY>
</HTML>