<%@ page contentType = "text/html; charset=UTF-8"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%  
	IUser s_user = (IUser) session.getAttribute("j_user");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IMenu menu = mnu.getMenu("CPP", s_user);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SystemMap</title>
<link rel="stylesheet" type="text/css" href="/ext/css/cooperation_<%=css %>.css" />
<!-- <script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.0.js"></script>
<script type="text/javascript" src="../js/common.js"></script> -->
<script>
function clickMap(id, link){
	if(id=="CPP_0501" || id=="CPP_0502"){
		opener.parent.pageRedirect(link,"?gridid=GridObj&formid="+id.replace("CPP_05","CPCOM_02"));
	}else{
		opener.parent.pageRedirect(link,"?gridid=GridObj&formid="+id);
	}
	self.close();
}
</script>
</head>
<body style="overflow:auto;">
<div class="winPop">
    <div class="header">
        <span>System Map</span>
        <a href="javascript:window.close();"><img src="/ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="SystemMap">
        <table class="SystemMapTable">
        <%
       		   
		        for (int i = 0; i < menu.getChildren().size(); i++) {
		            IMenu child = (IMenu) menu.getChildren().get(i);
		            String menu_name = child.getName(s_user.getLocale());
        			String menu_link = child.hasTarget() ? child.getTarget() : "N/A";
        			String menuId = child.getId();
        			
              		if(!menuId.equals("")) {
           %>
           	  		<tr>
            			<th scope="col"><%=menu_name%></th>
            			<td>
           <%			String div ="";
						for(int j=0; j<child.getChildren().size(); j++){
							IMenu second = (IMenu)child.getChildren().get(j);
							String second_name = second.getName(s_user.getLocale());
							String second_link = second.hasTarget() ? second.getTarget() : "N/A";
							String secondId = second.getId();
							
							if(!secondId.equals("")){
			%>
							<%=div%><a href="#" onclick="clickMap('<%=secondId%>','<%=second_link%>')"><%=second_name %></a>
			<%				div="<span>│</span>";	
							}
						} %>
						</td></tr>
						<%	
              		}
        		}
	%>
        </table>
    </div>
</div>
</body>
</html>