<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%
String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
String docId = StringUtils.paramReplace(request.getParameter("docId"));
String docTitle = StringUtils.paramReplace(request.getParameter("docTitle"));

String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path + "/";
 

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Excel Upload</title>
    
	<script type="text/javascript">

		function fileUpload() {
			var filename = document.Form.j_file.value; //전체 경로
			var lastIndex  = filename.lastIndexOf('.'); 
    		var extension = filename.substring(lastIndex+1, filename.len);
			extension = extension.toLowerCase();
			
			var file_name = filename.substr(filename.lastIndexOf("\\")+1); //파일명.확장자
				file_name = file_name.substring(0,file_name.lastIndexOf("."));//파일명만
			var zIndex = extension.indexOf("z");
			var val = Number(extension.substring(zIndex+1,extension.len));//extension.substring(zIndex+1, extension.len);
			
			if(file_name =="<%=docTitle%>"){ //파일명 같은지 여부 확인
				if(extension == "exe" || extension == "datzip" || extension == "cpd" ) { //exe, datzip 확장자일 경우
					document.Form.submit();
				} else if(filename == '' ) {
					parent.doQueryModalEnd2();
					alert("Upload 파일을 선택해 주세요.");
				} else if(zIndex == 0 && val > 0 && val <100){ //z01, z02 , ... , z99인 확장자를 갖는 경우
					document.Form.submit();
				}else{
					parent.doQueryModalEnd2();
					alert("리패킹후 생성된 파일을 선택해 주세요.");
				}
			}else{
				parent.doQueryModalEnd2();
				alert("리패킹 파일명이 동일하지 않습니다.");
			}
		}
			
	</script>

  </head>
  
  <body>
  <form name="Form" action="/mg/Exe.do" method="post" enctype="multipart/form-data">
  	<div height="20px">
  		<input type="file" name="j_file"  multiple="multiple"/>
  	</div>
    <input type="hidden" name="mod" value="tx_create" />
    <input type="hidden" name="docId" value="<%=docId %>"/>
    <input type="hidden" name="pgmId" value="<%=pgmId %>"/>
   </form>

  </body>
</html>
