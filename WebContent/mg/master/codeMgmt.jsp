<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   
   IUser s_user = (IUser) session.getAttribute("j_user");
%>

<html>
<head>
<title>화면제목</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<script>
function init() {
	var param = "&formid=<%=formId%>&gridid=<%=grid_obj%>";
	
	codeframe.location.href='../common/su_code_form.jsp?eventframe=codeListframe';
    codeListframe.location.href='codeList.jsp?' + param;
}

function refresh() {
    codeframe.location.href='../common/su_code_form.jsp?eventframe=codeListframe';
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
	  <%@ include file="../common/milestone.jsp"%>

      <table width="100%" height="100%" border="0">
        <colgroup>
        <col width="250px" />
        <col width="10px" />
        <col width="" />
        </colgroup>
        <tr>
          <td valign="top"><table width="100%" height="100%" border="0">
              <tr>
                <td class="leftrightdiv">
					<iframe name="codeframe" width="100%" height="500px" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no'></iframe>
				</td>
              </tr>
            </table></td>
          <td></td>
          <td valign="top"><table width="100%" height="100%" border="0">
              <tr>
                <td class="leftrightdiv" valign="top">
                	<div>
                		<iframe name="codeListframe" width="100%" height="100%" valign="top" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
                	</div>
                </td>
              </tr>
            </table></td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
</body>
</html>
