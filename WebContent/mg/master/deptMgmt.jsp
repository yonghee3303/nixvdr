<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.util.StringUtils"%>

<% 
   String formId = StringUtils.paramReplace(request.getParameter("formid"));
   String grid_obj = StringUtils.paramReplace(request.getParameter("gridid"));
%>

<html>
<head>
<title>부서코드관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<script>
function init() {
	var param = "&formid=<%=formId%>&gridid=<%=grid_obj%>";
	
	deptframe.location.href='../common/dept_form.jsp?eventframe=deptListframe';
    deptListframe.location.href='deptList.jsp?' + param;
}

function refresh() {
    deptframe.location.href='../common/dept_form.jsp?eventframe=deptListframe';
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
	  <%@ include file="../common/milestone.jsp"%>

      <table width="100%" height="100%" border="0">
        <colgroup>
        <col width="250px" />
        <col width="10px" />
        <col width="" />
        </colgroup>
        <tr>
          <td valign="top"><table width="100%" height="100%" border="0">
              <tr>
                <td class="leftrightdiv">
					<iframe name="deptframe" width="100%" height="500px" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no'></iframe>
				</td>
              </tr>
            </table></td>
          <td></td>
          <td valign="top"><table width="100%" height="100%" border="0">
              <tr>
                <td class="leftrightdiv" valign="top">
                	<div>
                		<iframe name="deptListframe" width="100%" height="100%" valign="top" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
                	</div>
                </td>
              </tr>
            </table></td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
</body>
</html>
