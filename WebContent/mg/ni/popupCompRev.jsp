<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>수행사 수정</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum

var G_SERVLETURL = "<%=contextPath%>/ni/comp.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	opener.getParentData();
	
	document.getElementById('COMP_ID').value = opener.comp_id;
	document.getElementById('COMP_NM').value = opener.comp_nm;
	document.getElementById('COMP_MEMO').value = opener.comp_memo;
	/* var addrA = opener.addr;
	var addrZ = addrA.substr(0, 7);
	var addrD = addrA.substr(7);
	var addrT = addrZ.replace(/\(/g, '');
	var addrTT = addrT.replace(/\)/g, '');
	document.getElementById('ADDR_ZIP').value = addrTT;
	document.getElementById('ADDR').value = addrD; */
	document.getElementById('ADDR').value = opener.addr;
	document.getElementById('BUS_REG_NO').value = opener.bus_reg_no;
	document.getElementById('OWNER').value = opener.owner;
	document.getElementById('TEL_NO').value = opener.tel_no;
	var use_yn = opener.use_yn;
	if (use_yn == "Y"){
		use_yn = true;
	} else {
		use_yn = false;
	}
	document.getElementById('USE_YN').checked = use_yn;
	document.getElementById('P_COMP_ID').value = opener.p_comp_id;
	document.getElementById('P_COMP_NM').value = opener.p_comp_nm;
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doRev()
{
	var comp_id = document.getElementById('COMP_ID').value;
	var comp_nm = document.getElementById('COMP_NM').value;
	var comp_memo = document.getElementById('COMP_MEMO').value;
/* 	var addr_z = document.getElementById('ADDR_ZIP').value;
	var addr_d = document.getElementById('ADDR').value;
	var addr = "(" + addr_z + ")" + addr_d */
	var addr = document.getElementById('ADDR').value;
	var bus_reg_no = document.getElementById('BUS_REG_NO').value;
	var owner = document.getElementById('OWNER').value;
	var tel_no = document.getElementById('TEL_NO').value;
	var use_yn = document.getElementById('USE_YN').checked;
	var p_comp_id = document.getElementById('P_COMP_ID').value;
	var p_comp_nm = document.getElementById('P_COMP_NM').value;
	
	if (use_yn == true){
		use_yn = "Y";
	} else {
		use_yn = "N";
	}
	
	if (comp_nm == "") {
		alert("수행사 명을 입력해주세요.");
		return;
/*  	} else if (addr_z == "") {
		alert("주소를 입력해주세요. *우편번호 필수입력");
		return; 
	} else if (bus_reg_no == "") {
		alert("사업자 번호를 입력해주세요.");
		return;
	} else if (owner == "") {
		alert("대표자를 입력해주세요.");
		return; */
	} else {
		opener.comp_id = comp_id;
		opener.comp_nm = comp_nm;
		opener.comp_memo = comp_memo;
		opener.addr = addr;
		opener.bus_reg_no = bus_reg_no;
		opener.owner = owner;
		opener.tel_no = tel_no;
		opener.use_yn = use_yn;
		opener.p_comp_id = p_comp_id;
		opener.p_comp_nm = p_comp_nm;
		
		opener.getRevPopupData();
		
		window.close();
	}
}

function doRevEnd()
{
}

//수행사 선택 팝업
function doProjCompPop() {
	popupProjComp("<%=mm.getMessage("NIMG_1014", s_user.getLocale())%>", '', '', 540, 400, "plant", "OkPopupProjComp", <%=pm.getString("component.contextPath.root")%>);
}

function OkPopupProjComp(p_comp_nm, p_comp_id) {
	document.getElementById('P_COMP_NM').value = p_comp_nm;
	document.getElementById('P_COMP_ID').value = p_comp_id;	
}

//전화번호 자동 하이픈 입력
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 4) {
        return number;
    } else if(number.length < 7) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3);
    } else if(number.length == 9) {
        phone += number.substr(0, 2);
        phone += "-";
        phone += number.substr(2, 3);
        phone += "-";
        phone += number.substr(5);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

//사업자번호 자동 하이픈 입력
function inputBusNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var bus = "";
    
    if(number.length < 4) {
        return number;
    } else if(number.length < 6) {
    	bus += number.substr(0, 3);
    	bus += "-";
    	bus += number.substr(3);
    } else {
    	bus += number.substr(0, 3);
    	bus += "-";
	    bus += number.substr(3, 2);
	    bus += "-";
	    bus += number.substr(5);
    }
        
    obj.value = bus;
}

/* function goJusoPopup(){
	// 주소검색을 수행할 팝업 페이지를 호출합니다.
	// 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(https://www.juso.go.kr/addrlink/addrLinkUrl.do)를 호출하게 됩니다.
	var pop = window.open("jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes"); 
	
	// 모바일 웹인 경우, 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(https://www.juso.go.kr/addrlink/addrMobileLinkUrl.do)를 호출하게 됩니다.
    //var pop = window.open("/popup/jusoPopup.jsp","pop","scrollbars=yes, resizable=yes"); 
}

function jusoCallBack(roadAddrPart1, addrDetail, zipNo){
	// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
	document.form.ADDR.value = zipNo + roadAddrPart1 + addrDetail;
} */

function init() {
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>수행사 수정</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="40%" />
            <col width="30%" />
            <col width="25%" />
            <col width="3%" />          
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1011", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="COMP_ID" id="COMP_ID" size="19" readonly>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1012", s_user.getLocale())%><a> *</a></td>
    <td>
    	<input type="text" name="COMP_NM" id="COMP_NM" size="19" maxlength="20">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1013", s_user.getLocale())%></td>
    <td>
    	<textarea rows="4" cols="50" id="COMP_MEMO" class="text"></textarea>
    </td>
    <td></td>
    <td></td>
  </tr>
<%--   <tr>
    <td class="fs_large"><%=mm.getMessage("NIMP_1014", s_user.getLocale())%><a>*</a></td>
    <td>
    	<input type="text" name="ADDR_ZIP" id="ADDR_ZIP" size="14" maxlength="5">
    	<a href="javascript:goJusoPopup();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>
    	<input type="text" name="ADDR" id="ADDR" size="20" maxlength="100">
    </td>
    <td></td>
    <td></td>
  </tr> --%>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1014", s_user.getLocale())%><a> *</a></td>
    <td>
    	<input type="text" name="ADDR" id="ADDR" size="51" maxlength="100">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1015", s_user.getLocale())%><a></a></td>
    <td>
    <input type="text" onKeyup="inputBusNumber(this);" name="BUS_REG_NO" id="BUS_REG_NO" size="14" maxlength="12" oninput="numberMaxLength(this);">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1016", s_user.getLocale())%><a></a></td>
    <td class="fs_large" colspan="3">
    <input type="text" name="OWNER" id="OWNER" size="10" maxlength="20">
    <%=mm.getMessage("NIMP_1017", s_user.getLocale())%>
    <input type="text" onKeyup="inputPhoneNumber(this);" name="TEL_NO" id="TEL_NO" size="18" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1018", s_user.getLocale())%></td>
    <td>
    	<input type="checkbox" name="USE_YN" id="USE_YN" value="Y" checked>
    </td>
    <td></td>
    <td></td>
  </tr>
   <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104F", s_user.getLocale())%><a> *</a></td>
    <td colspan='3'>
    	<input type="text" name="P_COMP_NM" id="P_COMP_NM" size="14" maxlength="50">
    	<a href="javascript:doProjCompPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" style='margin-bottom:4px'/></a>
    	<input type="text" name="P_COMP_ID" id="P_COMP_ID" size="14" maxlength="50">
    </td>
    <td></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea">
	        	<a href="javascript:doRev();" class="btn"><span><%=mm.getMessage("NIMG_1007", s_user.getLocale())%></span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
