<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>일정 수정</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum;

var G_SERVLETURL = "<%=contextPath%>/ni/man.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	opener.getParentData();
	
	document.getElementById('DATE_TIME').value = opener.date_time;
	document.getElementById('CUSTOMER_CD').value = opener.customer_cd;
	document.getElementById('CUSTOMER_NM').value = opener.customer_fir;
	document.getElementById('CUSTOMER_SEC').value = opener.customer_sec;
	document.getElementById('CUSTOMER_PROJECT').value = opener.customer_project;
	
	document.getElementById('CONTACT_MAN').value = opener.contact_man;
	document.getElementById('USER_ID').value = opener.user_id;
	document.getElementById('MAIN_DETAIL').value = opener.main_detail;
	document.getElementById('ISSUE_DETAIL').value = opener.issue_detail;
	document.getElementById('ETC_DETAIL').value = opener.etc_detail;
	document.getElementById('MANAGE_CD').value = opener.manage_cd;
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doRev()
{
	var date_time = document.getElementById('DATE_TIME').value;
	var customer_fir = document.getElementById('CUSTOMER_NM').value;
	var customer_cd = document.getElementById('CUSTOMER_CD').value;
	var customer_sec = document.getElementById('CUSTOMER_SEC').value;
	var customer_project = document.getElementById('CUSTOMER_PROJECT').value;
	
	var contact_man = document.getElementById('CONTACT_MAN').value;
	var user_id = document.getElementById('USER_ID').value;
	var main_detail = document.getElementById('MAIN_DETAIL').value;
	var issue_detail = document.getElementById('ISSUE_DETAIL').value;
	var etc_detail = document.getElementById('ETC_DETAIL').value;
	var manage_cd = document.getElementById('MANAGE_CD').value;
	
	opener.date_time = date_time;
	opener.customer_cd = customer_cd;
	opener.customer_fir = customer_fir;
	opener.customer_sec = customer_sec;
	opener.customer_project = customer_project;
	
	opener.contact_man = contact_man;
	opener.user_id = user_id;
	opener.main_detail = main_detail;
	opener.issue_detail = issue_detail;
	opener.etc_detail = etc_detail;
	opener.manage_cd = manage_cd;
	
	opener.doRevAddRow();

	window.close();
}

//전화번호 자동 하이픈 입력
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 4) {
        return number;
    } else if(number.length < 7) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3);
    } else if(number.length == 9) {
        phone += number.substr(0, 2);
        phone += "-";
        phone += number.substr(2, 3);
        phone += "-";
        phone += number.substr(5);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

//고객사 선택 팝업
function doCustomerPop() {
	popupCustomer("고객사 선택", '', '', 540, 400, "plant", "OkPopupCustomer", <%=pm.getString("component.contextPath.root")%>);
}

function OkPopupCustomer(customer_cd, customer_nm,customer_sec,customer_project) {
	document.getElementById('CUSTOMER_CD').value = customer_cd;
	document.getElementById('CUSTOMER_NM').value = customer_nm;	
	document.getElementById('CUSTOMER_SEC').value = customer_sec;
	document.getElementById('CUSTOMER_PROJECT').value = customer_project;
}

//달력
function init() {
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"DATE_TIME",button:"calendar_icon"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>수정</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="19%" />
            <col width="30%" />
            <col width="19%" />
            <col width="30%" />
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large">일정코드</td>
    <td>
    	<input type="text" name="MANAGE_CD" id="MANAGE_CD" size="20" placeholder="자동부여" readonly>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">일자<a> *</a></td>
    <td colspan="2">
    	<input type="text" name="DATE_TIME" id="DATE_TIME" size="14" maxlength="10"> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
    </td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large" rowspan="2">고객사<a> *</a></td>
    <td>
    	<input type="text" name="CUSTOMER_CD" id="CUSTOMER_CD" size="15" maxlength="50" placeholder="자동부여" readonly>
    	<a href="javascript:doCustomerPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" style='margin-bottom:4px; margin-right:2px;'/></a>
    </td>
    <td class="fs_large">고객사Sub</td>
    <td>
    	<input type="text" name="CUSTOMER_SEC" id="CUSTOMER_SEC" size="20" maxlength="20" placeholder="자동부여" readonly>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
    	<input type="text" name="CUSTOMER_NM" id="CUSTOMER_NM" size="15" maxlength="50" placeholder="자동부여" readonly>
    </td>
    <td class="fs_large">프로젝트</td>
    <td>
    	<input type="text" name="CUSTOMER_PROJECT" id="CUSTOMER_PROJECT" size="20" maxlength="50" placeholder="자동부여" readonly>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">컨택한 사람</td>
    <td>
    	<input type="text" name="CONTACT_MAN" id="CONTACT_MAN" size="20" maxlength="20">
    </td>
    <td class="fs_large">담당자</td>
    <td>
		<jsp:include page="/mg/ni/comboCodeUser.jsp" >
			<jsp:param name="codeId" value="A1199"/>
			<jsp:param name="tagname" value="USER_ID"/>
			<jsp:param name="def" value=" "/>
		</jsp:include>
	</td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">주요내용</td>
    <td colspan="4">
    	<textarea rows="4" cols="50" id="MAIN_DETAIL"></textarea>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">이슈내용</td>
    <td colspan="4">
    	<textarea rows="3" cols="50" id="ISSUE_DETAIL"></textarea>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">기타(평상시영업)내용</td>
    <td colspan="4">
    	<textarea rows="3" cols="50" id="ETC_DETAIL"></textarea>
    </td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea" style='margin:15px 0px 0px 0px'>
	        	<a href="javascript:doRev();" class="btn"><span>저장</span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
