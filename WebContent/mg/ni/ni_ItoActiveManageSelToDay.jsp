<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String suser_id = s_user.getProperty("USER_ID").toString();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>ITO Active Manage Select to Day Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var month 			= "";

var now = new Date();

var now_year		= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

//그리드폼
function setFormDraw() {	
	<%-- GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes();	--%>
	
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");

	/* GridObj.setHeader("고객사,사업명,2022년,#cspan,#cspan,#cspan,1사분기,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan");
	GridObj.attachHeader(["#rspan","#rspan","#rspan","#rspan","#rspan","#rspan","1","#cspan","#cspan","2","#cspan","#cspan","3","#cspan","#cspan","분기계","#cspan","#cspan"]); */
	GridObj.setHeader("일자,고객사1,고객사2,컨택한사람,담당자,주요내용,이슈내용,기타영업내용")
	GridObj.setInitWidths("120,80,80,80,80,300,300,300");
	GridObj.setColAlign("center,center,center,center,center,left,left,left");
	GridObj.setColumnIds("DATE_TIME,CUSTOMER_FIR,CUSTOMER_SEC,CONTACT_MAN,USER_ID,MAIN_DETAIL,ISSUE_DETAIL,ETC_DETAIL");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str,str,str,str,str,str");
	GridObj.setColTypes("ro,ro,ro,ro,coro,ro,ro,ro");
	
	doRequestUsingPOST_dhtmlxGrid(GridObj,"A1199","USER_ID","");
	
	/* doRequestUsingPOST_dhtmlxGrid(GridObj,"A1120","CUSTOMER_CD",""); */
	/* GridObj.setDateFormat("%Y-%m-%d") */
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);
/* 	GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange); */

	GridObj.init();

	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	var user_id = document.form.USER_ID.value;
	var start_ymd = document.form.START_YMD.value;
	var end_ymd = document.form.END_YMD.value;
	
	var grid_col_id  = "DATE_TIME,CUSTOMER_FIR,CUSTOMER_SEC,CONTACT_MAN,USER_ID,MAIN_DETAIL,ISSUE_DETAIL,ETC_DETAIL";
	var argu  = "&USER_ID="	+encodeURIComponent(user_id);
		argu  += "&START_YMD="	+encodeURIComponent(start_ymd);
		argu  += "&END_YMD="	+encodeURIComponent(end_ymd);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoActiveManageSelToDay&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

$(document).ready(function(){
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"START_YMD",button:"calendar_icon"});
	calendar2 = new dhtmlXCalendarObject({input:"END_YMD",button:"calendar_icon2"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");

		$('input[name="nowpage"]').change(function() {
	        var value = $(this).val();
			if(value==0) {
				parent.init();
			}
			if(value==2) {
				parent.toModify();
			}
			if(value==3) {
				parent.toCustomerModify();
			}
	    });
	});

function init() {
	<%-- document.getElementById('USER_ID').value = "<%=user_id%>"; --%>
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
        	<col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="140px" />
            <col width="40px" />
            <col width="50px" />
            <col width="110px" />
            <col width="40px" />
            <col width="65px" />
            <col width="110px" />
            <col width="40px" />
            <col width="50px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit" colspan="2">활동관리조회(담당자별)</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0">
			</td>
			<td></td>
			<td class="tit">활동관리조회(일자별)</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1" checked="checked">
			</td>
			<td></td>
        	<td class="tit">활동관리 입력</td>
	        <td>
	        	<input type="radio" name="nowpage" value="2">
			</td>
			<td></td>
			<td class="tit">고객사등록관리</td>
	        <td>
	        	<input type="radio" name="nowpage" value="3">
			</td>
			<td></td>
            <td></td>
        </tr>
        <tr>
        	<td class="tit">날짜</td>
        	<td colspan="5">
    			<input type="text" name="START_YMD" id="START_YMD" size="14" maxlength="64" class="text" style='font-size:12px'> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
				~
    			<input type="text" name="END_YMD" id="END_YMD" size="14" maxlength="64" class="text" style='font-size:12px'> <span><img id="calendar_icon2" src="../../../ext/images/calendar.png" border="0"></span>
    		</td>
    		<td></td>
        	<td class="tit">담당자</td>
	    	<td colspan="2">
				<jsp:include page="/mg/ni/comboCodeUser.jsp" >
					<jsp:param name="codeId" value="A1199"/>
					<jsp:param name="tagname" value="USER_ID"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
	    	<td></td>
	    	<td></td>
	    	<td></td>
	    	<td></td>
	    </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doQuery();" class="btn"><span>조회</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="77%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>