<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>

<html>
<head>

<title>Insert title here</title>

<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;
var cell1;
var cell2;

var now = new Date();

function init() {
	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "2E",
	    skin: "dhx_web"
	});
	
	
	cell1 = layout.cells('a');
	cell1.setHeight(650);

	//cell1.setText("<table style='padding:0; margin0; width:98%; height:100%;'><tr><td id='caption'>"+leftMenuName+"</td></tr></table>");
	
	cell1.setText(leftMenuName);
	
	cell1.attachURL("ni_ItoSalesStMaSelMgnt.jsp", null,  {
		formid: "NIM_040401",
		gridid: "<%=grid_obj%>"
	});
	
	cell2 = layout.cells('b');
	cell2.setHeight(350);
	cell2.hideHeader();
	
	cell2.attachURL("../../mg/ni/ni_ItoSalesStMaSelSec.jsp", null,  {
		formid: "NIM_040403",
		gridid: "<%=grid_obj%>",
		selecttype: "admit",
		nowyear: new Date().toISOString().substring(0, 4)
	});
}

function toResult() {
	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "4E",
	    skin: "dhx_web"
	});
	
	
	cell1 = layout.cells('a');
	cell1.setHeight(230);

	//cell.setText("<table style='padding:0; margin0; width:98%; height:100%;'><tr><td id='caption'>"+leftMenuName+"</td></tr></table>");

	cell1.setText("매출목표 달성도");
	
	cell1.attachURL("ni_ItoSalesStMaResultMgnt.jsp", null,  {
		formid: "NIM_040404",
		gridid: "<%=grid_obj%>"
	});
	
	cell2 = layout.cells('b');

	cell2.hideHeader();
	
	cell2.attachURL("../../mg/ni/ni_ItoSalesStMaResultSec.jsp", null,  {
		formid: "NIM_040406",
		gridid: "<%=grid_obj%>",
		nowyear: new Date().toISOString().substring(0, 4)
	});
	
	cell3 = layout.cells('c');

	cell3.hideHeader();
	
	cell3.attachURL("../../mg/ni/ni_ItoSalesStMaResultThi.jsp", null,  {
		formid: "NIM_040407",
		gridid: "<%=grid_obj%>",
		nowyear: new Date().toISOString().substring(0, 4)
	});
	
	cell4 = layout.cells('d');

	cell4.hideHeader();

	cell4.attachURL("../../mg/ni/ni_ItoSalesStMaResultFou.jsp", null,  {
		formid: "NIM_040408",
		gridid: "<%=grid_obj%>",
		nowyear: new Date().toISOString().substring(0, 4)
	});
}

function refreshSub(selecttype,nowyear) 
{	
	cell2.attachURL("../ni/ni_ItoSalesStMaSelSec.jsp", null,  {
		formid: "NIM_040403",
		gridid: "<%=grid_obj%>",
		selecttype: selecttype,
		nowyear: nowyear
	});
	
}

function refreshResult(nowyear) 
{	
	cell2.attachURL("../ni/ni_ItoSalesStMaResultSec.jsp", null,  {
		formid: "NIM_040405",
		gridid: "<%=grid_obj%>",
		nowyear: nowyear
	});
	
	cell3.attachURL("../ni/ni_ItoSalesStMaResultThi.jsp", null,  {
		formid: "NIM_040406",
		gridid: "<%=grid_obj%>",
		nowyear: nowyear
	});
	
	cell4.attachURL("../ni/ni_ItoSalesStMaResultFou.jsp", null,  {
		formid: "NIM_040407",
		gridid: "<%=grid_obj%>",
		nowyear: nowyear
	});
	
}

function resize() {
	layout.setSizes();
}






</script>

<style>

    #layoutObj {
        width: 100%;
        height: 100%;
        margin: 0px;
        overflow: hidden;
    }
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
</style>

</head>

<body onload="init();" onresize="resize();">

<div id="layoutObj" style="position: relative;"></div>

</body>
</html>