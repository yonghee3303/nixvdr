<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
/*    String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid"); */
   IUser s_user = (IUser) session.getAttribute("j_user");
   IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
   String contextPath = request.getContextPath();
%>

<html>
<head>

<style>/*지원자 등록*/
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>프로젝트 관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

var rowsNumb		= "";

function setFormDraw() {	
	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>";
	userframe.location.href='../../mg/ni/ni_RecruitMain.jsp?eventframe=authframe' + argu;
}

function init() 
{
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"PJYMD_FROM",button:"calendar_icon"});
	calendar2 = new dhtmlXCalendarObject({input:"PJYMD_TO",button:"calendar_icon2"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	
	document.getElementById('PJYMD_FROM').value = new Date().toISOString().substring(0, 10);
	
	setFormDraw();
}

function doQuery() 
{
/* 	var pjn = document.form.PJYMD_FROM.value
	
	if(pjn==""){
		alert("날짜를 선택해주세요");
		return;
	} */
	
	userframe.doQuery();
}

function getRowsNumb()
{
	document.getElementById("totalCntTD").innerHTML = rowsNumb;
}

//전화번호 자동 하이픈 입력
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 5) {
        return number;
    } else if(number.length < 9) {
        phone += number.substr(0, 4);
        phone += "-";
        phone += number.substr(4);
    } else if(number.length == 9) {
        phone += number.substr(0, 2);
        phone += "-";
        phone += number.substr(2, 3);
        phone += "-";
        phone += number.substr(5);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

//프로젝트 선택 팝업
function doProjectPop() {
	popupProject("<%=mm.getMessage("NIMG_1059", s_user.getLocale())%>", '', '', 620, 500, "plant", "OkPopupProject", <%=pm.getString("component.contextPath.root")%>);
}

function popupProject(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '프로젝트선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupProject.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupProject.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function OkPopupProject(proj_no, proj_nm) {
	document.getElementById('PROJ_NO').value = proj_no;
	document.getElementById('PROJ_NM').value = proj_nm;	
}


</script>

</head>
<body onload="init();" style="overflow:hidden">
<form name="form" method="post">
<table width="100%" height="100%">
<tr>
   	<td>
	  <%-- <%@ include file="/mg/common/milestone.jsp"%> --%>
	  
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="120px" />
            <col width="160px" />
            <col width="120px" />
            <col width="120px" />
            <col width="120px" />
            <col width="200px" />
            <col width="200px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("NIMG_1051", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="PROJ_NO" id="PROJ_NO" size="14" class="text">
            	<a href="javascript:doProjectPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
            </td>
            <td colspan="2">
            	<input type="text" name="PROJ_NM" id="PROJ_NM" size="34" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("NIMG_1052", s_user.getLocale())%></td>
            <td>
				<input type="text" name="PJYMD_FROM" id="PJYMD_FROM" size="14" maxlength="64" class="text" style='font-size:12px' readonly> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
            	 <span style='vertical-align: top;'>~</span>
            </td>
        	<td>
    			<input type="text" name="PJYMD_TO" id="PJYMD_TO" size="14" maxlength="64" class="text" style='font-size:12px' readonly> <span><img id="calendar_icon2" src="../../../ext/images/calendar.png" border="0"></span>
    		</td>
    		<td>
    			<div class="btnarea">
    			</div> 
    		</td>
        </tr>
        <tr>
    		<td class="tit"><%=mm.getMessage("NIMG_1053", s_user.getLocale())%></td>
            <td>
				<input type="text" name="COMP_NM" id="COMP_NM" size="14" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("NIMG_1054", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_ID" id="USER_ID" size="14" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("NIMG_1055", s_user.getLocale())%></td>
            <td colspan="2">
				<input type="text" onKeyup="inputPhoneNumber(this);" name="TEL_NO" id="TEL_NO" size="12" maxlength="13" oninput="numberMaxLength(this);" class="text" style='font-size:12px'>
            </td>
            <td>
    			<div class="btnarea">
    			</div> 
    		</td>
    	</tr>
    	<tr>
            <td class="tit"><%=mm.getMessage("NIMG_1056", s_user.getLocale())%></td>
            <td>
				<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1107"/>
					<jsp:param name="tagname" value="INDUS_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
            <td class="tit"><%=mm.getMessage("NIMG_1057", s_user.getLocale())%></td>
            <td>
				<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1108"/>
					<jsp:param name="tagname" value="PROJ_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
            <td class="tit"><%=mm.getMessage("NIMG_1058", s_user.getLocale())%></td>
            <td colspan="2">
				<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1110"/>
					<jsp:param name="tagname" value="PJ_AREA_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
            <td>
    			<div class="btnarea">
    			</div> 
    		</td>
    	</tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	           <div class="btnarea">
	           		<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMG_1004", s_user.getLocale())%></span></a>
	           </div> 
            </td>
        </tr>
      </table>
   </td>
</tr>
<tr>	     
	<td height="100%" valign="top">
		<iframe name="userframe" height="100%" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
	</td>

</tr>
</table>
</form>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="authframe=162"/>
</jsp:include> --%>
</body>
</html>
