<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
/*    String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid"); */
   IUser s_user = (IUser) session.getAttribute("j_user");
   IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
   String contextPath = request.getContextPath();
%>

<html>
<head>
<title>매출목표실적</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

var rowsNumb		= "";
var now_year		= "";
var selecttype		= "";

function setFormDraw() {
	selecttype = "admit";
	var argu = "&formid=NIM_040401&gridid=TARGET_MONTH,MANPOWER_OWN,MANMONTH_OWN,SAL_AMOUNT_OWN,SAL_PROFIT_OWN,MANPOWER_SON,MANMONTH_SON,SAL_AMOUNT_SON,SAL_PROFIT_SON,MANPOWER_GWA,MANMONTH_GWA,SAL_AMOUNT_GWA,SAL_PROFIT_GWA,MANPOWER_GO,MANMONTH_GO,SAL_AMOUNT_GO,SAL_PROFIT_GO,MANPOWER_TOTAL,MANMONTH_TOTAL,SAL_AMOUNT_TOTAL,SAL_PROFIT_TOTAL";
	userframe.location.href='../../mg/ni/ni_ItoSalesStMaSelFir.jsp?eventframe=authframe' + argu;
}

function doQuery() 
{
	userframe.doQuery();
}

function getNowYear()
{
	now_year = document.form.NOW_YEAR.value;
}

function getRowsNumb()
{
	document.getElementById("totalCntTD").innerHTML = rowsNumb;
}

function fnCalCount(type, ths){
    var $input = $(ths).parents("td").find("input[name='NOW_YEAR']");
    var tCount = Number($input.val());
    
    if(type=='p'){
        $input.val(Number(tCount)+1);
        
    }else{
        $input.val(Number(tCount)-1);    
    }
}

$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};

		$('input[name="nowpage"]').change(function() {
	        var value = $(this).val();
			if(value==1) {
				parent.toResult();
			}
	    });
		
		$('input[name="selecttype"]').change(function() {
	        var value = $(this).val();
			if(value=="admit") {
				selecttype = "admit";
			}
			if(value=="calcu") {
				selecttype = "calcu";
			}
			userframe.doQuery();
	    });
	});
	
function init() 
{
	document.getElementById('NOW_YEAR').value = new Date().toISOString().substring(0, 4);
	setFormDraw();
}

</script>

<style>/*프로젝트 상태 셀렉박스 스타일*/
select {
width: 60px;
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

</head>
<body onload="init();" style="overflow:hidden">
<form name="form" method="post">
<table width="100%" height="100%">
<tr>
   	<td>
	  <%-- <%@ include file="/mg/common/milestone.jsp"%> --%>
	  
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="110px" />
            <col width="40px" />
            <col width="50px" />
            <col width="120px" />
            <col width="40px" />
            <col width="50px" />
            <col width="50px" />
            <col width="150px" />
            <col/>
        </colgroup>
         <tr>
        	<td class="tit">실적조회화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0" checked="checked">
			</td>
			<td></td>
			<td class="tit">달성도조회화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1">
			</td>
			<td></td>
        	<td class="tit">날짜</td>
            <td>
            	<button type="button" onclick="fnCalCount('m',this);" style="padding: 3px 7px 0px 7px;">-</button>
            	<input type="text" name="NOW_YEAR" id="NOW_YEAR" size="7" maxlength="4" style="padding: 5px 0 0 0;" readonly>
            	<button type="button" onclick="fnCalCount('p',this);" style="padding: 3px 6px 0px 6px;">+</button>
            </td>
            <td></td>
        </tr>
        <tr>
        	<td class="tit">현업건 인정</td>
	        <td>
	        	<input type="radio" name="selecttype" value="admit" checked="checked">
			</td>
			<td></td>
			<td class="tit">현업건 정산처리</td>
	        <td>
	        	<input type="radio" name="selecttype" value="calcu">
			</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
        </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	           <div class="btnarea">
	           		<a href="javascript:doQuery();" class="btn"><span>조회</span></a>
	           </div> 
            </td>
        </tr>
      </table>
   </td>
</tr>
<tr>	     
	<td height="100%" valign="top">
		<iframe name="userframe" height="100%" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
	</td>

</tr>
</table>
</form>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="authframe=162"/>
</jsp:include> --%>
</body>
</html>
