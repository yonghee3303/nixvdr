<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  String contextPath = request.getContextPath();
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>매출목표진행도</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%> <%@ include file="../../ext/include/su_grid_common.jsp" %> <%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var now = new Date();

var now_year		= "";

var rev_check		= false;
var rowsNumb		= "";

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");

	GridObj.setHeader("ITO,인력,#cspan,#cspan,#cspan,MM,#cspan,#cspan,#cspan,금액,#cspan,#cspan,#cspan,이익,#cspan,#cspan,#cspan");
	GridObj.attachHeader(["#rspan","목표","실적","차이","진행율","목표","실적","차이","진행율","목표","실적","차이","진행율","목표","실적","차이","진행율"]);
	GridObj.setInitWidths("80,70,70,70,80,70,70,70,80,130,130,130,80,130,130,130,80");
	GridObj.setColAlign("center,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right");
	GridObj.setColumnIds("TARGET_MONTH,MANPOWER_TARGET,MANPOWER_PERF,MANPOWER_DIFF,MANPOWER_PROGR,MANMONTH_TARGET,MANMONTH_PERF,MANMONTH_DIFF,MANMONTH_PROGR,SAL_AMOUNT_TARGET,SAL_AMOUNT_PERF,SAL_AMOUNT_DIFF,SAL_AMOUNT_PROGR,SAL_PROFIT_TARGET,SAL_PROFIT_PERF,SAL_PROFIT_DIFF,SAL_PROFIT_PROGR");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int");
	GridObj.setColTypes("ro,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron");
	GridObj.setNumberFormat("0.0",1,".",",");
	GridObj.setNumberFormat("0.0",2,".",",");
	GridObj.setNumberFormat("0.0",3,".",",");
	GridObj.setNumberFormat("0.0%",4,".",",");
	GridObj.setNumberFormat("0.0",5,".",",");
	GridObj.setNumberFormat("0.0",6,".",",");
	GridObj.setNumberFormat("0.0",7,".",",");
	GridObj.setNumberFormat("0.0%",8,".",",");
	GridObj.setNumberFormat("0,000",9,".",",");
	GridObj.setNumberFormat("0,000",10,".",",");
	GridObj.setNumberFormat("0,000",11,".",",");
	GridObj.setNumberFormat("0.0%",12,".",",");
	GridObj.setNumberFormat("0,000",13,".",",");
	GridObj.setNumberFormat("0,000",14,".",",");
	GridObj.setNumberFormat("0,000",15,".",",");
	GridObj.setNumberFormat("0.0%",16,".",",");
	
	GridObj._in_header_stat_sum=function(tag,index,data){
	    var calck=function(){
	        var sum=0;                                     
	        this.forEachRow(function(id){
	        	
	            sum+=this.cellById(id,index).getValue()*1;
	            
	        })
	        sum=sum.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	    	return this._aplNF(sum,0);
	    };
	    this._stat_in_header(tag,calck,index,data);
	}
	
/* 	GridObj.attachFooter("합계,<div id='mp_tar'>0</div>,<div id='mp_perf'>0</div>,<div id='mp_diff'>0</div>,<div id='mp_pg'>0</div>,<div id='mm_tar'>0</div>,<div id='mm_perf'>0</div>,<div id='mm_diff'>0</div>,<div id='mm_pg'>0</div>,<div id='sa_tar'>0</div>,<div id='sa_perf'>0</div>,<div id='sa_diff'>0</div>,<div id='sa_pg'>0</div>,<div id='sp_tar'>0</div>,<div id='sp_perf'>0</div>,<div id='sp_diff'>0</div>,<div id='sp_pg'>0</div>"
			,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);
	 */
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);
	GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange);
	
	GridObj.init();

	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	//doRevPop();
}

function onRightButtonClick(rowId,cellInd) {
	var rightClick = GridObj.cells(rowId, GridObj.getColIndexById("PROJ_NO")).getValue();
}

function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	rev_check = true;
	
	//var proj_no = GridObj.cells(rowId, GridObj.getColIndexById("PROJ_NO")).getValue();
	//parent.parent.refresh(proj_no);
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	rev_check = false;
	parent.getNowYear();
	now_year = parent.now_year;
	var sel_type = "ito"
	var grid_col_id  = "TARGET_MONTH,MANPOWER_TARGET,MANPOWER_PERF,MANPOWER_DIFF,MANPOWER_PROGR,MANMONTH_TARGET,MANMONTH_PERF,MANMONTH_DIFF,MANMONTH_PROGR,SAL_AMOUNT_TARGET,SAL_AMOUNT_PERF,SAL_AMOUNT_DIFF,SAL_AMOUNT_PROGR,SAL_PROFIT_TARGET,SAL_PROFIT_PERF,SAL_PROFIT_DIFF,SAL_PROFIT_PROGR";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
		argu += "&SEL_TYPE="	+encodeURIComponent(sel_type);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=ItoSalesStMaResultSel&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");

	parent.rowsNumb = " <span class='point'>" + now_year.substring(0, 4)+"</span> 년 <span class='point'>"+now_year.substring(4, 8)+"</span> 월 기준 ITO";
	parent.getRowsNumb();
	
	// calculateFooterValues();
	
	GridObj.setRowTextStyle(1, "border:1px solid gray;");
	GridObj.setCellTextStyle(1, 4, "background-color: #EAF3FF; border:1px solid gray;");
	GridObj.setCellTextStyle(1, 8, "background-color: #EAF3FF; border:1px solid gray;");
	GridObj.setCellTextStyle(1, 12, "background-color: #EAF3FF; border:1px solid gray;");
	GridObj.setCellTextStyle(1, 16, "background-color: #EAF3FF; border:1px solid gray;");
	// GridObj.setRowTextStyle(12, "background-color: #CCCCFF; font-family: arial;");
	
	parent.parent.refreshResult(now_year);
	
	// if(status == "false") alert(msg);
	return true;
}

function calculateFooterValues(){
	var mp_tar = document.getElementById("mp_tar");
		mp_tar.innerHTML = sumColumnA(1);
	var mp_perf = document.getElementById("mp_perf");
		mp_perf.innerHTML = sumColumnA(2);
	var mp_diff = document.getElementById("mp_diff");
		mp_diff.innerHTML = sumColumnA(3);
	var mp_pg = document.getElementById("mp_pg");
		mp_pg.innerHTML = sumColumnA(4);
	var mm_tar = document.getElementById("mm_tar");
		mm_tar.innerHTML = sumColumnA(5);
	var mm_perf = document.getElementById("mm_perf");
		mm_perf.innerHTML = sumColumnA(6);
	var mm_diff = document.getElementById("mm_diff");
		mm_diff.innerHTML = sumColumnA(7);
	var mm_pg = document.getElementById("mm_pg");
		mm_pg.innerHTML = sumColumnA(8);
	var sa_tar = document.getElementById("sa_tar");
		sa_tar.innerHTML = sumColumnA(9);
	var sa_perf = document.getElementById("sa_perf");
		sa_perf.innerHTML = sumColumnA(10);
	var sa_diff = document.getElementById("sa_diff");
		sa_diff.innerHTML = sumColumnA(11);
	var sa_pg = document.getElementById("sa_pg");
		sa_pg.innerHTML = sumColumnA(12);
	var sp_tar = document.getElementById("sp_tar");
		sp_tar.innerHTML = sumColumnA(13);
	var sp_perf = document.getElementById("sp_perf");
		sp_perf.innerHTML = sumColumnA(14);
	var sp_diff = document.getElementById("sp_diff");
		sp_diff.innerHTML = sumColumnA(15);
	var sp_pg = document.getElementById("sp_pg");
		sp_pg.innerHTML = sumColumnA(16);
	return true;
}

function sumColumnA(ind){
	var out = 0;
	for(var i=1;i<=GridObj.getRowsNum();i++){
    	//if(GridObj.cells(i, GridObj.getColIndexById("SAL_AMOUNT_SON")).getValue() != 1) {
		out += parseFloat(GridObj.cells(i,ind).getValue());
    	//}
	}
	out = out.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return out;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();">
<!-- <form name="form" method="post">
<input type="file" name= "browseFile" style="display='none';">  

</form> -->
<div id="gridbox" name="gridbox" height="98%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=0"/>
</jsp:include> --%>
</body>
</html>
