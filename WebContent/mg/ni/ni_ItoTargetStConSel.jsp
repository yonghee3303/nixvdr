<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>ITO Target Standard Contact Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var month 			= "";

var now = new Date();

var now_year		= "";
var user_id			= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

//그리드폼
function setFormDraw() {	
	<%-- GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes();	--%>
	
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");

	/* GridObj.setHeader("고객사,사업명,2022년,#cspan,#cspan,#cspan,1사분기,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan");
	GridObj.attachHeader(["#rspan","#rspan","#rspan","#rspan","#rspan","#rspan","1","#cspan","#cspan","2","#cspan","#cspan","3","#cspan","#cspan","분기계","#cspan","#cspan"]); */
	GridObj.setHeader("고객사,사업명,올해,#cspan,#cspan,#cspan,1,#cspan,#cspan,2,#cspan,#cspan,3,#cspan,#cspan,분기계,#cspan,#cspan,4,#cspan,#cspan,5,#cspan,#cspan,6,#cspan,#cspan,분기계,#cspan,#cspan,7,#cspan,#cspan,8,#cspan,#cspan,9,#cspan,#cspan,분기계,#cspan,#cspan,10,#cspan,#cspan,11,#cspan,#cspan,12,#cspan,#cspan,분기계,#cspan,#cspan,C_CD")
	GridObj.attachHeader(["#rspan","#rspan","예상매출","이익","투입인원","년인력","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","투입인력","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","투입인력","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","투입인력","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","투입인력","#rspan"]);
	GridObj.setInitWidths("80,240,80,80,50,50,80,80,40,80,80,40,80,80,40,100,100,50,80,80,40,80,80,40,80,80,40,100,100,50,80,80,40,80,80,40,80,80,40,100,100,50,80,80,40,80,80,40,80,80,40,100,100,50,0");
	GridObj.setColAlign("center,left,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,center");
	GridObj.setColumnIds("CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_1Q,EXO_PRO_1Q,MANPOWER_1Q,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_2Q,EXO_PRO_2Q,MANPOWER_2Q,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_3Q,EXO_PRO_3Q,MANPOWER_3Q,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12,EXP_SAL_4Q,EXO_PRO_4Q,MANPOWER_4Q,CUSTOMER_CD");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,str");
	GridObj.setColTypes("ro,ro,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ro");
	/* doRequestUsingPOST_dhtmlxGrid(GridObj,"A1120","CUSTOMER_CD",""); */
	GridObj.setNumberFormat("0,000",2,".",",");
	GridObj.setNumberFormat("0,000",3,".",",");
	GridObj.setNumberFormat("0",4,".",",");
	GridObj.setNumberFormat("0",5,".",",");
	GridObj.setNumberFormat("0,000",6,".",",");
	GridObj.setNumberFormat("0,000",7,".",",");
	GridObj.setNumberFormat("0",8,".",",");
	GridObj.setNumberFormat("0,000",9,".",",");
	GridObj.setNumberFormat("0,000",10,".",",");
	GridObj.setNumberFormat("0",11,".",",");
	GridObj.setNumberFormat("0,000",12,".",",");
	GridObj.setNumberFormat("0,000",13,".",",");
	GridObj.setNumberFormat("0",14,".",",");
	GridObj.setNumberFormat("0,000",15,".",",");
	GridObj.setNumberFormat("0,000",16,".",",");
	GridObj.setNumberFormat("0",17,".",",");
	GridObj.setNumberFormat("0,000",18,".",",");
	GridObj.setNumberFormat("0,000",19,".",",");
	GridObj.setNumberFormat("0",20,".",",");
	GridObj.setNumberFormat("0,000",21,".",",");
	GridObj.setNumberFormat("0,000",22,".",",");
	GridObj.setNumberFormat("0",23,".",",");
	GridObj.setNumberFormat("0,000",24,".",",");
	GridObj.setNumberFormat("0,000",25,".",",");
	GridObj.setNumberFormat("0",26,".",",");
	GridObj.setNumberFormat("0,000",27,".",",");
	GridObj.setNumberFormat("0,000",28,".",",");
	GridObj.setNumberFormat("0",29,".",",");
	GridObj.setNumberFormat("0,000",30,".",",");
	GridObj.setNumberFormat("0,000",31,".",",");
	GridObj.setNumberFormat("0",32,".",",");
	GridObj.setNumberFormat("0,000",33,".",",");
	GridObj.setNumberFormat("0,000",34,".",",");
	GridObj.setNumberFormat("0",35,".",",");
	GridObj.setNumberFormat("0,000",36,".",",");
	GridObj.setNumberFormat("0,000",37,".",",");
	GridObj.setNumberFormat("0",38,".",",");
	GridObj.setNumberFormat("0,000",39,".",",");
	GridObj.setNumberFormat("0,000",40,".",",");
	GridObj.setNumberFormat("0",41,".",",");
	GridObj.setNumberFormat("0,000",42,".",",");
	GridObj.setNumberFormat("0,000",43,".",",");
	GridObj.setNumberFormat("0",44,".",",");
	GridObj.setNumberFormat("0,000",45,".",",");
	GridObj.setNumberFormat("0,000",46,".",",");
	GridObj.setNumberFormat("0",47,".",",");
	GridObj.setNumberFormat("0,000",48,".",",");
	GridObj.setNumberFormat("0,000",49,".",",");
	GridObj.setNumberFormat("0",50,".",",");
	GridObj.setNumberFormat("0,000",51,".",",");
	GridObj.setNumberFormat("0,000",52,".",",");
	GridObj.setNumberFormat("0",53,".",",");
	
	GridObj._in_header_stat_sum=function(tag,index,data){       
	    var calck=function(){
	        var sum=0;                                     
	        this.forEachRow(function(id){
	        		sum+=this.cellById(id,index).getValue()*1;
	        })
	        sum=sum.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	    	return this._aplNF(sum,0);
	    };
	    this._stat_in_header(tag,calck,index,data);
	}
	
	/* GridObj.attachFooter("합계,#cspan,<div id='last_exp_sal'>0</div>,<div id='last_exo_pro'>0</div>,<div id='last_in_manpower'>0</div>,<div id='last_manpower'>0</div>,<div id='exp_sal_1'>0</div>,<div id='exo_pro_1'>0</div>,<div id='manpower_1'>0</div>,<div id='exp_sal_2'>0</div>,<div id='exo_pro_2'>0</div>,<div id='manpower_2'>0</div>,<div id='exp_sal_3'>0</div>,<div id='exo_pro_3'>0</div>,<div id='manpower_3'>0</div>,<div id='exp_sal_1q'>0</div>,<div id='exo_pro_1q'>0</div>,<div id='manpower_1q'>0</div>,<div id='exp_sal_4'>0</div>,<div id='exo_pro_4'>0</div>,<div id='manpower_4'>0</div>,<div id='exp_sal_5'>0</div>,<div id='exo_pro_5'>0</div>,<div id='manpower_5'>0</div>,<div id='exp_sal_6'>0</div>,<div id='exo_pro_6'>0</div>,<div id='manpower_6'>0</div>,<div id='exp_sal_2q'>0</div>,<div id='exo_pro_2q'>0</div>,<div id='manpower_2q'>0</div>,<div id='exp_sal_7'>0</div>,<div id='exo_pro_7'>0</div>,<div id='manpower_7'>0</div>,<div id='exp_sal_8'>0</div>,<div id='exo_pro_8'>0</div>,<div id='manpower_8'>0</div>,<div id='exp_sal_9'>0</div>,<div id='exo_pro_9'>0</div>,<div id='manpower_9'>0</div>,<div id='exp_sal_3q'>0</div>,<div id='exo_pro_3q'>0</div>,<div id='manpower_3q'>0</div>,<div id='exp_sal_10'>0</div>,<div id='exo_pro_10'>0</div>,<div id='manpower_10'>0</div>,<div id='exp_sal_11'>0</div>,<div id='exo_pro_11'>0</div>,<div id='manpower_11'>0</div>,<div id='exp_sal_12'>0</div>,<div id='exo_pro_12'>0</div>,<div id='manpower_12'>0</div>,<div id='exp_sal_4q'>0</div>,<div id='exo_pro_4q'>0</div>,<div id='manpower_4q'>0</div>"
			,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;",
				"font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;",
				"font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;",
				"font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);
	 */
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);
	/* GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange); */
	

	GridObj.init();
	
	user_id = "s1024323";

	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	now_year = document.form.NOW_YEAR.value;
	
	var grid_col_id  = "CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_1Q,EXO_PRO_1Q,MANPOWER_1Q,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_2Q,EXO_PRO_2Q,MANPOWER_2Q,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_3Q,EXO_PRO_3Q,MANPOWER_3Q,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12,EXP_SAL_4Q,EXO_PRO_4Q,MANPOWER_4Q,CUSTOMER_CD";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
		argu += "&USER_ID="		+encodeURIComponent(user_id);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoTargetStConSelToSel&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = " <span class='point'>" + now_year+"</span> 년 ";

	//calculateFooterValues();
	for(var i=1;i<=GridObj.getRowsNum();i++) {
/* 	if(GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0150" || GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0250"
			|| GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0350" || GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0450"
			|| GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "1050"){
		GridObj.setRowTextStyle(i, "background-color: #CCCCFF; font-family: arial;");
		} */
	
	GridObj.setCellTextStyle(i, 15, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 16, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 17, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 27, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 28, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 29, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 39, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 40, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 41, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 51, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 52, "background-color: #D8E7FA; font-family: arial;");
	GridObj.setCellTextStyle(i, 53, "background-color: #D8E7FA; font-family: arial;");
	
	if(GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() != "합계" && GridObj.cells(i, GridObj.getColIndexById("BUSINESS_NAME")).getValue() == "소계"){
		GridObj.cells(i, GridObj.getColIndexById("CUSTOMER")).setValue("");
		GridObj.setRowTextStyle(i, "background-color: #D8E7FA; font-family: arial;");
		}
	if(GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "합계" && GridObj.cells(i, GridObj.getColIndexById("BUSINESS_NAME")).getValue() == "소계"){
		GridObj.cells(i, GridObj.getColIndexById("CUSTOMER")).setValue("합계");
		GridObj.cells(i, GridObj.getColIndexById("BUSINESS_NAME")).setValue("");
		GridObj.setRowTextStyle(i, "background-color: #CCCCCC; font-family: arial;");
		}
	}
	
	if(status == "false") alert(msg);
	return true;
}

function calculateFooterValues(){
	var last_exp_sal = document.getElementById("last_exp_sal");
		last_exp_sal.innerHTML = sumColumnA(2);
	var last_exo_pro = document.getElementById("last_exo_pro");
		last_exo_pro.innerHTML = sumColumnA(3);
	var last_in_manpower = document.getElementById("last_in_manpower");
		last_in_manpower.innerHTML = sumColumnA(4);
	var last_manpower = document.getElementById("last_manpower");
		last_manpower.innerHTML = sumColumnA(5);
	var exp_sal_1 = document.getElementById("exp_sal_1");
		exp_sal_1.innerHTML = sumColumnA(6);
	var exo_pro_1 = document.getElementById("exo_pro_1");
		exo_pro_1.innerHTML = sumColumnA(7);
	var manpower_1 = document.getElementById("manpower_1");
		manpower_1.innerHTML = sumColumnA(8);
	var exp_sal_2 = document.getElementById("exp_sal_2");
		exp_sal_2.innerHTML = sumColumnA(9);
	var exo_pro_2 = document.getElementById("exo_pro_2");
		exo_pro_2.innerHTML = sumColumnA(10);
	var manpower_2 = document.getElementById("manpower_2");
		manpower_2.innerHTML = sumColumnA(11);
	var exp_sal_3 = document.getElementById("exp_sal_3");
		exp_sal_3.innerHTML = sumColumnA(12);
	var exo_pro_3 = document.getElementById("exo_pro_3");
		exo_pro_3.innerHTML = sumColumnA(13);
	var manpower_3 = document.getElementById("manpower_3");
		manpower_3.innerHTML = sumColumnA(14);
	var exp_sal_1q = document.getElementById("exp_sal_1q");
		exp_sal_1q.innerHTML = sumColumnA(15);
	var exo_pro_1q = document.getElementById("exo_pro_1q");
		exo_pro_1q.innerHTML = sumColumnA(16);
	var manpower_1q = document.getElementById("manpower_1q");
		manpower_1q.innerHTML = sumColumnA(17);
	var exp_sal_4 = document.getElementById("exp_sal_4");
		exp_sal_4.innerHTML = sumColumnA(18);
	var exo_pro_4 = document.getElementById("exo_pro_4");
		exo_pro_4.innerHTML = sumColumnA(19);
	var manpower_4 = document.getElementById("manpower_4");
		manpower_4.innerHTML = sumColumnA(20);
	var exp_sal_5 = document.getElementById("exp_sal_5");
		exp_sal_5.innerHTML = sumColumnA(21);
	var exo_pro_5 = document.getElementById("exo_pro_5");
		exo_pro_5.innerHTML = sumColumnA(22);
	var manpower_5 = document.getElementById("manpower_5");
		manpower_5.innerHTML = sumColumnA(23);
	var exp_sal_6 = document.getElementById("exp_sal_6");
		exp_sal_6.innerHTML = sumColumnA(24);
	var exp_sal_6 = document.getElementById("exo_pro_6");
		exp_sal_6.innerHTML = sumColumnA(25);
	var manpower_6 = document.getElementById("manpower_6");
		manpower_6.innerHTML = sumColumnA(26);
	var exp_sal_2q = document.getElementById("exp_sal_2q");
		exp_sal_2q.innerHTML = sumColumnA(27);
	var exo_pro_2q = document.getElementById("exo_pro_2q");
		exo_pro_2q.innerHTML = sumColumnA(28);
	var manpower_2q = document.getElementById("manpower_2q");
		manpower_2q.innerHTML = sumColumnA(29);
	var exp_sal_7 = document.getElementById("exp_sal_7");
		exp_sal_7.innerHTML = sumColumnA(30);
	var exo_pro_7 = document.getElementById("exo_pro_7");
		exo_pro_7.innerHTML = sumColumnA(31);
	var manpower_7 = document.getElementById("manpower_7");
		manpower_7.innerHTML = sumColumnA(32);
	var exp_sal_8 = document.getElementById("exp_sal_8");
		exp_sal_8.innerHTML = sumColumnA(33);
	var exo_pro_8 = document.getElementById("exo_pro_8");
		exo_pro_8.innerHTML = sumColumnA(34);
	var manpower_8 = document.getElementById("manpower_8");
		manpower_8.innerHTML = sumColumnA(35);
	var exp_sal_9 = document.getElementById("exp_sal_9");
		exp_sal_9.innerHTML = sumColumnA(36);
	var exo_pro_9 = document.getElementById("exo_pro_9");
		exo_pro_9.innerHTML = sumColumnA(37);
	var manpower_9 = document.getElementById("manpower_9");
		manpower_9.innerHTML = sumColumnA(38);
	var exp_sal_3q = document.getElementById("exp_sal_3q");
		exp_sal_3q.innerHTML = sumColumnA(39);
	var exo_pro_3q = document.getElementById("exo_pro_3q");
		exo_pro_3q.innerHTML = sumColumnA(40);
	var manpower_3q = document.getElementById("manpower_3q");
		manpower_3q.innerHTML = sumColumnA(41);
	var exp_sal_10 = document.getElementById("exp_sal_10");
		exp_sal_10.innerHTML = sumColumnA(42);
	var exo_pro_10 = document.getElementById("exo_pro_10");
		exo_pro_10.innerHTML = sumColumnA(43);
	var manpower_10 = document.getElementById("manpower_10");
		manpower_10.innerHTML = sumColumnA(44);
	var exp_sal_11 = document.getElementById("exp_sal_11");
		exp_sal_11.innerHTML = sumColumnA(45);
	var exo_pro_11 = document.getElementById("exo_pro_11");
		exo_pro_11.innerHTML = sumColumnA(46);
	var manpower_11 = document.getElementById("manpower_11");
		manpower_11.innerHTML = sumColumnA(47);
	var exp_sal_12 = document.getElementById("exp_sal_12");
		exp_sal_12.innerHTML = sumColumnA(48);
	var exo_pro_12 = document.getElementById("exo_pro_12");
		exo_pro_12.innerHTML = sumColumnA(49);
	var manpower_12 = document.getElementById("manpower_12");
		manpower_12.innerHTML = sumColumnA(50);
	var exp_sal_4q = document.getElementById("exp_sal_4q");
		exp_sal_4q.innerHTML = sumColumnA(51);
	var exo_pro_4q = document.getElementById("exo_pro_4q");
		exo_pro_4q.innerHTML = sumColumnA(52);
	var manpower_4q = document.getElementById("manpower_4q");
		manpower_4q.innerHTML = sumColumnA(53);
		
	return true;
}

function sumColumnA(ind){
	var out = 0;
	for(var i=1;i<=GridObj.getRowsNum();i++){
		if(GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0100" || GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0200"
				|| GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0300" || GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "0400"
				|| GridObj.cells(i, GridObj.getColIndexById("CUSTOMER_CD")).getValue() == "1000") {
		out += parseFloat(GridObj.cells(i,ind).getValue());
    	}
	}
	out = out.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return out;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function fnCalCount(type, ths){
    var $input = $(ths).parents("td").find("input[name='NOW_YEAR']");
    var tCount = Number($input.val());
    
    if(type=='p'){
        $input.val(Number(tCount)+1);
        
    }else{
        $input.val(Number(tCount)-1);    
    }
}

$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};

		$('input[name="nowpage"]').change(function() {
	        var value = $(this).val();
			if(value==1) {
				parent.toModify();
			}
	    });
		
		$('input[name="selectuser"]').change(function() {
	        var value = $(this).val();
			if(value=="s1024323") {
				user_id = "s1024323";
			}
			if(value=="redeyesss") {
				user_id = "redeyesss";
			}
			if(value=="daeho") {
				user_id = "daeho";
			}
			if(value=="all") {
				user_id = "all";
			}
			doQuery();
	    });
	});

function init() {
	document.getElementById('NOW_YEAR').value = new Date().toISOString().substring(0, 4);
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="60px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">조회화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0" checked="checked">
			</td>
			<td></td>
			<td class="tit">수정화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1">
			</td>
			<td></td>
        	<td class="tit">날짜</td>
            <td colspan="3">
            	<button type="button" onclick="fnCalCount('m',this);" style="padding: 3px 7px 0px 7px;">-</button>
            	<input type="text" name="NOW_YEAR" id="NOW_YEAR" size="7" maxlength="4" style="padding: 5px 0 0 0;" readonly>
            	<button type="button" onclick="fnCalCount('p',this);" style="padding: 3px 6px 0px 6px;">+</button>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
        	<td class="tit">손병희</td>
	        <td>
	        	<input type="radio" name="selectuser" value="s1024323" checked="checked">
			</td>
			<td></td>
			<td class="tit">곽성찬</td>
	        <td>
	        	<input type="radio" name="selectuser" value="redeyesss">
			</td>
			<td></td>
			<td class="tit">고대호</td>
	        <td>
	        	<input type="radio" name="selectuser" value="daeho">
			</td>
			<td></td>
			<td class="tit">합계</td>
	        <td>
	        	<input type="radio" name="selectuser" value="all">
			</td>
			<td></td>
			<td></td>
			<td></td>
        </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doQuery();" class="btn"><span>조회</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="77%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>