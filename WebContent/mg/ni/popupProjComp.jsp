<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<% 
IUser s_user = (IUser) session.getAttribute("j_user");
String type= StringUtils.paramReplace(request.getParameter("type"));
String retfunc= StringUtils.paramReplace(request.getParameter("retfunc"));
String roll= StringUtils.paramReplace(request.getParameter("roll"));
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>수행사 선택</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var agency_id		= "";
var agency_nm		= "";


var G_SERVLETURL = "/ni/proj.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setHeader("<%=mm.getMessage("NIMP_1012", s_user.getLocale())%>,<%=mm.getMessage("NIMP_1011", s_user.getLocale())%>");
	GridObj.setInitWidths("260,260");
	GridObj.setColAlign("center,center");
	GridObj.setColumnIds("COMP_NM,COMP_ID");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str");
	GridObj.setColTypes("ro,ro");
	GridObj.attachEvent("onRowDblClicked",doOk);

	GridObj.init();
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId,cellInd) {
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var comp_nm = document.form.COMP_NM.value;
	var comp_id = document.form.COMP_ID.value;
	
	var grid_col_id = "COMP_NM,COMP_ID";
	var argu = "&COMP_NM=" +encodeURIComponent(comp_nm);
		argu += "&COMP_ID=" +encodeURIComponent(comp_id);

	GridObj.loadXML(G_SERVLETURL+"?mod=compPop&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doOk()
{
	row_id = GridObj.getSelectedRowId();
	
	var comp_nm = GridObj.cells(row_id, GridObj.getColIndexById("COMP_NM")).getValue();
	var comp_id = GridObj.cells(row_id, GridObj.getColIndexById("COMP_ID")).getValue();
	
	opener.<%=retfunc%>(comp_nm, comp_id);
	self.close();
}

function doCancel()
{
	window.close();
}

function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>
</head>
<body onload="init(); loadCombobox();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1><%=mm.getMessage("NIMP_1012", s_user.getLocale())%></h1>
    </td>
  </tr>
</table>
<table width="100%">
<tr>
   	<td>
        <table class="board-search">
        <colgroup>
            <col width="100px" />
            <col width="100px" />
            <col width="100px" />
            <col width="100px" />          
            <col width="100px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("NIMP_1012", s_user.getLocale())%></td>
			<td>
				<input type="text" name="COMP_NM" id="COMP_NM" size="16" class="text">
			</td>
        	<td class="tit"><%=mm.getMessage("NIMP_1011", s_user.getLocale())%></td>
			<td>
				<input type="text" name="COMP_ID" id="COMP_ID" size="16" class="text">
			</td>
            <td rowspan="2">
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" height="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=300"/>
</jsp:include>
</body>
</html>
