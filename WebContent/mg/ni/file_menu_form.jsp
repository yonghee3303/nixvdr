<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.List"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="mg.vdr.explorerAction"%>

<%@ include file="../../ext/include/su_grid_common.jsp"%>




<%
	IMenuManagement menus = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IUser user = (IUser) session.getAttribute("j_user");
	explorerAction exAction = new explorerAction();
	 
	/* String eframe = StringUtils.paramReplace(request.getParameter("eventframe")); */
	
	String eframe = "cell2";
	
	String contextPath = request.getContextPath();
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
%>

<HTML>
<HEAD>
<link rel="stylesheet" href="../../ext/css/dtree.css" type="text/css">
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/dtree.js"></script>

</HEAD>
<BODY onload="build();" style="overflow:hidden">
<table width="100%">
	<tr>
		<td>
			<div class="btnarea">
				<div id="toDisplay" style="display:none;">
				<a href="javascript:folderAuthSet();"><img src="/img/ni_auth.png" align="right"/></a>
				<a href="javascript:folderDelete();"><img src="/img/ni_deleteFolder.png" align="right"/></a>
				<a href="javascript:folderEdit();"><img src="/img/ni_editFolder.png" align="right"/></a>
				</div>
				<a href="javascript:folderAdd();"><img src="/img/ni_newFolder.png" align="right"/></a>
			</div>
		</td>
	</tr>
</table>
<div >
	<div id="menu_tree"></div>
</div>
<div id="gridbox" name="gridbox" width="0%" height="0%" style="background-color:white;overflow:hidden"></div>
<script>
	var check_menu_id = "";
	var menu_nm = "";
	var myDataProcessor = {};
	var setDisAuth = "0";
	var downAuth = "";
	var fileAuth = "";

	var tree = new dhtmlXTreeObject("menu_tree","100%","100%",0);
	tree.setSkin('dhx_skyblue');
	tree.setImagePath("../../dhtmlx_v403/skins/skyblue/imgs/dhxtree_skyblue/");
	tree.enableDragAndDrop(false);
	tree.setOnClickHandler(function(id){
		//alert("id : "+id+" was selected\n menupath : "+tree.getUserData(id,"menuPath"));
		check_menu_id = id;
		menu_nm = tree.getSelectedItemText();
		
		doQuery();
		
		<%-- parent.<%=eframe%>.menuClick(id,tree.getParentId(id),tree.getUserData(id,"menuPath")); getItemText(id)--%>
		parent.<%=eframe%>.getFrame().contentWindow.menuClick(id,tree.getParentId(id),tree.getUserData(id,"menuPath"),downAuth,fileAuth);
	});
	
function doSettingDisplay() {
	var con = document.getElementById("toDisplay");
	if (setDisAuth == "0") {
		con.style.display = "none";
	} else {
		con.style.display = "block";
	}
}

function doQuery() {
	check_menu_id;
	
	var grid_col_id = "SELECTED,FOLDER_NM,FOLDER_ID,P_FOLDER_ID";
	var argu = "&CHECK_P_ID=" +encodeURIComponent(check_menu_id);

	GridObj.loadXML("/mg/Upload.do?mod=searchNimsFolder&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");

	if(status == "1") {
		setDisAuth = "1";
		if(msg == "11"){
			downAuth = "1";
			fileAuth = "1";
		} else if(msg == "10") {
			downAuth = "1";
			fileAuth = "0";
		} else if(msg == "00") {
			downAuth = "0";
			fileAuth = "0";
		} else {
		}
		doSettingDisplay();
		parent.<%=eframe%>.getFrame().contentWindow.menuClickAfterAuth(downAuth,fileAuth);
	} else {
		setDisAuth = "0";
		if(msg == "11"){
			downAuth = "1";
			fileAuth = "1";
		} else if(msg == "10") {
			downAuth = "1";
			fileAuth = "0";
		} else if(msg == "00") {
			downAuth = "0";
			fileAuth = "0";
		} else {
		}
		doSettingDisplay();
		parent.<%=eframe%>.getFrame().contentWindow.menuClickAfterAuth(downAuth,fileAuth);
	}
	return true;
}
	
function folderSet(){
	if(check_menu_id == ""){
		alert("설정 할 폴더를 선택해주세요.");
		return;
	}
	
	var url = "/mg/ni/popupFolderSetting.jsp"
		popupNewAddBoard(url, "폴더 설정", 200, 700);
}

function folderAdd(){
	if(check_menu_id == ""){
		alert("폴더를 선택해주세요.");
		return;
	}
	
	var url = "/mg/ni/popupFolderAdd.jsp"
		popupNewAddBoard(url, "폴더 생성", 135, 510);
}

function folderEdit(){
	if(check_menu_id == ""){
		alert("폴더를 선택해주세요.");
		return;
	} else if(check_menu_id == "F2021112513423531055621054150") {
		alert("최상위 폴더는 수정 할 수 없습니다.");
		return;
	} else {
	
	var url = "/mg/ni/popupFolderEdit.jsp"
		popupNewAddBoard(url, "폴더 수정", 135, 500);
	}
}

function folderDelete(){
 	if(check_menu_id == ""){
		alert("폴더를 선택해주세요.");
		return;
	} else if(check_menu_id == "F2021112513423531055621054150") {
		alert("최상위 폴더는 삭제 할 수 없습니다.");
		return;
	} else {
 	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("폴더를 삭제하시겠습니까?")) {
		var cols_ids = "SELECTED,FOLDER_NM,FOLDER_ID,P_FOLDER_ID";
		var argu = "&FOLDER_ID=" +encodeURIComponent(check_menu_id);
		var SERVLETURL = "/vdr/explorer.do?mod=NimsouDeleteFolder&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionDelete(GridObj, myDataProcessor, "SELECTED", grid_array);
		location.reload();
	}
	}
	
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		//parent.refresh();		
		location.reload();
	} else {
		alert(messsage);
	}
	return false;
}

function folderAuthSet(){
	if(check_menu_id == ""){
		alert("폴더를 선택해주세요.");
		return;
	} else if(check_menu_id == "F2021112513423531055621054150") {
		alert("최상위 폴더는 접근 할 수 없습니다.");
		return;
	} else {
	
	var url = "/mg/ni/popupFolderAuthSet.jsp"
		popupNewAddBoard(url, "권한 수정", 380, 592);
	}
}

function popupNewAddBoard(url, title, height, width) {
    //화면 가운데로 배치
    var top = 100;
    var left = 100;

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
    
	var ret = window.open(url, title, 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function CenterWindow(height,width)
{
	var outx = screen.height;
	var outy = screen.width;
	var x = (outx - height)/2;
	var y = (outy - width)/2;
	dim = new Array(2);
	dim[0] = x;
	dim[1] = y;

	return  dim;
}

 function build(){
	 GridObj = new dhtmlXGridObject('gridbox');
		GridObj.setSkin("dhx_skyblue");
		GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
		GridObj.setHeader("#master_checkbox,폴더 명,FI,PI");
		GridObj.setInitWidths("50,300,0,0");
		GridObj.setColAlign("center,center,center,center");
		GridObj.setColumnIds("SELECTED,FOLDER_NM,FOLDER_ID,P_FOLDER_ID");
		GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
		GridObj.setColSorting("str,str,str,str");
		GridObj.setColTypes("ch,edtxt,ro,ro");
		GridObj.attachEvent("onXLE",doQueryEnd);
	 GridObj.init();
	 
		tree.deleteChildItems(0);
		
		 <%
			String mcode = "";
			List menuList = exAction.NimsouGetFolderTreeS(user);
			System.out.print(menuList);
			%>
			 
			<% //상위 객체부터 만들어야 하위 메뉴를 붙일 수 있기 때문에 
			for (Iterator all = menuList.iterator(); all.hasNext();) {
		        Map each = (Map) all.next();
		        String menu_id = each.get("FOLDER_ID").toString();
		        String menu_nm = each.get("FOLDER_NM").toString();
		        String p_menu_id = each.get("P_FOLDER_ID").toString();
		        String menuPath = /* each.get("FOLDER_PATH").toString() */"0";
		       // String sts = each.get("TYPE").toString();
		        
		        if("0".equals(p_menu_id)){
			%>
			
					//void insertNewChild(mixed parentId,mixed id,string text,function actionHandler,string image1,string image2,string image3,string optionStr,mixed children);
					tree.insertNewChild(0,"<%=menu_id%>","<%=menu_nm%>");
					tree.setUserData("<%=menu_id%>","menuPath","<%=menuPath%>");
			<%		
		        }else{	
			%>		
					//void insertNewChild(mixed parentId,mixed id,string text,function actionHandler,string image1,string image2,string image3,string optionStr,mixed children);
					tree.insertNewChild("<%=p_menu_id%>","<%=menu_id%>","<%=menu_nm%>");
					tree.setUserData("<%=menu_id%>","menuPath","<%=menuPath%>"); 
			<%		
		        }
			}
		%>
		
		tree.closeAllItems(0);	
}
</script>
</BODY>
</HTML>