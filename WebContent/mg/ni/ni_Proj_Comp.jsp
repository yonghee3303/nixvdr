<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>Comp Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var comp_id			="";
var comp_nm			="";
var comp_memo		="";
var addr			="";
var bus_reg_no		="";
var owner			="";
var tel_no			="";
var use_yn			="";
var p_comp_id		="";
var p_comp_nm		="";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/comp.do";

//그리드폼
function setFormDraw() {
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	GridObj.attachEvent("onRowDblClicked",doRevPop);
	GridObj.setSizes();
	
	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	var comp_nm = document.form.COMP_NM.value;

	var grid_col_id  = "<%=grid_col_id%>";
	var argu  = "&COMP_NM="	+encodeURIComponent(comp_nm);
	GridObj.loadXML(G_SERVLETURL+"?mod=compSelectUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

//수정 버튼 기능
function doRevPop() {
	if(!rev_check) {
		alert("수정할 행을 선택해주세요.");
		return;
	}

	if(!checkRowsFRev()) {
		alert("수정시엔 모든 체크박스를 해제해주세요.");
		return;
	}
	var url = "/mg/ni/popupCompRev.jsp"
	popupNewAddBoard(url, "정보 수정", 420, 654);
}

//수정 팝업창 기능
function getRevPopupData() {
	doRevAddRow();
}

//수정 데이터
function getParentData() {
	row_id = GridObj.getSelectedRowId();
	
   	comp_id = GridObj.cells(row_id, GridObj.getColIndexById("COMP_ID")).getValue();
	comp_nm = GridObj.cells(row_id, GridObj.getColIndexById("COMP_NM")).getValue();
	comp_memo = GridObj.cells(row_id, GridObj.getColIndexById("COMP_MEMO")).getValue();
	addr = GridObj.cells(row_id, GridObj.getColIndexById("ADDR")).getValue();
	bus_reg_no = GridObj.cells(row_id, GridObj.getColIndexById("BUS_REG_NO")).getValue();
	owner = GridObj.cells(row_id, GridObj.getColIndexById("OWNER")).getValue();
	tel_no = GridObj.cells(row_id, GridObj.getColIndexById("TEL_NO")).getValue();
	use_yn = GridObj.cells(row_id, GridObj.getColIndexById("USE_YN")).getValue();
	p_comp_id = GridObj.cells(row_id, GridObj.getColIndexById("P_COMP_ID")).getValue();
	p_comp_nm = GridObj.cells(row_id, GridObj.getColIndexById("P_COMP_NM")).getValue();
	
	GridObj.cells(row_id, GridObj.getColIndexById("SELECTED")).setValue("0");
}

//수정시 체크 해제
function checkRowsFRev() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return false;
	}
	return true;
}

//데이터 수정
function doRevAddRow() {
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
   	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_ID")).setValue(comp_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_NM")).setValue(comp_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_MEMO")).setValue(comp_memo);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADDR")).setValue(addr);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BUS_REG_NO")).setValue(bus_reg_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("OWNER")).setValue(owner);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TEL_NO")).setValue(tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USE_YN")).setValue(use_yn);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("P_COMP_ID")).setValue(p_comp_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("P_COMP_NM")).setValue(p_comp_nm);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doRevSave();
}

//데이터 수정sql
function doRevSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
	    var SERVLETURL = G_SERVLETURL + "?mod=saveComp&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

//신규 버튼 기능
function doAddNewPop() {
	var url = "/mg/ni/popupCompNewAdd.jsp"
	popupNewAddBoard(url, "신규 생성", 420, 654);
}

//신규 팝업창 기능
function getPopupData() {
	doAddRow();
}

//신규 데이터
function doAddRow() {

	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
   	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_ID")).setValue(comp_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_NM")).setValue(comp_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_MEMO")).setValue(comp_memo);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADDR")).setValue(addr);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BUS_REG_NO")).setValue(bus_reg_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("OWNER")).setValue(owner);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TEL_NO")).setValue(tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USE_YN")).setValue(use_yn);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("P_COMP_ID")).setValue(p_comp_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("P_COMP_NM")).setValue(p_comp_nm);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doAddSave();
}

//신규 데이터 sql
function doAddSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
    	var SERVLETURL = G_SERVLETURL + "?mod=saveNewComp&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {;		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

//삭제 버튼 기능
function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) {

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteComp&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function init() {
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="100px" />
            <col width="100px" />
            <col width="100px" />
            <col width="100px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("NIMG_1011", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="COMP_NM"  size="10" class="text">
            </td>
            <td>
            	<input style="visibility:hidden" type="text" name="BLAN_A"  size="0" class="text">
            </td>
            <td></td>
            <td></td>
        </tr>
		</table>
		
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doAddNewPop();" class="btn"><span><%=mm.getMessage("NIMG_1001", s_user.getLocale())%></span></a>
	            	<a href="javascript:doRevPop();" class="btn"><span><%=mm.getMessage("NIMG_1002", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("NIMG_1003", s_user.getLocale())%></span></a>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMG_1004", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="91%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>