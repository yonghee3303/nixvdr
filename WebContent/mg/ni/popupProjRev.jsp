<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>프로젝트 수정</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum;

var G_SERVLETURL = "<%=contextPath%>/ni/proj.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	opener.getParentData();
	
	document.getElementById('PROJ_NO').value = opener.proj_no;
	document.getElementById('PROJ_NM').value = opener.proj_nm;
	document.getElementById('COMP_NM').value = opener.comp_nm;
	document.getElementById('START_YMD').value = opener.start_ymd;
	document.getElementById('END_YMD').value = opener.end_ymd;
	var pj_user_nm = opener.pj_user_nm;
	if ("손병희" == pj_user_nm){
		pj_user_nm = "s1024323"
	} else if ("곽성찬" == pj_user_nm) {
		pj_user_nm = "redeyesss"
	} else if ("배준하" == pj_user_nm) {
		pj_user_nm = "junyugi"
	} else if ("고대호" == pj_user_nm) {
		pj_user_nm = "daeho"
	} else if ("고봉수" == pj_user_nm) {
		pj_user_nm = "bskosk"
	} else {
		pj_user_nm = null
	}
	document.form.PJ_USER_NM.value = pj_user_nm
	document.getElementById('PJ_TEL_NO').value = opener.pj_tel_no;
	document.getElementById('REQ_USER_NM').value = opener.req_user_nm;
	document.getElementById('REQ_USER_TEL_NO').value = opener.req_user_tel_no;
	document.getElementById('REQ_USER_EMAIL').value = opener.req_user_email;
	document.form.PROJ_IMP.value = opener.proj_imp;
	document.form.PROJ_EMER.value = opener.proj_emer;
	var indus_cl_cd = opener.indus_cl_cd;
	if ("일반" == indus_cl_cd){
		indus_cl_cd = "1"
	} else if ("금융" == indus_cl_cd) {
		indus_cl_cd = "2"
	} else if ("공공" == indus_cl_cd) {
		indus_cl_cd = "3"
	} else if ("그외" == indus_cl_cd) {
		indus_cl_cd = "4"
	} else if ("공공/금융" == indus_cl_cd) {
		indus_cl_cd = "5"
	} else {
		indus_cl_cd = null
	}
	document.form.INDUS_CL_CD.value = indus_cl_cd;
	var proj_cl_cd = opener.proj_cl_cd;
	if ("인프라" == proj_cl_cd){
		proj_cl_cd = "1"
	} else if ("SM" == proj_cl_cd) {
		proj_cl_cd = "2"
	} else if ("SI" == proj_cl_cd) {
		proj_cl_cd = "3"
	} else if ("기타" == proj_cl_cd) {
		proj_cl_cd = "4"
	} else {
		proj_cl_cd = null
	}
	document.form.PROJ_CL_CD.value = proj_cl_cd;
	document.getElementById('ESSEN_TECH_DESC').value = opener.essen_tech_desc;
	var pj_remark = opener.pj_remark;
	pj_remark = pj_remark.replaceAll("&amp;","\&");
	pj_remark = pj_remark.replaceAll("&lt;","\<");
	pj_remark = pj_remark.replaceAll("&gt;","\>");
	pj_remark = pj_remark.replaceAll("&apos;","\'");
	pj_remark = pj_remark.replaceAll("&quot;","\"");
	document.getElementById('PJ_REMARK').value = pj_remark;
	var requ_cnt_pre = opener.requ_cnt_pre;
	requ_cnt_pre = requ_cnt_pre.replaceAll("&amp;","\&");
	requ_cnt_pre = requ_cnt_pre.replaceAll("&lt;","\<");
	requ_cnt_pre = requ_cnt_pre.replaceAll("&gt;","\>");
	requ_cnt_pre = requ_cnt_pre.replaceAll("&apos;","\'");
	requ_cnt_pre = requ_cnt_pre.replaceAll("&quot;","\"");
	document.getElementById('REQU_CNT_PRE').value = requ_cnt_pre;
	document.getElementById('PJ_ETC').value = opener.pj_etc;
	var pj_area_cd = opener.pj_area_cd;
	if ("서울특별시" == pj_area_cd){
		pj_area_cd = "1"
	} else if ("부산광역시" == pj_area_cd) {
		pj_area_cd = "2"
	} else if ("대구광역시" == pj_area_cd) {
		pj_area_cd = "3"
	} else if ("인천광역시" == pj_area_cd) {
		pj_area_cd = "4"
	} else if ("광주광역시" == pj_area_cd) {
		pj_area_cd = "5"
	} else if ("대전광역시" == pj_area_cd) {
		pj_area_cd = "6"
	} else if ("울산광역시" == pj_area_cd) {
		pj_area_cd = "7"
	} else if ("세종특별자치시" == pj_area_cd) {
		pj_area_cd = "8"
	} else if ("경기도" == pj_area_cd) {
		pj_area_cd = "9"
	} else if ("강원도" == pj_area_cd) {
		pj_area_cd = "10"
	} else if ("충청북도" == pj_area_cd) {
		pj_area_cd = "11"
	} else if ("충청남도" == pj_area_cd) {
		pj_area_cd = "12"
	} else if ("전라북도" == pj_area_cd) {
		pj_area_cd = "13"
	} else if ("전라남도" == pj_area_cd) {
		pj_area_cd = "14"
	} else if ("경상북도" == pj_area_cd) {
		pj_area_cd = "15"
	} else if ("경상남도" == pj_area_cd) {
		pj_area_cd = "16"
	} else if ("제주특별자치도" == pj_area_cd) {
		pj_area_cd = "17"
	} else {
		pj_area_cd = null
	}
	document.form.PJ_AREA_CD.value = pj_area_cd;
	document.getElementById('ORDER_YMD').value = opener.order_ymd;
	document.getElementById('ORDER_END_YMD').value = opener.order_end_ymd;
	document.getElementById('PJ_AREA_SUB').value = opener.pj_area_sub;
	document.getElementById('COMP_ID').value = opener.comp_id;
	document.getElementById('COMP_NM').value = opener.comp_nm;
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doRev()
{
	var proj_no = document.getElementById('PROJ_NO').value;
	var proj_nm = document.getElementById('PROJ_NM').value;
	var start_ymdn = document.getElementById('START_YMD').value;
	var start_ymd = start_ymdn.replace(/\-/g, "");
	var end_ymdn = document.getElementById('END_YMD').value;
	var end_ymd = end_ymdn.replace(/\-/g, "");
	var pj_user_nm = RTrim(document.form.PJ_USER_NM.value);
	var pj_tel_no = document.getElementById('PJ_TEL_NO').value;
	var req_user_nm = document.getElementById('REQ_USER_NM').value;
	var req_user_tel_no = document.getElementById('REQ_USER_TEL_NO').value;
	var req_user_email = document.getElementById('REQ_USER_EMAIL').value;
	var proj_imp = RTrim(document.form.PROJ_IMP.value);
	var proj_emer = RTrim(document.form.PROJ_EMER.value);
	var indus_cl_cd = RTrim(document.form.INDUS_CL_CD.value);
	var proj_cl_cd = RTrim(document.form.PROJ_CL_CD.value);
	var order_ymdn = document.getElementById('ORDER_YMD').value;
	var order_ymd = order_ymdn.replace(/\-/g, "");
	var order_end_ymdn = document.getElementById('ORDER_END_YMD').value;
	var order_end_ymd = order_end_ymdn.replace(/\-/g, "");
	var pj_area_cd = RTrim(document.form.PJ_AREA_CD.value);
	var pj_area_sub = document.getElementById('PJ_AREA_SUB').value;
	var essen_tech_desc = document.getElementById('ESSEN_TECH_DESC').value;
	var pj_remark = document.getElementById('PJ_REMARK').value;
	var requ_cnt_pre = document.getElementById('REQU_CNT_PRE').value;
	var pj_etc = document.getElementById('PJ_ETC').value;
	var comp_nm = document.getElementById('COMP_NM').value;	
	var comp_id = document.getElementById('COMP_ID').value;	
	
	if (proj_nm == "") {
		alert("프로젝트명을 입력해주세요.");
		return;
	} else if (pj_user_nm == "") {
		alert("담당자를 입력해주세요.");
		return;
	} else if (order_ymd == "") {
		alert("의뢰일자를 선택해주세요.");
		return;
	} else if (comp_nm == "") {
		alert("수행사를 선택해주세요.");
		return;
	} else {
		opener.proj_no = proj_no;
		opener.proj_nm = proj_nm;
		opener.start_ymd = start_ymd;
		opener.end_ymd = end_ymd;
		opener.pj_user_nm = pj_user_nm;
		opener.pj_tel_no = pj_tel_no;
		opener.req_user_nm = req_user_nm;
		opener.req_user_tel_no = req_user_tel_no;
		opener.req_user_email = req_user_email;
		opener.proj_imp = proj_imp;
		opener.proj_emer = proj_emer;
		opener.indus_cl_cd = indus_cl_cd;
		opener.proj_cl_cd = proj_cl_cd;
		opener.order_ymd = order_ymd;
		opener.order_end_ymd = order_end_ymd;
		opener.pj_area_cd = pj_area_cd;
		opener.pj_area_sub = pj_area_sub;
		opener.essen_tech_desc = essen_tech_desc;
		opener.pj_remark = pj_remark;
		opener.requ_cnt_pre = requ_cnt_pre;
		opener.pj_etc = pj_etc;
		opener.comp_nm = comp_nm;
		opener.comp_id = comp_id;

		opener.getRevPopupData();
		
		window.close();
	}
}

function doAddEnd()
{
}

//전화번호 자동 하이픈 입력
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 4) {
        return number;
    } else if(number.length < 7) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3);
    } else if(number.length == 9) {
        phone += number.substr(0, 2);
        phone += "-";
        phone += number.substr(2, 3);
        phone += "-";
        phone += number.substr(5);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

//수행사 선택 팝업
function doProjCompPop() {
	popupProjComp("<%=mm.getMessage("NIMG_1014", s_user.getLocale())%>", '', '', 540, 400, "plant", "OkPopupProjComp", <%=pm.getString("component.contextPath.root")%>);
}

function OkPopupProjComp(comp_nm, comp_id) {
	document.getElementById('COMP_NM').value = comp_nm;
	document.getElementById('COMP_ID').value = comp_id;	
}

//달력
function init() {
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"START_YMD",button:"calendar_icon"});
	calendar2 = new dhtmlXCalendarObject({input:"END_YMD",button:"calendar_icon2"});
	calendar3 = new dhtmlXCalendarObject({input:"ORDER_YMD",button:"calendar_icon3"});
	calendar4 = new dhtmlXCalendarObject({input:"ORDER_END_YMD",button:"calendar_icon4"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar3.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar4.loadUserLanguage("<%=s_user.getLocale()%>");
	
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>프로젝트 수정</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="17%" />
            <col width="43%" />
            <col width="11%" />
            <col width="27%" />          
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1041", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="PROJ_NO" id="PROJ_NO" readonly>
    </td>
    <td class="fs_large">중요도</td>
    <td>
    	<jsp:include page="../../mg/ni/comboCodeUser.jsp" >
				<jsp:param name="codeId" value="A1117"/>
				<jsp:param name="tagname" value="PROJ_IMP"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1042", s_user.getLocale())%><a> *</a></td>
  	<td>
  		<input type="text" name="PROJ_NM" id="PROJ_NM" size="42" maxlength="128">
  	</td>
  	<td class="fs_large">긴급도</td>
    <td>
    	<jsp:include page="../../mg/ni/comboCodeUser.jsp" >
				<jsp:param name="codeId" value="A1117"/>
				<jsp:param name="tagname" value="PROJ_EMER"/>
				<jsp:param name="def" value="0"/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1043", s_user.getLocale())%></td>
  	<td colspan='3'>
  		<input type="text" name="START_YMD" id="START_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
  		 ~ 
  		<input type="text" name="END_YMD" id="END_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon2" src="../../../ext/images/calendar.png" border="0"></span>
 	</td>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1044", s_user.getLocale())%><a> *</a></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCodeUser.jsp" >
				<jsp:param name="codeId" value="A1199"/>
				<jsp:param name="tagname" value="PJ_USER_NM"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1045", s_user.getLocale())%></td>
    <td>
    	<input type="text" onKeyup="inputPhoneNumber(this);" name="PJ_TEL_NO" id="PJ_TEL_NO" size="16" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">요청자 명</td>
    <td>
    	<input type="text" name="REQ_USER_NM" id="REQ_USER_NM" size="7" maxlength="128">
    </td>
    <td class="fs_large">요청자 전화</td>
    <td>
    	<input type="text" onKeyup="inputPhoneNumber(this);" name="REQ_USER_TEL_NO" id="REQ_USER_TEL_NO" size="12" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large">요청자 이메일</td>
  	<td colspan='3'>
  		<input type="text" name="REQ_USER_EMAIL" id="REQ_USER_EMAIL" size="40" maxlength="128">
  	</td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1046", s_user.getLocale())%><a> *</a></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1107"/>
				<jsp:param name="tagname" value="INDUS_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1047", s_user.getLocale())%><a> *</a></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1108"/>
				<jsp:param name="tagname" value="PROJ_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td></td>
    <td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1049", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="ORDER_YMD" id="ORDER_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon3" src="../../../ext/images/calendar.png" border="0"></span>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104A", s_user.getLocale())%><a> *</a></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1110"/>
				<jsp:param name="tagname" value="PJ_AREA_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large">의뢰종료일자</td>
    <td>
    	<input type="text" name="ORDER_END_YMD" id="ORDER_END_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon4" src="../../../ext/images/calendar.png" border="0"></span>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104B", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="PJ_AREA_SUB" id="PJ_AREA_SUB" size="42" maxlength="128">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104C", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="ESSEN_TECH_DESC" id="ESSEN_TECH_DESC" size="42" maxlength="256">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104D", s_user.getLocale())%></td>
    <td colspan='3'>
    	<!-- <input type="text" name="PJ_REMARK" id="PJ_REMARK" size="40" maxlength="256"> -->
    	<textarea name="PJ_REMARK" id="PJ_REMARK" rows="5" cols="70"></textarea>
    </td>
    <td></td>
    <td></td>
  </tr>
    <tr>
  	<td></td>
    <td class="fs_large">등급별 필요인력 및 단가</td>
    <td colspan='3'>
    	<textarea name="REQU_CNT_PRE" id="REQU_CNT_PRE" rows="3" cols="70"></textarea>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104E", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="PJ_ETC" id="PJ_ETC" size="42" maxlength="256">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_104F", s_user.getLocale())%><a> *</a></td>
    <td colspan='3'>
    	<input type="text" name="COMP_NM" id="COMP_NM" size="17" maxlength="50" readonly>
    	<a href="javascript:doProjCompPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" style='margin-bottom:4px'/></a>
    	<input type="text" name="COMP_ID" id="COMP_ID" size="18" maxlength="50" readonly>
    </td>
    <td></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea">
	        	<a href="javascript:doRev();" class="btn"><span><%=mm.getMessage("NIMG_1007", s_user.getLocale())%></span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
