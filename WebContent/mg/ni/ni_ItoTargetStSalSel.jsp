<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>ITO Target Standard Sales Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var month 			= "";
var son_manp		= "";
var son_sal_amo		= "";
var son_sal_pro	 	= "";
var gwa_manp		= "";
var gwa_sal_amo		= "";
var gwa_sal_pro	 	= "";
var go_manp			= "";
var go_sal_amo		= "";
var go_sal_pro	 	= "";

var now = new Date();

var now_year		= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

//그리드폼
function setFormDraw() {	
	<%-- GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes();	--%>
	
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	


	GridObj.setHeader("구분,손병희,#cspan,#cspan,#cspan,곽성찬,#cspan,#cspan,#cspan,고대호,#cspan,#cspan,#cspan,소계,#cspan,#cspan,#cspan");
	GridObj.attachHeader(["#rspan","신규 투입인력","MM","매출금액","매출이익","신규 투입인력","MM","매출금액","매출이익","신규 투입인력","MM","매출금액","매출이익","투입인력","MM","매출금액","매출이익"]);
	GridObj.setInitWidths("80,80,40,120,120,80,40,120,120,80,40,120,120,80,40,120,120");
	GridObj.setColAlign("center,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right");
	GridObj.setColumnIds("TARGET_MONTH,MANPOWER_SON,MANMONTH_SON,SAL_AMOUNT_SON,SAL_PROFIT_SON,MANPOWER_GWA,MANMONTH_GWA,SAL_AMOUNT_GWA,SAL_PROFIT_GWA,MANPOWER_GO,MANMONTH_GO,SAL_AMOUNT_GO,SAL_PROFIT_GO,MANPOWER_TOTAL,MANMONTH_TOTAL,SAL_AMOUNT_TOTAL,SAL_PROFIT_TOTAL");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,int,int,int,int,int,int,int,int,int,int,int,int");
	GridObj.setColTypes("ro,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron");
	GridObj.setNumberFormat("0",1,".",",");
	GridObj.setNumberFormat("0.0",2,".",",");
	GridObj.setNumberFormat("0,000",3,".",",");
	GridObj.setNumberFormat("0,000",4,".",",");
	GridObj.setNumberFormat("0",5,".",",");
	GridObj.setNumberFormat("0.0",6,".",",");
	GridObj.setNumberFormat("0,000",7,".",",");
	GridObj.setNumberFormat("0,000",8,".",",");
	GridObj.setNumberFormat("0",9,".",",");
	GridObj.setNumberFormat("0.0",10,".",",");
	GridObj.setNumberFormat("0,000",11,".",",");
	GridObj.setNumberFormat("0,000",12,".",",");
	GridObj.setNumberFormat("0",13,".",",");
	GridObj.setNumberFormat("0.0",14,".",",");
	GridObj.setNumberFormat("0,000",15,".",",");
	GridObj.setNumberFormat("0,000",16,".",",");
	
	GridObj._in_header_stat_sum=function(tag,index,data){
	    var calck=function(){
	        var sum=0;                                     
	        this.forEachRow(function(id){
	        	
	            sum+=this.cellById(id,index).getValue()*1;
	            
	        })
	        sum=sum.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	    	return this._aplNF(sum,0);
	    };
	    this._stat_in_header(tag,calck,index,data);
	}
	
	GridObj.attachFooter("합계,<div id='son_a'>0</div>,<div id='son_b'>0</div>,<div id='son_c'>0</div>,<div id='son_d'>0</div>,<div id='gwa_a'>0</div>,<div id='gwa_b'>0</div>,<div id='gwa_c'>0</div>,<div id='gwa_d'>0</div>,<div id='go_a'>0</div>,<div id='go_b'>0</div>,<div id='go_c'>0</div>,<div id='go_d'>0</div>,<div id='total_a'>0</div>,<div id='total_b'>0</div>,<div id='total_c'>0</div>,<div id='total_d'>0</div>"
			,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);
	
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);
	GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange);

	GridObj.init();

	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	now_year = document.form.NOW_YEAR.value;
	var grid_col_id  = "TARGET_MONTH,MANPOWER_SON,MANMONTH_SON,SAL_AMOUNT_SON,SAL_PROFIT_SON,MANPOWER_GWA,MANMONTH_GWA,SAL_AMOUNT_GWA,SAL_PROFIT_GWA,MANPOWER_GO,MANMONTH_GO,SAL_AMOUNT_GO,SAL_PROFIT_GO,MANPOWER_TOTAL,MANMONTH_TOTAL,SAL_AMOUNT_TOTAL,SAL_PROFIT_TOTAL";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoTargetStSalSelToSel&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = " <span class='point'>" + now_year+"</span> 년 ";

	calculateFooterValues();
	
/* 	GridObj.setRowTextStyle(3, "background-color: #CCCCFF; font-family: arial;");
	GridObj.setRowTextStyle(6, "background-color: #CCCCFF; font-family: arial;");
	GridObj.setRowTextStyle(9, "background-color: #CCCCFF; font-family: arial;");
	GridObj.setRowTextStyle(12, "background-color: #CCCCFF; font-family: arial;"); */
	
	if(status == "false") alert(msg);
	return true;
}

function calculateFooterValues(){
	var son_a = document.getElementById("son_a");
		son_a.innerHTML = sumColumnA(1);
	var son_b = document.getElementById("son_b");
		son_b.innerHTML = sumColumnA(2);
	var son_c = document.getElementById("son_c");
		son_c.innerHTML = sumColumnA(3);
	var son_d = document.getElementById("son_d");
		son_d.innerHTML = sumColumnA(4);
	var gwa_a = document.getElementById("gwa_a");
		gwa_a.innerHTML = sumColumnA(5);
	var gwa_b = document.getElementById("gwa_b");
		gwa_b.innerHTML = sumColumnA(6);
	var gwa_c = document.getElementById("gwa_c");
		gwa_c.innerHTML = sumColumnA(7);
	var gwa_d = document.getElementById("gwa_d");
		gwa_d.innerHTML = sumColumnA(8);
	var go_a = document.getElementById("go_a");
		go_a.innerHTML = sumColumnA(9);
	var go_b = document.getElementById("go_b");
		go_b.innerHTML = sumColumnA(10);
	var go_c = document.getElementById("go_c");
		go_c.innerHTML = sumColumnA(11);
	var go_d = document.getElementById("go_d");
		go_d.innerHTML = sumColumnA(12);
	var total_a = document.getElementById("total_a");
		total_a.innerHTML = sumColumnA(13);
	var total_b = document.getElementById("total_b");
		total_b.innerHTML = sumColumnA(14);
	var total_c = document.getElementById("total_c");
		total_c.innerHTML = sumColumnA(15);
	var total_d = document.getElementById("total_d");
		total_d.innerHTML = sumColumnA(16);
	return true;
}

function sumColumnA(ind){
	var out = 0;
	for(var i=1;i<=GridObj.getRowsNum();i++){
    	//if(GridObj.cells(i, GridObj.getColIndexById("SAL_AMOUNT_SON")).getValue() != 1) {
		out += parseFloat(GridObj.cells(i,ind).getValue());
    	//}
	}
	out = out.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return out;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function fnCalCount(type, ths){
    var $input = $(ths).parents("td").find("input[name='NOW_YEAR']");
    var tCount = Number($input.val());
    
    if(type=='p'){
        $input.val(Number(tCount)+1);
        
    }else{
        $input.val(Number(tCount)-1);    
    }
}

//엑셀다운
function doExcelDown() {
	rev_check = false;
	now_year = document.form.NOW_YEAR.value;
	var grid_col_id  = "TARGET_MONTH,MANPOWER_SON,MANMONTH_SON,SAL_AMOUNT_SON,SAL_PROFIT_SON,MANPOWER_GWA,MANMONTH_GWA,SAL_AMOUNT_GWA,SAL_PROFIT_GWA,MANPOWER_GO,MANMONTH_GO,SAL_AMOUNT_GO,SAL_PROFIT_GO,MANPOWER_TOTAL,MANMONTH_TOTAL,SAL_AMOUNT_TOTAL,SAL_PROFIT_TOTAL";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoTargetStSalSelToSelExcel&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};

		$('input[name="nowpage"]').change(function() {
	        var value = $(this).val();
			if(value==1) {
				parent.toModify();
			}
	    });
	});

function init() {
	document.getElementById('NOW_YEAR').value = new Date().toISOString().substring(0, 4);
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="50px" />
            <col width="150px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">조회화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0" checked="checked">
			</td>
			<td></td>
			<td class="tit">수정화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1">
			</td>
			<td></td>
        	<td class="tit">날짜</td>
            <td>
            	<button type="button" onclick="fnCalCount('m',this);" style="padding: 3px 7px 0px 7px;">-</button>
            	<input type="text" name="NOW_YEAR" id="NOW_YEAR" size="7" maxlength="4" style="padding: 5px 0 0 0;" readonly>
            	<button type="button" onclick="fnCalCount('p',this);" style="padding: 3px 6px 0px 6px;">+</button>
            </td>
            <td></td>
        </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	           		<!-- <a href="javascript:doExcelDown();" class="btn"><span>엑셀 다운</span></a> -->
	            	<a href="javascript:doQuery();" class="btn"><span>조회</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="77%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>