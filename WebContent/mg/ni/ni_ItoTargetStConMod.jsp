<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>ITO Target Standard Contact Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var month 			= "";

var now = new Date();

var now_year		= "";
var user_id			= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

//그리드폼
function setFormDraw() {	
	<%-- GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes(); --%>
	
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	
	GridObj.setHeader("#master_checkbox,고객사코드,고객사,사업명,올해,#cspan,#cspan,#cspan,1,#cspan,#cspan,2,#cspan,#cspan,3,#cspan,#cspan,4,#cspan,#cspan,5,#cspan,#cspan,6,#cspan,#cspan,7,#cspan,#cspan,8,#cspan,#cspan,9,#cspan,#cspan,10,#cspan,#cspan,11,#cspan,#cspan,12,#cspan,#cspan")
	GridObj.attachHeader(["#rspan","#rspan","#rspan","#rspan","예상매출","이익","투입인원","년인력","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원","예상매출","이익","인원"]);
	GridObj.setInitWidths("30,70,80,240,80,80,60,40,80,80,40,80,80,40,80,80,40,100,100,50,80,80,40,80,80,40,80,80,40,100,100,50,80,80,40,80,80,40,80,80,40,100,100,50,80,80,40,80,80,40,80,80,40,100,100,50");
	GridObj.setColAlign("center,center,center,left,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right");
	GridObj.setColumnIds("SELECTED,CUSTOMER_CD,CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("na,int,str,str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int");
	GridObj.setColTypes("ch,edn,edtxt,edtxt,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn");
	//doRequestUsingPOST_dhtmlxGrid(GridObj,"A1120","CUSTOMER_CD","");
	
	GridObj.setNumberFormat("0,000",4,".",",");
	GridObj.setNumberFormat("0,000",5,".",",");
	GridObj.setNumberFormat("0",6,".",",");
	GridObj.setNumberFormat("0",7,".",",");
	GridObj.setNumberFormat("0,000",8,".",",");
	GridObj.setNumberFormat("0,000",9,".",",");
	GridObj.setNumberFormat("0",10,".",",");
	GridObj.setNumberFormat("0,000",11,".",",");
	GridObj.setNumberFormat("0,000",12,".",",");
	GridObj.setNumberFormat("0",13,".",",");
	GridObj.setNumberFormat("0,000",14,".",",");
	GridObj.setNumberFormat("0,000",15,".",",");
	GridObj.setNumberFormat("0",16,".",",");
	GridObj.setNumberFormat("0,000",17,".",",");
	GridObj.setNumberFormat("0,000",18,".",",");
	GridObj.setNumberFormat("0",19,".",",");
	GridObj.setNumberFormat("0,000",20,".",",");
	GridObj.setNumberFormat("0,000",21,".",",");
	GridObj.setNumberFormat("0",22,".",",");
	GridObj.setNumberFormat("0,000",23,".",",");
	GridObj.setNumberFormat("0,000",24,".",",");
	GridObj.setNumberFormat("0",25,".",",");
	GridObj.setNumberFormat("0,000",26,".",",");
	GridObj.setNumberFormat("0,000",27,".",",");
	GridObj.setNumberFormat("0",28,".",",");
	GridObj.setNumberFormat("0,000",29,".",",");
	GridObj.setNumberFormat("0,000",30,".",",");
	GridObj.setNumberFormat("0",31,".",",");
	GridObj.setNumberFormat("0,000",32,".",",");
	GridObj.setNumberFormat("0,000",33,".",",");
	GridObj.setNumberFormat("0",34,".",",");
	GridObj.setNumberFormat("0,000",35,".",",");
	GridObj.setNumberFormat("0,000",36,".",",");
	GridObj.setNumberFormat("0",37,".",",");
	GridObj.setNumberFormat("0,000",38,".",",");
	GridObj.setNumberFormat("0,000",39,".",",");
	GridObj.setNumberFormat("0",40,".",",");
	GridObj.setNumberFormat("0,000",41,".",",");
	GridObj.setNumberFormat("0,000",42,".",",");
	GridObj.setNumberFormat("0",43,".",",");
/* 	GridObj.setNumberFormat("0,000",4,".",",");
	GridObj.setNumberFormat("0,000",5,".",",");
	GridObj.setNumberFormat("0",6,".",",");
	GridObj.setNumberFormat("0,000",7,".",",");
	GridObj.setNumberFormat("0,000",8,".",",");
	GridObj.setNumberFormat("0",9,".",",");
	GridObj.setNumberFormat("0,000",10,".",",");
	GridObj.setNumberFormat("0,000",11,".",",");
	GridObj.setNumberFormat("0",12,".",",");
	GridObj.setNumberFormat("0,000",13,".",",");
	GridObj.setNumberFormat("0,000",14,".",",");
	GridObj.setNumberFormat("0",15,".",",");
	GridObj.setNumberFormat("0,000",16,".",",");
	GridObj.setNumberFormat("0,000",17,".",",");
	GridObj.setNumberFormat("0",18,".",",");
	GridObj.setNumberFormat("0,000",19,".",",");
	GridObj.setNumberFormat("0,000",20,".",",");
	GridObj.setNumberFormat("0",21,".",",");
	GridObj.setNumberFormat("0,000",22,".",",");
	GridObj.setNumberFormat("0,000",23,".",",");
	GridObj.setNumberFormat("0",24,".",",");
	GridObj.setNumberFormat("0,000",25,".",",");
	GridObj.setNumberFormat("0,000",26,".",",");
	GridObj.setNumberFormat("0",27,".",",");
	GridObj.setNumberFormat("0,000",28,".",",");
	GridObj.setNumberFormat("0,000",29,".",",");
	GridObj.setNumberFormat("0",30,".",",");
	GridObj.setNumberFormat("0,000",31,".",",");
	GridObj.setNumberFormat("0,000",32,".",",");
	GridObj.setNumberFormat("0",33,".",",");
	GridObj.setNumberFormat("0,000",34,".",",");
	GridObj.setNumberFormat("0,000",35,".",",");
	GridObj.setNumberFormat("0",36,".",",");
	GridObj.setNumberFormat("0,000",37,".",",");
	GridObj.setNumberFormat("0,000",38,".",",");
	GridObj.setNumberFormat("0",39,".",","); */
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);
	GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange);

	GridObj.init();
	
	user_id = "s1024323";
	
	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	now_year = document.form.NOW_YEAR.value;
	var grid_col_id  = "SELECTED,CUSTOMER_CD,CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
		argu += "&USER_ID="		+encodeURIComponent(user_id);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoTargetStConSelToMod&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = " <span class='point'>" + now_year+"</span> 년 ";
	
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doAddRow() {

	//dhtmlx_last_row_id++;
	var last_row_id = GridObj.getRowsNum();
   	var nMaxRow2 = ++last_row_id;
   	var row_data = "SELECTED,CUSTOMER_CD,CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_CD")).setValue("0100");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER")).setValue("");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BUSINESS_NAME")).setValue("");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("LAST_EXP_SAL")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("LAST_EXO_PRO")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("LAST_IN_MANPOWER")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("LAST_MANPOWER")).setValue("0");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_1")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_1")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_1")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_2")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_2")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_2")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_3")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_3")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_3")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_4")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_4")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_4")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_5")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_5")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_5")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_6")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_6")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_6")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_7")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_7")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_7")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_8")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_8")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_8")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_9")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_9")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_9")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_10")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_10")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_10")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_11")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_11")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_11")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXP_SAL_12")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXO_PRO_12")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MANPOWER_12")).setValue("0");

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

//데이터 저장
function doSave() {
	if(!checkRows()) return;
	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		<%-- var cols_ids = "<%=grid_col_id%>"; --%>
		var cols_ids  = "SELECTED,CUSTOMER_CD,CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12";
		var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
			argu += "&USER_ID="		+encodeURIComponent(user_id);

    	var SERVLETURL = G_SERVLETURL + "?mod=itoTargetStConSave&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}


function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {		
	} else {
		alert(messsage);
	}
	
	doQuery();
	
	return false;
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "SELECTED,CUSTOMER_CD,CUSTOMER,BUSINESS_NAME,LAST_EXP_SAL,LAST_EXO_PRO,LAST_IN_MANPOWER,LAST_MANPOWER,EXP_SAL_1,EXO_PRO_1,MANPOWER_1,EXP_SAL_2,EXO_PRO_2,MANPOWER_2,EXP_SAL_3,EXO_PRO_3,MANPOWER_3,EXP_SAL_4,EXO_PRO_4,MANPOWER_4,EXP_SAL_5,EXO_PRO_5,MANPOWER_5,EXP_SAL_6,EXO_PRO_6,MANPOWER_6,EXP_SAL_7,EXO_PRO_7,MANPOWER_7,EXP_SAL_8,EXO_PRO_8,MANPOWER_8,EXP_SAL_9,EXO_PRO_9,MANPOWER_9,EXP_SAL_10,EXO_PRO_10,MANPOWER_10,EXP_SAL_11,EXO_PRO_11,MANPOWER_11,EXP_SAL_12,EXO_PRO_12,MANPOWER_12";
			var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
				argu += "&USER_ID="		+encodeURIComponent(user_id);
			var SERVLETURL = G_SERVLETURL + "?mod=itoTargetStConDelete&col_ids="+cols_ids+argu;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}

function fnCalCount(type, ths){
    var $input = $(ths).parents("td").find("input[name='NOW_YEAR']");
    var tCount = Number($input.val());
    
    if(type=='p'){
        $input.val(Number(tCount)+1);
        
    }else{
        $input.val(Number(tCount)-1);    
    }
}

$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};

		$('input[name="nowpage"]').change(function() {
			var value = $(this).val();
			if(value==1) {
				parent.init();
			}
	    });
		
		$('input[name="selectuser"]').change(function() {
	        var value = $(this).val();
			if(value=="s1024323") {
				user_id = "s1024323";
			}
			if(value=="redeyesss") {
				user_id = "redeyesss";
			}
			if(value=="daeho") {
				user_id = "daeho";
			}
			if(value=="all") {
				user_id = "all";
			}
			doQuery();
	    });
	});

function init() {
	document.getElementById('NOW_YEAR').value = new Date().toISOString().substring(0, 4);
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="60px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">조회화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1">
			</td>
			<td></td>
			<td class="tit">수정화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0" checked="checked">
			</td>
			<td></td>
        	<td class="tit">날짜</td>
            <td colspan="3">
            	<button type="button" onclick="fnCalCount('m',this);" style="padding: 3px 7px 0px 7px;">-</button>
            	<input type="text" name="NOW_YEAR" id="NOW_YEAR" size="7" maxlength="4" style="padding: 5px 0 0 0;" readonly>
            	<button type="button" onclick="fnCalCount('p',this);" style="padding: 3px 6px 0px 6px;">+</button>
            </td>
            <td></td>
        </tr>
        <tr>
        	<td class="tit">손병희</td>
	        <td>
	        	<input type="radio" name="selectuser" value="s1024323" checked="checked">
			</td>
			<td></td>
			<td class="tit">곽성찬</td>
	        <td>
	        	<input type="radio" name="selectuser" value="redeyesss">
			</td>
			<td></td>
			<td class="tit">고대호</td>
	        <td>
	        	<input type="radio" name="selectuser" value="daeho">
			</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doQuery();" class="btn"><span>조회</span></a>
	            	<a href="javascript:doAddRow();" class="btn"><span>행추가</span></a>
	            	<a href="javascript:doSave();" class="btn"><span>저장</span></a>
	            	<a href="javascript:doDelete();" class="btn"><span>삭제</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="77%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>