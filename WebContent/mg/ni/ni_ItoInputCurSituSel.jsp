<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>ITO Target Input Current Situation</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var month 			= "";

var now = new Date();

var now_year		= "";
var user_id			= "";
var selecttype		= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

//그리드폼
function setFormDraw() {	
	<%-- GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes();	--%>
	
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");

	GridObj.setHeader("#master_checkbox,프로젝트,수행사,소속사,이름,계약금액,#cspan,#cspan,프로젝트 투입,#cspan,MM,#cspan,기여도,기여액,#cspan,#cspan,#cspan,CN,RE,RU")
	GridObj.attachHeader(["#rspan","#rspan","#rspan","#rspan","#rspan","계약금액","급여","이익","시작일","종료일","올해","내년","#rspan","올해매출","올해","내년","누적합계","#rspan","#rspan","#rspan"]);
	GridObj.setInitWidths("30,120,80,80,60,80,80,80,80,80,40,40,60,100,90,80,80,0,0,0");
	GridObj.setColAlign("center,left,left,left,center,right,right,right,center,center,center,center,center,right,right,right,right,center,center,center");
	GridObj.setColumnIds("SELECTED,PROJ_NM,COMP_NM,AGENCY_NM,MAN_NM,CONT_AMT,PAY_PRC,CHARGE,START_YMD,END_YMD,THIS_YEAR_MM,NEXT_YEAR_MM,CONTRIBUTION,CONTRI_THISSALES,CONTRI_THIS,CONTRI_NEXT,CONTRI_SUM,CONT_NO,RECOMMANDER,RU_CD");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("na,str,str,str,str,int,int,int,str,str,int,int,int,int,int,int,int,str,str,str");
	GridObj.setColTypes("ch,ro,ro,ro,ro,ron,ron,ron,ro,ro,ron,ron,ron,ron,ron,ron,ron,ro,ro,ro");

	doRequestUsingPOST_dhtmlxGrid(GridObj,"A1199","MANAGER","");
	doRequestUsingPOST_dhtmlxGrid(GridObj,"A1199","RECOMMANDER","");
	
	GridObj.setNumberFormat("0,000",5,".",",");
	GridObj.setNumberFormat("0,000",6,".",",");
	GridObj.setNumberFormat("0,000",7,".",",");
	GridObj.setNumberFormat("0.00",10,".",",");
	GridObj.setNumberFormat("0.00",11,".",",");
	GridObj.setNumberFormat("0%",12,".",",");
	GridObj.setNumberFormat("0,000",13,".",",");
	GridObj.setNumberFormat("0,000",14,".",",");
	GridObj.setNumberFormat("0,000",15,".",",");
	GridObj.setNumberFormat("0,000",16,".",",");
	
	GridObj._in_header_stat_sum=function(tag,index,data){
	    var calck=function(){
	        var sum=0;                                     
	        this.forEachRow(function(id){
	        	
	            sum+=this.cellById(id,index).getValue()*1;
	            
	        })
	        sum=sum.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	    	return this._aplNF(sum,0);
	    };
	    this._stat_in_header(tag,calck,index,data);
	}
	
	GridObj.attachFooter("합계,#cspan,#cspan,#cspan,#cspan,<div id='cont_amt'>0</div>,<div id='pay_prc'>0</div>,<div id='charge'>0</div>,<div></div>,<div></div>,<div id='this_year_mm'>0</div>,<div id='next_year_mm'>0</div>,<div></div>,<div id='contri_thissales'>0</div>,<div id='contri_this'>0</div>,<div id='contri_next'>0</div>,<div id='contri_sum'>0</div>,"
			,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);
	
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);

	GridObj.init();
	
	user_id = "bskosk";
	selecttype = "admit";

	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	now_year = document.form.NOW_YEAR.value;
	
	var grid_col_id  = "SELECTED,PROJ_NM,COMP_NM,AGENCY_NM,MAN_NM,CONT_AMT,PAY_PRC,CHARGE,START_YMD,END_YMD,THIS_YEAR_MM,NEXT_YEAR_MM,CONTRIBUTION,CONTRI_THISSALES,CONTRI_THIS,CONTRI_NEXT,CONTRI_SUM,CONT_NO,RECOMMANDER,RU_CD";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
		argu += "&USER_ID="		+encodeURIComponent(user_id);
		argu += "&SELECT_TYPE="		+encodeURIComponent(selecttype);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoInputCurSituSel&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = " <span class='point'>" + now_year+"</span> 년 ";

	calculateFooterValues();
	
	if(status == "false") alert(msg);
	return true;
}

function calculateFooterValues(){
	var son_a = document.getElementById("cont_amt");
		son_a.innerHTML = sumColumnA(5);
	var son_b = document.getElementById("pay_prc");
		son_b.innerHTML = sumColumnA(6);
	var son_c = document.getElementById("charge");
		son_c.innerHTML = sumColumnA(7);
	var son_d = document.getElementById("this_year_mm");
		son_d.innerHTML = sumColumnA(10);
	var gwa_a = document.getElementById("next_year_mm");
		gwa_a.innerHTML = sumColumnA(11);
	var gwa_b = document.getElementById("contri_thissales");
		gwa_b.innerHTML = sumColumnA(13);
	var gwa_c = document.getElementById("contri_this");
		gwa_c.innerHTML = sumColumnA(14);
	var gwa_d = document.getElementById("contri_next");
		gwa_d.innerHTML = sumColumnA(15);
	var go_a = document.getElementById("contri_sum");
		go_a.innerHTML = sumColumnA(16);
	return true;
}

function sumColumnA(ind){
	var out = 0;
	for(var i=1;i<=GridObj.getRowsNum();i++){
    	//if(GridObj.cells(i, GridObj.getColIndexById("SAL_AMOUNT_SON")).getValue() != 1) {
		out += parseFloat(GridObj.cells(i,ind).getValue());
    	//}
	}
	out = out.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return out;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function fnCalCount(type, ths){
    var $input = $(ths).parents("td").find("input[name='NOW_YEAR']");
    var tCount = Number($input.val());
    
    if(type=='p'){
        $input.val(Number(tCount)+1);
        
    }else{
        $input.val(Number(tCount)-1);    
    }
}

$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};

		$('input[name="nowpage"]').change(function() {
	        var value = $(this).val();
			if(value==1) {
				parent.toModify();
			}
	    });
		
		$('input[name="selectuser"]').change(function() {
	        var value = $(this).val();
	        if(value=="bskosk") {
				user_id = "bskosk";
			}
			if(value=="s1024323") {
				user_id = "s1024323";
			}
			if(value=="redeyesss") {
				user_id = "redeyesss";
			}
			if(value=="daeho") {
				user_id = "daeho";
			}
			if(value=="all") {
				user_id = "all";
			}
			doQuery();
	    });
		
		$('input[name="selecttype"]').change(function() {
	        var value = $(this).val();
			if(value=="admit") {
				selecttype = "admit";
			}
			if(value=="calcu") {
				selecttype = "calcu";
			}
			doQuery();
	    });
	});

function init() {
	document.getElementById('NOW_YEAR').value = new Date().toISOString().substring(0, 4);
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="80px" />
            <col width="40px" />
            <col width="50px" />
            <col width="60px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">조회화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0" checked="checked">
			</td>
			<td></td>
			<td class="tit">수정화면</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1">
			</td>
			<td></td>
        	<td class="tit">날짜</td>
            <td colspan="3">
            	<button type="button" onclick="fnCalCount('m',this);" style="padding: 3px 7px 0px 7px;">-</button>
            	<input type="text" name="NOW_YEAR" id="NOW_YEAR" size="7" maxlength="4" style="padding: 5px 0 0 0;" readonly>
            	<button type="button" onclick="fnCalCount('p',this);" style="padding: 3px 6px 0px 6px;">+</button>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
      		<td class="tit">고봉수</td>
	        <td>
	        	<input type="radio" name="selectuser" value="bskosk" checked="checked">
			</td>
			<td></td>
        	<td class="tit">손병희</td>
	        <td>
	        	<input type="radio" name="selectuser" value="s1024323">
			</td>
			<td></td>
			<td class="tit">곽성찬</td>
	        <td>
	        	<input type="radio" name="selectuser" value="redeyesss">
			</td>
			<td></td>
			<td class="tit">고대호</td>
	        <td>
	        	<input type="radio" name="selectuser" value="daeho">
			</td>
			<td></td>
			<td class="tit">합계</td>
	        <td>
	        	<input type="radio" name="selectuser" value="all">
			</td>
        </tr>
        <tr>
        	<td class="tit" colspan="2">현업건 인정</td>
	        <td>
	        	<input type="radio" name="selecttype" value="admit" checked="checked">
			</td>
			<td class="tit" colspan="2">현업건 정산처리</td>
	        <td>
	        	<input type="radio" name="selecttype" value="calcu">
			</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
        </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doQuery();" class="btn"><span>조회</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="77%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>