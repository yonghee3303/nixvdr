<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  
  String selecttype = request.getParameter("selecttype");
  String nowyear = request.getParameter("nowyear");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>매출목표실적</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%><%@ include file="../../ext/include/su_grid_common.jsp" %><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var now = new Date();

var now_year		= "";
var selecttype		= "";

// 등록화면
var rev_check		= false;
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	


	GridObj.setHeader("구분,고봉수,#cspan,#cspan,#cspan,손병희,#cspan,#cspan,#cspan,곽성찬,#cspan,#cspan,#cspan,고대호,#cspan,#cspan,#cspan,소계,#cspan,#cspan,#cspan");
	GridObj.attachHeader(["#rspan","신규 투입인력","MM","매출금액","매출이익","신규 투입인력","MM","매출금액","매출이익","신규 투입인력","MM","매출금액","매출이익","신규 투입인력","MM","매출금액","매출이익","투입인력","MM","매출금액","매출이익"]);
	GridObj.setInitWidths("80,80,40,120,120,80,40,120,120,80,40,120,120,80,40,120,120,80,40,120,120");
	GridObj.setColAlign("center,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right");
	GridObj.setColumnIds("TARGET_MONTH,MANPOWER_OWN,MANMONTH_OWN,SAL_AMOUNT_OWN,SAL_PROFIT_OWN,MANPOWER_SON,MANMONTH_SON,SAL_AMOUNT_SON,SAL_PROFIT_SON,MANPOWER_GWA,MANMONTH_GWA,SAL_AMOUNT_GWA,SAL_PROFIT_GWA,MANPOWER_GO,MANMONTH_GO,SAL_AMOUNT_GO,SAL_PROFIT_GO,MANPOWER_TOTAL,MANMONTH_TOTAL,SAL_AMOUNT_TOTAL,SAL_PROFIT_TOTAL");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int,int");
	GridObj.setColTypes("ro,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron,ron");
	GridObj.setNumberFormat("0.0",1,".",",");
	GridObj.setNumberFormat("0.0",2,".",",");
	GridObj.setNumberFormat("0,000",3,".",",");
	GridObj.setNumberFormat("0,000",4,".",",");
	GridObj.setNumberFormat("0.0",5,".",",");
	GridObj.setNumberFormat("0.0",6,".",",");
	GridObj.setNumberFormat("0,000",7,".",",");
	GridObj.setNumberFormat("0,000",8,".",",");
	GridObj.setNumberFormat("0.0",9,".",",");
	GridObj.setNumberFormat("0.0",10,".",",");
	GridObj.setNumberFormat("0,000",11,".",",");
	GridObj.setNumberFormat("0,000",12,".",",");
	GridObj.setNumberFormat("0.0",13,".",",");
	GridObj.setNumberFormat("0.0",14,".",",");
	GridObj.setNumberFormat("0,000",15,".",",");
	GridObj.setNumberFormat("0,000",16,".",",");
	GridObj.setNumberFormat("0.0",17,".",",");
	GridObj.setNumberFormat("0.0",18,".",",");
	GridObj.setNumberFormat("0,000",19,".",",");
	GridObj.setNumberFormat("0,000",20,".",",");
	
	GridObj._in_header_stat_sum=function(tag,index,data){
	    var calck=function(){
	        var sum=0;                                     
	        this.forEachRow(function(id){
	        	
	            sum+=this.cellById(id,index).getValue()*1;
	            
	        })
	        sum=sum.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	    	return this._aplNF(sum,0);
	    };
	    this._stat_in_header(tag,calck,index,data);
	}
	
	GridObj.attachFooter("합계,<div id='own_a'>0</div>,<div id='own_b'>0</div>,<div id='own_c'>0</div>,<div id='own_d'>0</div>,<div id='son_a'>0</div>,<div id='son_b'>0</div>,<div id='son_c'>0</div>,<div id='son_d'>0</div>,<div id='gwa_a'>0</div>,<div id='gwa_b'>0</div>,<div id='gwa_c'>0</div>,<div id='gwa_d'>0</div>,<div id='go_a'>0</div>,<div id='go_b'>0</div>,<div id='go_c'>0</div>,<div id='go_d'>0</div>,<div id='total_a'>0</div>,<div id='total_b'>0</div>,<div id='total_c'>0</div>,<div id='total_d'>0</div>"
			,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);
	
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onXLE",doQueryEnd);
	GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange);
	
	GridObj.init();

	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

function doOnRowSelect(rowId, cellInd) {}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}



// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() 
{
	rev_check = false;
	now_year = "<%=nowyear%>";
	selecttype = "<%=selecttype%>";
	var grid_col_id  = "TARGET_MONTH,MANPOWER_OWN,MANMONTH_OWN,SAL_AMOUNT_OWN,SAL_PROFIT_OWN,MANPOWER_SON,MANMONTH_SON,SAL_AMOUNT_SON,SAL_PROFIT_SON,MANPOWER_GWA,MANMONTH_GWA,SAL_AMOUNT_GWA,SAL_PROFIT_GWA,MANPOWER_GO,MANMONTH_GO,SAL_AMOUNT_GO,SAL_PROFIT_GO,MANPOWER_TOTAL,MANMONTH_TOTAL,SAL_AMOUNT_TOTAL,SAL_PROFIT_TOTAL";
	var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year);
		argu += "&SELECT_TYPE="		+encodeURIComponent(selecttype);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=ItoSalesStMaSeltoDiff&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);																																									
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) 
{
	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");

	document.getElementById("totalCntTD").innerHTML = " <span class='point'>" + now_year+"</span> 년 차이";
	
	calculateFooterValues();
	
	/* GridObj.setRowTextStyle(3, "background-color: #CCCCFF; font-family: arial;");
	GridObj.setRowTextStyle(6, "background-color: #CCCCFF; font-family: arial;");
	GridObj.setRowTextStyle(9, "background-color: #CCCCFF; font-family: arial;");
	GridObj.setRowTextStyle(12, "background-color: #CCCCFF; font-family: arial;"); */
	
	// if(status == "false") alert(msg);
	return true;
}

function calculateFooterValues(){
	var own_a = document.getElementById("own_a");
		own_a.innerHTML = sumColumnA(1);
	var own_b = document.getElementById("own_b");
		own_b.innerHTML = sumColumnA(2);
	var own_c = document.getElementById("own_c");
		own_c.innerHTML = sumColumnA(3);
	var own_d = document.getElementById("own_d");
		own_d.innerHTML = sumColumnA(4);
	var son_a = document.getElementById("son_a");
		son_a.innerHTML = sumColumnA(5);
	var son_b = document.getElementById("son_b");
		son_b.innerHTML = sumColumnA(6);
	var son_c = document.getElementById("son_c");
		son_c.innerHTML = sumColumnA(7);
	var son_d = document.getElementById("son_d");
		son_d.innerHTML = sumColumnA(8);
	var gwa_a = document.getElementById("gwa_a");
		gwa_a.innerHTML = sumColumnA(9);
	var gwa_b = document.getElementById("gwa_b");
		gwa_b.innerHTML = sumColumnA(10);
	var gwa_c = document.getElementById("gwa_c");
		gwa_c.innerHTML = sumColumnA(11);
	var gwa_d = document.getElementById("gwa_d");
		gwa_d.innerHTML = sumColumnA(12);
	var go_a = document.getElementById("go_a");
		go_a.innerHTML = sumColumnA(13);
	var go_b = document.getElementById("go_b");
		go_b.innerHTML = sumColumnA(14);
	var go_c = document.getElementById("go_c");
		go_c.innerHTML = sumColumnA(15);
	var go_d = document.getElementById("go_d");
		go_d.innerHTML = sumColumnA(16);
	var total_a = document.getElementById("total_a");
		total_a.innerHTML = sumColumnA(17);
	var total_b = document.getElementById("total_b");
		total_b.innerHTML = sumColumnA(18);
	var total_c = document.getElementById("total_c");
		total_c.innerHTML = sumColumnA(19);
	var total_d = document.getElementById("total_d");
		total_d.innerHTML = sumColumnA(20);
	return true;
}

function sumColumnA(ind){
	var out = 0;
	for(var i=1;i<=GridObj.getRowsNum();i++){
    	//if(GridObj.cells(i, GridObj.getColIndexById("SAL_AMOUNT_SON")).getValue() != 1) {
		out += parseFloat(GridObj.cells(i,ind).getValue());
    	//}
	}
	out = out.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return out;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();" style="overflow:hidden;">
<form name="form" method="post">
<!-- <input type="file" name= "browseFile" style="display='none';">   -->
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="93%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=142"/>
</jsp:include> --%>
</body>
</html>
