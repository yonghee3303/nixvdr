<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  
  String proj_no = request.getParameter("proj_no");
  String seq = request.getParameter("seq");
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>필요인원</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%><%@ include file="../../ext/include/su_grid_common.jsp" %><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var G_SERVLETURL = "<%=contextPath%>/ni/recruit.do";

var man_id			= "";
var proj_no			= "<%=proj_no%>";
var seq = "<%=seq%>";

var queOrSear = "";
var tech_lvl_cd = "";
var proj_cl_cd = "";
var indus_cl_cd = "";
var reg_no = "";
var major_desc = "";
var edu_cl_cd = "";
var qual_cl_cd = "";
var pj_area_cd = "";
var black_yn = "";
var skill_etc = "";
var evlu_cd = "";
var tech_cl_cd = "";
var role_cl_cd = "";

var man_id = "";
var sman_id = "";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
	/* 	var file_valueA = GridObj.cells(row_id, GridObj.getColIndexById("FILE_ICON")).getValue();
		var file_value = file_valueA.substring(file_valueA.length-5,file_valueA.length-4); */

		if( header_name == "FILE_ICON") {
	/* 		if(file_value == "b"){ */
			doFiledownLoad(row_id);
	/* 		} */
		}
	
}

function doOnRowSelect(rowId, cellInd) {
	man_id = GridObj.cells(rowId, GridObj.getColIndexById("MAN_ID")).getValue();
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}



// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() 
{
	if(proj_no == "") return;
	
	var grid_col_id = "<%=grid_col_id%>";
	
	GridObj.loadXML(G_SERVLETURL+"?mod=selectRecManList&grid_col_id="+grid_col_id+"&PROJ_NO=" + proj_no);																						
	GridObj.clearAll(false);

}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) 
{
	man_Id = "";
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	// if(status == "false") alert(msg);
	return true;
}

function doManSearch() {
	var url = "/mg/ni/popupManSearch.jsp"
		popupNewAddBoard(url, "신규 생성", 550, 1710);
}

function doAddSearchMan() {
	
	doAddSearchManDo(sman_id);
}

function doAddSearchManDo(sman_id) {
	var sman_id = sman_id;
	var grid_col_id = "<%=grid_col_id%>";
	
	GridObj.loadXML(G_SERVLETURL+"?mod=selectRecManList&grid_col_id="+grid_col_id+"&SMAN_ID=" + sman_id);																						
	GridObj.clearAll(false);
}

function doSelect() {
	if(proj_no == "" || proj_no == null || proj_no == "null") {
		alert("프로젝트를 선택하고 인력을 선택해주세요");
		return;
	}
	if(man_id == ""){
		alert("원하시는 인력을 먼저 선택해주세요");
		return;
	}
	
	addMan(man_id);
}

function addMan(man_id) {

	var man_id = man_id;

	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
   	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ID")).setValue(man_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_NO")).setValue(proj_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SEQ")).setValue(seq);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doAddMan();
}

function doAddMan() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
    	var SERVLETURL = G_SERVLETURL + "?mod=addRecMan&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);		
}

function doSaveEnd(obj) {

	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;
	
	
	
	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		// parent.refresh();
		parent.refresh(proj_no, seq);
		//doQuery();
	//} else if(status == "truee") {
	//	alert("인력이 이미 -" + messsage + "- 프로젝트에 들어가 있습니다");
	//	parent.refresh(proj_no, seq);
	//} else {
	//	alert(messsage);
	}
	return false;
}

function doFiledownLoad(row_id){
	var man_id = GridObj.cells(row_id, GridObj.getColIndexById("MAN_ID")).getValue();
	
	popupNimsAttachFile("File Upload", '', '', 500, 400, "manProfile", man_id, "", "write",<%=pm.getString("component.contextPath.root")%>);
 }

//첨부파일 관리  POPUP을 띄운다.
function popupNimsAttachFile(title, left, top, width, height, pgmId, docId, authority, callmode,root) {
    if (title == '') title = 'File Upload';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
    
    var url ="";
    
    if(root == false) {
    	url = contextPath;
    }
    
    if(callmode == "write") {
       url += "/mg/ni/popupNimsAttachFile.jsp";
    }else if(callmode=="repacking"){
    	url += "/mg/common/popupAttachFile_repacking.jsp";
    	resizable='no';
    }else if(callmode=="readPortal"){
    	url += "/mg/common/popupAttachFileRead_portal.jsp";
    	resizable='no';
    }else if(callmode=="readRepacking"){
    	url += "/mg/common/popupAttachFileSingleRead_portal.jsp";
    	resizable='no';
    }else {
    	url += "/mg/common/popupAttachFileRead.jsp";
    }
    
    if(callmode=="readRepacking"){
    	url +="?formid=POP_0109&gridid=GridObj";
    }else{
    	url += "?formid=POP_0106&gridid=GridObj";
    }
    url += "&pgmId=" + pgmId + "&docId=" + docId + "&authority=" + authority;
    if(callmode=="repacking"){
    	url += "&docTitle="+authority;
    }
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];
	
	var ret = window.open(url, "upload", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();" style="overflow:hidden;">
<form name="form" method="post">
<!-- <input type="file" name= "browseFile" style="display='none';">   -->
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="8%" />
            <col width="32%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td>추천인력 </td>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doManSearch();" class="btn"><span><%=mm.getMessage("인력검색", s_user.getLocale())%></span></a>
	            	<a href="javascript:doSelect();" class="btn"><span><%=mm.getMessage("선택", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="88%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=142"/>
</jsp:include> --%>
</body>
</html>
