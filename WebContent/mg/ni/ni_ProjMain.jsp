<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  String contextPath = request.getContextPath();
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>프로젝트</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%> <%@ include file="../../ext/include/su_grid_common.jsp" %> <%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var proj_no			= "";
var proj_nm			= "";
var comp_nm			= "";
var start_ymd		= "";
var end_ymd			= "";
var pj_user_nm		= "";
var pj_tel_no		= "";
var indus_cl_cd		= "";
var proj_cl_cd		= "";
var essen_tech_desc	= "";
var pj_remark		= "";
var requ_cnt_pre	= "";
var pj_area_cd		= "";
var pj_etc			= "";
var pj_area_sub		= "";
var order_ymd		= "";
var order_end_ymd	= "";
var comp_id			= "";
var comp_nm			= "";

var req_user_nm		= "";
var req_user_tel_no	= "";
var req_user_email	= "";
var proj_imp		= "";
var proj_emer		= "";
var current_date	= "";

var rev_check		= false;
var rowsNumb		= "";

var G_SERVLETURL = "<%=contextPath%>/ni/proj.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	MenuObj = new dhtmlXMenuObject();
	MenuObj.attachEvent("onClick",onRightButtonClick);
	MenuObj.loadStruct("../common/_context.xml");
  	GridObj = setGridDraw(GridObj);
  	GridObj.enableContextMenu(MenuObj);
   	GridObj.setSizes();
   	
   	var now = new Date();
   	current_date = new Date().toISOString().substring(0, 10);
   	
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	doRevPop();
}

function onRightButtonClick(rowId,cellInd) {
	var rightClick = GridObj.cells(rowId, GridObj.getColIndexById("PROJ_NO")).getValue();
}

function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	rev_check = true;
	
	var proj_no = GridObj.cells(rowId, GridObj.getColIndexById("PROJ_NO")).getValue();
	parent.parent.refresh(proj_no);
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	rev_check = false;
	
	var proj_nm = parent.document.form.PROJ_NM.value;
	var comp_nm = parent.document.form.COMP_NM.value;
	var pj_cl_cd = parent.document.form.PJ_CL_CD.value;
	var pjymd_from = parent.document.form.PJYMD_FROM.value;
	var pjymd_to = parent.document.form.PJYMD_TO.value;
	
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&PROJ_NM="	+proj_nm;
		argu += "&COMP_NM="	+comp_nm;
		argu +=	"&PJ_CL_CD=" +pj_cl_cd;
		argu += "&PJYMD_FROM="	+pjymd_from;
		argu += "&PJYMD_TO="	+pjymd_to;
		argu += "&CURRENT_DATE="+current_date;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=projSelect&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	
	parent.rowsNumb = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	parent.getRowsNumb();
	// if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

//신규 버튼 기능
function doAddNewPop() {
	var url = "/mg/ni/popupProjNewAdd.jsp"
	popupNewAddBoard(url, "신규 생성", 725, 1000);
}

//신규 팝업창 기능
function getPopupData() {
	doAddRow();
}

//신규 데이터
function doAddRow() {

	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_NO")).setValue(proj_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_NM")).setValue(proj_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_NM")).setValue(comp_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("START_YMD")).setValue(start_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("END_YMD")).setValue(end_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_USER_NM")).setValue(pj_user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_TEL_NO")).setValue(pj_tel_no);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQ_USER_NM")).setValue(req_user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQ_USER_TEL_NO")).setValue(req_user_tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQ_USER_EMAIL")).setValue(req_user_email);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_IMP")).setValue(proj_imp);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_EMER")).setValue(proj_emer);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INDUS_CL_CD")).setValue(indus_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_CL_CD")).setValue(proj_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ESSEN_TECH_DESC")).setValue(essen_tech_desc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_REMARK")).setValue(pj_remark);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQU_CNT_PRE")).setValue(requ_cnt_pre);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_AREA_CD")).setValue(pj_area_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_ETC")).setValue(pj_etc);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ORDER_YMD")).setValue(order_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ORDER_END_YMD")).setValue(order_end_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_AREA_SUB")).setValue(pj_area_sub);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_ID")).setValue(comp_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_NM")).setValue(comp_nm);

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doAddSave();
}

function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//신규 데이터 sql
function doAddSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
    	var SERVLETURL = G_SERVLETURL + "?mod=saveNewProj&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

//수정 버튼 기능
function doRevPop() {
	if(!rev_check) {
		alert("수정할 행을 선택해주세요.");
		return;
	}
	
	if(!checkRowsFRev()) {
		alert("수정시엔 모든 체크박스를 해제해주세요.");
		return;
	}
	
	if(GridObj.getSelectedRowId()=="") {
		alert("수정할 행을 선택해주세요.");
		return;
	}
	
	var url = "/mg/ni/popupProjRev.jsp"
		popupNewAddBoard(url, "정보수정", 725, 1000);
}

//수정 팝업창 기능
function getRevPopupData() {
	doRevAddRow();
}

//수정 데이터
function getParentData() {
	row_id = GridObj.getSelectedRowId();
	
	proj_no = GridObj.cells(row_id, GridObj.getColIndexById("PROJ_NO")).getValue();
	proj_nm = GridObj.cells(row_id, GridObj.getColIndexById("PROJ_NM")).getValue();
	comp_nm = GridObj.cells(row_id, GridObj.getColIndexById("COMP_NM")).getValue();
	start_ymd = GridObj.cells(row_id, GridObj.getColIndexById("START_YMD")).getValue();
	end_ymd = GridObj.cells(row_id, GridObj.getColIndexById("END_YMD")).getValue();
	pj_user_nm = GridObj.cells(row_id, GridObj.getColIndexById("PJ_USER_NM")).getValue();
	pj_tel_no = GridObj.cells(row_id, GridObj.getColIndexById("PJ_TEL_NO")).getValue();
	
	req_user_nm = GridObj.cells(row_id, GridObj.getColIndexById("REQ_USER_NM")).getValue();
	req_user_tel_no = GridObj.cells(row_id, GridObj.getColIndexById("REQ_USER_TEL_NO")).getValue();
	req_user_email = GridObj.cells(row_id, GridObj.getColIndexById("REQ_USER_EMAIL")).getValue();
	proj_imp = GridObj.cells(row_id, GridObj.getColIndexById("PROJ_IMP")).getValue();
	proj_emer = GridObj.cells(row_id, GridObj.getColIndexById("PROJ_EMER")).getValue();
	
	indus_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("INDUS_CL_CD")).getValue();
	proj_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("PROJ_CL_CD")).getValue();
	essen_tech_desc = GridObj.cells(row_id, GridObj.getColIndexById("ESSEN_TECH_DESC")).getValue();
	pj_remark = GridObj.cells(row_id, GridObj.getColIndexById("PJ_REMARK")).getValue();
	requ_cnt_pre = GridObj.cells(row_id, GridObj.getColIndexById("REQU_CNT_PRE")).getValue();
	pj_area_cd = GridObj.cells(row_id, GridObj.getColIndexById("PJ_AREA_CD")).getValue();
	pj_etc = GridObj.cells(row_id, GridObj.getColIndexById("PJ_ETC")).getValue();
	
	order_ymd = GridObj.cells(row_id, GridObj.getColIndexById("ORDER_YMD")).getValue();
	order_end_ymd = GridObj.cells(row_id, GridObj.getColIndexById("ORDER_END_YMD")).getValue();
	pj_area_sub = GridObj.cells(row_id, GridObj.getColIndexById("PJ_AREA_SUB")).getValue();
	comp_id = GridObj.cells(row_id, GridObj.getColIndexById("COMP_ID")).getValue();
	comp_nm = GridObj.cells(row_id, GridObj.getColIndexById("COMP_NM")).getValue();
		
	GridObj.cells(row_id, GridObj.getColIndexById("SELECTED")).setValue("0");
}

//수정시 체크 해제
function checkRowsFRev() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return false;
	}
	return true;
}

//데이터 수정
function doRevAddRow() {
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_NO")).setValue(proj_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_NM")).setValue(proj_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_NM")).setValue(comp_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("START_YMD")).setValue(start_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("END_YMD")).setValue(end_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_USER_NM")).setValue(pj_user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_TEL_NO")).setValue(pj_tel_no);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQ_USER_NM")).setValue(req_user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQ_USER_TEL_NO")).setValue(req_user_tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQ_USER_EMAIL")).setValue(req_user_email);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_IMP")).setValue(proj_imp);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_EMER")).setValue(proj_emer);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INDUS_CL_CD")).setValue(indus_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PROJ_CL_CD")).setValue(proj_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ESSEN_TECH_DESC")).setValue(essen_tech_desc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_REMARK")).setValue(pj_remark);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REQU_CNT_PRE")).setValue(requ_cnt_pre);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_AREA_CD")).setValue(pj_area_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_ETC")).setValue(pj_etc);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ORDER_YMD")).setValue(order_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ORDER_END_YMD")).setValue(order_end_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PJ_AREA_SUB")).setValue(pj_area_sub);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_ID")).setValue(comp_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMP_NM")).setValue(comp_nm);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doRevSave();
}

//데이터 수정sql
function doRevSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
	    var SERVLETURL = G_SERVLETURL + "?mod=saveProj&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

function checkRowsFRev() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return false;
	}
	return true;
}

//삭제 버튼 기능
function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) {

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteProj&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();">
<!-- <form name="form" method="post">
<input type="file" name= "browseFile" style="display='none';">  

</form> -->
<div id="gridbox" name="gridbox" height="98%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=0"/>
</jsp:include> --%>
</body>
</html>
