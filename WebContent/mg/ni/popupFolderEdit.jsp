<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<% 
IUser s_user = (IUser) session.getAttribute("j_user");
String type= StringUtils.paramReplace(request.getParameter("type"));
String retfunc= StringUtils.paramReplace(request.getParameter("retfunc"));
String roll= StringUtils.paramReplace(request.getParameter("roll"));
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>폴더 수정</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var menu_id 		= opener.check_menu_id;
var menu_nm			= opener.menu_nm;


var G_SERVLETURL = "/mg/menu.do";
var V_SERVLETURL = "/vdr/explorer.do";
var U_SERVLETURL = "/mg/Upload.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	GridObj = new dhtmlXGridObject('gridbox');
	
	document.getElementById('FOLDER_NM').value = menu_nm;
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	GridObj.setHeader("#master_checkbox,폴더 명,FI,PI");
	GridObj.setInitWidths("0,0,0,0");
	GridObj.setColAlign("center,center,center,center");
	GridObj.setColumnIds("SELECTED,FOLDER_NM,FOLDER_ID,P_FOLDER_ID");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str,str");
	GridObj.setColTypes("ch,edtxt,ro,ro");
	/* GridObj.attachEvent("onRowDblClicked",doOk); */

	GridObj.init();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId,cellInd) {
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	doAddFolder();
	opener.location.reload();
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	
	if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doAddFolder() {
	
	var folder_nm = document.getElementById('FOLDER_NM').value;
	
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("FOLDER_ID")).setValue(menu_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("FOLDER_NM")).setValue(folder_nm);

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doSaveFolder();
}

function doSaveFolder() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	for(var i=0; i < grid_array.length; i++) {
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("FOLDER_NM")).getValue() == "") {
			alert("폴더명을 입력해주세요");
			return;
		}
	}
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";

        var SERVLETURL = V_SERVLETURL + "?mod=NimsouCreateFolder&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		//parent.refresh();		
		opener.location.reload();
	    window.close();
	} else {
		alert(messsage);
	}
	return false;
}

function doApply()
{
	opener.location.reload();
    window.close();
}

function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>
</head>
<body class="hideScrollF" onload="init(); loadCombobox();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>폴더 수정</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="20%" />
            <col width="43%" />
            <col width="25%" />
            <col width="10%" />          
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large">폴더 명</td>
    <td>
    	<input type="text" name="FOLDER_NM" id="FOLDER_NM" size="20">
    </td>
    <td></td>
    <td></td>
  </tr>
</table>
<table width="100%">
<tr>
   	<td>
    	<div class="btnarea" style='margin:15px 0px 0px 0px'>
        	<a href="javascript:doAddFolder();" class="btn"><span>폴더 수정</span></a>
        </div>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="0%" height="0%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=0"/>
</jsp:include>
</body>
</html>
