<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>

<html>
<head>

<title>Insert title here</title>

<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;
var cell1;
var cell2;
var cell3;

function init() {

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "3E",
	    skin: "dhx_web"
	});
	
	
	cell1 = layout.cells('a');

	//cell1.setText("<table style='padding:0; margin0; width:98%; height:100%;'><tr><td id='caption'>"+leftMenuName+"</td></tr></table>");
	cell1.setHeight(350);
	cell1.setText(leftMenuName);
	
	cell1.attachURL("../../mg/ni/ni_RecruitMgmt.jsp", null,  {
		formid: "<%=formId%>",
		gridid: "<%=grid_obj%>",
		user_id: ""
	});
	
	cell2 = layout.cells('b');

	cell2.hideHeader();
	cell2.setHeight(200);
	
	cell2.attachURL("../../mg/ni/ni_RecruitSubFir.jsp", null,  {
		formid: "<%=formId%>01",
		gridid: "<%=grid_obj%>",
		user_id: ""
	});
	
	
	cell3 = layout.cells('c');

	cell3.hideHeader();
	cell3.setHeight(200);
	
	cell3.attachURL("../../mg/ni/ni_RecruitSubSec.jsp", null,  {
		formid: "<%=formId%>02",
		gridid: "<%=grid_obj%>",
		user_id: ""
	});
}

function refresh(proj_no, seq) 
{	
	cell2.attachURL("../../mg/ni/ni_RecruitSubFir.jsp", null,  {
		formid: "<%=formId%>01",
		gridid: "<%=grid_obj%>",
		proj_no: proj_no,
		seq: seq
	});
	
	cell3.attachURL("../../mg/ni/ni_RecruitSubSec.jsp", null,  {
		formid: "<%=formId%>02",
		gridid: "<%=grid_obj%>",
		proj_no: proj_no,
		seq: seq
	});
}

function resize() {
	layout.setSizes();
}

</script>

<style>
    #layoutObj {
        width: 100%;
        height: 92%;
        margin: 0px;
        overflow: hidden;
    }
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
</style>

</head>

<body onload="init();" onresize="resize();">

<div id="layoutObj" style="position: relative;"></div>

</body>
</html>