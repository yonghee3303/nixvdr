<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>인재풀 수정</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum;

var G_SERVLETURL = "<%=contextPath%>/ni/man.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	opener.getParentData();
	
	document.getElementById('MAN_ID').value = opener.man_id;
	document.getElementById('MAN_NM').value = opener.man_nm;
	document.getElementById('MAN_ENM').value = opener.man_enm;
	var reg_noA = opener.reg_no;
	var reg_nof = reg_noA.substr(0, 6);
	var reg_nob = reg_noA.substr(6);
	document.getElementById('REG_NOF').value = reg_nof;
	document.getElementById('REG_NOB').value = reg_nob;
	document.getElementById('NATIONAL_CD').value = opener.national_cd;
/* 	var addrA = opener.addr;
	var addrZ = addrA.substr(0, 7);
	var addrD = addrA.substr(7);
	var addrT = addrZ.replace(/\(/g, '');
	var addrTT = addrT.replace(/\)/g, '');
	document.getElementById('ADDR_ZIP').value = addrTT;
	document.getElementById('ADDR').value = addrD; */
	document.getElementById('ADDR').value = opener.addr;
	document.getElementById('TEL_NO').value = opener.tel_no;
	document.getElementById('EMAIL').value = opener.email;
	var edu_cl_cdA = opener.edu_cl_cd;
	if ("고졸" == edu_cl_cdA){
		edu_cl_cdA = "1"
	} else if ("초대졸" == edu_cl_cdA) {
		edu_cl_cdA = "2"
	} else if ("대졸" == edu_cl_cdA) {
		edu_cl_cdA = "3"
	} else if ("학사" == edu_cl_cdA) {
		edu_cl_cdA = "4"
	} else if ("석사" == edu_cl_cdA) {
		edu_cl_cdA = "5"
	} else if ("박사" == edu_cl_cdA) {
		edu_cl_cdA = "6"
	} else {
		edu_cl_cdA = null
	}
	document.form.EDU_CL_CD.value = edu_cl_cdA;
	var indus_cl_cdA = opener.indus_cl_cd;
	if ("일반" == indus_cl_cdA){
		indus_cl_cdA = "1"
	} else if ("금융" == indus_cl_cdA) {
		indus_cl_cdA = "2"
	} else if ("공공" == indus_cl_cdA) {
		indus_cl_cdA = "3"
	} else if ("그외" == indus_cl_cdA) {
		indus_cl_cdA = "4"
	} else if ("공공/금융" == indus_cl_cdA) {
		indus_cl_cdA = "5"
	} else {
		indus_cl_cdA = null
	}
	document.form.INDUS_CL_CD.value = indus_cl_cdA;
	var qual_cl_cdA = opener.qual_cl_cd;
	if ("산업기사" == qual_cl_cdA){
		qual_cl_cdA = "1"
	} else if ("정보처리기사" == qual_cl_cdA) {
		qual_cl_cdA = "2"
	} else {
		qual_cl_cdA = null
	}
	document.form.QUAL_CL_CD.value = qual_cl_cdA;
	var licen_yma = opener.licen_ym;
	if(licen_yma != ''){
	document.getElementById('LICEN_YM').value = licen_yma.substring(0,4)+"-"+licen_yma.substring(4,7)
	}
	document.getElementById('AGENCY_NM').value = opener.agency_nm;
	document.getElementById('AGENCY_ID').value = opener.agency_id;
	document.getElementById('MAJOR_DESC').value = opener.major_desc;
	var tech_cl_cdA = opener.tech_cl_cd;
	var tech_cdArray = tech_cl_cdA.split(',');
	for(var j = 0; j < tech_cdArray.length; j++) {
		if ("Java" == tech_cdArray[j]){
			tech_cdArray[j] = "1"
		} else if ("Oracle" == tech_cdArray[j]) {
			tech_cdArray[j] = "2"
		} else if ("Spring" == tech_cdArray[j]) {
			tech_cdArray[j] = "3"
		} else if ("Egov" == tech_cdArray[j]) {
			tech_cdArray[j] = "4"
		} else if ("AWS" == tech_cdArray[j]) {
			tech_cdArray[j] = "5"
		} else if ("NEXACRO" == tech_cdArray[j]) {
			tech_cdArray[j] = "6"
		} else if ("Rpa" == tech_cdArray[j]) {
			tech_cdArray[j] = "7"
		} else if ("Windows" == tech_cdArray[j]) {
			tech_cdArray[j] = "8"
		} else if ("Linux" == tech_cdArray[j]) {
			tech_cdArray[j] = "9"
		} else if ("보안" == tech_cdArray[j]) {
			tech_cdArray[j] = "10"
		} else if ("이관" == tech_cdArray[j]) {
			tech_cdArray[j] = "11"
		} else {
			tech_cdArray[j] = null
		}
	}
	
	var chk = $('input:checkbox[name=TECH_CL_CDN]');
	for(var i = 0; i < tech_cdArray.length; i++) {
		var ii = tech_cdArray[i];
		/* document.getElementsByName('TECH_CL_CDN')[ii].checked = true; */
		 chk.filter('[value=' + ii + ']').prop('checked', true);
	}
	document.getElementById('SKILL_ETC').value = opener.skill_etc;
	var evlu_cdA = opener.evlu_cd;
	if ("미달" == evlu_cdA){
		evlu_cdA = "1"
	} else if ("보통" == evlu_cdA) {
		evlu_cdA = "2"
	} else if ("우수" == evlu_cdA) {
		evlu_cdA = "3"
	} else if ("최우수" == evlu_cdA) {
		evlu_cdA = "4"
	} else {
		evlu_cdA = null
	}
	document.form.EVLU_CD.value = evlu_cdA;
	document.getElementById('EVLU_COMMT').value = opener.evlu_commt;
	var black_ynA = opener.black_yn;
	if (black_ynA == "Y"){
		black_ynA = true;
	} else {
		black_ynA = false;
	}
	document.getElementById('BLACK_YN').checked = black_ynA;
	document.getElementById('BLACK_REASON').value = opener.black_reason;
	var cur_yma = opener.cur_ym;
	if(cur_yma != ''){
	document.getElementById('CUR_YM').value = cur_yma.substring(0,4)+"-"+cur_yma.substring(4,7);
	}
	var cont_cl_cdA = opener.cont_cl_cd;
	if ("자사" == cont_cl_cdA){
		cont_cl_cdA = "1"
	} else if ("프리" == cont_cl_cdA) {
		cont_cl_cdA = "2"
	} else if ("외주" == cont_cl_cdA) {
		cont_cl_cdA = "3"
	} else {
		cont_cl_cdA = null
	}
	document.form.CONT_CL_CD.value = cont_cl_cdA;
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doRev()
{
	var man_id = document.getElementById('MAN_ID').value;
	var man_nm = document.getElementById('MAN_NM').value;
	var man_enm = document.getElementById('MAN_ENM').value;
	var reg_nof = document.getElementById('REG_NOF').value;
	var reg_nob = document.getElementById('REG_NOB').value;
	var reg_no = reg_nof + reg_nob;
	var national_cd = document.getElementById('NATIONAL_CD').value;
/* 	var addr_z = document.getElementById('ADDR_ZIP').value;
	var addr_d = document.getElementById('ADDR').value;
	var addr = "(" + addr_z + ")" + addr_d */
	var addr = document.getElementById('ADDR').value;
	var tel_no = phoneFomatter(document.getElementById('TEL_NO').value);
	var email = document.getElementById('EMAIL').value;
	var indus_cl_cd = RTrim(document.form.INDUS_CL_CD.value);
	var edu_cl_cd = RTrim(document.form.EDU_CL_CD.value);
	var qual_cl_cd = RTrim(document.form.QUAL_CL_CD.value);
	var licen_yma = document.getElementById('LICEN_YM').value;
	var licen_ym = licen_yma.replace(/\-/g, '').substring(0,6);
	var agency_nm = document.getElementById('AGENCY_NM').value;
	var agency_id = document.getElementById('AGENCY_ID').value;
	var major_desc = document.getElementById('MAJOR_DESC').value;
	var arrTech = "";
	var arrTechC = document.getElementsByName('TECH_CL_CDN');
	if(arrTechC.length > 0) {
		for (var i=0; i<arrTechC.length; i++) {
			if (arrTechC[i].checked == true) {
	        	arrTech += arrTechC[i].value + ','
	        }
		}
		arrTech = arrTech.substring(arrTech,arrTech.length-1);
	}
	var tech_cl_cd = arrTech;
	var skill_etc = document.getElementById('SKILL_ETC').value;
	var evlu_cd = RTrim(document.form.EVLU_CD.value);
	var evlu_commt = document.getElementById('EVLU_COMMT').value;
	var black_yn = document.getElementById('BLACK_YN').checked;
	var black_reason = document.getElementById('BLACK_REASON').value;
	var cur_yma = document.getElementById('CUR_YM').value;
	var cur_ym = licen_yma.replace(/\-/g, '').substring(0,6);
	var cont_cl_cd = RTrim(document.form.CONT_CL_CD.value);
	
	if (black_yn == true){
		black_yn = "Y";
	} else {
		black_yn = "N";
	}
	
	if (man_nm == "") {
		alert("성명을 입력해주세요.");
		return;
/* 	} else if (!email_check(email)) {
		alert("정확한 이메일 주소를 입력해주세요.");
		return;
	} else if (agency_id == "") {
		alert("소속사를 선택해주세요.");
		return;
 	} else if (addr_z == "") {
		alert("주소를 입력해주세요. *우편번호 필수입력.");
		return; */
	} else {
		opener.man_id = man_id;
		opener.man_nm = man_nm;
		opener.man_enm = man_enm;
		opener.reg_no = reg_no;
		//opener.reg_nof = reg_nof;
		//opener.reg_nob = reg_nob;
		opener.national_cd = national_cd;
		opener.addr = addr;
		opener.tel_no = tel_no;
		opener.email = email;
		opener.indus_cl_cd = indus_cl_cd;
		opener.edu_cl_cd = edu_cl_cd;
		opener.qual_cl_cd = qual_cl_cd;
		opener.licen_ym = licen_ym;
		opener.agency_nm = agency_nm;
		opener.agency_id = agency_id;
		//opener.agency_nmf = agency_nmf;
		//opener.agency_nmb = agency_nmb;
		opener.major_desc = major_desc;
		opener.tech_cl_cd = tech_cl_cd;
		opener.skill_etc = skill_etc;
		opener.evlu_cd = evlu_cd;
		opener.evlu_commt = evlu_commt;
		opener.black_yn = black_yn;
		opener.black_reason = black_reason;
		opener.cur_ym = cur_ym;
		opener.cont_cl_cd = cont_cl_cd;
		
		opener.getRevPopupData();
		
		window.close();
	}
}

function doRevEnd()
{
}

function doFileUpload(){
	var man_id = document.getElementById('MAN_ID').value;
	
	popupNimsAttachFile("File Upload", '', '', 500, 400, "manProfile", man_id, "", "write",<%=pm.getString("component.contextPath.root")%>);
 }

//첨부파일 관리  POPUP을 띄운다.
function popupNimsAttachFile(title, left, top, width, height, pgmId, docId, authority, callmode,root) {
    if (title == '') title = 'File Upload';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
    
    var url ="";
    
    if(root == false) {
    	url = contextPath;
    }
    
    if(callmode == "write") {
       url += "/mg/ni/popupNimsAttachFile.jsp";
    }else if(callmode=="repacking"){
    	url += "/mg/common/popupAttachFile_repacking.jsp";
    	resizable='no';
    }else if(callmode=="readPortal"){
    	url += "/mg/common/popupAttachFileRead_portal.jsp";
    	resizable='no';
    }else if(callmode=="readRepacking"){
    	url += "/mg/common/popupAttachFileSingleRead_portal.jsp";
    	resizable='no';
    }else {
    	url += "/mg/common/popupAttachFileRead.jsp";
    }
    
    if(callmode=="readRepacking"){
    	url +="?formid=POP_0109&gridid=GridObj";
    }else{
    	url += "?formid=POP_0106&gridid=GridObj";
    }
    url += "&pgmId=" + pgmId + "&docId=" + docId + "&authority=" + authority;
    if(callmode=="repacking"){
    	url += "&docTitle="+authority;
    }
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];
	
	var ret = window.open(url, "upload", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

//전화번호 자동 하이픈 입력
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 4) {
        return number;
    } else if(number.length < 7) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3);
    } else if(number.length == 9) {
        phone += number.substr(0, 2);
        phone += "-";
        phone += number.substr(2, 3);
        phone += "-";
        phone += number.substr(5);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

//이메일 양식 체크
function email_check(email) {
	var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	return (email != '' && email != 'undefined' && regex.test(email));
}

//소속사 선택 팝업
function doAgencyPop() {
	popupAgency("<%=mm.getMessage("NIMG_1032", s_user.getLocale())%>", '', '', 540, 400, "plant", "OkPopupAgency", <%=pm.getString("component.contextPath.root")%>);
}

function OkPopupAgency(agency_nm, agency_id) {
	document.getElementById('AGENCY_NM').value = agency_nm;
	document.getElementById('AGENCY_ID').value = agency_id;	
}

/* function goJusoPopup(){
	// 주소검색을 수행할 팝업 페이지를 호출합니다.
	// 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(https://www.juso.go.kr/addrlink/addrLinkUrl.do)를 호출하게 됩니다.
	var pop = window.open("jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes"); 
	
	// 모바일 웹인 경우, 호출된 페이지(jusopopup.jsp)에서 실제 주소검색URL(https://www.juso.go.kr/addrlink/addrMobileLinkUrl.do)를 호출하게 됩니다.
    //var pop = window.open("/popup/jusoPopup.jsp","pop","scrollbars=yes, resizable=yes"); 
}

function jusoCallBack(roadAddrPart1, addrDetail, zipNo){
	// 팝업페이지에서 주소입력한 정보를 받아서, 현 페이지에 정보를 등록합니다.
	document.form.ADDR.value = zipNo + roadAddrPart1 + addrDetail;
} */

//달력
function init() {
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"LICEN_YM",button:"calendar_icon"});
	calendar1 = new dhtmlXCalendarObject({input:"CUR_YM",button:"calendar_icon1"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>수정</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="15%" />
            <col width="43%" />
            <col width="15%" />
            <col width="25%" />          
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1021", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="MAN_ID" id="MAN_ID" size="20" placeholder="자동부여" readonly>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1022", s_user.getLocale())%><a> *</a></td>
    <td>
    	<input type="text" name="MAN_NM" id="MAN_NM" size="20" maxlength="128">
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1023", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="MAN_ENM" id="MAN_ENM" size="14" maxlength="128">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1024", s_user.getLocale())%></td>
    <td>
    	<input type="number" name="REG_NOF" id="REG_NOF" size="14" maxlength="4" oninput="numberMaxLength(this);" placeholder="YYYY or YYMMDD">
    	-
    	<input type="number" name="REG_NOB" id="REG_NOB" size="14" maxlength="7" oninput="numberMaxLength(this);" class="input-number-password">
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1025", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="NATIONAL_CD" id="NATIONAL_CD" size="14" maxlength="6">
    </td>
  </tr>
  <%-- <tr>
    <td class="fs_large"><%=mm.getMessage("NIMP_1014", s_user.getLocale())%><a>*</a></td>
    <td>
    	<input type="text" name="ADDR_ZIP" id="ADDR_ZIP" size="14" maxlength="5" class="text" style='font-size:16px'>
    	<a href="javascript:goJusoPopup();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>
    	<input type="text" name="ADDR" id="ADDR" size="35" maxlength="100" class="text" style='font-size:16px'>
    </td>
    <td></td>
    <td></td>
  </tr> --%>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1014", s_user.getLocale())%><a> *</a></td>
    <td>
    	<input type="text" name="ADDR" id="ADDR" size="44" maxlength="100">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1027", s_user.getLocale())%></td>
    <td>
    <input type="text" onKeyup="inputPhoneNumber(this);" name="TEL_NO" id="TEL_NO" size="18" maxlength="13" oninput="numberMaxLength(this);">
    </td>
    <td class="fs_large">계약구분</td>
    <td>
    	<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1114"/>
				<jsp:param name="tagname" value="CONT_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1028", s_user.getLocale())%><a></a></td>
    <td>
    	<input type="email" name="EMAIL" id="EMAIL" size="44" maxlength="64">
    </td>
    <td class="fs_large">경력기준연월</td>
    <td>
    	<input type="text" name="CUR_YM" id="CUR_YM" size="14" maxlength="10"> <span><img id="calendar_icon1" src="../../../ext/images/calendar.png" border="0"></span>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102C", s_user.getLocale())%><a></a></td>
    <td>
    	<input type="text" name="AGENCY_NM" id="AGENCY_NM" size="17" maxlength="50" readonly>
    	<a href="javascript:doAgencyPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" style='margin-bottom:4px; margin-right:2px;'/></a>
    	<input type="text" name="AGENCY_ID" id="AGENCY_ID" size="20" maxlength="50" readonly>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102D", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="MAJOR_DESC" id="MAJOR_DESC" size="14" maxlength="256">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1029", s_user.getLocale())%><a> *</a></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1101"/>
				<jsp:param name="tagname" value="EDU_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1046", s_user.getLocale())%><a> *</a></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1107"/>
				<jsp:param name="tagname" value="INDUS_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102A", s_user.getLocale())%><a> *</a></td>
    	<td>
			<jsp:include page="../../mg/ni/comboCode.jsp" >
				<jsp:param name="codeId" value="A1102"/>
				<jsp:param name="tagname" value="QUAL_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    	</td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102B", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="LICEN_YM" id="LICEN_YM" size="14" maxlength="10"> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
    </td>
  </tr>
    <tr>
    <td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_105D", s_user.getLocale())%></td>
    <td colspan='3'>
    	<jsp:include page="../../mg/ni/checkboxCodeTech.jsp" >
				<jsp:param name="codeId" value="A1106"/>
				<jsp:param name="tagname" value="TECH_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102E", s_user.getLocale())%></td>
    <td colspan='3'>
    	<input type="text" name="SKILL_ETC" id="SKILL_ETC" size="86" maxlength="256">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102F", s_user.getLocale())%><a> *</a></td>
    <td colspan='3'>
		<jsp:include page="../../mg/ni/comboCode.jsp" >
			<jsp:param name="codeId" value="A1103"/>
			<jsp:param name="tagname" value="EVLU_CD"/>
			<jsp:param name="def" value=" "/>
		</jsp:include>
     </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102G", s_user.getLocale())%></td>
    <td colspan='3'>
    	<input type="text" name="EVLU_COMMT" id="EVLU_COMMT" size="87" maxlength="128">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102H", s_user.getLocale())%></td>
    <td>
    	<input type="checkbox" name="BLACK_YN" id="BLACK_YN">
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102I", s_user.getLocale())%></td>
    <td colspan='3'>
    	<input type="text" name="BLACK_REASON" id="BLACK_REASON" size="87" maxlength="128">
    </td>
  </tr>
<%--   <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_102J", s_user.getLocale())%></td>
    <td colspan='3'>
    	<input type="text" name="MAN_PROFILE" id="MAN_PROFILE" size="75" maxlength="1024" class="text" readonly>
    	<div class="btnarea" style='margin:15px 0px 0px 0px'>
    		<a href="javascript:doFileUpload();" class="btn"><span>파일 업로드</span></a>
    	</div>
    </td>
  </tr> --%>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea" style='margin:15px 0px 0px 0px'>
	        	<a href="javascript:doFileUpload();" class="btn"><span>프로필 업로드</span></a>
	        	<a href="javascript:doRev();" class="btn"><span><%=mm.getMessage("NIMG_1007", s_user.getLocale())%></span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
