<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String userNm = s_user.getId();
int filterUser = 1;
if("S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString()) || "S03".equals(s_user.getProperty("USER_ROLL").toString()) || "S08".equals(s_user.getProperty("USER_ROLL").toString())) {
	filterUser = 0;
} else {
	filterUser = 1;
}
%>

<html>
<head>
<title>원가관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%> <%@ include file="../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/ni/contact.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	//기존 소스
	//GridObj = setGridDraw(GridObj);
	//contextPath 추가
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);	//contextPath가 root면 true 아니면 false
	GridObj.setSizes();
	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
		
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	
	var filter_user = <%=filterUser%>;
	var user_nm = "<%=userNm %>";

	var man_nm = document.form.MAN_NM.value;
	var agency_nm = document.form.AGENCY_NM.value;
	var cont_cl_cdA = document.form.CONT_CL_CD.value;
	var cont_cl_cd = cont_cl_cdA.trim();
	var proj_no = document.form.PROJ_NO.value;
	var proj_nm = document.form.PROJ_NM.value;
	
	
	var grid_col_id  = "<%=grid_col_id%>";
	var argu  = "&MAN_NM="	+encodeURIComponent(man_nm);
	argu  += "&AGENCY_NM="	+encodeURIComponent(agency_nm);
	argu  += "&CONT_CL_CD="	+encodeURIComponent(cont_cl_cd);
	argu  += "&PROJ_NO="	+encodeURIComponent(proj_no);
	argu  += "&PROJ_NM="	+encodeURIComponent(proj_nm);
	
	if(filter_user==(1)){
		argu += "&FILTER_USER="	+encodeURIComponent(user_nm);
	}
			
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUnitPrice&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%>&nbsp;&nbsp;/&nbsp;&nbsp;단위 : 천원";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doSave() {
	if(!checkRows()) {
		alert("저장할 데이터를 체크해주세요.");
		return;
	}
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
		
        var SERVLETURL = G_SERVLETURL + "?mod=saveUnitPrice&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

//삭제 버튼 기능
function doDelete() {
	if(!checkRows()) return;
	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) {

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteUnitPrice&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

//프로젝트 선택 팝업
function doProjectPop() {
	popupProject("<%=mm.getMessage("NIMG_1059", s_user.getLocale())%>", '', '', 620, 500, "plant", "OkPopupProject", <%=pm.getString("component.contextPath.root")%>);
}

function popupProject(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '프로젝트선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupProject.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupProject.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function OkPopupProject(proj_no, proj_nm) {
	document.getElementById('PROJ_NO').value = proj_no;
	document.getElementById('PROJ_NM').value = proj_nm;	
}

//달력
function init() {
<%-- 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 		
	calendar = new dhtmlXCalendarObject({input:"PROJ_YM",button:"calendar_icon"});

	calendar.loadUserLanguage("<%=s_user.getLocale()%>"); --%>
	
	setFormDraw();
}


</script>

<style>/*계약 세부 내용에서  계약구분 셀렉 박스 스타일 시트*/
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px" />
            <col width="120px" />
            <col width="80px" />
            <col width="80px" />
            <col width="100px" />
            <col width="160px" />
            <col width="40px" />
            <col width="180px" />
            <col/>
        </colgroup>
        <tr>
<!--         	<td class="tit">기간</td>
		    <td>
		    	<input type="text" name="PROJ_YM" id="PROJ_YM" size="14" maxlength="10"><span style="margin:6px 0 0 4px;"><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
		    </td> -->
        	<td class="tit">계약자 명</td>
            <td>
				<input type="text" name="MAN_NM" id="MAN_NM" size="14" class="text">
			</td>
			<td class="tit">계약구분</td>
            <td>
				<jsp:include page="/mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1114"/>
					<jsp:param name="tagname" value="CONT_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
	    	<td class="tit"><%=mm.getMessage("NIMG_1059", s_user.getLocale())%></td>
            <td>
				<input type="text" name="PROJ_NO" id="PROJ_NO" size="22" class="text">
			</td>
			<td>
				<a href="javascript:doProjectPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
			</td>
			<td colspan="2">
				<input type="text" name="PROJ_NM" id="PROJ_NM" size="26" class="text">
			</td>
		</tr>
		<tr>
			<td class="tit">소속사</td>
            <td colspan="7">
				<input type="text" name="AGENCY_NM" id="AGENCY_NM" size="14" class="text">
			</td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("NIMG_1003", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox"  height="88%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=142"/>
</jsp:include> --%>
</body>
</html>
