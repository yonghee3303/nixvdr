<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String userNm = s_user.getId();
int filterUser = 1;
if("S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString()) || "S03".equals(s_user.getProperty("USER_ROLL").toString()) || "S08".equals(s_user.getProperty("USER_ROLL").toString())) {
	filterUser = 0;
} else {
	filterUser = 1;
}
%>

<html>
<head>
<title>계약조회(월)</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="../../ext/js/jquery.mtz.monthpicker.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="../../ext/js/jquery.ui.monthpicker.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/contact.do";

//그리드폼
function setFormDraw() {	
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);

	GridObj._in_header_stat_sum=function(tag,index,data){       
	    var calck=function(){                              
	        var sum=0;                                     
	        this.forEachRow(function(id){                   
	            sum+=this.cellById(id,index).getValue()*1;     
	        })
	        sum=sum.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	    	return this._aplNF(sum,0);
	    };
	    this._stat_in_header(tag,calck,index,data);
	}

	/* GridObj.attachFooter("합계,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#stat_sum,#stat_sum,#stat_sum,#stat_sum,#stat_sum,#stat_sum"
						,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","","","","","","","","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);
 */	
 	GridObj.attachFooter("합계,#cspan,#cspan,<div id='tot'>0</div>,<div id='m_a'>0</div>,<div id='m_b'>0</div>,<div id='m_c'>0</div>,<div id='am_a'>0</div>,<div id='am_b'>0</div>,<div id='am_c'>0</div>"
			,["font-family:Arial;font-style:normal;text-align:center;height:35px;background-color: white;","","","font-family:Arial;font-style:normal;text-align:center;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;","font-family:Arial;font-style:normal;text-align:right;background-color:#ebf3ff;"]);

	GridObj.setSizes();
	
	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	
	var filter_user = <%=filterUser%>;
	var FUtoS = filter_user.toString;
	var user_nm = "<%=userNm %>";
	
	var start_yma = document.form.START_YM.value;
	var proj_nm = document.form.PROJ_NM.value;
	var user_id = document.form.USER_ID.value.trim();
	var start_day = "01";
	
	var start_ym = start_yma.replace(/\-/g, '');
	var end_ym = start_ym;
	
	var whatYear = start_ym.substring(0, 4);
	var whatMon = start_ym.substring(4, 6);

	var end_day = new Date(whatYear, whatMon, 0).getDate();
	
	start_ym = start_ym+start_day;
	end_ym = end_ym+end_day;
	
	var grid_col_id  = "<%=grid_col_id%>";
	var argu  = "&START_YM="	+encodeURIComponent(start_ym);
		argu += "&END_YM="	+encodeURIComponent(end_ym);
		argu += "&PROJ_NM="	+encodeURIComponent(proj_nm);
		argu += "&USER_ID="	+encodeURIComponent(user_id);
		argu += "&END_DAY="	+encodeURIComponent(end_day);
		
		if(filter_user==(1)){
			argu += "&FILTER_USER="	+encodeURIComponent(user_nm);
		}
		
	GridObj.loadXML(G_SERVLETURL+"?mod=contractSelectProj&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	
	calculateFooterValues();
	
	if(status == "false") alert(msg);
	return true;
}

function calculateFooterValues(){
	var tot = document.getElementById("tot");
		tot.innerHTML = sumColumnA(3);
	var m_a = document.getElementById("m_a");
		m_a.innerHTML = sumColumnA(4);
	var m_b = document.getElementById("m_b");
		m_b.innerHTML = sumColumnA(5);
	var m_c = document.getElementById("m_c");
		m_c.innerHTML = sumColumnA(6);
	var am_a = document.getElementById("am_a");
		am_a.innerHTML = sumColumnA(7);
	var am_b = document.getElementById("am_b");
		am_b.innerHTML = sumColumnA(8);
	var am_c = document.getElementById("am_c");
		am_c.innerHTML = sumColumnA(9);
	return true;
}

function sumColumnA(ind){
	var out = 0;
	for(var i=1;i<=GridObj.getRowsNum();i++){
		out += parseFloat(GridObj.cells(i,ind).getValue());
	}
	out = out.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	return out;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};
	
		$("#START_YM").monthpicker(options);
	
	
	});

function init() {
	var now = new Date();
	document.getElementById('START_YM').value = new Date().toISOString().substring(0, 7);
	setFormDraw();
}

</script>

<style>
select { /*계약구분 / 담당자 셀렉박스 스타일 시트*/
		padding: .1em .1em;
		border: 1px solid #E1E1E1;
		font-family: inherit;
		border-radius: 0px;
	}
</style>

</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
            <col width="180px" />
            <col width="60px" />
            <col width="120px" />
            <col width="70px" />
            <col width="100px" />
            <col/>
        </colgroup>
        <tr>
	        <td class="tit">프로젝트</td>
	        <td>
				<input type="text" name="PROJ_NM" id=""PROJ_NM"" size="13">
			</td>
        	<td class="tit">기간</td>
            <td>
            	<input type="text" name="START_YM" id="START_YM" size="7" maxlength="7" style="padding: 5px 0 0 0;">
            </td>
            <td class="tit">담당자</td>
            <td>
				<jsp:include page="/mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1199"/>
					<jsp:param name="tagname" value="USER_ID"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
            <td></td>
        </tr>
		</table>
		
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMG_1004", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="91%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>