<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();

String userID = s_user.getId();
%>

<html>
<head>
<title>비밀번호 변경</title>
<%@ include file="/ext/include/include_css.jsp"%>
<%@ include file="/ext/include/su_grid_common.jsp"%>
<%@ include file="/ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var pwdA			= "";
var pwdB			= "";

var G_SERVLETURL = "<%=contextPath%>/mg/user.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setHeader("#master_checkbox,passwd,prepass");
	GridObj.setInitWidths("0,0,0");
	GridObj.setColAlign("center,center,center");
	GridObj.setColumnIds("SELECTED,PASSWD,PRE_PASS");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str");
	GridObj.setColTypes("ch,ro,ro");

	document.getElementById('cur_user').value = "<%=userID%>";
	
		document.getElementById('new_Pwd').value = pwdA;
		document.getElementById('pwd_chk').value = pwdB;
	
	GridObj.init();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doPreCheckPw() {
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "SELECTED,PASSWD,PRE_PASS";
   	
	var pwd = document.getElementById('new_Pwd').value;
	var pre_Pwd = document.getElementById('pre_Pwd').value;
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PASSWD")).setValue(pwd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("PRE_PASS")).setValue(pre_Pwd);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doCheckPw();
}

function doCheckPw() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
        var cols_ids = "SELECTED,PASSWD,PRE_PASS";
	    var SERVLETURL = G_SERVLETURL + "?mod=personalCheckPW&col_ids="+cols_ids;
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

function doPreChangePw() {
	if(document.getElementById('new_Pwd').value != document.getElementById('pwd_chk').value) {
		alert("변경할 비밀번호가 일치하지 않습니다");
		return;
	}
	doChangePw();
}

function doChangePw() {
	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
        var cols_ids = "SELECTED,PASSWD,PRE_PASS";
	    var SERVLETURL = G_SERVLETURL + "?mod=personalSavePW&col_ids="+cols_ids;
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSave(){

}

function doSaveEnd(obj){
	var status   = obj.getAttribute("status");
	
	if(status == "noMatch") {
		alert("현재 비밀번호가 틀립니다.");
		
		pwdA = document.getElementById('new_Pwd').value;
		pwdB = document.getElementById('pwd_chk').value; 
		
		location.reload();
	} else if(status == "match") {
		doPreChangePw();
	} else {
		opener.pwdLogout();
		self.close();
	}
}

function init() {
	setFormDraw();
}

</script>
</head>
<body class="hideScrollF" onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>비밀번호 변경</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="30%" />
            <col width="68%" />     
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large" style="font-size:16">사용자 ID</td>
    <td>
    	<input type="text" name="cur_user" id="cur_user" readonly>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large" style="font-size:16">현재 비밀번호</td>
    <td>
    	<input type="password" name="pre_Pwd" id="pre_Pwd" placeholder="현재 비밀번호">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large" style="font-size:16">변경할 비밀번호</td>
    <td>
    	<input type="password" name="new_Pwd" id="new_Pwd" placeholder="변경할 비밀번호">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large" style="font-size:16">비밀번호 확인</td>
    <td>
    	<input type="password" name="pwd_chk" id="pwd_chk" placeholder="비밀번호 확인">
    </td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea">
	            	<a href="javascript:doPreCheckPw();" class="btn"><span>비밀번호변경</span></a>
	        </div>
        </td>
    </tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="0px" height="0px" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=0"/>
</jsp:include>
</body>
</html>
