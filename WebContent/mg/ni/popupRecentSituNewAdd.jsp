<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>인재 근황 등록</title>
<%@ include file="/ext/include/include_css.jsp"%>
<%@ include file="/ext/include/su_grid_common.jsp"%>
<%@ include file="/ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum

var G_SERVLETURL = "<%=contextPath%>/ni/man.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}


function doReset()
{
	document.getElementById('MAN_NM').value = "";
	document.getElementById('MAN_ID').value = "";
	document.form.INTVW_CL_CD.value = "";
	document.form.CONTACT_CL_CD.value = "";
	document.getElementById('BE_PROJ').value = "";
	document.getElementById('FREE_YMD').value = "";
	document.getElementById('CAN_PROJ_YMD').value = "";
	document.getElementById('NEXT_ALM_YMD').value = "";
	document.getElementById('INTVW_DESC').value = "";
	document.getElementById('INTVW_YMD').value = "";
	document.getElementById('INTVWETC').value = "";
}

function doAdd()
{
	var man_nm = document.getElementById('MAN_NM').value;
	var man_id = document.getElementById('MAN_ID').value;
	var intvw_cl_cd = RTrim(document.form.INTVW_CL_CD.value);
	var contact_cl_cd = RTrim(document.form.CONTACT_CL_CD.value);
	var be_proj = document.getElementById('BE_PROJ').value;
	var free_ymd = document.getElementById('FREE_YMD').value;
	var can_proj_ymd = document.getElementById('CAN_PROJ_YMD').value;
	var next_alm_ymd = document.getElementById('NEXT_ALM_YMD').value;
	var intvw_desc = document.getElementById('INTVW_DESC').value;
	var intvw_ymd = document.getElementById('INTVW_YMD').value;	
	var intvwetc = document.getElementById('INTVWETC').value;	
	
	if (man_nm == "") {
		alert("인재를 선택해주세요.");
		return;
	} else {
		opener.man_nm = man_nm;
		opener.man_id = man_id;
		opener.intvw_cl_cd = intvw_cl_cd;
		opener.contact_cl_cd = contact_cl_cd;
		opener.be_proj = be_proj;
		opener.free_ymd = free_ymd;
		opener.can_proj_ymd = can_proj_ymd;
		opener.next_alm_ymd = next_alm_ymd;
		opener.intvw_desc = intvw_desc;
		opener.intvw_ymd = intvw_ymd;
		opener.intvwetc = intvwetc;

		opener.getPopupData();
		
		window.close();
	}
}

function doAddEnd()
{
}

//인재 선택 팝업
function doManPop() {
	popupMan("<%=mm.getMessage("NIMG_1014", s_user.getLocale())%>", '', '', 540, 400, "plant", "OkPopupMan", <%=pm.getString("component.contextPath.root")%>);
}

function OkPopupMan(man_nm, man_id) {
	document.getElementById('MAN_NM').value = man_nm;
	document.getElementById('MAN_ID').value = man_id;	
}

//프로젝트 선택 팝업
function doProjectPop() {
	popupProject("<%=mm.getMessage("NIMG_1059", s_user.getLocale())%>", '', '', 620, 500, "plant", "OkPopupProject", <%=pm.getString("component.contextPath.root")%>);
}

function popupProject(title, left, top, width, height, type, retfunc, root) {
    if (title == '') title = '프로젝트선택';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    //기존 소스
    //var url = "/mg/common/popupDeptTree.jsp?retfunc=" + retfunc;
    //contextPath 추가
    var url;
    if(root == false) {
    	url = contextPath + "/mg/ni/popupProject.jsp?retfunc=" + retfunc;    	
    } else {
    	url = "/mg/ni/popupProject.jsp?retfunc=" + retfunc;
    }

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function OkPopupProject(proj_no, proj_nm) {
	document.getElementById('BE_PROJ').value = proj_nm;	
}

//달력
function init() {
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"FREE_YMD",button:"calendar_icon"});
	calendar2 = new dhtmlXCalendarObject({input:"NEXT_ALM_YMD",button:"calendar_icon2"});
	calendar3 = new dhtmlXCalendarObject({input:"INTVW_YMD",button:"calendar_icon3"});
	calendar4 = new dhtmlXCalendarObject({input:"CAN_PROJ_YMD",button:"calendar_icon4"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar3.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar4.loadUserLanguage("<%=s_user.getLocale()%>");
	
	document.getElementById('INTVW_YMD').value = new Date().toISOString().substring(0, 10);
	
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>인재 근황 등록</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="21%" />
            <col width="30%" />
            <col width="17%" />
            <col width="30%" />          
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1061", s_user.getLocale())%></td>
     <td colspan='3'>
    	<input type="text" name="MAN_NM" id="MAN_NM" size="10" maxlength="50" readonly>
    	<a href="javascript:doManPop();"><img src="/ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" style='margin-bottom:6px'/></a>
    	<input type="text" name="MAN_ID" id="MAN_ID" size="20" maxlength="50" readonly>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large">약속수단</td>
  	<td colspan='3'>
    	<jsp:include page="/mg/ni/comboCodeProj.jsp" >
				<jsp:param name="codeId" value="A1113"/>
				<jsp:param name="tagname" value="INTVW_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1063", s_user.getLocale())%></td>
  	<td colspan='3'>
    	<jsp:include page="/mg/ni/comboCodeProj.jsp" >
				<jsp:param name="codeId" value="A1112"/>
				<jsp:param name="tagname" value="CONTACT_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1064", s_user.getLocale())%></td>
    <td colspan='3'>
    	<input type="text" name="BE_PROJ" id="BE_PROJ" size="56" maxlength="64" maxlength="128">
    	<a href="javascript:doProjectPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" style='margin-bottom:6px'/></a>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large">인터뷰<br>가능일자</td>
  	<td>
  		<input type="text" name="FREE_YMD" id="FREE_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
 	</td>
 	<td class="fs_large">프로젝트투입<br>가능일자</td>
  	<td>
  		<input type="text" name="CAN_PROJ_YMD" id="CAN_PROJ_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon4" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
 	</td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1067", s_user.getLocale())%></td>
    <td colspan='3'>
    	<textarea rows="4" cols="60" id="INTVW_DESC" maxlength="256"></textarea>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1068", s_user.getLocale())%></td>
  	<td>
  		<input type="text" name="INTVW_YMD" id="INTVW_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon3" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
 	</td>
 	<td class="fs_large"><%=mm.getMessage("NIMP_1066", s_user.getLocale())%></td>
  	<td>
  		<input type="text" name="NEXT_ALM_YMD" id="NEXT_ALM_YMD" size="14" maxlength="64" readonly> <span><img id="calendar_icon2" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
 	</td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1069", s_user.getLocale())%></td>
    <td colspan='3'>
    	<textarea rows="4" cols="60" id="INTVWETC" maxlength="128"></textarea>
    </td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea">
	        	<a href="javascript:doReset();" class="btn"><span><%=mm.getMessage("NIMG_1008", s_user.getLocale())%></span></a>
	        	<a href="javascript:doAdd();" class="btn"><span><%=mm.getMessage("NIMG_1009", s_user.getLocale())%></span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
