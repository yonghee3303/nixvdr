<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String userNm = s_user.getId();
int filterUser = 1;
if("S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString()) || "S03".equals(s_user.getProperty("USER_ROLL").toString()) || "S08".equals(s_user.getProperty("USER_ROLL").toString())) {
	filterUser = 0;
} else {
	filterUser = 1;
}
%>

<html>
<head>
<title>인력급여지급 조회</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%> <%@ include file="../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="../../ext/js/jquery.mtz.monthpicker.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="../../ext/js/jquery.ui.monthpicker.js"></script>



<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/ni/contact.do";



// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	//기존 소스
	//GridObj = setGridDraw(GridObj);
	//contextPath 추가
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);	//contextPath가 root면 true 아니면 false
	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
		
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var pay_day_valA = document.getElementsByName('PAY_DAY');
	var pay_day_val;
	for (var i=0; i<pay_day_valA.length; i++) {
		if (pay_day_valA[i].checked == true) {
			pay_day_val = pay_day_valA[i].value;
        }
	}
	
	var filter_user = <%=filterUser%>;
	var user_nm = "<%=userNm %>";
	
	var start_yma = document.form.START_YM.value;
	var start_ym = start_yma.replace(/\-/g, '');

	var now_Month = start_ym.substring(4, 6);
	
	var pay_day = document.form.PAY_DAY.value;
	
	var grid_col_id  = "<%=grid_col_id%>";
	var argu  = "&PAY_DAY="	+encodeURIComponent(pay_day);
	
	if(pay_day_val == 10) {
		if(now_Month == 01) {
			var now_Y = start_ym.substring(0, 4)-1;
			var now_M = "12";
			
			var now_ym = now_Y + now_M;
			
			var start_day = "01";
			var end_ym = now_ym;
			alert(now_ym);
			var end_day = new Date(now_Y, now_M, 0).getDate();
			
			now_ym = now_ym+start_day;
			end_ym = end_ym+end_day;
		} else {
			var now_Y = start_ym.substring(0, 4);
			var now_M = start_ym.substring(4, 6);
			var now_ym = now_Y + now_M - 1;
			
			var start_day = "01";
			var end_ym = now_ym;

			var end_day = new Date(now_Y, now_M, 0).getDate();
			
			now_ym = now_ym+start_day;
			end_ym = end_ym+end_day;
		}
		
	} else if(pay_day_val == 21) {
		var now_Y = start_ym.substring(0, 4);
		var now_M = start_ym.substring(4, 6);
		
		var now_ym = now_Y + now_M;
		
		var start_day = "01";
		var end_ym = now_ym;

		var end_day = new Date(now_Y, now_M, 0).getDate();
		
		now_ym = now_ym+start_day;
		end_ym = end_ym+end_day;
	} else {
		return;
	}

	argu  += "&END_YM=" +encodeURIComponent(end_ym);
	argu  += "&START_YM=" +encodeURIComponent(now_ym);
	
	if(filter_user==(1)){
		argu += "&FILTER_USER="	+encodeURIComponent(user_nm);
	}
			
	GridObj.loadXML(G_SERVLETURL+"?mod=unitPaySelect&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doSave() {
	if(!checkRows()) {
		alert("저장할 데이터를 체크해주세요.");
		return;
	}
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("저장하시겠습니까?")) {
		var cols_ids = "<%=grid_col_id%>";

			var cont_rno = "T" + ranId;
			var argu  = "&CONT_RNO="	+cont_rno;
		
        var SERVLETURL = G_SERVLETURL + "?mod=contractSave&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=contractDelete&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function doExcelDown(){
	var filter_user = <%=filterUser%>;
	var user_nm = "<%=userNm %>";

	var grid_col_id  = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");

	var start_yma = document.form.START_YM.value;
	var start_ym = start_yma.replace(/\-/g, '');

	var now_Month = start_ym.substring(4, 6);
	
	var pay_day = document.form.PAY_DAY.value;
	
	var argu  = "&PAY_DAY="	+encodeURIComponent(pay_day);

	var pay_day_valA = document.getElementsByName('PAY_DAY');
	var pay_day_val;
	for (var i=0; i<pay_day_valA.length; i++) {
		if (pay_day_valA[i].checked == true) {
			pay_day_val = pay_day_valA[i].value;
        }
	}
	
	if(pay_day_val == 10) {
		if(now_Month == 01) {
			var now_Y = start_ym.substring(0, 4)-1;
			var now_M = "12";
			
			var now_ym = now_Y + now_M;
			
			var start_day = "01";
			var end_ym = now_ym;

			var end_day = new Date(now_Y, now_M, 0).getDate();
			
			now_ym = now_ym+start_day;
			end_ym = end_ym+end_day;
		} else {
			var now_Y = start_ym.substring(0, 4);
			var now_M = start_ym.substring(4, 6);

			var now_ym = now_Y + now_M;
			
			var start_day = "01";
			var end_ym = now_ym;

			var end_day = new Date(now_Y, now_M, 0).getDate();
			
			now_ym = now_ym+start_day;
			end_ym = end_ym+end_day;
		}
		
	} else if(pay_day_val == 21) {
		var now_Y = start_ym.substring(0, 4);
		var now_M = start_ym.substring(4, 6);
		
		var now_ym = now_Y + now_M;
		
		var start_day = "01";
		var end_ym = now_ym;

		var end_day = new Date(now_Y, now_M, 0).getDate();
		
		now_ym = now_ym+start_day;
		end_ym = end_ym+end_day;
	} else {
		return;
	}

	argu  += "&END_YM=" +encodeURIComponent(end_ym);
	argu  += "&START_YM=" +encodeURIComponent(now_ym);
		
	if(filter_user==(1)){
		argu += "&FILTER_USER="	+encodeURIComponent(user_nm);
	}
		
	fileDownload(G_SERVLETURL+"?mod=excelUnitPaySelect&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);	
}

//달력
function init() {
<%-- 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 		
	calendar = new dhtmlXCalendarObject({input:"PROJ_YM",button:"calendar_icon"});

	calendar.loadUserLanguage("<%=s_user.getLocale()%>"); --%>
	
	var now = new Date();
	document.getElementById('START_YM').value = new Date().toISOString().substring(0, 7);
	setFormDraw();
}

$(document).ready(function(){
var options = {
		monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
	        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
	    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	    showOn: "button",
	    buttonImage: "../../../ext/images/calendar.png",
	    buttonImageOnly: true,
	    changeYear: false,
	    yearRange: 'c-2:c+2',
	    dateFormat: 'yy-mm'
};

$("#START_YM").monthpicker(options);
});

</script>
<style>
select {
		padding: .1em .1em;
		border: 1px solid #E1E1E1;
		font-family: inherit;
		border-radius: 0px;
	}
</style>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="60px" />
        	<col width="180px" />
        	<col width="60px" />
        	<col width="180px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">기간</td>
		    <td>
		    	<input type="text" name="START_YM" id="START_YM" size="14" maxlength="10" style="padding: 5px 0 0 0;"><!-- <span style="margin:6px 0 0 4px;"><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span> -->
		    </td>
	        <td class="tit">지급일</td>
		    <td>
		    	<label><input type="radio" name="PAY_DAY" value="10" checked="checked">10일</label>
		    	<label><input type="radio" name="PAY_DAY" value="21">21일</label>
		    </td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doExcelDown();" class="btn"><span>엑셀 다운</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox"  height="91%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=142"/>
</jsp:include> --%>
</body>
</html>
