<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>인력 검색</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<style>
	.fs_large { font-size: 18px; }
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    	-webkit-appearance: none;
    }
    .input-number-password {
    	-webkit-text-security: disc;
	}
</style>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();

var sman_id = "";

var G_SERVLETURL = "<%=contextPath%>/ni/recruit.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	GridObj.setHeader("ID,성명,소속사,전화번호,주소,이메일,나이,자격구분,기사취득,전공,기타스킬,평가,평가코멘트,블랙,블랙사유");
	GridObj.setInitWidths("110,80,110,130,160,160,50,100,100,120,160,40,160,40,160");
	GridObj.setColAlign("center,center,center,center,left,left,center,center,center,left,left,center,left,center,left");
	GridObj.setColumnIds("MAN_ID,MAN_NM,AGENCY_ID,TEL_NO,ADDR,EMAIL,REG_NO,QUAL_CL_CD,LICEN_YM,MAJOR_DESC,SKILL_ETC,EVLU_CD,EVLU_COMMT,BLACK_YN,BLACK_REASON");
	GridObj.setColumnColor("##FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	GridObj.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	/* GridObj.setHeader("#master_checkbox,ID,성명,소속사,전화번호,주소,이메일,나이,자격구분,기사취득,전공,기타스킬,평가,평가코멘트,블랙,블랙사유");
	GridObj.setInitWidths("30,110,80,110,130,160,160,50,100,100,120,160,40,160,40,160");
	GridObj.setColAlign("center,center,center,center,center,left,left,center,center,center,left,left,center,left,center,left");
	GridObj.setColumnIds("SELECTED,MAN_ID,MAN_NM,AGENCY_ID,TEL_NO,ADDR,EMAIL,REG_NO,QUAL_CL_CD,LICEN_YM,MAJOR_DESC,SKILL_ETC,EVLU_CD,EVLU_COMMT,BLACK_YN,BLACK_REASON");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("na,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	GridObj.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro"); */
	
	GridObj.attachEvent("onRowSelect",doOnRowSelect);

	GridObj.init();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {
	sman_id = GridObj.cells(rowId, GridObj.getColIndexById("MAN_ID")).getValue();
}

function doQuery() {
	var man_nm = RTrim(document.form.MAN_NM.value);
	var tech_lvl_cd = RTrim(document.form.TECH_LVL_CD.value);
	var proj_cl_cd = RTrim(document.form.PROJ_CL_CD.value);
	var indus_cl_cd = RTrim(document.form.INDUS_CL_CD.value);
	var reg_no = document.getElementById('REG_NO').value;
	var major_desc = document.getElementById('MAJOR_DESC').value;
	var edu_cl_cd = RTrim(document.form.EDU_CL_CD.value);
	var qual_cl_cd = RTrim(document.form.QUAL_CL_CD.value);
	var pj_area_cd = RTrim(document.form.PJ_AREA_CD.value);
	var black_yn = document.getElementById('BLACK_YN').checked;
	var skill_etc = document.getElementById('SKILL_ETC').value;
	var evlu_cd = RTrim(document.form.EVLU_CD.value);
	
	var arrTech = "";
	var arrTechC = document.getElementsByName('TECH_CL_CDN');
	var arrRole = "";
	var arrRoleC = document.getElementsByName('ROLE_CL_CDN');
	
	if(arrTechC.length > 0) {
		for (var i=0; i<arrTechC.length; i++) {
			if (arrTechC[i].checked == true) {
	        	arrTech += arrTechC[i].value + ','
	        }
		}
		arrTech = arrTech.substring(arrTech,arrTech.length-1);
	}
	
/* 	if(arrTechC.length > 0) {
		for (var i=0; i<arrTechC.length; i++) {
			if(arrTech != "" && arrTechC[i].checked == true){
				arrTech += ",";
			}
	        if (arrTechC[i].checked == true) {
	        	arrTech += "\'" + arrTechC[i].value + "\'";
	        }
		}
	} */
	if(arrRoleC.length > 0) {
		for (var i=0; i<arrRoleC.length; i++) {
			if (arrRoleC[i].checked == true) {
				arrRole += arrRoleC[i].value + ','
	        }
		}
		arrRole = arrRole.substring(arrRole,arrRole.length-1);
	}
	
	var tech_cl_cd = arrTech;
	var role_cl_cd = arrRole;
	
	if (black_yn == true){
		black_yn = "Y";
	} else {
		black_yn = "N";
	}
	
	/* var grid_col_id = "SELECTED,MAN_ID,MAN_NM,AGENCY_ID,TEL_NO,ADDR,EMAIL,REG_NO,QUAL_CL_CD,LICEN_YM,MAJOR_DESC,SKILL_ETC,EVLU_CD,EVLU_COMMT,BLACK_YN,BLACK_REASON";
 */	var grid_col_id = "MAN_ID,MAN_NM,AGENCY_ID,TEL_NO,ADDR,EMAIL,REG_NO,QUAL_CL_CD,LICEN_YM,MAJOR_DESC,SKILL_ETC,EVLU_CD,EVLU_COMMT,BLACK_YN,BLACK_REASON";

	var argu = "&MAN_NM=" +encodeURIComponent(man_nm);
		argu += "&TECH_LVL_CD=" +encodeURIComponent(tech_lvl_cd);
		argu += "&PROJ_CL_CD=" +encodeURIComponent(proj_cl_cd);
		argu += "&INDUS_CL_CD=" +encodeURIComponent(indus_cl_cd);
		argu += "&REG_NO=" +encodeURIComponent(reg_no);
		argu += "&MAJOR_DESC=" +encodeURIComponent(major_desc);
		argu += "&EDU_CL_CD=" +encodeURIComponent(edu_cl_cd);
		argu += "&QUAL_CL_CD=" +encodeURIComponent(qual_cl_cd);
		argu += "&PJ_AREA_CD=" +encodeURIComponent(pj_area_cd);
		argu += "&BLACK_YN=" +encodeURIComponent(black_yn);
		argu += "&SKILL_ETC=" +encodeURIComponent(skill_etc);
		argu += "&EVLU_CD=" +encodeURIComponent(evlu_cd);
		argu += "&TECH_CL_CD=" +encodeURIComponent(tech_cl_cd);
		argu += "&ROLE_CL_CD=" +encodeURIComponent(role_cl_cd);

	GridObj.loadXML(G_SERVLETURL+"?mod=manSearchPop&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	// if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}


function doSaveCon()
{

}

function doAdd()
{
	if(sman_id == ""){
		alert("인력을 선택해주세요.");
		return;
	}
	opener.sman_id = sman_id;
	opener.doAddSearchMan();
	
	window.close();
}

function doAddEnd()
{
}

function init() {
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>인력검색</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
		<colgroup>
			<col width="1%" />
            <col width="9%" />
            <col width="24%" />
            <col width="9%" />
            <col width="24" />
            <col width="9%" />
            <col width="24%" />
        </colgroup>
<%--   <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1051", s_user.getLocale())%></td>
    <td colspan="5">
    	<input type="text" name="SEARCH_CON" id="SEARCH_CON" style='font-size:16px' readonly>
    </td>
  </tr> --%>
  <tr>
  	<td></td>
  	<td class="fs_large">성명</td>
    <td>
    	<input type="text" name="MAN_NM" id="MAN_NM" size="20" maxlength="64" class="text">
    </td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1055", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="REG_NO" id="REG_NO" size="20" maxlength="64" class="text">
    </td>
 	<td class="fs_large"><%=mm.getMessage("NIMP_1056", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="MAJOR_DESC" id="MAJOR_DESC" size="20" maxlength="64" class="text">
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1052", s_user.getLocale())%></td>
  	<td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1104"/>
				<jsp:param name="tagname" value="TECH_LVL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1053", s_user.getLocale())%></td>
  	<td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1108"/>
				<jsp:param name="tagname" value="PROJ_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1054", s_user.getLocale())%></td>
  	<td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1107"/>
				<jsp:param name="tagname" value="INDUS_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_1058", s_user.getLocale())%></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1102"/>
				<jsp:param name="tagname" value="QUAL_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1059", s_user.getLocale())%></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1110"/>
				<jsp:param name="tagname" value="PJ_AREA_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_1057", s_user.getLocale())%></td>
  	<td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1101"/>
				<jsp:param name="tagname" value="EDU_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
  	<td class="fs_large"><%=mm.getMessage("NIMP_105C", s_user.getLocale())%></td>
    <td>
    	<jsp:include page="../../mg/ni/comboCodeAll.jsp" >
				<jsp:param name="codeId" value="A1103"/>
				<jsp:param name="tagname" value="EVLU_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_105B", s_user.getLocale())%></td>
    <td>
    	<input type="text" name="SKILL_ETC" id="SKILL_ETC" size="20" maxlength="64" class="text">
    </td>
    <td class="fs_large"><%=mm.getMessage("NIMP_105A", s_user.getLocale())%></td>
    <td><input type="checkbox" name="BLACK_YN" id="BLACK_YN"></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_105D", s_user.getLocale())%></td>
    <td colspan="5">
    	<jsp:include page="../../mg/ni/checkboxCodeTech.jsp" >
				<jsp:param name="codeId" value="A1106"/>
				<jsp:param name="tagname" value="TECH_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large"><%=mm.getMessage("NIMP_105E", s_user.getLocale())%></td>
    <td colspan="5">
    	<jsp:include page="../../mg/ni/checkboxCodeRole.jsp" >
				<jsp:param name="codeId" value="A1105"/>
				<jsp:param name="tagname" value="ROLE_CL_CD"/>
				<jsp:param name="def" value=" "/>
			</jsp:include>
    </td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea" style="float: right">
	    		<%-- <a href="javascript:doSaveCon();" class="btn"><span><%=mm.getMessage("NIMP_105G", s_user.getLocale())%></span></a> --%>
	        	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMP_105F", s_user.getLocale())%></span></a>
	    	</div>
	    	<!-- <div align="left" style="float: right">
    				<input type="text" name="SEARCH_CON_NM" id="SEARCH_CON_NM" size="20" maxlength="64" placeholder="검색조건 저장 이름" class="text" style='font-size:16px'>
	        </div> -->
        </td>
    </tr>
    <tr>
    	<td>
	        <div class="btnarea">
	    		<a href="javascript:doAdd();" class="btn"><span><%=mm.getMessage("NIMP_105H", s_user.getLocale())%></span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" height="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=300"/>
</jsp:include>
</body>
</html>
