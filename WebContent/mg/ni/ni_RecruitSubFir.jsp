<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  
  String proj_no = request.getParameter("proj_no");
  String seq = request.getParameter("seq");
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>필요인원</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%><%@ include file="../../ext/include/su_grid_common.jsp" %><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var G_SERVLETURL = "<%=contextPath%>/ni/recruit.do";
var proj_no = "<%=proj_no%>"; 
var seq = "<%=seq%>"; 

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
   	GridObj.setSizes();
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

function doOnRowSelect(rowId, cellInd) {}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}



// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() 
{
	var grid_col_id = "<%=grid_col_id%>";

	if( proj_no == "") return;
	
	GridObj.loadXML(G_SERVLETURL+"?mod=selectManList&grid_col_id="+grid_col_id+"&PROJ_NO=" + proj_no+"&SEQ=" + seq);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) 
{
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	// if(status == "false") alert(msg);
	return true;
}

function doSave() {
	if(!checkRows()) {
		alert("수정할 인력을 체크해주세요");
		return;
	}
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("COMG_1009")%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=saveManList&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doConfirm() {
	if(!checkRows()) {
		alert("확정할 인력을 체크해주세요");
		return;
	}
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("확정하시겠습니까?")) {
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=confirmManList&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		// parent.refresh();
		parent.refresh(proj_no, seq);
		//doQuery();
	//} else if(status == "truee") {
	//	alert("이미 계약관리에 들어간 인원입니다");
	//	parent.refresh(proj_no, seq);
	//} else {
	//	alert(messsage);
	}
	return false;
}

function doDelete() {
	if(!checkRows()) {
		alert("삭제할 인력을 체크해주세요");
		return;
	}
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010")%>")) { //삭제하시겠습니까?
/* 		for(var i=0; i < grid_array.length; i++) {
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("SEQ")).getValue() == "") {
				GridObj.deleteRow(grid_array[i]);
			}
		}
		grid_array = getGridChangedRows(GridObj, "SELECTED"); */
		if(grid_array.length != 0) {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteManList&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		} else {
			parent.refresh(proj_no, seq);
		}
	}	
}


function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();" style="overflow:hidden;">
<form name="form" method="post">
<!-- <input type="file" name= "browseFile" style="display='none';">   -->
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
        	<col width="8%" />
            <col width="32%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td>선택인력 </td>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doSave();" class="btn"><span>저장</span></a>
	            	<a href="javascript:doConfirm();" class="btn"><span>확정</span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("NIMG_1003", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="89%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=142"/>
</jsp:include> --%>
</body>
</html>
