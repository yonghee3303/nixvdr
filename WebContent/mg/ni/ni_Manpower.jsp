<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>Manpower Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var man_id 			= "";
var man_nm 			= "";
var man_enm 		= "";
var reg_no	 		= "";	//주민번호
var reg_nof 		= "";
var reg_nob 		= "";
var national_cd 	= "";
var addr 			= "";
var tel_no 			= "";
var email 			= "";
var edu_cl_cd		= "";
var qual_cl_cd 		= "";
var licen_ym 		= "";
var agency_id 		= "";	//소속사
var agency_nm 		= "";
var major_desc 		= "";
var tech_cl_cd		= "";
var skill_etc 		= "";
var evlu_cd 		= "";
var evlu_commt 		= "";
var black_yn 		= "";
var black_reason 	= "";
var indus_cl_cd		= "";
var cur_ym			= "";
var cont_cl_cd		= "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/man.do";

//그리드폼
function setFormDraw() {	
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
/* 	var file_valueA = GridObj.cells(row_id, GridObj.getColIndexById("FILE_ICON")).getValue();
	var file_value = file_valueA.substring(file_valueA.length-5,file_valueA.length-4); */

	if( header_name == "FILE_ICON") {
/* 		if(file_value == "b"){ */
		doFiledownLoad(row_id);
/* 		} */
	} else {
		doRevPop();
	}
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	var man_nm = document.form.MAN_NM.value;
	var agency_id = document.form.AGENCY_NM.value;
	var cont_cl_cd = document.form.CONT_CL_CD.value.trim();
	var indus_cl_cd = document.form.INDUS_CL_CD.value.trim();
	var edu_cl_cd = document.form.EDU_CL_CD.value.trim();
	var qual_cl_cd = document.form.QUAL_CL_CD.value.trim();
	var major_desc = document.form.MAJOR_DESC.value;
	
	var arrTech = "";
	var arrTechC = document.getElementsByName('TECH_CL_CDN');
	
	if(arrTechC.length > 0) {
		for (var i=0; i<arrTechC.length; i++) {
			if (arrTechC[i].checked == true) {
	        	arrTech += arrTechC[i].value + ','
	        }
		}
		arrTech = arrTech.substring(arrTech,arrTech.length-1);
	}
	
	var tech_cl_cd = arrTech;

	var grid_col_id  = "<%=grid_col_id%>";
	var argu  = "&MAN_NM="	+encodeURIComponent(man_nm);
		argu += "&AGENCY_ID="	+encodeURIComponent(agency_id);
		argu += "&CONT_CL_CD="	+encodeURIComponent(cont_cl_cd);
		argu += "&INDUS_CL_CD="	+encodeURIComponent(indus_cl_cd);
		argu += "&TECH_CL_CD="	+encodeURIComponent(tech_cl_cd);
		argu += "&EDU_CL_CD="	+encodeURIComponent(edu_cl_cd);
		argu += "&QUAL_CL_CD="	+encodeURIComponent(qual_cl_cd);
		argu += "&MAJOR_DESC="	+encodeURIComponent(major_desc);
	GridObj.loadXML(G_SERVLETURL+"?mod=manSelectUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

//수정 버튼 기능
function doRevPop() {
	if(!rev_check) {
		alert("수정할 행을 선택해주세요.");
		return;
	}
	
	if(!checkRowsFRev()) {
		alert("수정시엔 모든 체크박스를 해제해주세요.");
		return;
	}
	var url = "/mg/ni/popupManpowerRev.jsp"
		popupNewAddBoard(url, "정보수정", 588, 1000);
}

//수정 팝업창 기능
function getRevPopupData() {
	doRevAddRow();
}

//수정 데이터
function getParentData() {
	row_id = GridObj.getSelectedRowId();
	
	man_id = GridObj.cells(row_id, GridObj.getColIndexById("MAN_ID")).getValue();
	man_nm = GridObj.cells(row_id, GridObj.getColIndexById("MAN_NM")).getValue();
	agency_nm = GridObj.cells(row_id, GridObj.getColIndexById("AGENCY_ID")).getValue();
	agency_id = GridObj.cells(row_id, GridObj.getColIndexById("AGENCY_FID")).getValue();

	tel_no = GridObj.cells(row_id, GridObj.getColIndexById("TEL_NO")).getValue();
	addr = GridObj.cells(row_id, GridObj.getColIndexById("ADDR")).getValue();
	email = GridObj.cells(row_id, GridObj.getColIndexById("EMAIL")).getValue();
	reg_no = GridObj.cells(row_id, GridObj.getColIndexById("REG_FNO")).getValue();
	indus_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("INDUS_CL_CD")).getValue();

	qual_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("QUAL_CL_CD")).getValue();
	licen_ym = GridObj.cells(row_id, GridObj.getColIndexById("LICEN_FYM")).getValue();
	major_desc = GridObj.cells(row_id, GridObj.getColIndexById("MAJOR_DESC")).getValue();
	tech_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("TECH_CL_CD")).getValue();
	skill_etc = GridObj.cells(row_id, GridObj.getColIndexById("SKILL_ETC")).getValue();
	evlu_cd = GridObj.cells(row_id, GridObj.getColIndexById("EVLU_CD")).getValue();
	black_yn = GridObj.cells(row_id, GridObj.getColIndexById("BLACK_YN")).getValue();
	/* man_enm = GridObj.cells(row_id, GridObj.getColIndexById("MAN_ENM")).getValue(); */
	national_cd = GridObj.cells(row_id, GridObj.getColIndexById("NATIONAL_CD")).getValue();
	evlu_commt = GridObj.cells(row_id, GridObj.getColIndexById("EVLU_COMMT")).getValue();
	black_reason = GridObj.cells(row_id, GridObj.getColIndexById("BLACK_REASON")).getValue();
	edu_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("EDU_CL_CD")).getValue();
	cur_ym = GridObj.cells(row_id, GridObj.getColIndexById("CUR_YMA")).getValue();
	cont_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("CONT_CL_CD")).getValue();
		
	GridObj.cells(row_id, GridObj.getColIndexById("SELECTED")).setValue("0");
}

//수정시 체크 해제
function checkRowsFRev() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return false;
	}
	return true;
}

//데이터 수정
function doRevAddRow() {
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ID")).setValue(man_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_NM")).setValue(man_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("AGENCY_ID")).setValue(agency_id);

	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TEL_NO")).setValue(tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADDR")).setValue(addr);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EMAIL")).setValue(email);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REG_NO")).setValue(reg_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INDUS_CL_CD")).setValue(indus_cl_cd);

	GridObj.cells(nMaxRow2, GridObj.getColIndexById("QUAL_CL_CD")).setValue(qual_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("LICEN_YM")).setValue(licen_ym);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAJOR_DESC")).setValue(major_desc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TECH_CL_CD")).setValue(tech_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SKILL_ETC")).setValue(skill_etc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EVLU_CD")).setValue(evlu_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BLACK_YN")).setValue(black_yn);
	/* GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ENM")).setValue(man_enm); */
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("NATIONAL_CD")).setValue(national_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EVLU_COMMT")).setValue(evlu_commt);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BLACK_REASON")).setValue(black_reason);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EDU_CL_CD")).setValue(edu_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUR_YM")).setValue(cur_ym);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CONT_CL_CD")).setValue(cont_cl_cd);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doRevSave();
}

//데이터 수정sql
function doRevSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&TECH_CL_CD=" +tech_cl_cd;
		argu += "&MAN_ID=" +man_id;
	    var SERVLETURL = G_SERVLETURL + "?mod=saveMan&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

//신규 버튼 기능
function doAddNewPop() {
	var url = "/mg/ni/popupManpowerNewAdd.jsp"
	popupNewAddBoard(url, "신규 생성", 588, 1000);
}

//신규 팝업창 기능
function getPopupData() {
	doAddRow();
}

//신규 데이터
function doAddRow() {

	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ID")).setValue(man_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_NM")).setValue(man_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("AGENCY_ID")).setValue(agency_id);

	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TEL_NO")).setValue(tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADDR")).setValue(addr);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EMAIL")).setValue(email);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REG_NO")).setValue(reg_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INDUS_CL_CD")).setValue(indus_cl_cd);

	GridObj.cells(nMaxRow2, GridObj.getColIndexById("QUAL_CL_CD")).setValue(qual_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("LICEN_YM")).setValue(licen_ym);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAJOR_DESC")).setValue(major_desc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TECH_CL_CD")).setValue(tech_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SKILL_ETC")).setValue(skill_etc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EVLU_CD")).setValue(evlu_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BLACK_YN")).setValue(black_yn);
	/* GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ENM")).setValue(man_enm); */
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("NATIONAL_CD")).setValue(national_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EVLU_COMMT")).setValue(evlu_commt);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BLACK_REASON")).setValue(black_reason);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EDU_CL_CD")).setValue(edu_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUR_YM")).setValue(cur_ym);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CONT_CL_CD")).setValue(cont_cl_cd);

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doAddSave();
}

//신규 데이터 sql
function doAddSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&TECH_CL_CD=" +tech_cl_cd;
		argu += "&MAN_ID=" +man_id;
    	var SERVLETURL = G_SERVLETURL + "?mod=saveNewMan&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}


function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {		
	} else {
		alert(messsage);
	}
	return false;
}

//삭제 버튼 기능
function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) {

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteMan&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

/* function doFiledownLoad() {
	 	row_id = rowId;	
		var filename = GridObj.cells(row_id, GridObj.getColIndexById("FILE_NAME")).getValue();
		
		row_id = GridObj.getSelectedRowId();
		man_id = GridObj.cells(row_id, GridObj.getColIndexById("MAN_ID")).getValue();
		
		 var SERVLETURL = "/mg/Upload.do" + "?mod=fileDownload";	
		 var argu = "&PGM_ID=" + "manProfile";
		 	 argu += "&DOC_ID=" + man_id;
		     argu += "&FILE_NAME=" + encodeURIComponent(filename);
		 fileDownload(SERVLETURL + argu);
	}
	 */
function doFiledownLoad(row_id){
	var man_id = GridObj.cells(row_id, GridObj.getColIndexById("MAN_ID")).getValue();
	
	popupNimsAttachFile("File Upload", '', '', 500, 400, "manProfile", man_id, "", "write",<%=pm.getString("component.contextPath.root")%>);
 }

//첨부파일 관리  POPUP을 띄운다.
function popupNimsAttachFile(title, left, top, width, height, pgmId, docId, authority, callmode,root) {
    if (title == '') title = 'File Upload';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
    
    var url ="";
    
    if(root == false) {
    	url = contextPath;
    }
    
    if(callmode == "write") {
       url += "/mg/ni/popupNimsAttachFile.jsp";
    }else if(callmode=="repacking"){
    	url += "/mg/common/popupAttachFile_repacking.jsp";
    	resizable='no';
    }else if(callmode=="readPortal"){
    	url += "/mg/common/popupAttachFileRead_portal.jsp";
    	resizable='no';
    }else if(callmode=="readRepacking"){
    	url += "/mg/common/popupAttachFileSingleRead_portal.jsp";
    	resizable='no';
    }else {
    	url += "/mg/common/popupAttachFileRead.jsp";
    }
    
    if(callmode=="readRepacking"){
    	url +="?formid=POP_0109&gridid=GridObj";
    }else{
    	url += "?formid=POP_0106&gridid=GridObj";
    }
    url += "&pgmId=" + pgmId + "&docId=" + docId + "&authority=" + authority;
    if(callmode=="repacking"){
    	url += "&docTitle="+authority;
    }
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];
	
	var ret = window.open(url, "upload", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function init() {
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="80px" />
            <col width="100px" />
            <col width="80px" />
            <col width="120px" />
            <col width="80px" />
            <col width="100px" />
            <col width="80px" />
            <col width="100px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">계약구분</td>
            <td>
				<jsp:include page="/mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1114"/>
					<jsp:param name="tagname" value="CONT_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
	    	<td class="tit">산업구분</td>
            <td>
				<jsp:include page="/mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1107"/>
					<jsp:param name="tagname" value="INDUS_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
        	<td class="tit"><%=mm.getMessage("NIMG_1021", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="MAN_NM"  size="10" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("NIMG_1022", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="AGENCY_NM"  size="10" class="text">
            </td>
            <td></td>
        </tr>
        <tr>
		    <td class="tit"><%=mm.getMessage("NIMP_105D", s_user.getLocale())%></td>
		    <td colspan="8">
		    	<jsp:include page="../../mg/ni/checkboxCodeTech.jsp" >
					<jsp:param name="codeId" value="A1106"/>
					<jsp:param name="tagname" value="TECH_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
		    </td>
	  	</tr>
	  	<tr>
	  		<td class="tit">학력</td>
            <td>
				<jsp:include page="/mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1101"/>
					<jsp:param name="tagname" value="EDU_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
	    	<td class="tit">자격</td>
            <td>
				<jsp:include page="/mg/ni/comboCodeAll.jsp" >
					<jsp:param name="codeId" value="A1102"/>
					<jsp:param name="tagname" value="QUAL_CL_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
	    	<td class="tit">전공</td>
            <td>
            	<input type="text" name="MAJOR_DESC"  size="10" class="text">
            </td>
            <td></td>
            <td></td>
            <td></td>
	    </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doAddNewPop();" class="btn"><span><%=mm.getMessage("NIMG_1001", s_user.getLocale())%></span></a>
	            	<a href="javascript:doRevPop();" class="btn"><span><%=mm.getMessage("NIMG_1002", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("NIMG_1003", s_user.getLocale())%></span></a>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMG_1004", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="83%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>