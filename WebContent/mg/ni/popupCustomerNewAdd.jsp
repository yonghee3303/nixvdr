<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>고객사 등록</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var ranIdNum		= getRanIdNum();
var todayNum		= getTodayNum();
var ranId			= todayNum + ranIdNum;
var mRanId			= "CU" + ranId;

var G_SERVLETURL = "<%=contextPath%>/ni/man.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {}

function doOnRowSelect(rowId,cellInd) {}

function doQuery() {
	
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd() {
		
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}

function doAdd()
{
	var customer_nm = document.getElementById('CUSTOMER_NM').value;
	var customer_sub_cd = document.getElementById('CUSTOMER_SUB_CD').value;
	var customer_project = document.getElementById('CUSTOMER_PROJECT').value;
	var user_id = document.getElementById('USER_ID').value;

	var customer_user_fir = document.getElementById('CUSTOMER_USER_FIR').value;
	var customer_tel_fir = phoneFomatter(document.getElementById('CUSTOMER_TEL_FIR').value);
	var customer_user_sec = document.getElementById('CUSTOMER_USER_SEC').value;
	var customer_tel_sec = phoneFomatter(document.getElementById('CUSTOMER_TEL_SEC').value);
	var customer_user_thi = document.getElementById('CUSTOMER_USER_THI').value;
	var customer_tel_thi = phoneFomatter(document.getElementById('CUSTOMER_TEL_THI').value);
	var customer_user_fou = document.getElementById('CUSTOMER_USER_FOU').value;
	var customer_tel_fou = phoneFomatter(document.getElementById('CUSTOMER_TEL_FOU').value);
	
	opener.customer_cd = mRanId;
	opener.customer_nm = customer_nm;
	opener.customer_sub_cd = customer_sub_cd;
	opener.customer_project = customer_project;
	opener.user_id = user_id;

	opener.customer_user_fir = customer_user_fir;
	opener.customer_tel_fir = customer_tel_fir;
	opener.customer_user_sec = customer_user_sec;
	opener.customer_tel_sec = customer_tel_sec;
	opener.customer_user_thi = customer_user_thi;
	opener.customer_tel_thi = customer_tel_thi;
	opener.customer_user_fou = customer_user_fou;
	opener.customer_tel_fou = customer_tel_fou;

	opener.doAddRow();
		
	window.close();
}

//전화번호 자동 하이픈 입력
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 4) {
        return number;
    } else if(number.length < 7) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3);
    } else if(number.length == 9) {
        phone += number.substr(0, 2);
        phone += "-";
        phone += number.substr(2, 3);
        phone += "-";
        phone += number.substr(5);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

//달력
function init() {
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"LICEN_YM",button:"calendar_icon"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	
	setFormDraw();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" border="0">
  <tr>
    <td class="popup_tit"><h1>신규등록</h1>
    </td>
  </tr>
</table>
<table width="100%" border="0" class="nimsou">
	<colgroup>
			<col width="2%" />
            <col width="19%" />
            <col width="30%" />
            <col width="19%" />
            <col width="30%" />
    </colgroup>
  <tr>
  	<td></td>
    <td class="fs_large">고객사ID</td>
    <td>
    	<input type="text" name="CUSTOMER_CD" id="CUSTOMER_CD" size="20" placeholder="자동부여" readonly>
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">고객사명</td>
    <td>
    	<input type="text" name="CUSTOMER_NM" id="CUSTOMER_NM" size="18" maxlength="50">
    </td>
    <td class="fs_large">고객사 Sub<a> *</a></td>
    <td>
    	<input type="text" name="CUSTOMER_SUB_CD" id="CUSTOMER_SUB_CD" size="18" maxlength="50">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">고객사 프로젝트</td>
    <td>
    	<input type="text" name="CUSTOMER_PROJECT" id="CUSTOMER_PROJECT" size="20" maxlength="50">
    </td>
    <td class="fs_large">담당자</td>
    <td>
		<jsp:include page="/mg/ni/comboCodeUser.jsp" >
			<jsp:param name="codeId" value="A1199"/>
			<jsp:param name="tagname" value="USER_ID"/>
			<jsp:param name="def" value=" "/>
		</jsp:include>
	</td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">고객사2 담당자</td>
    <td>
    	<input type="text" name="CUSTOMER_USER_FIR" id="CUSTOMER_USER_FIR" size="20" maxlength="50">
    </td>
    <td class="fs_large">전화번호</td>
    <td>
    	<input type="text" onKeyup="inputPhoneNumber(this);" name="CUSTOMER_TEL_FIR" id="CUSTOMER_TEL_FIR" size="18" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">고객사3 담당자</td>
    <td>
    	<input type="text" name="CUSTOMER_USER_SEC" id="CUSTOMER_USER_SEC" size="20" maxlength="50">
    </td>
    <td class="fs_large">전화번호</td>
    <td>
    	<input type="text" onKeyup="inputPhoneNumber(this);" name="CUSTOMER_TEL_SEC" id="CUSTOMER_TEL_SEC" size="18" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">고객사4 담당자</td>
    <td>
    	<input type="text" name="CUSTOMER_USER_THI" id="CUSTOMER_USER_THI" size="20" maxlength="50">
    </td>
    <td class="fs_large">전화번호</td>
    <td>
    	<input type="text" onKeyup="inputPhoneNumber(this);" name="CUSTOMER_TEL_THI" id="CUSTOMER_TEL_THI" size="18" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
  <tr>
  	<td></td>
    <td class="fs_large">고객사5 담당자</td>
    <td>
    	<input type="text" name="CUSTOMER_USER_FOU" id="CUSTOMER_USER_FOU" size="20" maxlength="50">
    </td>
    <td class="fs_large">전화번호</td>
    <td>
    	<input type="text" onKeyup="inputPhoneNumber(this);" name="CUSTOMER_TEL_FOU" id="CUSTOMER_TEL_FOU" size="18" maxlength="13" oninput="numberMaxLength(this);">
    </td>
  </tr>
</table>
<table width="100%" border="0">
	<tr>
    	<td>
	        <div class="btnarea" style='margin:15px 0px 0px 0px'>
	        	<a href="javascript:doAdd();" class="btn"><span>저장</span></a>
	    	</div>
        </td>
    </tr>
</table>
</form>
</body>
</html>
