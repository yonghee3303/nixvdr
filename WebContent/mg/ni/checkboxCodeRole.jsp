<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>

<%
	String tagname = StringUtils.paramReplace(request.getParameter("tagname"));
	String codeId = StringUtils.paramReplace(request.getParameter("codeId")); 
	String def_val = StringUtils.paramReplace(request.getParameter("def")); 
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IUser c_user = (IUser) session.getAttribute("j_user");
	 
	if("".equals(def_val)) {
		def_val = mm.getMessage("NIMG_1023", c_user.getLocale());
	}
	Locale locale =  c_user.getLocale();
	if(!"ko".equals(c_user.getLocale().getLanguage()) ) {
		locale = Locale.US;
	}
%>

<%-- 공통코드 검색조건 Checkbox 구현 --%>
				<%	ICodeManagement MCode = (ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
					List sCode = MCode.getCodes(codeId);
				
					for (Iterator all = sCode.iterator(); all.hasNext();) {
						ICode icd = (ICode) all.next();
	
				        String code = icd.getId();
				        String cdNm = icd.getName();
				        if(!"ko".equals(c_user.getLocale().getLanguage()) ) {
				        	cdNm = icd.getNameSl();
				        }
				        String cdKey = icd.getCd();
				        
				        boolean isShow = true;
				        
				        if(isShow) {
							%>
								<label><input type="checkbox" name="ROLE_CL_CDN" value="<%=cdKey%>"> <%=cdNm%> </label>
							<%
				        }
					}
				    %>