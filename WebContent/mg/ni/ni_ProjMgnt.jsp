<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
/*    String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid"); */
   IUser s_user = (IUser) session.getAttribute("j_user");
   IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
   String contextPath = request.getContextPath();
%>

<html>
<head>
<title>프로젝트 관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

var rowsNumb		= "";

function setFormDraw() {	
	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>";
	userframe.location.href='../../mg/ni/ni_ProjMain.jsp?eventframe=authframe' + argu;
}

function init() 
{
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"PJYMD_FROM",button:"calendar_icon"});
	calendar2 = new dhtmlXCalendarObject({input:"PJYMD_TO",button:"calendar_icon2"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	
	setFormDraw();
}

function refresh(proj_no) 
{	
	var argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&projNo=" + proj_no;
	authframe.location.href='/mg/ni/ni_ProjSub.jsp?' + argu;
}

function doQuery() 
{
	userframe.doQuery();
}

function doAddNewPop() 
{
	userframe.doAddNewPop();
}

function getRowsNumb()
{
	document.getElementById("totalCntTD").innerHTML = rowsNumb;
}

function doSave() 
{
	userframe.doRevPop();
}

function doDelete() 
{
	userframe.doDelete();
}

</script>

<style>/*프로젝트 상태 셀렉박스 스타일*/
select {
width: 60px;
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

</head>
<body onload="init();" style="overflow:hidden">
<form name="form" method="post">
<table width="100%" height="100%">
<tr>
   	<td>
	  <%-- <%@ include file="/mg/common/milestone.jsp"%> --%>
	  
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="120px" />
            <col width="120px" />
            <col width="120px" />
            <col width="120px" />
            <col width="50px" />
            <col width="80px" />
            <col width="120px" />
            <col width="50px" />
            <col width="80px" />
            <col width="310px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">프로젝트 상태</td>
			<td>
			<select id= "PJ_CL_CD" name= "pj_cl_cd" onChange="javascript:doQuery();">
			   <option value="">전체</option>
			   <option value="진행중" selected="selected">진행중</option>
			   <option value="종료">종료</option>				
			</select>
			</td>
        	<td class="tit"><%=mm.getMessage("NIMG_1041", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="PROJ_NM"  size="14" class="text">
            </td>
            <td></td>
            <td class="tit"><%=mm.getMessage("NIMG_1042", s_user.getLocale())%></td>
            <td>
				<input type="text" name="COMP_NM" size="14" class="text">
            </td>
            <td></td>
            <td class="tit"><%=mm.getMessage("NIMG_1043", s_user.getLocale())%></td>
        	<td>
    			<input type="text" name="PJYMD_FROM" id="PJYMD_FROM" size="14" maxlength="64" class="text" style='font-size:12px'> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span>
				~
    			<input type="text" name="PJYMD_TO" id="PJYMD_TO" size="14" maxlength="64" class="text" style='font-size:12px'> <span><img id="calendar_icon2" src="../../../ext/images/calendar.png" border="0"></span>
    		</td>
    		<td>
    			<div class="btnarea">
    			</div> 
    		</td>
        </tr>
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	           <div class="btnarea">
	           		<a href="javascript:doAddNewPop();" class="btn"><span><%=mm.getMessage("NIMG_1001", s_user.getLocale())%></span></a>
	          		<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("NIMG_1002", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("NIMG_1003", s_user.getLocale())%></span></a>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMG_1004", s_user.getLocale())%></span></a>
	           </div> 
            </td>
        </tr>
      </table>
   </td>
</tr>
<tr>	     
	<td height="100%" valign="top">
		<iframe name="userframe" height="100%" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
	</td>

</tr>
</table>
</form>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="authframe=162"/>
</jsp:include> --%>
</body>
</html>
