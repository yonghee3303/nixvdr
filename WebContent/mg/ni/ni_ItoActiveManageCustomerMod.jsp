<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<style>
select {
padding: .1em .1em;
border: 1px solid #E1E1E1;
font-family: inherit;
border-radius: 0px;
}
</style>

<title>ITO Active Manage Customer Mod Menu</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var month 			= "";

var now = new Date();

var now_year		= "";

var customer_cd		= "";
var customer_nm		= "";
var customer_sub_cd		= "";
var customer_project	= "";
var user_id			= "";

var customer_user_fir		= "";
var customer_tel_fir		= "";
var customer_user_sec		= "";
var customer_tel_sec		= "";
var customer_user_thi		= "";
var customer_tel_thi		= "";
var customer_user_fou		= "";
var customer_tel_fou		= "";


var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/itotarper.do";

//그리드폼
function setFormDraw() {	
	<%-- GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	/* GridObj.attachEvent("onRowDblClicked",doRevPop); */
	GridObj.setSizes(); --%>
	
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	
	GridObj.setHeader("#master_checkbox,고객사1,#cspan,#cspan,#cspan,담당자,고객사2,#cspan,고객사3,#cspan,고객사4,#cspan,고객사5,#cspan")
	GridObj.attachHeader(["#rspan","고객사ID","고객사명","고객사Sub","고객사 프로젝트","#rspan","고객사 담당자","전화번호","고객사 담당자","전화번호","고객사 담당자","전화번호","고객사 담당자","전화번호"]);
	GridObj.setInitWidths("30,70,80,80,140,80,80,80,80,80,80,80,80,80");
	GridObj.setColAlign("center,center,center,left,left,center,center,center,center,center,center,center,center,center");
	GridObj.setColumnIds("SELECTED,CUSTOMER_CD,CUSTOMER_NM,CUSTOMER_SUB_CD,CUSTOMER_PROJECT,USER_ID,CUSTOMER_USER_FIR,CUSTOMER_TEL_FIR,CUSTOMER_USER_SEC,CUSTOMER_TEL_SEC,CUSTOMER_USER_THI,CUSTOMER_TEL_THI,CUSTOMER_USER_FOU,CUSTOMER_TEL_FOU");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("na,str,str,str,str,str,str,str,str,str,str,str,str,str");
	GridObj.setColTypes("ch,ro,ro,ro,ro,coro,ro,ro,ro,ro,ro,ro,ro,ro");
	
	doRequestUsingPOST_dhtmlxGrid(GridObj,"A1199","USER_ID","");
	
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);
	GridObj.attachEvent("onXLE",doQueryEnd);
	/* GridObj.attachEvent("onEditCell",doOnCellEdit);	
	GridObj.attachEvent("onEditCell",doOnCellChange); */

	GridObj.init();
	
	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
	
	doRevPop();
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
/* 	var search_customer_nm = document.form.S_CUSTOMER_NM.value; */

	var grid_col_id  = "SELECTED,CUSTOMER_CD,CUSTOMER_NM,CUSTOMER_SUB_CD,CUSTOMER_PROJECT,USER_ID,CUSTOMER_USER_FIR,CUSTOMER_TEL_FIR,CUSTOMER_USER_SEC,CUSTOMER_TEL_SEC,CUSTOMER_USER_THI,CUSTOMER_TEL_THI,CUSTOMER_USER_FOU,CUSTOMER_TEL_FOU";
/* 	var argu  = "&S_CUSTOMER_NM="	+encodeURIComponent(search_customer_nm); */
	
	GridObj.loadXML(G_SERVLETURL+"?mod=itoActiveManageCustomerSel&grid_col_id="+grid_col_id);
	GridObj.clearAll(false);
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	/* document.getElementById("totalCntTD").innerHTML = " <span class='point'>" + now_year+"</span> 년 "; */
	
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

//신규 버튼 기능
function doAddNewPop() {
	var url = "/mg/ni/popupCustomerNewAdd.jsp"
	popupNewAddBoard(url, "신규 생성", 334, 755);
}

function doAddRow() {

	//dhtmlx_last_row_id++;
	var last_row_id = GridObj.getRowsNum();
   	var nMaxRow2 = ++last_row_id;
   	var row_data = "SELECTED,CUSTOMER_CD,CUSTOMER_NM,CUSTOMER_SUB_CD,CUSTOMER_PROJECT,USER_ID,CUSTOMER_USER_FIR,CUSTOMER_TEL_FIR,CUSTOMER_USER_SEC,CUSTOMER_TEL_SEC,CUSTOMER_USER_THI,CUSTOMER_TEL_THI,CUSTOMER_USER_FOU,CUSTOMER_TEL_FOU";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_CD")).setValue(customer_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_NM")).setValue(customer_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_SUB_CD")).setValue(customer_sub_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_PROJECT")).setValue(customer_project);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_ID")).setValue(user_id);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_FIR")).setValue(customer_user_fir);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_FIR")).setValue(customer_tel_fir);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_SEC")).setValue(customer_user_sec);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_SEC")).setValue(customer_tel_sec);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_THI")).setValue(customer_user_thi);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_THI")).setValue(customer_tel_thi);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_FOU")).setValue(customer_user_fou);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_FOU")).setValue(customer_tel_fou);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doSave();
}

//데이터 저장
function doSave() {
	if(!checkRows()) return;
	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		<%-- var cols_ids = "<%=grid_col_id%>"; --%>
		var cols_ids  = "SELECTED,CUSTOMER_CD,CUSTOMER_NM,CUSTOMER_SUB_CD,CUSTOMER_PROJECT,USER_ID,CUSTOMER_USER_FIR,CUSTOMER_TEL_FIR,CUSTOMER_USER_SEC,CUSTOMER_TEL_SEC,CUSTOMER_USER_THI,CUSTOMER_TEL_THI,CUSTOMER_USER_FOU,CUSTOMER_TEL_FOU";
		/* var argu  = "&NOW_YEAR="	+encodeURIComponent(now_year); */

    	var SERVLETURL = G_SERVLETURL + "?mod=itoActiveManageCustomerSave&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}


function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {		
	} else {
		alert(messsage);
	}
	
	doQuery();
	
	return false;
}

function doRevPop() {
	if(!rev_check) {
		alert("수정할 행을 선택해주세요.");
		return;
	}
	
	if(!checkRowsFRev()) {
		alert("수정시엔 모든 체크박스를 해제해주세요.");
		return;
	}
	var url = "/mg/ni/popupCustomerRev.jsp"
		popupNewAddBoard(url, "정보수정", 334, 755);
}

function getParentData() {
	row_id = GridObj.getSelectedRowId();
	
	customer_cd = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_CD")).getValue();
	customer_nm = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_NM")).getValue();
	customer_sub_cd = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_SUB_CD")).getValue();
	customer_project = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_PROJECT")).getValue();
	user_id = GridObj.cells(row_id, GridObj.getColIndexById("USER_ID")).getValue();
	
	customer_user_fir = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_USER_FIR")).getValue();
	customer_tel_fir = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_TEL_FIR")).getValue();
	customer_user_sec = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_USER_SEC")).getValue();
	customer_tel_sec = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_TEL_SEC")).getValue();
	customer_user_thi = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_USER_THI")).getValue();
	customer_tel_thi = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_TEL_THI")).getValue();
	customer_user_fou = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_USER_FOU")).getValue();
	customer_tel_fou = GridObj.cells(row_id, GridObj.getColIndexById("CUSTOMER_TEL_FOU")).getValue();
		
	GridObj.cells(row_id, GridObj.getColIndexById("SELECTED")).setValue("0");
}

//수정시 체크 해제
function checkRowsFRev() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return false;
	}
	return true;
}

//데이터 수정
function doRevAddRow() {
	//dhtmlx_last_row_id++;
	var last_row_id = GridObj.getRowsNum();
   	var nMaxRow2 = ++last_row_id;
   	var row_data = "SELECTED,CUSTOMER_CD,CUSTOMER_NM,CUSTOMER_SUB_CD,CUSTOMER_PROJECT,USER_ID,CUSTOMER_USER_FIR,CUSTOMER_TEL_FIR,CUSTOMER_USER_SEC,CUSTOMER_TEL_SEC,CUSTOMER_USER_THI,CUSTOMER_TEL_THI,CUSTOMER_USER_FOU,CUSTOMER_TEL_FOU";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_CD")).setValue(customer_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_NM")).setValue(customer_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_SUB_CD")).setValue(customer_sub_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_PROJECT")).setValue(customer_project);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_ID")).setValue(user_id);
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_FIR")).setValue(customer_user_fir);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_FIR")).setValue(customer_tel_fir);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_SEC")).setValue(customer_user_sec);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_SEC")).setValue(customer_tel_sec);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_THI")).setValue(customer_user_thi);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_THI")).setValue(customer_tel_thi);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_USER_FOU")).setValue(customer_user_fou);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CUSTOMER_TEL_FOU")).setValue(customer_tel_fou);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doSave();
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "SELECTED,CUSTOMER_CD,CUSTOMER_NM,CUSTOMER_SUB_CD,CUSTOMER_PROJECT,USER_ID,CUSTOMER_USER_FIR,CUSTOMER_TEL_FIR,CUSTOMER_USER_SEC,CUSTOMER_TEL_SEC,CUSTOMER_USER_THI,CUSTOMER_TEL_THI,CUSTOMER_USER_FOU,CUSTOMER_TEL_FOU";

			var SERVLETURL = G_SERVLETURL + "?mod=itoActiveManageCustomerDelete&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}


$(document).ready(function(){
	var options = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy-mm'
	};
	var options2 = {
			monthNames: ['1월(JAN)', '2월(FEB)', '3월(MAR)', '4월(APR)', '5월(MAY)', '6월(JUN)',
		        '7월(JUL)', '8월(AUG)', '9월(SEP)', '10월(OCT)', '11월(NOV)', '12월(DEC)'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    showOn: "button",
		    buttonImage: "../../../ext/images/calendar.png",
		    buttonImageOnly: true,
		    changeYear: false,
		    yearRange: 'c-2:c+2',
		    dateFormat: 'yy'
	};

		$('input[name="nowpage"]').change(function() {
	        var value = $(this).val();
			if(value==0) {
				parent.seltoday();
			}
			if(value==1) {
				parent.toModify();
			}
			if(value==2) {
				parent.toModify();
			}
	    });
	});

function init() {
	/* document.getElementById('NOW_YEAR').value = new Date().toISOString().substring(0, 4); */
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
        	<col width="160px" />
            <col width="40px" />
            <col width="50px" />
            <col width="140px" />
            <col width="40px" />
            <col width="50px" />
            <col width="110px" />
            <col width="40px" />
            <col width="65px" />
            <col width="110px" />
            <col width="40px" />
            <col width="50px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit">활동관리조회(담당자별)</td>
	        <td>
	        	<input type="radio" name="nowpage" value="0">
			</td>
			<td></td>
			<td class="tit">활동관리조회(일자별)</td>
	        <td>
	        	<input type="radio" name="nowpage" value="1">
			</td>
			<td></td>
        	<td class="tit">활동관리 입력</td>
	        <td>
	        	<input type="radio" name="nowpage" value="2">
			</td>
			<td></td>
			<td class="tit">고객사등록관리</td>
	        <td>
	        	<input type="radio" name="nowpage" value="3" checked="checked">
			</td>
			<td></td>
            <td></td>
        </tr>
<!--         <tr>
			<td class="tit">고객사명</td>
            <td>
            	<input type="text" name="S_CUSTOMER_NM"  size="10" class="text">
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr> -->
		</table>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<!-- <a href="javascript:doQuery();" class="btn"><span>조회</span></a> -->
	            	<a href="javascript:doAddNewPop();" class="btn"><span>고객사추가</span></a>
	            	<!-- <a href="javascript:doSave();" class="btn"><span>저장</span></a> -->
	            	<a href="javascript:doDelete();" class="btn"><span>삭제</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="77%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>