<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>인재 근황 관리</title>
<%@ include file="/ext/include/include_css.jsp"%>
<%@ include file="/ext/include/su_grid_common.jsp" %>
<%@ include file="/ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var addRowId		= 0;
var ed_flag			= "";
var sel_change		= "";

var man_nm = "";
var man_id = "";
var intvw_cl_cd = "";
var contact_cl_cd = "";
var be_proj = "";
var free_ymd = "";
var can_proj_ymd = "";
var next_alm_ymd = "";
var intvw_desc = "";
var intvw_ymd = "";
var intvwetc = "";

var rev_check		= false;

var G_SERVLETURL = "<%=contextPath%>/ni/man.do";

//그리드폼
function setFormDraw() {
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);
	GridObj.attachEvent("onRowDblClicked",doRevPop);
	GridObj.setSizes();
	doQuery();
}

//위로 행이동
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {
	rev_check = true;
}


//선택된 행의 존재 여부
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회
function doQuery() {
	rev_check = false;
	var user_nm = document.form.USER_NM.value;
	var intvw_from = document.form.INTVW_FROM.value;
	var intvw_to = document.form.INTVW_TO.value;
	var almymd = document.form.ALMYMD.value;
	var man_nm = document.form.MAN_NM.value;
	var freeymd = document.form.FREEYMD.value;
	var can_proj_ymd = document.form.CAN_PROJ_YMD.value;

	var grid_col_id  = "<%=grid_col_id%>";
	var argu  = "&USER_NM="	+encodeURIComponent(user_nm);
	argu  += "&INTVW_FROM="	+encodeURIComponent(intvw_from);
	argu  += "&INTVW_TO="	+encodeURIComponent(intvw_to);
	argu  += "&ALMYMD="	+encodeURIComponent(almymd);
	argu  += "&MAN_NM="	+encodeURIComponent(man_nm);
	argu  += "&FREEYMD="	+encodeURIComponent(freeymd);
	argu  += "&CAN_PROJ_YMD="	+encodeURIComponent(can_proj_ymd);
	
	GridObj.loadXML(G_SERVLETURL+"?mod=recentSituSelectUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

//doQuery 종료 시점 호출
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

//수정 버튼 기능
function doRevPop() {
	if(!rev_check) {
		alert("수정할 행을 선택해주세요.");
		return;
	}
	
	if(!checkRowsFRev()) {
		alert("수정시엔 모든 체크박스를 해제해주세요.");
		return;
	}
	var url = "/mg/ni/popupRecentSituRev.jsp"
	popupNewAddBoard(url, "인재 근황 수정", 491, 643);
}

//수정 팝업창 기능
function getRevPopupData() {
	doRevAddRow();
}

//수정 데이터
function getParentData() {
	row_id = GridObj.getSelectedRowId();
	
	man_nm = GridObj.cells(row_id, GridObj.getColIndexById("MAN_NM")).getValue();
   	man_id = GridObj.cells(row_id, GridObj.getColIndexById("MAN_ID")).getValue();
	intvw_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("INTVW_CL_CD")).getValue();
	contact_cl_cd = GridObj.cells(row_id, GridObj.getColIndexById("CONTACT_CL_CD")).getValue();
	be_proj = GridObj.cells(row_id, GridObj.getColIndexById("BE_PROJ")).getValue();
	free_ymd = GridObj.cells(row_id, GridObj.getColIndexById("FREE_YMD")).getValue();
	can_proj_ymd = GridObj.cells(row_id, GridObj.getColIndexById("CAN_PROJ_YMD")).getValue();
	next_alm_ymd = GridObj.cells(row_id, GridObj.getColIndexById("NEXT_ALM_YMD")).getValue();
	intvw_desc = GridObj.cells(row_id, GridObj.getColIndexById("INTVW_DESC")).getValue();
	intvw_ymd = GridObj.cells(row_id, GridObj.getColIndexById("INTVW_YMD")).getValue();
	intvwetc = GridObj.cells(row_id, GridObj.getColIndexById("INTVWETC")).getValue();
	
	GridObj.cells(row_id, GridObj.getColIndexById("SELECTED")).setValue("0");
}

//수정시 체크 해제
function checkRowsFRev() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return false;
	}
	return true;
}

//데이터 수정
function doRevAddRow() {
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
   	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_NM")).setValue(man_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ID")).setValue(man_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVW_CL_CD")).setValue(intvw_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CONTACT_CL_CD")).setValue(contact_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BE_PROJ")).setValue(be_proj);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("FREE_YMD")).setValue(free_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CAN_PROJ_YMD")).setValue(can_proj_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("NEXT_ALM_YMD")).setValue(next_alm_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVW_DESC")).setValue(intvw_desc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVW_YMD")).setValue(intvw_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVWETC")).setValue(intvwetc);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doRevSave();
}

//데이터 수정sql
function doRevSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
	    var SERVLETURL = G_SERVLETURL + "?mod=saveRecentSitu&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

//신규 버튼 기능
function doAddNewPop() {
	var url = "/mg/ni/popupRecentSituNewAdd.jsp"
	popupNewAddBoard(url, "인재 근황 등록", 491, 643);
}

//신규 팝업창 기능
function getPopupData() {
	doAddRow();
}

//신규 데이터
function doAddRow() {

	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_NM")).setValue(man_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAN_ID")).setValue(man_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVW_CL_CD")).setValue(intvw_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CONTACT_CL_CD")).setValue(contact_cl_cd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("BE_PROJ")).setValue(be_proj);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("FREE_YMD")).setValue(free_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("CAN_PROJ_YMD")).setValue(can_proj_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("NEXT_ALM_YMD")).setValue(next_alm_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVW_DESC")).setValue(intvw_desc);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVW_YMD")).setValue(intvw_ymd);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("INTVWETC")).setValue(intvwetc);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
	
	doAddSave();
}

//신규 데이터 sql
function doAddSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
		var cols_ids = "<%=grid_col_id%>";
    	var SERVLETURL = G_SERVLETURL + "?mod=saveNewRecentSitu&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

//삭제 버튼 기능
function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) {

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteRecentSitu&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function init() {
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"INTVW_FROM",button:"calendar_icon"});
	calendar2 = new dhtmlXCalendarObject({input:"INTVW_TO",button:"calendar_icon2"});
	calendar3 = new dhtmlXCalendarObject({input:"ALMYMD",button:"calendar_icon3"});
	calendar4 = new dhtmlXCalendarObject({input:"FREEYMD",button:"calendar_icon4"});
	calendar5 = new dhtmlXCalendarObject({input:"CAN_PROJ_YMD",button:"calendar_icon5"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar3.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar4.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar5.loadUserLanguage("<%=s_user.getLocale()%>");
	
	setFormDraw();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="8%" />
            <col width="10%" />
            <col width="8%" />
            <col width="42%" />
            <col width="10%" />
            <col width="22%" />
            <col/>
        </colgroup>
        <tr>
	        <td class="tit"><%=mm.getMessage("NIMG_1061", s_user.getLocale())%></td>
	    	<td>
				<jsp:include page="/mg/ni/comboCodeUser.jsp" >
					<jsp:param name="codeId" value="A1199"/>
					<jsp:param name="tagname" value="USER_NM"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
	    	</td>
	    	<td class="tit"><%=mm.getMessage("NIMG_1062", s_user.getLocale())%></td>
	    	<td>
    			<input type="text" name="INTVW_FROM" id="INTVW_FROM" readonly> <span><img id="calendar_icon" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
				~
    			<input type="text" name="INTVW_TO" id="INTVW_TO" readonly> <span><img id="calendar_icon2" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
    		</td>
	    	<td class="tit"><%=mm.getMessage("NIMG_1063", s_user.getLocale())%></td>
	    	<td>
	    		<input type="text" name="ALMYMD" id="ALMYMD" readonly> <span><img id="calendar_icon3" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
    		</td>
    	</tr>
        <tr>
        	<td class="tit"><%=mm.getMessage("NIMG_1064", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="MAN_NM"  size="10" class="text">
            </td>
            <td class="tit">인터뷰<br>가능일자</td>
            <td>
            	<input type="text" name="FREEYMD" id="FREEYMD" readonly> <span><img id="calendar_icon4" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
            </td>
            <td class="tit">프로젝트투입<br>가능일자</td>
            <td>
            	<input type="text" name="CAN_PROJ_YMD" id="CAN_PROJ_YMD" readonly> <span><img id="calendar_icon5" src="/../ext/images/calendar.png" border="0" style='vertical-align: text-bottom;'></span>
            </td>
        </tr>
		</table>
		
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doAddNewPop();" class="btn"><span><%=mm.getMessage("NIMG_1001", s_user.getLocale())%></span></a>
	            	<a href="javascript:doRevPop();" class="btn"><span><%=mm.getMessage("NIMG_1002", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("NIMG_1003", s_user.getLocale())%></span></a>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("NIMG_1004", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="91%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
</body>
</html>