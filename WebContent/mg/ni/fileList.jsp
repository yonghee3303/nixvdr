<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
  String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
	String docId = StringUtils.paramReplace(request.getParameter("docId"));
	String authority = StringUtils.paramReplace(request.getParameter("authority"));
%>

<html>
<head>
<title>메뉴등록관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%> <%@ include file="../../ext/include/su_grid_common.jsp" %><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%> 

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var menu_id			= "";
var down_auth 		= "";
var file_auth 		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/menu.do";
var V_SERVLETURL = "<%=contextPath%>/vdr/explorer.do";
var U_SERVLETURL = "/mg/Upload.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	doFiledownLoad(rowId);
}

function doOnRowSelect(rowId, cellInd) {}

function doFiledownLoad(rowId) {
	if(down_auth == "1") {
 	row_id = rowId;	
	var filename = GridObj.cells(row_id, GridObj.getColIndexById("FILE_NAME")).getValue();
		
	 var SERVLETURL = U_SERVLETURL + "?mod=fileDownload";	
	 var argu = "&PGM_ID=nims_file";
	 	 argu += "&DOC_ID=" + menu_id;
 	     argu += "&FILE_NAME=" + encodeURIComponent(filename);
	 fileDownload(SERVLETURL + argu);
	} else {
		alert("파일 다운로드 권한이 없습니다.");
		return;
	}
}

// 데이터 조회시점에 호출되는 함수입니다.
function doQuery() 
{
	var menu = document.form.MENU_ID.value;
	if(menu == "") {
		return;
	}
	
	var grid_col_id = "<%=grid_col_id%>";
	var argu = "&pgmId=nims_file&docId="+menu_id;

	GridObj.loadXML(U_SERVLETURL+"?mod=search&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}


// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	// if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doAddRow() {
	if(document.form.MENU_ID.value=="") {
		alert("폴더를 선택해주세요");
		return;
	}
	
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	

	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MENU_ID")).setValue(document.form.MENU_ID.value);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("P_MENU_ID")).setValue(document.form.P_MENU_ID.value);

	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MENU_PATH")).setValue(document.form.MENU_PATH.value);

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
 	for(var i=0; i < grid_array.length; i++) {
		
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("FILE_FOLDER")).getValue() != "F") {
			alert("생성할 폴더만 체크해주세요");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("FILE_NAME")).getValue() == "") {
			alert("폴더명을 입력해주세요");
			return;
		}
	}
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = V_SERVLETURL + "?mod=NimsouCreateFolder&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		//parent.refresh();		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function menuClick(menuId,parentId,menuPath,downAuth,fileAuth) {
	//alert("menuList : " + menuId + "==" + parentId + "---" + menuPath);
	document.form.MENU_ID.value = menuId;
	menu_id = menuId;
	document.form.P_MENU_ID.value = parentId;
	document.form.MENU_PATH.value = menuPath;
	down_auth = downAuth;
	file_auth = fileAuth;
	doQuery();
}

function menuClickAfterAuth(downAuth,fileAuth) {
	down_auth = downAuth;
	file_auth = fileAuth;
	doQuery();
}

function doAddFolder() {
	if(document.form.MENU_ID.value=="") {
		alert("폴더를 선택해주세요");
		return;
	}
	
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("FILE_FOLDER")).setValue("F");

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doSaveFolder() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	var p_menu_id = document.form.P_MENU_ID.value;
	var menu_id = document.form.MENU_ID.value;
	var menu_path = document.form.MENU_PATH.value;
	
	for(var i=0; i < grid_array.length; i++) {
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("FILE_FOLDER")).getValue() != "F") {
			alert("생성할 폴더만 체크해주세요");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("FILE_NAME")).getValue() == "") {
			alert("폴더명을 입력해주세요");
			return;
		}
	}
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
		var argu  = "&P_MENU_ID="	+encodeURIComponent(p_menu_id);
			argu  += "&MENU_ID="	+encodeURIComponent(menu_id);
			argu  += "&MENU_PATH="	+encodeURIComponent(menu_path);
        var SERVLETURL = V_SERVLETURL + "?mod=NimsouCreateFolder&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
	parent.refresh();
}

function doFileUpload(){
	if(file_auth == "1") {
	if(menu_id=="") {
		alert("폴더를 선택해주세요");
		return;
	}
	if(menu_id=="F2021112513423531055621054150") {
		alert("최상위 폴더를 제외한 폴더를 선택해주세요");
		return;
	}
	popupNimsAttachFile("File Upload", '', '', 500, 125, "nims_file", menu_id, "", "write","root");	
	} else {
		alert("파일 업로드 권한이 없습니다.");
		return;
	}

/* alert("MAN_ID : " +mRanId);
 var argu = "pgmId=manProfile&docId=" + mRanId;
 uploadframe.location.href='/mg/common/fileUpload.jsp?' + argu; */
 }

//첨부파일 관리  POPUP을 띄운다.
function popupNimsAttachFile(title, left, top, width, height, pgmId, docId, authority, callmode,root) {
    if (title == '') title = 'File Upload';
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 200;
    
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
    
    var url ="";
    
    if(root == false) {
    	url = contextPath;
    }
    
    if(callmode == "write") {
       url += "/mg/ni/popupNimsUploadFile.jsp";
    }else if(callmode=="repacking"){
    	url += "/mg/common/popupAttachFile_repacking.jsp";
    	resizable='no';
    }else if(callmode=="readPortal"){
    	url += "/mg/common/popupAttachFileRead_portal.jsp";
    	resizable='no';
    }else if(callmode=="readRepacking"){
    	url += "/mg/common/popupAttachFileSingleRead_portal.jsp";
    	resizable='no';
    }else {
    	url += "/mg/common/popupAttachFileRead.jsp";
    }
    
    if(callmode=="readRepacking"){
    	url +="?formid=POP_0109&gridid=GridObj";
    }else{
    	url += "?formid=POP_0106&gridid=GridObj";
    }
    url += "&pgmId=" + pgmId + "&docId=" + docId + "&authority=" + authority;
    if(callmode=="repacking"){
    	url += "&docTitle="+authority;
    }
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];
	
	var ret = window.open(url, "upload", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

function doDelete() {
	if(!checkRows()) return;
	if(file_auth == "1") {
		var grid_array = getGridChangedRows(GridObj, "SELECTED");
		if (confirm("파일을 삭제하시겠습니까?")) { // 선택하신 반출파일을 삭제하시겠습니까?
	
			var cols_ids = "<%=grid_col_id%>";
			var argu = "&pgmId=nims_file&docId="+menu_id;
	        var SERVLETURL = U_SERVLETURL + "?mod=tx_delete&col_ids="+cols_ids+argu;	
	      
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	} else {
		alert("파일 삭제 권한이 없습니다.");
		return;
	}
}

function init() {
	setFormDraw();
	// setTimeout("doQuery()",1000);
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<input name="MENU_ID" id="MENU_ID" type="hidden"  class="text" size="6" />
<input name="MENU_PATH" id="MENU_PATH" type="hidden"  class="text" size="6" />
<input name="P_MENU_ID" id="P_MENU_ID" type="hidden"  class="text" size="6" />
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="10%" />
            <col width="5%" />
            <col width="85%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
        	<td>
<%--         		<div class="title_num" align="right">
        			<%=mm.getMessage("COMG_1035", s_user.getLocale())%>  <input type="radio" name="OPTION_TYPE" value="1" checked >
        			<%=mm.getMessage("COMG_1036", s_user.getLocale())%> <input type="radio" name="OPTION_TYPE" value="0" >
        		</div> --%>
        	</td>
            <td>
	            <div class="btnarea"">
	            	<a href="javascript:doFileUpload();" class="btn"><span>파일 업로드</span></a>
	            	<!-- <a href="javascript:doQuery();" class="btn"><span>권한</span></a>
	            	<a href="javascript:doAddFolder();" class="btn"><span>새폴더 생성</span></a>
	            	<a href="javascript:doSaveFolder();" class="btn"><span>폴더 저장</span></a>
	            	<a href="javascript:doFiledownLoad();" class="btn"><span>다운로드</span></a> -->
	            	<a href="javascript:doDelete();" class="btn"><span>파일 삭제</span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=30"/>
</jsp:include>
</body>
</html>
