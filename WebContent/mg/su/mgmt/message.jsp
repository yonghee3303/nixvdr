<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%> 
<%
Log log = LogFactory.getLog(this.getClass());
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();

%>
<html>
<head>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>

var G_SERVLETURL = "<%=contextPath%>/mg/exportMgmt.do";
var usbId = "";
var rendId = "";

function init(){
	//doAjax_Message(usb, rend);	//exportMgmt.jsp 에서 호출
}

function doAjax_Message(usb, rend){
	usbId = usb;
	rendId = rend;
	
	var argu = "&USB_ID="+ usb;
	argu += "&USB_CREATE_ID="+rend;
	var url = G_SERVLETURL + "?mod=getLoginMessage"+argu; 
	sendRequest(doMessageEnd, " " ,"POST", url , false, false);
}

function doMessageEnd(oj){
	var jData = JSON.parse(oj.responseText);
	
	if(jData.errcode == "0") {
		if(jData.LOGIN_MSG.length > 0){
			document.form.LOGIN_MSG.value = jData.LOGIN_MSG;
			document.form.chk.checked = true;
			document.form.LOGIN_MSG.disabled = false;
		} else {
			document.form.LOGIN_MSG.value = "";
			document.form.chk.checked = false;
			document.form.LOGIN_MSG.disabled = true;
		}	
	} else {
		alert(jData.errmsg);
	}
}

function doSave(){
	
	var loginMsg;
	
	if(document.form.chk.checked == true) {
		loginMsg = document.form.LOGIN_MSG.value;
	} else {
		loginMsg = "";
	}

	var argu = "&USB_ID=" + encodeURIComponent(usbId);
	argu += "&USB_CREATE_ID=" + encodeURIComponent(rendId); 
	argu += "&LOGIN_MSG=" + encodeURIComponent(loginMsg);

	var url = G_SERVLETURL +"?mod=saveLoginMessage"+argu;
	sendRequest(doSaveEnd, " " ,"POST", url , false, false);
}

function doSaveEnd(oj){
	var jData = JSON.parse(oj.responseText);
	alert(jData.errmsg);	
}

function checkMsg() {
	if(document.form.chk.checked == true) {
		document.form.LOGIN_MSG.disabled = false;	
	} else {
		document.form.LOGIN_MSG.disabled = true;
	}
}

</script>
<style>
.msgDiv{
	padding:14px;
}
.msgTable1{
	width:100%;
}
.msgTable_td{
	padding-bottom:14px;
}
.msgTable_chk{
	margin-left:-1px;
	margin-bottom:-1px;
}
.msgDiv2{
	width:100%;
	border-radius:3px;
	border:solid 1px gray;
}
.msgTable2{
	width:100%;
}
.msgTable2_tr{
	border-bottom: solid 1px gray;
}
.msgTable2_td{
	padding:10px;
}
.msgTable2_textarea{
	width:90%;
	height:100px;
}
</style>
</head>
<body onload="init();">
<form name="form" method="post">
<div class="msgDiv">
	<table class="msgTable1">
		<tr>
			<td class="msgTable_td">
				<input type="checkbox" class="msgTable_chk" id="chk" name="chk" onclick="checkMsg()"><span><%=mm.getMessage("SUMG_1252", s_user.getLocale())%></span>
			</td>
		</tr>
	</table>
	<div class="msgDiv2">
	<table class="msgTable2">
		<tr class="msgTable2_tr">
			<td colspan="2" class="msgTable2_td"><%=mm.getMessage("SUMG_1253", s_user.getLocale())%></td>
		</tr>
		<tr>
			<td class="msgTable2_td"> 
				<textarea class="msgTable2_textarea"id="LOGIN_MSG"></textarea>
				<div class="btnarea">												
	       			<a href="javascript:doSave();" class="btn" style="padding-right:10px;"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	       		</div>	
	       	</td>
		</tr>
		<tr>
			<td colspan="2" class="msgTable2_td"><%=mm.getMessage("SUMG_1254", s_user.getLocale())%></td>
		</tr>
	</table>
	</div>
</div>	
</form>
</body>
</html>