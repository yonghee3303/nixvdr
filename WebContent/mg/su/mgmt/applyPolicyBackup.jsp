<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<%@ include file="../../../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/exportMgmt.do";
var calendar;
var lost_yn="";
var winChgPassword;
var initMac;

function init(){
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	
	document.getElementById("find").style.display = "none";
	document.getElementById("lost").style.display = "block"; 
	
}

function doAjax_Policy(usb, rent){

	var arg = "&USB_ID="+ usb;
	arg += "&USB_CREATE_ID="+rent;
	var url = G_SERVLETURL + "?mod=selectApplyPolicy"+arg; 
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj) {
	
	 var jData = JSON.parse(oj.responseText);
	 lost_yn = jData.RENT_STS_NM;
	if("대여"==jData.RENT_STS_NM){
		document.getElementById("find").style.display = "none";
		document.getElementById("lost").style.display = "block"; 
	}else if("분실"==jData.RENT_STS_NM){
		document.getElementById("lost").style.display = "none";
		document.getElementById("find").style.display = "block"; 
		
	}
	 document.getElementById("USB_ID").value=jData.USB_ID;
	 document.getElementById("USB_CREATE_ID").value=jData.USB_CREATE_ID;
	 
	 if(jData.MOD_STOP=='1'){	
		 		document.getElementById("MOD_STOP").checked=true; 
		 		document.getElementById("MOD_STOP_COLOR").color="red";
	}else{		
				document.getElementById("MOD_STOP").checked=false;
				document.getElementById("MOD_STOP_COLOR").color="black";
	}
	 
	 if(jData.IS_MODIFIED_USER_AUTH=='1'){	document.getElementById("IS_MODIFIED_USER_AUTH").checked=true; }
	 else{		document.getElementById("IS_MODIFIED_USER_AUTH").checked=false; }
	
	 if(jData.NO_PRINT=='1'){	document.getElementById("NO_PRINT").checked=true;	}
	 else{	document.getElementById("NO_PRINT").checked=false; }
	 	
	 if(jData.NO_COPY=='1'){	document.getElementById("NO_COPY").checked=true;	}
	 else{	document.getElementById("NO_COPY").checked=false; }
	 
	 switch(jData.NO_SAVE){	
	 case "1":
		document.getElementById("NO_SAVE1").checked=true;
		document.getElementById("NO_PRINT").disabled=true;
		document.getElementById("NO_COPY").disabled=true;	
		break;
	 case "2":
		document.getElementById("NO_SAVE2").checked=true;
		document.getElementById("NO_PRINT").disabled=false;
		document.getElementById("NO_COPY").disabled=false;
		break;
	 case "3":
		document.getElementById("NO_SAVE3").checked=true;
		document.getElementById("NO_PRINT").disabled=false;
		document.getElementById("NO_COPY").disabled=false;
	 	break;
	 case "4":	
		document.getElementById("NO_SAVE4").checked=true;
		document.getElementById("NO_PRINT").disabled=false;
		document.getElementById("NO_COPY").disabled=false;
		break;
	 }
	
	 document.getElementById("USB_NM").value=jData.USB_NM;
	 //document.getElementById("RETURN_YMD").value = jData.RETURN_YMD;
	 document.getElementById("calendar_input").value=jData.EXPIRE_YMD;
	if(jData.IS_EXPIRE_DT=='1'){	document.getElementById("IS_EXPIRE_DT").checked=true;	}
	else{	document.getElementById("IS_EXPIRE_DT").checked=false; }
	
	if(jData.IS_READCOUNT=='1'){	document.getElementById("IS_READCOUNT").checked=true;	}
	else{	document.getElementById("IS_READCOUNT").checked=false; }
	document.getElementById("READCOUNT").value = jData.READCOUNT;
	
	if(jData.IS_WAIT_CLOSE=='1'){	document.getElementById("IS_WAIT_CLOSE").checked=true;	}
	else{	document.getElementById("IS_WAIT_CLOSE").checked=false; }
	
	document.getElementById("WAIT_TIME").value = jData.WAIT_TIME;
	
	removeAllChild(document.getElementById("MAC_LIST"));
	document.getElementById("MAC_LIST").insertAdjacentHTML("afterBegin", initMac);
	
	var macAddrList=jData.MAC_ADDR.split("|");
	for(var j=0; j < macAddrList.length ;j++){
		var macHtml = form.MAC_LIST.innerHTML;
		var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>";
		if(form.MAC_LIST.value==""){
			initMac = macHtml;
			tempHtml += "<option value='"+macAddrList[j]+"' selected>"+macAddrList[j]+"</option>";
		}else{
			tempHtml += macHtml+"<option value='"+macAddrList[j]+"'>"+macAddrList[j]+"</option>";	
		}
		var tempMac = tempHtml.split(initMac);
		for(var i=0; i<tempMac.length; i++){
			if(i==0) tempHtml =tempMac[i];
			else tempHtml+=tempMac[i];
		}
		tempHtml +=initMac+"</select>";		
		removeAllChild(document.getElementById("MAC_TD"));
		document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	}
	
}
function onkeylengthMax(obj, nextobj, maxlength)
{
	if(obj.value.length==maxlength){
		if(obj != form.subMAC_6) nextobj.focus();
		return true;
	}else if(obj.value.length > maxlength){
		return false;
	}else{
		return true;
	}
}
function macAdd(){
	var submac= new Array(form.subMAC_1,form.subMAC_2,form.subMAC_3,form.subMAC_4,form.subMAC_5,form.subMAC_6);
	var macAddr="";
	for(var i=0; i<submac.length; i++){
		if(submac[i].value==""){
			alert("MAC 주소를 정확하게 입력하세요.");
			return;
		}else{
			macAddr+=submac[i].value;
		}
	}
	if(macAddr.length !=12){
		alert("MAC 주소를 정확하게 입력하세요.");
		return;
	}
	macAddr = macAddr.toUpperCase();
	
	var macHtml = form.MAC_LIST.innerHTML;//document.getElementById("MAC_LIST").innerHTML;
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>";
	if(form.MAC_LIST.value==""){
		initMac = macHtml;
		tempHtml += "<option value='"+macAddr+"' selected>"+macAddr+"</option>";
	}else{
		tempHtml += macHtml+"<option value='"+macAddr+"'>"+macAddr+"</option>";	
	}
	var tempMac = tempHtml.split(initMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	tempHtml +=initMac+"</select>";		
	removeAllChild(document.getElementById("MAC_TD"));
	document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	
	for(var i=0; i<submac.length; i++){
		submac[i].value="";
	}
	
}
function macDel(){
	var val = document.getElementById("MAC_LIST").value;

	var delMac = "<OPTION selected value="+val+">"+val+"</OPTION>";
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>"+form.MAC_LIST.innerHTML+"</select>";
	
	var tempMac = tempHtml.split(delMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	
	removeAllChild(document.getElementById("MAC_TD"));
	document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	
}
function removeAllChild(nodeTemp){
	while(nodeTemp.hasChildNodes()){
		nodeTemp.removeChild(nodeTemp.lastChild);
	}
}
function clickNoSave(){
	var noSaveType =document.getElementsByName("NO_SAVE");
	var val="";
	for(var i=0; i < noSaveType.length; i++){
        if(noSaveType[i].checked){
        	val = noSaveType[i].value;
        }
    }

	var noPrint =document.getElementById("NO_PRINT");
	var noCopy =document.getElementById("NO_COPY");
	
	switch(val){
	case "1": //파일 추가, 수정 가능
		noPrint.checked=false;
		noPrint.disabled=true;
		noCopy.checked=false;
		noCopy.disabled=true;
		break;
	case "2": //파일 추가, 수정 가능, 보안USB 외부로 복사 금지
		noPrint.checked=true;
		noPrint.disabled=false;
		noCopy.checked=true;
		noCopy.disabled=false;
		break;
	case "3": //파일 추가, 수정 금지(읽기 전용)
		noPrint.checked=true;
		noPrint.disabled=false;
		noCopy.checked=true;
		noCopy.disabled=false;
		break;
	case "4": //파일 추가 금지, 동일 파일명 수정 가능
		noPrint.checked=true;
		noPrint.disabled=false;
		noCopy.checked=true;
		noCopy.disabled=false;
		break;
	}
	
}
function clickReadCount(){
	var isReadCount = document.getElementById("IS_READCOUNT");
	var readCount = document.getElementById("READCOUNT");
	
	if(isReadCount.checked){
		readCount.value="50";
	}else{
		readCount.value="0";
	}
}
function clickWaitClose(){
	var isWaitTime = document.getElementById("IS_WAIT_CLOSE");
	var waitTime = document.getElementById("WAIT_TIME");
	
	if(isWaitTime.checked){
		waitTime.value="5";
	}else{
		waitTime.value="0";
	}
	
}
function lostUSB(){
	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	parent.updateUsbStatus(usbId, rentId, "05");
}
function findUSB(){
	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	parent.updateUsbStatus(usbId, rentId, "03");
}

function changePass(){

	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	
	if(usbId=="" || rentId==""){
		alert("<%=mm.getMessage("SUMG_2019", s_user.getLocale())%>");
		return;
	}
	
	winChgPassword = window.open("./chgPassword.jsp?USB_ID="+usbId + "&USB_CREATE_ID="+rentId,'','width=350px,height=250px,scrollbars=no,resizable=no');
}

function closeWinChgPassword(){
	winChgPassword.close();
}

//정책 적용 버튼을 눌렀을 때 발생하는 이벤트 
//각 입력된 정보를 url로 만들어 그리드가 있는  parent의 펑션으로 넘겨주어 action을 연결한다. 
function applyPolicy(){
	if(lost_yn=="분실"){
		alert("분실한 상태에서는 일체 사용할 수 없으며, 정책변경 또한 불가능합니다.");
		return;
	}
	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	
	if(usbId=="" && rentId==""){
		alert("<%=mm.getMessage("SUMG_2015", s_user.getLocale())%>");
	}else{
	
		if(confirm("<%=mm.getMessage("SUMG_1006", s_user.getLocale())%>")==false){
			return;
		}
		
		var url ="&USB_ID="+usbId+"&USB_CREATE_ID="+rentId;
		
		url+= "&MOD_STOP=";
		if(document.getElementById("MOD_STOP").checked)
			url+= "1";
		else
			url+= "0";
	
		url+="&IS_MODIFIED_USER_AUTH=";
		if(document.getElementById("IS_MODIFIED_USER_AUTH").checked)
			url+="1";
		else
			url+="0";
	
		url+="&NO_SAVE=";
		if(document.getElementById("NO_SAVE1").checked)
			url+="1";
		else if(document.getElementById("NO_SAVE2").checked)
			url+="2";	
		else if(document.getElementById("NO_SAVE3").checked)
			url+="3";
		else
			url+="4";
	
		//USB_NM은 nvachar 형태로 한글 입력이 가능하다.
		url+="&USB_NM="+ encodeURIComponent(document.getElementById("USB_NM").value);
		
		url+="&IS_EXPIRE_DT=";
		if(document.getElementById("IS_EXPIRE_DT").checked)
			url+="1";
		else
			url+="0";
		
		url+="&EXPIRE_YMD="+document.getElementById("calendar_input").value;
		
		url+="&IS_READCOUNT=";
		if(document.getElementById("IS_READCOUNT").checked)
			url+="1";
		else
			url+="0";
		
		url+="&READCOUNT="+document.getElementById("READCOUNT").value;
		
		url+="&NO_PRINT=";
		if(document.getElementById("NO_PRINT").checked)
			url+="1";
		else
			url+="0";
		
		url+="&NO_COPY=";
		if(document.getElementById("NO_COPY").checked)
			url+="1";
		else
			url+="0";
		
		url+="&IS_WAIT_CLOSE=";
		if(document.getElementById("IS_WAIT_CLOSE").checked)
			url+="1";
		else
			url+="0";
		
		url+="&WAIT_TIME="+document.getElementById("WAIT_TIME").value;
		
		var macAddrList = document.getElementById("MAC_LIST");
		var macAddr="";
		for(var i=0; i<macAddrList.options.length; i++){
			if(macAddrList.options[i].value!=""){
				if(macAddr=="") macAddr = macAddrList.options[i].value.toUpperCase();
				else macAddr +="|"+macAddrList.options[i].value.toUpperCase();
			}
		}
		
		url+="&MAC_ADDR="+macAddr;
	//그리드가 있는 페이지로 값을 넘겨준다. 
	parent.updatePolicy(url);
	
	}
}
</script>
<style>
 tr, td, div {
 font-family: Tahoma;
 padding:3px;
 }
 .macInput{
 	width:27px;
 	padding: 0px 2px 0px 2px;
 }
 .macTable td{
 	padding:3px 0px 3px 0px;
 }
</style>
</head>
<body onload="init();" style="margin: 0px; padding: 8px; overflow:hidden;">
<form name="form">
<input id="USB_ID" type="hidden"/>
<input id="USB_CREATE_ID" type="hidden"/>
<table style="width: 100%;">
	<tr>
		<td colspan="2">
		<table style="width: 100%;">
			<tr>
				<td><input id="MOD_STOP" type="checkbox"/><font id="MOD_STOP_COLOR"> <%=mm.getMessage("SUMG_1182", s_user.getLocale())%></font>
					<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="button" value="분실" onclick="lostUSB();" style="width:70px;"> &nbsp;&nbsp; <input type="button" value="패스워드 변경" onclick="changePass();"> -->
				</td>
				<td style="text-align:right"><!-- <input type="button" value="설정 적용" onclick="applyPolicy();"> -->
					<div class="btnarea">
						<a id="find" href="javascript:findUSB();" class="btn"><span id="find"><%=mm.getMessage("SUMG_1102", s_user.getLocale())%></span></a>
						<a id="lost" href="javascript:lostUSB();" class="btn"><span id="lost"><%=mm.getMessage("SUMG_1101", s_user.getLocale())%></span></a>
	            		<a href="javascript:changePass();" class="btn"><span><%=mm.getMessage("SUMG_1103", s_user.getLocale())%></span></a>
		            	<a href="javascript:applyPolicy();" class="btn"><span><%=mm.getMessage("SUMG_1104", s_user.getLocale())%></span></a>
		        	</div>
				</td>
			</tr>
			<tr><td colspan="2"><input id="IS_MODIFIED_USER_AUTH" type="checkbox"> <%=mm.getMessage("SUMG_1146", s_user.getLocale())%></td></tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp; <%=mm.getMessage("SUMG_1172", s_user.getLocale())%>
			<div style="border: 2px dashed #a1ceed;">
				<table><tr><td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1"> <%=mm.getMessage("SUMG_1106", s_user.getLocale())%></td></tr>
						<tr><td><input id="NO_SAVE2" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2"> <%=mm.getMessage("SUMG_1107", s_user.getLocale())%></td></tr>
						<tr><td><input id="NO_SAVE3" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3"> <%=mm.getMessage("SUMG_1108", s_user.getLocale())%></td></tr>
						<tr><td><input id="NO_SAVE4" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4"> <%=mm.getMessage("SUMG_1109", s_user.getLocale())%></td></tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td><table style="table-layout:fixed;" >
				<colgroup>
  		    		<col width="250px"/> <!-- 검색기간 -->
            		<col width="300px"/> <!-- 텍스트 필드 -->	
				</colgroup>
				<tr><td>&nbsp;<%=mm.getMessage("SUMG_1110", s_user.getLocale())%></td><td  ><input id="USB_NM" type="text" value="" style="width:120px"></td></tr>
				<tr><td><input id="IS_EXPIRE_DT" type="checkbox"> <%=mm.getMessage("SUMG_1111", s_user.getLocale())%></td><td><input type="text" id="calendar_input" style="width:100px"> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span></td></tr>
				<tr><td><input id="IS_READCOUNT" type="checkbox" onclick="clickReadCount();"> <%=mm.getMessage("SUMG_1112", s_user.getLocale())%></td><td><input  id="READCOUNT" type="text" style="width:50px" value=""><%=mm.getMessage("SUMG_1113", s_user.getLocale())%> </td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp; <%=mm.getMessage("SUMG_1114", s_user.getLocale())%>
			<div style="border: 2px dashed #a1ceed;">
				<table>
					<tr><td><input id="NO_PRINT" type="checkbox"> <%=mm.getMessage("SUMG_1115", s_user.getLocale())%></td></tr>
					<tr><td><input id="NO_COPY" type="checkbox"> <%=mm.getMessage("SUMG_1116", s_user.getLocale())%></td></tr>
					<tr><td><input id="IS_WAIT_CLOSE" type="checkbox" onclick="clickWaitClose();"> <%=mm.getMessage("SUMG_1117", s_user.getLocale())%></td></tr>
					<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=mm.getMessage("SUMG_1118", s_user.getLocale())%> <input id="WAIT_TIME" type="text" value="" style="width:50px"><%=mm.getMessage("SUMG_1119", s_user.getLocale())%></td></tr>
					<!-- <tr><td><input id="IS_EXPIRE_INIT" type="checkbox"> 기간 만료 시 보안 USB 초기화</td></tr>
					<tr><td><input id="IS_READ_INIT" type="checkbox"> 지정횟수 초과시 보안 USB 초기화</td></tr> -->
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td>&nbsp; 사용자 MAC 주소 리스트 
		<div style="border: 2px dashed #a1ceed;">
			<table class="macTable">
				<tr><td>
						<input type="text" name="subMAC_1" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_2, 2)"/>&nbsp;<input type="text" name="subMAC_2" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_3, 2)"/>&nbsp;
						<input type="text" name="subMAC_3" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_4, 2)"/>&nbsp;<input type="text" name="subMAC_4" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_5, 2)"/>&nbsp;
						<input type="text" name="subMAC_5" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_6, 2)"/>&nbsp;<input type="text" name="subMAC_6" class="macInput"  onkeypress="return onkeylengthMax(this, form.addMacBtn, 2)"/>
					</td>
					<td>
						<div class="btnarea">
				         	<a href="javascript:macAdd();" class="btn" name="addMacBtn"><span>Mac 추가</span></a>
				        </div>
					</td>
				</tr>
				<tr>
					<td id="MAC_TD">
						<input type="hidden" name="MAC_ADDR" value="">
						<select name="MAC_LIST" id="MAC_LIST" multiple>
							<option value=''>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</option>
						</select>
					</td>
					<td>
						<div class="btnarea">
				          	<a href="javascript:macDel();" class="btn"><span>Mac 삭제</span></a>
				        </div>
				    </td>
				</tr>
			</table>
			</div>	
		</td>
	</tr>
</table>
</form>
</body>
</html>