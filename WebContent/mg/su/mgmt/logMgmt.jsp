<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>

<title>보안USB 로그관리</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>

<script>

/* var layout;
var cell1;
var cell2; */
var calendar1;
var calendar2;

var GridObj = {};
var G_SERVLETURL = "<%=contextPath%>/mg/logMgmt.do";

function init() {
	
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar1 = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2 = new dhtmlXCalendarObject({input:"calendar_input2", button:"calendar_icon2"});
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	
	calendar1.hideTime();
	calendar2.hideTime();
	
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onXLE", doCheckListCount);
	
	document.getElementById("calendar_input").value="<%=fromDay%>";
	document.getElementById("calendar_input2").value="<%=serverDay%>";
	document.getElementById("week").checked=true;
	doQuery();
	
}
function doCheckListCount(grid_obj,count){
	var rowsCount = grid_obj.getRowsNum(); //총 갯수
	document.getElementById("listCount").innerHTML = "<b>"+rowsCount+"</b>";
}
//로그는 계속 쌓이기 때문에 당일 날짜만 나오도록 조건을 주어 초기 검색목록을 보여준다.
function doQuery() {
	//현재 날짜 세팅
	var now = new Date();
	
	// 년월일시분을 가져옴
	var year  = now.getFullYear();
    var month = now.getMonth() + 1; // 1월=0,12월=11이므로 1 더함
    var day   = now.getDate();
    var hour  = now.getHours();
    var minute   = now.getMinutes();

    // 각각 2자리수로 만듬
    if (("" + month).length == 1) { month = "0" + month; }
    if (("" + day).length   == 1) { day   = "0" + day;   }
    if (("" + hour).length  == 1) { hour  = "0" + hour;  }
    if (("" + minute).length   == 1) { minute   = "0" + minute;   }

   var nowDate = "" + year + "-" + month + "-" + day;
	
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectLogMgmt&grid_col_id="+grid_col_id+"&FROM_YMD="+nowDate);																						
	GridObj.clearAll(false);
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	<%-- document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> "; --%>
	if(status == "false") alert(msg);
	return true;
}


//라디오버튼 클릭시 발생이벤트
function setDate() {

	//라디오버튼을 배열로 반환
	var rb_array = document.getElementsByName("time");
	
	//어떤 라디오버튼이 눌렸는지 체크
	for (var i=0;i<rb_array.length;i++)
	{
		if(rb_array[i].checked)
		{
			var time = (rb_array[i].value);
		}
	}
	
	//현재 날짜 세팅
	var now = new Date();
	
	// 년월일시분을 가져옴
	var year  = now.getFullYear();
    var month = now.getMonth() + 1; // 1월=0,12월=11이므로 1 더함
    var day   = now.getDate();
    var hour  = now.getHours();
    var minute   = now.getMinutes();

    // 각각 2자리수로 만듬
    if (("" + month).length == 1) { month = "0" + month; }
    if (("" + day).length   == 1) { day   = "0" + day;   }
    if (("" + hour).length  == 1) { hour  = "0" + hour;  }
    if (("" + minute).length   == 1) { minute   = "0" + minute;   }

    document.getElementById("calendar_input2").value = "" + year + "-" + month + "-" + day;
	
    // 과거 날짜 세팅
    var ago = new Date();
    
	switch(time){
	
		case "all" :
			document.getElementById("calendar_input").value = "";
			document.getElementById("calendar_input2").value = "";
			return;
			
		case "week" :
		    
		    // 일주일 전 날짜 세팅
		    ago.setDate(ago.getDate() - 7);	    
			 
			break;
			
		case "month" :
			
			// 한달 전 날짜 세팅
		    ago.setMonth(ago.getMonth() - 1);
		    
			break;
			
		case "months" :
			
			// 3달 전 날짜 세팅
		    ago.setMonth(ago.getMonth() - 3);

			break;
	
	}
	
	var yy  = ago.getFullYear();
    var mm = ago.getMonth() + 1;
    var dd   = ago.getDate();
    var hh  = ago.getHours();
    var min   = ago.getMinutes();
	
	if (("" + mm).length == 1) { mm = "0" + mm; }
    if (("" + dd).length   == 1) { dd   = "0" + dd;   }
    if (("" + hh).length  == 1) { hh  = "0" + hh;  }
    if (("" + min).length   == 1) { minute   = "0" + min;   }
	
	document.getElementById("calendar_input").value = "" + yy + "-" + mm + "-" + dd;
	
}

function searchLog(){
	//검색버튼 이벤트
	var fromDate = document.getElementById("calendar_input").value;
	var toDate = document.getElementById("calendar_input2").value;
	var argu= "&FROM_YMD="+encodeURIComponent(fromDate);
	argu += "&TO_YMD="+encodeURIComponent(toDate);
	argu +="&USER_NM="+ encodeURIComponent(document.getElementById("USER_NM").value);
	argu +="&USB_ID="+ encodeURIComponent(document.getElementById("USB_ID").value);
	argu +="&DEPT_NM="+ encodeURIComponent(document.getElementById("DEPT_NM").value);
	argu +="&POSITION_NM="+ encodeURIComponent(document.getElementById("POSITION_NM").value);
	argu +="&MAC_ADDR="+ encodeURIComponent(document.getElementById("MAC_ADDR").value);
	argu +="&IP_ADDR="+ encodeURIComponent(document.getElementById("IP_ADDR").value);
	if(document.form.WORK_TYPE.value !=" "){
		argu +="&WORK_TYPE="+ encodeURIComponent(document.form.WORK_TYPE.value);
	}	
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectLogMgmt&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false); 
	
}

function resize() {
	GridObj.setSizes();
	
}

function initSearch(){ //초기화 하면 검색 조건들을 모두 지운다.
	
 	var rb_array = document.getElementsByName("time");
	rb_array[0].checked=true;
	document.getElementById("calendar_input").value="";
	document.getElementById("calendar_input2").value="";
	document.getElementById("USER_NM").value="";
	document.getElementById("USB_ID").value="";
	document.getElementById("DEPT_NM").value="";
	document.getElementById("POSITION_NM").value="";
	document.getElementById("MAC_ADDR").value="";
	document.getElementById("IP_ADDR").value="";
	
	//doQuery();
}

</script>

<style>
div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}

    
</style>

</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:5px; overflow: hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
       <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px"/> <!-- 검색기간 -->
            <col width="200px"/> <!-- 텍스트 필드 -->
            <col width="80px"/>  <!-- USB ID -->
            <col width="150px"/> <!-- 텍스트 필드 -->
            <col width="80px"/> <!-- 부서 -->
            <col width="250px"/><!-- 텍스트 필드 -->
            <col width="80px"/>	<!-- 직책 -->
            <col width="150px"/>	<!-- 텍스트 필드 -->
            <col/><!-- 버튼 -->
        </colgroup>
        <tr>
        	<td class="tit">
        		<%=mm.getMessage("COMG_1046", s_user.getLocale())%>
        	</td>
            <td>
            	<input class="text" type='text' id='calendar_input' value='' size="8"> <img id='calendar_icon' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"> 
            	~
				<input class="text" type='text' id='calendar_input2' value='' size="8"> <img id='calendar_icon2' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand">
			</td>
			<td colspan="2">
	            <input type="radio" name="time" value="all" onclick="setDate();"><%=mm.getMessage("COMG_1032", s_user.getLocale())%>
				<input type="radio" name="time" value="week" onclick="setDate();" id="week">1<%=mm.getMessage("COMG_1066", s_user.getLocale())%>
				<input type="radio" name="time" value="month" onclick="setDate();">1<%=mm.getMessage("COMG_1067", s_user.getLocale())%>
				<input type="radio" name="time" value="months" onclick="setDate();">3<%=mm.getMessage("COMG_1067", s_user.getLocale())%>
			</td>
			<td colspan="5"></td>
		</tr>
		<tr>
			<td class="tit">
            	<%=mm.getMessage("SUMG_1137", s_user.getLocale())%>
            </td>
            <td>
            	<input class="text" type="text" id="USER_NM">
            </td>
            <td class="tit">
            	<%=mm.getMessage("SUMG_1176", s_user.getLocale())%>
            </td>
            <td>
            	<input class="text" type="text" id="USB_ID">
            </td>
            <td class="tit">
            	<%=mm.getMessage("COMG_1063", s_user.getLocale())%>            
            </td>
            <td>
            	<input class="text" type="text" id="DEPT_NM">
            </td>
            <td class="tit">
            	<%=mm.getMessage("COMG_1062", s_user.getLocale())%>
            </td>
            <td>
            	<input class="text" type="text" id="POSITION_NM">
            </td>
            <td></td>
		</tr>
        <tr>
            <td class="tit">
            	MAC <%=mm.getMessage("SUMG_1183", s_user.getLocale())%>
            </td>
            <td>
            	<input class="text" type="text" id="MAC_ADDR">
            </td>
            <td class="tit">
            	IP <%=mm.getMessage("SUMG_1183", s_user.getLocale())%>
            </td>
            <td>
            	<input class="text" type="text" id="IP_ADDR">
            </td>
            <td class="tit">
            	<%=mm.getMessage("SUMG_1184", s_user.getLocale())%> 
            </td>
            <td>
				<jsp:include page="../../common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0502"/>
					<jsp:param name="tagname" value="WORK_TYPE"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
			</td>
			<td></td>
			<td colspan="2"><div class="btnarea">
	            	<a href="javascript:initSearch();" class="btn"><span><%=mm.getMessage("COMG_1065", s_user.getLocale())%></span></a>
	            	<a href="javascript:searchLog();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
	            </div>
	    	</td>
        </tr>
		</table>

		<!-- 임시 공간 -->
		<table width="100%" border="0">
        	<tr>
				<td>
				 총 : <font color="red" id="listCount"></font> 건
				</td>
			</tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" style="width:100%; height:83%; background-color:white; overflow:hidden;"></div>


</body>
</html>