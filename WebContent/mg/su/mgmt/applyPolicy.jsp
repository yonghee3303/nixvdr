<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%> 
<%
Log log = LogFactory.getLog(this.getClass());
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String userIp = request.getRemoteAddr();
%>
<html>
<head>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>
<%-- <%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %> --%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/exportMgmt.do";
var calendar;
var lost_yn="";
var winChgPassword;
var initMac;
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
//등록화면
var addRowId		= 0;
var ed_flag 		= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

//정책그룹세트를 저장
var jPolicyData;
var usbId = "";
var rentId = "";

var loading=false;

function init(){

	loading = true;
	setFormDraw();
	doPolicyGrpCombo();
	doLoad();
	

	//한글로 달력에 표시하기 위해서는 아래와 같은 데이터들이 필요로한다.
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar1 = new dhtmlXCalendarObject({input:"EXPIRE_YMD",button:"calendar_icon"});
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar1.hideTime();

}

function doAjax_Policy(usb, rent)
{
	usbId = usb;
	rentId = rent;
	
	var arg = "&USB_ID="+ usb;
	arg += "&USB_CREATE_ID="+rent;
	var url = G_SERVLETURL + "?mod=selectApplyPolicy"+arg; 
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj) {
		
	var jData = JSON.parse(oj.responseText);
	//MAC_LIST 초기화
	document.form.MAC_LIST.innerHTML = "<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";

	//정책그룹코드 세팅
	jPolicyData = new Object();
	jPolicyData.SU_POLICY_GRP_CD = jData.SU_POLICY_GRP_CD;
	doLoad();
	
	 lost_yn = jData.RENT_STS_NM;	//수불상태
	
	document.getElementById("IS_LOGIN_FAIL_COUNT").value = jData.IS_LOGIN_FAIL_COUNT;
	document.getElementById("EXPIRE_YMD").value = jData.EXPIRE_YMD; //만료 일자(실행가능 기간)
	//document.getElementById("MAC_LIST").value = jData.MAC_ADDR;
	document.getElementById("READCOUNT").value = jData.READCOUNT; //실행가능 횟수
	 
	rentUSB();						//수불 상태 
	setImageValue("MOD_STOP");		//USB 사용여부
	loginFailCount();				//로그인 실패 횟수
	setRadioValue("IS_READCOUNT");	//실행 횟수
	setRadioValue("IS_EXPIRE_DT");	//만료 기간
	setImageValue("IS_APPROVAL");	//외부 사용
	iswaitclose();					//지정시간 이상 사용시 자동 종료
	
	//콤보박스 설정
	if(jData.IS_LOGIN_FAIL_STOP == "Y") {
		document.form.FAIL_USB_USE_CD.value = "STOP";	
	} else {
		document.form.FAIL_USB_USE_CD.value = "DELETE";
	}
	if(jData.IS_READ_STOP == "Y") {
		document.form.READ_USB_USE_CD.value = "STOP";	
	} else {
		document.form.READ_USB_USE_CD.value = "DELETE";
	}
	if(jData.IS_EXPIRE_STOP == "Y") {
		document.form.EXPIRE_USB_USE_CD.value = "STOP";	
	} else {
		document.form.EXPIRE_USB_USE_CD.value = "DELETE";
	}
	
	//MAC LIST
	var macList = jData.MAC_ADDR.split("|");
	for(var i=0; i<macList.length; i++) {
		if(macList[i].length > 0) {
			macAdd(macList[i]);	
			document.form.MAC_USE_YN.checked = true;
		}else{
			document.form.MAC_USE_YN.checked = false;
		}
	}

	/*USB 상태. 보유 중, 대여 증, 사용금지 중, 분실 중, 삭제됨*/
	function rentUSB(){
		var rent = lost_yn;
		document.getElementById('RENT_ST_NM').innerHTML = rent;		
	}
	
	
	/*로그인 실패 횟수*/
	function loginFailCount(){
		var count = document.getElementById('IS_LOGIN_FAIL_COUNT').value;
		if(count > 0) {
			jData.IS_LOGIN_FAIL_YN = "Y";
		} else {
			jData.IS_LOGIN_FAIL_YN = "N";
		}
		setRadioValue("IS_LOGIN_FAIL_YN");	
	}
	
	
	/*지정시간 이상 사용시 자동 종료*/
	function iswaitclose(){
		
		var IS_WAIT_CLOSE = jData.IS_WAIT_CLOSE;  /*자동종료 값 받아옴*/
		var IS_WAIT_CLOSE1 = document.getElementsByName("IS_WAIT_CLOSE");
	
		if(IS_WAIT_CLOSE == 'Y'){
			IS_WAIT_CLOSE1[0].checked = true;
		} else {
			IS_WAIT_CLOSE1[0].checked = false;
		}
		document.getElementById('WAIT_TIME').value = jData.WAIT_TIME;	
	}
	
	//라디오박스 값 세팅
	function setRadioValue(id) {
		if(jData[id] == "Y") {
			document.getElementById(id+"_Y").checked = true;
		} else {
			document.getElementById(id+"_N").checked = true;
		}
	}
	
	function setImageValue(id){
		var imgSrc=document.getElementById(id+"_IMG").src;
		var imgSrcArray =imgSrc.split("/");
		var baseUrl="";
		var on ="ON.gif";
		var off="OFF.gif";
		if(imgSrcArray[imgSrcArray.length-1]==on){
			baseUrl=imgSrc.slice(0, imgSrc.length-on.length);	
		}else{
			baseUrl=imgSrc.slice(0, imgSrc.length-off.length);
		}

		if(jData[id] == "Y") {
			if(id=="MOD_STOP"){
				document.getElementById(id+"_IMG").src=baseUrl+off;
				document.form.MOD_STOP_DEL.checked=false;
			}
			else document.getElementById(id+"_IMG").src=baseUrl+on;
		}else if(jData[id] == "D" && id=="MOD_STOP"){
			document.form.MOD_STOP_DEL.checked=true;
			document.getElementById(id+"_IMG").src=baseUrl+off;
		}else{
			if(id=="MOD_STOP"){
				document.getElementById(id+"_IMG").src=baseUrl+on;
				document.form.MOD_STOP_DEL.checked=false;
			}
			else document.getElementById(id+"_IMG").src=baseUrl+off;
		}
	}
	
	//disable
	doChangeCheckBox(document.form.IS_WAIT_CLOSE);
	doChangeCheckBox(document.form.MAC_USE_YN);
	doChangeRadio("IS_LOGIN_FAIL_YN");
	doChangeRadio("IS_READCOUNT");
	doChangeRadio("IS_EXPIRE_DT");

	if(jData.USECOUNT!="")	document.getElementById("USECOUNT").innerHTML = "/ 현재 실행횟수: "+jData.USECOUNT;
}

/*맥 관리*/
function macAdd(id){
	var macAddr = "";
	var submac= new Array(form.subMAC_1,form.subMAC_2,form.subMAC_3,form.subMAC_4,form.subMAC_5,form.subMAC_6);
	
	if(id != null && id.length > 0) {
		macAddr = id;
	} else {	
		for(var i=0; i<submac.length; i++){
			if(submac[i].value==""){
				alert("<%=mm.getMessage("SUMG_2029", s_user.getLocale())%>");
				return;
			}else{
				macAddr+=submac[i].value;
			}
		}	
	}
	
	if(macAddr.length !=12){
		alert("<%=mm.getMessage("SUMG_2029", s_user.getLocale())%> : '" + macAddr + "' (" + macAddr.length + ")");
		return;
	}
	var macHtml = form.MAC_LIST.innerHTML;//docusment.getElementById("MAC_LIST").innerHTML;
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>";
	if(form.MAC_LIST.value==""){
		initMac = macHtml;
		tempHtml += "<option value='"+macAddr+"' selected>"+macAddr+"</option>";
	}else{
		tempHtml += macHtml+"<option value='"+macAddr+"'>"+macAddr+"</option>";	
	}
	var tempMac = tempHtml.split(initMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	tempHtml +=initMac+"</select>";		
	removeAllChild(document.getElementById("MAC_TD"));
	document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	
	for(var i=0; i<submac.length; i++){
		submac[i].value="";
	}
}

/*MAC 삭제*/
function macDel(){
									/*입력한 값 이 들어옴*/
	var val = document.getElementById("MAC_LIST").value;
	
	var delMac = "<OPTION selected value="+val+">"+val+"</OPTION>";
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>"+form.MAC_LIST.innerHTML+"</select>";
	
	//입력한 값 쪼개는거고
	var tempMac = tempHtml.split(delMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	
	var select = document.getElementById("MAC_LIST");
	var i;
	for(i=select.length - 1; i >= 0; i --){
		if(select.options[i].selected){
			select.remove(i);
		}
	}
	
}

function removeAllChild(nodeTemp){
	while(nodeTemp.hasChildNodes()){
		nodeTemp.removeChild(nodeTemp.lastChild);
	}
}

function onkeylengthMax(obj, nextobj, maxlength)
{
	if(obj.value.length==maxlength){
		if(obj != form.subMAC_6) nextobj.focus();
		else document.getElementById("addMacBtn").focus();
		return true;
	}else if(obj.value.length > maxlength){
		return false;
	}
}


/*저장 버튼 클릭시*/
function doSave(){
	
	if(rentId == null || rentId == "") {
		alert("<%=mm.getMessage("SUMG_2030", s_user.getLocale())%>");
		return;
	}
	
	var obj = new Object();
	
	obj.USB_ID = usbId;
	obj.USB_CREATE_ID = rentId; 
	getImageValue("MOD_STOP");		//USB 사용
	getImageValue("IS_APPROVAL");	//USB 외부 사용
	obj.IS_LOGIN_FAIL_COUNT = document.getElementById('IS_LOGIN_FAIL_COUNT').value;	//로그인 실패
	
	getRadioValue("IS_READCOUNT");	//파일 실행횟수 제한 여부
	if(obj.IS_READCOUNT == "Y") obj.READCOUNT = document.getElementById("READCOUNT").value;	//실행 가능 횟수
	
	getRadioValue("IS_EXPIRE_DT");	//사용기간 제한 여부
	if(obj.IS_EXPIRE_DT =="Y") obj.EXPIRE_YMD = document.getElementById("EXPIRE_YMD").value;	//사용기간
	else obj.EXPIRE_YMD ="";
	
	//콤보박스 값 추출
	if(document.form.READ_USB_USE_CD.value == "STOP") {
		obj.IS_READ_STOP = "Y";
		obj.IS_READ_INIT = "N";
	}else {
		obj.IS_READ_STOP = "N";
		obj.IS_READ_INIT = "Y";
	}
	
	if(document.form.FAIL_USB_USE_CD.value == "STOP") {
		obj.IS_LOGIN_FAIL_STOP = "Y";
		obj.IS_LOGIN_FAIL_INIT = "N";
	}else if(document.form.FAIL_USB_USE_CD.value == "DELETE"){
		obj.IS_LOGIN_FAIL_STOP = "N";
		obj.IS_LOGIN_FAIL_INIT = "Y";
	}
	if(document.getElementById("IS_LOGIN_FAIL_COUNT").value==""){
		obj.IS_LOGIN_FAIL_STOP = "N";
		obj.IS_LOGIN_FAIL_INIT = "N";
	}
	
	if(document.form.EXPIRE_USB_USE_CD.value == "STOP") {
		obj.IS_EXPIRE_STOP = "Y";
		obj.IS_EXPIRE_INIT = "N";
	} else {
		obj.IS_EXPIRE_STOP = "N";
		obj.IS_EXPIRE_INIT = "Y";
	}
	
	//지정 시간 경과후 자동 종료
	if(document.form.IS_WAIT_CLOSE.checked) {
		obj.IS_WAIT_CLOSE = "Y";
		obj.WAIT_TIME = document.getElementById("WAIT_TIME").value;
	} else {
		obj.IS_WAIT_CLOSE = "N";
		obj.WAIT_TIME = "";
	}
	
	//MAC 주소 추출
	if(document.form.MAC_USE_YN.checked){
		var macList = document.getElementById("MAC_LIST");
		var macAddr = "";
		for(var i=0; i<macList.length; i++) {
			if(macList[i].value != "") {
				macAddr += macList[i].value + "|";  
			}
		}
		obj.MAC_ADDR = macAddr.substring(0, macAddr.length-1);
	}else{
		obj.MAC_ADDR="";
	}
	
	//document.form.radioName.value 를 통한 value 값 추출 및 세팅이 안되고 있음
	//라디오박스 값 추출
	function getRadioValue(id) {
		var radio = document.getElementsByName(id);
		for(var i=0; i<radio.length; i++) {
			if(radio[i].checked) {
				obj[id] = radio[i].value;	
			}
		}
	}
	//이미지 주소를 읽어 값 갖고 오기
	function getImageValue(id){
		var imgSrc = document.getElementById(id+"_IMG").src.split("/");
		if(imgSrc[imgSrc.length-1] =="ON.gif"){
			if(id=="MOD_STOP") obj[id] = "N";
			else obj[id] = "Y";
		}else{
			if(id=="MOD_STOP"){
				if(document.form.MOD_STOP_DEL.checked) obj[id]="D";
				else obj[id] = "Y";
			}
			else obj[id] = "N";
		}
	}
	//정책그룹코드
	obj.SU_POLICY_GRP_CD = jPolicyData.SU_POLICY_GRP_CD;
	//수정자 IP
	obj.IP_ADDR = "<%=userIp%>";
	
	var argu = "&USB_ID=" + usbId;
	argu += "&USB_CREATE_ID=" + rentId; 
	argu += "&POLICY=" + JSON.stringify(obj);

	var url = G_SERVLETURL +"?mod=updateUsbExportPolicy"+argu;
	sendRequest(doSaveEnd, " " ,"POST", url , false, false);
}

function doSaveEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	alert(jData.errmsg);	
	//init();
}

function checkDisabled(jsonId, selId, status) {
	if(jsonId != null) {
		document.getElementById(jsonId).disabled = status;
	}
	if(selId != null) {
		document.form[selId].disabled = status;
	}
}

function doPwReset() {
	if(rentId == null || rentId == "") {
		alert("<%=mm.getMessage("SUMG_2030", s_user.getLocale())%>");
		return;
	}
	if (confirm("<%=mm.getMessage("SUMG_1009", s_user.getLocale())%> <%=mm.getMessage("SUMG_1010", s_user.getLocale())%>")) {
		var argu = "&USB_ID=" + usbId;
		argu += "&USB_CREATE_ID=" + rentId;
		argu += "&IP_ADDR=" + "<%=userIp%>";
		
		var url = G_SERVLETURL + "?mod=usbPassReset"+ argu;	
	 
		sendRequest(doSaveEnd, " " ,"POST", url , false, false);
	}
}

function lostUSB(){
	if(rentId == null || rentId == "") {
		alert("<%=mm.getMessage("SUMG_2030", s_user.getLocale())%>");
		return;
	}
	if (confirm("<%=mm.getMessage("SUMG_1011", s_user.getLocale())%> <%=mm.getMessage("SUMG_1010", s_user.getLocale())%>")) {
		parent.updateUsbStatus(usbId, rentId, "05");
	}
	
}
function findUSB(){
	if(rentId == null || rentId == "") {
		alert("<%=mm.getMessage("SUMG_2030", s_user.getLocale())%>");
		return;
	}
	
	if (confirm("분실 취소를 하시겠습니까? <%=mm.getMessage("SUMG_1010", s_user.getLocale())%>")) {
		parent.updateUsbStatus(usbId, rentId, "03");
	}
}
function doChangeImg(imgId, imgSrc){
	var src = imgSrc.split("/");
	var baseUrl="";
	var on ="ON.gif";
	var off="OFF.gif";
	if(src[src.length-1]==on){
		baseUrl=imgSrc.slice(0, imgSrc.length-on.length);
		document.getElementById(imgId).src=baseUrl+off;
	}else{
		baseUrl=imgSrc.slice(0, imgSrc.length-off.length);
		document.getElementById(imgId).src=baseUrl+on;
	}

}

//상세 내역 disable 
function doChangeRadio(radioNm){
	var radioValue="";
	var radio = document.getElementsByName(radioNm);
	for(var i=0; i<radio.length; i++) {
		if(radio[i].checked) {
			radioValue = radio[i].value;	
		}
	}
	var temp =false;
	if(radioValue=="N"){
		temp=true;
	}
	if(radioNm=="IS_LOGIN_FAIL_YN"){
		if(radioValue=="N") document.form.IS_LOGIN_FAIL_COUNT.value="";
		document.form.IS_LOGIN_FAIL_COUNT.disabled=temp;
		document.form.FAIL_USB_USE_CD.disabled=temp;
	}else if(radioNm=="IS_READCOUNT"){
		document.form.READCOUNT.disabled=temp;
		document.form.READ_USB_USE_CD.disabled=temp;
	}else if(radioNm=="IS_EXPIRE_DT"){
		document.form.EXPIRE_YMD.disabled=temp;
		document.form.EXPIRE_USB_USE_CD.disabled=temp;
	}
}

function doChangeCheckBox(check){
	var temp = true;
	if(check.checked){
		temp =false;
	}
	if(check.name =="IS_WAIT_CLOSE"){
		document.form.WAIT_TIME.disabled=temp;
	}else if(check.name =="MAC_USE_YN"){
		var submac= new Array(form.subMAC_1,form.subMAC_2,form.subMAC_3,form.subMAC_4,form.subMAC_5,form.subMAC_6);
		for(var i=0; i<submac.length; i++){
			submac[i].disabled=temp;
		}
		document.form.MAC_LIST.disabled=temp;
	}
	
}

function chageModDel(check){
	if(check.checked){
		alert("<%=mm.getMessage("SUMG_2031", s_user.getLocale())%>");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////


function doLoad() {
	if(jPolicyData != null) {
		document.form.comboPolicyGrp.value = jPolicyData.SU_POLICY_GRP_CD;
		doPolicyGrpChange();	
	}
}

//정책세트 combo 동적으로 생성
function doPolicyGrpCombo() {
	var url = "/mg/sugrp.do?mod=getComboPolicyGrpHtml";
	sendRequest(doPolicyGrpComboResult, "", 'POST', url , false, false);
}

function doPolicyGrpComboResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyGrpCombo = document.getElementById("comboPolicyGrp");
	policyGrpCombo.innerHTML = oj.responseText;//jData;
	//document.form.SU_POLICY_GRP_CD.value = document.form.comboPolicyGrp.value; 
}
function doPolicyGrpChange() {
	jPolicyData.SU_POLICY_GRP_CD = document.form.comboPolicyGrp.value;		//정책그룹 코드
	doQuery();
}

function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
}

function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

function doOnCellChange(stage,rowId,cellInd) 
{	
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		} else {
		}
		return true;
	}
	return false;
}

function doQuery() {
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML("<%=contextPath%>/mg/su/policySetMgmt.do?mod=selectPolicySetList&grid_col_id="+grid_col_id+"&SU_POLICY_GRP_CD="+document.form.comboPolicyGrp.value);
	GridObj.clearAll(false);																																							
} 

//doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
//GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
//setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}

function checkLength(obj){
	if(obj==document.getElementById("IS_LOGIN_FAIL_COUNT")){
		if(obj.value.length > 1){
			obj.value=9;
			alert("최대 입력 가능한 값은 9입니다.");	
		}
	}else if(obj==document.getElementById("READCOUNT")){
		if(obj.value.length > 5){
			obj.value=99999;
			alert("최대 입력 가능한 값은 99999입니다.");
		}
	}
}

</script>
<style>
/*  tr, td, div {
 font-family: Tahoma;
 padding:3px;
 } */
 .macInput{
 	width:27px;
 	padding: 0px 2px 0px 2px;
 }
 .macInputTd{
 	background-color:#F1F1F1;
 }
 
 .macTable td{
 	padding:3px 0px 3px 0px;
 }

 .polDiv{
	padding:14px 0px 14px 0px;
	width:1100px;/* 1075 */
	/*border-radius:10px;
	 border:solid 1px gray; */
 }
 .polDiv1{
	width:1071px;
	padding:14px;
	/*border-radius:10px;
	 border:solid 1px gray; */
 }
  .polDiv2{
	padding:12px;
	width:55%;
	border-radius:3px;
	border:solid 1px gray;
	float:left;
 }
  .polDiv3{
	padding:12px;
	width:35%;
	border-radius:3px;
	border:solid 1px gray;
	float:right;
 }
  .polDiv4{
	width:1099px;
	margin-top:42px;
	border-radius:3px;
	border:solid 1px gray;
 }
 .imgOnOff{
 	vertical-align:middle; 
 	padding-bottom:3px;
 }
 .pb5{
 	padding-bottom:5px;
 }
.polTable1{
	width:100%;
}
.rentState{
	background-color:#F1F1F1;
	text-align:center;
}
.polDetailTable{
	width:100%;
}
.polDetailTable td{
	padding:5px;
}
.bgColor{
	background-color:#F1F1F1;
}
.tdBorderBtStyle{
 	border-bottom-color: gray;
 	border-bottom-style: solid;
 	border-bottom-width:1px;
}
.polDetailTable_sub{
	margin: 0px 16px 0px 16px;
	width:95%;
}
.polDetailTable_sub td{
	padding:6px;
}
.txAlignRight
{
	text-align: right;
}
.rentStateTd{
	font-size: 15px;
	font-height:bold;
}
.tbPolicyGrp{
	width:90%;
}
.tbPolicyGrpCombo{
	text-align:right;
}
.w200{
	width:200px;
}
.policyGrpGrid{
	background-color:white;
	width:100%;
	height:200px;
}
/* background :#f4f4f4 url(../images/bul_arrow3.gif) 8px 50% no-repeat */
</style>
</head>
<body onload="init();" style="margin: 0px; padding: 8px; overflow:hidden;">
<form name="form" method="post">
<input type="hidden" name="USB_AUTH_CL_CD" value="">
<input type="hidden" name="NO_SAVE" value="">
<input type="hidden" name="NO_COPY" value="">
<input type="hidden" name="NO_PRINT" value="">
<!-- <input type="hidden" name="IS_EXPIRE_DT" value=""> -->
<input type="hidden" name="IS_EXPIRE_INIT" value="">
<!-- <input type="hidden" name="IS_READCOUNT" value=""> -->
<input type="hidden" name="IS_READ_INIT" value="">
<!-- <input type="hidden" name="IS_WAIT_CLOSE" value=""> -->
<!-- <input type="hidden" name="WAIT_TIME" value=""> -->
<input type="hidden" name="IS_LOGIN_FAIL_INIT" value="">
<!-- <input type="hidden" name="IS_LOGIN_FAIL_COUNT" value=""> -->
<input type="hidden" name="USB_ENCODE_CL_CD" value="">
<input type="hidden" name="USB_IMAGE_SIZE" value="">
<input type="hidden" name="IS_MODIFIED_USER_AUTH" value="0">
<input type="hidden" name="RETURN_YMD" value="">
<input type="hidden" name="USER_AUTH_YN" value="">
<div class="polDiv"> 
	<div class="polDiv1">
		<table class="polTable1">
			<colgroup>
				<col width="10%">
				<col width="10%">
				<col width="70%">
			</colgroup>
			<tr>
				<td><%=mm.getMessage("SUMG_1255", s_user.getLocale())%></td>
				<td class="rentState bgColor"><span class="rentStateTd" id ="RENT_ST_NM"></span></td>
				<td><div class="btnarea">
  						<a href="javascript:lostUSB();" class="btn"><span><%=mm.getMessage("SUMG_1256", s_user.getLocale())%></span></a>
  						<a href="javascript:findUSB();" class="btn"><span><%=mm.getMessage("SUMG_1102", s_user.getLocale())%></span></a>
					</div>
				</td>
			</tr>
		</table>
	</div><!-- polDiv1 -->
	<div class="polDiv2">
		<table class="polTable2">
			<colgroup>
				<col width="30%">
				<col width="30%">
				<col width="30%">
			</colgroup>
			<tr>
				<td class="pb5"><%=mm.getMessage("SUMG_1257", s_user.getLocale())%></td>
				<td><span><%=mm.getMessage("SUMG_1258", s_user.getLocale())%> &nbsp;</span><img src="../../../ext/images/button/ON.gif" id="MOD_STOP_IMG" class="imgOnOff" onClick="javascript:doChangeImg(this.id, this.src);"/><span>&nbsp; <%=mm.getMessage("SUMG_1126", s_user.getLocale())%></span></td>
				<td><input type="checkbox" name="MOD_STOP_DEL" class="imgOnOff" onclick="javascript:chageModDel(this);"> <%=mm.getMessage("SUMG_1260", s_user.getLocale())%></td>
			</tr>
		</table>
	</div><!-- polDiv2 End -->
	<div class="polDiv3">
		<table class="polTable2">
		<colgroup>
			<col width="20%">
			<col width="20%">
		</colgroup>
		<tr>
			<td class="pb5"><%=mm.getMessage("SUMG_1259", s_user.getLocale())%></td>
			<td><%=mm.getMessage("SUMG_1261", s_user.getLocale())%> &nbsp;<img src="../../../ext/images/button/OFF.gif" id="IS_APPROVAL_IMG" class="imgOnOff" onClick="javascript:doChangeImg(this.id, this.src);"/>&nbsp; <%=mm.getMessage("SUMG_1262", s_user.getLocale())%></td>
		</tr>
		</table>
	</div><!-- polDiv3 End -->
</div>

<div class="polDiv"> 
	<div class="polDiv4">
		<table class="polDetailTable">
			<tr>
				<td colspan="6" class="tdBorderBtStyle bgColor"><%=mm.getMessage("SUMG_1263", s_user.getLocale())%></td>
			</tr>
			<tr>
				<td>
					<table class="polDetailTable_sub">
						<colgroup>
							<col width="10%">
							<col width="15%">
							<col width="23%">
							<col width="10%">
							<col width="12%">
							<col width="15%">													
						</colgroup>
						<tr>
							<td class="tdBorderBtStyle txAlignRight"><%=mm.getMessage("SUMG_1220", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle"><input id="NO_SAVE1" type="radio"  name="NO_SAVE" value="1" checked><%=mm.getMessage("SUMG_1264", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle"><!-- <input id="NO_SAVE1" type="radio"  name="NO_SAVE" value="2" >아이디/패스워드 인증 --></td>
							<td class="tdBorderBtStyle" colspan="3">
								<div class="btnarea">
			  						<a href="javascript:doPwReset();" class="btn" style="padding-right: 13px;"><span><%=mm.getMessage("SUMG_1265", s_user.getLocale())%></span></a>
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><%=mm.getMessage("SUMG_1266", s_user.getLocale())%></td>
							<td><input type="radio" id="IS_LOGIN_FAIL_YN_N" name="IS_LOGIN_FAIL_YN" value="N" onclick="javascript:doChangeRadio(this.name);"><%=mm.getMessage("SUMG_1267", s_user.getLocale())%></td>
							<td colspan="3"></td>
						</tr>
						<tr>
							<td class="tdBorderBtStyle"></td>
							<td class="tdBorderBtStyle"></td>
							<td class="tdBorderBtStyle">
								<input type="radio" id="IS_LOGIN_FAIL_YN_Y" name="IS_LOGIN_FAIL_YN"  value="Y" onclick="javascript:doChangeRadio(this.name);">	<!--제한 있음-->	
								<input type="text" id="IS_LOGIN_FAIL_COUNT" name="IS_LOGIN_FAIL_COUNT" size=5 value="" onchange="checkLength(this);"> 회</td>
							<td class="tdBorderBtStyle txAlignRight"><%=mm.getMessage("SUMG_1268", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle"><jsp:include page="/mg/common/comboCode.jsp" >
									<jsp:param name="codeId" value="A0510"/>
									<jsp:param name="tagname" value="FAIL_USB_USE_CD"/>
								</jsp:include></td>
							<td class="tdBorderBtStyle"></td>	
						</tr>
						<tr>
							<td class="tdBorderBtStyle txAlignRight"><%=mm.getMessage("SUMG_1269", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle">
								<input type="radio" id="IS_READCOUNT_N" name="IS_READCOUNT" value="N" onclick="javascript:doChangeRadio(this.name);">
								<label><%=mm.getMessage("SUMG_1267", s_user.getLocale())%></label></td>				
							<td class="tdBorderBtStyle">
								<input type="radio" id="IS_READCOUNT_Y" name="IS_READCOUNT" value="Y" onclick="javascript:doChangeRadio(this.name);">
								<input type="text" name="READCOUNT" id="READCOUNT" size=9 value="" onchange="checkLength(this);">&nbsp;<%=mm.getMessage("SUMG_1228", s_user.getLocale())%>
								<span id="USECOUNT" style="text-color:red;"></span>
								</td>
							<td class="tdBorderBtStyle txAlignRight"><%=mm.getMessage("SUMG_1270", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle"><jsp:include page="/mg/common/comboCode.jsp" >
									<jsp:param name="codeId" value="A0510"/>
									<jsp:param name="tagname" value="READ_USB_USE_CD"/>
								</jsp:include></td>
							<td class="tdBorderBtStyle"></td>			
						</tr>
						<tr>
							<td class="tdBorderBtStyle txAlignRight"><%=mm.getMessage("SUMG_1271", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle">
								<input id="IS_EXPIRE_DT_N" type="radio" name="IS_EXPIRE_DT" value="N" onclick="javascript:doChangeRadio(this.name);">
								<label><%=mm.getMessage("SUMG_1267", s_user.getLocale())%></label></td>				
							<td class="tdBorderBtStyle">
								<input id="IS_EXPIRE_DT_Y" type="radio"  name="IS_EXPIRE_DT" value="Y" onclick="javascript:doChangeRadio(this.name);">
								<input type="text" name="EXPIRE_YMD" id="EXPIRE_YMD" size=9 value="">
								<img id='calendar_icon' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"> </td>
							<td class="tdBorderBtStyle txAlignRight"><%=mm.getMessage("SUMG_1272", s_user.getLocale())%></td>
							<td class="tdBorderBtStyle"><jsp:include page="/mg/common/comboCode.jsp" >
									<jsp:param name="codeId" value="A0510"/>
									<jsp:param name="tagname" value="EXPIRE_USB_USE_CD"/>
								</jsp:include>	</td>
							<td class="tdBorderBtStyle"></td>			
						</tr>
						<tr>
							<td class="txAlignRight"><%=mm.getMessage("SUMG_1273", s_user.getLocale())%></td>
							<td colspan="2">
								<input type="checkbox" name="IS_WAIT_CLOSE" onclick="javascript:doChangeCheckBox(this);" checked><label><%=mm.getMessage("SUMG_1274", s_user.getLocale())%></label>
							</td>
							<td class="txAlignRight">
								<label><%=mm.getMessage("SUMG_1275", s_user.getLocale())%></label>
							</td>
							<td><input type="text" size=5 id="WAIT_TIME" value="">분</td>
							<td></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
	</div>
</div>



<div class="polDiv"> 
	<div class="polDiv4" style="margin-top: -8px;">
		<table class="polDetailTable">
			<tr>
				<td colspan="6" class="tdBorderBtStyle bgColor"><%=mm.getMessage("SUMG_1276", s_user.getLocale())%></td>
			</tr>
			<tr>
				<td>
					<table style="width:95%;">
						<colgroup>
							<col width="15%">
							<col width="15%">
							<col width="35%">
							<col width="25%">
							<col width="10%">
						</colgroup>
						<tr>
							<td><input type="checkbox" name="MAC_USE_YN" onclick="javascript:doChangeCheckBox(this);" checked/> <%=mm.getMessage("SUMG_1277", s_user.getLocale())%> </td>
							<td><%=mm.getMessage("SUMG_1278", s_user.getLocale())%></td>
							<td>
								<table>
									<tr>
										<td class="macInputTd"><input type="text" name="subMAC_1" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_2, 2)"/></td>
										<td class="macInputTd"><input type="text" name="subMAC_2" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_3, 2)"/></td>
										<td class="macInputTd"><input type="text" name="subMAC_3" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_4, 2)"/></td>
										<td class="macInputTd"><input type="text" name="subMAC_4" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_5, 2)"/></td>
										<td class="macInputTd"><input type="text" name="subMAC_5" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_6, 2)"/></td>
										<td class="macInputTd"><input type="text" name="subMAC_6" class="macInput"  onkeypress="return onkeylengthMax(this, form.addMacBtn, 2)"/></td>
										<td>
											<div class="btnarea">
								           		<a href="javascript:macAdd();" class="btn" name="addMacBtn" id="addMacBtn"><span>추가</span></a>								 
								    		</div>
										</td>
									</tr>
								</table>
							</td>
							<td id="MAC_TD" rowspan="4">
								<input type="hidden" name="MAC_ADDR" value="">
								<select name="MAC_LIST" id="MAC_LIST" multiple>
									<option value=''>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</option>
								</select>
							</td>
							<td>
								<div class="btnarea">
					           		<a href="javascript:macDel();" class="btn"><span><%=mm.getMessage("SUMG_1279", s_user.getLocale())%></span></a>
					        	</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
	</div>
</div>

<div class="polDiv"> 
	<div class="polDiv4" style="margin-top: -8px;">
		<table class="polDetailTable">
			<tr><td class="tdBorderBtStyle bgColor"><%=mm.getMessage("SUMG_1280", s_user.getLocale())%></td></tr>
			<tr>
				<td>
					<table class="tbPolicyGrp">
						<tr>
							<td><%=mm.getMessage("SUMG_1281", s_user.getLocale())%></td>
							<td class="tbPolicyGrpCombo"><select id="comboPolicyGrp" class="w200" onchange="doPolicyGrpChange();"><option></option></select></td>
							
						</tr>
						<tr>
							<td colspan="2"><div id="gridbox" name="gridbox" class="policyGrpGrid"></div></td>
						</tr>
					</table>	
				</td>
			</tr>
			<tr>
				<td>
					<%-- <div class="btnarea">
				    	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
					</div> --%>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="polDiv"> 
	<div class="btnarea5">
		<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("SUMG_1172", s_user.getLocale())%> <%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	</div>
</div>
</form>
</body>
</html>