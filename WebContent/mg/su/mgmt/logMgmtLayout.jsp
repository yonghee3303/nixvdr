<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>

<html>
<head>

<title>Insert title here</title>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;

var cell;

function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "1C",
	    skin: "dhx_web"
	});

	cell = layout.cells('a');
	//cell.setText("보안USB 로그관리"); //캡션에 버튼을 추가하기 위해 아래와 같은 방법으로 지정하였다.
	cell.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'> "+leftMenuName+"</td>"
			+"<td id='goMain'><a onclick='goMain();'>"
			+"<img src='../../../ext/images/button/home_up.png' onMouseOver=\"this.src='../../../ext/images/button/home_down.gif'\"onMouseOut=\"this.src='../../../ext/images/button/home_up.png'\" />"
			+"</a></td></tr></table>");
	//cell1.setWidth(1500);
	//cell1.setHeight(125);
	//a.hideHeader();
	//a.setHeight(120);
	cell.attachURL("logMgmt.jsp", null,  {
		formid: "SU_0103",
		gridid: "GridObj"
	});

}

//캡션에 집 모양을 눌렀을 때 발생하는 이벤트
function goMain(){
	
	parent.mainFrame.location.href="../main/main.jsp";
}

function resize() {
	layout.setSizes();
}


</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        overflow: hidden;
    }
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
    
</style>

</head>

<body onload="init();" onresize="resize();" style="overflow:hidden;">

<div id="layoutObj" style="position: relative;"></div>

</body>
</html>