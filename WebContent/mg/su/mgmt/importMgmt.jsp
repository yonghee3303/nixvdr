<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String flag = (String)request.getParameter("flag");
String contextPath = request.getContextPath();
String usbId = request.getParameter("usbId");
%>

<html>
<head>

<title>보유 보안USB 관리</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>

<script>

/* var layout;
var cell1;
var cell2; */
var calendar1;
var calendar2;
var flag ="<%=flag%>";
var GridObj 		= {};
var G_SERVLETURL = "<%=contextPath%>/mg/importMgmt.do";
var clickFlag;

function init() {
	//달력을 한글로 표시하기 위해서 이러한 데이터들이 필요한다.
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	//달력을 선택했을 때 그 날짜가 들어갈 input 태그의 아이디와 누르면 달력이 표시될 button의 아이디를 지정해준다.
	calendar1 = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2 = new dhtmlXCalendarObject({input:"calendar_input2", button:"calendar_icon2"});
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	
	calendar1.hideTime();
	calendar2.hideTime();
	
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onXLE", doCheckListCount);
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);

	document.getElementById("calendar_input").value="<%=fromDay%>";
	document.getElementById("calendar_input2").value="<%=serverDay%>";
	
	if("<%=usbId%>"!=""){
		document.getElementById("selectItem").value="USB_ID";
		document.getElementById("term").value="<%=usbId%>";
		document.getElementById("calendar_input").value="";
		document.getElementById("calendar_input2").value="";
	}
	
	//flag별 초기 쿼리
	switch(flag){
		
		case "1":
			//보유+대여+분실+폐기 목록
			doAllQuery();
		break;
		case "2":
			//분실 목록
			doLostQuery();
		break;
			
		default :
			//보유상태 목록
			doQuery();		
		break;
	}
	
}

function doOnRowSelected(rowId, cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	
		var usb_id = GridObj.cells(rowId, GridObj.getColIndexById("USB_ID")).getValue();
		var state = GridObj.cells(rowId, GridObj.getColIndexById("RENT_STS_CD")).getValue();

		if(flag=="2"){
			alert("현재 보유 중인 USB 목록이 아니기 때문에 고장 처리를 하실 수 없습니다.");
			return;
		}else{	
			if(state =="01" || state =="02"){
				clickFlag=0;
				if(confirm(usb_id+"를 고장 처리 하시겠습니까?")){ 
					updateUsbStatus(usb_id,"malfunc");
				}else{
					return;
				}
			}/* else if(state =="05"){
				if(confirm(usb_id+"를 분실 해제 처리 하시겠습니까?")==false){ 
					clickFlag=1;
					return;
				}else{
					clickFlag=0;
					updateUsbStatus(usb_id,"find");//alert("분실해제 처리");
				}
			} */
		}
}
//분실/분실해제
function updateUsbStatus(usb, sts){
	if(sts == "lost" || sts == "find" || sts == "malfunc"){
		GridObj.clearAll(false);
		var grid_col_id  = "<%=grid_col_id%>";
		GridObj.loadXML(G_SERVLETURL +"?mod=updateUsbStatus&USB_ID="+usb+"&STATE="+sts+"&grid_col_id="+grid_col_id);		
	}
}

function doLostQuery(){
	flag="1";
	clickFlag=1;
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectLostList&grid_col_id="+grid_col_id);
	GridObj.clearAll(false);
}

function doCheckListCount(grid_obj,count){
	var rowsCount = grid_obj.getRowsNum(); //총 갯수
	document.getElementById("listCount").innerHTML = "<b>"+rowsCount+"</b>";
	
/* 	if(clickFlag ==0){
		document.getElementById("clickLost").innerText="분실 목록";
	}else{
		document.getElementById("clickLost").innerText="보유 목록";
	} */
	
	var conditionCell;
	
	for(var i=0; i<rowsCount; i++){
		conditionCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("RENT_STS_NM"));//상태
		
		//상태 cell의 값이 분실일 경우 글자 색을 빨강색으로 바꾼다.			
		if(conditionCell.getValue() =="<%=mm.getMessage("SUMG_1101", s_user.getLocale())%>"){
			conditionCell.setTextColor('red');
		}
	}
	
}
//보유+대여+분실+폐기 목록
function doAllQuery() {
	flag="2";
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectAllMgmt&grid_col_id="+grid_col_id);
	GridObj.clearAll(false);
}



//입력받은 검색 조건들을 url로 만들어 action에 넘겨준다.
function doQuery(){
	flag="1";
	clickFlag=0;
	var fromDate = document.getElementById("calendar_input").value;
	var toDate = document.getElementById("calendar_input2").value;
	var searchTerm = document.getElementById("selectItem").value;
	var term = document.getElementById("term").value;
	
	var argu= "&FROM_YMD="+encodeURIComponent(fromDate);
	argu += "&TO_YMD="+encodeURIComponent(toDate);
	
	
	switch(searchTerm){
	case "USER_NM":
		argu +="&USER_NM="+ encodeURIComponent(term);
		break;
	case "DEPT_NM":
		argu +="&DEPT_NM="+ encodeURIComponent(term);
		break;
	case "POSITION_NM":
		argu +="&POSITION_NM="+ encodeURIComponent(term);
		break;
	case "USB_ID":
		argu +="&USB_ID="+ encodeURIComponent(term);
		break;
	case "USB_MODEL_NM":
		argu +="&USB_MODEL_NM="+ encodeURIComponent(term);
		break;
	case "USB_CAPACITY":
		argu +="&USB_CAPACITY="+ encodeURIComponent(term);
		break;
	}
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectImportMgmt&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}


function resize() {
	GridObj.setSizes();
	
}
function initSearch(){ //초기화 버튼을 눌렀을 때 값들을 다 초기화 시켜준다.
	document.getElementById("calendar_input").value="";
	document.getElementById("calendar_input2").value="";
	document.getElementById("term").value="";
	doQuery();
}
function clickListButton(){
	if(clickFlag==0){
		doLostQuery();
	}else if(clickFlag==1){
		doQuery();
	}
}

function maekPolicy(){
	centerWin("/mg/su/mgmt/TreePopup.jsp",800, 550,"no"); //반출 신청 팝업.	
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}


</script>

<style>

div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}

    
    
</style>

</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:5px; overflow: hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
       <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px"/> <!-- 검색기간 -->
            <col width="200px"/> <!-- 텍스트 필드 -->
            <col width="80px"/> <!-- 검색조건 -->
            <col width="100px"/>  <!-- 셀렉트박스  -->
            <col width="150px"/> <!-- 검색텍스트필드 -->
            <col /> <!-- 띄어... -->
            <col width="180px" style="padding-right:16px;"/><!-- 버튼 -->
        </colgroup>
        <tr>
        	<td class="tit">
        		<%=mm.getMessage("COMG_1046", s_user.getLocale())%>
        	</td>
            <td>
            	<input class="text" type='text' id='calendar_input' value='' size="8"> <img id='calendar_icon' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"> 
            ~
				<input class="text" type='text' id='calendar_input2' value='' size="8"> <img id='calendar_icon2' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand">
            </td>
            <td class="tit">
            	<%=mm.getMessage("COMG_1064", s_user.getLocale())%>
            </td>
            <td>
				<select id="selectItem">
								<option value="USER_NM"><%=mm.getMessage("SUMG_1179", s_user.getLocale())%>
								<option value="DEPT_NM"><%=mm.getMessage("COMG_1063", s_user.getLocale())%>
								<option value="POSITION_NM"><%=mm.getMessage("COMG_1062", s_user.getLocale())%>
								<option value="USB_ID"><%=mm.getMessage("SUMG_1176", s_user.getLocale())%>
								<option value="USB_MODEL_NM"><%=mm.getMessage("SUMG_1155", s_user.getLocale())%>
								<option value="USB_CAPACITY"><%=mm.getMessage("SUMG_1156", s_user.getLocale())%>
				</select>
            </td>
        	<td>
        		<input class="text" type='text' id='term' value='' onkeypress="if(event.keyCode==13) {searchImport();}">
        	</td>
        	<td></td>
			<td><div class="btnarea">
	            	<a href="javascript:initSearch();" class="btn"><span><%=mm.getMessage("COMG_1065", s_user.getLocale())%></span></a>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
	            </div>
	    	</td>
        </tr>
		</table>

		<!-- 임시 공간 -->
		<table width="100%" border="0">
       		<tr>
				<td>
				 총 : <font color="red" id="listCount"></font> 건
				</td>
				<td>
					<!-- <div class="btnarea4" style="padding-right:16px;">
						<a href="javascript:maekPolicy();" class="btn"><sapn>보안USB 대여</sapn></a>
	            		<a href="javascript:clickListButton();" class="btn"><span id='clickLost'>분실 목록</span></a>
	            	</div> -->
				</td>
			</tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" style="width:100%; height:90%; background-color:white; overflow:hidden;"></div>


</body>
</html>