<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String flag = (String)request.getParameter("flag");

IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName;
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	IMenu menu = mnu.getMenu("SU_0101", s_user);
	leftMenuName = menu.getName(s_user.getLocale());
}

String usbId = request.getParameter("USB_ID");
if(usbId == null) usbId="";
%>

<html>
<head>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;
var cell;

function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "1C",
	    skin: "dhx_web"
	});
	
	cell = layout.cells('a');
	//cell.setText("<span id='caption'> 보유 보안USB 관리</span><span id='goMain'><a onclick='goMain()'>Go Main</a></span>");

	cell.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'> "+leftMenuName+"</td>"
			+"<td id='goMain'><a onclick='goMain();'>"
			+"<img src='../../../ext/images/button/home_up.png' onMouseOver=\"this.src='../../../ext/images/button/home_down.gif'\"onMouseOut=\"this.src='../../../ext/images/button/home_up.png'\" />"
			+"</a></td></tr></table>");
	
	/* 
	cell.setText("<table><tr style='width:90%'>"+
			"<td style='width:80%' id='caption'>보유 보안USB 관리</td>"+
			"<td style='text-alig: right' id='goMain'><a onclick='goMain();'>Go Main</a></td>"+
			"</tr></table>"); 
	*/
	
	//cell1.setWidth(1500);
	//cell1.setHeight(125);
	//a.hideHeader();
	//a.setHeight(120);
	cell.attachURL("importMgmt.jsp", null,  {
			formid: "SU_0101",
			gridid: "GridObj",
			flag:"<%=flag%>",
			usbId:"<%=usbId%>"
	});

}

function goMain(){
	
	parent.mainFrame.location.href="../main/main.jsp";
}


function resize() {
	layout.setSizes();
}


</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        overflow: hidden;
    }
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
</style>

</head>

<body onload="init();" onresize="resize();">

<div id="layoutObj" style="position: relative;"></div>

</body>
</html>