<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IUser s_user = (IUser) session.getAttribute("j_user");
%>
<html>
<head>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<title>[정책세트명]설정 정보</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<script>

</script>
<style>
	.centerTD{
	
		border-spacing: 35px;
	
	}
	
	.centerTD td{
		padding: 5px;
	}
</style>
</head>
<body style="height: 100%;">

	<table class="centerTD" style="float: left; border-right-style: 1px sold black;">
		<tr>
			<td><p>사용 제한</p>
		</tr>
		<tr>
			<td>기타 설정</td>
		</tr>
		<tr>
			<td>정책 적용세트</td>
		</tr>
	</table>

	<table>
		<colgroup>
			<col width="5%">
		</colgroup>
		<tr>
			<td/>
			<td>				<!--사용가능 횟수-->
				<%=mm.getMessage("SUMG_1221", s_user.getLocale())%>
			</td>
			<td>
				<input type="radio" id="">
			</td>
		</tr>
	</table>

</body>
</html>