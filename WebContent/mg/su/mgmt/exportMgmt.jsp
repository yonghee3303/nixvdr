<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String userIp = request.getRemoteAddr();
String flag = (String)request.getParameter("flag");
String contextPath = request.getContextPath();
String usbId = request.getParameter("usbId");
%>

<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<title>반출 보안USB 관리</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>
<script>

var calendar1;
var calendar2;
var usbId="";
var rentId="";
var tabbar;
var GridObj 		= {};
var G_SERVLETURL = "<%=contextPath%>/mg/exportMgmt.do";
var myDataProcessor;
var clickFlag=0;
function init() {
	
	GridObj = setGridDraw(GridObj);

	GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
		usbId = GridObj.cells(rId, GridObj.getColIndexById("USB_ID")).getValue(); 
		rentId = GridObj.cells(rId, GridObj.getColIndexById("USB_CREATE_ID")).getValue(); 
		doAjax_Detail(usbId, rentId); //그리드 아래 있는 폼으로 상세 정보를 보여준다.
		tabbar.tabs('apply').getFrame().contentWindow.doAjax_Policy(usbId, rentId);//그리드 목록 중 더블클릭한 목록의 정책 정보를 보여준다.
		tabbar.tabs('log').getFrame().contentWindow.doAjax_Log(usbId, rentId);//그리드 목록 중 더블클릭한 목록의 로그 정보를 보여준다.
		tabbar.tabs('message').getFrame().contentWindow.doAjax_Message(usbId, rentId);
		//window.open("../info/usbDetailInfo.jsp?usbId="+usbId,'','width=1024px,height=924px,scrollbars=no,resizable=no');
	});

	//한글로 달력에 표시하기 위해서는 아래와 같은 데이터들이 필요로한다.
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar1 = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2 = new dhtmlXCalendarObject({input:"calendar_input2", button:"calendar_icon2"});
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	
	calendar1.hideTime();
	calendar2.hideTime();
	
	/* tabbar = new dhtmlXTabBar("tabbar"); */
	tabbar = new dhtmlXTabBar({
    parent:         "tabbar",    // id/object, parent container where tabbar will be located
    skin:           "dhx_web"
    
	});
	
	
	tabbar.addTab("apply","<%=mm.getMessage("SUMG_1172", s_user.getLocale())%>",null,null,true);
	tabbar.addTab("log","<%=mm.getMessage("SUMG_1173", s_user.getLocale())%>");
	tabbar.addTab("message","<%=mm.getMessage("SUMG_1251", s_user.getLocale())%>");
	
	
	tabbar.tabs('apply').attachURL("<%=contextPath%>/mg/su/mgmt/applyPolicy.jsp", null, {
		formid: "CP_051703",
		gridid: "GridObj"
	});
	tabbar.tabs('log').attachURL("<%=contextPath%>/mg/su/mgmt/log.jsp", null, {
		formid: "SU_0103",
		gridid: "GridObj"
	});
	tabbar.tabs('message').attachURL("<%=contextPath%>/mg/su/mgmt/message.jsp", null, {}); //url에 같이 포함되어야 할 내용이다. 
	
	
	
	//자동 리사이즈~
	tabbar.enableAutoReSize(); 
	document.getElementById("calendar_input").value="<%=fromDay%>";
	document.getElementById("calendar_input2").value="<%=serverDay%>";
	
	if("<%=usbId%>"!=""){
		document.getElementById("selectItem").value="USB_ID";
		document.getElementById("term").value="<%=usbId%>";
		document.getElementById("calendar_input").value="";
		document.getElementById("calendar_input2").value="";
	}
	//flag별 초기 쿼리
	switch("<%=flag%>"){
		
		case "1":
			//반납일 초과
			doReturnDtQuery();
			break;
		
		case "2":
			//분실항목만 보여줌
			doLostQuery();
			break;
			
		case "3":
			//사용금지
			doUseBanQuery();
			break;
			
		case "4":
			//폐기 목록
			doMalQuery();
			break;
			
		case "5":
			//대여 목록
			doExportQuery();
			break;
			
		case "6":
			//만료일 초과
			doExpireDtQuery();
			break;
			
		default :
			//대여+분실 목록
			doQuery();
			break;
	}
	
	GridObj.attachEvent("onXLE", doCheckedCellValue);//onXLE는 모든 항목들이 로드 된 이후에 실행되는 이벤트로 이벤트 없이 메소드 자체만을 사용하면 로드 되기 전에 실행되어 해당 이벤트를 적용하지 못한다.
	
}

function doOnRowSelected(rowId,cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	/* var USB_ID = GridObj.cells(rowId, GridObj.getColIndexById("USB_ID")).getValue();
	var USB_CREATE_ID = GridObj.cells(rowId, GridObj.getColIndexById("USB_CREATE_ID")).getValue();
	parent.refresh(USB_ID,USB_CREATE_ID); */
}
function doOnRowSelect(rowId,cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	var USB_ID = GridObj.cells(rowId, GridObj.getColIndexById("USB_ID")).getValue();
	var USB_CREATE_ID = GridObj.cells(rowId, GridObj.getColIndexById("USB_CREATE_ID")).getValue();
	parent.refresh(USB_ID,USB_CREATE_ID);
}


//기한이 지났거나 분실상태일 때 글자 색상을 변경해주는 이벤트. 단, 컬럼의 위치가 바뀌면 이벤트가 적용되지 않는다. 
function doCheckedCellValue(grid_obj,count){
	var rowsCount = GridObj.getRowsNum(); //총 갯수
	document.getElementById("listCount").innerHTML = "<b>"+rowsCount+"</b>";
	
/* 	if(clickFlag ==0){
		document.getElementById("clickLost").innerText="분실 목록";
	}else{
		document.getElementById("clickLost").innerText="반출 목록";
	}
	 */
	var expireDtCell;
	var returnDtCell;
	var rentDtCell;
	var conditionCell;
	var readCountCell;
	var useCountCell;
	var temp;
	var todayArr = "<%=serverDay%>".split("-");
	var today = todayArr[0].concat(todayArr[1]).concat(todayArr[2]); //문자열을 숫자형태로 크고 작음을 비교하기 위해 이러한 형태로 다시 만들어주었다. 
	
	for(var i=0; i<rowsCount; i++){
		expireDtCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("EXPIRE_YMD"));//사용만료일 
		returnDtCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("RETURN_YMD"));//반납예정일
		rentDtCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("RENT_YMD"));//반납예정일
		conditionCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("RENT_STS_NM"));//상태
		readCountCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("READCOUNT"));//지정횟수
		useCountCell = grid_obj.cells(grid_obj.getRowId(i), GridObj.getColIndexById("USECOUNT"));//사용횟수
		
		if(readCountCell.getValue() !="" && parseInt(readCountCell.getValue()) < parseInt(useCountCell.getValue())){
			useCountCell.setTextColor('red');
		}
		//상태 cell의 값이 분실일 경우 글자 색을 빨강색으로 바꾼다.			
		if(conditionCell.getValue() =="<%=mm.getMessage("SUMG_1101", s_user.getLocale())%>"){
			conditionCell.setTextColor('red');
		}
		
		var expireArr =expireDtCell.getValue().split("-");
		temp = expireArr[0].concat(expireArr[1]).concat(expireArr[2]);

		if(temp < today){ //만료일이 지났을 경우 글자 색을 빨간색으로 바꾼다.
			expireDtCell.setTextColor('red');
		}
		
		var returnArr = returnDtCell.getValue().split("-");
		temp = returnArr[0].concat(returnArr[1]).concat(returnArr[2]);
		var rentArr = rentDtCell.getValue().split("-");
		var rent = rentArr[0].concat(rentArr[1]).concat(rentArr[2]);
		
		if(temp!= rent && temp < today){ //반납일이 지났을 경우 글자 색을 빨간색으로 바꾼다.
			returnDtCell.setTextColor('red');
		}
	}
	if(usbId!=""){
		tabbar.tabs('apply').getFrame().contentWindow.doAjax_Policy(usbId, rentId);//그리드 목록 중 더블클릭한 목록의 정책 정보를 보여준다.
		tabbar.tabs('log').getFrame().contentWindow.doAjax_Log(usbId, rentId);
		tabbar.tabs('message').getFrame().contentWindow.doAjax_Message(usbId, rentId);
	}
}

//보유 목록을 갖고 오는 action
function doExportQuery() {
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectOnlyExportList&grid_col_id="+grid_col_id);
	GridObj.clearAll(false);	
}

//폐기 목록을 갖고 오는 action
function doMalQuery(){
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectMalList&grid_col_id="+grid_col_id);
	GridObj.clearAll(false);
}
//사용 금지 목록을 갖고 오는 action
function doUseBanQuery(){
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUseBanList&grid_col_id="+grid_col_id);
}
//반납일이 지난 목록을 갖고오는 action 
function doReturnDtQuery(){
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectReturnDtList&grid_col_id="+grid_col_id);
}
//만료일이 지난 목록을 갖고오는 action 
function doExpireDtQuery(){
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectExpireDtList&grid_col_id="+grid_col_id);
}
//분실 상태인 목록을 갖고오는 action 
function doLostQuery() {
	clickFlag=1;
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectLostList&grid_col_id="+grid_col_id);
}
function grid_resize() {
	GridObj.setSizes();
}
//수불 상태를 분실로 바꾸기 위한 action 
function updateUsbStatus(usb, rent, sts){
	if(usb=="" && rent==""){
		alert("<%=mm.getMessage("SUMG_2018", s_user.getLocale())%>");
	}else{
		GridObj.clearAll(false);
		var grid_col_id  = "<%=grid_col_id%>";
		GridObj.loadXML(G_SERVLETURL +"?mod=updateUsbStatus&USB_ID="+usb+"&USB_CREATE_ID="+rent+"&RENT_STS_CD="+sts+"&grid_col_id="+grid_col_id);	
	}
}
//applyPolicy페이지의 applyPolicy()에서 업데이트에 필요한 값들을 url 변수에 담아 넘겨준다.
function updatePolicy(url){
	
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	var user ="<%= s_user.getId().toString()%>";
	GridObj.loadXML(G_SERVLETURL +"?mod=updatePolicy"+"&grid_col_id="+grid_col_id+url+"&USER_ID="+user+"&IP_ADDR="+"<%=userIp%>");		
}

//상세 정보를 갖고 오기 위한.. 더블 클릭을 했을 때 usbId와 createId를 받아 다음 action을 취한다. 
function doAjax_Detail(usb, rent) {
	
	document.getElementById("USB_ID").value = usb;
	var arg = "&USB_ID="+ encodeURIComponent(usb);
	arg+= "&USB_CREATE_ID="+encodeURIComponent(rent);
	var url = G_SERVLETURL + "?mod=selectDetailInfo"+arg; 
	sendRequest(doAjaxEnd_Detail, " " ,"POST", url , false, false); //json형태의 응답을 받아 처리하기 위해... 
}

function doAjaxEnd_Detail(oj) { //action 이후에 받아온 값들을 화면에 뿌려준다.
	var jData = JSON.parse(oj.responseText);
	document.getElementById("MODEL").value = jData.USB_MODEL;
	document.getElementById("CAPACITY").value = jData.USB_CAPACITY;
	
	document.getElementById("REG_DT").value = jData.REG_DT;
	document.getElementById("REG_USER_NM").value = jData.REG_USER_NM;
	document.getElementById("REG_DEPT_NM").value = jData.REG_DEPT_NM;
	document.getElementById("REG_POSITION_NM").value = jData.REG_POSITION_NM;
	
	document.getElementById("RENT_YMD").value = jData.RENT_YMD;
	document.getElementById("RENT_USER_NM").value = jData.RENT_USER_NM;
	document.getElementById("RENT_DEPT_NM").value = jData.RENT_DEPT_NM;
	document.getElementById("RENT_POSITION_NM").value = jData.RENT_POSITION_NM;
	document.getElementById("RENT_STS_NM").value = jData.RENT_STS_NM;
	
}
<%-- //초기화면에 반출된 USB 목록들을 얻어와 보여준다. 
function doQuery() {
	clickFlag=0;
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectExportMgmt&grid_col_id="+grid_col_id);																						
	
} --%>

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}
//검색을 위해 조건들을 url로 만들어준다. 
function doQuery(){
	clickFlag=0;
	//GridObj.clearAll(false);
	
	var fromDate = document.getElementById("calendar_input").value;
	var toDate = document.getElementById("calendar_input2").value;
	var searchTerm = document.getElementById("selectItem").value;
	var term = document.getElementById("term").value;
	
	var argu= "&FROM_YMD="+encodeURIComponent(fromDate);
	argu += "&TO_YMD="+encodeURIComponent(toDate);
	
	//select List로 되어 있어 어떤 것이 선택했는지에 따라 검색조건을 바꿔주어야 할 필요가 있어 switch문을 사용했다. 
	switch(searchTerm){
	case "USER_NM":
		argu +="&USER_NM="+ encodeURIComponent(term);
		break;
	case "DEPT_NM":
		argu +="&DEPT_NM="+ encodeURIComponent(term);
		break;
	case "POSITION_NM":
		argu +="&POSITION_NM="+ encodeURIComponent(term);
		break;
	case "USB_ID":
		argu +="&USB_ID="+ encodeURIComponent(term);
		break;
	case "USB_NM":
		argu +="&USB_NM="+ encodeURIComponent(term);
		break;
	case "RENT_STS_NM":
		argu +="&RENT_STS_NM="+ encodeURIComponent(term);
		break;	
	}
		
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectExportMgmt&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}
function initSearch(){ //초기화를 누르면 기존에 입력되어 있던 값들을 모두 없애준다. 
	document.getElementById("calendar_input").value="";
	document.getElementById("calendar_input2").value="";
	document.getElementById("term").value="";
	doQuery();
}
function clickListButton(){
	if(clickFlag==0){
		doLostQuery();
	}else if(clickFlag==1){
		doQuery();
	}
}

function maekPolicy(){
	
	centerWin("/mg/su/mgmt/TreePopup1.jsp",800, 600,"no"); //대기목록에서 보안USB 대여.
	
}

function centerWin(url, w, h, scroall){
	
		var winL = (screen.width-100-w)/2; 
		var winT = (screen.height-100-h)/2; 
		var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
		window.open(url,'',winOpt);
	}




</script>

<style>

	div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
	
	/* Tabbar Web스킨 사용시 간격조정 */
	.dhxtabbar_base_dhx_web div.dhx_cell_tabbar div.dhx_cell_cont_tabbar {
		
		padding: 0px;
		
	}

</style>


</head>
<body onload="init();" style="margin: 0px; padding:5px; overflow:auto;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
       <table class="board-search" style="table-layout:fixed">
        <colgroup>
  		    <col width="80px"/> <!-- 검색기간 -->
            <col width="200px"/> <!-- 텍스트 필드 -->
            <col width="80px"/> <!-- 검색조건 -->
            <col width="100px"/>  <!-- 셀렉트박스  -->
            <col width="150px"/> <!-- 검색텍스트필드 -->
            <col /> <!-- 띄어... -->
            <col width="180px" style="padding-right:16px;"/><!-- 버튼 -->
            <!-- <col width="8%"/> 검색기간
            <col width="20%"/> 텍스트 필드
            <col width="8%"/> 검색조건
            <col width="8%"/>  셀렉트박스 
            <col width="15%"/> 검색텍스트필드
            <col width="41%"/> 띄어... --> 
        </colgroup>
        <tr>
        	<td class="tit">
        		<%=mm.getMessage("COMG_1046", s_user.getLocale())%>
        	</td>
            <td>
            	<input class="text" type='text' id='calendar_input' value='' size="8"> <img id='calendar_icon' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"> 
            ~
				<input class="text" type='text' id='calendar_input2' value='' size="8"> <img id='calendar_icon2' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand">
            </td>
            <td class="tit">
            	<%=mm.getMessage("COMG_1064", s_user.getLocale())%>
            </td>
            <td>
				<select id='selectItem'>
					<option value='USER_NM'><%=mm.getMessage("SUMG_1137", s_user.getLocale())%>
					<option value='DEPT_NM'><%=mm.getMessage("COMG_1063", s_user.getLocale())%>
					<option value='POSITION_NM'><%=mm.getMessage("COMG_1062", s_user.getLocale())%>
					<option value='USB_ID'><%=mm.getMessage("SUMG_1176", s_user.getLocale())%>
					<option value='USB_NM'><%=mm.getMessage("SUMG_1110", s_user.getLocale())%>
					<option value='RENT_STS_NM'><%=mm.getMessage("SUMG_1175", s_user.getLocale())%>
				</select> 
            </td>
        	<td>
        		<input class="text" type='text' id='term' value='' onkeypress="if(event.keyCode==13) {doQuery();}">
        	</td>
        	<td></td>
			<td><div class="btnarea">
	            	<a href="javascript:initSearch();" class="btn"><span><%=mm.getMessage("COMG_1065", s_user.getLocale())%></span></a>
	            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
	            </div>
	    	</td>
        </tr>
		</table>

		<!-- 임시 공간 -->
		<table width="100%" border="0">
     	  <tr>
			<td>
				 총 : <font color="red" id="listCount"></font> 건
			</td>
			<td>
				<!-- <div class="btnarea4" style="padding-right:16px;">
	          		<a href="javascript:maekPolicy();" class="btn"><span>대기목록에서 보안USB 대여</span></a>
	           		<a href="javascript:clickListButton();" class="btn"><span id='clickLost'>분실 목록</span></a>
	           	</div> -->
			</td>
		  </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" style="width:100%; height:40%; background-color:white; overflow:hidden;"></div><!-- 반출 목록 그리드 -->
<br>
<div>
<form name="form" method="post">
<table style="width:100%; height:10%;">
<tr><td colspan="11">
		<div style="width:100%; height:20px; background-color:#3da0e3; border:none; overflow: hidden; padding-top:10px;">
			<font style="font-family: Tahoma; font-size: 12px; color: #ffffff; font-weight: bold;">&nbsp; 선택 보안USB 상세 정보</font>
		</div>
	</td>
</tr>
<tr>
   	<td>
       <table class="board-search" style="table-layout:fixed">
       <colgroup>
            <col width="100px"/> 
            <col width="50px"/>
            <col width="145px"/> 
            <col width="50px"/> 
            <col width="145px"/> 
            <col width="50px"/> 
            <col width="145px"/> 
            <col width="50px"/> 
            <col width="145px"/> 
            <col width="50px"/> 
            <col width="145px"/>
            <col/>
           <!--  <col width="10%"/> 
            <col width="5%"/>
            <col width="10%"/> 
            <col width="5%"/> 
            <col width="10%"/> 
            <col width="5%"/> 
            <col width="10%"/> 
            <col width="5%"/> 
            <col width="10%"/> 
            <col width="5%"/> 
            <col width="10%"/> -->
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("SUMG_1177", s_user.getLocale())%></td>
        	<td><%=mm.getMessage("SUMG_1176", s_user.getLocale())%></td>
            <td>
            	<input id="USB_ID" class="text" type='text' value='' disabled="disabled"> 
            </td>
            <td><%=mm.getMessage("SUMG_1155", s_user.getLocale())%></td>
            <td>
				<input id="MODEL" class="text" type='text' value='' disabled="disabled">
            </td>
            <td><%=mm.getMessage("SUMG_1156", s_user.getLocale())%></td>
            <td>
				<input id="CAPACITY" class="text" type='text' value='' disabled="disabled">
            </td>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td class="tit"><%=mm.getMessage("SUMG_1178", s_user.getLocale())%> </td>
            <td><%=mm.getMessage("CPMG_1103", s_user.getLocale())%></td>
            <td><input id="REG_DT" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("SUMG_1179", s_user.getLocale())%></td>
            <td><input id="REG_USER_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("COMG_1063", s_user.getLocale())%></td>
            <td><input id="REG_DEPT_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("COMG_1062", s_user.getLocale())%></td>
            <td><input id="REG_POSITION_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td colspan="3"></td>
       </tr>
        <tr>
            <td class="tit"><%=mm.getMessage("SUMG_1180", s_user.getLocale())%></td>
            <td><%=mm.getMessage("SUMG_1181", s_user.getLocale())%></td>
            <td><input id="RENT_YMD" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("SUMG_1137", s_user.getLocale())%></td>
            <td><input id="RENT_USER_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("COMG_1063", s_user.getLocale())%></td>
            <td><input id="RENT_DEPT_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("COMG_1062", s_user.getLocale())%></td>
            <td><input id="RENT_POSITION_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td><%=mm.getMessage("SUMG_1175", s_user.getLocale())%></td>
            <td><input id="RENT_STS_NM" class="text" type='text' value='' disabled="disabled"></td>
            <td></td>
       </tr>
	</table>
	</td>
</tr>
</table>
</form>
</div>
<div id="tabbar" style="width:100%; height:950px;"></div>
</body>
</html>