<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IUser s_user = (IUser) session.getAttribute("j_user");
%>
<html>
<head>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<title>반출 팝업 스타트</title>
<script>

var layout;
var cell;

/*이건  내가 뻘짓한 팝업창 띄우는거. 반출 신청화면 */
function init(){
	
	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "1C",
	    skin: "dhx_web"
	});
	
	cell = layout.cells('a');
    cell.setWidth(700);
	cell.setHeight(600);
	cell.setText('정책세트 리스트');
    cell.attachURL("mappingUser1.jsp",true,true); 
	
}

</script>
<style>

#layoutObj {
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

 #caption {
	white-space: nowrap;
	cursor: default;
	line-height: 31px;
	font-family: Tahoma;
	font-size: 12px;
	color: #ffffff;
	font-weight: bold;
	width: 97%;
} 

</style>

</head>
<body onload="init();"style="overflow:hidden;">
<div id="layoutObj" style="position:absolute;"> <!-- z-index:50; -->	
</div>


</body>
</html>