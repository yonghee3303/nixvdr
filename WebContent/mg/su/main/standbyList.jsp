<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
String contextPath = request.getContextPath();
%>

<html>
<head>

<title>대기목록</title>
<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="/ext/include/su_grid_common_render.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>

<script>
var selRowId = null;

var GridObj 		= {};

var G_SERVLETURL = "<%=contextPath%>/mg/standbyList.do";

function init() {
	
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);
	doQuery();
}

function resize() {
	GridObj.setSizes();
}

function doQuery() {
<%-- 	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectStandbyList&grid_col_id="+grid_col_id);																						
	GridObj.clearAll(false); --%>
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	<%-- document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> "; --%>
	if(status == "false") alert(msg);
	return true;
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
	var linkId = GridObj.cells(rowId, GridObj.getColIndexById("LINK_ID")).getValue();
	//var param ="?LINK_ID="+linkId;
	
	//popupUsbRequest('', '', '', 750, 615, param); 
	
	try {
		var selUsbId = parent.subCell3.getFrame().contentWindow.usbId; 
		var selDrive = parent.subCell3.getFrame().contentWindow.selDrive;
		var selDriveSize = parent.subCell3.getFrame().contentWindow.selUsbSize;
		var selUsbType = parent.subCell3.getFrame().contentWindow.selUsbType;
		
		if(selDrive == "" || selDrive == null) {
			alert("USB 읽어오기 후 진행해 주세요.");
			return;
		}
	} catch(err) {
		alert("USB 읽어오기 후 진행해 주세요.");
		return;
	}

	centerWin("<%=contextPath%>/mg/su/main/rentalPopup.jsp?LINK_ID=" + linkId + "&USB_ID=" + selUsbId + "&DRIVE=" + selDrive+"&DRIVE_SIZE="+selDriveSize+"&USB_TYPE="+selUsbType, 800, 550, "no"); //반출 신청 팝업.
}

function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}


//안쓸듯?
//협력사 선택 POPUP을 띄운다.
function popupUsbRequest(title, left, top, width, height, param) {
    if (title == '') title = "보안USB 신청정보";
    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    
    var url = "/mg/su/main/popupUsbRequest.jsp";
        url += param;
	
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'yes';
	
	var ret = window.open(url, "ddd", 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}
function refresh() {
	location.replace("standbyList.jsp?&formid=SU_0110&gridid=GridObj");
}
</script>

<style>
    #gridbox {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }
    
    div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
    
</style>

</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding: 0px; overflow: hidden;">

<table align="center" style="width: 100%; height: 100%; margin:0; padding: 0;">
	<tr>
		<td align="center"><div id="gridbox" style="position: relative;"></div></td>
	</tr>
</table>
</body>
</html>