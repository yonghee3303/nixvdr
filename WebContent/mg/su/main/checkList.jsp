<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String contextPath = request.getContextPath();
%>


<html>
<head>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>

<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<script>
var G_SERVLETURL = "<%=contextPath%>"+"/mg/checkList.do";

var allCnt;
var haveCnt;
var rentCnt;
var returnDtCnt;
var lostCnt;		//총 분실
var lostExportCnt;  //반출 중 분실
var lostImportCnt;  //보유 중 분실
var banCnt;
//var malCnt;
var expireDtCnt;
var limitCnt;

function initCheck(){
	doAjax();
}

//CheckList 와 Chart가 동적으로 작동하기 위한...
function doAjax() {
/* 	var url = G_SERVLETURL + "?mod=selectCountUsb";
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false); */
}

function doAjaxEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	 
	 limitCnt = jData.limitCnt;
	 allCnt = jData.allCnt; //등록된 모든 USB의 갯수
	 haveCnt = jData.haveCnt; //보유하고 있는 USB 갯수
	 rentCnt = jData.rentCnt; //반출한 USB 갯수
	 returnDtCnt = jData.returnDtCnt; //반납기간이 지난 USB 갯수
	 lostImportCnt = jData.lostImportCnt; //보유 중분실한 USB 갯수
	 lostExportCnt = jData.lostExportCnt;
	 lostCnt = jData.lostCnt;
	 banCnt = jData.banCnt;	//반출 중인 USB 중 사용금지된  보안USB 갯수
	 expireDtCnt = jData.expireDtCnt; //만료기간이 지난 USB 갯수
	 loadCheck(allCnt, haveCnt, rentCnt, returnDtCnt, lostImportCnt, lostExportCnt, banCnt, expireDtCnt, limitCnt);
	// malCnt = jData.malCnt; //폐기 시킨 USB 갯수
	//loadCheck(allCnt, haveCnt, rentCnt, returnDtCnt, lostImportCnt, banCnt, malCnt);
	
	//chart를 생성하기 위해 해당하는 값들을 전송한다.
	//parent.createChart(allCnt, haveCnt, rentCnt, returnDtCnt, lostImportCnt, banCnt, malCnt);
	 parent.createChart(allCnt, haveCnt, rentCnt, returnDtCnt, lostCnt, banCnt, expireDtCnt);
	
}

function loadCheck(all, have, rent, returnDt, lostImport, lostExport, ban, expireDt, limit){
	//각 값들을 페이지에 보여주기 위한 처리다.
	
 	var textNode1 = document.createTextNode(all);
	document.getElementById("allCnt").appendChild(textNode1); //등록된 보안USB
	
	var textNode2 = document.createTextNode(have);
	document.getElementById("haveCnt").appendChild(textNode2); // 보유
	
	var textNode3 = document.createTextNode(rent);
	document.getElementById("rentCnt").appendChild(textNode3); // 대여
	
	<% if("Y".equals(pm.getString("component.usb.returnDay.useYn"))){%>
	var textNode4 =document.createTextNode(returnDt);
	document.getElementById("returnDtCnt").appendChild(textNode4); //반납일 초과
	<%}%>
	var textNode5 = document.createTextNode(lostImport);
	document.getElementById("lostImportCnt").appendChild(textNode5); //보유 중 분실
	
	var textNode9 = document.createTextNode(lostExport);
	document.getElementById("lostExportCnt").appendChild(textNode9);//대여 중 분실
	
	var textNode6 = document.createTextNode(ban);
	document.getElementById("banCnt").appendChild(textNode6); // 사용금지
	
	/* var textNode7 = document.createTextNode(malCnt);
	document.getElementById("malCnt").appendChild(textNode7); */ // 폐기
	
	var textNode7 = document.createTextNode(expireDt);
	document.getElementById("expireDtCnt").appendChild(textNode7); //만료기간 지남
	
	var textNode8 = document.createTextNode(limit);
	document.getElementById("limitCnt").appendChild(textNode8); // 생성가능 갯수
}


function refresh() {
	location.replace("checkList.jsp");
}

</script>

<style>
	td {
		padding:8px;
		font-size: 12px;
		border-bottom:1px solid #d9d9d9; 
	}
</style>

</head>
<body onload="initCheck();" style="padding:20px,0px,0px,0px; overflow:hidden;">
<form>
<table align="center" style="height:90%; width:90%;">
	<colgroup>
		<col width="25%">
		<col width="25%">
		<col width="25%">
		<col width="25%">
	</colgroup>
	<tr> 
		<td colspan="4" style="border-bottom:2px solid #2281d1;">
			> <%=mm.getMessage("SUMG_1189", s_user.getLocale())%><font style="font-weight:bold;" id="limitCnt"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%>
		</td>
	</tr>
	<tr> 
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1120", s_user.getLocale())%></td>
		<td colspan="3" style="text-align:right;"><font style="font-weight:bold;" id="allCnt"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></td>
	</tr>
	<tr>	
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1122", s_user.getLocale())%></td>
		<td style="text-align:right;"><a href="../mgmt/importMgmtLayout.jsp?formid=SU_0101&Gridid=GridObj" target="_parent"><font id="haveCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1123", s_user.getLocale())%></td>
		<td style="text-align:right;"><a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=5" target="_parent"><font id="rentCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
	</tr>
	<% if("Y".equals(pm.getString("component.usb.returnDay.useYn"))){%>
	<tr>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1124", s_user.getLocale())%></td>
		<td style="text-align:right;"><a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=1" target="_parent"><font id="returnDtCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1201", s_user.getLocale())%></td>
		<td style="text-align:right;"><a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=6" target="_parent"><font id="expireDtCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
	</tr>
	<tr>
		<td style="border-bottom:2px solid #d9d9d9;background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1126", s_user.getLocale())%></td>
		<td style="border-bottom:2px solid #d9d9d9; text-align:right;"><a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=3" target="_parent"><font id="banCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
		<td style="border-bottom:2px solid #d9d9d9;background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1101", s_user.getLocale())%>(<%=mm.getMessage("SUMG_1122", s_user.getLocale())%>/<%=mm.getMessage("SUMG_1123", s_user.getLocale())%>)</td>
		<td style="border-bottom:2px solid #d9d9d9; text-align:right;">
			<a href="../mgmt/importMgmtLayout.jsp?formid=SU_0101&Gridid=GridObj&flag=2" target="_parent">
				<font id="lostImportCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;
				<%=mm.getMessage("SUMG_1121", s_user.getLocale())%>
			</a>
			/<a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=2" target="_parent">
				<font id="lostExportCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;
				<%=mm.getMessage("SUMG_1121", s_user.getLocale())%>
			</a>
		</td>
	</tr>
	<%}else{ %>
	<tr>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1201", s_user.getLocale())%></td>
		<td style="text-align:right;"><a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=6" target="_parent"><font id="expireDtCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1126", s_user.getLocale())%></td>
		<td style="text-align:right;"><a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=3" target="_parent"><font id="banCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%></a></td>
	</tr>
	<tr>
		<td style="border-bottom:2px solid #d9d9d9;background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1101", s_user.getLocale())%>(<%=mm.getMessage("SUMG_1122", s_user.getLocale())%>)</td>
		<td style="border-bottom:2px solid #d9d9d9; text-align:right;">
			<a href="../mgmt/importMgmtLayout.jsp?formid=SU_0101&Gridid=GridObj&flag=2" target="_parent">
				<font id="lostImportCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;<%=mm.getMessage("SUMG_1121", s_user.getLocale())%>
			</a>
		</td>
		<td style="border-bottom:2px solid #d9d9d9;background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1101", s_user.getLocale())%>(<%=mm.getMessage("SUMG_1123", s_user.getLocale())%>)</td>
		<td style="border-bottom:2px solid #d9d9d9; text-align:right;">
			<a href="../mgmt/exportMgmtLayout.jsp?formid=SU_0102&Gridid=GridObj&flag=2" target="_parent">
				<font id="lostExportCnt" style="font-weight:bold;color:#2589ce;"></font>&nbsp;
				<%=mm.getMessage("SUMG_1121", s_user.getLocale())%>
			</a>
		</td>
	</tr>
	<%} %>
</table>
</form>
</body>
</html>