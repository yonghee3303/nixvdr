<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String formid = (String)request.getParameter("formid");
String contextPath = request.getContextPath();
%>


<html>
<head>

<title>사용자 선택</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<!-- 그리드 생성시 필요 -->
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

var GridObj 		= {};
var G_SERVLETURL = "<%=contextPath%>/mg/selectUser.do";

var userId;
var userNm;
var deptNm;
var position;

function init() {
	GridObj = setGridDraw(GridObj);
	
	GridObj.attachEvent("onRowSelect", function(rId,cInd){
		userId = GridObj.cells(rId,0).getValue();
		userNm = GridObj.cells(rId,1).getValue();
		deptNm = GridObj.cells(rId,2).getValue();
		position = GridObj.cells(rId,3).getValue();
		opener.selectUser_End(userId, userNm, deptNm, position);
	});
}

function doQuery() {
	
	var user_nm = document.getElementById("user_nm").value;
	
	var argu= "&USER_NM="+encodeURIComponent(user_nm);
	
	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUserList&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
	
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}

//자신의 창 닫기
function close(){
	opener.closeWinSelectUser();
}

//그리드에서 선택을 하고 확인을 누르면 자신을 열었던 창에 정보를 보내준다. 

function select(){
	opener.selectUser_End(userId, userNm, deptNm, position);
}

</script>

<style>

	#gridbox {
        width: 100%;
        height: 75%;
        margin: 0px;
        padding: 0px;
    }


	div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}

</style>

</head>

<body onload="init();" style="margin: 0px; padding:15px; overflow: hidden;">

<form name="form" method="post" onsubmit="return false;">
<table style="width:100%;">
<tr>
   	<td colspan="3">
	    <table class="board-search">
		<colgroup>
		    <col width="20%"/>
		    <col width="20%"/>
		    <col width="40%"/>
		    <col width="20%"/>
	    </colgroup>
		<tr>
			<td class="tit">
				<%=mm.getMessage("SUMG_1137", s_user.getLocale())%> <%=mm.getMessage("COMG_1021", s_user.getLocale())%>
			</td>
			<td>
				<input id="user_nm" class="text" type="text" value="" onkeypress="if(event.keyCode==13) {doQuery();}">
			</td>
			<td></td>
			<td>
				<div class="btnarea">
		            	<%-- <a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("검색", s_user.getLocale())%></span></a> --%>
		            	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
		        </div>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>


<div id="gridbox" style="position: relative;"></div>

<table style="width:100%;">
<tr>
	<td style="padding:10px,0px,0px,0px;">
		<!-- 빈 공간 -->
	</td>
	<td width="60%" style="padding:10px,0px,0px,0px;">
		<div class="btnarea">
	            	<a href="javascript:select();" class="btn"><span><%=mm.getMessage("SUMG_1151", s_user.getLocale())%></span></a>
	            	<a href="javascript:close();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
	    </div>
	</td>
</tr>

</table>
</form>
</body>
</html>