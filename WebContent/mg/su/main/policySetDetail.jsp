<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String contextPath =request.getContextPath();
%>

<html>
<head>

<title>정책세트 사용제한</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<style type="text/css">
	.centerTD{
		border-spacing: 25px;
	}
	.centerTD td{
		padding: 5px;
	}
	.bottomLine {
		border-width: 0px 0px 1px 0px;
		border-style: solid;
		border-color: rgb(232, 232, 232);
	}
	.margin5 {
		margin: 5px 5px 5px 5px;
	}
	.w13p {
		width: 13%;
	}
	.w15p {
		width: 15%;
	}
	.w20p {
		width: 20%;
	}
	.w22p {
		width: 22%;
	}
	.w30p {
		width: 30%;
	}
	.w100p {
		width: 100%;
	}
	.w50 {
		width:50px;
	}
	.w94 {
		width:94px;
	}
	.w200 {
		width:200px;
	}
	.textRight {
		text-align: right;
		padding-right: 15px;
	}
	.paddingLeft29 {
		padding-left: 29px !important;
	}
</style>

<script>

var loading = false;

function init() {
	loading = true;
	doLoad();
}

function doLoad() {
	var jdata = parent.jPolicyData;
		
	setRadioValue("IS_READCOUNT");
	if(jdata.IS_READCOUNT == "Y") {
		checkDisabled("READCOUNT", "READ_USB_USE_CD", false);	
	} else {
		checkDisabled("READCOUNT", "READ_USB_USE_CD", true);
	}
	
	document.form.READCOUNT.value = jdata.READCOUNT;
	
	//실행회수 초과시 이벤트
	if(jdata.IS_READ_STOP == "Y") {
		document.form.READ_USB_USE_CD.value = "STOP";
	} else {
		document.form.READ_USB_USE_CD.value = "DELETE";
	}
	
	setRadioValue("IS_EXPIRE_DT");
	
	if(jdata.IS_EXPIRE_DT == "Y") {
		checkDisabled("EXPIRE_YMD", "EXPIRE_USB_USE_CD", false);
	} else {
		checkDisabled("EXPIRE_YMD", "EXPIRE_USB_USE_CD", true);
	}
	
	document.form.EXPIRE_YMD.value = jdata.EXPIRE_YMD;
	//만료기간 초과시 이벤트
	if(jdata.IS_EXPIRE_STOP == "Y") {
		document.form.EXPIRE_USB_USE_CD.value = "STOP";
	} else {
		document.form.EXPIRE_USB_USE_CD.value = "DELETE";
	}
	setRadioValue("RUN_MAC_YN");
}

function doSave() {
	
	var jdata = parent.jPolicyData;
	
	getRadioValue("IS_READCOUNT");	
	jdata.READCOUNT = document.form.READCOUNT.value;
	//실행회수 초과시 이벤트
	if(document.form.READ_USB_USE_CD.value == "STOP") {
		jdata.IS_READ_STOP = "Y";
		jdata.IS_READ_INIT = "N";
	} else {
		jdata.IS_READ_STOP = "N";
		jdata.IS_READ_INIT = "Y";
	}
	
	//그리고 parnet.jPolicyData 이게 var temp = parnet.jPolicyData 로 담아서 temp에서 처리해도 부모쪽 json에 반영되는지 확인해보자 
	jdata.IS_EXPIRE_DT = document.form.IS_EXPIRE_DT.value;
	
	getRadioValue("IS_EXPIRE_DT");
	jdata.EXPIRE_YMD = document.form.EXPIRE_YMD.value;
	//만료기간 초과시 이벤트
	if(document.form.EXPIRE_USB_USE_CD.value == "STOP") {
		jdata.IS_EXPIRE_STOP = "Y";
		jdata.IS_EXPIRE_INIT = "N";
	} else {
		jdata.IS_EXPIRE_STOP = "N";
		jdata.IS_EXPIRE_INIT = "Y";
	}
	getRadioValue("RUN_MAC_YN");
	
	if(jdata.IS_READCOUNT=="Y"){
		if(jdata.READCOUNT==""){
			alert("사용 가능 횟수를 지정하지 않았습니다.");
			return false;
		}else if(jdata.READCOUNT>=10000){
			alert("사용 가능 횟수 최대 입력 가능한 값은 99999입니다.");
			return false;
		}
		
	}
	if(jdata.IS_EXPIRE_DT=="Y"){
		if(jdata.EXPIRE_YMD==""){
			alert("사용 가능 기간을 지정하지 않았습니다.");
			return false;
		}
	}
	return true;
	//alert(oj.responseText);
	//refresh(oj);
	//공통 컬럼
	/* 
	//jPolicyData = JSON.parse(oj.responseText);
	jPolicyData.SU_POLICY_BASIC_CD;		//기본정책코드
	jPolicyData.IS_EXPIRE_DT;			//만료일자지정 여부
	jPolicyData.EXPIRE_YMD;				//만료기간
	jPolicyData.IS_EXPIRE_INIT;			//기간만료시 자동삭제
	jPolicyData.IS_EXPIRE_STOP;			//기간만료시 사용중지
	jPolicyData.IS_READCOUNT;			//실행회수 지정여부
	jPolicyData.READCOUNT;				//실행회수
	jPolicyData.IS_READ_INIT;			//실행회수초과시 자동삭제
	jPolicyData.IS_READ_STOP;			//실행회수초과시 사용중지
	jPolicyData.IS_WAIT_CLOSE;			//실행후 지정시간 경과시 자동종료 여부
	jPolicyData.WAIT_TIME;				//지정시간
	jPolicyData.IS_LOGIN_FAIL_INIT;		//로그인 실패시 자동삭제
	jPolicyData.IS_LOGIN_FAIL_STOP;		//로그인 실패시 사용중지
	jPolicyData.IS_LOGIN_FAIL_COUNT;	//로그인실패 회수(로그인 실패 사용 유무컬럼은 존재하지 않는다. 따라서 0일 경우 실패 제한 없음 처리)
	jPolicyData.USB_ENCODE_CL_CD;		//보안방법 구분코드 (현재 사용안함 NULL)
	jPolicyData.USB_IMAGE_SIZE;			//지정용량		  (현재 사용안함 NULL)
		
	//SU_BASIC_POLICY 고유 컬럼
	jPolicyData.RUN_MAC_YN;				//지정PC 적용여부
	jPolicyData.USER_ATUH_YN;			//사용자인증여부
	
	//SU_POLICY 고유 컬럼
	jPolicyData.MAC_ADDR; 				//실행 MAC주소 
	jPolicyData.USB_ID;					//USB ID
	jPolicyData.USB_CREATE_ID;			//대여ID
	jPolicyData.SU_POLICY_GRP_CD;		//정책그룹 코드
	jPolicyData.USB_AUTH_CL_CD;			//보안USB 인증구분코드 (현재 사용안함 '' 처리)
	 */
 
	 //작업 순서
	//MAC_ADDR > SU_POLICY 별도 컬럼, MAC 은 MAC_LIST를 따로 저장해야함 추후 MAC_ADDR에 통합
	//보안USB 외부사용 TRUE 일 경우 SU_RENT_MGT 의 값은 Y
	 //1. SU_RENT_MGT 등록
	 //2. SU_POLICY 등록	
}

function checkDisabled(jsonId, selId, status) {
	if(jsonId != null) {
		document.getElementById(jsonId).disabled = status;
	}
	if(selId != null) {
		document.form[selId].disabled = status;
	}
}

//document.form.radioName.value 를 통한 value 값 추출 및 세팅이 안되고 있음
//라디오박스 값 추출
function getRadioValue(id) {
	var radio = document.getElementsByName(id);
	for(var i=0; i<radio.length; i++) {
		if(radio[i].checked) {
			parent.jPolicyData[id] = radio[i].value;	
		}
	}
}
//라디오박스 값 세팅
function setRadioValue(id) {
	if(parent.jPolicyData[id] == "Y") {
		document.getElementById(id+"_Y").checked = true;
	} else {
		document.getElementById(id+"_N").checked = true;
	}
}

</script>
<style>
	
</style>
</head>
<body onload="init();">
<form name="form" method="post">
	<table class="w100p margin5 centerTD">
		<colgroup>
			<col class="w15p">
			<col class="w30p">
			<col>
		</colgroup>
		<tr>
			<td class="textRight">
				사용가능횟수
			</td>
			<td>
				<input type="radio" id="IS_READCOUNT_Y" name="IS_READCOUNT" value="Y" onclick="checkDisabled('READCOUNT', 'READ_USB_USE_CD', false);">&nbsp;
				<input type="text" id="READCOUNT" name="READCOUNT" class="w50">&nbsp;회
			</td>
			<td>
				<input type="radio" id="IS_READCOUNT_N" name="IS_READCOUNT" value="N" onclick="checkDisabled('READCOUNT', 'READ_USB_USE_CD', true);">&nbsp;
				제한없음
			</td>
		</tr>
		<tr>
			<td class="textRight">
				초과시
			</td>
			<td colspan="2" class="paddingLeft29">
				<jsp:include page="/mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0510"/>
					<jsp:param name="tagname" value="READ_USB_USE_CD"/>
				</jsp:include>
			</td>
		</tr>
		<tr class="bottomLine">
			<td colspan="3"><br/></td>
		</tr>
		<tr>
			<td><br/></td>
		</tr>
		<tr>
			<td class="textRight">
				사용가능기간
			</td>
			<td>
				<input type="radio" id="IS_EXPIRE_DT_Y" name="IS_EXPIRE_DT" value="Y" onclick="checkDisabled('EXPIRE_YMD', 'EXPIRE_USB_USE_CD', false);">&nbsp;
				<input type="text" id="EXPIRE_YMD" name="EXPIRE_YMD" class="w50">&nbsp;일
			</td>
			<td>
				<input type="radio" id="IS_EXPIRE_DT_N" name="IS_EXPIRE_DT" value="N" onclick="checkDisabled('EXPIRE_YMD', 'EXPIRE_USB_USE_CD', true);">&nbsp;
				제한없음
			</td>
		</tr>
		<tr>
			<td class="textRight">
				경과시
			</td>
			<td colspan="2" class="paddingLeft29">
				<jsp:include page="/mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0510"/>
					<jsp:param name="tagname" value="EXPIRE_USB_USE_CD"/>
				</jsp:include>
			</td>
		</tr>
		<tr class="bottomLine">
			<td colspan="3"><br/></td>
		</tr>
		<tr>
			<td><br/></td>
		</tr>
		<tr>
			<td class="textRight">
				MAC 제어
			</td>
			<td>
				<input type="radio" id="RUN_MAC_YN_Y" name="RUN_MAC_YN" value="Y">&nbsp;
				허용된 MAC에서 구동가능
			</td>
		</tr>
		<tr>
			<td/>
			<td>
				<input type="radio" id="RUN_MAC_YN_N" name="RUN_MAC_YN" value="N">&nbsp;
				모든PC 구동가능
			</td>
		</tr>
	</table>			
</form>
</body>
</html>