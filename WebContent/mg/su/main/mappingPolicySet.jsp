<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String contextPath =request.getContextPath();
%>

<html>
<head>

<title>보안USB 생성창</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<style type="text/css">
	.w200 {
		width:200px;
	}
</style>

<script>

var jPolicyData = parent.jPolicyData;

function init(){
	menuLink(1);
}

function menuLink(no) {
	if(policyframe.loading == true) {
		if(!policyframe.doSave()) {
			return;
		}
	}
	for(var i=1; i<=3; i++) {
		if(no == i) {
			document.getElementById("menu"+i).style.fontWeight =	"bold";
		} else {
			document.getElementById("menu"+i).style.fontWeight =	"normal";
		}
	}
	
	switch(no) {
		case 1:
			policyframe.location.href='<%=contextPath%>/mg/su/main/policySetDetail.jsp';
			break;
		case 2:
			policyframe.location.href='<%=contextPath%>/mg/su/main/policySetEtc.jsp';
			break;
		case 3:
			policyframe.location.href='<%=contextPath%>/mg/su/main/policySetGrp.jsp?gridid=GridObj&formid=CP_051703';
			break;
	}
}

function doSave() {
	if(!policyframe.doSave()) {
		return;
	}
	
	var keys = Object.keys(jPolicyData);
	for(var i=0; i<keys.length; i++) {
		var key = keys[i];
		parent.opener.jPolicyData[key] = jPolicyData[key];
	}
	parent.opener.refresh();
	doCancel();
	
	//alert(oj.responseText);
	//refresh(oj);
	//공통 컬럼
	/* 
	//jPolicyData = JSON.parse(oj.responseText);
	jPolicyData.SU_POLICY_BASIC_CD;		//기본정책코드
	jPolicyData.IS_EXPIRE_DT;			//만료일자지정 여부
	jPolicyData.EXPIRE_YMD;				//만료기간
	jPolicyData.IS_EXPIRE_INIT;			//기간만료시 자동삭제
	jPolicyData.IS_EXPIRE_STOP;			//기간만료시 사용중지
	jPolicyData.IS_READCOUNT;			//실행회수 지정여부
	jPolicyData.READCOUNT;				//실행회수
	jPolicyData.IS_READ_INIT;			//실행회수초과시 자동삭제
	jPolicyData.IS_READ_STOP;			//실행회수초과시 사용중지
	jPolicyData.IS_WAIT_CLOSE;			//실행후 지정시간 경과시 자동종료 여부
	jPolicyData.WAIT_TIME;				//지정시간
	jPolicyData.IS_LOGIN_FAIL_INIT;		//로그인 실패시 자동삭제
	jPolicyData.IS_LOGIN_FAIL_STOP;		//로그인 실패시 사용중지
	jPolicyData.IS_LOGIN_FAIL_COUNT;	//로그인실패 회수(로그인 실패 사용 유무컬럼은 존재하지 않는다. 따라서 0일 경우 실패 제한 없음 처리)
	jPolicyData.USB_ENCODE_CL_CD;		//보안방법 구분코드 (현재 사용안함 NULL)
	jPolicyData.USB_IMAGE_SIZE;			//지정용량		  (현재 사용안함 NULL)
		
	//SU_BASIC_POLICY 고유 컬럼
	jPolicyData.RUN_MAC_YN;				//지정PC 적용여부
	jPolicyData.USER_ATUH_YN;			//사용자인증여부
	
	//SU_POLICY 고유 컬럼
	jPolicyData.MAC_ADDR; 				//실행 MAC주소 
	jPolicyData.USB_ID;					//USB ID
	jPolicyData.USB_CREATE_ID;			//대여ID
	jPolicyData.SU_POLICY_GRP_CD;		//정책그룹 코드
	jPolicyData.USB_AUTH_CL_CD;			//보안USB 인증구분코드 (현재 사용안함 '' 처리)
	 */
 
	 //작업 순서
	//MAC_ADDR > SU_POLICY 별도 컬럼, MAC 은 MAC_LIST를 따로 저장해야함 추후 MAC_ADDR에 통합
	//보안USB 외부사용 TRUE 일 경우 SU_RENT_MGT 의 값은 Y
	 //1. SU_RENT_MGT 등록
	 //2. SU_POLICY 등록	
}

function doCancel() {
	parent.window.close();
}





</script>
<style>
	.centerTD{
		border-spacing: 25px;
	}
	.centerTD td{
		padding: 5px;
	}
	.va_top {
		vertical-align: top;
	}
	.w1p {
		width: 1%;
	}
	.w8p {
		width: 8%;
	}
	.w10p {
		width: 10%;
	}
	.w13p {
		width: 13%;
	}
	.w15p {
		width: 15%;
	}
	.w70p {
		width: 70%;
	}
	.w90p {
		width: 90%;
	}
	.w98p {
		width: 98%;
	}
	.w100p {
		width: 100%;
	}
	.h88p {
		height: 88%;
	}
	.h90p {
		height: 90%;
	}
	.h100p {
		height: 100%;
	}
	.rightLine {
		border-width: 0px 1px 0px 0px;
		border-style: solid;
		border-color: rgb(232, 232, 232);
	}
</style>
</head>
<body onload="init();">
<form name="form" method="post">
<br>
	<table class="centerTD h88p">
		<colgroup>
			<col class="w1p">
			<col class="w13p">
			<col class="w85p">
			<col class="w1p">
		</colgroup>
		<tr>
			<td/>
			<td class="va_top rightLine">
				<!-- 메뉴바 -->
				<table>
					<tr>
						<td><p id="menu1" onclick="menuLink(1);">사용제한</p></td>
					</tr>
					<tr></tr>
					<tr>
						<td><p id="menu2" onclick="menuLink(2);">기타 설정</p></td>
					</tr>
					<tr></tr>
					<tr>
						<td><p id="menu3" onclick="menuLink(3);">정책 적용세트</p></td>
					</tr>
				</table>
			</td>
			<td>
				<!-- 아이프레임 본문 -->
				<iframe name="policyframe" class="w98p h100p" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
			</td>
		</tr>
	</table>
					
	<table class="w98p">
		<tr>
			<td>
				<div class="btnarea">
					<a href="javascript:doCancel();" class="btn">
					<span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
					<a href="javascript:doSave();" class="btn">
					<span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
				</div>
			</td>
		</tr>
	</table>			
</form>
</body>
</html>