<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="/ext/include/su_grid_common_render.jsp"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String contextPath = request.getContextPath();
String usbId = request.getParameter("usbId");
String drive = request.getParameter("drive");
%>
<html>
<head>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>

var GridObj 		= {};

var G_SERVLETURL = "<%=contextPath%>"+"/mg/importList.do";

var selRowId = null;
var selUsbId;
var selDrive;
var selDriveSize;

var winMappingUser;

function init() {

	GridObj = setGridDraw(GridObj);
	GridObj.enableColumnMove(false);
	GridObj.attachEvent("onXLE",doQueryEnd);
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);
	doQuery();
	
}

//목록 더블클릭 시 화면 이동 
function doOnRowSelected(rowId, cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	var usb_id = GridObj.cells(rowId, GridObj.getColIndexById("USB_ID")).getValue();
	parent.parent.mainFrame.location.href ='../mgmt/importMgmtLayout.jsp?USB_ID='+usb_id;

}
// 보안USB생성 팝업창 오픈
function mappingUser(){
	var regId=GridObj.cells(selRowId, GridObj.getColIndexById("ADMIN_NM")).getValue();
	var creDt=GridObj.cells(selRowId, GridObj.getColIndexById("REG_DT")).getValue();
	if(regId=="" || creDt==""){//웹소켓 연결 끊김으로 관리자 등록 안될 경우 자동 등록
		var url = "<%=contextPath%>/mg/enrollUSB.do?mod=enrollUSB";
		url+="&USB_ID="+selUsbId;
		sendRequest(reMappingUser, " " ,"POST", url , false, false);	
	}else{
		centerWin("<%=contextPath%>"+"/mg/su/main/rentalPopup.jsp?USB_ID="+selUsbId+"&DRIVE="+selDrive+"&DRIVE_SIZE="+selDriveSize, 800, 550, "no"); //반출 신청 팝업.
	}
}
function reMappingUser(oj){
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode=="0"){
		centerWin("<%=contextPath%>"+"/mg/su/main/rentalPopup.jsp?USB_ID=" + selUsbId + "&DRIVE=" + selDrive+"&DRIVE_SIZE="+selDriveSize, 800, 550, "no"); //반출 신청 팝업.
		doQuery();
	}
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}

//보안USB생성 팝업창 닫기
function closeWinMappingUser(){
	winMappingUser.close();
}

function resize() {
	GridObj.setSizes();
}

function doQuery() {
<%-- 	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectImportList&grid_col_id="+grid_col_id);																						
	GridObj.clearAll(false); --%>
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	<%-- document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> "; --%>
	//if(status == "false") alert(msg);
	
	selectedUSB("<%=usbId%>", "<%=drive%>");
	return true;
}

function selectedUSB(usbId, drive) {
	// 전달받은 값이 없으면 함수 종료
	if(usbId==null || usbId==""){
		return;
	}
	
	// 전달받은 값을 Grid 전체에서 찾아 셀의 위치 반환
	var result = GridObj.findCell(usbId);
	var chResult = String(result);
	// 셀의 위치에서 row값만 추출
	var array = chResult.split(',');
	
	// 일치하는 항목이 없는경우는 함수 종료
	if(array[0]==null || array[0]==""){
		return;
	}
	
	selUsbId = usbId;
	selDrive = drive;
	
	// 기존 선택된 row값(전역변수값)의 배경색 해제
	if (selRowId!=null){
		GridObj.setRowColor(selRowId,"white");
		//GridObj.changeCellType(selRowId,4,"ro");
		GridObj.cells(selRowId,4).setValue("");
	}
	
	// 전역변수로 지정
	selRowId = array[0];

	// row의 배경색 변경 & 최상위로 이동
	GridObj.setRowColor(selRowId,"#F2F0A0");
	GridObj.moveRow(selRowId,"row_sibling","0");
	
	//GridObj.changeCellType(selRowId,4,"img");
	
	GridObj.cells(selRowId,4).setValue("<input type='image' src='"+"<%=contextPath%>"+"/ext/images/btn_usb_export2.gif' onclick='mappingUser();' />");
	//드라이브 사이즈를 mappingUser페이지로 넘겨주기 위해... 
	selDriveSize=GridObj.cells(selRowId,3).getValue();
}

function refresh(id, dr) {
	location.replace("importList.jsp?&formid=SU_0105&gridid=GridObj&usbId="+id+"&drive="+dr);
	
}

function checkUsb(checkUsbId) {
	var result = true;
	GridObj.forEachRow(function(id){
		if(result != false) {
			if(checkUsbId == GridObj.cells(id, GridObj.getColIndexById("USB_ID")).getValue()) {
				result = false;
				return;
			}	
		} 	
	});	
}


</script>

<style>

    #gridbox {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }
    
    div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
    
</style>

</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding: 0px; overflow:hidden;">

<table style="width: 100%; height: 100%; margin:0px; padding: 0px;">
	<tr>
		<td><div id="gridbox" style="position: relative;"></div></td>
	</tr>
</table>
</body>
</html>