<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%
String usbId = (String)request.getParameter("USB_ID");
String rentId = (String)request.getParameter("USB_CREATE_ID");
String userIp = request.getRemoteAddr();

IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IUser s_user = (IUser) session.getAttribute("j_user");
String contextPath = request.getContextPath();
%>

<html>
<head>

<title>비밀번호 변경</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>


var usbId = "<%=usbId%>";
var rentId = "<%=rentId%>";


function chkPassword(){
	
	var chgPwd = document.getElementById("chgPwd").value;
	var chgPwdConfirm = document.getElementById("chgPwdConfirm").value;
	
	// 변경 비밀번호와 확인 비밀번호가 동일하게 입력되었는지 체크
	if(chgPwd != chgPwdConfirm){
		alert("<%=mm.getMessage("COMG_1004", s_user.getLocale())%>");
		return;
	}
	
	// 비밀번호와 USBID를 가지고 DB의 비밀번호와 맞는지 확인
	var initPwd = document.getElementById("initPwd").value;
	
	//alert(usbId);
	//alert(rentId);
	//alert(initPwd);
	
	
	var url = "<%=contextPath%>"+"/mg/su/chgPassword.do?mod=chkPassword&USB_ID="+usbId+"&USB_CREATE_ID="+rentId+"&PASSWORD="+initPwd;
	sendRequest(chkPasswordEnd, " " ,"POST", url , false, false);

}

function chkPasswordEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);

	//alert(jData.chkValue);
	
	if(jData.chkValue=="false"){
		alert("<%=mm.getMessage("COMG_1004", s_user.getLocale())%>");
		return;
	}
	
	// DB의 비밀번호와 일치할 경우만 변경가능
	chgPassword();
}


function chgPassword() {
	
	var chgPwd = document.getElementById("chgPwd").value;
	
	//비밀번호 변경
	var url = "<%=contextPath%>/mg/su/chgPassword.do?mod=chgPassword&USB_ID="+usbId+"&USB_CREATE_ID="+rentId+"&PASSWORD="+chgPwd+"&IP_ADDR="+"<%=userIp%>";
	sendRequest(chgPasswordEnd, " " ,"POST", url , false, false);
	
}

function chgPasswordEnd() {
	alert("<%=mm.getMessage("COMG_1002", s_user.getLocale())%>"); //작업성공.
	close();
}
function close() {
	
	opener.closeWinChgPassword();
}

</script>


<style>

	.board-search { width:100%; margin-bottom:10px; border:0;}
	
	.board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:0; background:#ffffff;}

	.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(/ext/images/bul_arrow3.gif) 8px 50% no-repeat;}

</style>


</head>
<body style="margin: 0px; padding:30px,30px,30px,30px; overflow: hidden;">

<form name="form" method="post">
<table class="board-search" align="center" style="width: 100%; margin:0px; padding: 0;">
	<tr>
		<td class="tit">
			<b><%=mm.getMessage("SUMG_1125", s_user.getLocale())%></b>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding:0px,0px,0px,30px;">
			<table align="center" style="width: 100%; margin:0px; padding: 0;">
				<tr>
					<td style="padding:15px,0px,0px,0px;">
						<%=mm.getMessage("SUMG_1128", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="initPwd" class="text" type="password" />
					</td>
				</tr>
				<tr>
					<td style="padding:15px,0px,0px,0px;">
						<%=mm.getMessage("SUMG_1129", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="chgPwd" class="text" type="password" />
					</td>
				</tr>
				<tr>
					<td style="padding:15px,0px,0px,0px;">
						<%=mm.getMessage("SUMG_1130", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="chgPwdConfirm" class="text" type="password" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding:30px,0px,0px,0px;">
			<div class="btnarea">
						<a href="javascript:chkPassword();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            		<a href="javascript:close();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
		    </div>
		</td>
	</tr>
</table>
</form>

</body>
</html>