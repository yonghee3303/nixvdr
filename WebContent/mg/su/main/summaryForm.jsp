<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String userIp = request.getRemoteAddr(); //접속한 user의 ip를 얻어온다.
String contextPath = request.getContextPath();
%>

<html>
<head>
<!--<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="description" content="Creative Button Styles  - Modern and subtle styles &amp; effects for buttons" />
<meta name="keywords" content="button styles, css3, modern, flat button, subtle, effects, hover, web design" />
<meta name="author" content="Codrops" /> -->

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<!-- WebSocket 적용 -->
<script type="text/javascript" src="../../../ext/js/websocket.js"></script>
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<!-- 버튼관련 참조 -->
<link href="../../../ext/CreativeButtons/css/default.css" rel="stylesheet" type="text/css">
<link href="../../../ext/CreativeButtons/css/component.css" rel="stylesheet" type="text/css">
<script src="../../../ext/CreativeButtons/js/modernizr.custom.js"></script>
<script src="../../../ext/CreativeButtons/js/classie.js"></script>


<script>
var G_SERVLETURL = "<%=contextPath%>/mg/enrollUSB.do";

var usbId = "";
var selDrive = "";
var selModel = "";
var selUsbSize = "";
var selUsbType = "";
var selUsbModelNm = "";

//USB상태값 (일반/보안/다른곳에서생성/에러)
var isStatus = "";

var enckey = "";

var winSelectUSB;

////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
var dhxWins;
var prg_win;
function BwindowWidth() {
	 if (window.innerWidth) {
		 return window.innerWidth;
	 } else if (document.body && document.body.offsetWidth) {
	 	return document.body.offsetWidth;
	 } else {
		 return 0;
	}
}	
function BwindowHeight() {
	 if (window.innerHeight) {
		 return window.innerHeight;
	} else if (document.body && document.body.offsetHeight) {
		 return document.body.offsetHeight;
	} else {
		 return 0;
	}
}
function doQueryDuring()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
}
////////////////////////////////////////////////////프로그래스바 처리때문에 

function readUSB(){
	/* window.open("/mg/su/main/selectUSB.jsp?formid=SU_0109&Gridid=GridObj",'','width=500, height=300,scrollbars=no,resizable=no'); */
	winSelectUSB = window.open("selectUsbPopUp.jsp?formid=SU_0109&Gridid=GridObj",'','width=500, height=250,scrollbars=no,resizable=no');
}

function closeWinSelectUSB() {
	winSelectUSB.close();
}

function init() {
	getEnckey();
	//WebSocketConnect();
	
	if(dhxWins == null) {
    	dhxWins = new dhtmlXWindows();
    	dhxWins.setImagePath("<%=contextPath%>/dthmlx/dhtmlxWindows/codebase/imgs/");
    }

	if(prg_win == null) {
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;
		
		prg_win = dhxWins.createWindow("prg_win", top, left, 180, 110);
		prg_win.setText("Please wait for a moment.");
		prg_win.button("close").hide();
		prg_win.button("minmax1").hide();
		prg_win.button("minmax2").hide();
		prg_win.button("park").hide();
		dhxWins.window("prg_win").setModal(false);
		prg_win.attachURL("../../../ext/include/progress_ing.htm");
		dhxWins.window("prg_win").hide();
	}
	
}

function getEnckey() {
	
/* 	var url = G_SERVLETURL + "?mod=selectEnckey";
	sendRequest(getEnckeyEnd, " " ,"POST", url , false, false); */
}


function getEnckeyEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);
	
	enckey = jData.ENCKEY;
	
}
function deliver(drive, model, usbSize, usbType, chkId) {
	
	// 받은 USB 정보를 전역변수에 저장 (용량은 GB 문자를 떼어 저장)
	selDrive = drive;
	selModel = model;
	selUsbType=usbType;
	
	try {
		var arr_usbSize = usbSize.split(' GB');
		selUsbSize = parseInt(arr_usbSize[0]) * 1024;
	} catch(err) {
		selUsbSize = usbSize;
	} 
	
	// USB정보를 div에 표시한다
	document.getElementById("deviceNM").innerHTML = "<b><%=mm.getMessage("SUMG_1154", s_user.getLocale())%> : " + drive + "<br />" + "<%=mm.getMessage("SUMG_1155", s_user.getLocale())%> : " + model + "<br />" + "<%=mm.getMessage("SUMG_1156", s_user.getLocale())%> : " + usbSize + "</b>";
	
	if(chkId.length > 0) {
		usbId = chkId;
		isStatus = "TRUE";
		//USBID 값으로 DB에서 보유/반출 상태 확인
		var url = G_SERVLETURL + "?mod=checkUSB&USB_ID=" + usbId+"&usbType="+usbType;
		sendRequest(checkUSBEnd, " " ,"POST", url , false, false);
	} else {
		/* // 보안USB인지 체크
		var isSecureUSB = document.USBInfo.IsSecureUSBDrive(drive, enckey);
		 // 리턴값의 구분자를 삭제 : 배열[1]에 구분에 필요한 데이터가 담긴다
		var array = isSecureUSB.split('{}[]');
		isStatus = array[1];
		
		// 보안USB인 경우 보유/반출을 구분하기 위한 처리
		if(isStatus=="-1" || isStatus=="0"){
			usbId = "";
			checkUsbStatus(isStatus,usbId,selDrive,"");//페이지에 읽어온 USB정보를 보여준다.
		}else{
			usbId= array[3];
			//USBID 값으로 DB에서 보유/반출 상태 확인
			var url = G_SERVLETURL + "?mod=checkUSB&USB_ID=" + usbId+"&usbType="+usbType;
			sendRequest(checkUSBEnd, " " ,"POST", url , false, false);	
		} */		
	}
}

function checkUSBEnd(oj){
	var jData = JSON.parse(oj.responseText);

	 var status = jData.haveUsb;
	//alert(oj.responseText);
	 // 버튼 활성/비활성화 처리
	 if("true"!=status){ //반출 USB
		 checkUsbStatus(isStatus,usbId,selDrive,status);//페이지에 읽어온 USB정보를 보여준다.
		 document.getElementById("btnEnrollUSB").disabled=true;
		 document.getElementById("btnEnrollUSB").className ="disable_bbt";  // 비활성화
		 
		 document.getElementById("btnCollectUSB").disabled=false;
		 document.getElementById("btnCollectUSB").className="active_bbt";  // 활성화
		 
		 document.getElementById("btnDelUSB").disabled=false;
		 document.getElementById("btnDelUSB").className="active_bbt_red";  // 활성화
	 }else { //보유 USB
		 checkUsbStatus(isStatus,usbId,selDrive,status);//페이지에 읽어온 USB정보를 보여준다.
		 document.getElementById("btnEnrollUSB").disabled=true;
		 document.getElementById("btnEnrollUSB").className="disable_bbt";  // 비활성화
		 
		 document.getElementById("btnCollectUSB").disabled=true;
		 document.getElementById("btnCollectUSB").className="disable_bbt";  // 비활성화
		 
		 document.getElementById("btnDelUSB").disabled=false;
		 document.getElementById("btnDelUSB").className="active_bbt_red";  // 활성화
	 } 
}

//페이지에 읽어온 USB정보를 보여준다. 
function checkUsbStatus(sts, id, dr, st){
	
	var msg = "";
	
	// 리턴값에 따른 처리
	switch(sts) {
		case "TRUE":
			if(st!="false"){
				//msg = dr + " 드라이브는 보유한 보안USB 입니다.";
				msg = "<%=mm.getMessage("SUMG_1159", s_user.getLocale())%>";
				/* document.getElementById("deviceDetail").innerHTML = "<br /><b>USB ID : " + id +"<br /><br />"+ msg + "</b>"; */
				
			}else if(st!="true"){
				//msg = dr + " 드라이브는 반출된 보안USB 입니다.";	
				msg = "<%=mm.getMessage("SUMG_1160", s_user.getLocale())%>";
				//document.getElementById("deviceDetail").innerHTML = "<br /><b>USB ID : " + id +"<br /><br />"+ msg + "</b>";
			}
			document.getElementById("deviceDetail").innerHTML = "<b>"+ msg + "</b>";
			break;
			
		case "FALSE":
			
			//usbId = array[3];
			//msg = dr + " 드라이브는 일반 USB 입니다.";
			msg = "<%=mm.getMessage("SUMG_1158", s_user.getLocale())%>";
			document.getElementById("deviceDetail").innerHTML = "<b>" + msg + "</b>";
			
			document.getElementById("btnEnrollUSB").disabled=false;
			document.getElementById("btnEnrollUSB").className="active_bbt";  // 활성화
			
			document.getElementById("btnCollectUSB").disabled=true;
			document.getElementById("btnCollectUSB").className="disable_bbt";  // 비활성화
			
			document.getElementById("btnDelUSB").disabled=true;
			document.getElementById("btnDelUSB").className="disable_bbt_red";  // 비활성화
			break;
			
		default:
			
			msg = dr + "<%=mm.getMessage("SUMG_1163", s_user.getLocale())%>";
			document.getElementById("deviceDetail").innerHTML = msg;
			break;
	}
	//보유목록에 해당 USB_ID 전송
	var ifr = parent.subCell4.getFrame();	//importList.jsp
	ifr.contentWindow.selectedUSB(id, dr);
	var ex_irf = parent.subCell6.getFrame();	//exportList.jsp
	ex_irf.contentWindow.selectedUSB(id);
}
function enrollUSB() {
	
	//드라이브가 선택되지 않은 경우
	if(selDrive==""){
		alert("<%=mm.getMessage("SUMG_2016", s_user.getLocale())%>");
		return;
	}
	alert("<%=mm.getMessage("SUMG_2014", s_user.getLocale())%>");
	var url = G_SERVLETURL + "?mod=selectEnrollUSB&drive=" + selDrive+"&usbType="+selUsbType;
	sendRequest(enrollUSBEnd, " " ,"POST", url , false, false);
	doQueryDuring();
}

function enrollUSBEnd(oj){
	var jData = JSON.parse(oj.responseText);
	
	if(jData.errcode == "-1"){
		alert(jData.errmsg);
		doQueryModalEnd();
		return;
	}
	usbId = jData.SU_POLICY.USB_ID;
	sendWsMessage(oj.responseText);
}

function updateImportList(){
	
	var checkList_ifr = parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	var import_ifr = parent.subCell4.getFrame();//반출 refresh
	
	//importList.jsp 반출 아이콘을 활성화하기위해 호출
	if(winSelectUSB.closed == true && selUsbModelNm != "") {
		alert("["+usbId+"] 등록이 완료되었습니다.");
		//보유목록에 해당 USB_ID 전송
		import_ifr.contentWindow.refresh(usbId, selDrive);
		var ex_irf = parent.subCell6.getFrame();	//exportList.jsp
		ex_irf.contentWindow.selectedUSB(usbId);
	} else {
		import_ifr.contentWindow.refresh();	
	}
	isStatus="1";

	<%-- alert("<%=mm.getMessage("SUMG_1005", s_user.getLocale())%>"); --%>
	
	document.getElementById("deviceNM").innerHTML = "<b><%=mm.getMessage("SUMG_1154", s_user.getLocale())%> : "+selDrive +"<br />" + "<%=mm.getMessage("SUMG_1155", s_user.getLocale())%> : "+selModel+ "<br />" + "<%=mm.getMessage("SUMG_1156", s_user.getLocale())%> : "+selUsbSize+ "</b>";
	document.getElementById("deviceDetail").innerHTML = "<b><%=mm.getMessage("SUMG_1164", s_user.getLocale())%></b>";
	document.getElementById("btnEnrollUSB").disabled=true;
	document.getElementById("btnEnrollUSB").className="disable_bbt";  // 비활성화
	document.getElementById("btnCollectUSB").disabled=true;
	document.getElementById("btnCollectUSB").className="disable_bbt";  // 비활성화
	document.getElementById("btnDelUSB").disabled=false;
	document.getElementById("btnDelUSB").className="active_bbt_red";  // 활성화
	doQueryModalEnd();
}
function collectUSB(){
	
	if(confirm("<%=mm.getMessage("SUMG_1165", s_user.getLocale())%> " + selDrive + " <%=mm.getMessage("SUMG_1154", s_user.getLocale())%> " + usbId + " <%=mm.getMessage("SUMG_1166", s_user.getLocale())%>")==false){
		return;
	}
	
	var url = G_SERVLETURL + "?mod=collectUSB&USB_ID=" + usbId;
	var user = "<%= s_user.getId().toString()%>";
	url+="&USER_ID=<%= s_user.getId().toString()%>&IP_ADDR="+"<%=userIp%>";
	sendRequest(collectUSBEnd, " " ,"POST", url , false, false);
	
}

function collectUSBEnd() {
	
	var checkList_ifr = parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	
	var import_ifr = parent.subCell4.getFrame();//보유 refresh
	import_ifr.contentWindow.refresh();
	import_ifr.contentWindow.selectedUSB(usbId, selDrive);
	var export_ifr = parent.subCell6.getFrame();//반출 refresh
	export_ifr.contentWindow.refresh();
	export_ifr.contentWindow.selectedUSB(usbId);
	
	alert(usbId+"<%=mm.getMessage("SUMG_1167", s_user.getLocale())%>");
	
	document.getElementById("deviceNM").innerHTML = "<b><%=mm.getMessage("SUMG_1154", s_user.getLocale())%> : "+selDrive +"<br />" + "<%=mm.getMessage("SUMG_1155", s_user.getLocale())%> : "+selModel+ "<br />" + "<%=mm.getMessage("SUMG_1156", s_user.getLocale())%> : "+selUsbSize+ "</b>";
	document.getElementById("deviceDetail").innerHTML = "<b><%=mm.getMessage("SUMG_1164", s_user.getLocale())%></b>";
	document.getElementById("btnEnrollUSB").disabled=true;
	document.getElementById("btnEnrollUSB").className="disable_bbt";  // 비활성화
	document.getElementById("btnCollectUSB").disabled=true;
	document.getElementById("btnCollectUSB").className="disable_bbt";  // 비활성화
	document.getElementById("btnDelUSB").disabled=false;
	document.getElementById("btnDelUSB").className="active_bbt_red";  // 활성화
	
}
function delUSB(){
	
	if(confirm("<%=mm.getMessage("SUMG_2017", s_user.getLocale())%>")==false){
		return;
	}
	if("" != usbId && usbId !=null){
		//document.USBInfo.DeleteSecureUSB(selDrive);
		var usbInfo = new Object();
		usbInfo.action = "DeleteSecureUSB";
		usbInfo.DRIVE = selDrive;
		sendWsMessage(JSON.stringify(usbInfo));
		doQueryDuring();
	}else{
		alert("USB를 선택해주세요.");
		return
	}
}
function delUSBEnd(){
	
	var checkList_ifr = parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	
	var import_ifr = parent.subCell4.getFrame();//보유 refresh
	import_ifr.contentWindow.refresh();
	var export_ifr = parent.subCell6.getFrame();//반출 refresh
	export_ifr.contentWindow.refresh();
	
	doQueryModalEnd();
	
	alert("["+usbId+"] <%=mm.getMessage("SUMG_1168", s_user.getLocale())%>");
	
	document.getElementById("deviceNM").innerHTML = "<b><%=mm.getMessage("SUMG_1154", s_user.getLocale())%> : "+selDrive +"<br />" + "<%=mm.getMessage("SUMG_1155", s_user.getLocale())%> : "+selModel+ "<br />" + "<%=mm.getMessage("SUMG_1156", s_user.getLocale())%> : "+selUsbSize+ "</b>";
	document.getElementById("deviceDetail").innerHTML = "<b>등록되지 않은 USB 입니다. 등록 버튼을 누르면 보유 목록으로 들어갑니다.</b>";
	document.getElementById("btnEnrollUSB").disabled=false;
	document.getElementById("btnEnrollUSB").className="active_bbt";  // 활성화
	document.getElementById("btnCollectUSB").disabled=true;
	document.getElementById("btnCollectUSB").className="disable_bbt";   // 비활성화
	document.getElementById("btnDelUSB").disabled=true;
	document.getElementById("btnDelUSB").className="disable_bbt_red";  // 비활성화
	
}
function createUSBEnd(){

	document.getElementById("deviceNM").innerHTML = "<b><%=mm.getMessage("SUMG_1154", s_user.getLocale())%> : "+selDrive +"<br />" + "<%=mm.getMessage("SUMG_1155", s_user.getLocale())%> : "+selModel+ "<br />" + "<%=mm.getMessage("SUMG_1156", s_user.getLocale())%> : "+selUsbSize+ "</b>";
	document.getElementById("deviceDetail").innerHTML = "<b><%=mm.getMessage("SUMG_1160", s_user.getLocale())%></b>";
	document.getElementById("btnEnrollUSB").disabled=true;
	document.getElementById("btnEnrollUSB").className="disable_bbt";  // 비활성화
	 
	document.getElementById("btnCollectUSB").disabled=false;
	document.getElementById("btnCollectUSB").className="active_bbt";  // 활성화
	 
	document.getElementById("btnDelUSB").disabled=false;
	document.getElementById("btnDelUSB").className="active_bbt_red";  // 활성화
}


//Websocket 이벤트 핸들러
function onOpen(evt){
    //called as soon as a connection is opened
    isConnection = true;
} // end onOpen

function onClose(evt){
    //called when connection is severed
    isConnection = false;
} // end onClose;

function onMessage(evt){
	//called on receipt of message
	var jData;
	try {
		jData = JSON.parse(evt.data);	
	} catch(err) {
		return;
	}
	    
    if(jData.errmsg.indexOf("DeleteSecureUSB") != -1) {
		if(jData.errcode == "0") {
			var url = G_SERVLETURL + "?mod=delUSB&USB_ID=" + usbId;
			var user = "<%= s_user.getId().toString()%>";
			url+="&USER_ID="+user+"&IP_ADDR="+"<%=userIp%>";
			sendRequest(delUSBEnd, " " ,"POST", url , false, false);
    	} else {
    		alert("보안 USB 해제하는 중 오류가 발생하였습니다.");
    		//alert("Failed to connect to the DeleteSecureUSB Service.");
    	}
    } else if(winSelectUSB.closed == true && jData.errmsg.indexOf("Secure USB Create") != -1 && parent.subCell4.getFrame().contentWindow.checkUsb(usbId)) {
    	if(jData.result == "TRUE") {
    		//RESPONSE: { "result": "TRUE", "errcode": "0", "errmsg": "Secure USB Create Success." }
			//RESPONSE: { "result": "FALSE", "errcode": "-1", "errmsg": "Secure USB Create Fail." }
			var usbInfo = new Object();
			usbInfo.action = "getUSBInfo";
			usbInfo.DRIVE = selDrive;
			sendWsMessage(JSON.stringify(usbInfo));	
    	} else {
    		doQueryModalEnd();
    		alert("보안USB를 생성하는 중 오류가 발생하였습니다.");
    		//alert("Failed to connect to the NewCreateSecureUSB Service.");
    	}
    } else if(winSelectUSB.closed == true && jData.errmsg.indexOf("getUSBInfo") != -1) {
    	if(jData.result == "TRUE") {
    		//RESPONSE: { "PRODID": "", "VENDORID": "", "SERIALNO": "89388C84", "DISKFULLSIZE": "8162086912", "DRIVESIZE": "8162082816", "PNPDEVID": "USBSTOR\\DISK&VEN_SMI&PROD_USB_DISK&REV_1100\\6&3636BEF7&0", "result": "TRUE", "errcode": "0", "errmsg": "getUSBInfo " }
    		var usbCapacity = jData.DISKFULLSIZE/1024/1024;
    		selUsbModelNm = jData.VENDORID;
    		var url = G_SERVLETURL + "?mod=enrollUSB";
    		url+="&USB_ID="+usbId;
    		url+="&USB_CAPACITY="+usbCapacity;
    		url+="&usbType="+selUsbType;
    		url+="&USB_MODEL_NM="+selUsbModelNm;
    		url+="&USB_KEY="+jData.SERIALNO;
    		url+="&ADMIN_ID=<%=s_user.getId().toString()%>";
    		url+="&REG_ID=<%=s_user.getId().toString()%>";
    		
    		sendRequest(updateImportList, " " ,"POST", url , false, false);	
    	} else {
    		doQueryModalEnd();
    		alert("USB 정보를 읽어오는 중 오류가 발생하였습니다.");
			//alert("Failed to connect to the getUSBInfo Service.");
    	}
    }
} // end onMessage

function onError(evt){
    //called on error
} // end onError
</script>


<style>


	/* 원본 .btnarea { float:right; width:auto; margin:0; height:auto;}
	.board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:1px dashed #ccc; background:#f4f4f4;}
	table, td, th, li  { font-size:12px; font-family:"굴림", Verdana, Tahoma; color:#565656; }
	table {border-collapse:collapse; border-spacing:0;}
	
	
			
	.btnarea a.btn:link, .btnarea a.btn:visited, .btnarea a:active { color: #FFF;}
	.btnarea a.btn { float:left; display: block; margin-right:5px; height:25px; padding-left:15px; background: transparent url('../../../ext/images/btn1_left_bg.gif') 0 0 no-repeat; font:bold 11px Dotum, Verdana, Trebuchet MS; line-height: 25px; text-decoration: none;}
	a { color:#555; text-decoration:none; cursor:pointer} 
	.board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:1px dashed #ccc; background:#f4f4f4;}
	table {border-collapse:collapse; border-spacing:0;}
	
	
	.btnarea a.btn span {display:block; padding: 0 15px 0 0; background:transparent url('../../../ext/images/btn1_right_bg.gif') no-repeat top right;}
	.btnarea a.btn:link, .btnarea a.btn:visited, .btnarea a:active { color: #FFF;}
	.btnarea a.btn { float:left; display: block; margin-right:5px; height:25px; padding-left:15px; background: transparent url('../../../ext/images/btn1_left_bg.gif') 0 0 no-repeat; font:bold 11px Dotum, Verdana, Trebuchet MS; line-height: 25px; text-decoration: none;}
	a { color:#555; text-decoration:none; cursor:pointer}
	.board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:1px dashed #ccc; background:#f4f4f4;}
	table {border-collapse:collapse; border-spacing:0;} */
	
.btnReadUSB {
	-moz-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	box-shadow:inset 0px 1px 0px 0px #bee2f9;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #63b8ee), color-stop(1, #468ccf));
	background:-moz-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:-webkit-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:-o-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:-ms-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:linear-gradient(to bottom, #63b8ee 5%, #468ccf 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#63b8ee', endColorstr='#468ccf',GradientType=0);
	background-color:#63b8ee;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #3866a3;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:malgun gothic, Verdana, Tahoma;
	font-size:12px;
	font-weight:bold;
	text-decoration:none;
	text-shadow:0px 1px 0px #7cacde;
	width:100%;
	height:100%;
}
.btnReadUSB:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #468ccf), color-stop(1, #63b8ee));
	background:-moz-linear-gradient(top, #468ccf 5%, #63b8ee 100%);
	background:-webkit-linear-gradient(top, #468ccf 5%, #63b8ee 100%);
	background:-o-linear-gradient(top, #468ccf 5%, #63b8ee 100%);
	background:-ms-linear-gradient(top, #468ccf 5%, #63b8ee 100%);
	background:linear-gradient(to bottom, #468ccf 5%, #63b8ee 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#468ccf', endColorstr='#63b8ee',GradientType=0);
	background-color:#468ccf;
}
.btnReadUSB:active {
	position:relative;
	top:1px;
}
	
	
	#USBstate {
		background-color:#e8f5df;
		border:1px solid #9ece9e;
		font-family:malgun gothic, Verdana, Tahoma;
		width:100%;
		height:100%;
		
	}
	#USBstate2 {
		background-color:#dfeff5;
		border:1px solid #9ebcce;
		font-family:malgun gothic, Verdana, Tahoma;
		background-image: URL(../../../ext/images/icon/info.png);
		background-repeat: no-repeat;
		background-position: 3% 50%;
		width:100%;
		height:100%;
		padding: 0px 0px 0px 30px;
	}
	
	#bottombt {
		font-family:malgun gothic, Verdana, Tahoma;
		width:100%;
		height:100%;
	}
	
	.btnEnrollUSB {
		color:white;
		/* background-color:#2589CE; */
		background-color:#F95329;
		width:96%;
		height:95%;
	}
	
	.btnCollectUSB {
		color:white;
		/* background-color:#2589CE; */
		background-color:#F95329;
		width:96%;
		height:95%;
	}
	
	.btnDelUSB {
		color:white;
		/* background-color:#2589CE; */
		background-color:#F95329;
		width:96%;
		height:95%;
	}

.messagebox {
		color:black;
		font-family:malgun gothic, Verdana, Tahoma;
		font-size:12px;
		
	}
	
.BButtonst1 {
	-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	background-color:#79bbff;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #84bbf3;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:malgun gothic, Verdana, Tahoma;
	font-size:30px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #528ecc;
	width:100%;
	height:100%;
}
.BButtonst1:hover {
	background-color:#378de5;
}
.BButtonst1:active {
	position:relative;
	top:1px;
}

.active_bbt {
	border-top: 1px solid #9ccce8;
	-moz-box-shadow:inset 0px 1px 0px 0px #54a3f7;
	-webkit-box-shadow:inset 0px 1px 0px 0px #54a3f7;
	box-shadow:inset 0px 1px 0px 0px #54a3f7;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #007dc1), color-stop(1, #0061a7));
	background:-moz-linear-gradient(top, #007dc1 5%, #0061a7 100%);
	background:-webkit-linear-gradient(top, #007dc1 5%, #0061a7 100%);
	background:-o-linear-gradient(top, #007dc1 5%, #0061a7 100%);
	background:-ms-linear-gradient(top, #007dc1 5%, #0061a7 100%);
	background:linear-gradient(to bottom, #007dc1 5%, #0061a7 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#007dc1', endColorstr='#0061a7',GradientType=0);
	background-color:#007dc1;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	border:1px solid #124d77;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:malgun gothic, Verdana, Tahoma;
	font-size:13px;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #154682;
	width:100%;
	height:80%;
}
.active_bbt:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #0061a7), color-stop(1, #007dc1));
	background:-moz-linear-gradient(top, #0061a7 5%, #007dc1 100%);
	background:-webkit-linear-gradient(top, #0061a7 5%, #007dc1 100%);
	background:-o-linear-gradient(top, #0061a7 5%, #007dc1 100%);
	background:-ms-linear-gradient(top, #0061a7 5%, #007dc1 100%);
	background:linear-gradient(to bottom, #0061a7 5%, #007dc1 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0061a7', endColorstr='#007dc1',GradientType=0);
	background-color:#0061a7;
}
.active_bbt:active {
	position:relative;
	top:1px;
}

.disable_bbt {
	background-color:#a4c0ca;
  border:1px solid #acacac;
  width:100%;
	height:80%;
	font-family:malgun gothic, Verdana, Tahoma;
	font-size:13px;
}
.active_bbt_red {
	border-top: 1px solid #FFA7A7;
	-moz-box-shadow:inset 0px 1px 0px 0px #F15F5F;
	-webkit-box-shadow:inset 0px 1px 0px 0px #F15F5F;
	box-shadow:inset 0px 1px 0px 0px #F15F5F;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #CC3D3D), color-stop(1, #980000));
	background:-moz-linear-gradient(top, #CC3D3D 5%, #980000 100%);
	background:-webkit-linear-gradient(top, #CC3D3D 5%, #980000 100%);
	background:-o-linear-gradient(top, #CC3D3D 5%, #980000 100%);
	background:-ms-linear-gradient(top, #CC3D3D 5%, #980000 100%);
	background:linear-gradient(to bottom, #CC3D3D 5%, #980000 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#CC3D3D', endColorstr='#980000',GradientType=0);
	background-color:#CC3D3D;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	border:1px solid #670000;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:malgun gothic, Verdana, Tahoma;
	font-size:13px;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #670000;
	width:100%;
	height:80%;
}
.active_bbt_red:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #980000), color-stop(1, #CC3D3D));
	background:-moz-linear-gradient(top, #980000 5%, #CC3D3D 100%);
	background:-webkit-linear-gradient(top, #980000 5%, #CC3D3D 100%);
	background:-o-linear-gradient(top, #980000 5%, #CC3D3D 100%);
	background:-ms-linear-gradient(top, #980000 5%, #CC3D3D 100%);
	background:linear-gradient(to bottom, #980000 5%, #CC3D3D 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#980000', endColorstr='#CC3D3D',GradientType=0);
	background-color:#980000;
}
.active_bbt_red:active {
	position:relative;
	top:1px;
}
.disable_bbt_red {
	background-color:#CAA5A5;
  border:1px solid #acacac;
  width:100%;
	height:80%;
	font-family:malgun gothic, Verdana, Tahoma;
	font-size:13px;
}	
</style>

</head>
<body onload="init();" style="margin: 0px,0px,0px,0px; padding: 0px,0px,0px,0px; overflow:hidden;">


<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->
<form name="summaryForm" method="post">
<table cellspacing="7" align="center" style="width:350px; height:230px" >

	<tr>
		<td style="width:33%; height:30%; border:0px solid;">
			<!-- <button id="detailBtn" style="background-color:#2589CE; width:100%; height:100%;"></button> -->
			<button class="btnReadUSB" onclick="readUSB(); return false;"><%=mm.getMessage("SUMG_1185", s_user.getLocale())%></button>
		</td>
		<td colspan="2" align="center" style="width:50%; height:25%; padding: 5px 0px 5px 10px;">
			
			<table id="USBstate" align="center" >
			<tr>
				<td align="center">
								<p id="deviceNM" class="messagebox" align="center"><%=mm.getMessage("SUMG_1004", s_user.getLocale())%></p>
							</td>
						</tr>
			</table>
							
		</td>
	</tr>

	<tr>
		<td colspan="3" style="width:100%; height:50%; padding: 5px 0px 5px 0px;">
			
			<table id="USBstate2" class="message2" align="center">
				<tr>
					<td style="padding: 0px 10px 0px 40px;">
								<p id="deviceDetail" class="messagebox" ><%=mm.getMessage("SUMG_2016", s_user.getLocale())%></p>
							</td>
						</tr>
					</table>
		</td>
	</tr>

	<tr>
		<td style="width:33.3%; height:20%; padding: 0px 4px 0px 0px;">
			<!-- <button style="width:100%; height:100%;"></button> -->
			
			<table id="bottombt" align="left">
				<tr>
					<td align="center" style="width:100%; height:100%; border:0px solid;">
			<button id="btnEnrollUSB" class="disable_bbt" onclick="enrollUSB(); return false;" disabled><%=mm.getMessage("SUMG_1169", s_user.getLocale())%></button>
		</td>
	</tr>
</table>

		</td>
		<td style="width:33.3%; height:20%; padding: 0px 2px 0px 2px;">
			<!-- <button style="width:100%; height:100%;"></button> -->
			
			<table id="bottombt">
				<tr>
					<td align="center" style="width:100%; height:100%; border:0px solid;">
			<button id="btnCollectUSB" class="disable_bbt" onclick="collectUSB(); return false;" disabled><%=mm.getMessage("SUMG_1170", s_user.getLocale())%></button>
				</td>
	</tr>
</table>

		</td>
		<td style="width:33.3%; height:20%; padding: 0px 0px 0px 4px;">
			<!-- <button style="width:100%; height:100%;"></button> -->
			
			<table id="bottombt">
				<tr>
					<td align="center" >			
			<button id="btnDelUSB" class="disable_bbt_red" onclick="delUSB(); return false;" disabled><%=mm.getMessage("SUMG_1171", s_user.getLocale())%></button>
				</td>
	</tr>
</table>
			
			
		</td>
	</tr>

</table>

</form>

</body>
</html>