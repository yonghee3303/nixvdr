<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String contextPath =request.getContextPath();
String formid = request.getParameter("formid");
String gridid = request.getParameter("gridid");
%>

<html>
<head>

<title>정책세트 정책적용세트</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%-- <%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %> --%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<style type="text/css">
	.centerTD{
		border-spacing: 25px;
	}
	.centerTD td{
		padding: 5px;
	}
	.bottomLine {
		border-width: 0px 0px 1px 0px;
		border-style: solid;
		border-color: rgb(232, 232, 232);
	}
	.margin5 {
		margin: 5px 5px 5px 5px;
	}
	.w13p {
		width: 13%;
	}
	.w15p {
		width: 15%;
	}
	.w20p {
		width: 20%;
	}
	.w22p {
		width: 22%;
	}
	.w30p {
		width: 30%;
	}
	.w35p {
		width: 35%;
	}
	.w50p {
		width: 50%;
	}
	.w100p {
		width: 100%;
	}
	.w50 {
		width:50px;
	}
	.w94 {
		width:94px;
	}
	.w200 {
		width:200px;
	}
	.textRight {
		text-align: right;
		padding-right: 15px;
	}
	.paddingTop3 {
		padding-top: 3px !important;
	}
	.paddingLeft29 {
		padding-left: 29px !important;
	}
</style>

<script>

var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

//등록화면
var addRowId		= 0;
var ed_flag 		= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/su/policySetMgmt.do";

var loading = false;


function init() {
	loading = true;
	setFormDraw();
	doPolicyGrpCombo();
	doLoad();
}

function doLoad() {
	if(parent.jPolicyData != null) {
		var jdata = parent.jPolicyData;
		document.form.comboPolicyGrp.value = jdata.SU_POLICY_GRP_CD;
		doPolicyGrpChange();	
	}
}

function doSave() {
	return true;
	//var jdata = parent.jPolicyData;
	//select의 onchange 이벤트에 의해 doPolicyGrpChange() 함수로 parent.jPolicyData에 실시간 저장중 
	
	//alert(oj.responseText);
	//refresh(oj);
	//공통 컬럼
	/* 
	//jPolicyData = JSON.parse(oj.responseText);
	jPolicyData.SU_POLICY_BASIC_CD;		//기본정책코드
	jPolicyData.IS_EXPIRE_DT;			//만료일자지정 여부
	jPolicyData.EXPIRE_YMD;				//만료기간
	jPolicyData.IS_EXPIRE_INIT;			//기간만료시 자동삭제
	jPolicyData.IS_EXPIRE_STOP;			//기간만료시 사용중지
	jPolicyData.IS_READCOUNT;			//실행회수 지정여부
	jPolicyData.READCOUNT;				//실행회수
	jPolicyData.IS_READ_INIT;			//실행회수초과시 자동삭제
	jPolicyData.IS_READ_STOP;			//실행회수초과시 사용중지
	jPolicyData.IS_WAIT_CLOSE;			//실행후 지정시간 경과시 자동종료 여부
	jPolicyData.WAIT_TIME;				//지정시간
	jPolicyData.IS_LOGIN_FAIL_INIT;		//로그인 실패시 자동삭제
	jPolicyData.IS_LOGIN_FAIL_STOP;		//로그인 실패시 사용중지
	jPolicyData.IS_LOGIN_FAIL_COUNT;	//로그인실패 회수(로그인 실패 사용 유무컬럼은 존재하지 않는다. 따라서 0일 경우 실패 제한 없음 처리)
	jPolicyData.USB_ENCODE_CL_CD;		//보안방법 구분코드 (현재 사용안함 NULL)
	jPolicyData.USB_IMAGE_SIZE;			//지정용량		  (현재 사용안함 NULL)
		
	//SU_BASIC_POLICY 고유 컬럼
	jPolicyData.RUN_MAC_YN;				//지정PC 적용여부
	jPolicyData.USER_ATUH_YN;			//사용자인증여부
	
	//SU_POLICY 고유 컬럼
	jPolicyData.MAC_ADDR; 				//실행 MAC주소 
	jPolicyData.USB_ID;					//USB ID
	jPolicyData.USB_CREATE_ID;			//대여ID
	jPolicyData.SU_POLICY_GRP_CD;		//정책그룹 코드
	jPolicyData.USB_AUTH_CL_CD;			//보안USB 인증구분코드 (현재 사용안함 '' 처리)
	 */
 
	 //작업 순서
	//MAC_ADDR > SU_POLICY 별도 컬럼, MAC 은 MAC_LIST를 따로 저장해야함 추후 MAC_ADDR에 통합
	//보안USB 외부사용 TRUE 일 경우 SU_RENT_MGT 의 값은 Y
	 //1. SU_RENT_MGT 등록
	 //2. SU_POLICY 등록	
}

//정책세트 combo 동적으로 생성
function doPolicyGrpCombo() {
	var url = "/mg/sugrp.do?mod=getComboPolicyGrpHtml";
	sendRequest(doPolicyGrpComboResult, "", 'POST', url , false, false);
}

function doPolicyGrpComboResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyGrpCombo = document.getElementById("comboPolicyGrp");
	policyGrpCombo.innerHTML = oj.responseText;//jData;
	//document.form.SU_POLICY_GRP_CD.value = document.form.comboPolicyGrp.value; 
}
function doPolicyGrpChange() {
	parent.jPolicyData.SU_POLICY_GRP_CD = document.form.comboPolicyGrp.value;		//정책그룹 코드
	doQuery();
}

/* function checkDisabled(jsonId, selId, status) {
	if(jsonId != null) {
		document.getElementById(jsonId).disabled = status;
	}
	if(selId != null) {
		document.form[selId].disabled = status;
	}
} */

function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
}

function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

function doOnCellChange(stage,rowId,cellInd) 
{	
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		} else {
		}
		return true;
	}
	return false;
}

function doQuery() {
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectPolicySetList&grid_col_id="+grid_col_id+"&SU_POLICY_GRP_CD="+document.form.comboPolicyGrp.value);
	GridObj.clearAll(false);																																												
} 

//doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
//GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
//setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	if(status == "false") {
		alert(msg);
	}
	<%-- document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%>  <span class='point'> : " + 0+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> "; --%>
	return true;
}
<%-- 
//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}


function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=savePolicySet&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}



function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		//parent.refresh();		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}


function doAddRow(){
 	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
		
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SU_POLICY_GRP_CD")).setValue(document.form.comboPolicyGrp.value);

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		for(var i=0; i < grid_array.length; i++) {
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("SU_POLICY_GRP_CD")).getValue() == "") {
				GridObj.deleteRow(grid_array[i]);
			}
		}
		grid_array = getGridChangedRows(GridObj, "SELECTED");
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deletePolicySet&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}
 --%>


</script>
<style>
	
</style>
</head>
<body onload="init();">
<form name="form" method="post">
	<table class="w100p margin5 centerTD">
		<tr>
			<td>
				<span>정책 세트 선택</span>&nbsp;
				<select id="comboPolicyGrp" class="w200" onchange="doPolicyGrpChange();"><option></option></select>
			</td>
		</tr>
		<tr>
			<td>
				<!-- <div id="gridbox" name="gridbox" style="margin-left:0.5%;height:60%; width:97.75%; background-color:white;overflow:hidden"></div> -->
				<div id="gridbox" name="gridbox" style="background-color:white;overflow:hidden"></div>
			</td>
		</tr>
	</table>			
</form>
</body>
</html>