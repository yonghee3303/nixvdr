<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String userIp = request.getRemoteAddr();
String contextPath = request.getContextPath();
%>

<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<title>보안USB 정보</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>
<script>

var usbId;
var rentId;
var tabbar;
var GridObj 		= {};
var G_SERVLETURL = "<%=contextPath%>/mg/exportMgmt.do";
var winChgPassword;

function init() {
	
	GridObj = setGridDraw(GridObj);

	 GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
		usbId = GridObj.cells(rId,4).getValue();
		rentId = GridObj.cells(rId,14).getValue();
		tabbar.tabs('apply').getFrame().contentWindow.doAjax_Policy(usbId, rentId);//그리드 목록 중 더블클릭한 목록의 정책 정보를 보여준다.// 여기는 작동이 안됨... 
		tabbar.tabs('log').getFrame().contentWindow.doAjax_Log(usbId, rentId);//그리드 목록 중 더블클릭한 목록의 로그 정보를 보여준다. //여기는 제대록 작동 중..
	}); 
	
	tabbar = new dhtmlXTabBar("tabbar");
	
	tabbar.addTab("apply","<%=mm.getMessage("SUMG_1172", s_user.getLocale())%>",null,null,true);
	tabbar.addTab("log","<%=mm.getMessage("SUMG_1173", s_user.getLocale())%>");

	tabbar.tabs('apply').attachURL("<%=contextPath%>/mg/su/main/applyPolicyDetail.jsp", null, {});
	tabbar.tabs('log').attachURL("<%=contextPath%>/mg/su/mgmt/log.jsp", null, {
		formid: "SU_0103",
		gridid: "GridObj"
	});
	
	//자동 리사이즈~
	tabbar.enableAutoReSize(); 
	doQuery();
}

function grid_resize() {
	GridObj.setSizes();
}

function doQuery() {
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	var user = "<%=s_user.getId().toString()%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectExportMgmt&grid_col_id="+grid_col_id+"&RENDER_ID="+user);		
	/* GridObj.loadXML(G_SERVLETURL+"?mod=selectExportMgmt&grid_col_id="+grid_col_id); */
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}

//applyPolicyDetail페이지 applyPolicy()에서 설정한 값들을 url로 넘겨준다. 
function updatePolicy(url){
	
	GridObj.clearAll(false);
	var grid_col_id  = "<%=grid_col_id%>";
	
	GridObj.loadXML(G_SERVLETURL +"?mod=updatePolicy"+"&grid_col_id="+grid_col_id+url+"&IP_ADDR="+"<%=userIp%>"+"&USER_ID="+"<%=s_user.getId()%>");	
	
}

function searchExport(){
	
	//GridObj.clearAll(false);
	
	var fromDate = document.getElementById("calendar_input").value;
	var toDate = document.getElementById("calendar_input2").value;
	var searchTerm = document.getElementById("selectItem").value;
	var term = document.getElementById("term").value;
	
	var argu= "&FROM_YMD="+encodeURIComponent(fromDate);
	argu += "&TO_YMD="+encodeURIComponent(toDate);
	
	switch(searchTerm){
	case "USER_NM":
		argu +="&USER_NM="+ encodeURIComponent(term);
		break;
	case "DEPT_NM":
		argu +="&DEPT_NM="+ encodeURIComponent(term);
		break;
	case "POSITION_NM":
		argu +="&POSITION_NM="+ encodeURIComponent(term);
		break;
	case "USB_ID":
		argu +="&USB_ID="+ encodeURIComponent(term);
		break;
	case "USB_NM":
		argu +="&USB_NM="+ encodeURIComponent(term);
		break;
	case "RENT_STS_NM":
		argu +="&RENT_STS_NM="+ encodeURIComponent(term);
		break;	
	}
		
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectExportMgmt&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}

function resize() {
	layout.setSizes();
	
}
//수불 상태를 분실로 바꾸기 위한 action 
function updateUsbStatus(usb, rent, sts){
	if(usb=="" && rent==""){
		alert("<%=mm.getMessage("SUMG_2018", s_user.getLocale())%>");
	}else{
		GridObj.clearAll(false);
		var grid_col_id  = "<%=grid_col_id%>";
		GridObj.loadXML(G_SERVLETURL +"?mod=updateUsbStatus&USB_ID="+usb+"&USB_CREATE_ID="+rent+"&RENT_STS_CD="+sts+"&RENDER_ID="+"<%=s_user.getId().toString()%>"+"&grid_col_id="+grid_col_id);	
	}
}

</script>

<style>

div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
	
	/* Tabbar Web스킨 사용시 간격조정 */
	.dhxtabbar_base_dhx_web div.dhx_cell_tabbar div.dhx_cell_cont_tabbar {
		
		padding: 0px;
		
	}

	
	/* .board-search { width:100%; margin-bottom:10px; border:1px solid #dde1e3;} */
	.board-search { width:100%; margin-bottom:10px; border:0;}
	
	.board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:0; background:#ffffff;}
	
	/* .board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#f4f4f4 url(../images/bul_arrow3.gif) 8px 50% no-repeat;} */
	.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(../../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}
	
	
	.btnarea {
		background-color:#ffffff;
	}
	
</style>


</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:5px; overflow: auto;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
       <table class="board-search" style="width:100%; table-layout:fixed;">
       <colgroup>
		    <col width="140px"/>
		    <col width="140px"/>
		    <col width="140px"/>
		    <col width="160px"/>
		    <col style="padding-right: 17px;"/>    	    
	    </colgroup>
	       <tr>
		       	<td class="tit">
		    		<%=mm.getMessage("SUMG_1137", s_user.getLocale())%> : <%=s_user.getName().toString() %>
		       	</td>
		       	<td class="tit">
		    		<%=mm.getMessage("COMG_1020", s_user.getLocale())%> : <%=s_user.getId().toString() %>
		       	</td>		       	
		       	<td class="tit">
		    		<%=mm.getMessage("COMG_1062", s_user.getLocale())%> : <%=s_user.getProperty("FUNCTION_NM").toString() %>
		       	</td>		       	
		       	<td class="tit">
		    		<%=mm.getMessage("COMG_1063", s_user.getLocale())%> : <%=s_user.getProperty("DEPT_NM").toString()%>
		       	</td>
		       	<td>
		       	</td>
	       </tr>
	       
		</table>

		<!-- 임시 공간 -->
		<table class="board-search" style="width:100%;">    
        	<tr>
		       	<td class="tit">
		       		<%=mm.getMessage("SUMG_1174", s_user.getLocale())%>
		       	</td>
	       </tr>
        </table>
        
	</td>
</tr>
</table>
</form>
<div id="gridbox" style="width:100%; height:20%; background-color:white; overflow:hidden;"></div><!-- 반출 목록 그리드 -->
<br>
<div id="tabbar" style="width:100%; height:550px;"></div>
</body>
</html>