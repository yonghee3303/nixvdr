<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String formid = (String)request.getParameter("formid");
String usbType =(String)request.getParameter("usbTypeCd");
%>


<html>
<head>

<title>장치 선택</title>


<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<!-- 그리드 생성시 필요 -->
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<!-- WebSocket 적용 -->
<script type="text/javascript" src="../../../ext/js/websocket.js"></script>

<script>


var GridObj 		= {};

//드라이브명 배열생성
var driveList = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

var selDrive = "";
var selModel = "";
var selUsbSize = "";
var selUsbType="U";

function init() {
	setGridDraw();
	
	WebSocketConnect();
}

function setGridDraw(){

	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	GridObj.setHeader("<%=mm.getMessage("SUMG_1154", s_user.getLocale())%>,<%=mm.getMessage("SUMG_1155", s_user.getLocale())%>,<%=mm.getMessage("SUMG_1156", s_user.getLocale())%>,외장매체타입");
	GridObj.setInitWidths("80,150,100,130");
	GridObj.setColAlign("center,center,center,center");
	GridObj.setColumnIds("DRIVE,MODEL,CAPACITY,USB_TYPE");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str,str");
	GridObj.setColTypes("ro,ro,ro,coro");
	
	 doRequestUsingPOST_dhtmlxGrid(GridObj,"A0505", "USB_TYPE", "" ); 
	
	GridObj.enableMultiline(true); //수정
	GridObj.enableMultiselect(false); //수정
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);
	GridObj.enableEditEvents(true,false,false);
	GridObj.init();

	// 따로 함수로 만들어 동작시키면 동작하지 않는다...
	// 배열을 돌며 USB인지 체크하여 USB일경우 Grid에 USB정보를 표시한다
	for(var i=0;i<driveList.length;i++){
	
		// USB드라이브 인지 체크
		// var isUSB = document.USBInfo.IsUSBDrive(driveList[i]);
		
		// var req = {"action":"GetUSBInfo","DRIVE":""}
		

		var json = new Object();
		json.action = "IsUSBDrive";
		json.DRIVE =  driveList[i];
		
		websocket.send(JSON.stringify(json));

		if(isUSB){
			
			//alert(driveList[i] + "드라이브는 USB 드라이브");
			
			//usb정보 얻어옴
			var usbInfo = document.USBInfo.GetUSBInfo(driveList[i]);
			
			// 리턴값의 구분자를 삭제
			var array = usbInfo.split('{}[]');
			
			var newId = (new Date()).valueOf();
			GridObj.addRow(newId,"");
			GridObj.cells(newId,GridObj.getColIndexById("DRIVE")).setValue(driveList[i]);
			GridObj.cells(newId,GridObj.getColIndexById("MODEL")).setValue(array[2]);
			GridObj.cells(newId,GridObj.getColIndexById("CAPACITY")).setValue((array[5]/1024).toFixed(2) +" GB");
			GridObj.cells(newId,GridObj.getColIndexById("USB_TYPE")).setValue("<%=usbType%>");
		}
		
	}
}

function doOnRowSelected(rowId,cellInd){
	selDrive = GridObj.cells(rowId,GridObj.getColIndexById("DRIVE")).getValue();
	selModel = GridObj.cells(rowId, GridObj.getColIndexById("MODEL")).getValue();
	selUsbSize = GridObj.cells(rowId,GridObj.getColIndexById("CAPACITY")).getValue();
	selUsbType = GridObj.cells(rowId, GridObj.getColIndexById("USB_TYPE")).getValue();

	if(selDrive==""){
		alert("<%=mm.getMessage("SUMG_2015", s_user.getLocale())%>");
	}else if(selUsbType == ""){
		alert("외장매체 타입을 선택해주세요.");
	}else{
		parent.deliver(selDrive, selModel, selUsbSize,selUsbType);
	}
}
function resize() {
	GridObj.setSizes();
}

function refresh() {
	location.replace("selectUSB.jsp");
}

function onOpen(evt){
    //called as soon as a connection is opened
    // 성공을 확인 해야함.
} // end onOpen

function onClose(evt){
    //called when connection is severed
} // end onClose;

function onMessage(evt){
    //called on receipt of message
    var jData;
	try {
		jData = JSON.parse(evt.data);	
	} catch(err) {
		return;
	}
    
} // end onMessage

function onError(evt){
    //called on error
    // 실패시 사용자 Alert
} // end onError
  
  

</script>



<style>

	#gridbox {
        width: 100%;
        height: 70%;
        margin: 0px;
        padding: 0px;
    }
    
    .board-search { width:100%; margin-bottom:5px; border:0;}
    
    .board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:0; background:#ffffff;}

	.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(../../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}
	
	.btnarea {
		background-color:#ffffff;
	}
	
</style>




</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:15px; overflow: hidden;">

<!--
<OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT>
  -->

<form>
<table class="board-search" style="width:100%; padding:0px,0px,50px,0px;">
	<tr>
		<td class="tit">
			<%=mm.getMessage("SUMG_1004", s_user.getLocale())%>
		</td>
		<td>
			<div class="btnarea">
				<a href="JavaScript:refresh();" class="btn"><span><%=mm.getMessage("SUMG_1157", s_user.getLocale())%></span></a>
			</div>
		</td>
	</tr>
</table>

<div id="gridbox" style="position: relative;"></div>

<%-- <table style="width:100%;">
	<tr>
		<td>
			<div class="btnarea" style="padding:10px,0px,0px,0px;">
	            	<a href="JavaScript:selectUSB();" class="btn"><span><%=mm.getMessage("COMG_1022", s_user.getLocale())%></span></a>
	            	<a href="JavaScript:close();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
	    	</div>
		</td>
	</tr>
</table> --%>
</form>
</body>
</html>