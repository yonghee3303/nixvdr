<%@page import="java.net.URLEncoder"%>
<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String contextPath = request.getContextPath();
%>


<html>
<head>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>

<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var G_SERVLETURL = "/ni/proj.do";

function setFormDraw() {
	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setHeader("프로젝트,소싱일자,확정인력,담당자,PN");
	GridObj.setInitWidths("150,100,70,80,0");
	GridObj.setColAlign("center,center,center,center,center");
	GridObj.setColumnIds("PROJ_NM,ORDER_YMD,REQU_CNT,PJ_USER_NM,PROJ_NO");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str,str,str");
	GridObj.setColTypes("ro,ro,ro,ro,ro");
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);

	GridObj.init();
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
var header_name = GridObj.getColumnId(cellInd);

}

function doOnRowSelect(rowId,cellInd) {
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	
	var grid_col_id = "PROJ_NM,ORDER_YMD,REQU_CNT,PJ_USER_NM,PROJ_NO";

	GridObj.loadXML(G_SERVLETURL+"?mod=selectMainProProj&grid_col_id="+grid_col_id);																						
	GridObj.clearAll(false);
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}

	return false;
}


function refresh() {
	location.replace("ni_MainProProj.jsp");
}

function init() {
	setFormDraw();
}
function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}

</script>
</head>
<body onload="init(); loadCombobox();">
<form name="form" method="post">
<table width="100%" border="0">
</table>
</form>
<div id="gridbox" name="gridbox" width="105%" height="104%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=300"/>
</jsp:include>
</body>
</html>
