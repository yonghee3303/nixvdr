<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>
var layout;

var cell;

function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "1C",
	    skin: "dhx_web"
	});
	
	
	cell = layout.cells('a');
	//cell.setText("반출 보안USB 관리");
	/* cell.setText('<table style="width:98%; font-family:Tahoma; font-size:12px;"><tr><td><font color="white"><b>반출 보안USB 관리</b></font></td><td align="right"><font color="yellow"><a onclick="goMain()">Go Main!</a></font></td></tr></table>'); */
	cell.setText("<span id='caption'>보안USB 정보</span>");
	
	//cell1.setWidth(1500);
	//cell1.setHeight(125);
	//a.hideHeader();
	//a.setHeight(120);
	cell.attachURL("userDetail.jsp",null,  {
		formid: "SU_0102",
		gridid: "GridObj"
	});

}


function resize() {
	layout.setSizes();
}


</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        overflow: hidden;
    }

#caption {
	white-space: nowrap;
	cursor: default;
	line-height: 31px;
	font-family: Tahoma;
	font-size: 12px;
	color: #ffffff;
	font-weight: bold;
	width: 97%;
}

</style>

</head>

<body onload="init();" onresize="resize();" style="overflow:hidden;">

<div id="layoutObj" style="position: relative; z-index:50;"></div>

</body>
</html>