<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String usbId = (String)request.getParameter("USB_ID");
String drive = (String)request.getParameter("DRIVE");
String driveSize = (String)request.getParameter("DRIVE_SIZE");
String linkId = (String)request.getParameter("LINK_ID");
String usbType = (String)request.getParameter("USB_TYPE");
String userIp = request.getRemoteAddr(); //접속한 user의 ip를 얻어온다.
String contextPath =request.getContextPath();
%>

<html>
<head>

<title>보안USB 생성창</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/websocket.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<style type="text/css">
	.w200 {
		width:200px;
	}
</style>

<script>

var G_SERVLETURL = "<%=contextPath%>/mg/enrollUSB.do";

var usbId = "<%=usbId%>";
var drive = "<%=drive%>";
var linkId = "<%=linkId%>";
var usbType = "<%=usbType%>";
var calendar;
var winSelectUser;

var renderId = ""; //대여자 id
var returnDt = "";	//반납 예정일
var isModified = "0"; //사용자가 정책 수정권한이 부여되는지
var initMac;
var userAuthYn;
var runMacYn;
////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
var dhxWins;
var prg_win;
var jPolicyData;
var jCreateUsb;

function BwindowWidth() {
	 if (window.innerWidth) {
		 return window.innerWidth;
	 } else if (document.body && document.body.offsetWidth) {
	 	return document.body.offsetWidth;
	 } else {
		 return 0;
	}
}	
function BwindowHeight() {
	 if (window.innerHeight) {
		 return window.innerHeight;
	} else if (document.body && document.body.offsetHeight) {
		 return document.body.offsetHeight;
	} else {
		 return 0;
	}
}
function doQueryDuring()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
}
////////////////////////////////////////////////////프로그래스바 처리때문에 
function close(){
	
	if(winSelectUser != null){
		winSelectUser.close();
	}
	window.close();
}


function init(){
	
	if(dhxWins == null) {
    	dhxWins = new dhtmlXWindows();
    	dhxWins.setImagePath("<%=contextPath%>/dthmlx/dhtmlxWindows/codebase/imgs/");
    }
	if(prg_win == null) {
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;
		
		prg_win = dhxWins.createWindow("prg_win", top, left, 180, 110);
		prg_win.setText("Please wait for a moment.");
		prg_win.button("close").hide();
		prg_win.button("minmax1").hide();
		prg_win.button("minmax2").hide();
		prg_win.button("park").hide();
		dhxWins.window("prg_win").setModal(false);
		prg_win.attachURL("../../../ext/include/progress_ing.htm");
		dhxWins.window("prg_win").hide();
	}
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	calendar = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	
	calendar.hideTime();
	
	
	WebSocketConnect();
	checkUsb();
	doPolicyGrpCombo();
	doBasicPolicyCombo();
	/* doPolicyCombo();
	suGrpChange(); */
}

//USB 보유/반출 상태 확인
function checkUsb() {
	//USBID 값으로 DB에서 보유/반출 상태 확인
	var url = "<%=contextPath%>/mg/enrollUSB.do?mod=checkUSB&USB_ID=" + usbId +"&usbType="+usbType;
	sendRequest(checkUsbEnd, " " ,"POST", url , false, false);
}

function checkUsbEnd(oj){
	var jData = JSON.parse(oj.responseText);

	 var status = jData.haveUsb;
	 // 버튼 활성/비활성화 처리
	 if("false"==status){ //반출 USB
		alert("이미 반출된 USB입니다. 회수 후 반출하시기 바랍니다.");
	 	doCancel();
	 }
}

//사용자선택 팝업창 오픈
function selectUser(){
	winSelectUser = window.open("selectUser.jsp?formid=SU_0107&gridid=GridObj",'','width=530px,height=320px,scrollbars=no,resizable=no');
}
function selectUser_End(userId, userNm, deptNm, position){
	//form.RENDER_ID.value=userId;
	jPolicyData.RENDER_ID = userId;
	document.getElementById("renderInfo").innerText=deptNm+" / "+userNm+" / "+position;
	winSelectUser.close();
}
function closeWinSelectUser(){
	winSelectUser.close();
}
//기본정책 combo 동적으로 생성
function doBasicPolicyCombo() {
	var url = "/mg/sugrp.do?mod=getComboBasicPolicyGrpHtml";
	sendRequest(doBasicPolicyComboResult, "", 'POST', url , false, false);
}

function doBasicPolicyComboResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var basicPolicyCombo = document.getElementById("comboBasicPolicyGrp");
	basicPolicyCombo.innerHTML = oj.responseText;//jData;
	document.form.comboBasicPolicyGrp.value = "<%=pm.getString("component.usb.export.default.policyCd")%>"; 
	document.form.SU_POLICY_BASIC_CD.value ="<%=pm.getString("component.usb.export.default.policyCd")%>";
	doBasicPolicyChange();
}
function doBasicPolicyChange() {
	var policyNo = document.form.SU_POLICY_BASIC_CD.value;
	var url ="/mg/sugrp.do?mod=getBasicPolicy&SU_POLICY_BASIC_CD="+policyNo;
	sendRequest(doBasicPolicyChangeEnd, "", 'POST', url , false, false);
}

function doBasicPolicyChangeEnd(oj){
	
	jPolicyData = JSON.parse(oj.responseText);
	jPolicyData.SU_POLICY_GRP_CD = document.form.comboPolicyGrp.value; 
	//refresh();
	//기본정책 불로오기 후 대기리스트의 정책으로 overwrite
	doSelectStandbyList();
	
	//공통 컬럼
	/* 
	//jPolicyData = JSON.parse(oj.responseText);
	jPolicyData.SU_POLICY_BASIC_CD;		//기본정책코드
	jPolicyData.IS_EXPIRE_DT;			//만료일자지정 여부
	jPolicyData.EXPIRE_YMD;				//만료기간
	jPolicyData.IS_EXPIRE_INIT;			//기간만료시 자동삭제
	jPolicyData.IS_EXPIRE_STOP;			//기간만료시 사용중지
	jPolicyData.IS_READCOUNT;			//실행회수 지정여부
	jPolicyData.READCOUNT;				//실행회수
	jPolicyData.IS_READ_INIT;			//실행회수초과시 자동삭제
	jPolicyData.IS_READ_STOP;			//실행회수초과시 사용중지
	jPolicyData.IS_WAIT_CLOSE;			//실행후 지정시간 경과시 자동종료 여부
	jPolicyData.WAIT_TIME;				//지정시간
	jPolicyData.IS_LOGIN_FAIL_INIT;		//로그인 실패시 자동삭제
	jPolicyData.IS_LOGIN_FAIL_STOP;		//로그인 실패시 사용중지
	jPolicyData.IS_LOGIN_FAIL_COUNT;	//로그인실패 회수(로그인 실패 사용 유무컬럼은 존재하지 않는다. 따라서 0일 경우 실패 제한 없음 처리)
	jPolicyData.USB_ENCODE_CL_CD;		//보안방법 구분코드 (현재 사용안함 NULL)
	jPolicyData.USB_IMAGE_SIZE;			//지정용량		  (현재 사용안함 NULL)
		
	//SU_BASIC_POLICY 고유 컬럼
	jPolicyData.RUN_MAC_YN;				//지정PC 적용여부
	jPolicyData.USER_ATUH_YN;			//사용자인증여부
	
	//SU_POLICY 고유 컬럼
	jPolicyData.MAC_ADDR; 				//실행 MAC주소 
	jPolicyData.USB_ID;					//USB ID
	jPolicyData.USB_CREATE_ID;			//대여ID
	jPolicyData.SU_POLICY_GRP_CD;		//정책그룹 코드
	jPolicyData.USB_AUTH_CL_CD;			//보안USB 인증구분코드 (현재 사용안함 '' 처리)
	 */
 
	 //작업 순서
	//MAC_ADDR > SU_POLICY 별도 컬럼, MAC 은 MAC_LIST를 따로 저장해야함 추후 MAC_ADDR에 통합
	//보안USB 외부사용 TRUE 일 경우 SU_RENT_MGT 의 값은 Y
	 //1. SU_RENT_MGT 등록
	 //2. SU_POLICY 등록	
}

//대기목록 특정값 조회
function doSelectStandbyList() {
	var url ="<%=contextPath%>/mg/standbyList.do?mod=selectStandbyObj&LINK_ID="+linkId;
	sendRequest(doSelectStandbyListEnd, "", 'POST', url , false, false);
}
function doSelectStandbyListEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	
	if(usbType == jData.USB_TYPE_CD) {
		document.getElementById("USB_TYPE").innerHTML = jData.USB_TYPE;
	} else {
		alert("장치 타입이 일치하지 않습니다.");
		doCancel();
	}
	
	jPolicyData.RENDER_ID = jData.RENDER_ID;
	jPolicyData.EXPIRE_YMD = jData.EXPIRE_YMD;
	if(jData.EXPIRE_YMD > 0) {
		jPolicyData.IS_EXPIRE_DT = "Y";			//만료일자지정 여부
	}
	
	document.getElementById("RENDER_NM").innerHTML = jData.RENDER_NM; 
	document.getElementById("RENDER_ID").innerHTML = jData.RENDER_ID;
	document.getElementById("DEPT_NM").innerHTML = jData.DEPT_NM;
	document.getElementById("RENT_E_DT").innerHTML = jData.RENT_E_DT;
	//document.getElementById("EXPIRE_YMD").innerHTML = jData.EXPIRE_YMD;
	document.getElementById("RENT_DESC").innerHTML = jData.RENT_DESC;
	
	//MAC LIST
	var macList = jData.RUN_MAC_ADDR.split("|");
	if(macList.length > 0) {
		jPolicyData.RUN_MAC_YN = "Y";
		for(var i=0; i<macList.length; i++) {
			macAdd(macList[i]);
		}	
	} else {
		jPolicyData.RUN_MAC_YN = "N";
	}
	
	refresh();
}

function refresh() {
	document.form.comboPolicyGrp.value = jPolicyData.SU_POLICY_GRP_CD;	//정책세트
	
	if(jPolicyData.IS_READCOUNT == "Y") {	//실행회수
		document.getElementById("READCOUNT").innerHTML = jPolicyData.READCOUNT + " 회";			
	} else {
		document.getElementById("READCOUNT").innerHTML = "제한 없음";
	}
	/* if(jPolicyData.IS_EXPIRE_DT == "Y") {	//만료기간
		document.getElementById("EXPIRE_YMD").innerHTML = jPolicyData.EXPIRE_YMD + " 일";	
	} else {
		document.getElementById("EXPIRE_YMD").innerHTML = "제한 없음"
	} */
	
	document.form.RUN_MAC_YN.value = jPolicyData.RUN_MAC_YN;		//지정PC 적용여부
	setRadioValue("RUN_MAC_YN");

	if(jPolicyData.RUN_MAC_YN == "Y") {
		checkMacDisabled(false);	
	} else {
		checkMacDisabled(true);
	}
	
	setRadioValue("USER_ATUH_YN");	//보안USB 외부사용					
}

//정책세트 combo 동적으로 생성
function doPolicyGrpCombo() {
	var url = "/mg/sugrp.do?mod=getComboPolicyGrpHtml";
	sendRequest(doPolicyGrpComboResult, "", 'POST', url , false, false);
}

function doPolicyGrpComboResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyGrpCombo = document.getElementById("comboPolicyGrp");
	policyGrpCombo.innerHTML = oj.responseText;//jData;
	document.form.SU_POLICY_GRP_CD.value = document.form.comboPolicyGrp.value; 
	doPolicyGrpChange();
}
function doPolicyGrpChange() {
	if(jPolicyData) {
		jPolicyData.SU_POLICY_GRP_CD = document.form.comboPolicyGrp.value;		//정책그룹 코드
	}
	/* var policyNo = document.form.policyCode.value;
	var url ="/mg/sugrp.do?mod=getPolicySuGrp&SU_POLICY_GRP_CD="+policyNo;
	sendRequest(suGrpChangeEnd, "", 'POST', url , false, false); */
}

function checkMacDisabled(status) {
	if(status == true) {
		jPolicyData.RUN_MAC_YN = "N";
		document.getElementById("MAC_INPUT").disabled = true;
		document.getElementById("MAC_ADD").disabled = true;
		document.getElementById("MAC_LIST").disabled = true;
		document.getElementById("MAC_DEL").disabled = true;
	} else {
		jPolicyData.RUN_MAC_YN = "Y";
		document.getElementById("MAC_INPUT").disabled = false;
		document.getElementById("MAC_ADD").disabled = false;
		document.getElementById("MAC_LIST").disabled = false;
		document.getElementById("MAC_DEL").disabled = false;
	}
}

function macAdd(id){
	var macInput = document.getElementById("MAC_INPUT");
	var macAddr;
	
	if(id != null) {
		macAddr = id;
	} else {
		macAddr= macInput.value;	
	}
	/* var submac= new Array(form.subMAC_1,form.subMAC_2,form.subMAC_3,form.subMAC_4,form.subMAC_5,form.subMAC_6);
	var macAddr="";
	
	for(var i=0; i<submac.length; i++){
		if(submac[i].value==""){
			alert("MAC 주소를 정확하게 입력하세요.");
			return;
		}else{
			macAddr+=submac[i].value;
		}
	} */
	
	macAddr = macAddr.replace(/-/ig, "");
	if(macAddr.length !=12){
		alert("MAC 주소를 정확하게 입력하세요.");
		return;
	}
	var macHtml = form.MAC_LIST.innerHTML;//document.getElementById("MAC_LIST").innerHTML;
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' class='w200' multiple>";
	if(form.MAC_LIST.value==""){
		initMac = macHtml;
		tempHtml += "<option value='"+macAddr+"' selected>"+macAddr+"</option>";
	}else{
		tempHtml += macHtml+"<option value='"+macAddr+"'>"+macAddr+"</option>";	
	}
	var tempMac = tempHtml.split(initMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	tempHtml +=initMac+"</select>";		
	removeAllChild(document.getElementById("MAC_TD"));
	document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	
	/* for(var i=0; i<submac.length; i++){
		submac[i].value="";
	} */
	macInput.value = "";
	
}
function macDel(){
	var macList = document.getElementById("MAC_LIST");
	var val = macList.value;
	var tempHtml = macList.innerHTML;
	var tempMac = new Array();
	var cnt = 0;
	
	while(true) {
		var index = tempHtml.indexOf("</option>");
		if(index == -1) {
			break;
		} else {
			index += 9;
		}
		tempMac[cnt] = tempHtml.substring(0, index);
		tempHtml = tempHtml.substring(index);
		
		cnt++;
	}
	
	tempHtml = "";
	
	for(var i=0; i<tempMac.length; i++){
		if(tempMac[i].indexOf('value="'+val+'"') != -1) {
			continue;
		}
		tempHtml += tempMac[i];
	}
	
	macList.innerHTML = tempHtml;
}

function removeAllChild(nodeTemp){
	while(nodeTemp.hasChildNodes()){
		nodeTemp.removeChild(nodeTemp.lastChild);
	}
}

function policySetPopup(){
	
	centerWin("/mg/su/main/policySetPopup.jsp", 800, 550, "no", "popup");
}

function centerWin(url, w, h, scroall, name){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	var nm;
	if(name != "" || name != null) {
		nm = "win";
	} else {
		nm = name;
	}
	window.open(url, nm, winOpt);
}

//document.form.radioName.value 를 통한 value 값 추출 및 세팅이 안되고 있음
//라디오박스 값 추출
function getRadioValue(id) {
	var radio = document.getElementsByName(id);
	for(var i=0; i<radio.length; i++) {
		if(radio[i].checked) {
			jPolicyData[id] = radio[i].value;	
		}
	}
}
//라디오박스 값 세팅
function setRadioValue(id) {
	if(jPolicyData[id] == "Y") {
		document.getElementById(id+"_Y").checked = true;
	} else {
		document.getElementById(id+"_N").checked = true;
	}
}

function doSave() {
	/* 
	//공통 컬럼
		- jPolicyData.SU_POLICY_BASIC_CD;		//기본정책코드
		- jPolicyData.IS_EXPIRE_DT;			//만료일자지정 여부
		- jPolicyData.EXPIRE_YMD;				//만료기간
		- jPolicyData.IS_EXPIRE_INIT;			//기간만료시 자동삭제
		- jPolicyData.IS_EXPIRE_STOP;			//기간만료시 사용중지
		- jPolicyData.IS_READCOUNT;			//실행회수 지정여부
		- jPolicyData.READCOUNT;				//실행회수
		- jPolicyData.IS_READ_INIT;			//실행회수초과시 자동삭제
		- jPolicyData.IS_READ_STOP;			//실행회수초과시 사용중지
		- jPolicyData.IS_WAIT_CLOSE;			//실행후 지정시간 경과시 자동종료 여부
		- jPolicyData.WAIT_TIME;				//지정시간
		- jPolicyData.IS_LOGIN_FAIL_INIT;		//로그인 실패시 자동삭제
		- jPolicyData.IS_LOGIN_FAIL_STOP;		//로그인 실패시 사용중지
		- jPolicyData.IS_LOGIN_FAIL_COUNT;	//로그인실패 회수(로그인 실패 사용 유무컬럼은 존재하지 않는다. 따라서 0일 경우 실패 제한 없음 처리)
		- jPolicyData.USB_ENCODE_CL_CD;		//보안방법 구분코드 (현재 사용안함 NULL)
		- jPolicyData.USB_IMAGE_SIZE;			//지정용량		  (현재 사용안함 NULL)
		
	//SU_BASIC_POLICY 고유 컬럼
		- jPolicyData.RUN_MAC_YN;				//지정PC 적용여부
		- jPolicyData.USER_ATUH_YN;			//사용자인증여부
	
	//SU_POLICY 고유 컬럼
		- jPolicyData.SU_POLICY_GRP_CD;		//정책그룹 코드
			- jPolicyData.MAC_ADDR;	//MAC_LIST로 추출		//실행 MAC주소 
			- jPolicyData.USB_ID;		//SU_RENT_MGT 값을 넘겨받음
	jPolicyData.USB_AUTH_CL_CD;			//보안USB 인증구분코드 (현재 사용안함 '' 처리)
	
	
	
	//SU_RENT_MGT 컬럼
	USB_CREATE_ID		//SU_RENT_MGT 생성 서비스에서 yyyymmddhh24mi+난수6로 생성
	USB_NM = jPolicyData.USB_NM
		- jPolicyData.RENDER_ID
	RENT_STS_CD
	MOD_STOP
	USER_AUTH_YN = 'N'
	RENT_YMD = jPolicyData.EXPIRE_YMD;
	RETURN_YMD = jPolicyData.EXPIRE_YMD;
	PASSWD_W = "pm.getString("component.usb.initPass.default")"; //초기 비밀번호, USB 첫 인증 실행 시 보안USB에서 비밀번호를 변경하라는 입력창이 뜬다.
	PASSWD_R = ""
	IS_MODIFIED_USER_AUTH = "N"
	LOGIN_MSG = ""
	IS_APPROVAL = jPolicyData.USER_ATUH_YN;			//USER_ATUH_YN 값으로 구분
	
	 */
	 
	doQueryDuring();
	
	if(jPolicyData.RENDER_ID == "" || jPolicyData.RENDER_ID == null) {
		alert("대여 사용자를 입력해주세요.");
		doQueryModalEnd();
		return; 
	}
	if(jPolicyData.RUN_MAC_YN == "Y") {
		var macList = document.getElementById("MAC_LIST");
		var macAddr = "";
		for(var i=0; i<macList.length; i++) {
			if(macList[i].value != "") {
				macAddr += macList[i].value + "|";  
			}
		}
		if(macAddr == "") {
			alert("사용가능한 MAC 주소를 입력해주세요.");
			doQueryModalEnd();
			return;
		} else {
			jPolicyData.MAC_ADDR = macAddr.substring(0, macAddr.length-1);
		}
	}
	jPolicyData.USB_ID = usbId;
	jPolicyData.DRIVE = drive;
	jPolicyData.IP_ADDR = "<%=userIp%>";
	getRadioValue("USER_ATUH_YN");
	//jPolicyData.IS_APPROVAL = jPolicyData.USER_ATUH_YN;
	
	var url = G_SERVLETURL + "?mod=createSecureUSB&policy=" + JSON.stringify(jPolicyData);
	sendRequest(doSaveEnd, " " ,"POST", url , false, false);
}

function doSaveEnd(oj) {
	//alert("1: " + oj.responseText);
	jCreateUsb = JSON.parse(oj.responseText);
	//alert("2 : " + JSON.stringify(oj.responseText));
	//sendWsMessage(JSON.stringify(oj.responseText));
	sendWsMessage(oj.responseText);
	/* var usbInfo = new Object();
	usbInfo.action = "IsUSBDrive";
	usbInfo.DRIVE = driveList[i]; */
	
	
	<%-- document.USBInfo.CreateSecureUSB(oj.responseText);
	
	var url = G_SERVLETURL + "?mod=savePolicy&RENDER_ID="+renderId; 
		url+= "&RETURN_YMD="+returnDt ;
		url+= "&IS_MODIFIED_USER_AUTH=" + isModified;
		url+= "&policy=" + oj.responseText;
		url+= "&USER_ID="+"<%=s_user.getId() %>";
		url+= "&IP_ADDR="+"<%=userIp%>";
		url+= "&USER_AUTH_YN="+userAuthYn;
		url+= "&RUN_MAC_YN="+runMacYn;
		
	sendRequest(savePolicyEnd, " " ,"POST", url , false, false);
	
	opener.closeWinMappingUser(); --%>
}

function savePolicyEnd() {

	doQueryModalEnd();
	alert("<%=mm.getMessage("SUMG_1003", s_user.getLocale())%>");
	
	var export_ifr = parent.opener.parent.subCell6.getFrame();//반출 refresh
	export_ifr.contentWindow.refresh();
	
	var checkList_ifr = parent.opener.parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	
	var summary_ifr = parent.opener.parent.subCell3.getFrame();
	summary_ifr.contentWindow.createUSBEnd();
	
	var import_ifr = parent.opener.parent.subCell4.getFrame();//보유 refresh
	import_ifr.contentWindow.refresh();
	
	<%	if("Y".equals(pm.getString("component.usb.standbyList.useYn"))){	%>
	var standby_ifr = parent.opener.parent.subCell5.getFrame();
	standby_ifr.contentWindow.refresh();
	<%	}	%>

	export_ifr.contentWindow.selectedUSB(usbId);

	doCancel();
}

function doCancel() {
	parent.window.close();
}


//Websocket 이벤트 핸들러
function onOpen(evt){
    //called as soon as a connection is opened
    isConnection = true;
} // end onOpen

function onClose(evt){
    //called when connection is severed
    isConnection = false;
} // end onClose;

function onMessage(evt){
    //called on receipt of message
    var jData;
	//JSON 형태가 아닌 반환 데이터 패스
	try {
		jData = JSON.parse(evt.data);	
	} catch(err) {
		return;
	}
	
	//JSON 타입 반환값에서 오류 데이터르를 제외한 정상값 처리
    if(jData.errmsg.indexOf("Secure USB Create Success") != -1) {
    	//깡통정책(jCreateUsb)을 대여정책(jPolicyData)으로 교체
		if(jData.errcode == "0") {
			
			//공통 컬럼
			jCreateUsb.SU_POLICY.SU_POLICY_BASIC_CD = jPolicyData.SU_POLICY_BASIC_CD;	//기본정책코드
			jCreateUsb.SU_POLICY.IS_EXPIRE_DT = jPolicyData.IS_EXPIRE_DT;				//만료일자지정 여부
			jCreateUsb.SU_POLICY.EXPIRE_YMD = jPolicyData.EXPIRE_YMD;					//만료기간
			jCreateUsb.SU_POLICY.IS_EXPIRE_INIT = jPolicyData.IS_EXPIRE_INIT;			//기간만료시 자동삭제
			jCreateUsb.SU_POLICY.IS_EXPIRE_STOP = jPolicyData.IS_EXPIRE_STOP;			//기간만료시 사용중지
			jCreateUsb.SU_POLICY.IS_READCOUNT = jPolicyData.IS_READCOUNT;				//실행회수 지정여부
			jCreateUsb.SU_POLICY.READCOUNT = jPolicyData.READCOUNT;						//실행회수
			jCreateUsb.SU_POLICY.IS_READ_INIT = jPolicyData.IS_READ_INIT;				//실행회수초과시 자동삭제
			jCreateUsb.SU_POLICY.IS_READ_STOP = jPolicyData.IS_READ_STOP;				//실행회수초과시 사용중지
			jCreateUsb.SU_POLICY.IS_WAIT_CLOSE = jPolicyData.IS_WAIT_CLOSE;				//실행후 지정시간 경과시 자동종료 여부
			jCreateUsb.SU_POLICY.WAIT_TIME = jPolicyData.WAIT_TIME;						//지정시간
			jCreateUsb.SU_POLICY.IS_LOGIN_FAIL_INIT = jPolicyData.IS_LOGIN_FAIL_INIT;	//로그인 실패시 자동삭제
			jCreateUsb.SU_POLICY.IS_LOGIN_FAIL_STOP = jPolicyData.IS_LOGIN_FAIL_STOP;	//로그인 실패시 사용중지
			jCreateUsb.SU_POLICY.IS_LOGIN_FAIL_COUNT = jPolicyData.IS_LOGIN_FAIL_COUNT;	//로그인실패 회수(로그인 실패 사용 유무컬럼은 존재하지 않는다. 따라서 0일 경우 실패 제한 없음 처리)
			jCreateUsb.SU_POLICY.USB_ENCODE_CL_CD = jPolicyData.USB_ENCODE_CL_CD;		//보안방법 구분코드 (현재 사용안함 NULL)
			jCreateUsb.SU_POLICY.USB_IMAGE_SIZE = jPolicyData.USB_IMAGE_SIZE;			//지정용량		  (현재 사용안함 NULL)
			//SU_BASIC_POLICY 고유 컬럼
			jCreateUsb.SU_POLICY.RUN_MAC_YN = jPolicyData.RUN_MAC_YN;					//지정PC 적용여부
			jCreateUsb.SU_POLICY.USER_ATUH_YN = jPolicyData.USER_ATUH_YN;				//사용자인증여부
			//SU_POLICY 고유 컬럼
			jCreateUsb.SU_POLICY.SU_POLICY_GRP_CD = jPolicyData.SU_POLICY_GRP_CD;		//정책그룹 코드
			jCreateUsb.SU_POLICY.MAC_ADDR = jPolicyData.MAC_ADDR;						//실행 MAC주소(MAC_LIST로 추출) 
			jCreateUsb.SU_POLICY.USB_ID = jPolicyData.USB_ID;							//SU_RENT_MGT 값을 넘겨받음
			//SU_RENT_MGT 컬럼
			jCreateUsb.SU_POLICY.RENDER_ID = jPolicyData.RENDER_ID;						//document.form.RENDER_ID.value;
			//DB 컬럼이 없는 대여화면에서만 존재하는 값
			jCreateUsb.SU_POLICY.IS_LOGIN_FAIL_YN = jPolicyData.IS_LOGIN_FAIL_YN;
			jCreateUsb.SU_POLICY.IP_ADDR = jPolicyData.IP_ADDR;
			
			var url = G_SERVLETURL + "?mod=savePolicy&LINK_ID="+ linkId +"&policySet=" + JSON.stringify(jCreateUsb.SU_POLICY); 
    		sendRequest(savePolicyEnd, " " ,"POST", url , false, false);	
    	} else {
    		alert("보안USB를 생성하는 중 오류가 발생하였습니다.");
    		//alert("Failed to connect to the CreateSecureUSB Service.");
    	}
    }
    
} // end onMessage

function onError(evt){
    //called on error
    alert("웹소켓에서 오류가 발생하였습니다.");
    //alert("Websocket Error!");
} // end onError


















function doAjaxResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyCombo = document.getElementById("comboBasicPolicyGrp");
	policyCombo.innerHTML = oj.responseText;//jData;
	document.form.policyCode.value ="<%=pm.getString("component.usb.export.default.policyCd")%>"; 
}
function suGrpChange(){
	var policyNo = document.form.policyCode.value;
	var url ="/mg/sugrp.do?mod=getPolicySuGrp&SU_POLICY_GRP_CD="+policyNo;
	sendRequest(suGrpChangeEnd, "", 'POST', url , false, false);
}

function suGrpChangeEnd(oj){
	var jDataPolicy = JSON.parse(oj.responseText);

	var policyNo = document.form.policyCode.value;
	var usbAuth = jDataPolicy.USB_AUTH_CL_CD; 
	var noSave = jDataPolicy.NO_SAVE; 
	var noCopy = jDataPolicy.NO_COPY; 
	var noPrint =jDataPolicy.NO_PRINT; 
	var isExpireDt = jDataPolicy.IS_EXPIRE_DT; 
	var isExpireInit = jDataPolicy.IS_EXPIRE_INIT; 
	var isReadcount = jDataPolicy.IS_READCOUNT; 
	var isReadInit = jDataPolicy.IS_READ_INIT; 
	var isWaitclose = jDataPolicy.IS_WAIT_CLOSE; 
	var waitTime = jDataPolicy.WAIT_TIME; 
	var isLoginFailInit = jDataPolicy.IS_LOGIN_FAIL_INIT; 
	var isLoginFileCnt = jDataPolicy.IS_LOGIN_FAIL_COUNT; 
	var usbEncode = jDataPolicy.USB_ENCODE_CL_CD; 
	var imageSize = jDataPolicy.USB_IMAGE_SIZE; 
	var expireDt = jDataPolicy.EXPIRE_YMD; 
	var readcnt = jDataPolicy.READCOUNT; 
	var userAuth = jDataPolicy.USER_AUTH_YN; 
	var runMac = jDataPolicy.RUN_MAC_YN;
	var returnDt = jDataPolicy.RETURN_YMD;
	
	form.USB_AUTH_CL_CD.value = usbAuth;
	form.NO_SAVE.value = noSave;
	form.NO_COPY.value = noCopy;
	form.NO_PRINT.value =noPrint;
	form.IS_EXPIRE_DT.value =isExpireDt;
	form.IS_EXPIRE_INIT.value =	isExpireInit;	
	form.IS_READCOUNT.value =isReadcount;
	form.IS_READ_INIT.value =isReadInit	;	
	form.IS_WAIT_CLOSE.value =isWaitclose;
	form.WAIT_TIME.value =	waitTime;	
	form.IS_LOGIN_FAIL_INIT.value =isLoginFailInit;
	form.IS_LOGIN_FAIL_COUNT.value =isLoginFileCnt;	
	form.USB_ENCODE_CL_CD.value =usbEncode;
	form.USB_IMAGE_SIZE.value =	imageSize;
	form.USER_AUTH_YN.value =userAuth;		
	form.RUN_MAC_YN.value =runMac;
	form.RETURN_YMD.value=returnDt;
	
	if(isExpireDt == "1"){
		form.EXPIRE_YMD.value=expireDt;
	}else{
		form.EXPIRE_YMD.value="제한없음";		
	}
	if(isReadcount == "1"){
		form.READCOUNT.value=readcnt;
	}else{
		form.READCOUNT.value="제한없음";		
	}
	
	if(runMac =="0"){
		form.tureMac.value="읽기/쓰기";
		form.falseMac.value="읽기/쓰기";
	}else if(runMac =="1"){
		form.tureMac.value="읽기/쓰기";
		form.falseMac.value="읽기전용";
	}else if(runMac =="2"){
		form.tureMac.value="읽기/쓰기";
		form.falseMac.value="사용금지";
	}
	
	//반납예정일은 기본 1년 
}

function onkeylengthMax(obj, nextobj, maxlength)
{
	if(obj.value.length==maxlength){
		if(obj != form.subMAC_6) nextobj.focus();
		return true;
	}else if(obj.value.length > maxlength){
		return false;
	}else{
		return true;
	}
}

function createSecureUSB(){
	
	doQueryDuring();
	
	var encodeTypeValue ="0"; //보안영역 생성방식
	var imageSize = "0"; //이미지일 경우 생성용량
	var sizeUnit = ""; //생성용량의 단위 
	var authTypeValue = ""; //인증방식
	var usbNm = ""; //usb명
	
	var isExpireDt = "0";
	var expireDt = ""; // 만료일
	var isReadcount = "0";
	var readcount = "0"; // 실행횟수
	var isProtectWrite = "1"; // 쓰기금지적용
	
	var passwd_w = "";
	var passwd_r = "";
	var isNoPrint = "0";	//문서출력을 금할 것인지
	var isNoCapture = "0";		//클립보드, 스크립 캡쳐 금할 것인지
	var isWaitClose = "0"; // 동작하지 않는다면 unMount할 것인지
	var waitTime = "0";	// 동작하지 않는 시간을 얼마나 기다려줄것인지
	var macAddr="";
	
	userAuthYn = form.USER_AUTH_YN.value;//0: 사용안함, 1: 사용함
	runMacYn = form.RUN_MAC_YN.value; //0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
	
	//selectUser에서 전달받음
	renderId= form.RENDER_ID.value;//document.getElementById("RENDER_ID").value;
	if(renderId == null || renderId ==""){
		alert("<%=mm.getMessage("GMSG_1016", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}
	
	authType = form.USB_AUTH_CL_CD.value; //웹인증, 자체인증방식 선택

	returnDt = form.RETURN_YMD.value; //일자
	if(returnDt==null || returnDt==""){
		alert("<%=mm.getMessage("SUMG_2008", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}
	 
	isExpireDt = form.IS_EXPIRE_DT.value;
	if(isExpireDt == "1"){
		expireDt = form.EXPIRE_YMD.value; //만료일자 사용시 
		if(expireDt==null || expireDt==""){
				alert("<%=mm.getMessage("SUMG_2009", s_user.getLocale())%>");
				doQueryModalEnd();
				return;
		}
	}
	
	isReadcount=form.IS_READCOUNT.value;
	if(isReadcount == "1"){
		readcount = form.READCOUNT.value; //횟수제한 사용시
		if(readcount==null || readcount=="" || readcount=="0"){
			alert("<%=mm.getMessage("SUMG_2010", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
	}
	authTypeValue=form.USB_AUTH_CL_CD.value;

	if(authTypeValue=="0"){ //웹인증 시 
		isProtectWrite = form.NO_SAVE.value; //파일추가,수정가능,보안USB외부로복사금지
		
		if(isProtectWrite!="1"){
			isNoPrint="1";
			isNoCapture="1";
		}
		
		isModified =form.IS_MODIFIED_USER_AUTH.value;

		passwd_w = "<%=pm.getString("component.usb.initPass.default")%>"; //초기 비밀번호, USB 첫 인증 실행 시 보안USB에서 비밀번호를 변경하라는 입력창이 뜬다. 
		passwd_r = "";
		
	}else{
		alert("현재 페이지는 웹인증 시 반출 화면입니다. 웹인증 관련 정책만 선택하실 수 있으십니다.");
		return;
	}
	
	if(runMacYn =="1" || runMacYn=="2"){//0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
		//macAddr=	
		var macAddrList = document.getElementById("MAC_LIST");
		for(var i=0; i<macAddrList.options.length; i++){
			if(macAddrList.options[i].value!=""){
				if(macAddr=="") macAddr = macAddrList.options[i].value.toUpperCase();
				else macAddr +="|"+macAddrList.options[i].value.toUpperCase();
			}
		}
	}
	
	isWaitClose = form.IS_WAIT_CLOSE.value;
	waitTime = form.WAIT_TIME.value;
	
	alert("<%=mm.getMessage("SUMG_2014", s_user.getLocale())%>");
	
	var obj = new Object();

	obj.USBID = usbId;
	obj.DRIVE = drive;
	obj.USEIMAGE = encodeTypeValue;
	obj.DRIVESIZE = imageSize;
	obj.DRIVEUNIT = sizeUnit;
	obj.AUTHORITYTYPE = authTypeValue;
	obj.USBTITLE = encodeURIComponent(usbNm);	//현재 사용안함 "" 값 전송
	//obj.RETURN_DT = returnDt;
	obj.ISUSEEXPIREDATE = isExpireDt;
	obj.EXPIREDATE = expireDt;
	obj.ISCHECKUSECOUNT = isReadcount;
	obj.USECOUNT = readcount;
	//obj.ISMODIFIED = isModified;
	obj.ISPROTECTWRITE = isProtectWrite;
	obj.PASSWORD = passwd_r;
	obj.FULLPASSWORD = passwd_w;
	obj.ISPROTECTPRINT = isNoPrint;
	obj.ISPROTECTCOPY = isNoCapture;
	obj.ISCHECKSLEEP = isWaitClose;
	obj.SLEEPTIME = waitTime;
	
	//추가
	obj.USERID=renderId;	
	obj.ISUSEUSERID=userAuthYn;  //0: 사용안함, 1: 사용함
	obj.ISUSEMACLIST=runMacYn; //0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
	obj.MAC_LIST =macAddr;
		
	//보안USB생성 서비스 호출
	var policy = JSON.stringify(obj);
	var url = G_SERVLETURL + "?mod=createSecureUSB&policy=" + policy;
	
	sendRequest(createSecureUSBEnd, " " ,"POST", url , false, false);
	
}

function createSecureUSBEnd(oj){

	document.USBInfo.CreateSecureUSB(oj.responseText);
	
	var url = G_SERVLETURL + "?mod=savePolicy&RENDER_ID="+renderId; 
		url+= "&RETURN_YMD="+returnDt ;
		url+= "&IS_MODIFIED_USER_AUTH=" + isModified;
		url+= "&policy=" + oj.responseText;
		url+= "&USER_ID="+"<%=s_user.getId() %>";
		url+= "&IP_ADDR="+"<%=userIp%>";
		url+= "&USER_AUTH_YN="+userAuthYn;
		url+= "&RUN_MAC_YN="+runMacYn;
		
	sendRequest(savePolicyEnd, " " ,"POST", url , false, false);
	
	opener.closeWinMappingUser();
}

</script>
<style>
	.centerTD{
		border-spacing: 25px;
	}
	.centerTD td{
		padding: 5px;
	}
	
</style>
</head>
<body onload="init();">
<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->
<form name="form" method="post">
<!-- <input type="hidden" name="USB_AUTH_CL_CD" value="">
<input type="hidden" name="NO_SAVE" value="">
<input type="hidden" name="NO_COPY" value="">
<input type="hidden" name="NO_PRINT" value="">
<input type="hidden" name="IS_EXPIRE_DT" value="">
<input type="hidden" name="IS_EXPIRE_INIT" value="">
<input type="hidden" name="IS_READCOUNT" value="">
<input type="hidden" name="IS_READ_INIT" value="">
<input type="hidden" name="IS_WAIT_CLOSE" value="">
<input type="hidden" name="WAIT_TIME" value="">
<input type="hidden" name="IS_LOGIN_FAIL_INIT" value="">
<input type="hidden" name="IS_LOGIN_FAIL_COUNT" value="">
<input type="hidden" name="USB_ENCODE_CL_CD" value="">
<input type="hidden" name="USB_IMAGE_SIZE" value="">
<input type="hidden" name="IS_MODIFIED_USER_AUTH" value="0">
<input type="hidden" name="RETURN_YMD" value="">
<input type="hidden" name="USER_AUTH_YN" value="">
<input type="hidden" name="MAC_ADDR" value=""> -->
<br>			
	<table class="centerTD">
		<colgroup>
			<col width="1%">
			<col width="15%">
			<col width="20%">
			<col width="15%">
			<col width="20%">
		</colgroup>
		
		<tr>
			<td/>
			<td>대여사용자</td>
			<td colspan="3"><label id="RENDER_NM"></label></td>
		</tr>
		<tr>
			<td/>
			<td>대여사용자ID</td>
			<td><label id="RENDER_ID"></label></td>
			<td>근무지</td>
			<td><label id="DEPT_NM"></label></td>
		</tr>
		<tr>
			<td/>
			<td>사용 가능 기간</td>
			<td><label id="RENT_E_DT">-</label></td>
			<td>요청기기</td>
			<td><label id="USB_TYPE">-</label></td>
		</tr>
		<tr>
			<td/>
			<td>요청사유</td>
			<td colspan="3"><label id="RENT_DESC"></label></td>
		</tr>
	</table>
		
	<div style="border: 2px solid #e8e8e8;">	
		<br>	
			<table class="centerTD">
					<colgroup>
						<col width="1%">
						<col width="15%">
						<col width="20%">
						<col width="20%">
					</colgroup>
					<!-- 기본 정책. 현재 하나이므로 감춰버린다. -->
					<tr style="display:none;">
						<td/>
						<td><p>기본 정책 선택</p></td>
						<td>
							<input type="hidden" name="SU_POLICY_BASIC_CD" value="">
							<select id="comboBasicPolicyGrp" class="w200" onchange="doBasicPolicyChange();"><option></option></select>
						</td>
					</tr>
					<tr>
						<td/>
						<td><p>정책 세트 선택</p></td>
						<td>
							<input type="hidden" name="SU_POLICY_GRP_CD" value="">
							<select id="comboPolicyGrp" class="w200" onchange="doPolicyGrpChange();"><option></option></select>
						</td>
					</tr>
					<tr>
						<td/>
						<td>사용 가능 횟수</td>
						<td><label id="READCOUNT"></label></td>
					</tr>
					<!-- <tr>
						<td/>
						<td>사용 가능 기간</td>
						<td><label id="EXPIRE_YMD">-</label></td>
					</tr> -->
					<tr>
						<td/>
						<td>지정 MAC</td>
						<td>
							<input type="radio" id="RUN_MAC_YN_N" name="RUN_MAC_YN" value="N" onclick="checkMacDisabled(true);">
							모든 PC 사용 가능
						</td>
						<td>
							<input type="radio" id="RUN_MAC_YN_Y" name="RUN_MAC_YN" value="Y" onclick="checkMacDisabled(false);">
							사용가능 MAC 구동
						</td> 				
					</tr>
					<tr>
						<td/>
						<td>사용 가능 MAC</td>
						<td>
							<input type="text" id="MAC_INPUT" class="w200" value="">
						</td>	
						<td>
							<div class="btnarea2">
								<a id="MAC_ADD" href="javascript:macAdd();" class="btn"><span>Mac 추가</span></a>
							</div>
						</td>
					</tr>
					<tr>
						<td/>
						<td/>
						<td id="MAC_TD">
							<select name="MAC_LIST" id="MAC_LIST" class="w200" multiple>
								<option value=''></option>
							</select>
						</td>
						<td>
							<div class="btnarea2">
				           		<a id="MAC_DEL" href="javascript:macDel();" class="btn"><span>선택 삭제</span></a>
				        	</div>
				        </td>
						<td/>
					</tr>
			</table>
				
				<br><br>
				
			<table class="centerTD">
					<colgroup>
						<col width="1%">
						<col width="15%">
						<col width="25%">
						<col width="60%">
					</colgroup>
					<tr>
						<td/>
						<td>보안USB 외부사용</td>
						<td>
							<input type="radio" id="USER_ATUH_YN_Y" name="USER_ATUH_YN" value="Y">
							허용
						</td>
						<td>
							<input type="radio" id="USER_ATUH_YN_N" name="USER_ATUH_YN" value="N">
							미허용
						</td> 
					</tr>
					<tr>
						<td/>
						<td>
							<a href="javascript:policySetPopup();"><font color="blue">정책세트 열기</font></a>
						</td>
						<td/>
						<td/>			
						<!--td 두개 들어갈곳-->
					</tr>	
				</table>
			</div>
			
			<br>
			
			<table style="width:100%">
				<colgroup>
					<col width="70%">
					<col width="10%">
					<col width="15%">
				</colgroup>
				<tr>
					<td></td>
					<td>
						<div class="btnarea">
							<a href="javascript:doCancel();" class="btn">
							<span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
						</div>
					<td>
						<div class="btnarea">
							<a href="javascript:doSave();" class="btn">
							<span><%=mm.getMessage("보안USB 대여", s_user.getLocale())%></span></a>
						</div>
					</td>
				</tr>
			</table>			
</form>
</body>
</html>