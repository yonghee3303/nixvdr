<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String drive = (String)request.getParameter("drive");
String model = (String)request.getParameter("model");
String usbSize = (String)request.getParameter("usbSize");
String contextPath = request.getContextPath();
%>

<html>
<head>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<!-- WebSocket 적용 -->
<script type="text/javascript" src="../../../ext/js/websocket.js"></script>
<!-- JSON 적용 -->
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<link href="/ext/css/common.css" rel="stylesheet" type="text/css" />

<title>장치 선택</title>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/enrollUSB.do";

var selDrive = "<%=drive%>";
var selModel = "<%=model%>";
var selUsbSize = "<%=usbSize%>";
var selUsbType="U";
var usbId="";
function init() {
	if(dhxWins == null) {
    	dhxWins = new dhtmlXWindows();
    	dhxWins.setImagePath("<%=contextPath%>/dthmlx/dhtmlxWindows/codebase/imgs/");
    }

	if(prg_win == null) {
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;
		
		prg_win = dhxWins.createWindow("prg_win", top, left, 180, 110);
		prg_win.setText("Please wait for a moment.");
		prg_win.button("close").hide();
		prg_win.button("minmax1").hide();
		prg_win.button("minmax2").hide();
		prg_win.button("park").hide();
		dhxWins.window("prg_win").setModal(false);
		prg_win.attachURL("../../../ext/include/progress_ing.htm");
		dhxWins.window("prg_win").hide();
	}
	
	WebSocketConnect();
	
	form.usbType.value=selUsbType;
	var arr_usbSize = selUsbSize.split(' GB');
	selUsbSize = parseInt(arr_usbSize[0]) * 1024;
}

function selectType(){
	//opener.close();
	enrollUSB();
}
function enrollUSB() {
	var typeRadio =document.getElementsByName("usbType");
	for(var i=0; i < typeRadio.length; i++){
		if(typeRadio[i].checked){
			selUsbType=typeRadio[i].value;
		}
    }
	//드라이브가 선택되지 않은 경우
	if(selDrive==""){
		alert("<%=mm.getMessage("SUMG_2016", s_user.getLocale())%>");
		return;
	}
	
	doQueryDuring();
	alert("<%=mm.getMessage("SUMG_2014", s_user.getLocale())%>");
	var url = G_SERVLETURL + "?mod=selectEnrollUSB&drive=" + selDrive+"&usbType="+selUsbType;
	sendRequest(enrollUSBEnd, " " ,"POST", url , false, false);	
}
function enrollUSBEnd(oj){
	var jData = JSON.parse(oj.responseText);
	
	if(jData.RESULT == "false"){
		alert(jData.ERROR);
		return;
	}
	usbId = jData.SU_POLICY.USB_ID;
	sendWsMessage(oj.responseText);
}
function enrollFinish(){
	window.opener.callRefreshImportList();
	alert("["+usbId+"] 등록이 완료되었습니다.");
	doQueryModalEnd();
	window.opener.callOpener(selDrive, selModel, selUsbSize ,selUsbType, usbId);
	window.close();
}

//Websocket 이벤트 핸들러
function onOpen(evt){
    //called as soon as a connection is opened
    isConnection = true;
} // end onOpen

function onClose(evt){
    //called when connection is severed
    isConnection = false;
} // end onClose;

function onMessage(evt){
    //called on receipt of message
    var jData;
	try {
		jData = JSON.parse(evt.data);	
	} catch(err) {
		return;
	}
	
    if(jData.errmsg.indexOf("Secure USB Create") != -1) {
		if(jData.result == "TRUE") {
			//RESPONSE: { "result": "TRUE", "errcode": "0", "errmsg": "Secure USB Create Success." }
			//RESPONSE: { "result": "FALSE", "errcode": "-1", "errmsg": "Secure USB Create Fail." }
			var usbInfo = new Object();
			usbInfo.action = "getUSBInfo";
			usbInfo.DRIVE = selDrive;
			sendWsMessage(JSON.stringify(usbInfo));
    	} else {
    		alert("보안USB를 생성하는 중 오류가 발생하였습니다.");
    		//alert("Failed to connect to the NewCreateSecureUSB Service.");
    	}
    } else if(jData.errmsg.indexOf("getUSBInfo") != -1) {
    	if(jData.result == "TRUE") {
    		//RESPONSE: { "PRODID": "", "VENDORID": "", "SERIALNO": "89388C84", "DISKFULLSIZE": "8162086912", "DRIVESIZE": "8162082816", "PNPDEVID": "USBSTOR\\DISK&VEN_SMI&PROD_USB_DISK&REV_1100\\6&3636BEF7&0", "result": "TRUE", "errcode": "0", "errmsg": "getUSBInfo " }
    		var usbCapacity = jData.DISKFULLSIZE/1024/1024;
    		var url = G_SERVLETURL + "?mod=enrollUSB";
    		url+="&USB_ID="+usbId;
    		url+="&USB_CAPACITY="+usbCapacity;
    		url+="&usbType="+selUsbType;
    		url+="&USB_MODEL_NM="+jData.VENDORID;
    		url+="&USB_KEY="+jData.SERIALNO;
    		url+="&ADMIN_ID=<%=s_user.getId().toString()%>";
    		url+="&REG_ID=<%=s_user.getId().toString()%>";
    		
    		sendRequest(enrollFinish, " " ,"POST", url , false, false);	
    	} else {
    		alert("USB 정보를 읽어오는 중 오류가 발생하였습니다.");
    		//alert("Failed to connect to the getUSBInfo Service.");
    	}
    }
    
} // end onMessage

function onError(evt){
    //called on error
   alert("웹소켓에서 오류가 발생하였습니다.");
    // alert("Websocket Error!");
} // end onError


////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
var dhxWins;
var prg_win;
function BwindowWidth() {
	 if (window.innerWidth) {
		 return window.innerWidth;
	 } else if (document.body && document.body.offsetWidth) {
	 	return document.body.offsetWidth;
	 } else {
		 return 0;
	}
}	
function BwindowHeight() {
	 if (window.innerHeight) {
		 return window.innerHeight;
	} else if (document.body && document.body.offsetHeight) {
		 return document.body.offsetHeight;
	} else {
		 return 0;
	}
}
function doQueryDuring()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
}
////////////////////////////////////////////////////프로그래스바 처리때문에

</script>



<style>
.selectType {
		background-color:#dfeff5;
		border:1px solid #9ebcce;
		/* width:100%;
		height:100%; */
		margin:10px;
		padding:5px;
}
body, div, table td{
	font-family:malgun gothic, Verdana, Tahoma; 
	font-size:12px;
}
</style>
</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:15px; overflow: hidden;">

<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->

<form name="form">
등록하지 않은 USB입니다. 지금 등록하시겠습니까?
<div class="selectType">
	<table>
		<tr><td colspan="2">등록할 장치 유형을 선택하세요.</td></tr>
		<tr>
			<td><input type="radio" name="usbType" value="U"> USB장치</td>
			<td><input type="radio" name="usbType" value="H"> HDD</td>
		</tr>
	</table>
</div>
<table style="width:100%;">
	<tr>
		<td>
			<div class="btnarea" style="padding:10px,0px,0px,0px;">
	            	<a href="JavaScript:selectType();" class="btn"><span>예</span></a>
	            	<a href="JavaScript:close();" class="btn"><span>아니오</span></a>
	    	</div>
		</td>
	</tr>
</table>


</form>
</body>
</html>