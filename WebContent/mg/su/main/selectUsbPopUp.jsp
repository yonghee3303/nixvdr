<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);

IUser s_user = (IUser) session.getAttribute("j_user");
String formid = (String)request.getParameter("formid");
%>


<html>
<head>

<title>장치 선택</title>


<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<!-- 그리드 생성시 필요 -->
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<!-- WebSocket 적용 -->
<script type="text/javascript" src="../../../ext/js/websocket.js"></script>
<!-- JSON 적용 -->
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<script>
/** 
 * USB 드라이버 및 정보 조회
 * 최종 수정일 : 2016. 03. 21
 * 변경 내용 : OCX -> WebSocket 변환
 */


var GridObj 		= {};

//드라이브명 배열생성
var driveList = new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
var currentDrive = "";
var driveCnt = 0;

var selDrive = "";
var selModel = "";
var selUsbSize = "";
var selUsbType="U";
var enckey = "";
var version="";
function init() {
 	setGridDraw();
	WebSocketConnect();
	getEnckey();
	//alert("USB를 제거한 후 다시 연결하세요.");
}
function setGridDraw(){

	GridObj = new dhtmlXGridObject('gridbox');
	
	GridObj.setSkin("dhx_skyblue");
	GridObj.setImagePath("../../../dhtmlx_v403/sources/dhtmlxGrid/codebase/imgs/");
	GridObj.setHeader("<%=mm.getMessage("SUMG_1154", s_user.getLocale())%>,<%=mm.getMessage("SUMG_1155", s_user.getLocale())%>,<%=mm.getMessage("SUMG_1156", s_user.getLocale())%>");
	GridObj.setInitWidths("80,190,190");
	GridObj.setColAlign("center,center,center");
	GridObj.setColumnIds("DRIVE,MODEL,CAPACITY");
	GridObj.setColumnColor("#FFFFFF,#FFFFFF,#FFFFFF");
	GridObj.setColSorting("str,str,str");
	GridObj.setColTypes("ro,ro,ro");
	GridObj.enableMultiline(true); //수정
	GridObj.enableMultiselect(false); //수정
	GridObj.attachEvent("onRowSelect", function(id,ind){
		selDrive = GridObj.cells(id, 0).getValue();
		selModel = GridObj.cells(id, 1).getValue();
		selUsbSize = GridObj.cells(id, 2).getValue();
	});
	GridObj.attachEvent("onRowDblClicked", function(id,ind){
		selDrive = GridObj.cells(id, 0).getValue();
		selModel = GridObj.cells(id, 1).getValue();
		selUsbSize = GridObj.cells(id, 2).getValue();
		if(selDrive==""){
			alert("<%=mm.getMessage("SUMG_2015", s_user.getLocale())%>");
		}else{
			isSecureUSB(selDrive);
		}
	});
	GridObj.init();

	// USB드라이브 인지 체크
	//var isUSB = document.USBInfo.IsUSBDrive(driveList[i]);
	getVersionInfo();
	
}
function getVersionInfo(){
	var versionInfo = new Object();
	versionInfo.action="getVersionInfo";
	versionInfo.VERSION=version;
	sendWsMessage(JSON.stringify(versionInfo));
}
function getUsbDrive() {
	var usbInfo = new Object();
	usbInfo.action = "IsUSBDrive";
	usbInfo.DRIVE = driveList[driveCnt];
	sendWsMessage(JSON.stringify(usbInfo));
} 
function getUsbInfo(drive) {
	//usb정보 얻어옴
	//var usbInfo = document.USBInfo.GetUSBInfo(driveList[i]);
	var usbInfo = new Object();
	usbInfo.action = "getUSBInfo";
	usbInfo.DRIVE = drive;
	sendWsMessage(JSON.stringify(usbInfo));
}

function getEnckey() {
<%-- 	var url = "<%=request.getContextPath()%>/mg/enrollUSB.do?mod=selectEnckey";
	sendRequest(getEnckeyEnd, " " ,"POST", url , false, false); --%>
}
function getEnckeyEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	enckey = jData.ENCKEY;
	version = jData.VERSION;
}
function isSecureUSB(drive){
	//var isSecure = document.USBInfo.IsSecureUSBDrive(drive, enckey);
	var usbInfo = new Object();
	usbInfo.action = "IsSecureUSBDrive";
	usbInfo.DRIVE = drive;
	usbInfo.ENCKEY = enckey;
	sendWsMessage(JSON.stringify(usbInfo));
}
function selectUSB() {
	
	if(selDrive==""){
		alert("<%=mm.getMessage("SUMG_2015", s_user.getLocale())%>");
	}else{
		isSecureUSB(selDrive);
	}
}
function resize() {
	GridObj.setSizes();
}

function refresh() {
	location.replace("selectUsbPopUp.jsp");
}
function callOpener(drive, model, usbSize, usbType, usbId){
	window.opener.deliver(selDrive, selModel, selUsbSize ,selUsbType, usbId);
	window.close();
}
function callRefreshImportList(){
	window.opener.updateImportList();
}

//Websocket 이벤트 핸들러
function onOpen(evt){
    //called as soon as a connection is opened
    isConnection = true;
    setGridDraw();
    //window.opener.init();
    if(window.opener.isConnection == false) {
	    window.opener.WebSocketConnect();
    }
} // end onOpen

function onClose(evt){
    //called when connection is severed
	isConnection = false;
} // end onClose;

function onMessage(evt){
    //called on receipt of message
    var jData;
	try {
		jData = JSON.parse(evt.data);	
	} catch(err) {
		return;
	}
	
    if(jData.errmsg.indexOf("IsUSBDrive") != -1) {
    	
    	if(jData.errcode == "0") {
    		currentDrive = driveList[driveCnt];
    		getUsbInfo(currentDrive);
    	} else {
    		driveCnt++;
    		if(driveCnt != driveList.length) {
    			getUsbDrive();
        	}
    	}
    } else if(jData.errmsg.indexOf("getUSBInfo") != -1) {
    	if(jData.errcode == "0") {
    		var newId = (new Date()).valueOf();
    		GridObj.addRow(newId,"");
    		GridObj.cells(newId,GridObj.getColIndexById("DRIVE")).setValue(currentDrive);
    		GridObj.cells(newId,GridObj.getColIndexById("MODEL")).setValue(jData.VENDORID);
    		GridObj.cells(newId,GridObj.getColIndexById("CAPACITY")).setValue((jData.DISKFULLSIZE/1024/1024/1024).toFixed(2) +" GB");
    	} else {
    		//alert("Failed to connect to the getUSBInfo Service.");
    		alert("USB 정보를 읽어오는 중 오류가 발생하였습니다.");
    	}
    	driveCnt++;
		if(driveCnt != driveList.length) {
			getUsbDrive();
    	}
    } else if(jData.errmsg.indexOf("IsSecureUSBDrive") != -1) {

    	if(jData.errcode == "1" && jData.result == "TRUE") {
			window.opener.deliver(selDrive, selModel, selUsbSize ,selUsbType, jData.USB_ID);
			window.close();
    	} else if(jData.errcode == "0" && jData.result == "FALSE") {
			window.open("selectUsbType.jsp?drive="+selDrive+"&model="+selModel+"&usbSize="+selUsbSize,'','width=350, height=150,scrollbars=no,resizable=no');
    	} else {
    		alert("보안USB로 등록되어있는지 확인하는 중 오류가 발생하였습니다.");
    		//alert("Failed to connect to the IsSecureUSBDrive Service.");
    	}
    }else if(jData.errmsg.indexOf("getVersionInfo") != -1) {
    	if(jData.result == "FALSE"){
    		window.close();
    	}else{
    		getUsbDrive();
    	}
    }

} // end onMessage

function onError(evt){
    //called on error
	//alert("Websocket Error!");
    websocketServiceDownload();
} // end onError

</script>



<style>

	#gridbox {
        width: 100%;
        height: 70%;
        margin: 0px;
        padding: 0px;
    }
    
    .board-search { width:100%; margin-bottom:5px; border:0;}
    
    .board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:0; background:#ffffff;}

	.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(../../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}
	
	.btnarea {
		background-color:#ffffff;
	}
	
</style>




</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:15px; overflow: hidden;">

<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->

<form>
<table class="board-search" style="width:100%; padding:0px,0px,50px,0px;">
	<tr>
		<td class="tit">
			<%=mm.getMessage("SUMG_1004", s_user.getLocale())%>
		</td>
		<td>
			<div class="btnarea">
				<a href="JavaScript:refresh();" class="btn"><span><%=mm.getMessage("SUMG_1157", s_user.getLocale())%></span></a>
			</div>
		</td>
	</tr>
</table>

<div id="gridbox" style="position: relative;"></div>

<table style="width:100%;">
	<tr>
		<td>
			<div class="btnarea" style="padding:10px,0px,0px,0px;">
	            	<a href="JavaScript:selectUSB();" class="btn"><span><%=mm.getMessage("COMG_1022", s_user.getLocale())%></span></a>
	            	<a href="JavaScript:close();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
	    	</div>
		</td>
	</tr>
</table>


</form>
</body>
</html>