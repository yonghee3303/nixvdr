<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
	IUser s_user = (IUser) session.getAttribute("j_user");
	String usbId = request.getParameter("usbId");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String contextPath = request.getContextPath();
%>
<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<%@ include file="../../../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/exportMgmt.do";
var calendar;


function init(){
	//달력을 한글로 표시하기 위해 아래와 같이 지정해야한다.
	
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
 	
	calendar = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	
	document.getElementById("find").style.display = "none";
	document.getElementById("lost").style.display = "block"; 
}

//상단에 있는 그리드 목록 중 하나를 더블클릭했을 때 이벤트가 이 페이지 요 메소드로 전달되어 정책 정보를 뿌려준다.
function doAjax_Policy(usb, rent){

	var arg = "&USB_ID="+ usb;
	arg += "&USB_CREATE_ID="+rent;
	var url = G_SERVLETURL + "?mod=selectApplyPolicy"+arg; 
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}
//실질적인 정보를 화면에 뿌려주는 부분
function doAjaxEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	
	if("대여"==jData.RENT_STS_NM){
		document.getElementById("find").style.display = "none";
		document.getElementById("lost").style.display = "block"; 
	}else if("분실"==jData.RENT_STS_NM){
		document.getElementById("lost").style.display = "none";
		document.getElementById("find").style.display = "block"; 
	}
	
	 document.getElementById("USB_ID").value=jData.USB_ID;
	 document.getElementById("USB_CREATE_ID").value=jData.USB_CREATE_ID;
	 document.getElementById("MOD_STOP").value=jData.MOD_STOP;
	 document.getElementById("IS_MODIFIED_USER_AUTH").value=jData.IS_MODIFIED_USER_AUTH;
	 
	 //var textNode = document.createTextNode("정책 변경 권한을 가지고 있지 않습니다.");
	 
	 if(jData.IS_MODIFIED_USER_AUTH=='1'){
		 document.getElementById("IS_MODIFIED_USER_AUTH_bluetext").innerText="<%=mm.getMessage("SUMG_1001", s_user.getLocale())%>";
	 	 document.getElementById("IS_MODIFIED_USER_AUTH_redtext").innerText="";
	 }else{
		 document.getElementById("IS_MODIFIED_USER_AUTH_bluetext").innerText="";
		 document.getElementById("IS_MODIFIED_USER_AUTH_redtext").innerText="<%=mm.getMessage("SUMG_1002", s_user.getLocale())%>";
	 }
	 
	if(jData.NO_PRINT=='1'){	document.getElementById("NO_PRINT").checked=true;	}
	else{	document.getElementById("NO_PRINT").checked=false; }
		
	if(jData.NO_COPY=='1'){	document.getElementById("NO_COPY").checked=true;	}
	else{	document.getElementById("NO_COPY").checked=false; }
		
	
	 switch(jData.NO_SAVE){	
	 case "1":
		document.getElementById("NO_SAVE1").checked=true;
		
		document.getElementById("NO_PRINT").checked=false;
		document.getElementById("NO_PRINT").disabled=true;
		document.getElementById("NO_COPY").checked=false;
		document.getElementById("NO_COPY").disabled=true;
		break;
	 case "2":
		document.getElementById("NO_SAVE2").checked=true;
		break;
	 case "3":
		document.getElementById("NO_SAVE3").checked=true;
	 	break;
	 case "4":	
		document.getElementById("NO_SAVE4").checked=true;
		break;
	 }
	
	 document.getElementById("USB_NM").value=jData.USB_NM;
	 //document.getElementById("RETURN_YMD").value = jData.RETURN_YMD;
	 document.getElementById("calendar_input").value=jData.EXPIRE_YMD;
	if(jData.IS_EXPIRE_DT=='1'){	document.getElementById("IS_EXPIRE_DT").checked=true;	}
	else{	document.getElementById("IS_EXPIRE_DT").checked=false; }
	
	if(jData.IS_READCOUNT=='1'){	document.getElementById("IS_READCOUNT").checked=true;	}
	else{	document.getElementById("IS_READCOUNT").checked=false; }
	document.getElementById("READCOUNT").value = jData.READCOUNT;
	
	if(jData.IS_WAIT_CLOSE=='1'){	document.getElementById("IS_WAIT_CLOSE").checked=true;	}
	else{	document.getElementById("IS_WAIT_CLOSE").checked=false; }
	
	document.getElementById("WAIT_TIME").value = jData.WAIT_TIME;
	
	//alert("jData.RENT_STS_NM :"+jData.RENT_STS_NM);
	
}
function clickNoSave(){
	var noSaveType =document.getElementsByName("NO_SAVE");
	var val="";
	for(var i=0; i < noSaveType.length; i++){
        if(noSaveType[i].checked){
        	val = noSaveType[i].value;
        }
    }

	var noPrint =document.getElementById("NO_PRINT");
	var noCopy =document.getElementById("NO_COPY");
	
	switch(val){
	case "1": //파일 추가, 수정 가능
		noPrint.checked=false;
		noPrint.disabled=true;
		noCopy.checked=false;
		noCopy.disabled=true;
		break;
	case "2": //파일 추가, 수정 가능, 보안USB 외부로 복사 금지
		noPrint.checked=true;
		noPrint.disabled=false;
		noCopy.checked=true;
		noCopy.disabled=false;
		break;
	case "3": //파일 추가, 수정 금지(읽기 전용)
		noPrint.checked=true;
		noPrint.disabled=false;
		noCopy.checked=true;
		noCopy.disabled=false;
		break;
	case "4": //파일 추가 금지, 동일 파일명 수정 가능
		noPrint.checked=true;
		noPrint.disabled=false;
		noCopy.checked=true;
		noCopy.disabled=false;
		break;
	}
	
}
function clickReadCount(){
	var isReadCount = document.getElementById("IS_READCOUNT");
	var readCount = document.getElementById("READCOUNT");
	
	if(isReadCount.checked){
		readCount.value="50";
	}else{
		readCount.value="0";
	}
}
function clickWaitClose(){
	var isWaitTime = document.getElementById("IS_WAIT_CLOSE");
	var waitTime = document.getElementById("WAIT_TIME");
	
	if(isWaitTime.checked){
		waitTime.value="5";
	}else{
		waitTime.value="0";
	}
	
}
//정책적용 버튼을 누르면 호출된다. 필요한 값들을 URL로 만들어 다른 페이지로 전달할 뿐...
function applyPolicy(){

	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	var modStop = document.getElementById("MOD_STOP").value;
	var modifiedAuth = document.getElementById("IS_MODIFIED_USER_AUTH").value;
	
	if(usbId=="" && rentId==""){ //어떠한 것도 선택하지 않고 눌렀을 경우 
		alert("<%=mm.getMessage("SUMG_2001", s_user.getLocale())%>"); //정책 적용할 USB를 선택하지 않으셨습니다.
	}else if(modifiedAuth=="0"){ //권한이 없지만 시도할 경우
		alert("<%=mm.getMessage("SUMG_2002", s_user.getLocale())%>"); //정책을 수정할 권한이 없습니다. 관리자에게 문의하세요.
	}
	else{
		
		if(confirm("<%=mm.getMessage("SUMG_1006", s_user.getLocale())%>")==false){ 
			return;
		}
	
		var url ="&USB_ID="+usbId+"&USB_CREATE_ID="+rentId+"&MOD_STOP="+modStop+"&IS_MODIFIED_USER_AUTH="+modifiedAuth;
	
		
		url+="&NO_SAVE=";
		if(document.getElementById("NO_SAVE1").checked)
			url+="1";
		else if(document.getElementById("NO_SAVE2").checked)
			url+="2";	
		else if(document.getElementById("NO_SAVE3").checked)
			url+="3";
		else
			url+="4";
	
		//USB_NM은 nvachar 형태로 한글 입력이 가능하다.
		url+="&USB_NM="+ encodeURIComponent(document.getElementById("USB_NM").value);
		
		url+="&IS_EXPIRE_DT=";
		if(document.getElementById("IS_EXPIRE_DT").checked)
			url+="1";
		else
			url+="0";
		
		url+="&EXPIRE_YMD="+document.getElementById("calendar_input").value;
		
		url+="&IS_READCOUNT=";
		if(document.getElementById("IS_READCOUNT").checked)
			url+="1";
		else
			url+="0";
		
		url+="&READCOUNT="+document.getElementById("READCOUNT").value;
		
		url+="&NO_PRINT=";
		if(document.getElementById("NO_PRINT").checked)
			url+="1";
		else
			url+="0";
		
		url+="&NO_COPY=";
		if(document.getElementById("NO_COPY").checked)
			url+="1";
		else
			url+="0";
		
		url+="&IS_WAIT_CLOSE=";
		if(document.getElementById("IS_WAIT_CLOSE").checked)
			url+="1";
		else
			url+="0";
		
		url+="&WAIT_TIME="+document.getElementById("WAIT_TIME").value;
		
		url+="&RENDER_ID="+"<%=s_user.getId().toString()%>";
	//그리드가 있는 페이지로 값을 넘겨준다. 
	parent.updatePolicy(url);
	
	}
}
function lostUSB(){
	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	parent.updateUsbStatus(usbId, rentId, "05");
}
function findUSB(){
	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	parent.updateUsbStatus(usbId, rentId, "03");
}
function chgPassword(){
	//alert("패스워드 변경");
	var usbId = document.getElementById("USB_ID").value;
	var rentId = document.getElementById("USB_CREATE_ID").value;
	
	if(usbId=="" || rentId==""){
		alert("<%=mm.getMessage("COMG_1018", s_user.getLocale())%>"); //비밀번호를 변경할 사용자를 선택해 주십시오. ->선택된 정보가 없습니다.
		return;
	}
	winChgPassword = window.open("./chgPasswordDetail.jsp?USB_ID="+usbId + "&USB_CREATE_ID="+rentId,'','width=350px,height=250px,scrollbars=no,resizable=no');
}

function closeWinChgPassword(){
	winChgPassword.close();
}
</script>
<style>
 tr, td, div {
 font-family: Tahoma;
 padding:3px;
 }
</style>
</head>
<body onload="init();" style="margin: 0px; padding: 8px; overflow:hidden;">
<input id="USB_ID" type="hidden"/>
<input id="USB_CREATE_ID" type="hidden"/>
<input id="MOD_STOP" type="hidden"/>
<input id="IS_MODIFIED_USER_AUTH" type="hidden"/>
<table style="width: 100%;">
	<tr>
		<td colspan="2">
		<table style="width: 100%;">
			<tr>
				<td>
					 <font id="IS_MODIFIED_USER_AUTH_bluetext" color="blue"> </font>
					 <font id="IS_MODIFIED_USER_AUTH_redtext" color="red"> </font>
				</td>
				<td>
					<div class="btnarea">
							<a id="find" href="javascript:findUSB();" class="btn"><span id="find"><%=mm.getMessage("SUMG_1102", s_user.getLocale())%></span></a>
							<a id="lost" href="javascript:lostUSB();" class="btn"><span id="lost"><%=mm.getMessage("SUMG_1101", s_user.getLocale())%></span></a>
		            		<a href="javascript:chgPassword();" class="btn"><span><%=mm.getMessage("SUMG_1103", s_user.getLocale())%></span></a>
			            	<a href="javascript:applyPolicy();" class="btn"><span><%=mm.getMessage("SUMG_1104", s_user.getLocale())%></span></a>
			        </div>
		        </td>
			</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">&nbsp; <%=mm.getMessage("SUMG_1105", s_user.getLocale())%>
			<div style="border: 2px dashed #a1ceed;">
				<table><tr><td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();"> <%=mm.getMessage("SUMG_1106", s_user.getLocale())%></td></tr>
						<tr><td><input id="NO_SAVE2" type="radio"  name="NO_SAVE" onclick="clickNoSave();"> <%=mm.getMessage("SUMG_1107", s_user.getLocale())%></td></tr>
						<tr><td><input id="NO_SAVE3" type="radio"  name="NO_SAVE" onclick="clickNoSave();"> <%=mm.getMessage("SUMG_1108", s_user.getLocale())%></td></tr>
						<tr><td><input id="NO_SAVE4" type="radio"  name="NO_SAVE" onclick="clickNoSave();"> <%=mm.getMessage("SUMG_1109", s_user.getLocale())%></td></tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td><table style="table-layout:fixed;" >
				<colgroup>
  		    		<col width="250px"/> <!-- 검색기간 -->
            		<col width="300px"/> <!-- 텍스트 필드 -->	
				</colgroup>
				<tr><td>&nbsp;<%=mm.getMessage("SUMG_1110", s_user.getLocale())%></td><td  ><input id="USB_NM" type="text" value="" style="width:120px"></td></tr>
				<tr><td><input id="IS_EXPIRE_DT" type="checkbox"> <%=mm.getMessage("SUMG_1111", s_user.getLocale())%></td><td><input type="text" id="calendar_input" style="width:100px"> <span><img id="calendar_icon" src="../../../ext/images/calendar.png" border="0"></span></td></tr>
				<tr><td><input id="IS_READCOUNT" type="checkbox" onclick="clickReadCount();"> <%=mm.getMessage("SUMG_1112", s_user.getLocale())%></td><td><input  id="READCOUNT" type="text" style="width:50px" value=""><%=mm.getMessage("SUMG_1113", s_user.getLocale())%> </td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp; <%=mm.getMessage("SUMG_1114", s_user.getLocale())%>
			<div style="border: 2px dashed #a1ceed;">
				<table>
					<tr><td><input id="NO_PRINT" type="checkbox"> <%=mm.getMessage("SUMG_1115", s_user.getLocale())%></td></tr>
					<tr><td><input id="NO_COPY" type="checkbox"> <%=mm.getMessage("SUMG_1116", s_user.getLocale())%></td></tr>
					<tr><td><input id="IS_WAIT_CLOSE" type="checkbox" onclick="clickWaitClose();"> <%=mm.getMessage("SUMG_1117", s_user.getLocale())%></td></tr>
					<tr><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=mm.getMessage("SUMG_1118", s_user.getLocale())%> <input id="WAIT_TIME" type="text" value="" style="width:50px"><%=mm.getMessage("SUMG_1119", s_user.getLocale())%></td></tr>
					<!-- <tr><td><input id="IS_EXPIRE_INIT" type="checkbox"> 기간 만료 시 보안 USB 초기화</td></tr>
					<tr><td><input id="IS_READ_INIT" type="checkbox"> 지정횟수 초과시 보안 USB 초기화</td></tr> -->
				</table>
			</div>
		</td>
	</tr>
</table>

</body>
</html>