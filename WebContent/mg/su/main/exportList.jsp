<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
String contextPath = request.getContextPath();
%>

<html>
<head>

<title>반출목록</title>
<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="/ext/include/su_grid_common_render.jsp"%>


<script>
var selRowId = null;

var GridObj 		= {};

var G_SERVLETURL = "<%=contextPath%>/mg/exportList.do";

function init() {
	
	GridObj = setGridDraw(GridObj);
	
	/* GridObj.enableDragAndDrop(true);
	//GridObj.setDragBehavior("complex");
	GridObj.attachEvent("onDrag", function (sid,tid) {
		window.open("/mg/su/main/selectUser.jsp?sId="+sId+"&tId="+tId+"&sObj="+sObj+"&tObj="+tObj+"&sInd="+sInd+"&tInd="+tInd,'','width=300, height=300,scrollbars=no,resizable=no');
		alert("export-onDrag");
	});	
	GridObj.attachEvent("onDragIn", function (sid,tid,sgrid,tgrid) {
		window.open("/mg/su/main/selectUser.jsp?sId="+sId+"&tId="+tId+"&sObj="+sObj+"&tObj="+tObj+"&sInd="+sInd+"&tInd="+tInd,'','width=300, height=300,scrollbars=no,resizable=no');
		alert("export-onDragIn");
	});
	GridObj.attachEvent("onDragOut", function (tid) {
		window.open("/mg/su/main/selectUser.jsp?sId="+sId+"&tId="+tId+"&sObj="+sObj+"&tObj="+tObj+"&sInd="+sInd+"&tInd="+tInd,'','width=300, height=300,scrollbars=no,resizable=no');
		alert("export-onDragOut");
	}); */
	GridObj.attachEvent("onRowDblClicked",doOnRowSelected);
	doQuery();
}
//목록 더블클릭 시 화면 이동 
function doOnRowSelected(rowId, cellInd) {
	//var header_name = GridObj.getColumnId(cellInd);
	
		var usb_id = GridObj.cells(rowId, GridObj.getColIndexById("USB_ID")).getValue();
		parent.parent.mainFrame.location.href ='../mgmt/exportMgmtLayout.jsp?USB_ID='+usb_id;

}
function resize() {
	GridObj.setSizes();
}

function doQuery() {
<%-- 	var grid_col_id  = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectExportList&grid_col_id="+grid_col_id);																						
	GridObj.clearAll(false); --%>
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	<%-- document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> "; --%>
	if(status == "false") alert(msg);
	return true;
}

function drop(target, food) {
	/* window.open("/mg/su/main/selectUser.jsp?sId="+sId+"&tId="+tId+"&sObj="+sObj+"&tObj="+tObj+"&sInd="+sInd+"&tInd="+tInd,'','width=300, height=300,scrollbars=no,resizable=no'); */
	alert("export drop method");
}


function refresh() {
	
	location.replace("exportList.jsp?&formid=SU_0106&gridid=GridObj");
}
function selectedUSB(id){
	// 전달받은 값이 없으면 함수 종료
	if(id==null || id==""){
		return;
	}
	
	// 전달받은 값을 Grid 전체에서 찾아 셀의 위치 반환
	var result = GridObj.findCell(id);
	var chResult = String(result);
	// 셀의 위치에서 row값만 추출
	var array = chResult.split(',');
	
	// 일치하는 항목이 없는경우는 함수 종료
	if(array[0]==null || array[0]==""){
		return;
	}
	
	// 기존 선택된 row값(전역변수값)의 배경색 해제
	if (selRowId!=null){
		GridObj.setRowColor(selRowId,"white");
		//GridObj.changeCellType(selRowId,4,"ro");
		GridObj.cells(selRowId,4).setValue("");
	}
	
	// 전역변수로 지정
	selRowId = array[0];

	// row의 배경색 변경 & 최상위로 이동
	GridObj.setRowColor(selRowId,"#F2F0A0");
	GridObj.moveRow(selRowId,"row_sibling","0");

}

</script>

<style>
    #gridbox {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }
    
    div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
    
</style>

</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding: 0px; overflow: hidden;">

<table align="center" style="width: 100%; height: 100%; margin:0; padding: 0;">
	<tr>
		<td align="center"><div id="gridbox" style="position: relative;"></div></td>
	</tr>
</table>
</body>
</html>