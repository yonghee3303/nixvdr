<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String usbId = (String)request.getParameter("USB_ID");
String drive = (String)request.getParameter("DRIVE");
String driveSize = (String)request.getParameter("DRIVE_SIZE");
String userIp = request.getRemoteAddr(); //접속한 user의 ip를 얻어온다.
String contextPath =request.getContextPath();
%>

<html>
<head>

<title>보안USB 생성창</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<!-- WebSocket 적용 -->
<script type="text/javascript" src="../../../ext/js/websocket.js"></script>

<script>

var G_SERVLETURL = "<%=contextPath%>/mg/enrollUSB.do";

var usbId = "<%=usbId%>";
var drive = "<%=drive%>";

var winSelectUser;

////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
var dhxWins;
var prg_win;
function BwindowWidth() {
	 if (window.innerWidth) {
		 return window.innerWidth;
	 } else if (document.body && document.body.offsetWidth) {
	 	return document.body.offsetWidth;
	 } else {
		 return 0;
	}
}	
function BwindowHeight() {
	 if (window.innerHeight) {
		 return window.innerHeight;
	} else if (document.body && document.body.offsetHeight) {
		 return document.body.offsetHeight;
	} else {
		 return 0;
	}
}
function doQueryDuring()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
}
////////////////////////////////////////////////////프로그래스바 처리때문에 

//사용자선택 팝업창 오픈
function selectUser(){
	winSelectUser = window.open("selectUser.jsp?formid=SU_0107&gridid=GridObj",'','width=530px,height=320px,scrollbars=no,resizable=no');
}
function selectUser_End(userId, userNm, deptNm, position){
	//alert(userId+":"+userNm+":"+deptNm+":"+position);
	document.getElementById("RENDER_ID").value=userId;
	document.getElementById("userNm").innerText=userNm;
	document.getElementById("deptNm").innerText=deptNm;
	document.getElementById("position").innerText=position;
	document.getElementById("USB_NM").value=userId+"_USB";
}

function encodeType(){
	var creType =document.getElementsByName("USB_ENCODE_CL_CD"); //A0504
	var val;
	for(var i=0; i < creType.length; i++){
        if(creType[i].checked){
        	val = creType[i].value;	
        }
    }
	switch(val){
	case "0":
		document.getElementById("folderType").style.display ="block";
		document.getElementById("imageType").style.display = "none";
		break;
	case "1":
		document.getElementById("folderType").style.display ="none";
		document.getElementById("imageType").style.display = "block";
		break;
	}
	
}

//인증 타입에 따른 비밀번호 폼 변경, 웹을 통한 변경 권한 체크항목의 활성화 변경
function ctrTypeAuth(){
	var authType =document.getElementsByName("USB_AUTH_CL_CD");
	var val;
	for(var i=0; i < authType.length; i++){
        if(authType[i].checked){
        	val = authType[i].value;
        }
    }
 	var div_1 = document.getElementById("webDiv");
	var div_2 = document.getElementById("notWebDiv");
	var check_modi = document.getElementById("IS_MODIFIED_USER_AUTH");
	var opt_1 = document.getElementById("NO_SAVE_1");
	var opt_2 = document.getElementById("NO_SAVE_2");
	var opt_3 = document.getElementById("NO_SAVE_3");
	var opt_4 =document.getElementById("NO_SAVE_4"); 
	var authText = document.getElementById("authTypeText");
	
	switch(val){
	case "0": //web인증
		div_1.style.display ="block";
		div_2.style.display = "none";
		check_modi.disabled=false;
		
		opt_1.disabled=false;
		opt_2.disabled=false;
		opt_3.disabled=true;
		opt_4.disabled=true;
		
		opt_1.style.backgroundColor = "white";
		opt_2.style.backgroundColor = "white";
		opt_3.style.backgroundColor = "#c3c3c3";
		opt_4.style.backgroundColor = "#c3c3c3"; 
		
		/* opt_1.style.color = "black";
		opt_2.style.color = "black";
		opt_3.style.color = "white";
		opt_4.style.color = "white";
		 */
		opt_1.selected=true;
		authText.innerHTML ="<%=mm.getMessage("SUMG_1191", s_user.getLocale())%>";
		
		break;
	case "1": //자체 인증
		div_1.style.display = "none";
		div_2.style.display = "block";
		check_modi.disabled=true;
		opt_1.disabled=true;
		opt_2.disabled=true;
		opt_3.disabled=true;
		opt_4.disabled=false;
		
		opt_1.style.backgroundColor = "#c3c3c3";
		opt_2.style.backgroundColor = "#c3c3c3";
		opt_3.style.backgroundColor = "#c3c3c3";
		opt_4.style.backgroundColor = "white";
		
		/* opt_1.style.color = "white";
		opt_2.style.color = "white";
		opt_3.style.color = "white";
		opt_4.style.color = "black"; */
		
		opt_4.selected=true;
		authText.innerHTML  ="<%=mm.getMessage("SUMG_1192", s_user.getLocale())%>";
		
		break;
	case "2": //하이브리드 인증
		div_1.style.display ="block";
		div_2.style.display = "none";
		check_modi.disabled=false;
		opt_1.disabled=false;
		opt_2.disabled=false;
		opt_3.disabled=false;
		opt_4.disabled=true;
		
		opt_1.style.backgroundColor = "white";
		opt_2.style.backgroundColor = "white";
		opt_3.style.backgroundColor = "white";
		opt_4.style.backgroundColor = "#c3c3c3";
		
		/* opt_1.style.color = "black";
		opt_2.style.color = "black";
		opt_3.style.color = "black";
		opt_4.style.color = "white"; */
		
		opt_2.selected=true;
		authText.innerHTML  ="<%=mm.getMessage("SUMG_1193", s_user.getLocale())%>";
		
		break;
	}
	changeNoSave();
}
function changeNoSave(){
	//noSaveText 
	var val = document.getElementById("NO_SAVE").value;
	var saveText = document.getElementById("noSaveText");
	
	switch(val){
	case "1":
		saveText.innerHTML="<%=mm.getMessage("SUMG_1194", s_user.getLocale())%>";//<br/>&nbsp;<br/>&nbsp;
		break;
	case "2":
		saveText.innerHTML="<%=mm.getMessage("SUMG_1195", s_user.getLocale())%>";//<br/>&nbsp;
		break;
	case "3":
		if(document.getElementById("NO_SAVE_3").selected){
			saveText.innerHTML="<%=mm.getMessage("SUMG_1196", s_user.getLocale())%>";//<br/>&nbsp;
		}
		if(document.getElementById("NO_SAVE_4").selected){
			saveText.innerHTML="<%=mm.getMessage("SUMG_1197", s_user.getLocale())%>";
		}
		break;
	
	}
}
//고급설정 텍스트 클릭시 호출
function detailConf() {
	
	var display = document.getElementById("detailConf").style.display;
	
	switch(display){
	
		case "none" :
			document.getElementById("detailConf").style.display = "block";
			break;
		
		case "block" :
			document.getElementById("detailConf").style.display = "none";
			break;
	}
}
//자신의 창 닫기
function close(){
	
	if(winSelectUser != null){
		winSelectUser.close();
	}
	opener.closeWinMappingUser();
}

//사용자선택창 닫기
function closeWinSelectUser(){
	winSelectUser.close();
}

var renderId = ""; //대여자 id
var returnDt = "";	//반납 예정일
var isModified = "0"; //사용자가 정책 수정권한이 부여되는지

//보안USB 생성 버튼을 눌렀을 때... 
function createSecureUSB(){
	
	doQueryDuring();
	var encodeTypeValue =""; //보안영역 생성방식
	var imageSize = "0"; //이미지일 경우 생성용량
	var sizeUnit = ""; //생성용량의 단위 
	var authTypeValue = ""; //인증방식
	var usbNm = ""; //usb명
	
	var isExpireDt = "0";
	var expireDt = ""; // 만료일
	var isReadcount = "0";
	var readcount = "0"; // 실행횟수
	var isProtectWrite = "1"; // 쓰기금지적용
	
	var passwd_w = "";
	var passwd_r = "";
	var passwd_w_confirm = "";
	var passwd_r_confirm = "";
	var isNoPrint = "0";	//문서출력을 금할 것인지
	var isNoCapture = "0";		//클립보드, 스크립 캡쳐 금할 것인지
	var isWaitClose = "0"; // 동작하지 않는다면 unMount할 것인지
	var waitTime = "0";	// 동작하지 않는 시간을 얼마나 기다려줄것인지

	//selectUser에서 전달받음
	renderId= document.getElementById("RENDER_ID").value;
	if(renderId == null || renderId ==""){
		alert("<%=mm.getMessage("GMSG_1016", s_user.getLocale())%>");
		// alert("사용자를 선택해 주세요.");
		doQueryModalEnd();
		return;
	}

	//현재페이지 form에서 전달받음
	encodeType = document.getElementsByName("USB_ENCODE_CL_CD"); //폴더,이미지방식 선택
	for(var i=0; i < encodeType.length; i++){
        if(encodeType[i].checked){
        	encodeTypeValue = encodeType[i].value;	
        }
    }
	
	if(encodeTypeValue=="1"){
		//이미지 방식일 경우 이미지 크기 및 단위
		//alert("이미지방식을 사용합니다.");
		imageSize = document.getElementById("USB_IMAGE_SIZE").value;
		sizeUnit = document.getElementById("SIZE_UNIT").value;
		var drive_size = <%=driveSize%>;
		 if(imageSize == null || imageSize == ""){
			alert("<%=mm.getMessage("SUMG_2003", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}else if(imageSize>=4 && sizeUnit=="GB"){
			alert("<%=mm.getMessage("SUMG_2004", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}else if(imageSize>=4096 && sizeUnit=="MB"){
			alert("<%=mm.getMessage("SUMG_2004", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		} else if(sizeUnit=="MB"){
			if(imageSize > drive_size){
				alert("<%=mm.getMessage("SUMG_2005", s_user.getLocale())%> "+drive_size+"MB"+"<%=mm.getMessage("SUMG_2006", s_user.getLocale())%>");
				doQueryModalEnd();
				return;
			}
		}else if(sizeUnit=="GB"){
			if(imageSize > drive_size/1024){
				alert("<%=mm.getMessage("SUMG_2005", s_user.getLocale())%> "+drive_size/1024+"GB"+"<%=mm.getMessage("SUMG_2006", s_user.getLocale())%>");
				doQueryModalEnd();
				return;
			}
		} 
		
	}
	
	authType = document.getElementsByName("USB_AUTH_CL_CD"); //웹인증, 자체인증방식 선택
	for(var i=0; i < authType.length; i++){
        if(authType[i].checked){
        	authTypeValue = authType[i].value;	
        }
    }
	

	usbNm = encodeURIComponent(document.getElementById("USB_NM").value); // usb명
	if(usbNm ==null || usbNm == ""){
		alert("<%=mm.getMessage("SUMG_2007", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}
	//alert("USB명 : " + usbNm);
	
	returnDt = document.getElementById("calendar_input").value; //일자
	if(returnDt==null || returnDt==""){
		alert("<%=mm.getMessage("SUMG_2008", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}
	 
	if(document.getElementById("IS_EXPIRE_DT").checked){
		isExpireDt = "1";
	expireDt = document.getElementById("calendar_input2").value; //만료일자 사용시 
		
		if(expireDt==null || expireDt==""){
			alert("<%=mm.getMessage("SUMG_2009", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
		
	}

	if(document.getElementById("IS_READCOUNT").checked){
		isReadcount = "1";
		readcount = document.getElementById("READCOUNT").value; //횟수제한 사용시
		
		if(readcount==null || readcount=="" || readcount=="0"){
			alert("<%=mm.getMessage("SUMG_2010", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}

	}
	
	if(authTypeValue=="0"){
	
		isProtectWrite = document.getElementById("NO_SAVE").value; //파일추가,수정가능,보안USB외부로복사금지
		
		if(isProtectWrite!="1"){
			isNoPrint="1";
			isNoCapture="1";
		}
		
		if(document.getElementById("IS_MODIFIED_USER_AUTH").checked){
			isModified = "1";
			//alert("사용자에게 정책 수정권한 부여를 사용합니다.");
		}

		passwd_w = document.getElementById("PASSWD_W_WEB").value;
		passwd_w_confirm = document.getElementById("PASSWD_W_WEB_CONFIRM").value;
		
		if(passwd_w == null || passwd_w == "" || passwd_w !=passwd_w_confirm){
			alert("<%=mm.getMessage("SUMG_2011", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
		passwd_r = "";
		
	}else if(authTypeValue=="1"){
		
		isProtectWrite = document.getElementById("NO_SAVE").value;//"3"; //파일추가,수정금지(읽기전용)
		
		passwd_w = document.getElementById("PASSWD_W").value;
		passwd_r = document.getElementById("PASSWD_R").value;
		passwd_w_confirm = document.getElementById("PASSWD_W_CONFIRM").value;
		passwd_r_confirm = document.getElementById("PASSWD_R_CONFIRM").value;
		
		if(passwd_w != passwd_w_confirm || passwd_r != passwd_r_confirm){
			alert("<%=mm.getMessage("SUMG_2012", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
	}else if(authTypeValue=="2"){

		isProtectWrite = document.getElementById("NO_SAVE").value; //파일추가,수정가능,보안USB외부로복사금지
		
		if(isProtectWrite!="1"){
			isNoPrint="1";
			isNoCapture="1";
		}
		
		if(document.getElementById("IS_MODIFIED_USER_AUTH").checked){
			isModified = "1";
			//alert("사용자에게 정책 수정권한 부여를 사용합니다.");
		}

		passwd_r = document.getElementById("PASSWD_W_WEB").value;
		passwd_r_confirm = document.getElementById("PASSWD_W_WEB_CONFIRM").value;
		
		if(passwd_r == null || passwd_r == "" || passwd_r !=passwd_r_confirm){
			alert("<%=mm.getMessage("SUMG_2011", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
		passwd_w = "";
	}
	
	/* if(document.getElementById("IS_NO_PRINT").checked){
		isNoPrint = "1";
	
	} */
	
	/* if(document.getElementById("IS_NO_CAPTURE").checked){
		isNoCapture = "1";
	
	} */
	
	if(document.getElementById("IS_WAIT_CLOSE").checked){
		isWaitClose = "1";
		
		waitTime = document.getElementById("WAIT_TIME").value;
		
		if(waitTime == null || waitTime == ""){
			alert("<%=mm.getMessage("SUMG_2013", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
	}
	
	alert("<%=mm.getMessage("SUMG_2014", s_user.getLocale())%>");
	
	
	var obj = new Object();
	
	//obj.RENDER_ID = renderId;
	obj.USBID = usbId;
	obj.DRIVE = drive;
	obj.USEIMAGE = encodeTypeValue;
	obj.DRIVESIZE = imageSize;
	obj.DRIVEUNIT = sizeUnit;
	obj.AUTHORITYTYPE = authTypeValue;
	obj.USBTITLE = encodeURIComponent(usbNm);
	//obj.RETURN_DT = returnDt;
	obj.ISUSEEXPIREDATE = isExpireDt;
	obj.EXPIREDATE = expireDt;
	obj.ISCHECKUSECOUNT = isReadcount;
	obj.USECOUNT = readcount;
	//obj.ISMODIFIED = isModified;
	obj.ISPROTECTWRITE = isProtectWrite;
	obj.PASSWORD = passwd_r;
	obj.FULLPASSWORD = passwd_w;
	obj.ISPROTECTPRINT = isNoPrint;
	obj.ISPROTECTCOPY = isNoCapture;
	obj.ISCHECKSLEEP = isWaitClose;
	obj.SLEEPTIME = waitTime;
	
/* 	alert("obj.USBID ="+ usbId+"\n obj.DRIVE ="+ drive+"\n obj.USEIMAGE ="+ encodeTypeValue
			+"\n obj.DRIVESIZE ="+ imageSize+"\n obj.DRIVEUNIT ="+ sizeUnit+"\n obj.AUTHORITYTYPE ="+ authTypeValue
			+"\n obj.USBTITLE ="+ usbNm+"\n obj.ISUSEEXPIREDATE ="+ isExpireDt+"\n obj.EXPIREDATE ="+ expireDt
			+"\n obj.ISCHECKUSECOUNT ="+ isReadcount+"\n obj.USECOUNT ="+ readcount+"\n obj.ISPROTECTWRITE ="+ isProtectWrite
			+"\n obj.PASSWORD ="+ passwd_r+"\n obj.FULLPASSWORD ="+ passwd_w+"\n obj.ISPROTECTPRINT ="+ isNoPrint
			+"\n obj.ISPROTECTCOPY ="+ isNoCapture+"\n obj.ISCHECKSLEEP ="+ isWaitClose+"\n obj.SLEEPTIME ="+ waitTime);
	 */
	//보안USB생성 서비스 호출
	var policy = JSON.stringify(obj);
	var url = G_SERVLETURL + "?mod=createSecureUSB&policy=" + policy;
	
	sendRequest(createSecureUSBEnd, " " ,"POST", url , false, false);
	
}

function createSecureUSBEnd(oj){

	
	// var result =document.USBInfo.CreateSecureUSB(oj.responseText);
	
	websock.send(oj.responseText);
	
	//alert("document.USBInfo.CreateSecureUSB(jData)********************"+result);
	
	
	
	
	//opener.closeWinMappingUser();
}

function onOpen(evt){
    //called as soon as a connection is opened
    // 성공을 확인 해야함.
} // end onOpen

function onClose(evt){
    //called when connection is severed
} // end onClose;

function onMessage(evt) {

	// {"return":"TRUE", "errcode":"0","errmsg":"Success"}
	
	var jData = JSON.parse(evt.data);
	
	if(jData.errcode == "0") // 통신성공
	
    if(jData.result == "TRUE") // 작업성공    
	
	// 반출이 성공적으로 끝나면, 해당 정책정보를 반출테이블에 저장
	/* var obj = new Object();
	var policy = JSON.stringify(obj); */
	var url = G_SERVLETURL + "?mod=savePolicy&RENDER_ID="+renderId +"&RETURN_YMD="+returnDt + "&IS_MODIFIED_USER_AUTH=" + isModified + "&policy=" + oj.responseText+"&USER_ID="+"<%=s_user.getId() %>";
	url+="&IP_ADDR="+"<%=userIp%>";
	sendRequest(savePolicyEnd, " " ,"POST", url , false, false);
    
} // end onMessage

function onError(evt){
    //called on error
    // 실패시 사용자 Alert
} // end onError

function savePolicyEnd() {
	/* alert("보안USB가 성공적으로 반출되었습니다.");
	var ifr = opener.parent.cell3.getFrame();
	ifr.contentWindow.location.reload(true); */
	doQueryModalEnd();
	alert("<%=mm.getMessage("SUMG_1003", s_user.getLocale())%>");
	
	close();
	
	var checkList_ifr = opener.parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	
	var export_ifr = opener.parent.cell3.getFrame();//반출 refresh
	export_ifr.contentWindow.refresh();
	var import_ifr = opener.parent.cell2.getFrame();//보유 refresh
	import_ifr.contentWindow.refresh();
	
	export_ifr.contentWindow.selectedUSB(usbId);
	
	var summary_ifr = opener.parent.subCell3.getFrame();
	summary_ifr.contentWindow.createUSBEnd();
	
}
var calendar1;
var calendar2;

function init(){
	
	if(dhxWins == null) {
    	dhxWins = new dhtmlXWindows();
    	dhxWins.setImagePath("<%=contextPath%>/dthmlx/dhtmlxWindows/codebase/imgs/");
    }

	if(prg_win == null) {
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;
		
		prg_win = dhxWins.createWindow("prg_win", top, left, 180, 110);
		prg_win.setText("Please wait for a moment.");
		prg_win.button("close").hide();
		prg_win.button("minmax1").hide();
		prg_win.button("minmax2").hide();
		prg_win.button("park").hide();
		dhxWins.window("prg_win").setModal(false);
		prg_win.attachURL("../../../ext/include/progress_ing.htm");
		dhxWins.window("prg_win").hide();
	}

	
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar1 = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar2 = new dhtmlXCalendarObject({input:"calendar_input2", button:"calendar_icon2"});
	calendar2.loadUserLanguage("<%=s_user.getLocale()%>");
	
	calendar1.hideTime();
	calendar2.hideTime();
	
	//기본값으로 반납일을 한달 후로 셋팅
	var now = new Date();
	
	// 년월일시분을 가져옴
	var year  = now.getFullYear();
    var month = now.getMonth() + 2; // 1월=0,12월=11이므로 1 더함
    var day   = now.getDate();
    var hour  = now.getHours();
    var minute   = now.getMinutes();

    // 각각 2자리수로 만듬
    if (("" + month).length == 1) { month = "0" + month; }
    if (("" + day).length   == 1) { day   = "0" + day;   }
    if (("" + hour).length  == 1) { hour  = "0" + hour;  }
    if (("" + minute).length   == 1) { minute   = "0" + minute;   }

    if(month>12){
    	year+=1;
    	month = 1;
    }
    if(month==4 || month==6 || month==9 || month==11){
    	if(day==31){
    		day = 30;
    	}
    }else if(month==2){
    	if(day>28){
    		day=28;
    	}
    }
    
    document.getElementById("calendar_input").value = "" + year + "-" + month + "-" + day;
    document.getElementsByName("USB_ENCODE_CL_CD")[0].checked =true;
	document.getElementsByName("USB_AUTH_CL_CD")[2].checked =true;
	ctrTypeAuth();
	 
	WebSocketConnect();
}

function checkPass(){
	if(document.getElementById("PASSWD_W_WEB_CONFIRM").value==document.getElementById("PASSWD_W_WEB").value){
		document.getElementById("agree").style.visibility ="visible";
	}else{
		document.getElementById("agree").style.visibility ="hidden";
	}
}
function checkPass_W(){
	if(document.getElementById("PASSWD_W_CONFIRM").value==document.getElementById("PASSWD_W").value){
		document.getElementById("agree_w").style.visibility ="visible";
	}else{
		document.getElementById("agree_w").style.visibility ="hidden";
	} 
}
function checkPass_R(){
	if(document.getElementById("PASSWD_R_CONFIRM").value==document.getElementById("PASSWD_R").value){
		document.getElementById("agree_r").style.visibility ="visible";
	}else{
		document.getElementById("agree_r").style.visibility ="hidden";
	} 
}

</script>

<style>
 /* tr, td, div {
 font-family: Tahoma;
 } */
 	
 	.board-search { width:100%; margin-bottom:10px; border:0;}
	
	.board-search td { font-size:12px; text-align:left; border-bottom:0; background:#ffffff;}

	.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(../../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}
 
</style>


</head>
<body onload="init();" unload="closeWinSelectUser();" style="margin: 0px; padding:10px0px,0px,0px; overflow: auto;">
<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->

<form name="form" method="post">
<table class="board-search" align="center" style="width: 90%; margin:0px; padding: 0;">
	<tr>
		<td colspan="3" style="padding-left:0px;">
			<table style="width:100%;"><!-- 사용자 선택 -->
				<colgroup>
					<col width="60px" />
          		 	<col />
           			<col width="110px" />
				</colgroup>
				<tr>
					<td class="tit" colspan="3">
						<b><%=mm.getMessage("SUMG_1135", s_user.getLocale())%></b>
					</td>
				</tr>
				<tr>
					<td style="padding-top:9px;">&nbsp;&nbsp;&nbsp;&nbsp;<%=mm.getMessage("SUMG_1137", s_user.getLocale())%> : &nbsp;</td>
					<td style="padding-top:5px;">
						<div style="height:100%; text-align:center; background-color:#dfeff5; border:1px solid #9ebcce; margin-top: 2px; padding: 2px 0px 2px 2px;" >
							<input type="hidden" id="RENDER_ID">
						<span id="deptNm"></span>&nbsp;/&nbsp;<span id="userNm"></span>&nbsp;/&nbsp;<span id="position"></span>
						</div>
					</td>
					<td>
						<div class="btnarea">
				           <a href="javascript:selectUser();" class="btn"><span><%=mm.getMessage("SUMG_1136", s_user.getLocale())%></span></a>
				        </div>
					</td>
					</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="padding:0px,0px,0px,0px;">
			<table style="width:100%;"><!-- 보안영역 생성방식 -->
			<colgroup>
				<col width="30%">
				<col width="70%">
			</colgroup>
				<tr>
					<td class="tit" colspan="2">
						<b><%=mm.getMessage("SUMG_1138", s_user.getLocale())%></b>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding:0px,0px,10px,16px;">
						<%-- <input type="radio" name="USB_ENCODE_CL_CD" value="0" onclick="folderType();" checked="checked"><%=mm.getMessage("SUMG_1139", s_user.getLocale())%>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="USB_ENCODE_CL_CD" value="1" onclick="imageType();"><%=mm.getMessage("SUMG_1140", s_user.getLocale())%> --%>
						<!-- encodeType -->
						<% ICodeManagement enCode=(ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
									List encCode = enCode.getCodes("A0504");
								
									for (Iterator all = encCode.iterator(); all.hasNext();) {
										ICode icd = (ICode) all.next();
				
							       		String code = icd.getId();
							       		String cdNm = icd.getName();
							        	if("en".equals(s_user.getLocale().getLanguage()) ) {
							        		cdNm = icd.getNameSl();
							        	}
							        	String cdKey = icd.getCd();
							        
							       	boolean isShow = true;
							        
							        if(isShow) {
								%>
									<input type="radio" name="USB_ENCODE_CL_CD" value="<%=cdKey %>" onclick="encodeType();"><%=cdNm%>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<%
							      	  }
							 	 }
								
							%>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding:0px,0px,10px,16px;">
						<div id="folderType" style="margin-bottom:4px; padding:4px 0px 2px 0px;height:27px; text-align:center; display: table-cell; vertical-align:middle; border:1px solid #9ebcce;">
							<table>
								<tr>
									<td>
										<%=mm.getMessage("SUMG_1141", s_user.getLocale())%>
									</td>
								</tr>
							</table>
						</div>
						<div id="imageType" style="height:27px; text-align:center; vertical-align:middle; border:1px solid #9ebcce; display:none;">
							<table>
								<tr>
									<td>
										<%=mm.getMessage("SUMG_1142", s_user.getLocale())%>
									</td>
									<td>
										<input class="text" id="USB_IMAGE_SIZE" type="text" size="8" style="margin-top: 1px;"/>
										<select id="SIZE_UNIT" style="margin-top:2px;">
											<option value="GB">GB
											<option value="MB">MB
										</select>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<!-- 보안USB 인증방식 -->
				<tr><td class="tit" colspan="2">
						<b><%=mm.getMessage("SUMG_1165", s_user.getLocale())%> <%=mm.getMessage("SUMG_1186", s_user.getLocale())%></b>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding:0px,0px,10px,16px;">
						<table>
							<colgroup>
							<col width="30%">
							<col width="30%">
							<col width="40%">
							</colgroup>
							<tr>
								<% ICodeManagement MCode=(ICodeManagement) ComponentRegistry.lookup(ICodeManagement.class);
									List sCode = MCode.getCodes("A0503");
								
									for (Iterator all = sCode.iterator(); all.hasNext();) {
										ICode icd = (ICode) all.next();
				
							       		String code = icd.getId();
							       		String cdNm = icd.getName();
							        	if("en".equals(s_user.getLocale().getLanguage()) ) {
							        		cdNm = icd.getNameSl();
							        	}
							        	String cdKey = icd.getCd();
							        
							       	boolean isShow = true;
							        
							        if(isShow) {
								%>
								<td style="padding:0px,0px,7px,0px;">
									<input type="radio"  name="USB_AUTH_CL_CD" value="<%=cdKey %>" onclick="ctrTypeAuth();"><%=cdNm%>
								</td>
							<%
							      	  }
							 	 }
							%>
							</tr>
							<tr>
								<td colspan="3" style="width:400px; padding-left:0px;"><font color="#c3c3c3" id="authTypeText" style="font-size:10px;">* 각 인증방식에 관한 보조설명</font></td>
							</tr>
						</table>
						</td>
				</tr>
				<!-- 보안USB 설정 -->
				<tr><td class="tit" colspan="2">
						<b><%=mm.getMessage("SUMG_1165", s_user.getLocale())%> <%=mm.getMessage("SUMG_1188", s_user.getLocale())%></b>
					</td>
				</tr>
				<tr>
					<td style="padding:0px,0px,10px,20px;">
						<%=mm.getMessage("SUMG_1110", s_user.getLocale())%>
					</td>
					<td style="padding:0px,0px,10px,16px;">
						<input class="text" id="USB_NM" type="text" size="10" />
					</td>
				</tr>
				<tr><td style="padding:0px,0px,10px,20px;">
						<%=mm.getMessage("SUMG_1105", s_user.getLocale())%>
					</td>
					<td style="padding:0px,0px,10px,16px;">
					<select id='NO_SAVE'  onchange="changeNoSave()">
							<option id='NO_SAVE_1' value='1'> <%=mm.getMessage("SUMG_1106", s_user.getLocale())%>
							<option id='NO_SAVE_2' value='2'> <%=mm.getMessage("SUMG_1107", s_user.getLocale())%>
							<option id='NO_SAVE_3' value='3'> <%=mm.getMessage("SUMG_1108", s_user.getLocale())%>
							<option id='NO_SAVE_4' value='3'> <%=mm.getMessage("SUMG_1132", s_user.getLocale())%>
					</select> 
					</td>
				</tr>
				<tr><td colspan="2" style="padding:0px,0px,10px,20px;"><font style="font-size:10px;" id="noSaveText"color="#c3c3c3">* 각 권한에 관한 보조설명 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td></tr>
				<tr>
					<td style="padding:0px,0px,10px,20px;">
						<%=mm.getMessage("SUMG_1145", s_user.getLocale())%>
					</td>
					<td style="padding:0px,0px,10px,16px;">
						<input class="text" type='text' id='calendar_input' value='' size="10"> <img id='calendar_icon' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand">
					</td>
				</tr>
				<!-- 비밀번호 설정 -->
				<tr><td class="tit" colspan="2">
						<b><%=mm.getMessage("SUMG_1147", s_user.getLocale())%> <%=mm.getMessage("SUMG_1188", s_user.getLocale())%></b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><!-- 웹제어 유무 Div 부분 -->
		<td colspan="3" style="padding:0px,0px,20px,16px;">
			<div id="webDiv" style="display:block;">
				<table style="width: 100%;">
					<tr>
						<td style="padding:5px,0px,10px,4px;">
							<%=mm.getMessage("SUMG_1147", s_user.getLocale())%>
						</td>
						<td style="padding:5px,0px,10px,125px;">
							<input id="PASSWD_W_WEB" class="text" type="password" size="10" />
						</td>
					</tr>
					<tr>
						<td style="padding:0px,0px,10px,4px;">
							<%=mm.getMessage("SUMG_1130", s_user.getLocale())%>
						</td>
						<td style="padding:0px,0px,10px,125px;">
							<input id="PASSWD_W_WEB_CONFIRM" class="text" type="password" size="10" onkeyup="checkPass()"/>
							<img src="../../../ext/images/agree.jpg" id="agree" style="visibility:hidden; height:13px;"/>						
						</td>
					</tr>
				</table>
			</div>
			<div id="notWebDiv" style="display:none;">
				<table style="width: 100%;">
					<tr>
						<td style="padding:5px,0px,10px,4px;">
							<%=mm.getMessage("SUMG_1148", s_user.getLocale())%>
						</td>
						<td style="padding:5px,0px,10px,35px;">
							<%=mm.getMessage("SUMG_1149", s_user.getLocale())%>
						</td>
					</tr>
					<tr>
						<td style="padding:0px,0px,10px,4px;">
							<%=mm.getMessage("SUMG_1150", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="PASSWD_W" class="text" type="password" size="10" />
						</td>
						<td style="padding:0px,0px,10px,35px;">
							<%=mm.getMessage("SUMG_1150", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="PASSWD_R" class="text" type="password" size="10" />
						</td>
					</tr>
					<tr>
						<td style="padding:0px,0px,0px,4px;">
							<%=mm.getMessage("SUMG_1151", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="PASSWD_W_CONFIRM" class="text" type="password" size="10" onkeyup="checkPass_W()"/>
							<img src="../../../ext/images/agree.jpg" id="agree_w" style=" visibility:hidden; height:13px;"/>
						</td>
						<td style="padding:0px,0px,0px,35px;">
							<%=mm.getMessage("SUMG_1151", s_user.getLocale())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="PASSWD_R_CONFIRM" class="text" type="password" size="10" onkeyup="checkPass_R()"/>
							<img src="../../../ext/images/agree.jpg" id="agree_r" style=" visibility:hidden; height:13px;"/>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td class="tit" colspan="3"> <!-- 고급 설정 -->
			<a href="javascript:detailConf();"><font style="color:blue;"><%=mm.getMessage("SUMG_1152", s_user.getLocale())%></font></a>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="padding:10px,0px,0px,16px;">
			<div id="detailConf" style="border:1px solid #9ebcce; padding: 8px 0px 2px 2px; display:none;">
			<!-- <div id="detailConf" style="border: 2px solid #a1ceed; margin-top: 2px; padding: 2px 0px 2px 2px;"> -->
				<table>
					<tr>
						<td style="padding:0px,0px,10px,0px;  ">
							<input id="IS_EXPIRE_DT" type="checkbox" /><%=mm.getMessage("SUMG_1111", s_user.getLocale())%>
						</td>
						<td style="padding:0px,0px,10px,0px;  ">
							<input class="text" type='text' id='calendar_input2' value='' size="10"> <img id='calendar_icon2' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand">
						</td>
					</tr>
					<tr>
						<td style="padding:0px,0px,10px,0px;  ">
							<input type="checkbox" id="IS_READCOUNT" /><%=mm.getMessage("SUMG_1112", s_user.getLocale())%>
						</td>
						<td style="padding:0px,0px,10px,0px;  ">
							<input class="text" id="READCOUNT" type="text" size="10" /> <%=mm.getMessage("SUMG_1113", s_user.getLocale())%>
						</td>
					</tr>
					<%-- <tr>
						<td style="padding:0px,0px,10px,4px;" colspan="2">
							<%=mm.getMessage("SUMG_1114", s_user.getLocale())%>
						</td>
					</tr> --%>
					<%-- <tr>
						<td style="padding:0px,0px,10px,0px;" colspan="2">
							<input id="IS_NO_PRINT" type="checkbox" />
							<%=mm.getMessage("SUMG_1115", s_user.getLocale())%>
						</td>
					</tr>
					<tr>
						<td style="padding:0px,0px,10px,0px;" colspan="2">
							<input id="IS_NO_CAPTURE" type="checkbox" />
							<%=mm.getMessage("SUMG_1116", s_user.getLocale())%>
						</td>
					</tr> --%>
					<tr>
						<td style="padding:0px,0px,10px,0px;  " colspan="2">
							<input id="IS_WAIT_CLOSE" type="checkbox" />
							<%=mm.getMessage("SUMG_1117", s_user.getLocale())%>
						</td>
					<tr>
						<td style="padding:0px,0px,0px,25px;  " colspan="2"><%=mm.getMessage("SUMG_1118", s_user.getLocale())%>
							<input id="WAIT_TIME" type="text" class="text" size="10"> <%=mm.getMessage("SUMG_1119", s_user.getLocale())%>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding:0px,0px,10px,0px;  ">
							<input type="checkbox" id="IS_MODIFIED_USER_AUTH" /><%=mm.getMessage("SUMG_1146", s_user.getLocale())%>
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td style="padding:35px,0px,0px,0px;">
		</td>
		<td style="padding:35px,0px,0px,0px;">
		</td>
		<td style="padding:35px,0px,0px,0px;">
			<div class="btnarea">
	            	<a href="javascript:close();" class="btn"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a>
	            	<a href="javascript:createSecureUSB();" class="btn"><span><%=mm.getMessage("SUMG_1153", s_user.getLocale())%></span></a>
	        </div>
		</td>
	</tr>
</table>
</form>

</body>
</html>