<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
	IUser s_user = (IUser) session.getAttribute("j_user");
	String linkId = request.getParameter("LINK_ID");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String contextPath = request.getContextPath();
%>
<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<%@ include file="../../../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/standbyMgmt.do";

var usbDrive="";
var usbModel;
var size;
var sizeInt;
var usbTypeCd;
var usbId="";
var enckey = "";

function init(){
	doAjax();
}
function doAjax(){
	var url = G_SERVLETURL + "?mod=getDetailStandby&LINK_ID=<%=linkId%>"; 
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false); 
}
//실질적인 정보를 화면에 뿌려주는 부분
function doAjaxEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	
	document.getElementById("RENDER_ID").innerText=jData.RENDER_ID;
	document.getElementById("RENDER_NM").innerText=jData.RENDER_NM;
	document.getElementById("DEPT_NM").innerText=jData.DEPT_NM;
	if(jData.IS_EXPIRE_DT == "0"){
		document.getElementById("EXPIRE_YMD").innerText="제한 없음";			
	}else if(jData.IS_EXPIRE_DT =="1"){
		document.getElementById("EXPIRE_YMD").innerText=jData.EXPIRE_YMD;		
	}
	usbTypeCd=jData.USB_TYPE_CD;
	if(jData.USB_TYPE_CD == "U"){
		document.getElementById("USB_TYPE").innerText="USB";
	}else if(jData.USB_TYPE_CD == "H"){
		document.getElementById("USB_TYPE").innerText="외장 하드";
	}
	document.getElementById("RENT_DESC").innerText=jData.RENT_DESC;
	
	usbList.location.href="/mg/su/main/selectUSB.jsp?formid=SU_0109&gridid=GridObj&usbTypeCd="+usbTypeCd;
}

function getEnckey() {
	
/* 	var url = G_SERVLETURL + "?mod=selectEnckey";
	sendRequest(getEnckeyEnd, " " ,"POST", url , false, false); */
}


function getEnckeyEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);
	
	enckey = jData.ENCKEY;
	
}
function deliver(drive, model, usbSize, usbType) {

	usbDrive =drive;
	usbModel=model;
	size=usbSize;
	usbTypeCd=usbType;
	
	var arr_usbSize = usbSize.split(' GB');
	sizeInt = parseInt(arr_usbSize[0]) * 1024;
	
	// 보안USB인지 체크
	var isSecureUSB = document.USBInfo.IsSecureUSBDrive(drive, enckey);
	
	 // 리턴값의 구분자를 삭제 : 배열[1]에 구분에 필요한 데이터가 담긴다
	var array = isSecureUSB.split('{}[]');
	isStatus = array[1];
	
	// 보안USB인 경우 보유/반출을 구분하기 위한 처리
	if(isStatus=="-1" || isStatus=="0"){
		usbId = "";
		enrollUSB(drive,usbType);
	}else{
		usbId= array[3];
		//USBID 값으로 DB에서 보유/반출 상태 확인
		var url = "<%=contextPath%>/mg/enrollUSB.do?mod=checkUSB&USB_ID=" + usbId+"&usbType="+usbType;
		sendRequest(checkUSBEnd, " " ,"POST", url , false, false);	
	}
}

function checkUSBEnd(oj){
	var jData = JSON.parse(oj.responseText);

	 var status = jData.haveUsb;
	 // 버튼 활성/비활성화 처리
	 if("true"==status){ //보유 USB
		if(confirm("해당 USB로 반출 하시겠습니까?")){
			location.href="/mg/su/main/selectPolicy.jsp?LINK_ID=<%=linkId%>&usbDrive="+usbDrive+"&usbModel="+usbModel+"&usbSize="+size+"&usbType="+usbTypeCd+"&USB_ID="+usbId;
		}
	 }else if("false"==status){ //반출 USB
		alert("이미 반출된 USBd입니다. 회수 후 반출하시기 바랍니다.");
	 }
}
function enrollUSB(selDrive, selUsbType) {
	//드라이브가 선택되지 않은 경우
	if(selDrive==""){
		alert("<%=mm.getMessage("SUMG_2016", s_user.getLocale())%>");
		return;
	}
	alert("<%=mm.getMessage("SUMG_2014", s_user.getLocale())%>");
	var url = "<%=contextPath%>/mg/enrollUSB.do?mod=selectEnrollUSB&drive=" + selDrive+"&usbType="+selUsbType;
	sendRequest(enrollUSBEnd, " " ,"POST", url , false, false);	
}
function enrollUSBEnd(oj){
	var jData = JSON.parse(oj.responseText);
	
	if(jData.RESULT == "false"){
		alert(jData.ERROR);
		return;
	}

	var result =document.USBInfo.CreateSecureUSB(oj.responseText);
	//alert("document.USBInfo.CreateSecureUSB(jData)********************"+result);
	
	var array= result.split('{}[]');

	var url ="<%=contextPath%>/mg/enrollUSB.do?mod=enrollUSB";
	
	var	USB_ID = jData.USBID;
	url+="&USB_ID="+USB_ID;
	
	url+="&USB_CAPACITY="+sizeInt;
	url+="&usbType="+usbTypeCd;
	url+="&USB_MODEL_NM="+usbModel;
	
	var USB_KEY=array[1];
	url+="&USB_KEY="+USB_KEY;
	
	var USER_ID="<%=s_user.getId().toString()%>";
	
	url+="&ADMIN_ID="+USER_ID;
	
	var REG_ID="<%=s_user.getId().toString()%>";
	url+="&REG_ID="+REG_ID;
	sendRequest(updateImportList, " " ,"POST", url , false, false);
}

function updateImportList(){
	
	var checkList_ifr = opener.parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	
	var import_ifr = opener.parent.subCell4.getFrame();//보유 refresh
	import_ifr.contentWindow.refresh();
	isStatus="1";

	if(confirm("등록이 완료 되었습니다. 해당 외장 매체로 반출 진행하시겠습니까?")){
		deliver(usbDrive, usbModel, size, usbTypeCd);
	}
}
</script>
<style>
	td {
		padding:8px;
		font-size: 12px;
		border-bottom:1px solid #d9d9d9; 
	}
</style>

</head>
<body onload="init();" style="padding:10px; overflow:hidden;">
<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->
<form name="form">
<table align="center" style="width:100%;">
	<colgroup>
		<col width="25%">
		<col width="25%">
		<col width="25%">
		<col width="25%">
	</colgroup>
	<!-- 보안usb 신청정보 -->
	<tr> 
		<td colspan="4" style="border-bottom:2px solid #2281d1;">
			<%=mm.getMessage("SUMG_1211", s_user.getLocale())%>
		</td>
	</tr>
	<!-- 사용자명 -->
	<tr> 
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1203", s_user.getLocale())%></td>
		<td colspan="3"><font style="font-weight:bold;" id="RENDER_NM"></font></td>
	</tr>
	<!-- 사용자id, 근무지 -->
	<tr> 
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1204", s_user.getLocale())%></td>
		<td><font style="font-weight:bold;" id="RENDER_ID"></font></td>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1205", s_user.getLocale())%></td>
		<td><font style="font-weight:bold;" id="DEPT_NM"></font></td>
	</tr>
	<!-- 사용기간, 요청기기 -->
	<tr> 
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1206", s_user.getLocale())%></td>
		<td><font style="font-weight:bold;" id="EXPIRE_YMD"></font></td>
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1207", s_user.getLocale())%></td>
		<td><font style="font-weight:bold;" id="USB_TYPE"></font></td>
	</tr>
	<!-- 요청사유 -->
	<tr> 
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1208", s_user.getLocale())%></td>
		<td colspan="3"><font style="font-weight:bold;" id="RENT_DESC"></font></td>
	</tr>
	<!-- 반출가능USB -->
	<tr> 
		<td style="background-color:#ebf2f5;"><%=mm.getMessage("SUMG_1209", s_user.getLocale())%></td>
		<td colspan="3">
			<iframe name="usbList" id="usbList" title="usbList" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no' style="width:100%; height:300px;"></iframe>
		</td>
	</tr>
	<tr>
		<td colspan="4" style="border-bottom:0px;">
		<font color="blue"> 외장매체 타입을 선택 후 해당 리스트를 더블 클릭하시면 다음 단계로 진행됩니다. </font>
		</td>
	</tr>
</table>
</form>
</body>
</html>