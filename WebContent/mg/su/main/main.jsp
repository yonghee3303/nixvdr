<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IUser s_user = (IUser) session.getAttribute("j_user");
%>
<html>
<head>

<title>Main</title>


<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp" %>

<script>
var layout;
var subLayout1;
var subLayout2;
/* var layout2;
var layout3; */
var cell1;
var cell2;
var subCell1;
var subCell2;
var subCell3;
var subCell4;
var subCell5;
var subCell6;

var chartTabbar;

var chartTab_1;
var chartTab_2;
var chartTab_3;
	

function upImportImg(){
	
	document.getElementById("importImg").src = "../../../ext/images/button/plus_down.gif";
	
}

function downImportImg() {
	
	document.getElementById("importImg").src = "../../../ext/images/button/plus_up.png";
}

function upExportImg(){
	
	document.getElementById("exportImg").src = "../../../ext/images/button/plus_down.gif";
	
}

function downExportImg() {
	
	document.getElementById("exportImg").src = "../../../ext/images/button/plus_up.png";
}


<%-- function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "2E",
	    skin: "dhx_web"
	});

	cell1 = layout.cells('a');
	cell1.setText("<%=mm.getMessage("NIMM_1001", s_user.getLocale())%>");
	
	cell2 = layout.cells('b');
	cell2.setText("<%=mm.getMessage("NIMM_1001", s_user.getLocale())%>");
	
	subLayout1 = cell1.attachLayout({
	    pattern: "3W",
	    skin: "dhx_skyblue"
	});
	
	subCell1 = subLayout1.cells('a');
	subCell1.setText("<%=mm.getMessage("NIMM_1002", s_user.getLocale())%>");
	subCell1.attachURL("ni_MainProProj.jsp");
	
 	/* subCell2 = subLayout1.cells('b');
	subCell2.setText("");
	subCell2.attachURL("ni.jsp"); */
	
	subCell2 = subLayout1.cells('b');
	subCell2.setText("<%=mm.getMessage("NIMM_1003", s_user.getLocale())%>");
	subCell2.fixSize(true,true);
	
	//tabbar 생성 //chart는 아직 들어가있지 않음.
	chartTabbar = subCell2.attachTabbar({
		align: "left",
		mode: "top",
		tabs:[
		      {id: "Donut", text:"", active: true},
		      {id: "Pie3D", text:""},
		      {id: "Bar", text: ""}
		      ]
	});
	
	subCell3 = subLayout1.cells('c');
	subCell3.setText("<%=mm.getMessage("NIMM_1004", s_user.getLocale())%>");
	subCell3.attachURL("ni_MainContract.jsp");
	
	
	<%	if("Y".equals(pm.getString("component.usb.standbyList.useYn"))){	%>
	subLayout2 = cell2.attachLayout({
	    pattern: "3W",
	    skin: "dhx_web"
	});
	<%	}else{	%>
	subLayout2 = cell2.attachLayout({
	    pattern: "2U",
	    skin: "dhx_web"
	});
	<%}%>
	
	subCell4 = subLayout2.cells('a');
	subCell4.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'>"+"<%=mm.getMessage("NIMM_1005", s_user.getLocale())%></td>"+"<td id='go'><a onclick='goImport();'><img id='importImg' src='../../../ext/images/button/plus_up.png' onmouseover='upImportImg();' onmouseout='downImportImg();' /></a></td></tr></table>");
	subCell4.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'>"+"<%=mm.getMessage("NIMM_1005", s_user.getLocale())%></td></tr></table>");
	subCell4.fixSize(true,true);
	subCell4.attachURL("ni_MainInterview.jsp", null, {
		formid: "SU_0105",
		gridid: "GridObj"
	});
	<% if("Y".equals(pm.getString("component.usb.standbyList.useYn"))){%>
	
	subCell5 = subLayout2.cells('b');
	//subCell5.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'>"+"<%=mm.getMessage("SUMG_1202", s_user.getLocale())%>"+"</td><td id='go'><a onclick='goStandby();'><img id='importImg' src='../../../ext/images/button/plus_up.png' onmouseover='upImportImg();' onmouseout='downImportImg();' /></a></td></tr></table>");
	subCell5.setText("<%=mm.getMessage("SUMG_1202", s_user.getLocale())%>");
	subCell5.fixSize(true,true);
	subCell5.attachURL("standbyList.jsp", null, {
		formid: "SU_0110",
		gridid: "GridObj"
	});
	
	subCell6 = subLayout2.cells('c');
	
	<%}else{%>
	
	subCell6 = subLayout2.cells('b');
	
	<%}%>
	subCell6.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'>"+"<%=mm.getMessage("NIMM_1006", s_user.getLocale())%></td>"+"<td id='go'><a onclick='goExport();'><img id='exportImg' src='../../../ext/images/button/plus_up.png' onmouseover='upExportImg();' onmouseout='downExportImg();' /></a></td></tr></table>");
	subCell6.setText("<table style='padding:0; margin0; width:100%; height:100%;'><tr><td id='caption'>"+"<%=mm.getMessage("NIMM_1006", s_user.getLocale())%></td></tr></table>");
	subCell6.attachURL("", null,  {
		formid: "SU_0106",
		gridid: "GridObj"
	});
	
	
	//subCell2.showHeader();
	cell1.showHeader();
	cell1.setHeight(320);
	
	cell1.hideArrow();
	cell2.hideArrow();
	subCell1.hideArrow();
	subCell2.hideArrow();
	subCell3.hideArrow();
	subCell4.hideArrow();
	<% if("Y".equals(pm.getString("component.usb.standbyList.useYn"))){%> subCell5.hideArrow(); <%}%>
	subCell6.hideArrow();
	
} --%>

function init() {

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "2E",
	    skin: "dhx_web"
	});
	
	
	cell1 = layout.cells('a');
	cell1.setHeight(570);

	//cell1.setText("<table style='padding:0; margin0; width:98%; height:100%;'><tr><td id='caption'>"+leftMenuName+"</td></tr></table>");
	
	cell1.setText("프로젝트 등록");
	
	cell1.attachURL("../../../mg/ni/ni_ProjMgnt.jsp", null,  {
		formid: "NIM_0104",
		gridid: "GridObj",
		user_id: ""
	});
	
	
	
	cell2 = layout.cells('b');
	
	cell2.hideHeader();
	
	cell2.attachURL("../../../mg/ni/ni_ProjSub.jsp", null,  {
		formid: "NIM_010401",
		gridid: "<GridObj",
		user_id: ""
	});
}


function refresh(proj_no) 
{	
	cell2.attachURL("../../../mg/ni/ni_ProjSub.jsp", null,  {
		formid: "NIM_010401",
		gridid: "GridObj",
		projNo: proj_no
	});
	
}

function resize() {
	//location.reload(true);
	layout.setSizes();
	
}

function goImport(){	
	parent.mainFrame.location.href ='../mgmt/importMgmtLayout.jsp';
}
function goExport(){
	parent.mainFrame.location.href ='../mgmt/exportMgmtLayout.jsp';
}

//checkList.jsp에서 전달받은 값으로 chart를 만든다.
function createChart(allCnt, haveCnt, rentCnt, expireCnt, lostCnt, banCnt, expireDtCnt){
	
	var data=[{sales: haveCnt, year: "<%=mm.getMessage("SUMG_1122", s_user.getLocale())%>", color:"#4477C5"},
	          {sales: rentCnt, year: "<%=mm.getMessage("SUMG_1123", s_user.getLocale())%>", color:"#9FCF4E"},
	          <% if("Y".equals(pm.getString("component.usb.returnDay.useYn"))){%>
	          {sales: expireCnt, year: "<%=mm.getMessage("SUMG_1124", s_user.getLocale())%>", color:"#F2F0A0"},
	          <%}%>
	          {sales: lostCnt, year: "<%=mm.getMessage("SUMG_1101", s_user.getLocale())%>", color:"#DDA55C"},
	          {sales: banCnt, year: "<%=mm.getMessage("SUMG_1126", s_user.getLocale())%>", color:"#EB6A6C"},
	          {sales: expireDtCnt, year: "<%=mm.getMessage("SUMG_1201", s_user.getLocale())%>", color:"#A962E3"}]; 
	
	//tab에 차트 생성
	chartTab_1 = chartTabbar.tabs("Donut").attachChart({
		view: "donut",
		value: "#sales#",
		color:"#color#",
		legend: {
			width: 100,
			align: "right",
			valign: "middle",
			template: "#year#"
		},
		labelOffset:-5,
		 label:function(obj){
             return "<div class='label'style='border:1px solid "+obj.color+"'>"+obj.sales+"</div>";
         }
	}).parse(data,"json");
	
	chartTab_2 = chartTabbar.tabs("Pie3D").attachChart({
		view: "pie3D",
		value: "#sales#",
		legend: {
			width: 100,
			align: "right",
			valign: "middle",
			template: "#year#"
		},
		color:"#color#",
		labelOffset:-5,
		 label:function(obj){
             return "<div class='label'style='border:1px solid "+obj.color+"'>"+obj.sales+"</div>";
         }
	}).parse(data,"json");
	 
	
	
	chartTab_3=chartTabbar.tabs("Bar").attachChart({
		view: "barH",
		value: "#sales#",
		labelOffset:-5,
		 label:function(obj){
            return "<div class='label' style='border:1px solid "+obj.color+"'>"+obj.sales+"</div>";
        },
		color:"#color#",
		barWidth:20,
		radius:5,
		tooltip:{
			template:"#sales#"
		},
		legend: {
			width: 90,
			align: "right",
			valign: "middle",
			template: "#year#"
		},
		/* yAxis:{
            template:"#year#"
        }, */
        xAxis:{
        	template:function(obj){return(obj%10?"":obj);},
            start:0,
            end:allCnt,
            step:10
        }
	}).parse(data,"json");
	
}



</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        padding: 10px,0px,0px,0px;
        overflow: hidden;
    }
     .label{
            background-color:#ffffff;
            -moz-border-radius:4px;
            -ms-border-radius:4px;
            -webkit-border-radius:4px;
            border-radius:4px;
            height:15px;
            line-height:15px;
            font-size: 9px;
            width:25px;
            text-align:center;
        }
	
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#go {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
    
</style>

</head>

<body onload="init();" onresize="resize();">
<div id="layoutObj" style="position: relative;"></div>
</body>
</html>