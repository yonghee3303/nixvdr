<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String usbId = (String)request.getParameter("USB_ID");
String drive = (String)request.getParameter("DRIVE");
String driveSize = (String)request.getParameter("DRIVE_SIZE");
String userIp = request.getRemoteAddr(); //접속한 user의 ip를 얻어온다.
String contextPath =request.getContextPath();
%>

<html>
<head>

<title>보안USB 생성창</title>

<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<script>

var G_SERVLETURL = "<%=contextPath%>/mg/enrollUSB.do";

var usbId = "<%=usbId%>";
var drive = "<%=drive%>";
var calendar;
var winSelectUser;

var renderId = ""; //대여자 id
var returnDt = "";	//반납 예정일
var isModified = "0"; //사용자가 정책 수정권한이 부여되는지
var initMac;
var userAuthYn;
var runMacYn;
////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
var dhxWins;
var prg_win;
function BwindowWidth() {
	 if (window.innerWidth) {
		 return window.innerWidth;
	 } else if (document.body && document.body.offsetWidth) {
	 	return document.body.offsetWidth;
	 } else {
		 return 0;
	}
}	
function BwindowHeight() {
	 if (window.innerHeight) {
		 return window.innerHeight;
	} else if (document.body && document.body.offsetHeight) {
		 return document.body.offsetHeight;
	} else {
		 return 0;
	}
}
function doQueryDuring()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
}
////////////////////////////////////////////////////프로그래스바 처리때문에 
function close(){
	
	if(winSelectUser != null){
		winSelectUser.close();
	}
	window.close();
}

function savePolicyEnd() {

	doQueryModalEnd();
	alert("<%=mm.getMessage("SUMG_1003", s_user.getLocale())%>");
	
	var export_ifr = opener.parent.subCell6.getFrame();//반출 refresh
	export_ifr.contentWindow.refresh();
	
	var checkList_ifr = opener.parent.subCell1.getFrame();
	checkList_ifr.contentWindow.refresh();
	
	var summary_ifr = opener.parent.subCell3.getFrame();
	summary_ifr.contentWindow.createUSBEnd();
	
	var import_ifr = opener.parent.subCell4.getFrame();//보유 refresh
	import_ifr.contentWindow.refresh();
	
	<%	if("Y".equals(pm.getString("component.usb.standbyList.useYn"))){	%>
	var standby_ifr = parent.opener.parent.subCell5.getFrame();
	standby_ifr.contentWindow.refresh();
	<%	}	%>

	export_ifr.contentWindow.selectedUSB(usbId);

	window.opener.closeWinMappingUser();

}

function init(){
	if(dhxWins == null) {
    	dhxWins = new dhtmlXWindows();
    	dhxWins.setImagePath("<%=contextPath%>/dthmlx/dhtmlxWindows/codebase/imgs/");
    }

	if(prg_win == null) {
		var top = BwindowWidth()/2 - 180;
		var left  = BwindowHeight()/2 - 73;
		
		prg_win = dhxWins.createWindow("prg_win", top, left, 180, 110);
		prg_win.setText("Please wait for a moment.");
		prg_win.button("close").hide();
		prg_win.button("minmax1").hide();
		prg_win.button("minmax2").hide();
		prg_win.button("park").hide();
		dhxWins.window("prg_win").setModal(false);
		prg_win.attachURL("../../../ext/include/progress_ing.htm");
		dhxWins.window("prg_win").hide();
	}
	
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar = new dhtmlXCalendarObject({input:"calendar_input",button:"calendar_icon"});
	calendar.loadUserLanguage("<%=s_user.getLocale()%>");
	
	calendar.hideTime();
	
	doPolicyCombo();
	suGrpChange();
}
//사용자선택 팝업창 오픈
function selectUser(){
	winSelectUser = window.open("selectUser.jsp?formid=SU_0107&gridid=GridObj",'','width=530px,height=320px,scrollbars=no,resizable=no');
}
function selectUser_End(userId, userNm, deptNm, position){
	form.RENDER_ID.value=userId;
	document.getElementById("renderInfo").innerText=deptNm+" / "+userNm+" / "+position;
	form.USB_NM.value=userId+"_USB";
	winSelectUser.close();
}

//policy combo 동적으로 생성
function doPolicyCombo() { 
	var url = "/mg/sugrp.do?mod=getComboSuGrpHtml";
	sendRequest(doAjaxResult, "", 'POST', url , false, false);
}

function doAjaxResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyCombo = document.getElementById("comboSuGrp");
	policyCombo.innerHTML = oj.responseText;//jData;
	document.form.policyCode.value ="<%=pm.getString("component.usb.export.default.policyCd")%>"; 
}
function suGrpChange(){
	var policyNo = document.form.policyCode.value;
	var url ="/mg/sugrp.do?mod=getPolicySuGrp&SU_POLICY_GRP_CD="+policyNo;
	sendRequest(suGrpChangeEnd, "", 'POST', url , false, false);
}

function suGrpChangeEnd(oj){
	var jDataPolicy = JSON.parse(oj.responseText);

	var policyNo = document.form.policyCode.value;
	var usbAuth = jDataPolicy.USB_AUTH_CL_CD; 
	var noSave = jDataPolicy.NO_SAVE; 
	var noCopy = jDataPolicy.NO_COPY; 
	var noPrint =jDataPolicy.NO_PRINT; 
	var isExpireDt = jDataPolicy.IS_EXPIRE_DT; 
	var isExpireInit = jDataPolicy.IS_EXPIRE_INIT; 
	var isReadcount = jDataPolicy.IS_READCOUNT; 
	var isReadInit = jDataPolicy.IS_READ_INIT; 
	var isWaitclose = jDataPolicy.IS_WAIT_CLOSE; 
	var waitTime = jDataPolicy.WAIT_TIME; 
	var isLoginFailInit = jDataPolicy.IS_LOGIN_FAIL_INIT; 
	var isLoginFileCnt = jDataPolicy.IS_LOGIN_FAIL_COUNT; 
	var usbEncode = jDataPolicy.USB_ENCODE_CL_CD; 
	var imageSize = jDataPolicy.USB_IMAGE_SIZE; 
	var expireDt = jDataPolicy.EXPIRE_YMD; 
	var readcnt = jDataPolicy.READCOUNT; 
	var userAuth = jDataPolicy.USER_AUTH_YN; 
	var runMac = jDataPolicy.RUN_MAC_YN;
	var returnDt = jDataPolicy.RETURN_YMD;
	
	form.USB_AUTH_CL_CD.value = usbAuth;
	form.NO_SAVE.value = noSave;
	form.NO_COPY.value = noCopy;
	form.NO_PRINT.value =noPrint;
	form.IS_EXPIRE_DT.value =isExpireDt;
	form.IS_EXPIRE_INIT.value =	isExpireInit;	
	form.IS_READCOUNT.value =isReadcount;
	form.IS_READ_INIT.value =isReadInit	;	
	form.IS_WAIT_CLOSE.value =isWaitclose;
	form.WAIT_TIME.value =	waitTime;	
	form.IS_LOGIN_FAIL_INIT.value =isLoginFailInit;
	form.IS_LOGIN_FAIL_COUNT.value =isLoginFileCnt;	
	form.USB_ENCODE_CL_CD.value =usbEncode;
	form.USB_IMAGE_SIZE.value =	imageSize;
	form.USER_AUTH_YN.value =userAuth;		
	form.RUN_MAC_YN.value =runMac;
	form.RETURN_YMD.value=returnDt;
	
	if(isExpireDt == "1"){
		form.EXPIRE_YMD.value=expireDt;
	}else{
		form.EXPIRE_YMD.value="제한없음";		
	}
	if(isReadcount == "1"){
		form.READCOUNT.value=readcnt;
	}else{
		form.READCOUNT.value="제한없음";		
	}
	
	if(runMac =="0"){
		form.tureMac.value="읽기/쓰기";
		form.falseMac.value="읽기/쓰기";
	}else if(runMac =="1"){
		form.tureMac.value="읽기/쓰기";
		form.falseMac.value="읽기전용";
	}else if(runMac =="2"){
		form.tureMac.value="읽기/쓰기";
		form.falseMac.value="사용금지";
	}
	
	//반납예정일은 기본 1년 
}

function onkeylengthMax(obj, nextobj, maxlength)
{
	if(obj.value.length==maxlength){
		if(obj != form.subMAC_6) nextobj.focus();
		return true;
	}else if(obj.value.length > maxlength){
		return false;
	}else{
		return true;
	}
}
function macAdd(){
	var submac= new Array(form.subMAC_1,form.subMAC_2,form.subMAC_3,form.subMAC_4,form.subMAC_5,form.subMAC_6);
	var macAddr="";
	for(var i=0; i<submac.length; i++){
		if(submac[i].value==""){
			alert("MAC 주소를 정확하게 입력하세요.");
			return;
		}else{
			macAddr+=submac[i].value;
		}
	}
	if(macAddr.length !=12){
		alert("MAC 주소를 정확하게 입력하세요.");
		return;
	}
	var macHtml = form.MAC_LIST.innerHTML;//document.getElementById("MAC_LIST").innerHTML;
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>";
	if(form.MAC_LIST.value==""){
		initMac = macHtml;
		tempHtml += "<option value='"+macAddr+"' selected>"+macAddr+"</option>";
	}else{
		tempHtml += macHtml+"<option value='"+macAddr+"'>"+macAddr+"</option>";	
	}
	var tempMac = tempHtml.split(initMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	tempHtml +=initMac+"</select>";		
	removeAllChild(document.getElementById("MAC_TD"));
	document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	
	for(var i=0; i<submac.length; i++){
		submac[i].value="";
	}
	
}
function macDel(){
	var val = document.getElementById("MAC_LIST").value;
	
	var delMac = "<OPTION selected value="+val+">"+val+"</OPTION>";
	var tempHtml="<select name='MAC_LIST' id='MAC_LIST' multiple>"+form.MAC_LIST.innerHTML+"</select>";
	
	var tempMac = tempHtml.split(delMac);
	for(var i=0; i<tempMac.length; i++){
		if(i==0) tempHtml =tempMac[i];
		else tempHtml+=tempMac[i];
	}
	
	removeAllChild(document.getElementById("MAC_TD"));
	document.getElementById("MAC_TD").insertAdjacentHTML("afterBegin", tempHtml);
	
}
function removeAllChild(nodeTemp){
	while(nodeTemp.hasChildNodes()){
		nodeTemp.removeChild(nodeTemp.lastChild);
	}
}
function createSecureUSB(){
	
	doQueryDuring();
	
	var encodeTypeValue ="0"; //보안영역 생성방식
	var imageSize = "0"; //이미지일 경우 생성용량
	var sizeUnit = ""; //생성용량의 단위 
	var authTypeValue = ""; //인증방식
	var usbNm = ""; //usb명
	
	var isExpireDt = "0";
	var expireDt = ""; // 만료일
	var isReadcount = "0";
	var readcount = "0"; // 실행횟수
	var isProtectWrite = "1"; // 쓰기금지적용
	
	var passwd_w = "";
	var passwd_r = "";
	var isNoPrint = "0";	//문서출력을 금할 것인지
	var isNoCapture = "0";		//클립보드, 스크립 캡쳐 금할 것인지
	var isWaitClose = "0"; // 동작하지 않는다면 unMount할 것인지
	var waitTime = "0";	// 동작하지 않는 시간을 얼마나 기다려줄것인지
	var macAddr="";
	
	userAuthYn = form.USER_AUTH_YN.value;//0: 사용안함, 1: 사용함
	runMacYn = form.RUN_MAC_YN.value; //0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
	
	//selectUser에서 전달받음
	renderId= form.RENDER_ID.value;//document.getElementById("RENDER_ID").value;
	if(renderId == null || renderId ==""){
		alert("<%=mm.getMessage("GMSG_1016", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}
	
	authType = form.USB_AUTH_CL_CD.value; //웹인증, 자체인증방식 선택

	usbNm = encodeURIComponent(form.USB_NM.value); // usb명
	if(usbNm ==null || usbNm == ""){
		alert("<%=mm.getMessage("SUMG_2007", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}

	returnDt = form.RETURN_YMD.value; //일자
	if(returnDt==null || returnDt==""){
		alert("<%=mm.getMessage("SUMG_2008", s_user.getLocale())%>");
		doQueryModalEnd();
		return;
	}
	 
	isExpireDt = form.IS_EXPIRE_DT.value;
	if(isExpireDt == "1"){
		expireDt = form.EXPIRE_YMD.value; //만료일자 사용시 
		if(expireDt==null || expireDt==""){
				alert("<%=mm.getMessage("SUMG_2009", s_user.getLocale())%>");
				doQueryModalEnd();
				return;
		}
	}
	
	isReadcount=form.IS_READCOUNT.value;
	if(isReadcount == "1"){
		readcount = form.READCOUNT.value; //횟수제한 사용시
		if(readcount==null || readcount=="" || readcount=="0"){
			alert("<%=mm.getMessage("SUMG_2010", s_user.getLocale())%>");
			doQueryModalEnd();
			return;
		}
	}
	authTypeValue=form.USB_AUTH_CL_CD.value;

	if(authTypeValue=="0"){ //웹인증 시 
		isProtectWrite = form.NO_SAVE.value; //파일추가,수정가능,보안USB외부로복사금지
		
		if(isProtectWrite!="1"){
			isNoPrint="1";
			isNoCapture="1";
		}
		
		isModified =form.IS_MODIFIED_USER_AUTH.value;

		passwd_w = "<%=pm.getString("component.usb.initPass.default")%>"; //초기 비밀번호, USB 첫 인증 실행 시 보안USB에서 비밀번호를 변경하라는 입력창이 뜬다. 
		passwd_r = "";
		
	}else{
		alert("현재 페이지는 웹인증 시 반출 화면입니다. 웹인증 관련 정책만 선택하실 수 있으십니다.");
		return;
	}
	
	if(runMacYn =="1" || runMacYn=="2"){//0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
		//macAddr=	
		var macAddrList = document.getElementById("MAC_LIST");
		for(var i=0; i<macAddrList.options.length; i++){
			if(macAddrList.options[i].value!=""){
				if(macAddr=="") macAddr = macAddrList.options[i].value.toUpperCase();
				else macAddr +="|"+macAddrList.options[i].value.toUpperCase();
			}
		}
	}
	
	isWaitClose = form.IS_WAIT_CLOSE.value;
	waitTime = form.WAIT_TIME.value;
	
	alert("<%=mm.getMessage("SUMG_2014", s_user.getLocale())%>");
	
	var obj = new Object();

	obj.USBID = usbId;
	obj.DRIVE = drive;
	obj.USEIMAGE = encodeTypeValue;
	obj.DRIVESIZE = imageSize;
	obj.DRIVEUNIT = sizeUnit;
	obj.AUTHORITYTYPE = authTypeValue;
	obj.USBTITLE = encodeURIComponent(usbNm);
	//obj.RETURN_DT = returnDt;
	obj.ISUSEEXPIREDATE = isExpireDt;
	obj.EXPIREDATE = expireDt;
	obj.ISCHECKUSECOUNT = isReadcount;
	obj.USECOUNT = readcount;
	//obj.ISMODIFIED = isModified;
	obj.ISPROTECTWRITE = isProtectWrite;
	obj.PASSWORD = passwd_r;
	obj.FULLPASSWORD = passwd_w;
	obj.ISPROTECTPRINT = isNoPrint;
	obj.ISPROTECTCOPY = isNoCapture;
	obj.ISCHECKSLEEP = isWaitClose;
	obj.SLEEPTIME = waitTime;
	
	//추가
	obj.USERID=renderId;	
	obj.ISUSEUSERID=userAuthYn;  //0: 사용안함, 1: 사용함
	obj.ISUSEMACLIST=runMacYn; //0: 사용안함, 1: 일치하지 않으면 읽기전용, 2: 일치하지 않으면 사용금지
	obj.MAC_LIST =macAddr;
		
	//보안USB생성 서비스 호출
	var policy = JSON.stringify(obj);
	var url = G_SERVLETURL + "?mod=createSecureUSB&policy=" + policy;
	
	sendRequest(createSecureUSBEnd, " " ,"POST", url , false, false);
	
}

function createSecureUSBEnd(oj){

	//document.USBInfo.CreateSecureUSB(oj.responseText);
	websock.send(oj.responseText);
	
	var url = G_SERVLETURL + "?mod=savePolicy&RENDER_ID="+renderId; 
		url+= "&RETURN_YMD="+returnDt ;
		url+= "&IS_MODIFIED_USER_AUTH=" + isModified;
		url+= "&policy=" + oj.responseText;
		url+= "&USER_ID="+"<%=s_user.getId() %>";
		url+= "&IP_ADDR="+"<%=userIp%>";
		url+= "&USER_AUTH_YN="+userAuthYn;
		url+= "&RUN_MAC_YN="+runMacYn;
		
	sendRequest(savePolicyEnd, " " ,"POST", url , false, false);
	
	opener.closeWinMappingUser();
}

</script>

<style>
body{
	padding:20px;
}
table{ 
	width:100%;
}
/* td{
	border: 2px double black;
} */
input{ 
	width:100px;
}
.macInput{
 	width:27px;
 	padding: 0px 2px 0px 2px;
 }
.mainTable{
 	width:100%;
 	padding:10px;
 }
.subTable1{
	width:97%; 
	margin:5px 10px 5px 7px;
} 
.subTable2 td{
 	padding:7px;
 }
 .sub1_td2{
	width:105px;
 }
 .macTable td{
 	padding:3px 0px 3px 0px;
 }
 .authTable td{
	padding:3px 0px 3px 0px;
 }
 .sub1_div{
	height:100%; 
	width:90%; 
	text-align:center; 
	background-color:#dfeff5; 
	border:1px solid #9ebcce; 
	padding: 3px 0px 4px 0px;
 }
</style>
</head>
<body onload="init();">
<!-- <OBJECT ID="USBInfo"
CLASSID="CLSID:81AD4264-52CB-4E2A-A143-FA3C986BD499"
CODEBASE="../../../ext/ocx/SecureUSB.CAB#version=4,0,0,10" width="0" height="0">
</OBJECT> -->
<form name="form" method="post">
<input type="hidden" name="USB_AUTH_CL_CD" value="">
<input type="hidden" name="NO_SAVE" value="">
<input type="hidden" name="NO_COPY" value="">
<input type="hidden" name="NO_PRINT" value="">
<input type="hidden" name="IS_EXPIRE_DT" value="">
<input type="hidden" name="IS_EXPIRE_INIT" value="">
<input type="hidden" name="IS_READCOUNT" value="">
<input type="hidden" name="IS_READ_INIT" value="">
<input type="hidden" name="IS_WAIT_CLOSE" value="">
<input type="hidden" name="WAIT_TIME" value="">
<input type="hidden" name="IS_LOGIN_FAIL_INIT" value="">
<input type="hidden" name="IS_LOGIN_FAIL_COUNT" value="">
<input type="hidden" name="USB_ENCODE_CL_CD" value="">
<input type="hidden" name="USB_IMAGE_SIZE" value="">
<input type="hidden" name="IS_MODIFIED_USER_AUTH" value="0">
<input type="hidden" name="RETURN_YMD" value="">
<input type="hidden" name="USER_AUTH_YN" value="">
<table class="mainTable">
	<tr><td>▶ 사용자 정보</td></tr>
	<tr>
		<td>
			<table class="subTable1" style="">
				<tr>
					<td class="sub1_td1">
						<div class="sub1_div">
							<input type="hidden" name="RENDER_ID">
							<span id="renderInfo">&nbsp;</span>
						</div>
					</td>
					<td class="sub1_td2">
						<div class="btnarea">
				           <a href="javascript:selectUser();" class="btn"><span>사용자 검색</span></a>
				        </div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>▶ 보안USB 설정</td></tr>
	<tr>
		<td>
			<table class="subTable2">
				<tr>
					<td>보안USB 이름</td>
					<td><input type="text" name="USB_NM" value=""/></td>
				</tr>
				<tr>
					<td>정책 선택</td>
					<td>
						<input type="hidden" name="SU_POLICY_GRP_CD" value="">
						<font id="comboSuGrp"></font>
					</td>
				</tr>
				<%-- <tr>
					<td>인증방법</td>
					<td>
						<jsp:include page="../../common/comboCode.jsp" >
							<jsp:param name="codeId" value="A0506"/>
							<jsp:param name="tagname" value="USER_AUTH_YN"/>
							<jsp:param name="def" value=" "/>
						</jsp:include>
					</td>
				</tr> --%>
				<tr>
					<td>사용가능 횟수</td>
					<td><input type="text" name="READCOUNT" value=""/> 회</td>
				</tr>
				<tr>
					<td>사용만료 일자</td>
					<td><input type='text' name="EXPIRE_YMD" id='calendar_input' value=''> <img id='calendar_icon' src='../../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"></td>
				</tr>
				<tr>
					<td>사용가능 MAC</td>
					<td rowspan="2">
						<table class="macTable">
							<tr><td>
									<input type="text" name="subMAC_1" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_2, 2)"/>&nbsp;<input type="text" name="subMAC_2" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_3, 2)"/>&nbsp;
									<input type="text" name="subMAC_3" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_4, 2)"/>&nbsp;<input type="text" name="subMAC_4" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_5, 2)"/>&nbsp;
									<input type="text" name="subMAC_5" class="macInput" onkeypress="return onkeylengthMax(this, form.subMAC_6, 2)"/>&nbsp;<input type="text" name="subMAC_6" class="macInput"  onkeypress="return onkeylengthMax(this, form.addMacBtn, 2)"/></td>
								<td>
									<div class="btnarea">
				           				<a href="javascript:macAdd();" class="btn" name="addMacBtn"><span>Mac 추가</span></a>
				        			</div>
								</td>
							</tr>
							<tr>
								<td id="MAC_TD">
									<input type="hidden" name="MAC_ADDR" value="">
									<select name="MAC_LIST" id="MAC_LIST" multiple>
										<option value=''>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</option>
									</select>
								</td>
								<td>
									<div class="btnarea">
				           				<a href="javascript:macDel();" class="btn"><span>Mac 삭제</span></a>
				        			</div>
				        		</td>
							</tr>
						</table>	
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="padding-top:11px;">사용권한</td>
					<td rowspan="2">
						<input type="hidden" name="RUN_MAC_YN" value="0">
						<table class="authTable">
							<tr>
								<td>지정MAC</td>
								<td><input type="text" name="tureMac" value=""/></td>
							</tr>
							<tr>
								<td>지정되지 않은 MAC</td>
								<td><input type="text" name="falseMac" value=""/></td>
							</tr>
						</table>
				    </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<div class="btnarea">
				<a href="javascript:createSecureUSB();" class="btn"><span>보안USB 반출</span></a>
			</div>
		</td>
	</tr>
</table>
</form>
</body>
</html>