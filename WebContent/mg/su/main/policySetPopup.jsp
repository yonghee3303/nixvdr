<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IUser s_user = (IUser) session.getAttribute("j_user");
	String usbId = request.getParameter("USB_ID");
	String drive = request.getParameter("DRIVE");
%>
<html>
<head>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<title>정책세트 팝업</title>
<script>

var layout;
var cell;
var jPolicyData = new Object();

function init(){
	copyData();
	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "1C",
	    skin: "dhx_web"
	});
	
	cell = layout.cells('a');
    cell.setWidth(700);
	cell.setHeight(500);
	cell.setText('정책세트 설정');
    cell.attachURL("mappingPolicySet.jsp", null, null); 
}

//대여화면에서 불러온 기본정책 복제 (저장 버튼을 눌렀을시 복제된 정책을 반영)
function copyData() {
	var keys = Object.keys(opener.jPolicyData);
	for(var i=0; i<keys.length; i++) {
		var key = keys[i];
		jPolicyData[key] = opener.jPolicyData[key];
	}
}
</script>
<style>

#layoutObj {
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

 #caption {
	white-space: nowrap;
	cursor: default;
	line-height: 31px;
	font-family: Tahoma;
	font-size: 12px;
	color: #ffffff;
	font-weight: bold;
	width: 97%;
} 

</style>

</head>
<body onload="init();" style="overflow:hidden; margin:0px;">
<div id="layoutObj" style="position:absolute;"> <!-- z-index:50; -->	
</div>


</body>
</html>