<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
  //IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);

%>

<html>
<head>
<title>보안USB 사용권한 설정</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag 		= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/su/basicSetMgmt.do";

var preId = "";
var currentId = "";
var policyList = new Object();

function init() {
	setFormDraw();
	doQuery();
}


function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
}
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

function doOnCellChange(stage,rowId,cellInd) 
{
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}


function doQuery() {
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectPolicySetList&grid_col_id="+grid_col_id);
	GridObj.clearAll(false);																																											
}

//doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
//GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
//setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	if(status == "false") {
		alert(msg);
	}
	<%-- document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%>  <span class='point'> : " + 0+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> "; --%>
	return true;
}



function doAddRow() {

   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
		
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doDeleteRow() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	<%-- for(var i=0; i < grid_array.length; i++) {
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("CP_STS_CD")).getValue() != "01") {
			alert("<%=mm.getMessage("CPMG_1037", s_user.getLocale())%>");
			return;
		}
	}
	
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteDoc&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	 --%>
}






</script>
</head>
<body onload="init();">
<form name="form" method="post">
<!--추가 삭제 테이블-->
<br>
 <table style="margin-left:3%; width:95%;">
 	<colgroup>
 	<col width="81%">
 	<col width="10%">
 	</colgroup>
 	<tr>
 		<td></td>
 		<td><button name="add" onclick="doAddRow(); return false;"><%=mm.getMessage("CPMG_1135",s_user.getLocale())%></button></td>	<!--추가-->
 		<td><button name="delete" onclick="doDelete(); return false;"><%=mm.getMessage("COMG_1013",s_user.getLocale())%></button></td> <!--삭제-->
 	</tr>
 </table>
	<!------그리드-------> 
	<div id="gridbox" style="margin-left:3%; margin-top:3%; width:94%; height:40%; background-color:white; overflow:hidden;"></div>

<br>
<table style="margin-left:3%;width:95%;">
	<tr>
		<td>
		<textarea name="contents" class="text" rows="10" style="WIDTH: 100%"></textarea>
		</td>
	</tr>
</table>
<br>
<table style="margin-left:5%;width:95%;">
	<colgroup>
		<col width="85%">
	</colgroup>
	<tr>
		<td><input id="chkSave" type="checkbox"><%=mm.getMessage("SUMG_1234", s_user.getLocale())%></td>
		<td><input id="chkCopy" type="checkbox"><%=mm.getMessage("SUMG_1235", s_user.getLocale())%></td>
	</tr>
	<tr>
		<td><input id="chkClip" type="checkbox"><%=mm.getMessage("SUMG_1236", s_user.getLocale())%></td>
		<td><input id="chkPrint" type="checkbox"><%=mm.getMessage("SUMG_1237", s_user.getLocale())%></td>
	</tr>
	<tr>
		<td><input id="chkUse" type="checkbox"><%=mm.getMessage("SUMG_1238", s_user.getLocale())%></td>
		<td><input id="chkDel" type="checkbox"><%=mm.getMessage("SUMG_1239", s_user.getLocale())%></td>	
	</tr>
</table>
<br><br>
 <a href="javascript:doSave();" class="btn" style="margin-left:93%"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>


</form>		
</body>
</html>