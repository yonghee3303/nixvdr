<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
  //IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);

%>

<html>
<head>
<title>보안USB 사용권한 설정</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag 		= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/su/basicSetMgmt.do";

var preId = null;
var currentId = null;
var policyList = null;

function init() {
	setFormDraw();
	doQuery();
}


function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	//이전 행 저장
	if(currentId != null) {
		preRowSave();
	}
	
	row_id = rowId;
	//선택된 행의 데이터 호출
	currentId = GridObj.cells(row_id, GridObj.getColIndexById("POLICY_SET_CD")).getValue();
	if(currentId != "") {
		document.getElementById("contents").value = policyList[currentId].POLICY_SET_NM;
		getCheckSts("chkSave", policyList[currentId].NO_SAVE);
		getCheckSts("chkCopy", policyList[currentId].NO_COPY);
		getCheckSts("chkClip", policyList[currentId].NO_CLIP);
		getCheckSts("chkPrint", policyList[currentId].NO_PRINT);
		getCheckSts("chkUse", policyList[currentId].NO_USE);
		getCheckSts("chkDel", policyList[currentId].DEL_YN);
	} else {
		document.getElementById("contents").value = "";
		getCheckSts("chkSave", "N");
		getCheckSts("chkCopy", "N");
		getCheckSts("chkClip", "N");
		getCheckSts("chkPrint", "N");
		getCheckSts("chkUse", "N");
		getCheckSts("chkDel", "N");
	}
}


function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}
//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
/* function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
} */

function doOnCellChange(stage,rowId,cellInd) 
{
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doQuery() {
	var url = G_SERVLETURL+"?mod=selectPolicySetList";
	sendRequest(doQueryEnd, " " ,"POST", url , false, false);																																											
}

//doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
//GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
//setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode == "0") {
		policyList = jData.policySet;
		var policyNmList = Object.keys(policyList);
		
		for(var i=0; i<policyNmList.length; i++) {
			dhtmlx_last_row_id++;
		   	var nMaxRow2 = dhtmlx_last_row_id;
		   	var row_data = "<%=grid_col_id%>";
		  	
			GridObj.enableSmartRendering(true);
			GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
		  	GridObj.selectRowById(nMaxRow2, false, true);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_SET_CD")).setValue(policyList[policyNmList[i]].POLICY_SET_CD);
				
			dhtmlx_before_row_id = nMaxRow2;
			addRowId = nMaxRow2;
			ed_flag = "Y";
		}
	} else {
		alert(jData.errmsg);
	}
}

function doAddRow() {

   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	/* GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1"); */
		
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doDelete() {
	if(currentId != null) {
		GridObj.enableSmartRendering(false);
		if(currentId == "") {
			GridObj.deleteRow(row_id);
		} else {
			GridObj.deleteRow(row_id);
			policyList[currentId].STS = "DEL";
		}
		currentId = null;
	}
}

//정책 설정을 JSON 객체에 저장
function setCheckSts(element, sts) {
	if(sts == true) {
		policyList[preId][element] = "Y";
	} else {
		policyList[preId][element] = "N";
	}
}

//JSON 객체에서 정책 설정 불러오기
function getCheckSts(element, sts) {
	if(sts == "Y") {
		document.getElementById(element).checked = true;
	} else {
		document.getElementById(element).checked = false;
	}
}

//행변경시 이전값 저장
function preRowSave() {

	var prePolicySetCd = GridObj.cells(row_id, GridObj.getColIndexById("POLICY_SET_CD")).getValue(); 
	
	//새로 추가된 행에 코드값이 입력됬을 경우 해당 행을 저장하도록 처리
	if(currentId == "") {
		if(prePolicySetCd.trim().length > 0) {
			currentId = prePolicySetCd;
			policyList[currentId] = new Object(); 
			policyList[currentId].POLICY_SET_CD = currentId; 
		} else {
			GridObj.enableSmartRendering(false);
			GridObj.deleteRow(row_id);
			return;
		}
	}
	
	//이전값 저장
	if(currentId != null) {
		if(currentId != "") {
			preId = currentId;
			
			//기존 값의 CD 값을 삭제하고 다른 행을 선택하였을 때 처리
			if(prePolicySetCd.trim().length == 0) {
				if (confirm("<%=mm.getMessage("SUMG_2022", s_user.getLocale())%>")) {  // 이전 데이터의 정책코드가 존재하지 않습니다. 삭제하시겠습니까? (예 : 삭제, 아니요 : 정책코드 복원)
					GridObj.enableSmartRendering(false);
					GridObj.deleteRow(row_id);
					policyList[preId].STS = "DEL";
					return;
				} else {
					GridObj.cells(row_id, GridObj.getColIndexById("POLICY_SET_CD")).setValue(policyList[preId].POLICY_SET_CD);
				}
			}
			
			//기존 값이 코드값이 변경되었을 경우
			if(policyList[preId].POLICY_SET_CD != prePolicySetCd) {
				policyList[preId].STS = "DEL";
				
				preId = prePolicySetCd;
				policyList[preId] = new Object(); 
				policyList[preId].POLICY_SET_CD = preId;
			}
				
			var policySetNm = document.getElementById("contents").value;
			var noSave = document.getElementById("chkSave").checked;
			var noCopy = document.getElementById("chkCopy").checked;
			var noClip = document.getElementById("chkClip").checked;
			var noPrint = document.getElementById("chkPrint").checked;
			var noUse = document.getElementById("chkUse").checked;
			var delYn = document.getElementById("chkDel").checked;
		
			policyList[preId].POLICY_SET_NM = policySetNm;	//필요시 TextArea의 \n을 <br>로 치환해야함
			setCheckSts("NO_SAVE", noSave);
			setCheckSts("NO_COPY", noCopy);
			setCheckSts("NO_CLIP", noClip);
			setCheckSts("NO_PRINT", noPrint);
			setCheckSts("NO_USE", noUse);
			setCheckSts("DEL_YN", delYn);
			policyList[preId].STS = "SAVE";	
		}
	}
}

//정책코드 저장
function doSave() {
	
	if (confirm("<%=mm.getMessage("COMG_1009")%>")) {  // 저장하시겠습니까?
		
		//이전 행 저장
		if(currentId != null) {
			preRowSave();
		}
			
		//공백코드 삭제
		GridObj.enableSmartRendering(false);	//그리드 행 삭제시 랜더링 기능을 비활성화 시켜야한다.
		GridObj.forEachRow(function(id){
			if(GridObj.cells(id, GridObj.getColIndexById("POLICY_SET_CD")).getValue() == "") {
				GridObj.deleteRow(id);
			}
		});
		
		url = G_SERVLETURL + "?mod=saveAuth";
		var argu = "&authList=" + encodeURIComponent(JSON.stringify(policyList));
		
		sendRequest(doSaveEnd, argu, "POST", url , false, false);
	}
}

function doSaveEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode == "0") {
	} 	
	alert(jData.errmsg);
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<!--추가 삭제 테이블-->
<br>
 <table style="margin-left:3%; width:95%;">
 	<colgroup>
 	<col width="81%">
 	<col width="10%">
 	</colgroup>
 	<tr>
 		<td></td>
 		<td><button name="add" onclick="doAddRow(); return false;"><%=mm.getMessage("CPMG_1135",s_user.getLocale())%></button></td>	<!--추가-->
 		<td><button name="delete" onclick="doDelete(); return false;"><%=mm.getMessage("COMG_1013",s_user.getLocale())%></button></td> <!--삭제-->
 	</tr>
 </table>
	<!------그리드-------> 
	<div id="gridbox" style="margin-left:3%; margin-top:3%; width:94%; height:40%; background-color:white; overflow:hidden;"></div>

<br>
<table style="margin-left:3%;width:95%;">
	<tr>
		<td>
		<textarea id="contents" name="contents" class="text" rows="10" style="WIDTH: 100%"></textarea>
		</td>
	</tr>
</table>
<br>
<table style="margin-left:5%;width:95%;">
	<colgroup>
		<col width="85%">
	</colgroup>
	<tr>
		<td><input id="chkSave" type="checkbox"><%=mm.getMessage("SUMG_1234", s_user.getLocale())%></td>
		<td><input id="chkCopy" type="checkbox"><%=mm.getMessage("SUMG_1235", s_user.getLocale())%></td>
	</tr>
	<tr>
		<td><input id="chkClip" type="checkbox"><%=mm.getMessage("SUMG_1236", s_user.getLocale())%></td>
		<td><input id="chkPrint" type="checkbox"><%=mm.getMessage("SUMG_1237", s_user.getLocale())%></td>
	</tr>
	<tr>
		<td><input id="chkUse" type="checkbox"><%=mm.getMessage("SUMG_1238", s_user.getLocale())%></td>
		<td><input id="chkDel" type="checkbox"><%=mm.getMessage("SUMG_1239", s_user.getLocale())%></td>	
	</tr>
</table>
<br><br>
 <a href="javascript:doSave();" class="btn" style="margin-left:93%"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>


</form>		
</body>
</html>