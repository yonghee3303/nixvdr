<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>


<html>
<head>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>


<script>
var layout;

var cell;
var cell2;
var cell3;
function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "2E",
	    skin: "dhx_web"
	});
	
	
	cell = layout.cells('a');/* 
    cell.setText('<table style="width:98%; font-family:Tahoma; font-size:12px;"><tr><td><font color="white"><b>내부 판단 기준</b></font></td><td align="right"><font color="yellow"><a onclick="goMain()">Go Main!</a></font></td></tr></table>'); */ 
	cell.setText('정책세트 리스트');
    cell.attachURL("policySetGrid.jsp",null,  {
		formid: "CP_051701",
		gridid: "<%=grid_obj%>"
	});  
    
  
    cell2 = layout.cells('b');/* 
    cell.setText('<table style="width:98%; font-family:Tahoma; font-size:12px;"><tr><td><font color="white"><b>내부 판단 기준</b></font></td><td align="right"><font color="yellow"><a onclick="goMain()">Go Main!</a></font></td></tr></table>'); */ 
    cell2.setWidth(300);
	cell2.setHeight(500);
    cell2.setText('[기본반출 정책]설정 정보');
    cell2.attachURL("policySetDetail.jsp",null,  {
    	formid: "CP_051702",
    	gridid: "<%=grid_obj%>"
	});  
}

function refresh(grpCD) 
{	
	cell2.attachURL("<%=contextPath%>/mg/su/policy/policySetDetail.jsp", null,  {
		formid: "CP_051702",
		gridid: "<%=grid_obj%>",
		grpCD : grpCD
	});
	
}



function resize() {
	layout.setSizes();
}


</script>

<style>
    #layoutObj {
        width: 100%;
        height: 100%;
        margin: 0px;
        overflow: hidden;
    }

#caption {
	white-space: nowrap;
	cursor: default;
	line-height: 31px;
	font-family: Tahoma;
	font-size: 12px;
	color: #ffffff;
	font-weight: bold;
	width: 100%;
}

</style>

</head>

<body onload="init();" onresize="resize();" style="overflow:hidden;">

<div id="layoutObj" style="position: relative; z-index:50;"></div>
	
</body>
</html>