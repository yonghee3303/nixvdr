<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<% 
    IUser s_user = (IUser) session.getAttribute("j_user");
	//String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
	//String docId = StringUtils.paramReplace(request.getParameter("docId"));
	//String authority = StringUtils.paramReplace(request.getParameter("authority"));
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	//String css = pm.getString("component.ui.portal.css");
	String contextPath = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Biz Admin Management</title>
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var currentDeptCd = null;
var currentAdminList = null;

////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
/* function doQueryDuring2()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd2()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
} */ 
////////////////////////////////////
// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

function doOnRowSelect(rowId,cellInd){
	
}
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if(grid_array.length > 0) {
		
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery(deptCd, adminList) {
	
	//컬럼 초기화
	dhtmlx_before_row_id = 0;
	dhtmlx_last_row_id = 0;
	GridObj.clearAll(false);
	
	//이전 데이터 저장
	if(currentDeptCd != null) {
		preRowSave();
	}
	
	currentDeptCd = deptCd;
	currentAdminList = adminList;
	
	for(var i=0; i<currentAdminList.length; i++) {
		if(currentAdminList[i].STS != "DEL") {
			dhtmlx_last_row_id++;
		   	var nMaxRow2 = dhtmlx_last_row_id;
		   	var row_data = "<%=grid_col_id%>";
		  	
			GridObj.enableSmartRendering(true);
			GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
		  	GridObj.selectRowById(nMaxRow2, false, true);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADMIN_ID")).setValue(currentAdminList[i].ADMIN_ID);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADMIN_NM")).setValue(currentAdminList[i].ADMIN_NM);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REG_NM")).setValue(currentAdminList[i].REG_NM);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("REG_DT")).setValue(currentAdminList[i].REG_DT);
				
			dhtmlx_before_row_id = nMaxRow2;
			addRowId = nMaxRow2;
			ed_flag = "Y";	
		}
	}																								
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(oj) {
}

function doAddRow(userId, userNm) {
	var len = currentAdminList.length;
	var regInfo = new Object();
	regInfo.ADMIN_ID = userId;
	regInfo.ADMIN_NM = userNm;
	regInfo.STS = "SAVE";
	
	if(len == null) {
		len = 0;
		currentAdminList = new Array();
		currentAdminList.push(regInfo);
	} else {
		for(var i=0; i<len; i++) {
			if(currentAdminList[i].ADMIN_ID == userId) {
				alert("<%=mm.getMessage("SUMG_2024",s_user.getLocale())%>");
				return;
			}
		}
		currentAdminList[len] = regInfo; 
	}
	
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADMIN_ID")).setValue(currentAdminList[len].ADMIN_ID);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("ADMIN_NM")).setValue(currentAdminList[len].ADMIN_NM);
		
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doDeleteRow() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	GridObj.enableSmartRendering(false);	//그리드 행 삭제시 랜더링 기능을 비활성화 시켜야한다.
	for(var i=0; i < grid_array.length; i++) {
		var userId = GridObj.cells(grid_array[i], GridObj.getColIndexById("ADMIN_ID")).getValue();
		var len = currentAdminList.length;
		
		for(var j=0; j<len; j++) {
			if(currentAdminList[j].ADMIN_ID == userId) {
				currentAdminList[j].STS = "DEL";
			}
		}
		GridObj.deleteRow(grid_array[i]);
	}
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}
	return false;
}

function init() {
	 setFormDraw();
	 parent.resize();
}

function preRowSave() {
	parent.adminList[currentDeptCd] = currentAdminList;
}


</script>
<style>
input {border:none;}
</style>
</head>
<body onload="init();" style="overflow:hidden;" oncontextmenu="return false">
<form name="form" method="post">
</form>	
<div id="gridbox" name="gridbox" width="99%" height="97%" style="background-color:white;overflow:hidden"></div>
</body>
</html>
