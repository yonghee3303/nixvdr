<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);

%>

<html>
<head>
<title>사업장 및 관리자 등록</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>


var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/su/basicSetMgmt.do";

var cellWidth;
var cellHeight;
var deptList = null;
var adminList = null;
var currentDeptCd = null;

function init() {
	cellWidth = (document.body.clientWidth)*0.95;
	cellHeight = ((document.body.clientHeight))/4;
	setFormDraw();	
	adminlistframe.location.href="basicBizAdminList.jsp?formid=CP_05160302&gridid=GridObj";
	doQuery();
}


function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	//resize();	//가장 늦게 로딩되는 iframe 쪽에서 resize 함수 호출
}

function resize() {
	cellWidth = (document.body.clientWidth)*0.95;
	cellHeight = ((document.body.clientHeight))/4;
	/* document.getElementById("formTb").style.width = cellWidth;
	document.getElementById("td2").style.height = cellHeight;
	document.getElementById("td4").style.height = cellHeight+3; */
	/* cellsResize("", cellHeight); */
}

/* function cellsResize(width, height) {
	try {
		setTimeout(function() {
			document.getElementById("gridbox").style.width = cellWidth;
			document.getElementById("gridbox").style.height = cellHeight;
			document.getElementById("adminlistframe").style.height = cellHeight+3;
			adminlistframe.document.getElementById("gridbox").style.height = cellHeight;
		}, 300);
		//document.getElementById("gridbox").style.height = cellHeight;
		
	} catch(exception) {}
} */

function doQuery() {
	var url = G_SERVLETURL+"?mod=selectBizAdminList";
	sendRequest(doQueryEnd, " " ,"POST", url , false, false);		
}

function doQueryEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode == "0") {
		deptList = jData.bizList;
		adminList = jData.adminList;
		
		var deptNmList = Object.keys(deptList);
		
		for(var i=0; i<deptNmList.length; i++) {
			dhtmlx_last_row_id++;
		   	var nMaxRow2 = dhtmlx_last_row_id;
		   	var row_data = "<%=grid_col_id%>";
		  	
			GridObj.enableSmartRendering(true);
			GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
		  	GridObj.selectRowById(nMaxRow2, false, true);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("DEPT_CD")).setValue(deptNmList[i]);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("DEPT_NM")).setValue(deptList[deptNmList[i]]);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_SET")).setValue('/ext/images/btn_usb_export2.gif');
				
			dhtmlx_before_row_id = nMaxRow2;
			addRowId = nMaxRow2;
			ed_flag = "Y";
		}
	} else {		
		alert(jData.errmsg);
	}
}

function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	currentDeptCd = GridObj.cells(row_id, GridObj.getColIndexById("DEPT_CD")).getValue();
	
	adminlistframe.doQuery(currentDeptCd, adminList[currentDeptCd]);
}
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}

function doOnCellChange(stage,rowId,cellInd) 
{
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}


//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doAddBiz() {
	popupDeptTree("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant", "OkPopupDeptGrid", <%=pm.getString("component.contextPath.root")%>);
}

function OkPopupDeptGrid(dept_cd, dept_nm) {
	if(deptList[dept_cd] != null) {
		alert("<%=mm.getMessage("SUMG_2023", s_user.getLocale())%>");
		//관리자 목록에 해당 사업장의 정보를 보여주기.
		adminlistframe.doQuery(dept_cd, adminList[dept_cd]);
		return;
	}
	deptList[dept_cd] = dept_nm;
	
	if(adminList[dept_cd] == null) {
		adminList[dept_cd] = new Object();	
	}

	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("DEPT_CD")).setValue(dept_cd);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("DEPT_NM")).setValue(dept_nm);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_SET")).setValue('/ext/images/btn_usb_export2.gif');
		
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";	
	
	//관리자 목록 호출
	adminlistframe.doQuery(dept_cd, adminList[dept_cd]);
	popupUser("관리자 선택", '', '', 440, 400);
}

function OkPopupUser(userId, userNm) {
	adminlistframe.doAddRow(userId, userNm);
	/* document.form.APPROVER_ID.value = userId;
	document.form.APPROVER_NM.value = userNm; */	
}


function doDeleteBiz() {
	if(!checkRows()) return;
	GridObj.enableSmartRendering(false);	//그리드 행 삭제시 랜더링 기능을 비활성화 시켜야한다.
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	for(var i=0; i < grid_array.length; i++) {
		GridObj.deleteRow(grid_array[i]);
	}
}

function doAddAdmin() {
	if(currentDeptCd == null) {
		alert("<%=mm.getMessage("SUMG_2025", s_user.getLocale())%>")
		return;
	}
	popupUser("관리자 선택", '', '', 440, 400);
}

function doDeleteAdmin() {
	adminlistframe.doDeleteRow();
}

function doSave() {
	//사업장 정보는 현재 남아있는 그리드 값을 json으로 가공해 서버로 전달
	//관리자 정보는 기존 doQuery시 받아왔던 adminList 정보를 전달
	if (confirm("<%=mm.getMessage("COMG_1009")%>")) {  // 저장하시겠습니까?
		//사업장 리스트 저장
		var saveDeptList = "";
		
		GridObj.forEachRow(function(id){
			saveDeptList += GridObj.cells(id, GridObj.getColIndexById("DEPT_CD")).getValue() + "|";
			//saveDeptList[GridObj.cells(id, GridObj.getColIndexById("DEPT_CD")).getValue()] = GridObj.cells(id, GridObj.getColIndexById("DEPT_NM")).getValue(); 	
		});
		
		//마지막 선택된 사업장의 관리자 리스트 저장
		adminlistframe.preRowSave();	
		
		url = G_SERVLETURL + "?mod=saveBizAdmin";
		var argu = "&deptList=" + encodeURIComponent(saveDeptList);
		argu += "&adminList=" + encodeURIComponent(JSON.stringify(adminList));
		
		sendRequest(doSaveEnd, argu, "POST", url , false, false);
	}
}

function doSaveEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode == "0") {
	} 	
	alert(jData.errmsg);
}


</script>
<style type="text/css">
.bizTable{
	width:95%;
	margin:10px;
}
.bizTable td{
	padding:6px;
}
</style>
</head>
<body onload="init();" style="margin: 0px; padding:5px; overflow: auto;">
<form name="form" method="post">
<table class="bizTable">
  	<colgroup>
 	<col width="30%">
 	<col width="70%">
 	</colgroup>
	<tr>
		<td><%=mm.getMessage("SUMG_1240",s_user.getLocale())%></td>
		<td>
			<div class="btnarea4">
	 			<a href="javascript:doDeleteBiz();" class="btn"><span><%=mm.getMessage("COMG_1013",s_user.getLocale())%></span></a>	<!--삭제-->
	 			<a href="javascript:doAddBiz();" class="btn"><span><%=mm.getMessage("CPMG_1135",s_user.getLocale())%></span></a>  <!--추가-->
	 		</div>
	 	</td>
	</tr>
	<tr>
		<td colspan="2"><div id="gridbox" style="height:173px; width:95%; margins-left:1%; background-color:white; overflow:hidden"></div></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td><%=mm.getMessage("SUMG_1242",s_user.getLocale())%></td>
		<td>
			<div class="btnarea4">
				<a href="javascript:doDeleteAdmin();" class="btn"><span><%=mm.getMessage("COMG_1013",s_user.getLocale())%></span></a>	<!--삭제-->
				<a href="javascript:doAddAdmin();" class="btn"><span><%=mm.getMessage("CPMG_1135",s_user.getLocale())%></span></a>  <!--추가-->
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<iframe id="adminlistframe" name="adminlistframe" style="height:175px; width:96%; background-color:white; overflow:hidden;" frameborder="0" scrolling="auto"></iframe>
		</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2" style="padding-top:12px">
			<div class="btnarea">
  				<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("SUMG_1241", s_user.getLocale())%><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
  			</div>
  		</td>
	</tr>
</table>
</form>	
</body>
</html>