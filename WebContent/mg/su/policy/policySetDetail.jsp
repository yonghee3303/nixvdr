<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%> 
<%
Log log = LogFactory.getLog(this.getClass());
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
  String grpCd = request.getParameter("grpCD");
  log.info("grpCd : " +grpCd);
%>

<html>
<head>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>

var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag 		= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var policyGrpCd		= "<%=grpCd%>";

var G_SERVLETURL = "<%=contextPath%>/mg/su/policySetMgmt.do";


function init() {
	setFormDraw();
	doQuery(); 
	//doPolicyGrpInfo();	//20160331 mine
}

function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
}

function doOnCellChange(stage,rowId,cellInd) 
{	
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		} else {
			GridObj.forEachRow(function(id){
				var defYn = GridObj.cells(id, GridObj.getColIndexById("DEF_YN")).getValue();
				if(defYn == "Y" && id != rowId) {
					GridObj.cells(id, GridObj.getColIndexById("SELECTED")).setValue("1");
					GridObj.cells(id, GridObj.getColIndexById("DEF_YN")).setValue("N");
				} 	
			});
		}
		return true;
	}
	return false;
}

function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}


 function doQuery() {
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectPolicySetList&grid_col_id="+grid_col_id+"&SU_POLICY_GRP_CD="+policyGrpCd);
	// alert(G_SERVLETURL+"?mod=selectPolicySetList&grid_col_id="+grid_col_id+"&SU_POLICY_GRP_CD="+policyGrpCd);
	GridObj.clearAll(false);																																												
} 

//doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
//GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
//setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	if(status == "false") {
		alert(msg);
	}
	
	var setClCd = new Array("IN","OU","OF");
	var pcClCd = new Array("RC","RCAP","DC","DCAP");
	//조회된 항목이 없으며 기본 정책 삽입 
	var i =0;
	if(GridObj.getRowsNum()==0 && policyGrpCd != "null" && policyGrpCd != ""){
		for(var j=0; j<setClCd.length; j++){
			for(var k=0; k<pcClCd.length; k++){
				if(setClCd[j]=="IN" && (pcClCd[k] == "RCAP" || pcClCd[k] =="DCAP")){
		
				}else{
					dhtmlx_last_row_id++;
					i=dhtmlx_last_row_id;
					GridObj.addRow(i, "", GridObj.getRowIndex(i));
					GridObj.cells(i, GridObj.getColIndexById("SELECTED")).setValue("1");
					GridObj.cells(i, GridObj.getColIndexById("SET_CL_CD")).setValue(setClCd[j]);//구분 : A0507 IN,OU,OF
					GridObj.cells(i, GridObj.getColIndexById("PC_CL_CD")).setValue(pcClCd[k]);//적용환경 : A0509 RC,RCAP,DC,DCAP
					GridObj.cells(i, GridObj.getColIndexById("SU_POLICY_GRP_CD")).setValue(policyGrpCd);//정책그룹코드
					GridObj.cells(i, GridObj.getColIndexById("POLICY_SET_CD")).setValue("RW");//정책그룹코드
					GridObj.cells(i, GridObj.getColIndexById("SET_NM")).setValue(GridObj.cells(i, GridObj.getColIndexById("SET_CL_CD")).getTitle()+" : "+GridObj.cells(i, GridObj.getColIndexById("PC_CL_CD")).getTitle());//사용자 지정 정책명 : [구분 :적용 환경] 이런식으로 기본 입력
				}
				
			}
		}	
	}
	
	return true;
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doSave() {

	//모든 정책 저장 필요
	for(var i=1; i <= GridObj.getRowsNum(); i++) {
		GridObj.cells(i, GridObj.getColIndexById("SELECTED")).setValue("1");
	}

	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	var checkId = true;
	
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		//SET_ID check
		//SET_ID(SET_CL_CD + PC_CL_CD)는 중복 불가  
		for(var i=0; i < grid_array.length; i++) {
			GridObj.forEachRow(function(rowId) {
				if(checkId != false) {
					if(grid_array[i] != rowId) {
						var setClCdVal = GridObj.cells(grid_array[i], GridObj.getColIndexById("SET_CL_CD")).getValue();
						var pcClCdVal = GridObj.cells(grid_array[i], GridObj.getColIndexById("PC_CL_CD")).getValue();
						var setClCdCombo = GridObj.getCombo(GridObj.getColIndexById("SET_CL_CD"));
						var pcClCdCombo = GridObj.getCombo(GridObj.getColIndexById("PC_CL_CD")); 
						var policySetCd = GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_SET_CD")).getValue();
						
						if(setClCdVal == GridObj.cells(rowId, GridObj.getColIndexById("SET_CL_CD")).getValue()) {
							if(pcClCdVal == GridObj.cells(rowId, GridObj.getColIndexById("PC_CL_CD")).getValue()) {
								var setClCdText = setClCdCombo.get(setClCdVal);
								var pcClCdText = pcClCdCombo.get(pcClCdVal);
								alert("<%=mm.getMessage("SUMG_2026", s_user.getLocale())%> (" + setClCdText + " / " + pcClCdText + ")");
								checkId = false;
								return;
							}
							
							if(policySetCd == null || policySetCd ==""){
								alert("권한 설정을 하지 않은 항목이 있습니다. 입력 후 저장해 주세요.");
								checkId = false;
								return;
							}
						}
					}	
				}
			});
		}
		if(checkId == true) {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=savePolicySet&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);	
		}
	}
}



function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		//parent.refresh();		
		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function setPolicyGrpCD(grpCD)
{
	policyGrpCd = grpCD;
}

function doAddRow(){
	if(policyGrpCd == "null") {
		alert("<%=mm.getMessage("SUMG_2027", s_user.getLocale())%>");
	} else if(policyGrpCd == "") {
		alert("<%=mm.getMessage("SUMG_2028", s_user.getLocale())%>");
	} else {
		dhtmlx_last_row_id++;
	   	var nMaxRow2 = dhtmlx_last_row_id;
	   	var row_data = "<%=grid_col_id%>";
	  	
		GridObj.enableSmartRendering(true);
		GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
	  	GridObj.selectRowById(nMaxRow2, false, true);
	  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
		GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
			
		GridObj.cells(nMaxRow2, GridObj.getColIndexById("SU_POLICY_GRP_CD")).setValue(policyGrpCd);

		dhtmlx_before_row_id = nMaxRow2;
		addRowId = nMaxRow2;
		ed_flag = "Y";	
	}
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		for(var i=0; i < grid_array.length; i++) {
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("SU_POLICY_GRP_CD")).getValue() == "") {
				GridObj.deleteRow(grid_array[i]);
			}
		}
		grid_array = getGridChangedRows(GridObj, "SELECTED");
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deletePolicySet&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

</script>

</head>
<body onload="init();">
 <br>
	<table style="width:100%;">
	<colgroup>
		<col width=98.9%>
	</colgroup>
	<tr>
		<td>
			 <div class="btnarea">
	            <%-- <a href="javascript:doAddRow();" class="btn"><span><%=mm.getMessage("COMG_1011", s_user.getLocale())%></span></a> --%>
	            <a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	           <%--  <a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("COMG_1013", s_user.getLocale())%></span></a> --%>
	        </div>
		<td/>
	</tr>
	</table>
	<br>
	<div id="gridbox" name="gridbox" style="margin-left:0.5%;height:60%; width:98%; background-color:white;overflow:hidden"></div>
	
</body>

</html>