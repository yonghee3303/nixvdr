<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>트리 (정책 적용 세트 - 3_24 완료)</title>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>

</script>
<style>

.bottomDiv{

	border: 2px solid rgb(232, 232, 232); 
	border-image: none; 
	padding-top: 10px; 
	padding-bottom: 10px;

}

</style>
</head>
<body>
<form name="form" method="post" style="width:680px;">

조건별 적용을 선택하세요
<table width="100%" style="margin:5px;">
	<tr>
		<td>
			<div class="bottomDiv">	
			<table width="100%">
				<colgroup>
					<col width="5%">
					<col width="10%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					
				</colgroup>
				<tr>
					<td rowspan=2>내부</td>
					<td>등록된 pc</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
				<tr>
					<td>등록되지 않은 pc</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
			<!--첫번째테이블 끝-->
			</table>
		</div>
		
			<!--두번째-->
		
		<div class="bottomDiv">	
			<table width="100%">
				<colgroup>
					<col width="5%">
					<col width="10%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
				</colgroup>
				<tr>
					<td rowspan=4>외부</td>
					<td>등록된 pc</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
				<tr>
					<td>등록되지 않은 pc</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
				<tr>
					<td>등록된 pc + 반출승인</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 	
				</tr>
				<tr>
						<td>등록되지 않은 pc + 반출승인</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
			</table>
		</div>
		
			<!--세번째-->
		<div class="bottomDiv">	
			<table width="100%">
			<colgroup>
					<col width="5%">
					<col width="10%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
					<col width="5%">
				</colgroup>
				<tr>
					<td rowspan=4>오프라인</td>
					<td>등록된 pc</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
				<tr>
					<td>등록되지 않은 pc</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
				<tr>
					<td>등록된 pc + 반출승인</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 	
				</tr>
				<tr>
						<td>등록되지 않은 pc + 반출승인</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="1">RW</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="2">RR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="3">DR</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="4">BAN</td>
						<td><input id="NO_SAVE1" type="radio"  name="NO_SAVE" onclick="clickNoSave();" value="5">DEL</td> 
				</tr>
			</table>
		</div>
<table style="margin-top:5px;margin-left:340px;">
	<colgroup>
		<col width="60%">
		<col width="25%">
		<col width="50%">
	<colgroup>
	<tr>
		<td></td>
		<td>
			<button onclick="back();">이전</button>
 		</td>
 		<td>
 			<button onclick="next();"><%=mm.getMessage("SUMG_1213", s_user.getLocale())%></button>
 		</td>
 	</tr>
</table>
	</td>
</tr>
</table>
</form>	
</body>
</html>