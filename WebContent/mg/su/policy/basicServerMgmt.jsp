<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
%>

<html>
<head>
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>

var GridObj 		= {};
var myDataProcessor = {};
var G_SERVLETURL ="<%=contextPath%>/mg/su/basicSetMgmt.do";
var innerChkFlag = false;
var outerChkFlag = false;
String.prototype.trim = function () {
	   return this.replace(/^\s+|\s+$/g, "");
	}

function init() {
	setFormDraw();
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다.
}
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

function doQuery() {
	var url = G_SERVLETURL+"?mod=selectServer";
	sendRequest(doQueryEnd, " " ,"POST", url , false, false);
}
function doQueryEnd(oj){
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode == "0") {
		document.form.innerUrl.value = jData.INNER_SVR_URL;
		document.form.outerUrl.value = jData.OUTER_SVR_URL;
		var ipList = jData.IP_RANGE_ADDR;
		
		for(var i=0; i<ipList.length; i++) {
			dhtmlx_last_row_id++;
		   	var nMaxRow2 = dhtmlx_last_row_id;
		   	var row_data = "<%=grid_col_id%>";
		  	
			GridObj.enableSmartRendering(true);
			GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
		  	GridObj.selectRowById(nMaxRow2, false, true);
		  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
			GridObj.cells(nMaxRow2, GridObj.getColIndexById("IP_ADDR_OP")).setValue(ipList[i].IP_ADDR_OP);
			GridObj.cells(nMaxRow2, GridObj.getColIndexById("IP_ADDR_ED")).setValue(ipList[i].IP_ADDR_ED);
				
			dhtmlx_before_row_id = nMaxRow2;
			addRowId = nMaxRow2;
			ed_flag = "Y";
		}
	} else {
		alert(jData.errmsg);
	}
}
/* function resize() {
	GridObj.setSizes();	
} */

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doAddRow() {

   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
		
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doDelete() {
	if(!checkRows()) return;
	GridObj.enableSmartRendering(false);	//그리드 행 삭제시 랜더링 기능을 비활성화 시켜야한다.
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	for(var i=0; i < grid_array.length; i++) {
		GridObj.deleteRow(grid_array[i]);
	}
}


//내부서버 연결 확인
function innerCheckServer() {
	var url = document.form.innerUrl.value.trim();
	if(lengthCheck(url)) {
		url = url + "<%=contextPath%>/mg/su/basicSetMgmt.do?mod=checkServer";
		sendRequest(innerCheckServerEnd, " " ,"POST", url , false, false);	
	} else {
		alert("<%=mm.getMessage("SUMG_2020",s_user.getLocale())%>");	
	}
}
function innerCheckServerEnd(oj) {
	try {
		var jData = JSON.parse(oj.responseText);
		if(jData.errcode == "0") {
			document.getElementById("innerChk").innerHTML = "<%=mm.getMessage("SUMG_1007",s_user.getLocale())%>";
			document.getElementById("innerChk").style.color = "#7DB459";
			innerChkFlag = true;
		} else {
			document.getElementById("innerChk").innerHTML = "<%=mm.getMessage("SUMG_1008",s_user.getLocale())%>";
			document.getElementById("innerChk").style.color = "red";
			innerChkFlag = false;
		}
	} catch(error) {
			document.getElementById("innerChk").innerHTML = "<%=mm.getMessage("SUMG_1008",s_user.getLocale())%>";
			document.getElementById("innerChk").style.color = "red";
			innerChkFlag = false;
	}
}

//외부서버 연결 확인
function outerCheckServer() {
	var url = document.form.outerUrl.value.trim();
	if(lengthCheck(url)) {
		url = url + "<%=contextPath%>/mg/su/basicSetMgmt.do?mod=checkServer";
		sendRequest(outerCheckServerEnd, " " ,"POST", url , false, false);	
	} else {
		alert("<%=mm.getMessage("SUMG_2021",s_user.getLocale())%>");	
	}
}
function outerCheckServerEnd(oj) {
	try {
		var jData = JSON.parse(oj.responseText);
		if(jData.errcode == "0") {
			document.getElementById("outerChk").innerHTML = "<%=mm.getMessage("SUMG_1007",s_user.getLocale())%>";
			document.getElementById("outerChk").style.color = "#7DB459";
			outerChkFlag = true;
		} else {
			document.getElementById("outerChk").innerHTML = "<%=mm.getMessage("SUMG_1008",s_user.getLocale())%>";
			document.getElementById("outerChk").style.color = "red";
			outerChkFlag = false;
		}	
	} catch(err) {
		document.getElementById("outerChk").innerHTML = "<%=mm.getMessage("SUMG_1008",s_user.getLocale())%>";
		document.getElementById("outerChk").style.color = "red";
		outerChkFlag = false;
	}
}

function lengthCheck(url) {
	if(url.length == 0) {
		return false;			
	}
	return true;
}

//서버설정 저장
function doSave() {
	if (confirm("<%=mm.getMessage("COMG_1009")%>")) {  // 저장하시겠습니까?
		var innerUrl = document.getElementById("innerUrl").value.trim();
		var outerUrl = document.getElementById("outerUrl").value.trim();
		
		if(!outerChkFlag) {
			alert("<%=mm.getMessage("CPCOM_1301",s_user.getLocale())%>");
			return;
		}
		
		if(lengthCheck(innerUrl) == false || innerChkFlag == false) {
			alert("<%=mm.getMessage("SUMG_2020",s_user.getLocale())%>");
			return;
		}
		if(outerUrl.length > 0) {
			if(outerChkFlag == false) {
				alert("<%=mm.getMessage("SUMG_2021",s_user.getLocale())%>");
			}
		}
		//그리드 컬럼내 커서 디자인 변경
		var ipAddrs = "";
		GridObj.enableSmartRendering(false);	//그리드 행 삭제시 랜더링 기능을 비활성화 시켜야한다.
		GridObj.forEachRow(function(id){
			if(GridObj.cells(id, GridObj.getColIndexById("IP_ADDR_OP")).getValue() == "") {
				GridObj.deleteRow(id);
			} else if(GridObj.cells(id, GridObj.getColIndexById("IP_ADDR_ED")).getValue() == "") {
				GridObj.deleteRow(id);
			} else {
				ipAddrs += GridObj.cells(id, GridObj.getColIndexById("IP_ADDR_OP")).getValue()+"~"+GridObj.cells(id, GridObj.getColIndexById("IP_ADDR_ED")).getValue()+"|";
			}
		});
		
		var ipLen = ipAddrs.length;
		if(ipLen > 0) {
			ipAddrs = ipAddrs.substring(0, ipLen-1);	
		}
		
		url = G_SERVLETURL + "?mod=saveServer";
		var argu = "&INNER_SVR_URL=" + encodeURIComponent(innerUrl);
		argu += "&USB_SVR_URL=" + encodeURIComponent(outerUrl);
		argu += "&IP_RANGE_ADDR=" + encodeURIComponent(ipAddrs);
		url += argu;
		
		sendRequest(doSaveEnd, " " ,"POST", url , false, false);
	}
}

function doSaveEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	if(jData.errcode == "0") {
	} 	
	//alert(jData.errmsg);
}

//서버체크 키다운 이벤트 넣어야함



</script>
<style type="text/css">
.serverTable{
	width:95%;
	margin:10px;
}
.serverTable td{
	padding:6px;
}
.w330{
	width:330px;
}
.taRight{
	text-align:right;
}
</style>
</head>
<body onload="init();" style="margin: 0px; padding:5px; overflow: auto;">
<form name="form" method="post">

	<table class="serverTable">
		<tr><td colspan="2"><%=mm.getMessage("SUMG_1232",s_user.getLocale())%></td></tr>
		<tr>
			<td class="w330"><input id="outerUrl" name="outerUrl" type="text" class="text2" size="50"></td>
			<td>
				<div class="btnarea4">
    				<a href="javascript:outerCheckServer();" class="btn"><span><%=mm.getMessage("SUMG_1151",s_user.getLocale())%></span></a>
    			</div>
			</td>
		</tr>
		<tr><td class="w330 taRight"><span id="outerChk" style="text-align:right;">&nbsp;</span></td>
			<td></td>
		</tr>
		<tr><td colspan="2"><%=mm.getMessage("SUMG_1231",s_user.getLocale())%></td></tr>
		<tr>
			<td class="w330"><input id="innerUrl" name="innerUrl" type="text" class="text2" size="50"></td>
			<td>
				<div class="btnarea4">
    				<a href="javascript:innerCheckServer();"class="btn"><span><%=mm.getMessage("SUMG_1151",s_user.getLocale())%></span></a>
    			</div>
    		</td>
    	</tr>
    	<tr><td class="w330 taRight"><span id="innerChk" style="text-align:right;">&nbsp;</span></td>
    		<td></td>
    	</tr>
    	<tr>
    		<td></td>
    		<td></td>
    	</tr>
    	<tr>
    		<td colspan="2"><%=mm.getMessage("SUMG_1233",s_user.getLocale())%>
    			<div class="btnarea4">
 					<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("COMG_1013",s_user.getLocale())%></span></a><!--삭제-->
 					<a href="javascript:doAddRow();" class="btn"><span><%=mm.getMessage("CPMG_1135",s_user.getLocale())%></span></a>  <!--추가-->
 	  		   </div>
 	  		</td>
    	</tr>
    	<tr>
    		<td colspan="2"><div id="gridbox" style="height:250px; width:96%; margins-left:1%; background-color:white; overflow:hidden"></div></td>
    	</tr>
    	<tr>
			<td colspan="2"></td>
		</tr>
    	<tr>
    		<td colspan="2" style="padding-top:11px;">
    			<div class="btnarea">
  					<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("SUMG_1282", s_user.getLocale())%><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
  				</div>
    		</td>
    	</tr>    	
	</table>

</form>
</body>
</html>