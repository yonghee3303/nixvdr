<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%  
 	String formId = StringUtils.paramReplace(request.getParameter("formid"));
	String grid_obj = StringUtils.paramReplace(request.getParameter("gridid"));
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/ext/css/common.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>
<script>

function getBoard(boardClsCd) {
    var SERVLETURL = "/mg/board.do?mod=getBoard";
    var argu = "&boardClsCd=" + boardClsCd;

    if (boardClsCd == "B") sendRequest(doAjaxGetBoardB , argu, 'POST', SERVLETURL , false, false);
    else sendRequest(doAjaxGetBoardQ , argu, 'POST', SERVLETURL , false, false);
}

function doAjaxGetBoardB(oj) {
	document.getElementById("noticeListB").innerHTML = oj.responseText;
}

function doAjaxGetBoardQ(oj) {
	document.getElementById("noticeListQ").innerHTML = oj.responseText;
}

function boardOpen(board_no, boardClsCd) {
	var url = "/mg/common/popup_board.jsp?board_no=" + board_no + "&init_flag=false&boardClsCd=" + boardClsCd;
	
	popupBoard(url, "Board", 400, 480); 
}


function goNoticeMore(board_cd) {

	var btitle = "";

	if(board_cd == "B") btitle = encodeURIComponent("게시판");
	else btitle = encodeURIComponent("질의응답");
	
	var url = "/mg/board/boardPopup.jsp?formid=POP_0108&gridid=GridObj&btitle=" + btitle + "&board_cd=" + board_cd;
	popupBoard(url, "BoardList", 560, 600); 
}

function init() {
   //  parent.topFrame.load_left_frame('A','CP_01');
   //  parent.topFrame.menu_click('0', 'CP_01', '/ext/images/left_tit_1.gif');
}
</script>

</head>
<body bgcolor="#f8f9f9" onload="init();">
<form name="mainform">
	<input type="hidden" name="prodCd" value="">
	<input type="hidden" name="deptCd" value="">
	<input type="hidden" name="modelType" value="">
	<input type="hidden" name="selDay" value="">
	<input type="hidden" name="formDt" value="">
	<input type="hidden" name="toDt" value="">
</form>
<table width="800px" border="0" class="mgt-55" align="center">
<colgroup>
    <col width="30%" />
    <col width="1%" />
    <col width="30%" />
</colgroup>
<tr>
    <td valign="top" align="center" height="200px">
        <div>
      		<iframe name="reqframe" width="100%" height="100%" valign="top" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
      	</div>
    </td>
    <td>
    </td>
    <td valign="top" align="center" height="200px">
        <div>
      		<iframe name="apprframe" width="100%" height="100%" valign="top" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
      	</div>
    </td>
</tr>
</table>

</body>
</html>