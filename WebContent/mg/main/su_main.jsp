<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="java.text.*" %>
<%@ page import="java.net.*"%>
<%@ page import="java.util.Vector"%>

<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="mg.vdr.explorerAction"%>



<%@ include file="../../ext/include/ui_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%
Log log = LogFactory.getLog(this.getClass());
String contextPath = request.getContextPath();
String type = request.getParameter("type");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
explorerAction exAction = new explorerAction();

String company = paramemter.getString("component.site.product");
String logo_image =  paramemter.getString("component.site.image");

IUser s_user = (IUser) session.getAttribute("j_user");
String m_path = ("en".equals(s_user.getLocale().getLanguage()) ? "/ext/images/en/" : "/ext/images/");
String userNm = s_user.getName();
String deptNm = s_user.getProperty("DEPT_NM").toString();

//로그인 사용자의 권한을 가져와 관리자/사용자 초기화면을 다르게 설정
String pgmType = paramemter.getString("component.site.product");

/* if((String)request.getParameter("flag")!=null){
	pgmType = (String)request.getParameter("flag");
} */
//log.info("pgmType : " + pgmType);

boolean isAdmin = false;
String initpage = "";

log.info("관리자 권한 : " + s_user.getProperty("USER_ROLL").toString());

//CP커뮤니티인 경우에는 flag값 없이 진입
if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString()) || "S03".equals(s_user.getProperty("USER_ROLL").toString()) || "S04".equals(s_user.getProperty("USER_ROLL").toString()) || "S05".equals(s_user.getProperty("USER_ROLL").toString()) || "S06".equals(s_user.getProperty("USER_ROLL").toString()) || "S07".equals(s_user.getProperty("USER_ROLL").toString()) || "S08".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
	if(pgmType.equals("CP") || pgmType.equals("cp")){
		log.info("관리자 CP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cp.initpage.admin");	
	}else if(pgmType.equals("SU") || pgmType.equals("su")){
		log.info("관리자 SU 초기화면 세팅");
		initpage = paramemter.getString("component.site.su.initpage.admin");
	}else{
		log.info("관리자 CPCOMP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cpcomp.initpage.admin");
	}
	log.info("pgmType="+pgmType+"/ initPate="+initpage);
}else{
	if(pgmType.equals("CP") || pgmType.equals("cp")){
		log.info("사용자 CP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cp.initpage.user");	
	}else if(pgmType.equals("SU") || pgmType.equals("su")){
		log.info("사용자 SU 초기화면 세팅");
		initpage = paramemter.getString("component.site.su.initpage.user");
	}else{
		log.info("사용자 CPCOMP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cpcomp.initpage.user");
	}
	log.info("pgmType="+pgmType+"/ initPate="+initpage);
}

%>
<html>
<head>
<link rel="stylesheet" href="../../ext/css/common.css" type="text/css">
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script language=javascript src="../../ext/js/sec.js"></script>

<title><%=mm.getMessage("WTMG_1003", s_user.getLocale())%></title>
<script language="javascript"> 
var typeParam = "<%=type%>";
var menutype = "A";
var initflag = true;

var param=""; //자식 페이지 창 전환 후에도 검색조건 유지를 위해
//var winCreateSecureUSB;
//var winSelectUser;

function form_init()
{
	//추가
	doOnLoad();
	//initLayout();
	
	var page = "<%=initpage%>";
	
	
	
	document.mainFrame.location.href = "../../" + page;
}

function pageRedirect(page,argu){

	document.mainFrame.location.href = "../../" + page + argu;
}


function change(a,b) {
  	var c = document.search.btn_flg.value;
  	// if(b != c) document.getElementById('juil_'+b).src = a;
}

function change2(d, val) {
	var c = 1;
    document.search.btn_flg.value = c;
}

function Init() {
	document.mainFrame.location.href ="<%=contextPath+initpage%>";
}

function logout() {
	if(confirm("<%=mm.getMessage("COMG_1034", s_user.getLocale())%>")) {  // 시스템을 종료 하시겠습니까 ?
	   top.location.href = "<%=contextPath%>/Logout.do";
	}
}

function pwdLogout() {
	top.location.href = "<%=contextPath%>/Logout.do";
	location.reload();
}

function setMenuType(type) {
	menutype = type;
}

function menu_click(menu_order, menu_code, left_image) 
{   //사용자가 상단메뉴 클릭했을 때 이벤트
	//메뉴 클릭시 좌측 메뉴 열림
	parent.framesetMenu.cols = "210,*";
	
   	var leftmenu = "menu_main_left.jsp?qm_init=true&top_menuname="+encodeURIComponent(document.search.top_menuname.value);
	var exeurl = "&url="+document.search.url.value;
	var menucode = "&MENU_OBJECT_CODE="+menu_code + "&menu_order=" + menu_order;
		 menucode += "&MENU_IMG=" + left_image;
		
	parent.leftFrame.location.href=leftmenu + exeurl + menucode;
	
	if(initflag == true) {
		parent.framesetMenu.cols = "210,*";
	}
}

function load_left_frame(type,menu_code) {
	parent.leftFrame.location.href="menu_main_left.jsp?qm_init=true&top_menuname="+encodeURIComponent(document.search.top_menuname.value)+"&url="+document.search.url.value+"&MENU_OBJECT_CODE="+menu_code + "&type=" + type ;
}

function form_onunload()
{
    top.location.href = "<%=contextPath%>/Logout.do";
}

function doPWDDisplay() {
	var con = document.getElementById("toDisplay");
	if (PWDChangeShow == "0000") {
		con.style.display = "none";
	} else {
		con.style.display = "block";
	}
}

var PWDChangeShow;
var myMenu;
function doOnLoad(){
	<%
	String PWDisDef = exAction.NimsouChangePWDShow(s_user);
	%>
	PWDChangeShow = <%=PWDisDef%>;
	myMenu = new dhtmlXMenuObject("menuObj");
	
	doPWDDisplay();
	
	//web모드는 마우스 오버 일 때 자동으로 열림
	//win모드 일 때는 클릭해야 열린다.
	//myMenu.setOpenMode("win");
	//slide로 열리면서 투명하게 효과를 넣어봤다.
	myMenu.enableEffect("slide+", 150, 85);
	
	<% IMenu menu = mnu.getMenu("MG", s_user);
	String enable_menuId = null;
	String[] hideMenuList = {"CPCOM", "CPP", "LM"};	//숨김 메뉴 리스트 (메뉴관리에서 숨김처리를 할 수 없을 경우 해당 코드에서 처리)
	
     for (int i = 0; i < menu.getChildren().size(); i++) {
         IMenu child = (IMenu) menu.getChildren().get(i);
         String menu_name = child.getName(s_user.getLocale());
			String menu_link = child.hasTarget() ? child.getTarget() : "N/A";
			String menuId = child.getId();
			String pre_menuId = null;
		
			//이전 메뉴Id가 필요
			if(i>0){ //null은 양옆에 따옴표가 붙으면 안되기 때문에 이러한 처리가 필요한 것이다. 
				IMenu pre_menu= (IMenu) menu.getChildren().get(i-1);
				pre_menuId = "\""+pre_menu.getId()+"\"";
			}
			
			//상위 메뉴 만드는 곳
			//옆에 메뉴 아이디, 메뉴아이디, 이름, 사용여부,이미지관련
			boolean menuStatus = true;
			boolean preMenuStatus = true;
			
			//숨김처리할 메뉴 체크
			for(String hideMenu:hideMenuList) {
				if(menuStatus == true && hideMenu.equals(menuId)) {
					menuStatus = false;
				}
				if(preMenuStatus == true && hideMenu.equals(pre_menuId)) {
					preMenuStatus = false;
				}
			}
			
			if(menuStatus == true) {	//커뮤니티 관려 메뉴들은 생성하지 않는다. 상위메뉴를 생성하지 않으면 하위메뉴들을 생성해도 소용없다.
				if(preMenuStatus == false) {
					%>
					myMenu.addNewSibling(null,"<%=menuId%>","<%=menu_name%>" ,false ,null ,null);
					<%
					enable_menuId = menuId;
				} else {
					if(enable_menuId == null) {
						%>
						myMenu.addNewSibling(null,"<%=menuId%>","<%=menu_name%>" ,false ,null ,null);
						<%
					} else {
						%>
						myMenu.addNewSibling("<%=enable_menuId%>","<%=menuId%>","<%=menu_name%>" ,false ,null ,null);
						<%
					}
					enable_menuId = menuId;
				}
			}

			for(int j=0; j< child.getChildren().size(); j++){
					IMenu sub_menu = (IMenu) child.getChildren().get(j);
					String sub_menu_name = sub_menu.getName(s_user.getLocale());
					String sub_menuId = sub_menu.getId();
					String mileStoneName = sub_menu.getPath();

					if("en".equals(s_user.getLocale().getLanguage())){
						mileStoneName = sub_menu.getPathEn();
					}
					//url 생성
					String sub_url=sub_menu.getTarget();
					String urlparam="&formid="+sub_menuId+"&gridid=GridObj";
					String gbn="?";
					if(sub_url.indexOf("?")>-1) gbn="&"; 
					String menuUrl=sub_url+gbn+"mileStoneName="+java.net.URLEncoder.encode(mileStoneName,"utf-8")+"&leftMenuName="+java.net.URLEncoder.encode(sub_menu_name,"utf-8")+urlparam;
	%>
				//하위 메뉴 만들기
				//부모 메뉴 아디이, 위치넘버, 메뉴아이디, 이름, 사용여부, 이미지관련
				myMenu.addNewChild("<%=menuId%>","<%=j%>","<%=sub_menuId%>","<%=sub_menu_name%>",false,null,null);
				//서브메뉴에 링크걸기 서브메뉴아이디, 주소, 타겟
				myMenu.setHref("<%=sub_menuId%>","<%=contextPath + menuUrl%>","mainFrame");
				
	<%				for(int k=0; k< sub_menu.getChildren().size(); k++){
						IMenu third_menu = (IMenu) sub_menu.getChildren().get(k);
						String third_menu_name = third_menu.getName(s_user.getLocale());
						String third_menuId = third_menu.getId();
						String third_mileStoneName = third_menu.getPath();

						if("en".equals(s_user.getLocale().getLanguage())){
							third_mileStoneName = third_menu.getPathEn();
						}
						//url 생성
						String third_url=third_menu.getTarget();
						String third_urlparam="&formid="+third_menuId+"&gridid=GridObj";
						String third_gbn="?";
						if(third_url.indexOf("?")>-1) gbn="&"; 
								String third_menuUrl=third_url+gbn+"mileStoneName="+java.net.URLEncoder.encode(third_mileStoneName,"utf-8")+"&leftMenuName="+java.net.URLEncoder.encode(third_menu_name,"utf-8")+third_urlparam;
					
	%>
				myMenu.addNewChild("<%=sub_menuId%>","<%=k%>","<%=third_menuId%>","<%=third_menu_name%>",false,null,null);
				//서브메뉴에 링크걸기 서브메뉴아이디, 주소, 타겟
				myMenu.setHref("<%=third_menuId%>","<%=contextPath + third_menuUrl%>","mainFrame");
	<%							
					}
				}
			}
	%>
}

function fn_initialize() { 
	mainFrame.location.href="/mg/main/menu_main.jsp?&formid=PRT_0101&gridid=GridObj";
}

function resize() {
	document.getElementById('iframe_block').style.width = document.body.clientWidth-30;
	document.getElementById('iframe_block').style.height = document.body.clientHeight - document.getElementById('top').offsetHeight;
}

function doChangePwPage() {
    //화면 가운데로 배치
    var top = 100;
    var left = 100;

    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(245,500);
    top = dim[0];
    left = dim[1];

    var url = '/mg/ni/popupChangePassword.jsp';
    var title = '비밀번호 변경';
    var height = 245;
    var width = 500;
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
    
	var ret = window.open(url, title, 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	ret.focus();
	
	return ret;
}

</script>

</head>
<BODY id="thisbody" onload="form_init();" onresize="resize();" unload="form_onunload();" topmargin="0" leftmargin="0" bgcolor="#f8f9f9" style='overflow:auto; padding-left: 15px; margin-right: 15px;'>
<form name="search" style="margin-bottom:0px">
	<input type="hidden" name="btn_flg" 			value=''>
	<input type="hidden" name="order_seq" 			value=''>
	<input type="hidden" name="menu_type" 			value=''>
	<input type="hidden" name="totaldata" 			value=''>
	<input type="hidden" name="lastLoginDateTime" 	value=''>
	<input type="hidden" name="compare_menu_type" 	value=''>
	<input type="hidden" name="flag" 				value=''>
	<input type="hidden" name="top_menuname"		value=''>
	<input type="hidden" name="url" 				value=''>
	<input type="hidden" name="MENU_OBJECT_CODE" 	value=''>
	<input type="hidden" name="mileStoneName"		value="">	
	<input type="hidden" name="leftMenuName"  		value="">
</form>
<div id="top" style="z-index:1;">
	<div class="sub_gnb_bg">
    <table width="100%" border="0">
<%--       <colgroup>
	      <col width="210px" />
	      <col width="" />
      </colgroup> --%>
      <tr>
      <td class="logo_area" style="padding:10px 10px 10px 0px; text-align:left;"><a href="javascript:Init()"><img src="<%=contextPath+logo_image %>" alt="serviceLogo"  class="logo_image"/></a></td>
	    <td>
	        <div class="gnb_userdata" align="right" style="margin-right:0px; margin-bottom:0px;">
	           <%=deptNm %>   <strong><%=userNm %></strong> <%=mm.getMessage("COMG_1033", s_user.getLocale())%>
	           <div class="btnarea">
	           <a href="javascript:doChangePwPage();" class="btn" id="toDisplay" style="display:none;"><span>비밀번호변경</span></a>
	           <a href="javascript:logout();" class="btn"><span>로그아웃</span></a>
	            <!-- <a href="javascript:logout();" class="btn"><img src="../../ext/images/btn_logout1.gif" alt="Logout" /></a> -->
	        </div>
	        </div>
	    </td>
      </tr>
      <tr>
        <td colspan="2" class="gnb_area">
        	<div id="menuObj"></div>
        </td>
      </tr>
    </table>
	</div>
</div>
<!-- <div id="layoutObj" style=" width: 100%;
        height: 100%;
        margin: 0px;
        overflow: hidden;
        z-index:0; resize: both;"></div> -->
<div id="iframe_block" style="z-index:0;">
<iframe name="mainFrame" id="mainFrame" title="mainFrame" marginwidth="0" marginheight="0" scrolling="no" frameborder='no' style="width: 100%; height: 95%; margin-top: 7px;"></iframe>   
</div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp">
	<jsp:param name="grid_object_name_height" value="mainFrame=162"/>
</jsp:include> --%>
</body>
</html>
