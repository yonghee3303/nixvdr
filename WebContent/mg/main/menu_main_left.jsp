<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>

<%@ include file="/ext/include/ui_common.jsp"%>

<% 
	IMenuManagement mm = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IUser s_user = (IUser) session.getAttribute("j_user");
	String qm_init 			= StringUtils.paramReplace(request.getParameter("qm_init"));
	String url 				= StringUtils.paramReplace(request.getParameter("url"));
	String m_path = ("en".equals(s_user.getLocale().getLanguage()) ? "/ext/images/en/" : "/ext/images/");
	String img_src 			= StringUtils.paramReplace(request.getParameter("MENU_IMG"));
	String menu_order       = StringUtils.paramReplace(request.getParameter("menu_order"));
	String menu_object_code = java.net.URLDecoder.decode(StringUtils.paramReplace(request.getParameter("MENU_OBJECT_CODE")), "utf-8");
	String top_menuname 	= java.net.URLDecoder.decode(StringUtils.paramReplace(request.getParameter("top_menuname")), "utf-8");

	if(top_menuname.indexOf("-") > 0) {
		top_menuname = top_menuname.substring(0, top_menuname.indexOf("-"));
	}	
 	String tdClass = "";
	String tempMenuFieldCode = "";
	String tempMenuName = "";
	String menuName = "";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="/ext/css/common.css" rel="stylesheet"  type="text/css"/>
<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script language=javascript src="/ext/js/sec.js"></script>
<script LANGUAGE="JavaScript">

function exe_menu(link, mileStoneName, leftMenuName, formId) {
	
	// alert(link + "," + mileStoneName + "," + leftMenuName + "," + formId);
	var param = "&formid="+formId+"&gridid=GridObj"; 
	
	
	var gbn = "?";
	if(link.indexOf("?") > -1) gbn = "&";
	
	// alert(mileStoneName);
	// alert(link + gbn + "mileStoneName="+encodeURIComponent(mileStoneName)+"&leftMenuName="+encodeURIComponent(leftMenuName)+param);
	
	parent.mainFrame.location.href= link + gbn + "mileStoneName="+encodeURIComponent(mileStoneName)+"&leftMenuName="+encodeURIComponent(leftMenuName)+param;
}

function transfer_form(formname)
{
	// alert(formname);
}

function refresh() {
	var param = "";
	param += "?url=" + "<%=url%>";
	param += "&type=" + "<%=menu_order%>";
	param += "&qm_init=" + "<%=qm_init%>";
	param += "&MENU_OBJECT_CODE=" + "<%=menu_object_code%>";
	param += "&top_menuname=" + "<%=top_menuname%>";
	document.location.href = "menu_main_left.jsp" + param;
}

function displayMenu(menuType){
	if (menuType == "A"){
    	parent.framesetMenu.cols = "50,*";
    	document.all['leftMenu'].style.display = "none";
    	document.all['btn'].style.display = "inline";
    	
	}else{
	    parent.framesetMenu.cols = "210,*";
	    document.all['leftMenu'].style.display = "inline";
	    document.all['btn'].style.display = "none";
    }
	
    parent.topFrame.setMenuType(menuType);
}

function init() 
{
		var linkurl = document.form.linkurl.value;
		var mileStone = document.form.mileStoneName.value;
		var leftMenuName = document.form.leftMenuName.value;
		var formId = document.form.formId.value;
			
		exe_menu(linkurl, mileStone, leftMenuName, formId); 
}

function OnKeyEvent()
{
  var keycode = event.keyCode;
  if(keycode==13) {
     parent.mainFrame.doQuery();
  }
	
}

function goLink(sys) {
	
	/*
	var width = 900;
	var height = 700;
	
	var dim = new Array(2);
    dim = CenterWindow(height,width);
    var top = dim[0];
    var left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'yes';
    var resizable = 'yes';
	
	var ret = window.open("http://cihelp.svcace.com", '', 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
	*/
}

</script>
</head>
<body onload="init();" topmargin="0" leftmargin="0" id="body" onKeyPress="OnKeyEvent();">
<form name="form" method="post">
<span id="leftMenu" style="display:inline">
<table width="186" cellpadding="0" cellspacing="0" border="0">
  <colgroup>
  <col width="180px" />
  <col width="6px" />
  </colgroup>
  <tr>
    <td align="center" valign="top">
    	<table width="160" cellpadding="0" cellspacing="0" border="0">
        	<tr>
            	<td><img src="/ext/images/left_line1.gif" alt="" /></td>
                <td><a href="javascript:displayMenu('A');"><img src="<%=m_path%>btn_left_close.gif" alt="닫기" /></a></td>
            </tr>
        </table>
		 
		<table width="160"  border="0" cellspacing="0" cellpadding="0" class="mgt-15">
		<tr>
			<td>
		<%if(!menu_object_code.equals("")) { %>
				<img src="<%=img_src%>">
		<%} %>	
			</td>
		</tr>

		<%
		if( !menu_order.equals("-1") ) {
			String menu_style = "B"; // A:자식있음,  B:자식없음,  C:자식
			IMenu leftmenu = mm.getMenu(menu_object_code, s_user);
			
			if(!"".equals(menu_object_code)) {
				for (int i = 0; i < leftmenu.getChildren().size(); i++) {
					IMenu child = (IMenu) leftmenu.getChildren().get(i);
					String mileStoneName = child.getPath();
					
					String menuUrl = child.getTarget();
					String formId = child.getId();
					if("en".equals(s_user.getLocale().getLanguage())) {
						menuName = child.getName(s_user.getLocale());
						mileStoneName = child.getPathEn();
					} else {
						menuName = child.getName(); 
					}
					String leftMenuName = menuName;
					tdClass = "left_menu2"; //  "left_menu1";
					menu_style = "B";  // B, C
					%>		
					<tr>
						<td class="<%=tdClass%>"><nobr><a href="javascript:;" class="left" onclick="exe_menu('<%=menuUrl%>', '<%=mileStoneName%>', '<%=leftMenuName%>', '<%=formId%>');"><%=menuName%></a></nobr></td>
						<% if(i == 0 ) { %>
							<input type="hidden" name="linkurl" value="<%=menuUrl%>">
							<input type="hidden" name="mileStoneName" value="<%=mileStoneName%>">
							<input type="hidden" name="leftMenuName" value="<%=leftMenuName%>">
							<input type="hidden" name="formId" value="<%=formId%>">
						<% } %>
					</tr>
				<%	
				}
			}
		}	
		%>
		</table>
	</td>
	<td valign="top"><a href="javascript:displayMenu('B');"><img src="/ext/images/left_line2.gif" alt="" /></a></td>
</tr>

</table>
</span>
<span id="btn" style="display:none">
	<table width="22" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><a href="javascript:displayMenu('B');"><img src="<%=m_path%>btn_open.gif" alt="펼치기" /></a></td>
	</tr>
	<tr>
		<td><img src="/ext/images/left_line3.gif" alt="" /></td>
	</tr>

	</table>
</span>
</form>

</body>
</html>
