<%@ page contentType = "text/html; charset=UTF-8"  session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>

<%
	String type = request.getParameter("type");
    IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	
    String initpage = paramemter.getString("component.site.initpage");
    String initmenu = paramemter.getString("component.site.initmenu");
    String logo_image =  paramemter.getString("component.site.image");
    String menuimg =  paramemter.getString("component.site.initmenuimage");

    IUser s_user = (IUser) session.getAttribute("j_user");
    String m_path = ("en".equals(s_user.getLocale().getLanguage()) ? "/ext/images/en/" : "/ext/images/");
	String userNm = s_user.getName();
	String deptNm = s_user.getProperty("DEPT_NM").toString();
%>

<html>
<head>
<link rel="stylesheet" href="/ext/css/common.css" type="text/css">
<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script language=javascript src="/ext/js/sec.js"></script>
<script language="JavaScript">

var typeParam = "<%=type%>";
var menutype = "A";
var initflag = true;

function change(a,b) {
  	var c = document.search.btn_flg.value;
  	// if(b != c) document.getElementById('juil_'+b).src = a;
}

function change2(d, val) {
	var c = 1;
    document.search.btn_flg.value = c;
}

function Init() {
	parent.fn_initialize();
	// parent.leftFrame.displayMenu('A');
	// parent.document.location.href = "/mg/main/main.jsp?loginFlag=true&initFlag=true";
}

function logout() {
	if(confirm("<%=mm.getMessage("COMG_1034", s_user.getLocale())%>")) {  // 시스템을 종료 하시겠습니까 ?
	   top.location.href = "/Logout.do";
	}
}

function setMenuType(type) {
	menutype = type;
}

function menu_click(menu_order, menu_code, left_image) 
{   //사용자가 상단메뉴 클릭했을 때 이벤트
	//메뉴 클릭시 좌측 메뉴 열림
	parent.framesetMenu.cols = "210,*";
	// alert(menu_order + "," + menu_code + "," + left_image);
	
   	var leftmenu = "menu_main_left.jsp?qm_init=true&top_menuname="+encodeURIComponent(document.search.top_menuname.value);
	var exeurl = "&url="+document.search.url.value
	var menucode = "&MENU_OBJECT_CODE="+menu_code + "&menu_order=" + menu_order;
		 menucode += "&MENU_IMG=" + left_image;
		
	parent.leftFrame.location.href=leftmenu + exeurl + menucode;
	
	if(initflag == true) {setMenuType
		parent.framesetMenu.cols = "210,*";
		// parent.leftFrame.displayMenu("B");
	}
}

function load_left_frame(type,menu_code) {
	parent.leftFrame.location.href="menu_main_left.jsp?qm_init=true&top_menuname="+encodeURIComponent(document.search.top_menuname.value)+"&url="+document.search.url.value+"&MENU_OBJECT_CODE="+menu_code + "&type=" + type ;
}

function form_onunload()
{
    top.location.href = "/Logout.do";
}

function form_init()
{
	parent.framesetMenu.cols = "20,*";
	menu_click('<%=initpage%>', '<%=initmenu%>', '<%=m_path%><%=menuimg%>');
}

</script>


</HEAD>
<BODY onload="form_init();" unload="form_onunload();" topmargin="0" leftmargin="0" bgcolor="#f8f9f9" >
<form name="search" style="margin-bottom:0px">
	<input type="hidden" name="btn_flg" 			value=''>
	<input type="hidden" name="order_seq" 			value=''>
	<input type="hidden" name="menu_type" 			value=''>
	<input type="hidden" name="totaldata" 			value=''>
	<input type="hidden" name="lastLoginDateTime" 	value=''>
	<input type="hidden" name="compare_menu_type" 	value=''>
	<input type="hidden" name="flag" 				value=''>
	<input type="hidden" name="top_menuname"		value=''>
	<input type="hidden" name="url" 				value=''>
	<input type="hidden" name="MENU_OBJECT_CODE" 	value=''>
	<input type="hidden" name="mileStoneName"		value="">	
	<input type="hidden" name="leftMenuName"  		value="">
</form>
<div class="sub_gnb_bg">
    <table width="100%" border="0">
      <colgroup>
	      <col width="210px" />
	      <col width="" />
      </colgroup>
      <tr>
        <td rowspan="2" class="logo_area"><a href="javascript:Init()"><img src="<%=logo_image %>" alt="serviceLogo"  class="logo_image"/></a></td>
	    <td>
	        <div class="gnb_userdata" align="right">
	           <%=deptNm %>   <strong><%=userNm %></strong> <%=mm.getMessage("COMG_1033", s_user.getLocale())%>
	            <a href="javascript:logout();" class="btn"><img src="/ext/images/btn_logout1.gif" alt="Logout" /></a>
	        </div>
	    </td>
      </tr>
      <tr>
        <td class="gnb_area">
        	 <ul class="main_menu">
       		   <%
       		   	IMenu menu = mnu.getMenu("CP", s_user);
		        for (int i = 0; i < menu.getChildren().size(); i++) {
		            
		            IMenu child = (IMenu) menu.getChildren().get(i);
		            String menu_name = child.getName();
        			String menu_link = child.hasTarget() ? child.getTarget() : "N/A";
        			String val = child.getId();
        			String menuId = child.getId();
        			String menu_image = m_path + child.getImage();
        			String down_image = m_path + child.getImage().replace(".gif", "") + "down.gif";
        			String over_image = m_path + child.getImage().replace(".gif", "") + "over.gif";
        			String left_image = m_path + child.getDescription();
        			
              		if(!menuId.equals("") && i < 4) {
						out.println("<li><a parent.leftFrame.location.href='"+(val.equals("") ? "javascript:;" : "menu_main_left.jsp?top_menuname="+java.net.URLEncoder.encode(menu_name, "utf-8")+"&url="+menu_link+"&MENU_OBJECT_CODE="+val)+"' target='left' onClick=menu_click('"+i+"','"+val+"','"+left_image+"') onFocus='this.blur()'><img src='"+menu_image+"'  id='juil_"+i+"' onMouseOver=change('"+ over_image +"','"+i+"') onMouseOut=change('"+down_image+"','"+i+"') style='cursor:hand'></a></li>"+"\n");
              		} else {
						out.println("<li class='last'><a parent.leftFrame.location.href='"+(val.equals("") ? "javascript:;" : "menu_main_left.jsp?top_menuname="+java.net.URLEncoder.encode(menu_name, "utf-8")+"&url="+menu_link+"&MENU_OBJECT_CODE="+val)+"' target='left' onClick=menu_click('"+i+"','"+val+"','"+left_image+"') onFocus='this.blur()'><img src='"+menu_image+"'  id='juil_"+i+"' onMouseOver=change('"+ over_image+"','"+i+"') onMouseOut=change('"+ down_image+"','"+i+"') style='cursor:hand'></a></li>"+"\n");
              		}
        		}
				%>
			</ul>
        </td>
      </tr>
    </table>
</div>
</BODY>
</HTML>
