<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="java.text.*" %>
<%@ page import="java.net.*"%>
<%@ page import="java.util.Vector"%>

<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>



<%@ include file="../../ext/include/ui_common.jsp" %>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%
Log log = LogFactory.getLog(this.getClass());
String contextPath = request.getContextPath();
String type = request.getParameter("type");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);

String company = paramemter.getString("component.site.product");
String logo_image =  paramemter.getString("component.site.pp.image.foreign");
String footterUse = paramemter.getString("component.ui.footer.useYn");
IUser s_user = (IUser) session.getAttribute("j_user");
String locale = s_user.getLocale().getLanguage();
String m_path = ("en".equals(locale) ? "/ext/images/en/" : "/ext/images/");
String userNm = s_user.getName();
String deptNm = s_user.getProperty("DEPT_NM").toString();

//로그인 사용자의 권한을 가져와 관리자/사용자 초기화면을 다르게 설정
String pgmType ="";

if((String)request.getParameter("flag")!=null){
	pgmType = (String)request.getParameter("flag");
}
//log.info("pgmType : " + pgmType);

boolean isAdmin = false;
String initpage = "";

//CP커뮤니티인 경우에는 flag값 없이 진입
if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
	if(pgmType.equals("CP")){
		//log.info("관리자 CP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cp.initpage.admin");	
	}else if(pgmType.equals("SU")){
		//log.info("관리자 SU 초기화면 세팅");
		initpage = paramemter.getString("component.site.su.initpage.admin");
	}else{
		//log.info("관리자 CPCOMP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cpcomp.initpage.admin");
	}
}else{
	if(pgmType.equals("CP")){
		//log.info("사용자 CP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cp.initpage.user");	
	}else if(pgmType.equals("SU")){
		//log.info("사용자 SU 초기화면 세팅");
		initpage = paramemter.getString("component.site.su.initpage.user");
	}else{
		//log.info("사용자 CPCOMP 초기화면 세팅");
		initpage = paramemter.getString("component.site.cpcomp.initpage.user");
	}
}

String css = paramemter.getString("component.ui.portal.css"); 
//Image locale
if(locale.equals("ko")) {
	locale = "";
} else if(locale.equals("en") || locale.equals("zh")) {
	locale = "/" + locale;
} else {
	locale = "/en";
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../../ext/css/cppp_portal_<%=css %>.css" />
<link rel="stylesheet" href="../../ext/css/common.css" type="text/css">
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script language=javascript src="../../ext/js/sec.js"></script>

<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>

<title><%=mm.getMessage("WTMG_1001", s_user.getLocale())%></title>
<script language="javascript">
var typeParam = "<%=type%>";
var menutype = "A";
var initflag = true;


//var winCreateSecureUSB;
//var winSelectUser;
window.onresize = function() { 
/* 	document.getElementById("mainFrame").style.height=getWindowHeight()-138;*/  
	document.getElementById("mainFrame").style.height=650;
}
function getWindowWidth(){
	var w = 0;

	if (typeof (window.innerWidth) == 'number') { //Chrome
		 w = window.innerWidth;
	} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		 w = document.documentElement.clientWidth;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) { //IE9
		 w = document.body.clientWidth;
	}
	return w;
}
function getWindowHeight(){
	var h = 0;

	if (typeof (window.innerWidth) == 'number') { //Chrome
		 h = window.innerHeight;
	} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		 h = document.documentElement.clientHeight;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) { //IE9
		 h = document.body.clientHeight;
	}
	return h;
}
function doAjax(){
	var url = "/mg/cppp/member.do"+"?mod=checkPassChangeDate&USER_ID=<%=s_user.getId().toString()%>";
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj){
	var jData = JSON.parse(oj.responseText);
	 
	 var result = jData.RESULT; //사용현황-신청건수
	 if(result=="false"){ //비밀번호를 바꾼지 90일이 지났으면 
		 location.href = "/cppp/changePass.jsp";
	 }else{ //비밀번호를 바꾸진 90일이 지나지 않았으면 
		//추가
			doOnLoad();
			//initLayout();
			var page = "<%=initpage%>";
			/* 	document.getElementById("mainFrame").style.height=getWindowHeight()-138;*/  
			document.getElementById("mainFrame").style.height=720;
			document.mainFrame.location.href = "../.." + page;
	 }
}
function form_init()
{
	//CHG_DT가 90일이 지났을 경우 패스워드변경 창으로 전환해야된다.
	doAjax();
	
	<%-- //추가
	doOnLoad();
	//initLayout();
	
	var page = "<%=initpage%>";
	/* 	document.getElementById("mainFrame").style.height=getWindowHeight()-138;*/  
	document.getElementById("mainFrame").style.height=720;
	document.mainFrame.location.href = "../.." + page; --%>
}

function pageRedirect(page,argu){

	document.mainFrame.location.href = "../../" + page + argu;
}


function change(a,b) {
  	var c = document.search.btn_flg.value;
  	// if(b != c) document.getElementById('juil_'+b).src = a;
}

function change2(d, val) {
	var c = 1;
    document.search.btn_flg.value = c;
}

function Init() {
	document.mainFrame.location.href ="<%=contextPath+initpage%>";
}

function logout() {
	if(confirm("<%=mm.getMessage("COMG_1034", s_user.getLocale())%>")) {  // 시스템을 종료 하시겠습니까 ?
	   top.location.href = "<%=contextPath%>/Logout.do";
	}
}

function setMenuType(type) {
	menutype = type;
}

function menu_click(menu_order, menu_code, left_image) 
{   //사용자가 상단메뉴 클릭했을 때 이벤트
	//메뉴 클릭시 좌측 메뉴 열림
	parent.framesetMenu.cols = "210,*";
	
   	var leftmenu = "menu_main_left.jsp?qm_init=true&top_menuname="+encodeURIComponent(document.search.top_menuname.value);
	var exeurl = "&url="+document.search.url.value;
	var menucode = "&MENU_OBJECT_CODE="+menu_code + "&menu_order=" + menu_order;
		 menucode += "&MENU_IMG=" + left_image;
		
	parent.leftFrame.location.href=leftmenu + exeurl + menucode;
	
	if(initflag == true) {
		parent.framesetMenu.cols = "210,*";
	}
}

function load_left_frame(type,menu_code) {
	parent.leftFrame.location.href="menu_main_left.jsp?qm_init=true&top_menuname="+encodeURIComponent(document.search.top_menuname.value)+"&url="+document.search.url.value+"&MENU_OBJECT_CODE="+menu_code + "&type=" + type ;
}

function form_onunload()
{
    top.location.href = "<%=contextPath%>/Logout.do";
}



var myMenu;
function doOnLoad(){
	myMenu = new dhtmlXMenuObject("menuObj");
	
	//web모드는 마우스 오버 일 때 자동으로 열림
	//win모드 일 때는 클릭해야 열린다.
	//myMenu.setOpenMode("win");
	//slide로 열리면서 투명하게 효과를 넣어봤다.
	myMenu.enableEffect("slide+", 150, 85);
	
	<% IMenu menu = mnu.getMenu("CPCOM", s_user);
     for (int i = 0; i < menu.getChildren().size(); i++) {
         IMenu child = (IMenu) menu.getChildren().get(i);
         String menu_name = child.getName(s_user.getLocale());
			String menu_link = child.hasTarget() ? child.getTarget() : "N/A";
			String menuId = child.getId();
			String pre_menuId = null;
		
			//이전 메뉴Id가 필요
			if(i>0){ //null은 양옆에 따옴표가 붙으면 안되기 때문에 이러한 처리가 필요한 것이다. 
				IMenu pre_menu= (IMenu) menu.getChildren().get(i-1);
				pre_menuId = "\""+pre_menu.getId()+"\"";
			} 
	%>
			//상위 메뉴 만드는 곳
			//옆에 메뉴 아이디, 메뉴아이디, 이름, 사용여부,이미지관련
			myMenu.addNewSibling(<%=pre_menuId%>,"<%=menuId%>","<%=menu_name%>" ,false ,null ,null);
			
	<%			
			//하위 메뉴 리스트 만들기 위함...
			for(int j=0; j< child.getChildren().size(); j++){
					IMenu sub_menu = (IMenu) child.getChildren().get(j);
					String sub_menu_name = sub_menu.getName(s_user.getLocale());
					String sub_menuId = sub_menu.getId();
					String mileStoneName = child.getName(s_user.getLocale());//sub_menu.getPath();

					if("en".equals(s_user.getLocale().getLanguage())){
						mileStoneName = sub_menu.getPathEn();
					}
					//url 생성
					String sub_url=sub_menu.getTarget();
					String urlparam="&formid="+sub_menuId+"&gridid=GridObj";
					String gbn="?";
					if(sub_url.indexOf("?")>-1) gbn="&"; 
					String menuUrl=sub_url+gbn+"mileStoneName="+java.net.URLEncoder.encode(mileStoneName,"utf-8")+"&leftMenuName="+java.net.URLEncoder.encode(sub_menu_name,"utf-8")+urlparam;
	%>
				//하위 메뉴 만들기
				//부모 메뉴 아디이, 위치넘버, 메뉴아이디, 이름, 사용여부, 이미지관련
				myMenu.addNewChild("<%=menuId%>","<%=j%>","<%=sub_menuId%>","<%=sub_menu_name%>",false,null,null);
				//서브메뉴에 링크걸기 서브메뉴아이디, 주소, 타겟
				myMenu.setHref("<%=sub_menuId%>","<%=contextPath + menuUrl%>","mainFrame");
				
	<%				for(int k=0; k< sub_menu.getChildren().size(); k++){
						IMenu third_menu = (IMenu) sub_menu.getChildren().get(k);
						String third_menu_name = third_menu.getName(s_user.getLocale());
						String third_menuId = third_menu.getId();
						String third_mileStoneName = sub_menu.getName(s_user.getLocale());//third_menu.getPath();

						if("en".equals(s_user.getLocale().getLanguage())){
							third_mileStoneName = third_menu.getPathEn();
						}
						//url 생성
						String third_url=third_menu.getTarget();
						String third_urlparam="&formid="+third_menuId+"&gridid=GridObj";
						String third_gbn="?";
						if(third_url.indexOf("?")>-1) gbn="&"; 
								String third_menuUrl=third_url+gbn+"mileStoneName="+java.net.URLEncoder.encode(third_mileStoneName,"utf-8")+"&leftMenuName="+java.net.URLEncoder.encode(third_menu_name,"utf-8")+third_urlparam;
					
	%>
				myMenu.addNewChild("<%=sub_menuId%>","<%=k%>","<%=third_menuId%>","<%=third_menu_name%>",false,null,null);
				//서브메뉴에 링크걸기 서브메뉴아이디, 주소, 타겟
				myMenu.setHref("<%=third_menuId%>","<%=contextPath + third_menuUrl%>","mainFrame");
	<%							
					}
				}
			}
	%>
}
function fn_initialize() { 
	mainFrame.location.href="/mg/main/menu_main.jsp?&formid=PRT_0101&gridid=GridObj";
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
function privacyInfo(){
	centerWin("/cppp/privacyInfo.jsp",720,480,"yes");
	//window.open("/cppp/privacyInfo.jsp",'','width=720px,height=480px,scrollbars=yes,resizable=no');
}

function emailInfo(){
	centerWin("/cppp/emailInfo.jsp",395,207,"yes");
	//window.open("/cppp/emailInfo.jsp",'','width=395px,height=207px,scrollbars=yes,resizable=no');
}

function clickQuestion(){
	centerWin("/mg/common/inquiry.html",189,117,"no");
	//window.open("/mg/common/inquiry.html",'','width=189px,height=117px,scrollbars=no,resizable=no');
}
</script>
<style>
/* body 
{	margin:0; padding:0px;  text-align:center; overflow:hidden; background-image: url(../../ext/images/menu-background.png); background-repeat: repeat-x; }
#body_wrap {padding-left:15px; margin:0; width:1100px;  text-align:center; }
.dhtmlxMenu_dhx_skyblue_Middle{
	height:48px;
	background-color:#0070bd;
}
div.dhtmlxMenu_dhx_skyblue_TopLevel_Item_Normal,
div.dhtmlxMenu_dhx_skyblue_TopLevel_Item_Disabled {
  border: 1px solid #0070bd;
}
DIV.dhtmlxMenu_dhx_skyblue_TopLevel_Item_Normal{
	line-height:42px;
	font-size:18px;
	font-weight:bold;
	color:#ffffff;
}
DIV.dhtmlxMenu_dhx_skyblue_TopLevel_Item_Selected{
	line-height:42px;
	font-size:18px;
	font-weight:bold;
	color:#ffffff;
	border:1px solid #2281d1;
	background-color:#2281d1;
}
DIV.dhtmlxMenu_dhx_skyblue_SubLevelArea_Polygon TABLE.dhtmlxMebu_SubLevelArea_Tbl DIV.sub_item_text{
	line-height:39px;
	font-size:15px;
	font-weight:bold;
	color:#ffffff;
}
DIV.dhtmlxMenu_dhx_skyblue_SubLevelArea_Polygon TABLE.dhtmlxMebu_SubLevelArea_Tbl TR.sub_item_selected TD{
	line-height:39px;
	border:0px solid #2281d1;
	background-color:#1c4ea1;
	font-size:15px;
	font-weight:bold;
	color:#ffffff;
}
DIV.dhtmlxMenu_dhx_skyblue_SubLevelArea_Polygon{
	border:1px solid #2281d1;
	background-color:#2281d1;
}
div.dhtmlxMenu_dhx_skyblue_SubLevelArea_Polygon table.dhtmlxMebu_SubLevelArea_Tbl td{
 	border-top: 1px solid #2281d1;
 	border-bottom: 1px solid #2281d1;
} 메뉴 블루*/
</style>
</head>
<BODY onload="form_init();" unload="form_onunload();" topmargin="0" leftmargin="0" class="cpComMainWrap">
<form name="search" style="margin-bottom:0px">
	<input type="hidden" name="btn_flg" 			value=''>
	<input type="hidden" name="order_seq" 			value=''>
	<input type="hidden" name="menu_type" 			value=''>
	<input type="hidden" name="totaldata" 			value=''>
	<input type="hidden" name="lastLoginDateTime" 	value=''>
	<input type="hidden" name="compare_menu_type" 	value=''>
	<input type="hidden" name="flag" 				value=''>
	<input type="hidden" name="top_menuname"		value=''>
	<input type="hidden" name="url" 				value=''>
	<input type="hidden" name="MENU_OBJECT_CODE" 	value=''>
	<input type="hidden" name="mileStoneName"		value="">	
	<input type="hidden" name="leftMenuName"  		value="">
</form>
<div id="body_wrap" class="cpComMainBox">
<div id="top" class="cpComMainTop">
    <table>
    	<tr>
      <!--  <th rowspan="2" class="logo_area"><a href="javascript:Init()"> --><th><a href="javascript:Init()"><img src="<%=contextPath%>/ext/images<%=locale%><%=logo_image %>" alt="serviceLogo" /></a></th>
	   		<td>
	        	<div class="gnb_userdata">
	           <%=deptNm %>   <strong><%=userNm %></strong> <%=mm.getMessage("COMG_1033", s_user.getLocale())%>
	            <a href="javascript:logout();" class="btn"><img src="../../ext/images/btn_logout1.gif" alt="Logout" /></a>
	        	</div>
	    	</td>
      	</tr>
      </table>    
      <table>
      	<tr>
      		<td>
        		<div id="menuObj"></div>
      		</td>
     	 </tr>
      </table>
</div>
<!-- </div> -->
<div class="cpComMainContentBox">
<iframe name="mainFrame" id="mainFrame" title="mainFrame" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no' class="cpSubGrid4"></iframe>   
</div>
</div>
<%if("Y".equals(footterUse)){ %>
<div class="footerPoral">
	<div>
		<dl>
		<%if("LGCHEM".equals(paramemter.getString("component.site.company"))) {%>
		<dt><img src="/ext/images/footer_Lglogo.png"></dt>
		<%} else {%>
		<dt></dt>
		<%}%>
		<dd class="footerPoral_address">
		<span><a href="javascript:clickQuestion();"><%=mm.getMessage("FOMG_0001")%></a></span>
		<span class="footerTxt1"><a href="javascript:privacyInfo();"><%=mm.getMessage("CPCOM_1218")%></a></span>
		<span><a href="javascript:emailInfo();"><%=mm.getMessage("FOMG_0002")%></a></span><br />
		<%=mm.getMessage("FOMG_0003")%></dd>
		<%if("LGCHEM".equals(paramemter.getString("component.site.company"))) {%>
		<dd class="footerPoral_banner">
		<a href="http://ethics.lg.co.kr/ETHICS/Korean/Jebo/JeboReceive.aspx" target="blank"><img src="/ext/images/footer_jungdo.gif" target="_blank" /></a>
		<a href="http://ethics.lg.co.kr/ETHICS/Korean/Collaboration/Twins.aspx" target="blank"><img src="/ext/images/with_lg.gif" /></a></dd>
		<%}%>
		</dl>
	</div>
</div>
<%} %>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp">
	<jsp:param name="grid_object_name_height" value="mainFrame=162"/>
</jsp:include> --%>
</body>
</html>
