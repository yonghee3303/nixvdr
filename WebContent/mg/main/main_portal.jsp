<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>    

<%
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IUser s_user = (IUser) session.getAttribute("j_user");

//로그인 사용자의 권한을 가져와 메인포털 화면배치를 다르게 설정
boolean isAdmin = false;
String initpage = "";
if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
}


//서버에서 보내준 레이아웃 정보
int[] arr_layout = new int[] {4, 2, 2};
String[] arr_url = new String[] {"../../mg/su/mgmt/chgPassword.jsp","../../mg/su/main/selectUSB.jsp","../../mg/su/main/importList.jsp","../../mg/su/main/exportList.jsp","../../mg/su/main/userDetailLayout.jsp","../../mg/su/main/mappingUser.jsp","../../mg/su/main/checkList.jsp","../../mg/sys/userList.jsp"};
String[] arr_formId = new String[] {"","","SU_0105","SU_0106","SU_0108","","","CP_0503"};
String[] arr_gridId = new String[] {"GridObj","GridObj","GridObj","GridObj","GridObj","GridObj","GridObj","GridObj"};


//레이아웃 총 개수 구함
int layoutCnt = 0;

for(int i=0;i<arr_layout.length;i++){
	
	layoutCnt += arr_layout[i];
	
}

%>

<html>
<head>

<title>Main Portal</title>

<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

//전역변수 동적 선언
var layoutCnt = <%=layoutCnt%>;

<%for(int i=1;i<layoutCnt+1;i++){%>
	
	var layout<%=i%>;
	var cell<%=i%>;
	
<%}%>

var arr_url = new Array(<%=layoutCnt%>);
var arr_formId = new Array(<%=layoutCnt%>);
var arr_gridId = new Array(<%=layoutCnt%>);


function init() {
	
	//자바스크립트 배열로 데이터 전달
	<%for(int i=0;i<layoutCnt;i++){%>
	
		arr_url[<%=i%>] = "<%=arr_url[i]%>";
		arr_formId[<%=i%>] = "<%=arr_formId[i]%>";
		arr_gridId[<%=i%>] = "<%=arr_gridId[i]%>";
		
	<%}%>
	
	
	
	//총 개수만큼 레이아웃객체 동적 생성 및 페이지 연결
	for(var i=1;i<layoutCnt+1;i++){
		
		eval(
				"layout" + i + "=new dhtmlXLayoutObject({"
			    + "parent: document.getElementById(\"layoutObj"+ i + "\"),"
			    + "pattern: \"1C\","
			    + "skin: \"dhx_web\""
			+ "});"
			+ "cell"+ i +" = layout"+ i +".cells(\"a\");"
			/* + "cell"+ i +".setText(\"" + i + "번째 레이아웃\");" */
			+ "cell"+ i +".hideHeader();"
			);
		
		eval(
				"cell"+i+".attachURL(\""+ arr_url[i-1] +"\",null,  {"
				+	"formid: \""+ arr_formId[i-1] +"\","
				+	"gridid: \""+ arr_gridId[i-1] +"\""
				+"});"
		);
		
	}
	
	
	
	
	if(<%=isAdmin%> == true) {
		document.getElementById("auth").value = "관리자 화면배치가 적용";
	}else{
		document.getElementById("auth").value = "일반사용자 화면배치가 적용";
	}
	
}

function goCpMain() {
	location.href = "./su_main.jsp?flag=cp";
}

function goSuMain() {
	location.href = "./su_main.jsp?flag=su";
}


function resize() {
	
	<%for(int i=1;i<layoutCnt+1;i++){%>
		
		layout<%=i%>.setSizes();
		
	<%}%>
	
}

</script>

<style>


/* 동적으로 선택자 생성 */
<%for(int i=1;i<layoutCnt+1;i++){
	
	if(i==layoutCnt){
		out.print("#layoutObj"+i);
	}else{
		out.print("#layoutObj"+i+",");
	}
}
%> {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px,0px,0px,0px;
        overflow: hidden;
    }


</style>

</head>


<body onload="init();" onresize="resize();" style="overflow:auto;">
<center>
<input type="text" id="auth" /><br />
<button onclick="goCpMain();">CP 메인으로(임시)</button>
<button onclick="goSuMain();">보안USB 메인으로(임시)</button>
</center>


<!-- 동적 테이블 생성 -->
<%
int cnt = 1;

for(int i=0;i<arr_layout.length;i++){%>
	<center>
	<table border="0" width="1400px" height="<%=(900/arr_layout.length)%>px" margin="0" padding="0"><tr>
	
		<%for(int j=0;j<arr_layout[i];j++){%>
			<td><div id="layoutObj<%=cnt %>" style="position: relative;"></div></td>
		<%cnt++;
			}%>
		
	</tr></table></center>
<%}%>

</body>
</html>