<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  String deptCd = s_user.getProperty("DEPT_CD").toString();
  String deptNm = s_user.getProperty("DEPT_NM").toString();
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>사용자별 권한관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="/ext/include/grid_common.jsp"%> --%> <%@ include file="../../ext/include/su_grid_common.jsp" %> <%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/user.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	// GridObj.setColSorting("<%=grid_col_sort%>");
   	// doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	var user_id = GridObj.cells(rowId, GridObj.getColIndexById("USER_ID")).getValue();
	parent.parent.refresh(user_id);
}

function doOnRowSelect(rowId, cellInd) {}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var user_id = parent.document.form.USER_ID.value;
	var dept_cd = parent.document.form.DEPT_CD.value;
	var user_nm = parent.document.form.USER_NM.value;
	var auth_cd = RTrim(parent.document.form.ROLL_GRP_CD.value);
	
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+user_id;
		argu += "&DEPT_CD="	+encodeURIComponent(dept_cd);
		argu += "&USER_NM="	+encodeURIComponent(user_nm);
		argu += "&ROLL_GRP_CD="	+encodeURIComponent(auth_cd);			
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	// document.getElementById("totalCntTD").innerHTML = "총 <span class='point'> : " + GridObj.getRowsNum()+"</span> 건 ";

	// if(status == "false") alert(msg);
	return true;
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}


function init() {
	setFormDraw();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();">
<!-- <form name="form" method="post">
<input type="file" name= "browseFile" style="display='none';">  

</form> -->
<div id="gridbox" name="gridbox" height="100%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=0"/>
</jsp:include> --%>
</body>
</html>
