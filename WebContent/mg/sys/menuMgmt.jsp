<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   
   IUser search_user = (IUser) session.getAttribute("j_user");
%>

<html>
<head>
<title>화면제목</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<script>
function init() {
	
	var param = "&formid=<%=formId%>&gridid=<%=grid_obj%>";
	
	menuframe.location.href='../../mg/common/su_menu_form.jsp?eventframe=menuEditframe';
    menuEditframe.location.href='../../mg/sys/menuList.jsp?' + param;
}

function refresh() {
    menuframe.location.href='../../mg/common/su_menu_form.jsp?eventframe=menuEditframe';
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
	  <%@ include file="../../mg/common/milestone.jsp"%>

      <table width="100%" height="100%" border="0">
        <colgroup>
        <col width="250px" />
        <col width="10px" />
        <col width="" />
        </colgroup>
        <tr>
          <td valign="top"><table width="100%" height="100%" border="0">
              <tr>
                <td class="leftrightdiv">
					<iframe name="menuframe" width="100%" height="500px" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no'></iframe>
				</td>
              </tr>
            </table></td>
          <td></td>
          <td valign="top"><table width="100%" height="100%" border="0">
              <tr>
                <td class="leftrightdiv" valign="top">
                	<div>
                		<iframe name="menuEditframe" width="100%" height="100%" valign="top" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
                	</div>
                </td>
              </tr>
            </table></td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
</body>
</html>
