<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   IUser s_user = (IUser) session.getAttribute("j_user");
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>사용자권한</title>
<%@ include file="../../ext/include/include_css.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>

<script>

function init() 
{

	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>" + "&user_id=";
	userframe.location.href='../../mg/sys/authUserList.jsp?eventframe=authframe' + argu;
	
	<%-- argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&user_id=";
	authframe.location.href='/mg/sys/authSubList.jsp?' + argu; --%>
}

function refresh(user_id) 
{	
	var argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&userId=" + user_id;
	authframe.location.href='/mg/sys/authSubList.jsp?' + argu;
}

function doQuery() 
{
	var user_id = document.form.USER_ID.value;
	var dept_cd = document.form.DEPT_CD.value;
	var user_nm = document.form.USER_NM.value;
	var auth_cd = RTrim(document.form.ROLL_GRP_CD.value);
	
	if(user_id == "" && dept_cd  == "" && user_nm == "" && auth_cd == "" ) {
		alert("<%=mm.getMessage("COMG_1041", s_user.getLocale())%>");
		return;
	}
	
	userframe.doQuery();
}

function doDeptPop() {
	popupDeptTree("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant", "OkPopupDept");
}

function OkPopupDept(dept_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}

</script>

</head>
<body onload="init();" style="overflow:hidden">
<form name="form" method="post">
<table width="100%" height="100%">
<tr>
   	<td>
	  <%-- <%@ include file="/mg/common/milestone.jsp"%> --%>
	  
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px" />
            <col width="80px" />
            <col width="80px" />
            <col width="110px" />
            <col width="80px" />
            <col width="120px" />
            <col width="80px" />
            <col width="210px" />
            <col/>
        </colgroup>
           <!--  <col width="6%" />
            <col width="8%" />
            <col width="6%" />
            <col width="10%" />
            <col width="10%" />
            <col width="12%" />
            <col width="6%" />
            <col width="30%" />
            <col width="80px" /> -->
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1020", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="USER_ID"  size="6" class="text" onchange="document.form.USER_NM.value=''">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1021", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_NM" size="10" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1044", s_user.getLocale())%></td>
            <td>			
				<jsp:include page="../../mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0103"/>
					<jsp:param name="tagname" value="ROLL_GRP_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
        	<td class="tit"><%=mm.getMessage("COMG_1025", s_user.getLocale())%></td>
            <td>
				<input type="text" name="DEPT_CD"  size="6"  value="" class="text" onchange="document.form.DEPT_NM.value=''" readonly="readonly">
				<a href="javascript:doDeptPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="Search" class="mgl-5" /></a>
				<input type="text" name="DEPT_NM"  size="14" value="" class="text" readonly="readonly">
			</td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
      </table>
   </td>
</tr>

<tr>	     
        <td height="70%" valign="top">
				<iframe name="userframe" height="100%" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
		</td>
</tr>
</table>
</form>
<%-- <jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="authframe=162"/>
</jsp:include> --%>
</body>
</html>
