<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   IUser s_user = (IUser) session.getAttribute("j_user");
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>Agent 설치현황</title>
<%@ include file="/ext/include/include_css.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="/ext/js/JDate.js"></script>
<script type="text/javascript" src="/ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="/jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="/jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>

function init() 
{
	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>" + "&user_id=";
	masterframe.location.href='/mg/report/pcAccessList.jsp?eventframe=detailframe' + argu;
	
	argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&dptCd=";
	detailframe.location.href='/mg/report/pcAccessLog.jsp?' + argu;
}

function refresh(dptCd) 
{	
	var argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&dptCd=" + dptCd;
	detailframe.location.href='/mg/report/pcAccessLog.jsp?' + argu;
}

function doQuery() 
{
	masterframe.doQuery();
}

function doDeptPop() {
	popupDeptAuth("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant",'<%=s_user.getProperty("USER_ROLL").toString()%>' , "OkPopupDept");
}

function OkPopupDept(dept_cd, plant_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}

</script>

</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%" hright="100%">
<tr>
   	<td>
	  <%@ include file="/mg/common/milestone.jsp"%>
	  
        <table width="100%" class="board-search">
        <colgroup>
            <col width="6%" />
            <col width="8%" />
            <col width="6%" />
            <col width="10%" />
            <col width="10%" />
            <col width="12%" />
            <col width="6%" />
            <col width="30%" />
            <col width="80px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1020", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="USER_ID"  size="6" class="text" onchange="document.form.USER_NM.value=''">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1021", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_NM" size="10" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1047", s_user.getLocale())%></td>
            <td>			
				<jsp:include page="/mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0402"/>
					<jsp:param name="tagname" value="SEARCH_CL_CD"/>
				</jsp:include>
            </td>
        	<td class="tit"><%=mm.getMessage("COMG_1048", s_user.getLocale())%></td>
            <td>
            	<INPUT type="checkbox" id="isDate" >
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id="imgToDate" src="/ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
			</td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
      </table>
   </td>
</tr>

<tr>
	<td>
      <table width="100%" border="0">
       <rowgroup>
            <row height="20%" />
            <row height="40%" />
        </rowgroup>
        <tr>
          <td valign="top">
				<iframe name="masterframe" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
        </tr>
        <tr>
          <td valign="top">
               <iframe name="detailframe" width="100%" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
           </td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="detailframe=162"/>
</jsp:include>
</body>
</html>
