<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
// Dthmlx Grid 전역변수들..
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>외부장치 사용현황 조회</title>
<%@ include file="/ext/include/include_css.jsp"%>
<%@ include file="/ext/include/grid_common.jsp"%><%-- <%@ include file="/ext/include/su_grid_common_render.jsp" %> --%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="/ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="/ext/js/JDate.js"></script>
<script type="text/javascript" src="/ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="/jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="/jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "/report/report.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
}

// Grid Row one Clieck Event 처리
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
}


// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+ document.form.USER_ID.value;
		argu += "&USER_NM="	+encodeURIComponent(document.form.USER_NM.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		argu += "&MediaType=" + RTrim(document.form.MediaType.value);
		argu += "&DEPT_CD=" + RTrim(document.form.DEPT_CD.value);
		argu += "&PnPClass=" + document.form.PnPClass.value;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectMedia&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doExcelDown()
{
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");

	var argu  = "&USER_ID="	+ document.form.USER_ID.value;
	argu += "&USER_NM="	+encodeURIComponent(document.form.USER_NM.value);
	argu += "&FROM_YMD=" + document.form.fromDate.value;	
	argu += "&TO_YMD=" + document.form.toDate.value;
	argu += "&DEPT_CD=" + RTrim(document.form.DEPT_CD.value);
	argu += "&MediaType=" + RTrim(document.form.MediaType.value);
	argu += "&PnPClass=" + RTrim(document.form.PnPClass.value);
		
	fileDownload(G_SERVLETURL+"?mod=excelMedia&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);		
}

function doDeptPop() {
	popupDeptTree("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant", "OkPopupDept");
}

function OkPopupDept(dept_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}

function init() {
	// document.form.DOC_TITLE.focus();
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	
	setFormDraw();
	
	// doQuery();
}
</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<%@ include file="/mg/common/milestone.jsp"%>

        <table class="board-search">
        <colgroup>
            <col width="10%" />
            <col width="15%" />
            <col width="15%" />
            <col width="15%" />
            <col width="10%" />
            <col width="25%" />
            <col width="80px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1020", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="USER_ID"  size="12" class="text" onchange="document.form.USER_NM.value=''">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1021", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_NM" size="12" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1046", s_user.getLocale())%></td>
            <td>
            	<input type="text" id="fromDate" value="" size="8" class="text" align="center">
				<img id="imgFromDate" src="/ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand"> ~
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id="imgToDate" src="/ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
			</td>
			<td>
				
            </td>
        </tr>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1050", s_user.getLocale())%></td>
            <td>
            	<jsp:include page="/mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0407"/>
					<jsp:param name="tagname" value="MediaType"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
        	<td class="tit"><%=mm.getMessage("COMG_1055", s_user.getLocale())%></td>
            <td>
           		<input type="text" name="PnPClass"  size="20" class="text">
            </td> 
            <td class="tit"><%=mm.getMessage("COMG_1025", s_user.getLocale())%></td>
            <td>
				<input type="text" name="DEPT_CD"  size="6"  value="" class="text" onchange="document.form.DEPT_NM.value=''">
				<a href="javascript:doDeptPop();"><img src="/ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
				<input type="text" name="DEPT_NM"  size="14" value="" class="text">
			</td> 
            <td>
                <div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=162"/>
</jsp:include>
</body>
</html>
