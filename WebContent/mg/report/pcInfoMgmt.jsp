<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   IUser s_user = (IUser) session.getAttribute("j_user");
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

   String contextPath = request.getContextPath();
%>

<html>
<head>
<title>Agent 설치현황</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>

<script>

function init() 
{
	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar1 = new dhtmlXCalendarObject({input:"toDate",button:"calendar_icon"});
	calendar1.loadUserLanguage("<%=s_user.getLocale()%>");
	calendar1.hideTime();

	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>" + "&user_id=";
	masterframe.location.href='<%=contextPath%>/mg/report/pcInfoList.jsp?eventframe=detailframe' + argu;

}

function doQuery() 
{
	masterframe.doQuery();
}

function doDeptPop() {
	popupDeptAuth("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant",'<%=s_user.getProperty("USER_ROLL").toString()%>' , "OkPopupDept");
}

function OkPopupDept(dept_cd, plant_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}
function refresh(dptCd) 
{		
	parent.refresh(dptCd);
	
}

</script>

</head>
<body onload="init();" style="overflow:hidden">
<form name="form" method="post">
<table width="100%" hright="100%">
<tr>
   	<td>
	<table width="100%" class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px" />
            <col width="100px" />
            <col width="80px" />
            <col width="100px" />
            <col width="80px" />
            <col width="100px" />
            <col width="80px" />
            <col width="100px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1020", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="USER_ID"  size="6" class="text" onchange="document.form.USER_NM.value=''">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1021", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_NM" size="10" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1047", s_user.getLocale())%></td>
            <td>			
				<jsp:include page="../common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0402"/>
					<jsp:param name="tagname" value="SEARCH_CL_CD"/>
				</jsp:include>
            </td>
        	<td class="tit"><%=mm.getMessage("COMG_1048", s_user.getLocale())%></td>
            <td>
            	<!-- <input type="checkbox" id="isDate" >  -->
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id='calendar_icon' src='../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand">
			</td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
      </table>
   </td>
</tr>
</table>
<iframe name="masterframe" width="100%" height="85%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
</form>
</body>
</html>
