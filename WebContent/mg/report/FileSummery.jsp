<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
   String formId = request.getParameter("formid");
   String grid_obj = request.getParameter("gridid");
   IUser s_user = (IUser) session.getAttribute("j_user");
   String fromDay = s_user.getProperty("FROMDAY").toString();
   String serverDay = s_user.getProperty("TODAY").toString();
   IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
   String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>
<html>
<head>

<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;
var cell1;
var cell2;
function init() {

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "2E",
	    skin: "dhx_web"
	});
	
	cell1 = layout.cells('a');
	cell2 = layout.cells('b');

	cell1.attachURL("FileSummeryList.jsp",null,  {
		eventframe: "detailframe",
		formid: "<%=formId%>",
		gridid: "<%=grid_obj%>",
		user_id: ""
	});
	
	cell2.attachURL("FileSummeryUser.jsp",null,  {
		formid: "RD_0104",
		gridid: "<%=grid_obj%>",
		DEPT_CD: "",
		USER_ID:"",
		FILEACTION: ""
	});
	
	cell1.setText("<table style='padding:0; margin0; width:98%; height:100%;'><tr><td id='caption'>"+leftMenuName+"</td></tr></table>");
	cell2.hideHeader();
}
function resize() {
	layout.setSizes();
	//cell1.getFrame().contentWindow.GridObj.setSizes();
}
</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        overflow: hidden;
    }
    
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}


</style>

</head>

<body onload="init();" onresize="resize();">
<div id="layoutObj" style="position: relative; z-index:50;"></div>
</body>
</html>