<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  String deptCd = s_user.getProperty("DEPT_CD").toString();
  String deptNm = s_user.getProperty("DEPT_NM").toString();
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<title>Agent 설치현황(마스터)</title>
<%@ include file="/ext/include/include_css.jsp"%>
<%@ include file="/ext/include/grid_common.jsp"%><%-- <%@ include file="/ext/include/su_grid_common_render.jsp" %> --%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="/ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="/ext/js/json2.js"></script>
<script type="text/javascript" src="/ext/js/JDate.js"></script>

<script JavaScript>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var G_SERVLETURL = "/report/report.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	var deptCd = GridObj.cells(rowId, GridObj.getColIndexById("DPTCD")).getValue();
	parent.refresh(deptCd);
}


function setFormReDraw() 
{
	var d = new Date("2014-02-02");
		alert(d.getDate());
	var curDT = replace(parent.document.form.toDate.value,"-","");
  	var gHeader = "조직명";
  	for(var i=6; i >= 0; i--) {
  		if(parent.document.form.SEARCH_CL_CD.value == "M") {
  			gHeader += ","  + getYearMonth(curDT ,-i) + " 월";
  		} else {
  			
  			gHeader += ","  + getAddDate(curDT, -i) + " 일";
  		}
  	}
  	gHeader += ",합계,코드";
  	GridObj = setGridReDraw(GridObj, gHeader);
  	GridObj.setSizes();
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var user_id = parent.document.form.USER_ID.value;
	var user_nm = parent.document.form.USER_NM.value;
	
	setFormReDraw();
	
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+user_id;
		argu += "&USER_NM="+encodeURIComponent(user_nm);
		//argu += "&formDt="	+parent.document.form.fromDate.value;
		argu += "&toDt="	+parent.document.form.toDate.value;
		if(parent.document.form.isDate.checked) {
			argu += "&isDate=Y";
		} else {
			argu += "&isDate=N";
		}
		argu += "&SEARCH_CL_CD="	+parent.document.form.SEARCH_CL_CD.value;	
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectPcInfo&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	// if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doExcelDown()
{	
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");
	
	var argu  = "&USER_ID="	+user_id;
	argu += "&USER_NM="+encodeURIComponent(user_nm);
	argu += "&toDt="	+parent.document.form.toDate.value;
	if(parent.document.form.isDate.checked) {
		argu += "&isDate=Y";
	} else {
		argu += "&isDate=N";
	}
	argu += "&SEARCH_CL_CD="	+parent.document.form.SEARCH_CL_CD.value;	
		
	fileDownload(G_SERVLETURL+"?mod=excelPcInfo&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);	
}


function init() {
	setFormDraw();
	//doAjaxCall();
	
	doPcInfo();
}

// 최종 접속일을 구한다.
function doPcInfo() {
	var url = "/report/report.do?mod=getPcAccessDt";
	sendRequest(doAjaxResult, "", 'POST', url , false, false);
}

function doAjaxResult(oj) {
	var jData = JSON.parse(oj.responseText);
	parent.document.form.toDate.value = jData.INSTALLTIME;	
	document.getElementById("totalCntTD").innerHTML = "설치 건수 합계 <span class='point'> : " + jData.TOT_CNT+"</span> 건 ";
	
	doQuery();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
</script>

</head>
<body onload="init(); loadCombobox();">
<form name="form" method="post">
<input type="file" name= "browseFile" style="display='none';">  
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	                <a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=30"/>
</jsp:include>
</body>
</html>
