<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  String deptCd = s_user.getProperty("DEPT_CD").toString();
  String deptNm = s_user.getProperty("DEPT_NM").toString();
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>부서별정책설정현황</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common.jsp"%> --%>	<%@ include file="../../ext/include/su_grid_common.jsp" %><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>

<script type="text/javascript">
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/report/report.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	var deptCd = GridObj.cells(rowId, GridObj.getColIndexById("DptCd")).getValue();
	var authCd = GridObj.cells(rowId, GridObj.getColIndexById("AuthCode")).getValue();
	refresh(deptCd, authCd);
}
function doOnRowSelect(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	var deptCd = GridObj.cells(rowId, GridObj.getColIndexById("DptCd")).getValue();
	var authCd = GridObj.cells(rowId, GridObj.getColIndexById("AuthCode")).getValue();
	refresh(deptCd, authCd);
}
// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var user_id = document.form.USER_ID.value;
	var user_nm = document.form.USER_NM.value;
	
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+user_id;
		argu += "&USER_NM="+encodeURIComponent(user_nm);
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectAuth&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	// if(status == "false") alert(msg);
	refresh("", "");
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doExcelDown()
{
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");

	var user_id = document.form.USER_ID.value;
	var user_nm = document.form.USER_NM.value;
	
	var argu  = "&USER_ID="	+user_id;
		argu += "&USER_NM="+encodeURIComponent(user_nm);
		
	fileDownload(G_SERVLETURL+"?mod=excelAuth&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);		
}

// 정책적용 데이터 수집
function doExeProc()
{		
	fileDownload(G_SERVLETURL+"?mod=execRD_USERAUTH");		
}

function init() {
	setFormDraw();
	doQuery();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}
/////////////////////////////////////
function refresh(dptCd, authCd) 
{	
	var argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&dptCd=" + dptCd + "&authCd=" +authCd;
	parent.cell2.getFrame().contentWindow.location.href='<%=contextPath%>/mg/report/AuthUserList.jsp?' + argu;
}

function doDeptPop() {
	popupDeptAuth("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant",'<%=s_user.getProperty("USER_ROLL").toString()%>' , "OkPopupDept");
}

function OkPopupDept(dept_cd, plant_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}
</script>

</head>
<body onload="init(); loadCombobox();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%" hright="100%">
<tr>
   	<td>
        <table width="100%" class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px" />
            <col width="100px" />
            <col width="80px" />
            <col width="150px" />
            <col width="80px" />
            <col width="200px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1020", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="USER_ID"  size="6" class="text" onchange="document.form.USER_NM.value=''">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1021", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_NM" size="10" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1047", s_user.getLocale())%></td>
            <td>			
				<jsp:include page="../common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0406"/>
					<jsp:param name="tagname" value="AUTH_S_CD"/>
				</jsp:include>
            </td>
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
      </table>
   </td>
</tr>
</table>
<input type="file" name= "browseFile" style="display:'none';">  
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doExeProc();" class="btn"><span><%=mm.getMessage("COMG_1056")%></span></a>
	                <a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" style="background-color:white; width:100%; height:70%;"></div>
</body>
</html>
