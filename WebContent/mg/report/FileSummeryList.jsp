<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String fromDay = s_user.getProperty("FROMDAY").toString();
  String serverDay = s_user.getProperty("TODAY").toString();
  
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>파일 사용 현황</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common.jsp"%> --%>	<%@ include file="../../ext/include/su_grid_common.jsp" %><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="/jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="/jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="/jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
</script>
<script type="text/javascript">
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var sel_change		= "";

var calendar1;
var calendar2;

var G_SERVLETURL = "<%=contextPath%>/report/report.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	var deptCd = GridObj.cells(rowId, GridObj.getColIndexById("DPTCD")).getValue();
	var userId = GridObj.cells(rowId, GridObj.getColIndexById("USERCD")).getValue();
	var FileAction = GridObj.cells(rowId, GridObj.getColIndexById("FILEACTION")).getValue();
	refresh(deptCd, userId, FileAction);
}
function doOnRowSelect(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	var deptCd = GridObj.cells(rowId, GridObj.getColIndexById("DPTCD")).getValue();
	var userId = GridObj.cells(rowId, GridObj.getColIndexById("USERCD")).getValue();
	var FileAction = GridObj.cells(rowId, GridObj.getColIndexById("FILEACTION")).getValue();
	refresh(deptCd, userId, FileAction);
}
// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var user_id = document.form.USER_ID.value;
	var user_nm = document.form.USER_NM.value;
	
	var grid_col_id = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+user_id;
		 argu += "&USER_NM="+encodeURIComponent(user_nm);
		 argu += "&FROM_YMD="+ document.form.fromDate.value;
		 argu += "&TO_YMD="+ document.form.toDate.value;
		 argu += "&FilePath="+ encodeURIComponent(document.form.FilePath.value);
		 argu += "&FileAction="+ encodeURIComponent(RTrim(document.form.FileAction.value));
		 argu += "&DEPT_CD="+ document.form.DEPT_CD.value;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectSummeryList&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	// if(status == "false") alert(msg);
	refresh("", "");
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doExcelDown()
{
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");

	var user_id = document.form.USER_ID.value;
	var user_nm = document.form.USER_NM.value;
	
	var argu  = "&USER_ID="	+user_id;
	 argu += "&USER_NM="+encodeURIComponent(user_nm);
	 argu += "&FROM_YMD="+ document.form.fromDate.value;
	 argu += "&TO_YMD="+ document.form.toDate.value;
	 argu += "&FilePath="+ document.form.FilePath.value;
	 argu += "&FileAction="+ RTrim(document.form.FileAction.value);
	 argu += "&DEPT_CD="+ document.form.DEPT_CD.value;
		
	fileDownload(G_SERVLETURL+"?mod=excelSummeryList&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);		
}

// 정책적용 데이터 수집
function doExeProc()
{		
	fileDownload(G_SERVLETURL+"?mod=execlSummeryList");		
}

function init() {
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	
	//한글로 달력에 표시하기 위해서는 아래와 같은 데이터들이 필요로한다.
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
	calendar1 = new dhtmlXCalendarObject({input:"fromDate",button:"calendar_icon"});
	calendar1.loadUserLanguage("ko");
	calendar2 = new dhtmlXCalendarObject({input:"toDate", button:"calendar_icon2"});
	calendar2.loadUserLanguage("ko");
	
	calendar1.hideTime();
	calendar2.hideTime();
	
	setFormDraw();
	doQuery();
}

function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
}

//////////////
function refresh(dptCd, userId, fileAction) 
{	
	var argu = "&formid=RD_0104&gridid=<%=grid_obj%>&DEPT_CD=" + dptCd + "&USER_ID=" + userId + "&FILEACTION=" + fileAction;
	parent.cell2.getFrame().contentWindow.location.href='<%=contextPath%>/mg/report/FileSummeryUser.jsp?' + argu;
}

function doDeptPop() {
	popupDeptTree("<%=mm.getMessage("COMG_1025", s_user.getLocale())%>", '', '', 440, 400, "plant", "OkPopupDept");
}

function OkPopupDept(dept_cd, dept_nm) {
	document.form.DEPT_CD.value = dept_cd;
	document.form.DEPT_NM.value = dept_nm;	
}

</script>

</head>
<body onload="init(); loadCombobox();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%" hright="100%">
<tr>
   	<td>
        <table class="board-search" style="table-layout:fixed">
        <colgroup>
            <col width="80px" />
            <col width="150px" />
            <col width="80px" />
            <col width="150px" />
            <col width="80px" />
            <col width="230px" />
            <col/>
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1020", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="USER_ID"  size="12" class="text" onchange="document.form.USER_NM.value=''">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1021", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_NM" size="12" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1046", s_user.getLocale())%></td>
            <td>
            	<input type="text" id="fromDate" value="" size="8" class="text" align="center">
				<img id='calendar_icon' src='../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"> ~
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id='calendar_icon2' src='../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand"> 
			</td>
			<td>
            </td>
        </tr>
        <tr>
        	<td class="tit"><%=mm.getMessage("COMG_1052", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="FilePath"  size="20" class="text">
            </td>
        	<td class="tit"><%=mm.getMessage("COMG_1050", s_user.getLocale())%></td>
            <td>
            	<jsp:include page="../common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0403"/>
					<jsp:param name="tagname" value="FileAction"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
            <td class="tit"><%=mm.getMessage("COMG_1025", s_user.getLocale())%></td>
            <td>
				<input type="text" name="DEPT_CD"  size="6"  value="" class="text" onchange="document.form.DEPT_NM.value=''">
				<a href="javascript:doDeptPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="조회" class="mgl-5" /></a>
				<input type="text" name="DEPT_NM"  size="14" value="" class="text">
			</td>  
            <td>
            	<div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>          	
                </div>
            </td>
        </tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<input type="file" name= "browseFile" style="display:none;">  
		<table width="100%">
		<tr>
   			<td>
			<table width="100%" border="0">
        		<colgroup>
            	<col width="40%" />
            	<col width="60%" />
        		</colgroup>
        	<tr>
        		<td><div class="title_num" id="totalCntTD"></div></td>
            	<td>
	            	<div class="btnarea">
	               		<a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a>
	            	</div>
            	</td>
        	</tr>
        	</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" style="background-color:white; width:100%; height:68%;"></div>
</body>
</html>
