<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="java.util.Locale"%>
<% 
	String managerName = (String)request.getParameter("manager");
	//XSS(Cross Site Scripting) 막기, 이 페이지는 필터를 거치지 않기때문에 따로 처리
	managerName = managerName.replaceAll("<","&lt;");
	managerName = managerName.replaceAll(">","&gt;");
	
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String j_language = request.getParameter("j_language");
	Locale locale = null;

	if(j_language != null) {
		if(j_language.equals("ko")) {
			locale = Locale.KOREA;
		} else if(j_language.equals("zh")) {
			locale = Locale.CHINA;
		} else if(j_language.equals("ja")) {
			locale = Locale.JAPAN;
		} else {
			locale = Locale.US;
		}	
	} else {
		locale = Locale.KOREA;
		j_language = "ko";
	}
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../ext/include/include_css.jsp"%>
<%@ include file="../ext/include/cpCom_grid_common_render.jsp"%>
<title>담당자 검색</title>
<script>
var GridObj 		= {};
var G_SERVLETURL = "/mg/cppp/joiningUser.do";
var managerName = "<%= managerName%>";

function init() {
	
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
		opener.document.getElementById("managerId").value =GridObj.cells(rId,0).getValue();
		opener.document.getElementById("manager").value=GridObj.cells(rId,1).getValue();
		closeWin();		
	});
	document.getElementById("managerNm").value = managerName;
	if(managerName!=""){
		doQuery();
	}
}
function doQuery() {
	var managerNm = document.getElementById("managerNm").value;
	var grid_col_id  = "<%=grid_col_id%>";
	var argu = "&USER_NM="+encodeURIComponent(managerNm);
	GridObj.loadXML(G_SERVLETURL+"?mod=searchManagerNoTree&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false); 
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}
function resize() {
	GridObj.setSizes();
	
}
function closeWin(){
	self.close();
}

</script>
</head>
<body onload="init();" onresize="resize();" class="cpFindIdPassWrap">
<input type="hidden" id="deptCd" value=""/>
<input type="hidden" id="pDeptCd" value=""/>
<p class="cpFindIdPassTitle"><%=mm.getMessage("WTMG_1008", locale)%></p>
<p class="cpSrchComTxt"><%=mm.getMessage("CPCOM_1161", locale)%></p>
<table class="cpSrchComSearch">
	<tr>	
		<td>
			<%=mm.getMessage("CPCOM_1164", locale)%> <input type="text" class="p_text" id="managerNm"  onkeypress="if(event.keyCode==13) {doQuery();}">
			<a href="javascript:doQuery();"><img class="search" src="../ext/images/<%=css%>/icon/i_search.png"></a>
		</td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid"></div>
<table class="cpEnrollComBtn">
	<tr>
		<td>
			<div class="btnarea9">
				<a class="btn" href="javascript:closeWin();"><span><%=mm.getMessage("CPCOM_1104", locale)%></span></a>
			</div>
		</td>
	</tr>
</table>
</body>
</html>