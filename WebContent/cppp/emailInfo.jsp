<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>    
<%
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=mm.getMessage("WTMG_1002")%></title>
<link rel="stylesheet" type="text/css" href="../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.0.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<!-- // Window Popup start -->
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("CPCOM_1214")%></span>
        <a href="javascript:window.close();"><img src="../ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="body">
        <div class="content3">
		<%=mm.getMessage("CPCOM_1215")%><br />
		<%=mm.getMessage("CPCOM_1216")%>
		<p class="cpEmailInfoTxt"><%=mm.getMessage("CPCOM_1217")%></p>
        </div>
        <div class="clear"> </div>
    </div>
</div>
<!-- // Window Popup end -->
</body>
</html>
