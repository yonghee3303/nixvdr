<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="java.util.Locale"%>
<%
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String staffId;
Locale locale = null;

if(null != session.getAttribute("j_user")){
	IUser s_user = (IUser) session.getAttribute("j_user");
	staffId = s_user.getId();
	locale = s_user.getLocale();
}else{
	staffId = "";
	String j_language = request.getParameter("j_language");
	if(j_language != null) {
		if(j_language.equals("ko")) {
			locale = Locale.KOREA;
		} else if(j_language.equals("zh")) {
			locale = Locale.CHINA;
		} else if(j_language.equals("ja")) {
			locale = Locale.JAPAN;
		} else {
			locale = Locale.US;
		}	
	} else {
		locale = Locale.KOREA;
		j_language = "ko";
	}
}
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<%@ include file="../ext/include/include_css.jsp"%>
<%@ include file="../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../ext/include/su_grid_common_render.jsp"%>
<title><%=mm.getMessage("WTMG_1004", locale)%></title>
<script>
var G_SERVLETURL = "/mg/cppp/joiningUser.do";
var valid = false;
var chkRegNo = true;

function enrollCompany(){
	
	var companyNo = document.getElementById("regNo").value.replace(/-/g,"");
	var companyNm = document.getElementById("companyNm").value;
	var ceoNm = document.getElementById("ceo").value;
	var companyTel = document.getElementById("companyTelNo").value;
	var address = document.getElementById("addr").value;
	if(chkTelNumber()){
		alert("<%=mm.getMessage("CPCOM_1009", locale)%>");
		return;
	}
	if(!valid){
		alert("<%=mm.getMessage("CPCOM_1010", locale)%>");
		return;
	}
	if(companyNm == ""){
		alert("<%=mm.getMessage("CPCOM_1011", locale)%>");
		return;
	}
	if(ceoNm ==""){
		alert("<%=mm.getMessage("CPCOM_1012", locale)%>");
		return;
	}
	if(companyTel == ""){
		alert("<%=mm.getMessage("CPCOM_1013", locale)%>");
		return;
	}
	if(address==""){
		alert("<%=mm.getMessage("CPCOM_1014", locale)%>");
		return;
	}
	
	var url =G_SERVLETURL +"?mod=insertCompany&REG_NO="+encodeURIComponent(companyNo)
	+"&COMPANY_NM="+encodeURIComponent(companyNm)+"&CEO_NM="+encodeURIComponent(ceoNm)
	+"&COMPANY_TELNO="+encodeURIComponent(companyTel)
	+"&ADDR="+encodeURIComponent(address)+"&STAFF_ID="+"<%=staffId%>";
	
	sendRequest(enrollCompanyEnd, " " ,"POST", url , false, false);
}
function enrollCompanyEnd(oj){
	if(oj.responseText == 'insertCompany Success') {
		alert('<%=mm.getMessage("CPCOM_1015", locale)%>');
		opener.doQuery();
	} else {
		alert(oj.responseText);
	}
	closeWin();
}
function chkTelNumber(){
	var companyTel = document.getElementById("companyTelNo").value.replace(/-/g,"");
	var mobile = new Array('010','011','016','018','017','019');
	
	for(var i=0; i<mobile.length; i++){
		if(mobile[i] == companyTel.substring(0,3)) return true;
	}	
	return false;
}
function closeWin(){
	self.close();
}
function chkDuplicationRegNo(){
	//해외 사업체 경우 사업자 등록번호 유효성 검사를 생략함
	if(chkRegNo == true) {
		if(!checkValidRegNo()){
			return;
		}	
	}
	var id = document.getElementById("regNo").value.replace(/-/g,"");
	
	var url =G_SERVLETURL +"?mod=checkRegNo&REG_NO="+id;
	sendRequest(chkDuplicationEnd, " " ,"POST", url , false, false); 
}
function chkDuplicationEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);
	
	if(jData.chkValue=="false"){
		valid=false;
		alert("<%=mm.getMessage("CPCOM_1016", locale)%>");
		return;
	}else{
		valid=true;
		alert("<%=mm.getMessage("CPCOM_1017", locale)%>");
	}
}
function checkValidRegNo(){
	var number = document.getElementById("regNo").value.replace(/-/g,"");
	var sumMod = 0;
	if(number.length!=10){
		alert("<%=mm.getMessage("CPCOM_1018", locale)%>");
		return false;
	}
	sumMod += parseInt(number.substring(0,1));
	sumMod += parseInt(number.substring(1,2))* 3 % 10;
	sumMod += parseInt(number.substring(2,3))* 7 % 10;
	sumMod += parseInt(number.substring(3,4))* 1 % 10;
	sumMod += parseInt(number.substring(4,5))* 3 % 10;
	sumMod += parseInt(number.substring(5,6))* 7 % 10;
	sumMod += parseInt(number.substring(6,7))* 1 % 10;
	sumMod += parseInt(number.substring(7,8))* 3 % 10;
	sumMod += Math.floor(parseInt(number.substring(8,9)) * 5 /10);
	sumMod += parseInt(number.substring(8,9))* 5 % 10;
	sumMod += parseInt(number.substring(9,10));
	
	if(sumMod % 10!=0)
	{
		alert("<%=mm.getMessage("CPCOM_1019", locale)%>");
		valid=false;
		return false;
	}else{
		return true;
	}
}

function formatStr(){
	var number = document.getElementById("regNo").value.replace(/-/g,"");	
	var result="";
	
	if(number.length==10){
		result= number.substring(0,3) + "-" + number.substring(3,5) + "-" + number.substring(5,10);
		document.getElementById("regNo").value = result;
	}
}

function chgRegNo(checked) {
	if(checked == true) {
		valid = false;
		chkRegNo = true;
	} else {
		chkRegNo = false;
	}
	
}
</script>
</head>
<body onresize="resize();" class="cpFindIdPassWrap">
<p class="cpFindIdPassTitle"><%=mm.getMessage("WTMG_1004", locale)%></p>
<table class="cpEnrollComBoard">
	<%if("Y".equals(pm.getString("component.ui.company.country"))) {%>
	<tr>
		<th></th>
		<td>
			<input type="radio" name="country" id="ko" value="ko" onclick="chgRegNo(true)" checked/><label for="ko"><%=mm.getMessage("CPCOM_1168", locale)%></label>
			<input type="radio" name="country" id="etc" value="etc" onclick="chgRegNo(false)"/><label for="etc"><%=mm.getMessage("CPCOM_1169", locale)%></label>
		</td>
	</tr>
	<%}%>
	<tr>
		<th><%=mm.getMessage("CPMG_1170", locale)%></th>
		<td><input type="text" class="p_text" id="regNo" onkeyup="formatStr()"/>
			<div class="btnarea10">
				<a class="btn" href="javascript:chkDuplicationRegNo();"><span><%=mm.getMessage("SUMG_1151", locale)%></span></a>
			</div>
		</td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1112", locale)%></th>
		<td><input type="text" class="p_text"  id="companyNm"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPMG_1171", locale)%></th>
		<td><input type="text" class="p_text" id="ceo"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1114", locale)%></th>
		<td><input type="text" class="p_text" id="companyTelNo"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1130", locale)%></th>
		<td><input type="text" class="p_text" id="addr" onkeypress="if(event.keyCode==13) {enrollCompany();}"></td>
	</tr>
</table>
<table class="cpEnrollComBtn">
	<tr>
		<td>
			<div class="btnarea10" >
				<a class="btn" href="javascript:closeWin();"><span><%=mm.getMessage("COMG_1023", locale)%></span></a>
				<a class="btn" href="javascript:enrollCompany();"><span><%=mm.getMessage("SUMG_1169", locale)%></span></a>
			</div>
		</td>
	</tr>
</table>
</body>
</html>