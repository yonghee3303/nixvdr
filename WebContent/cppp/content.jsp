<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IUser s_user = (IUser) session.getAttribute("j_user");
String userRoll = s_user.getProperty("USER_ROLL").toString(); //S07 PP사용자
String contentId = (String)request.getParameter("BOARD_NO");
String contentCd = (String)request.getParameter("BOARD_CLS_CD");
int contentNo = Integer.parseInt(contentId.substring(1));
String leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String mileStoneName_ = mnu.getMenu("CPCOM_02", s_user).getName(s_user.getLocale());
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>
var boardNo ="<%=contentId%>";
var boardCd ="<%=contentCd%>";
var G_SERVLETURL = "/mg/cppp/board.do";

function init() {
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
}
function doQuery(){

	var arg = "&BOARD_NO="+ boardNo;
	arg += "&BOARD_CLS_CD="+boardCd;
	var url = G_SERVLETURL + "?mod=selectBoardContent"+arg; 
	sendRequest(doQueryEnd, " " ,"POST", url , false, false);
}

function doQueryEnd(oj) {
	
	 var jData = JSON.parse(oj.responseText);
	
	document.getElementById("reg").innerText=jData.CHG_ID;
	document.getElementById("regDt").innerText=jData.CHG_DT;
	document.getElementById("title").innerText=jData.BTITLE;
	document.getElementById("content").innerHTML="<div style='overflow:auto; height:100%;'>"+jData.BCONTENT+"</div>";
	
	var fileList = jData.FILE_LIST.split("|");
	var tempString ="";
	for(var i =0; i< fileList.length; i++){
		tempString=tempString+"<p><a style='text-decoration:underline;color: blue;' href=\"javascript:fileDown('"+fileList[i].replace("'", "`") +"');\">"+fileList[i]+"</a></p>";
	}
	document.getElementById("addFiles").innerHTML=tempString;
}
function fileDown(fileName){

	 var SERVLETURL = "/mg/Upload.do?mod=fileDownload";	
	 var argu = "&PGM_ID=boardList";
 	 	 argu += "&DOC_ID=" + boardNo;
     	 argu += "&FILE_NAME=" + encodeURIComponent(fileName.replace("`", "'"));
	 fileDownload(SERVLETURL + argu);
	
}
function goList(){
	if(boardCd == "N"){
		parent.mainFrame.location.href ='notice.jsp?formid=CPCOM_0201&gridid=GridObj&leftMenuName='+encodeURIComponent("공지사항")+"&mileStoneName=<%=mileStoneName_%>";
	}else if(boardCd == "P"){
		parent.mainFrame.location.href ='FAQ.jsp?formid=CPCOM_0202&gridid=GridObj&leftMenuName='+encodeURIComponent("FAQ")+"&mileStoneName=<%=mileStoneName_%>";
	}
}
function conModify(){
	parent.mainFrame.location.href ='htmlEditor.jsp?BOARD_CLS_CD='+boardCd+'&BOARD_NO='+boardNo;
}
function conDelete() {
	if(confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) {
		var arg = "&BOARD_NO="+ boardNo;
		arg += "&BOARD_CLS_CD="+boardCd;
		var url = G_SERVLETURL + "?mod=deleteBoardContent"+arg;
		sendRequest(conDeleteEnd, "", 'POST', url , false, false);	
	}
}
function conDeleteEnd(oj) {
	if(oj.responseText == "success") {
		goList();
	} else {
		alert(oj.responseText);
	}
}
</script>
<style>
body {margin:0; padding:0px; height:800px; text-align:left; }
</style>

</head>
<body onload="init();" class="cpBoardWrap">
<%@ include file="/mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<!-- <tr>
		<td>설명 텍스트 입력</td>
	</tr> -->
</table>
</div>
<table class="cpContentView">
	<tr>
		<th><%=mm.getMessage("CPCOM_1116", s_user.getLocale())%></th>
		<td id="reg"></td>
		<th><%=mm.getMessage("CPCOM_1117", s_user.getLocale())%></th>
		<td id="regDt"></td>
		<th><%=mm.getMessage("CPCOM_1118", s_user.getLocale())%></th>
		<td id="readCnt"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1119", s_user.getLocale())%></th>
		<td colspan="3" id="title"></td>
		<th><%=mm.getMessage("CPMG_1109", s_user.getLocale())%></th>
		<td id="docNo">NO. <%=contentNo%></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1120", s_user.getLocale())%></th>
		<td colspan="5" id="addFiles"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1121", s_user.getLocale())%></th>
		<td colspan="5" id="content"></td>
	</tr>
</table>
<table class="cpBtnArea">
	<tr>
		<td>
			<div>
			<%if("S01".equals(userRoll)){ %>
			<div class="btnarea9"><a class="btn ml5" href="javascript:conDelete();"><span><%=mm.getMessage("COMG_1013", s_user.getLocale())%></span></a></div>
			<div class="btnarea9"><a class="btn" href="javascript:conModify();"><span><%=mm.getMessage("CPCOM_1122", s_user.getLocale())%></span></a></div>
			<%} %>
			<div class="btnarea6"><a class="btn" href="javascript:goList();"><span><%=mm.getMessage("CPCOM_1123", s_user.getLocale())%></span></a></div>
			</div>
		</td>
	</tr>
</table>
</body>
</html>