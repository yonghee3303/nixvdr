<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName_;
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	
	IMenu menu = mnu.getMenu("CPCOM_0102", s_user);
	leftMenuName_ = menu.getName(s_user.getLocale());
}
String contextPath = request.getContextPath();

IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String flag = request.getParameter("flag");
%>

<html>
<head>
<%@ include file="../ext/include/include_css.jsp"%>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../ext/include/su_grid_common_render.jsp"%>	

<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/JDate.js"></script>
<script type="text/javascript" src="../ext/js/doAjaxCombo.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>


var GridObj 		= {};
var G_SERVLETURL ="/cp/approval.do";

function init() {
	
	setFormDraw();
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 

}
function setFormDraw(){
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onXLE",doQueryEnd);
	GridObj.attachEvent("onRowSelect",doOnRowSelect);
	GridObj.setSizes();
}

function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	if( header_name == "FILE_ICON" ) {
	
		var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		 var state = GridObj.cells(rowId, GridObj.getColIndexById("FILE_ICON")).getValue();
		 var docNm = GridObj.cells(rowId, GridObj.getColIndexById("DOC_TITLE")).getValue();
		 if(docNm.lastIndexOf(".cpd") != -1 || docNm.lastIndexOf(".exe") != -1)	docNm = docNm.substring(0,docNm.lastIndexOf("."));
		 var schVaild = GridObj.cells(rowId, GridObj.getColIndexById("REG_DT")).getValue();
		 var num = state.lastIndexOf("/"); 
		 var image = state.substr(num+1); //이미지 파일명만 비교
		
		 if(image == "upload_un.png"){
			 if(schVaild !='-'){
				alert("<%=mm.getMessage("CPCOM_1042", s_user.getLocale())%>");
				return;
			 }else{
				 alert("<%=mm.getMessage("CPCOM_1043", s_user.getLocale())%>");
			 }
		 } 
		
		 popupAttachFile("File Upload", '', '', 440, 420, "repack", docId, docNm, "repacking",<%=pm.getString("component.contextPath.root")%>);
	}
}
function doQuery() {
	var grid_col_id  = "<%=grid_col_id%>";
	var argu = "&USER_ID="+"<%=s_user.getId()%>";
	var flag = "<%=flag%>"; 
	if(flag != "" && flag != "null") {
		argu += "&FLAG="+flag
	}
	GridObj.loadXML(G_SERVLETURL+"?mod=selectRepackingList&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

function doQueryEnd(GridObj, RowCnt) {
	
	var listCount = GridObj.getRowsNum();
	document.getElementById("listCount").innerHTML = "<b>"+listCount+"</b>";
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}
function doOnCellChange(stage,rowId,cellInd) 
{
	 var max_value = GridObj.cells(rowId, cellInd).getValue();

	if(stage==0) {
		sel_change = GridObj.cells(rowId, cellInd).getValue();
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}
function resize() {
	GridObj.setSizes();
	
}
</script>
</head>
<body onload="init();" onresize="resize();">
<!-- <div id="body_wrap" style="height:100%;"> -->
<%-- <h1 style="text-align: left; font-size:25px;">▶ <%=leftMenuName %></h1> --%>
<%@ include file="/mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<tr>
		<td><%=mm.getMessage("CPCOM_1160", s_user.getLocale())%></td>
	</tr>
</table>
</div>
<table class="cpSubTxt">
	<tr>
		<td><%=mm.getMessage("COMG_1014",s_user.getLocale())%> : <font id="listCount"></font>건</td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid"></div>
</body>
</html>