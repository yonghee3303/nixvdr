<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="java.util.Locale"%>

<% 
	String companyName = (String)request.getParameter("company");
    //XSS(Cross Site Scripting) 막기, 이 페이지는 필터를 거치지 않기때문에 따로 처리
	companyName = companyName.replaceAll("<","&lt;");
	companyName = companyName.replaceAll(">","&gt;");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String j_language = request.getParameter("j_language");
	Locale locale = null;

	if(j_language != null) {
		if(j_language.equals("ko")) {
			locale = Locale.KOREA;
		} else if(j_language.equals("zh")) {
			locale = Locale.CHINA;
		} else if(j_language.equals("ja")) {
			locale = Locale.JAPAN;
		} else {
			locale = Locale.US;
		}	
	} else {
		locale = Locale.KOREA;
		j_language = "ko";
	}
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../ext/include/include_css.jsp"%>
<%@ include file="../ext/include/cpCom_grid_common_render.jsp"%>
<script>
var GridObj 		= {};
var G_SERVLETURL = "/mg/cppp/joiningUser.do";
var companyName = "<%= companyName%>";

function init() {
	
	GridObj = setGridDraw(GridObj);
	
	GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
		opener.document.getElementById("companyId").value =GridObj.cells(rId,0).getValue();
		opener.document.getElementById("company").value=GridObj.cells(rId,2).getValue();
		closeWin();		
	});
	
	document.getElementById("con").value = companyName;
	doQuery();
}
function doQuery() {
	
	var selectBox = document.getElementById("searchTerm");
	var term="";
	for(var i=0; i<selectBox.options.length; i++){
		if(selectBox.options[i].selected ==true){
			term = selectBox.options[i].value;
		}
	}
	var argu;
	var mod="searchCompany";
	var con = encodeURIComponent(document.getElementById("con").value);
	
	switch(term){
		case "compNm":
			argu="&COMPANY_NM="+con;
		break;
		
		case "compNo":
			argu="&REG_NO="+con;
		break;
		
		case "ceoNm":
			argu="&CEO_NM="+con;
		break;
		
		default:
			argu="&COMPANY_NM="+con+"&REG_NO="+con+"&CEO_NM="+con;
			mod="searchAllCompany";
		break;
	}

	var grid_col_id     = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod="+mod+"&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
	
}
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}
function resize() {
	GridObj.setSizes();
	
}
function enrollCompany(){
	var lang = "<%=j_language%>";
	var winL = (screen.width-100-550)/2; 
	var winT = (screen.height-100-340)/2; 
	var winOpt = 'width=550px, height=340px, left='+winL+', top='+winT+',scrollbars=yes,resizable=no';
	window.open("/cppp/enrollCompany.jsp?j_language="+lang,'',winOpt);
	//window.open("/cppp/enrollCompany.jsp",'','width=550px,height=340px,scrollbars=yes,resizable=no');
}
function closeWin(){
	self.close();
}
</script>
<title><%=mm.getMessage("WTMG_1007", locale)%></title>
</head>
<body onload="init();" onresize="resize();" class="cpFindIdPassWrap">
<p class="cpFindIdPassTitle"><%=mm.getMessage("WTMG_1007", locale)%></p>
<p class="cpSrchComTxt"><%=mm.getMessage("CPCOM_1161", locale)%></p>
<table class="cpSrchComSearch">
	<tr>
		<td colspan="2">
			<select id="searchTerm" class="searchTerm">
				<option value="all"><%=mm.getMessage("COMG_1032", locale)%></option>
				<option value="compNm"><%=mm.getMessage("COMG_1030", locale)%></option>
				<option value="compNo"><%=mm.getMessage("CPMG_1170", locale)%></option>
				<option value="ceoNm"><%=mm.getMessage("CPMG_1171", locale)%></option>
			</select>
			<input type="text" class="p_text" id="con" onkeypress="if(event.keyCode==13) {doQuery();}"/>
			<a href="javascript:doQuery();"><img class="search" src="../ext/images/<%=css%>/icon/i_search.png"></a>
		</td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid"></div>
<table class="cpSrchComBtn">
	<tr>
		<th>
			<font><%=mm.getMessage("CPCOM_1162", locale)%></font>
		</th>
		<td>
			<div class="btnarea9">
				<a class="btn" href="javascript:enrollCompany();"><span><%=mm.getMessage("WTMG_1004", locale)%></span></a>
			</div>
		</td>
	</tr>
</table>
</body>
</html>