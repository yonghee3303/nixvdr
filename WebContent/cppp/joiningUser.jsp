<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="java.util.Locale"%>
<% 
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String contextPath = request.getContextPath(); 
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String MacUseYn = pm.getString("component.ui.macAddr.useYn");
	String companyHiding = pm.getString("component.ui.company.hiding");  
	String j_language = request.getParameter("j_language");
	Locale locale = null;

	if(j_language != null) {
		if(j_language.equals("ko")) {
			locale = Locale.KOREA;
		} else if(j_language.equals("zh")) {
			locale = Locale.CHINA;
		} else if(j_language.equals("ja")) {
			locale = Locale.JAPAN;
		} else {
			locale = Locale.US;
		}	
	} else {
		locale = Locale.KOREA;
		j_language = "ko";
	}
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script>

var G_SERVLETURL = "/mg/cppp/joiningUser.do";
var valid=false;
var lang = "<%=j_language%>";

function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
function searchCompany(){
	if("<%=companyHiding%>" == "Y") {
		var company = document.getElementById("company");
		if(company.value.length < 2) {
			alert("<%=mm.getMessage("CPCOM_1050", locale)%>");
			company.focus();
			return;
		}
	}
	centerWin("/cppp/searchCompany.jsp?company="+encodeURIComponent(document.getElementById("company").value)+"&formid=CPCOM_0401&gridid=GridObj&j_language="+lang,482,625,"yes");
}
function searchManager(){
	centerWin("/cppp/searchManager_noTree.jsp?manager="+encodeURIComponent(document.getElementById("manager").value)+"&formid=CPCOM_0402&gridid=GridObj&j_language="+lang,400,470,"yes");
}
function checkMail(strEmail){
	
	 var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	
	 if(!strEmail.match(regExp)){
		return false;
	}else{
		return true;
	}
}
function chkDuplicationEmail(){
	var email = document.getElementById("email").value;
	
	if(!checkMail(email)){
		alert("<%=mm.getMessage("CPMG_1169", locale)%>");
		return;
	} 
	
	var url =G_SERVLETURL +"?mod=checkId&USER_ID="+email;
	sendRequest(chkDuplicationEnd, " " ,"POST", url , false, false); 
}
function chkDuplicationEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);

//	alert(jData.chkValue);
	
	if(jData.chkValue=="false"){
		valid = false;
		alert("<%=mm.getMessage("CPCOM_1026", locale)%>");
		return;
	}else{
		valid = true;
		alert("<%=mm.getMessage("CPCOM_1027", locale)%>");
	}
}

function checkValidPass(pw){ //a1!은 true가 됨
 var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
 var number = "1234567890";
 var sChar = "-_=+\|()*&^%$#@!~`?></;,.:'";
 
 var sChar_Count = 0;
 var alphaCheck = false;
 var numberCheck = false;

 if(8 <= pw.length && pw.length <= 16){
  for(var i=0; i<pw.length; i++){
   if(sChar.indexOf(pw.charAt(i)) != -1){
    sChar_Count++;
   }
   if(alpha.indexOf(pw.charAt(i)) != -1){
    alphaCheck = true;
   }
   if(number.indexOf(pw.charAt(i)) != -1){
    numberCheck = true;
   }
  }//for
 
   	if(alphaCheck==true && numberCheck==true && sChar_Count > 0){ //영문,숫자,특수문자 조합일 경우 8자이상
   	
		return true;
	}else if(alphaCheck == true && numberCheck==true &&  pw.length>=10){ //영문,숫자 조합일 경우 10자이상
	
		return true;
  	}else if(alphaCheck == true && sChar_Count > 1 && pw.length>=10){ //영문,특수 문자 조합일 경우 10자이상
  	
		return true;  
  	}else if(numberCheck==true && sChar_Count > 1 && pw.length>=10){ //숫자,특수문자 조합일 경우 10자이상
  	
  		return true;
  	}else{
  	   	return false;
  	}
 }else{
  return false;
 }

}  

function checkPass(){
	var passwd = document.getElementById("PASSWD").value;
	var passwd_check = document.getElementById("PASSWD_CONFIRM").value;
	if(passwd_check==passwd && checkValidPass(passwd)){
		document.getElementById("agree").style.visibility ="visible";
	}else{
		document.getElementById("agree").style.visibility ="hidden";
	} 
} 
function initForm(){
	document.getElementById("PASSWD").value="";
	document.getElementById("PASSWD_CONFIRM").value="";
	document.getElementById("company").value="";
	document.getElementById("manager").value="";
	document.getElementById("email").value="";
	document.getElementById("name").value="";
	document.getElementById("position").value="";
	document.getElementById("telNum").value="";
	document.getElementById("macAddr").value=""
}
function joining(){
	
	var email = document.getElementById("email").value;
	var name = document.getElementById("name").value;
	var passwd = document.getElementById("PASSWD").value;
	var passwd_check = document.getElementById("PASSWD_CONFIRM").value;
	var company = document.getElementById("company").value;
	var companyId = document.getElementById("companyId").value;

	var position = document.getElementById("position").value;
	var telNum = document.getElementById("telNum").value;
	telNum = telNum.replace(/-/gi, '');
	
	var manager = document.getElementById("manager").value;
	var managerId = document.getElementById("managerId").value;
	var macAddr = document.getElementById("macAddr").value.replace(/-/g,"");

	//비밀번호 유효성 검사
	if(passwd ==""){
		alert("<%=mm.getMessage("CPCOM_1028", locale)%>");	
		return;
	}else if(passwd_check != passwd){
		alert("<%=mm.getMessage("CPCOM_1029", locale)%>");
		return;
	}else if(!checkValidPass(passwd)){ //비밀번호가 8~16자 영문,숫자 1자 이상,특수문자 1자 이상으로 조합되었는지 확인하는 부분
		alert("<%=mm.getMessage("CPCOM_1005", locale)%>");
		return;
	}
	//담당자 선택 검사
	if(managerId == ""){
		alert("<%=mm.getMessage("CPCOM_1030", locale)%>");
		return;
	}
	//업체 선택 검사
	if(companyId==""){
		alert("<%=mm.getMessage("CPCOM_1031", locale)%>");
		return;
	}
	//성염을 입력했는지 검사	
	if(name==""){
		alert("<%=mm.getMessage("CPCOM_1032", locale)%>");
		return;
	}
	//전화번호를 입력했는지 검사
	if(telNum ==""){
		alert("<%=mm.getMessage("CPCOM_1033", locale)%>");
		return;
	}
	//다시한번 중복체크/e-mail형식 체크
	if(!valid){
		alert("<%=mm.getMessage("CPCOM_1034", locale)%>");	
		return;
	}
	//맥주소 형식 검사
	if(macAddr.length != 0) {
		if("Y" == "<%=MacUseYn%>" && !validMac(macAddr)){
			alert("<%=mm.getMessage("CPCOM_1046", locale)%>");
			document.getElementById("macAddr").focus();
			return;
		}else if("N" =="<%=MacUseYn%>"){
			macAddr="";		
		}	
	}
	
	var url =G_SERVLETURL +"?mod=insertCompUser&USER_ID="+encodeURIComponent(email)
			+"&USER_NM="+encodeURIComponent(name)+"&PASSWD="+encodeURIComponent(passwd)+"&COMPANY_NM="+encodeURIComponent(company)
			+"&COMPANY_ID="+encodeURIComponent(companyId)+"&POSITION_NM="+encodeURIComponent(position)
			+"&TEL_NO="+telNum+"&STAFF_ID="+managerId+"&STAFF_NM="+encodeURIComponent(manager)+"&MAC_ADDR="+macAddr.toUpperCase()
			+"&j_language="+lang;
	sendRequest(joiningEnd, " " ,"POST", url , false, false);
}
function validMac(mac){

	 if(mac.length !=12){
		return false;
	 }
	 document.getElementById("macAddr").value = mac.toUpperCase();
	 return true;
}
function joiningEnd(){
	alert("<%=mm.getMessage("CPCOM_1035", locale)%>");
	self.close();
}
function init(){
	if("Y" == "<%=MacUseYn%>"){
		document.getElementById("macAddrTR").style.display ="block";
		document.getElementById("macExplanationTR").style.display ="block";
	}
}

//DB 저장시 글자수 제한을 위한 함수, Ascii 코드를 넘어가는 값은 모두 3byte 로 처리한다.
//element : elementById
//limit : DB 컬럼내 제한량
function len_chk(element, limit) {
	
	if('Y' == '<%=pm.getString("component.ui.input.limit")%>') {
		
		var frm = document.getElementById(element);
		var cnt = 0; 
		
		for(var i=0; i<frm.value.length; i++) {
			var chr = frm.value.substring(i,i+1);
			var numUnicode = chr.charCodeAt(0);	//number of the decimal Unicode
			
			if(numUnicode >= 0 && numUnicode <= 127) {
				cnt += 1;	
			} else {
				cnt += 3;
			}
			if(cnt > limit) {
				alert('<%=mm.getMessage("CPMG_1054", locale)%>');
				frm.value = frm.value.substring(0,i);
				frm.focus();
				break;
			}
		}
	}
}

</script>
<%@ include file="../ext/include/include_css.jsp"%>
<style>

TD{
	font-size:14px;
}
</style>
</head>
<body class="cpJoinWrap" onload="init()">
<div id="body_wrap">
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=mm.getMessage("WTMG_1006", locale)%></th>
	</tr>
</table>
</div>
<div class="cpJoinBoardWrap">
<table class="cpJoinBoard">
	<tr><!-- 회사명 -->
		<th>* <%=mm.getMessage("COMG_1030", locale)%></th>
		<td>
			<input type="text" id="company" class="p_text" size ="30" onkeypress="if(event.keyCode==13) {searchCompany();}">
			<input type="hidden" id="companyId" class="p_text" value=""/>
			<span class="cpJoinBtn"><a href="javascript:searchCompany();"><img class="search" src="../ext/images/<%=css%>/icon/i_search.png"></a></span>
		</td>
	</tr>
	<tr><!-- 메일 주소 -->
		<th>* <%=mm.getMessage("CPCOM_1143", locale)%></th>
		<td>
			<input type="text" class="p_text" id="email" size ="30" onkeypress="if(event.keyCode==13) {chkDuplicationEmail();}">
			<div class="btnarea8"><a class="btn" href="javascript:chkDuplicationEmail();"><span><%=mm.getMessage("CPCOM_1153", locale)%></span></a></div>
		</td>
	</tr>
	<tr>
		<th></th>
		<td class="cpJoinTxt"><%=mm.getMessage("CPCOM_1036", locale)%></td>
	</tr>	
	<tr><!-- 성명 -->
		<th>* <%=mm.getMessage("COMG_1021", locale)%></th>
		<td><input type="text" class="p_text" id="name" size ="30" onKeyup="len_chk('name', 256);"></td>
	</tr>
	<tr><!-- 직위 -->
		<th><%=mm.getMessage("CPMG_1176", locale)%></th>
		<td><input type="text" class="p_text" id="position" size ="30" onKeyup="len_chk('position', 50);"></td>
	</tr>
	<tr><!-- 전화번호 -->
		<th>* <%=mm.getMessage("CPCOM_1145", locale)%></th>
		<td><input type="text" class="p_text" id="telNum" size ="30" onKeyup="len_chk('telNum', 14);"></td>
	</tr>
	<tr>
		<th></th>
		<td class="cpJoinTxt"><%=mm.getMessage("CPCOM_1037", locale)%></td>
	</tr>
	<tr id="macAddrTR" style="display:none;"><!-- Mac Address -->
		<th><%=mm.getMessage("CPCOM_1045", locale)%></th>
		<td><input type="text" class="p_text" id="macAddr" size="30"></td>
	</tr>
	<tr id="macExplanationTR" style="display:none;">
		<th></th>
		<td class="cpJoinTxt"> <%=mm.getMessage("CPCOM_1047", locale)%> <br/> <%=mm.getMessage("CPCOM_1048", locale)%></td>
	</tr>
	<tr><!-- 담당자 선택 -->
		<th>* <%=mm.getMessage("CPCOM_1154", locale)%></th>
		<td>
			<input type="text" class="p_text" id="manager" size ="30" onkeypress="if(event.keyCode==13) {searchManager();}">
			<input type="hidden" class="p_text" id="managerId" value=""/>
			<span><a href="javascript:searchManager();"><img class="search" src="../ext/images/<%=css%>/icon/i_search.png"></a></span>
		</td>
	</tr>
	<tr>
		<th></th>
		<td class="cpJoinTxt"><%=mm.getMessage("CPCOM_1038", locale)%></td>
	</tr>
	<tr><!-- 비밀번호 -->
		<th>* <%=mm.getMessage("CPCOM_1155", locale)%></th>
		<td><input type="password" class="p_text" id="PASSWD" size ="31" onKeyup="len_chk('PASSWD', 128);"></td>
	</tr>
	<tr>
		<th></th>
		<td class="cpJoinTxt"><%=mm.getMessage("CPCOM_1039", locale)%></td>
	</tr>
	<tr><!-- 비밀번호 확인 -->
		<th>* <%=mm.getMessage("CPCOM_1109", locale)%></th>
		<td><input type="password" class="p_text" id="PASSWD_CONFIRM" size ="31" onkeyup="checkPass();" onkeypress="if(event.keyCode==13) {joining();}"/>
		<span><img src="../ext/images/agree.jpg" id="agree" class="hiddenV" /></span>
		</td>
	</tr>
	</table>
</div>
<table class="cpBtnArea">
	<tr>
		<td>
			<div>
				<div class="btnarea9"><a class="btn" href="javascript:joining();"><span><%=mm.getMessage("CPCOM_1156", locale)%></span></a></div>
				<div class="btnarea6"><a class="btn" href="javascript:initForm();"><span><%=mm.getMessage("COMG_1065", locale)%></span></a></div>
	        </div>
	   	</td>
	</tr>
</table>	
</div>
</body>
</html>