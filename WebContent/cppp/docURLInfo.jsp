<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<% 
	String downloadPath = (String)request.getParameter("path"); 
	String docId = (String)request.getParameter("DOC_NO");
	IUser s_user = (IUser) session.getAttribute("j_user");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

%>
<html>
<head>
<%@ include file="../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var G_SERVLETURL = "/cp/approval.do";
var downloadPath = "<%= downloadPath%>";
var docId = <%=docId%>;

function init() {
	document.getElementById("path").innerText = downloadPath+"DOC_NO="+docId; 
}
function closeWin(){
	self.close();
}
</script>
<style>
tr{
	padding :10px;
}
</style>
</head>
<body onload="init();" onresize="resize();" style="overflow: hidden; padding:20px;">
<table width="100%">
	<colgroup>
		<col width="25%"/>
		<col width="25%"/>
		<col width="20%"/>
		<col width="30%"/>
	</colgroup>
	<tr>
		<td colspan="4"><font id="path" style="color:#002060; font-size:10pt; font-weight:bold;"></font></td>
	</tr>
	<tr>
		<td colspan="4"><%=mm.getMessage("CPCOM_1007", s_user.getLocale())%></td>
	</tr>
	<tr>
		<td colspan="4"><%=mm.getMessage("CPCOM_1008", s_user.getLocale())%></td>
	</tr>
	<tr>
		<td colspan="4">
			<div class="btnarea10">
				<a class="btn" href="javascript:closeWin();"><span><%=mm.getMessage("SUMG_1151", s_user.getLocale())%></span></a>
			</div>
		</td>
	</tr>
</table>
</body>
</html>