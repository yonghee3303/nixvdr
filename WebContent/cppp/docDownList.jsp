<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName_;
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	
	IMenu menu = mnu.getMenu("CPCOM_0101", s_user);
	leftMenuName_ = menu.getName(s_user.getLocale());
}

String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path + "/";

IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/include_css.jsp"%>
<%@ include file="../ext/include/portal_grid_common.jsp"%>	

<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/JDate.js"></script>
<script type="text/javascript" src="../ext/js/doAjaxCombo.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var doc_id;
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "/cp/approval.do";

function init() {
	setFormDraw();
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
}
//Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
//이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	GridObj = setGridDraw(GridObj);
	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

//아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	
}

function docDownload(){
	
	 var docId = doc_id;
	 var SERVLETURL = G_SERVLETURL + "?mod=down&PGM_ID=regMgmt";	
	 var argu = "&DOC_NO=" + docId;
	 fileDownload(SERVLETURL + argu);
	 
}
//Grid Row one Click Event 처리
function doOnRowSelect(rowId, cellInd) {
	row_id = rowId;
	
	doc_id = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
	centerWin("/cppp/docDetailInfo.jsp?docId="+doc_id+"&user_id=<%=s_user.getId() %>",482,242);
}

//파라메터의 문자열을 Clipboard로 복사
function copyToClipboard(s)
{
 if( window.clipboardData && clipboardData.setData )
 {
     clipboardData.setData("Text", s);
 }
 else
 {
     // You have to sign the code to enable this or allow the action in about:config by changing
     user_pref("signed.applets.codebase_principal_support", true);
     netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

     var clip = Components.classes['@mozilla.org/widget/clipboard;[[[[1]]]]'].createInstance(Components.interfaces.nsIClipboard);
     if (!clip) return;

     // create a transferable
     var trans = Components.classes['@mozilla.org/widget/transferable;[[[[1]]]]'].createInstance(Components.interfaces.nsITransferable);
     if (!trans) return;

     // specify the data we wish to handle. Plaintext in this case.
     trans.addDataFlavor('text/unicode');

     // To get the data from the transferable we need two new objects
     var str = new Object();
     var len = new Object();

     var str = Components.classes["@mozilla.org/supports-string;[[[[1]]]]"].createInstance(Components.interfaces.nsISupportsString);

     var copytext=meintext;

     str.data=copytext;

     trans.setTransferData("text/unicode",str,copytext.length*[[[[2]]]]);

     var clipid=Components.interfaces.nsIClipboard;

     if (!clip) return false;

     clip.setData(trans,null,clipid.kGlobalClipboard);      
 }
}

function OkFileUpload(fileList) {
	
}

function OkPopupDeptGrid(dept_cd, plant_cd, dept_nm) {
	
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//데이터 조회시점에 호출되는 함수입니다.
//조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
//그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&APPROVER_ID="	+ "<%=s_user.getId() %>";
		
	GridObj.loadXML(G_SERVLETURL+"?mod=exeDownList_CPCOM&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

//doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
//GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
//setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
	
	var listCount = GridObj.getRowsNum();
	document.getElementById("listCount").innerHTML = "<b>"+listCount+"</b>";
	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
	var max_value = GridObj.cells(rowId, cellInd).getValue();

	if(stage==0) {
		sel_change = GridObj.cells(rowId, cellInd).getValue();
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doSave(approvalYn) {
	if(!checkRows()) return;	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}


function doDelete() {
	if(!checkRows()) return;
}
function copyURL(docNo){
	 var SERVLETURL = G_SERVLETURL + "?mod=down&PGM_ID=regMgmt";	
	 var argu = "&DOC_NO=" + docNo;
	 var downloadPath = "<%=basePath%>"+SERVLETURL+argu;
	copyToClipboard(downloadPath);
	
	centerWin("/cppp/docURLInfo.jsp?path="+downloadPath,482,200);
}
function centerWin(url, w, h){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2;  
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars=no, resizable=no, toolbar=no, menubar=no status=no';
	window.open(url,'',winOpt);
}
</script>
</head>
<body onload="init();">
<%@ include file="/mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<tr>
		<td><%=mm.getMessage("CPCOM_1129", s_user.getLocale())%></td>
	</tr>
</table>
</div>
<table class="cpSubTxt">
	<tr>
		<td><%=mm.getMessage("COMG_1014",s_user.getLocale())%> : <font id="listCount"></font>건</td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid"></div>
</body>
</html>