<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<% 
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	IUser s_user = (IUser) session.getAttribute("j_user");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String docId = (String)request.getParameter("docId"); 
	String userId = (String)request.getParameter("user_id"); 
%>
<html>
<head>
<%-- <%@ include file="../ext/include/include_css.jsp"%> --%>
<link rel="stylesheet" type="text/css" href="../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var G_SERVLETURL = "/cp/approval.do";
var docId = "<%= docId%>";
var userId="<%=userId%>";
function init() {
	detailDocInfo(docId);
}
function detailDocInfo(doc){
	
	var arg = "&DOC_NO=" + doc+"&USER_ID="+userId; 
	var url = G_SERVLETURL + "?mod=selectDetailDocInfo"+arg;
	sendRequest(detailDocInfoEnd, " " ,"POST", url , false, false);
}
function detailDocInfoEnd(oj){
	var jData = JSON.parse(oj.responseText);
	document.getElementById("sender").innerText = jData.USER_NM; 
	document.getElementById("sendDt").innerText = jData.APPROVE_YMD; 
	document.getElementById("fileNum").innerText = jData.FILE_NUM;
	document.getElementById("readCnt").innerText = jData.READ_COUNT; 

	if(jData.PERIOD == "false"){
		document.getElementById("period").innerText=jData.APPROVE_YMD+" ~ ";
	}else{
		document.getElementById("period").innerText=jData.APPROVE_YMD+" ~ "+jData.PERIOD;
	}
}
function closeWin(){
	self.close();
}
function downloadCall(){
	opener.docDownload();
	closeWin();
}
function copyURLCall(){
	opener.copyURL(docId);
	closeWin();
}
</script>
</head>
<body onload="init();" onresize="resize();" style="overflow: auto;">
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("CPCOM_1124", s_user.getLocale())%></span>
        <a href="javascript:window.close();"><img src="../ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="body">
        <div class="content2">
            <!-- Inquiry Table start -->
            <div class="tableInquiry3">
                <table class="tableInquiry3_tb1">
                <caption>조회</caption>
                <colgroup>
                <col class="col_15p" />
                <col />
                <col class="col_17p" />
                <col />
                </colgroup>
                <tbody>
                <tr>
                    <th><%=mm.getMessage("CPMG_1173", s_user.getLocale())%> :</th>
                    <td id="sender"></td>
                    <th><%=mm.getMessage("CPCOM_1125", s_user.getLocale())%> :</th>
                    <td id="sendDt"></td>
                </tr>
                <tr>
                    <th><%=mm.getMessage("CPCOM_1126", s_user.getLocale())%> :</th>
                    <td id="fileNum"></td>
                    <th></th>
                    <td></td>
                </tr>
                </tbody>
                </table>
                <table class="tableInquiry3_tb2">
                <caption><%=mm.getMessage("CPMG_1164", s_user.getLocale())%></caption>
                <colgroup>
                <col class="col_30p" />
                <col />
                </colgroup>
                <tbody>
                <tr>
                    <th><%=mm.getMessage("CPCOM_1127", s_user.getLocale())%> :</th>
                    <td id="period"></td>
                </tr>
                <tr>
                    <th><%=mm.getMessage("CPCOM_1128", s_user.getLocale())%> :</th>
                    <td id="readCnt">10</td>
                </tr>
                </tbody>
                </table>
            </div>
            <!--// Inquiry Table end -->
        </div>
    </div>
</div>
<div class="buttonArea mt8 mr20">
    <ul class="btn_crud">
        <li><a class="darken" href="#" onclick="downloadCall()"><span><%=mm.getMessage("CPMG_1201", s_user.getLocale())%></span></a></li>
       <%--  <%if() %>
        <li><a class="darken" href="#" onclick="copyURLCall()"><span>파일링크 복사</span></a></li> --%>
        <li><a href="#" onclick="closeWin()"><span><%=mm.getMessage("COMG_1023", s_user.getLocale())%></span></a></li>
    </ul>
</div>
</body>
</html>