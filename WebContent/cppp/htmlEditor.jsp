<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%
	IUser s_user = (IUser) session.getAttribute("j_user");
	String userNm = s_user.getName();
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String boardCd = request.getParameter("BOARD_CLS_CD");
	String contentId = (String)request.getParameter("BOARD_NO");
	String serverDay = s_user.getProperty("TODAY").toString();
	String leftMenuName="";
	if(boardCd.equals("B") || boardCd.equals("N")){
		leftMenuName ="공지사항";
	}else{
		leftMenuName="FAQ";
	}
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css"); 
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script type="text/javascript" src="../ext/js/doAjaxCombo.js"></script>
<script type="text/javascript" src="../ext/SmartEditor/js/HuskyEZCreator.js" charset="utf-8"></script>
</head>
<body onload="init();">
<form name="form" action="sample.php" method="post">
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName%></th>
	</tr>
	<!-- <tr>
		<td>설명 텍스트 입력</td>
	</tr> -->
</table>
</div>
<table class="cpContentView">
	<tr>
		<th><%=mm.getMessage("CPCOM_1116", s_user.getLocale())%></th>
		<td id="reg"><%=userNm %></td>
		<th><%=mm.getMessage("CPCOM_1117", s_user.getLocale())%></th>
		<td id="regDt"><%=serverDay %></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1119", s_user.getLocale())%></th>
		<td colspan="3">
			<input type="text" class="p_text" id="b_title" value="" size ="50"/>
			<input type="hidden" id="docNo" value=""/>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<textarea name="ir1" id="ir1" rows="10" cols="100"></textarea>
		</td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1120", s_user.getLocale())%></th>
		<td>
			<input type="text" class="p_text" name="FILE_LIST" size="100" readonly="readonly" style="IME-MODE:disabled">
		</td>
		<td colspan="2">
			<div class="btnarea8"><a href="javascript:doUpload();" class="btn"><span>Upload</span></a></div>
		</td>
	</tr>
</table>	
<table class="cpBtnArea">
	<tr>
		<td>
			<div>
				<div class="btnarea9"><a href="javascript:submitContents();" class="btn"><span><%=mm.getMessage("CPCOM_1146", s_user.getLocale())%></span></a></div>
				<div class="btnarea6"><a href="javascript:initContent();" class="btn"><span><%=mm.getMessage("COMG_1065", s_user.getLocale())%></span></a></div>
	        </div>
	   	</td>
	</tr>
</table>
<!-- 	<p>
		<input type="button" onclick="pasteHTML();" value="본문에 내용 넣기" />
		<input type="button" onclick="showHTML();" value="본문 내용 가져오기" />
		<input type="button" onclick="submitContents(this);" value="서버로 내용 전송" />
		<input type="button" onclick="setDefaultFont();" value="기본 폰트 지정하기 " />
	</p> -->
</form>
<script type="text/javascript">
var oEditors = [];
var board="<%=boardCd%>";
var boardNo="<%=contentId%>";
var G_SERVLETURL = "/mg/cppp/board.do";
var modify;
//추가 글꼴 목록
//var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];

nhn.husky.EZCreator.createInIFrame({
	oAppRef: oEditors,
	elPlaceHolder: "ir1",
	sSkinURI: "../ext/SmartEditor/SmartEditor2Skin.html",	
	htParams : {
		bUseToolbar : true,				// 툴바 사용 여부 (true:ì¬ì©/ false:ì¬ì©íì§ ìì)
		bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:ì¬ì©/ false:ì¬ì©íì§ ìì)
		bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) ì¬ì© ì¬ë¶ (true:ì¬ì©/ false:ì¬ì©íì§ ìì)
		//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
		fOnBeforeUnload : function(){
			//alert("완료");
		}
	}, //boolean
	fOnAppLoad : function(){
		//예제 코드
		//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
	},
	fCreator: "createSEditor2"
});

function initContent(){
	oEditors.getById["ir1"].exec("SET_IR", ['']); // 에디터 내용 초기화
}
function pasteHTML() {
	var sHTML = "<span style='color:#FF0000;'>이미지도 같은 방식으로 삽입합니다.<\/span>";
	oEditors.getById["ir1"].exec("PASTE_HTML", [sHTML]); //에디터 내용 추가
}

function showHTML() {
	var sHTML = oEditors.getById["ir1"].getIR(); // 에디터 내용 얻기
	alert(sHTML);
}
function formatBoardNo(str){
	var a = 4-str.length;
	var tempBoard = board;
	for(var i=0; i <a; i++){
		tempBoard=tempBoard.concat("0");
	}
	return tempBoard+str;
}	
function submitContents() {
	
	oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);

	var content = oEditors.getById["ir1"].getIR();//document.getElementById("ir1").value;
	var title = document.getElementById("b_title").value;
	
	if(content==""){
		alert("<%=mm.getMessage("CPCOM_1023", s_user.getLocale())%>");
		return;
	}
	if(title==""){
		alert("<%=mm.getMessage("CPCOM_1024", s_user.getLocale())%>");
		return;
	}
	//text만을 뽑아내기 위한 작업
 	oEditors.getById["ir1"].exec("CHANGE_EDITING_MODE", ["TEXT"]);
	oEditors.getById["ir1"].exec('MSG_NOTIFY_CLICKCR', ['textmode']);
	var text = oEditors.getById["ir1"].getIR(); 
	
	var mod;
	
	if(modify){
		mod="updateBoard";
	}else{
		mod="insertBoard";
	}
	
	var url =G_SERVLETURL +"?mod="+mod+"&BCONTENT="+encodeURIComponent(content)+"&BTEXT="+encodeURIComponent(text) //html태그를 뺀 택스트만 저장.
	+"&BTITLE="+encodeURIComponent(title)+"&BOARD_CLS_CD="+board+"&REG_ID="+"<%=s_user.getId() %>"+"&BOARD_NO="+boardNo;

	sendRequest(submitContentsEnd, " " ,"POST", url , false, false);
}
function submitContentsEnd(){
	alert("<%=mm.getMessage("CPCOM_1025", s_user.getLocale())%>");
	if(board == "N"){
		parent.mainFrame.location.href ='notice.jsp?formid=CPCOM_0201&gridid=GridObj&leftMenuName='+encodeURIComponent("<%=mm.getMessage("CPCOM_1147", s_user.getLocale())%>");
	}else if(board == "P"){
		parent.mainFrame.location.href ='FAQ.jsp?formid=CPCOM_0202&gridid=GridObj&leftMenuName='+encodeURIComponent("<%=mm.getMessage("CPCOM_1148", s_user.getLocale())%>");
	}
}
function setDefaultFont() {
	var sDefaultFont = '궁서';
	var nFontSize = 24;
	oEditors.getById["ir1"].setDefaultFont(sDefaultFont, nFontSize);
}

function doQuery(){

	var url = G_SERVLETURL + "?mod=getNewBoardNo&BOARD_CLS_CD="+board;
	sendRequest(doQueryEnd, " " ,"POST", url , false, false); 
}

function doQueryEnd(oj) {

	var jData = JSON.parse(oj.responseText);	
	document.getElementById("docNo").value=jData.DocNo;
	boardNo=formatBoardNo(jData.DocNo);
	loadAttachFile();
	
}
function doConQuery(){
	document.getElementById("docNo").value=boardNo;
	var arg = "&BOARD_NO="+ boardNo;
	arg += "&BOARD_CLS_CD="+board;
	var url = G_SERVLETURL + "?mod=selectBoardContent"+arg; 
	sendRequest(doConQueryEnd, " " ,"POST", url , false, false);
}
//수정일 경우 기존의 있던 내용을 로드
function doConQueryEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);
	
	document.getElementById("b_title").value=jData.BTITLE;
	document.getElementById("ir1").innerText=jData.BCONTENT;
}
function init(){
	if(boardNo=="null"){
		doQuery();
		modify=false;
	}else{
		doConQuery();
		modify=true;
	}
	loadAttachFile();
}
function doUpload() {
	var docId = boardNo;
	//var authority = "";//document.form.AUTHORITY.value;

	popupAttachFile("File Upload", '', '', 440, 400, "boardList", docId, "", "write",<%=pm.getString("component.contextPath.root")%>);
}
function OkFileUpload(fileList) {
	document.form.FILE_LIST.value = fileList;
}
</script>

</body>
</html>