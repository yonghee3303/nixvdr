<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String authCode = s_user.getProperty("USER_ROLL").toString();
String userCompanyId="";
if("S07".equals(authCode)){
	userCompanyId = s_user.getProperty("COMPANY_ID").toString();
}
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

 IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName_;
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	
	IMenu menu = mnu.getMenu("CPCOM_0301", s_user);
	leftMenuName_ = menu.getName(s_user.getLocale());
} 
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String locale = s_user.getLocale().toString();
if(locale.equals("ko")) {
	locale = "";
} else {
	locale = "/en";
} 
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>

var GridObj 		= {};
var G_SERVLETURL = "/mg/cppp/member.do";
var passValid="false";

function init() {
	if("S07"=="<%=authCode%>"){
		doQuery(); 
	}else{
		document.getElementById("body").innerHTML="<br><br><img src='../ext/images<%=locale%>/error_Page_.jpg'/>";
	}
	<%if("Y".equals( pm.getString("component.ui.macAddr.useYn"))){%>
		document.getElementById("MacTR").style.display="block";
	<%}%>
}
function doQuery(){

	var arg = "&USER_ID=" + "<%=s_user.getId().toString() %>"; 
	var url = G_SERVLETURL + "?mod=selectMemberInfo"+arg; 
	sendRequest(doQueryEnd, " " ,"POST", url , false, false); 
}

function doQueryEnd(oj) {

	var jData = JSON.parse(oj.responseText);
	
	document.getElementById("userId").innerText=jData.USER_ID;
	document.getElementById("userNm").innerText=jData.USER_NM;
	document.getElementById("comNm").innerText=jData.COMPANY_NM;
	document.getElementById("position").innerText=jData.POSITION_NM;
	document.getElementById("telNum").value=jData.TEL_NO;
	document.getElementById("macAddr").value=jData.MAC_ADDR;
}
function goMain(){
	parent.location.href ='../mg/main/cpCom_main.jsp';
}
function checkOldPass(){
	var inputOldPass = document.getElementById("pass_old").value;
	var arg = "&USER_ID=" + "<%=s_user.getId().toString() %>"+"&PASSWD="+inputOldPass; 
	var url = G_SERVLETURL + "?mod=selectPasswdValid"+arg; 
	sendRequest(checkOldPassEnd, " " ,"POST", url , false, false); 
}
function checkOldPassEnd(oj){
	var jData = JSON.parse(oj.responseText);
	passValid = jData.RESULT; //틀리면 false, 맞으면  true를 json형태로 반환 
	modifyInfo();
}
function modifyInfo(){
	if(passValid=="false"){
		alert("<%=mm.getMessage("CPCOM_1003", s_user.getLocale())%>");
		return;
	}
	var newPass=document.getElementById("pass_new").value;
	var newCheckPass = document.getElementById("pass_new_check").value;
	if(newPass==""){
		newPass=document.getElementById("pass_old").value;
	}else{
		if(newPass!=newCheckPass){
			alert("<%=mm.getMessage("CPCOM_1004", s_user.getLocale())%>");
			return;
		}else if(!checkValidPass(newPass)){ //비밀번호가 8~15자 영문,숫자 1자 이상,특수문자 1자 이상으로 조합되었는지 확인하는 부분
			alert("<%=mm.getMessage("CPCOM_1005", s_user.getLocale())%>");
			return;
		}
	}
	var position = document.getElementById("position").innerText;
	var telNum =document.getElementById("telNum").value;
	var macAddr = document.getElementById("macAddr").value;	
	
	if(telNum ==""){
		alert("<%=mm.getMessage("CPCOM_1040", s_user.getLocale())%>");
		return;
	}
	<%if("Y".equals( pm.getString("component.ui.macAddr.useYn"))){%>
	if(macAddr ==""){
		alert("<%=mm.getMessage("CPCOM_1049", s_user.getLocale())%>");
		return;
	}
	<%}%>
	var argu = "&POSITION_NM="+encodeURIComponent(position)+"&TEL_NO="+telNum+"&PASSWD="+newPass+"&USER_ID="+"<%=s_user.getId().toString() %>"+"&MAC_ADDR="+macAddr;
	var url = G_SERVLETURL + "?mod=updateMemberInfo"+argu; 
	sendRequest(modifyInfoEnd, " " ,"POST", url , false, false); 
}
function modifyInfoEnd(){
	alert("<%=mm.getMessage("CPCOM_1041", s_user.getLocale())%>");
	parent.mainFrame.location.href ='memberInfo.jsp';
}
function checkValidPass(pw){
	 var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	 var number = "1234567890";
	 var sChar = "-_=+\|()*&^%$#@!~`?></;,.:'";
	 
	 var sChar_Count = 0;
	 var alphaCheck = false;
	 var numberCheck = false;
	  
	 if(8 <= pw.length || pw.length <= 16){
	  for(var i=0; i<pw.length; i++){
	   if(sChar.indexOf(pw.charAt(i)) != -1){
	    sChar_Count++;
	   }
	   if(alpha.indexOf(pw.charAt(i)) != -1){
	    alphaCheck = true;
	   }
	   if(number.indexOf(pw.charAt(i)) != -1){
	    numberCheck = true;
	   }
	  }//for
	 
	   	if(alphaCheck==true && numberCheck==true && sChar_Count > 0){ //영문,숫자,특수문자 조합일 경우 8자이상
			return true;
		}else if(alphaCheck == true && numberCheck==true &&  pw.length>=10){ //영문,숫자 조합일 경우 10자이상
			return true;
	  	}else if(alphaCheck == true && sChar_Count > 1 && pw.length>=10){ //영문,특수 문자 조합일 경우 10자이상
			return true;  
	  	}else if(numberCheck==true && sChar_Count > 1 && pw.length>=10){ //숫자,특수문자 조합일 경우 10자이상
	  		return true;
	  	}else{
	  	   	return false;
	  	}
	 }else{
	  return false;
	 }
} 
function checkPass(){
	if(document.getElementById("pass_new").value==document.getElementById("pass_new_check").value){
		document.getElementById("img_pass").style.visibility ="visible";
	}else{
		document.getElementById("img_pass").style.visibility ="hidden";
	} 
} 
</script>
</head>
<body onload="init();" id="body" class="cpBoardWrap">
<%@ include file="../mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<!-- <tr>
		<td>설명 텍스트 입력</td>
	</tr> -->
</table>
</div>
<p class="cpMemberTitle"><%=mm.getMessage("CPCOM_1157", s_user.getLocale())%></p>
<table class="cpMemberBoard">
	<tr>
		<th><%=mm.getMessage("CPCOM_1143", s_user.getLocale())%></th>
		<td id="userId"></td>
		<th><%=mm.getMessage("CPCOM_1144", s_user.getLocale())%></th>
		<td id="userNm"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1112", s_user.getLocale())%></th>
		<td id="comNm"></td>
		<th><%=mm.getMessage("CPMG_1176", s_user.getLocale())%></th>
		<td id="position"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1145", s_user.getLocale())%></th>
		<td colspan="3"><input type="text" class="p_text" id="telNum"></td>
	</tr>
	<tr id="MacTR" style="display:none;">
		<th><%=mm.getMessage("CPCOM_1045", s_user.getLocale())%></th>
		<td colspan="3"><input type="text" class="p_text" id="macAddr"></td>
	</tr>
</table>
<p class="cpMemberTitle"><%=mm.getMessage("CPCOM_1158", s_user.getLocale())%></p>
<table class="cpMemberBoard">
	<tr>
		<th><%=mm.getMessage("CPCOM_1106", s_user.getLocale())%></th>
		<td colspan="3"><input type="password" class="p_text" id="pass_old"> &nbsp;&nbsp;&nbsp;&nbsp;<%=mm.getMessage("CPCOM_1051", s_user.getLocale())%></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1107", s_user.getLocale())%></th>
		<td><input type="password" class="p_text" id="pass_new"></td>
		<th><%=mm.getMessage("CPCOM_1109", s_user.getLocale())%></th>
		<td><input type="password" class="p_text" id="pass_new_check" onkeyup="checkPass()" onkeypress="if(event.keyCode==13) {checkOldPass();}"><img src="../ext/images/agree.jpg" id="img_pass" class="hiddenV" /></td>
	</tr>
</table>
<table class="cpBtnArea">
	<tr>
		<td>
			<div>
				<div class="btnarea9"><a class="btn" href="javascript:checkOldPass();"><span><%=mm.getMessage("CPCOM_1122", s_user.getLocale())%></span></a></div>
				<div class="btnarea6"><a class="btn" href="javascript:goMain();"><span><%=mm.getMessage("CPCOM_1159", s_user.getLocale())%></span></a></div>
	        </div>
	   	</td>
	</tr>
</table>
</body>
</html>