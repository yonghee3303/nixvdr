<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.message.IMessageManagement"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
IUser s_user = (IUser) session.getAttribute("j_user");
String userId = s_user.getId();
String userNm = s_user.getName();
String userLocale = s_user.getLocale().getLanguage();
String deptNm = s_user.getProperty("DEPT_NM").toString();
String contextPath = request.getContextPath();

IMenu menu = mnu.getMenu("CPCOM_0301", s_user);
String leftMenuName = menu.getName(s_user.getLocale());
IMenu mileStoneMenu = mnu.getMenu("CPCOM_03", s_user);
String mileStoneName = mileStoneMenu.getName(s_user.getLocale());
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="../../../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../../ext/include/include_css.jsp"%>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>

<script>

var G_SERVLETURL = "<%=contextPath%>"+"/cp/approval.do";

var downCnt = "";
var uploadCnt = "";
var recentLogin = "";


function init() {
	
	//document.getElementById("user_id").innerHTML = "";
	doAjax();
}

function doAjax() {
	
	var argu = "&userId=" + "<%=userId%>";
	argu += "&locale=" + "<%=userLocale%>";
	var url = G_SERVLETURL + "?mod=cntDocList" + argu;
	//alert(url + "    cp/approval.do?mod=cntDocList&userId=jcann@itmsystem.com");
	
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);

}

function doAjaxEnd(oj) {

	var jData = JSON.parse(oj.responseText);
	
	downCnt = jData.downCnt;
	uploadCnt = jData.uploadCnt;
	recentLogin = jData.recentLogin;
	
	document.getElementById("downCnt").innerHTML = downCnt;
	document.getElementById("uploadCnt").innerHTML = uploadCnt;
	document.getElementById("recentLogin").innerHTML = recentLogin;
	
}

function logout() {
	if(confirm("<%=mm.getMessage("COMG_1034", s_user.getLocale())%>")) {  // 시스템을 종료 하시겠습니까 ?
	   parent.parent.location.href = "<%=contextPath%>/Logout.do";
	}
}

function modifyInfo(){
	parent.location.href ="/cppp/memberInfo.jsp?formid=CPCOM_0301&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>";
}



function download(){
	<%
	leftMenuName = mnu.getMenu("CPCOM_0101", s_user).getName(s_user.getLocale());
	%>
	parent.location.href = "/cppp/docDownList.jsp?formid=CPCOM_0101&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>";
}

function upload(){
	<%
	leftMenuName = mnu.getMenu("CPCOM_0102", s_user).getName(s_user.getLocale());
	%>
	parent.location.href = "/cppp/repackingUpList.jsp?formid=CPCOM_0102&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>&flag=02";
}
</script>
</head>
<body onload="init();"">
<table class="cpUserName">
<tr>
	<td>		
    	<strong>[<%=deptNm %>]   <%=userNm %> <%=mm.getMessage("CPCOM_1101", s_user.getLocale())%></strong>       
	</td>
</tr>
</table>
<table class="cpUserId">
	<colgroup>
	<col class="col100px">
	<col class="col150px">
	<col />	
	</colgroup>
	<tr>
		<th><%=mm.getMessage("CPCOM_1149", s_user.getLocale())%></th>
		<td id="user_id" colspan="2"><%=userId%></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1150", s_user.getLocale())%></th>
		<td><font id="recentLogin"></font></td>
		<td>
			<div class="btnarea10">
		            <a href="javascript:modifyInfo();" class="btn"><span><%=mm.getMessage("CPCOM_1102", s_user.getLocale())%></span></a>
		    </div>
	    </td>
	</tr>
</table>
<table class="cpUserBnnr">
	<colgroup>
	<col class="col50p">
	<col class="col50p">
	</colgroup>
	<tr>
		<td>
			<span><a href="javascript:download();"><font id="downCnt"></font></a></span>
			<span><font><%=mm.getMessage("CPCOM_1151", s_user.getLocale())%></font></span>
		</td>
		<td class="bnnrRight">
			<span><a href="javascript:upload();"><font id="uploadCnt"></font></a></span>
			<span><font><%=mm.getMessage("CPCOM_1152", s_user.getLocale())%></font></span>
		</td>
	</tr>
</table>
</body>
</html>