<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>   
<%
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="../../../ext/css/cppp_portal_<%=css %>.css" />
</head>
<body>
<p><img src="../../ext/images/banners.png" alt="사이트 이용가이드" /></p>
</body>
</html>