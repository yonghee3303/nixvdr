<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>   
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String locale = s_user.getLocale().toString();
if(locale.equals("ko")) {
	locale = "";
} else if(locale.equals("en") || locale.equals("zh")) {
	locale = "/" + locale;
} else {
	locale = "/en";
} 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body style="margin:0; padding:0; overflow:hidden;">
<a href="../../ext/guide/coworkGuide.pdf" target="_blank">
	<img src="../../ext/images/<%=css %><%=locale%>/guide.gif" width="491" height="220" style="border:none;"/>
</a>
</body>
</html>