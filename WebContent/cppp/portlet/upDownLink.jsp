<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String contextPath = request.getContextPath();
String leftMenuName = "";
String mileStoneName = mnu.getMenu("CPCOM_01", s_user).getName(s_user.getLocale());
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String locale = s_user.getLocale().toString();
if(locale.equals("ko")) {
	locale = "";
} else if(locale.equals("en") || locale.equals("zh")) {
	locale = "/" + locale;
} else {
	locale = "/en";
} 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="../../../ext/css/cppp_portal_<%=css %>.css" />
<script>
function download(){
	<%
	leftMenuName = mnu.getMenu("CPCOM_0101", s_user).getName(s_user.getLocale());
	%>
	parent.location.href = "/cppp/docDownList.jsp?formid=CPCOM_0101&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>";
}

function upload(){
	<%
	leftMenuName = mnu.getMenu("CPCOM_0102", s_user).getName(s_user.getLocale());
	%>
	parent.location.href = "/cppp/repackingUpList.jsp?formid=CPCOM_0102&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>";
}

</script>
</head>
<body class="cpUpdownWrap">
<div><a href="javascript:download();"><img src="../../ext/images/<%=css%><%=locale%>/left_bigbutton.gif" /></a><a href="javascript:upload();"><img src="../../ext/images/<%=css%><%=locale%>/right_bigbutton.gif" /></a></div>
</body>
</html>