<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.user.IUser"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "/mg/cppp/member.do";
var passValid="false";

function init(){
	document.getElementById("pass_old").focus();
}
function checkOldPass(){
	var inputOldPass = document.getElementById("pass_old").value;
	var newPass=document.getElementById("pass_new").value;
	if(inputOldPass == newPass){
		alert("<%=mm.getMessage("CPCOM_1002", s_user.getLocale())%>");
		return;
	}
	var arg = "&USER_ID=" + "<%=s_user.getId().toString() %>"+"&PASSWD="+encodeURIComponent(inputOldPass); 
	var url = G_SERVLETURL + "?mod=selectPasswdValid"+arg; 
	sendRequest(checkOldPassEnd, " " ,"POST", url , false, false); 
}
function checkOldPassEnd(oj){
	var jData = JSON.parse(oj.responseText);
	passValid = jData.RESULT; //틀리면 false, 맞으면  true를 json형태로 반환 
	modifyInfo();
}
function modifyInfo(){
	if(passValid=="false"){
		alert("<%=mm.getMessage("CPCOM_1003", s_user.getLocale())%>");
		return;
	}
	var newPass=document.getElementById("pass_new").value;
	var newCheckPass = document.getElementById("pass_new_check").value;

	if(newPass!=newCheckPass){
		alert("<%=mm.getMessage("CPCOM_1004", s_user.getLocale())%>");
		return;
	}else if(!checkValidPass(newPass)){ //비밀번호가 8~15자 영문,숫자 1자 이상,특수문자 1자 이상으로 조합되었는지 확인하는 부분
		alert("<%=mm.getMessage("CPCOM_1005", s_user.getLocale())%>");
		return;
	}
	var argu = "&PASSWD="+encodeURIComponent(newPass)+"&USER_ID="+"<%=s_user.getId().toString() %>"+"&callFrom=changePass";
	var url = G_SERVLETURL + "?mod=updateMemberInfo"+argu; 
	sendRequest(modifyInfoEnd, " " ,"POST", url , false, false); 
}
function modifyInfoEnd(oj){
	var jData = JSON.parse(oj.responseText);
	
	alert("<%=mm.getMessage("CPCOM_1006", s_user.getLocale())%>");
	 location.href = "/mg/main/cpCom_main.jsp";
}
function checkValidPass(pw){
	 var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	 var number = "1234567890";
	 var sChar = "-_=+\|()*&^%$#@!~`?></;,.:'";
	 
	 var sChar_Count = 0;
	 var alphaCheck = false;
	 var numberCheck = false;
	  
	 if(8 <= pw.length && pw.length <= 16){
	  for(var i=0; i<pw.length; i++){
	   if(sChar.indexOf(pw.charAt(i)) != -1){
	    sChar_Count++;
	   }
	   if(alpha.indexOf(pw.charAt(i)) != -1){
	    alphaCheck = true;
	   }
	   if(number.indexOf(pw.charAt(i)) != -1){
	    numberCheck = true;
	   }
	  }//for
	 
	   	if(alphaCheck==true && numberCheck==true && sChar_Count > 0){ //영문,숫자,특수문자 조합일 경우 8자이상
			return true;
		}else if(alphaCheck == true && numberCheck==true &&  pw.length>=10){ //영문,숫자 조합일 경우 10자이상
			return true;
	  	}else if(alphaCheck == true && sChar_Count > 1 && pw.length>=10){ //영문,특수 문자 조합일 경우 10자이상
			return true;  
	  	}else if(numberCheck==true && sChar_Count > 1 && pw.length>=10){ //숫자,특수문자 조합일 경우 10자이상
	  		return true;
	  	}else{
	  	   	return false;
	  	}
	 }else{
	  return false;
	 }
} 
function checkPass(){
	if(document.getElementById("pass_new").value==document.getElementById("pass_new_check").value){
		document.getElementById("img_pass").style.visibility ="visible";
	}else{
		document.getElementById("img_pass").style.visibility ="hidden";
	} 
} 
</script>
</head>
<body onload="init();" class="cpChangePassWrap">
<div class="cpChangePassBox">
    <div class="cpChangePassInBox">
        <div class="cpChangePassTitle">
            <span><%=mm.getMessage("CPCOM_1105", s_user.getLocale())%></span>
        </div>
        <div class="pt15">
            <div class="cpChangePassContentBox">
                <table>
                <tr>
                    <th scope="col"><span>＊</span><%=mm.getMessage("CPCOM_1106", s_user.getLocale())%></th>
                    <td><input id="pass_old" type="password" class="cpChangePassInput1" /></td>
                </tr>
                <tr>
                    <th scope="col" class="cpChangePassLine"><span>＊</span><%=mm.getMessage("CPCOM_1107", s_user.getLocale())%></th>
                    <td class="cpChangePassLine">
                        <input id="pass_new" type="password" class="cpChangePassInput2" /><br /><span><%=mm.getMessage("CPCOM_1108", s_user.getLocale())%></span>
                    </td>
                </tr>
                <tr>
                    <th scope="col"><span>＊</span><%=mm.getMessage("CPCOM_1109", s_user.getLocale())%></th>
                    <td>
                    	<input id="pass_new_check" onkeyup="checkPass()" onkeypress="if(event.keyCode==13) {checkOldPass();}" type="password" class="cpChangePassInput3" />
                    	<img src="/ext/images/agree.jpg" id="img_pass" class="hiddenV" />
                    </td>
                </tr>
                </table>
            </div>
            <div class="cpChangePassBtn">
                <a href="javascript:checkOldPass();"><span><%=mm.getMessage("SUMG_1151", s_user.getLocale())%></span></a>
            </div>
        </div>
    </div>
</div>
</body>
</html>