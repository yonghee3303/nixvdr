<%@ page contentType = "text/html; charset=UTF-8"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="java.util.Locale"%>
<%    
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String j_language = request.getParameter("j_language");
Locale locale = null;

if(j_language != null) {
	if(j_language.equals("ko")) {
		locale = Locale.KOREA;
	} else if(j_language.equals("zh")) {
		locale = Locale.CHINA;
	} else if(j_language.equals("ja")) {
		locale = Locale.JAPAN;
	} else {
		locale = Locale.US;
	}	
} else {
	locale = Locale.KOREA;
	j_language = "ko";
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "/mg/cppp/member.do";
var val="";
function init() {
	document.getElementById("radioId").checked=true;
	clickIdPass();
}
function doQuery_Id(companyId, userNm, telNo){
	var arg = "&COMPANY_ID="+encodeURIComponent(companyId)+"&USER_NM="+encodeURIComponent(userNm)+"&TEL_NO="+telNo; 
	var url = G_SERVLETURL + "?mod=selectUserId"+arg; 
	sendRequest(doQueryEnd_Id, " " ,"POST", url , false, false); 
}

function doQueryEnd_Id(oj) {

	var jData = JSON.parse(oj.responseText);
	if(jData.RESULT=="ture"){
	document.getElementById("userId").innerText=jData.USER_ID;
	document.getElementById("resultId").style.display ="block";
	document.getElementById("resultPass").style.display ="none";
	}else{
		alert("<%=mm.getMessage("CPCOM_1044", locale)%>");
	}
}
function doQuery_Pass(companyId, userId, userNm){
	var arg = "&COMPANY_ID="+encodeURIComponent(companyId)+"&USER_NM="+encodeURIComponent(userNm)+"&USER_ID="+encodeURIComponent(userId); 
	arg += "&COMPANY_NM="+encodeURIComponent(document.getElementById("company").value);
	arg += "&j_language=<%=j_language%>";
	
	var url = G_SERVLETURL + "?mod=selectUserPasswd"+arg; 
	sendRequest(doQueryEnd_Pass, " " ,"POST", url , false, false); 	
}
function doQueryEnd_Pass(oj) {
	var jData = JSON.parse(oj.responseText);
	if(jData.RESULT=="ture"){
		document.getElementById("resultId").style.display ="none";
		document.getElementById("resultPass").style.display ="block";
	}else{
		alert("<%=mm.getMessage("CPCOM_1044", locale)%>");
	}
}
function clickIdPass(){
	var idPass =document.getElementsByName("idPass");
	val="";
	for(var i=0; i < idPass.length; i++){
        if(idPass[i].checked){
        	val = idPass[i].value;
        }
    }
	switch(val){
	case "id": //아이디 찾기
		document.getElementById("trId").style.display ="none";
		document.getElementById("resultId").style.display ="none";
		document.getElementById("resultPass").style.display ="none";
		document.getElementById("trPhone").style.display="block";
			
		break;
	case "passwd": //비밀번호 찾기
		document.getElementById("trId").style.display ="block";
		document.getElementById("resultId").style.display ="none";
		document.getElementById("resultPass").style.display ="none";
		document.getElementById("trPhone").style.display="none";
		break;
	}
}
function searchCompany(){
	var lang = "<%=j_language%>";
	var winL = (screen.width-100-482)/2; 
	var winT = (screen.height-100-625)/2; 
	var winOpt = 'width=482px,height=625px, left='+winL+', top='+winT+',scrollbars=yes,resizable=no';
	window.open("/cppp/searchCompany.jsp?company="+encodeURIComponent(document.getElementById("company").value)+"&formid=CPCOM_0401&gridid=GridObj&j_language="+lang,'',winOpt);
	//window.open("/cppp/searchCompany.jsp?company="+encodeURIComponent(document.getElementById("company").value)+"&formid=CPCOM_0401&gridid=GridObj",'','width=482px,height=625px,scrollbars=yes,resizable=no');
}
function searchIdPass(){
	var companyId = document.getElementById("companyId").value;
	var userNm =document.getElementById("userNm").value;
	var telNo = document.getElementById("telNo").value;
	telNo = telNo.replace(/-/gi, '');
	
	var userId = document.getElementById("id").value;

	switch(val){
		case "id": //아이디 찾기
			doQuery_Id(companyId, userNm, telNo);
			break;
		case "passwd": //비밀번호 찾기
			if("block" == document.getElementById("resultPass").style.display){
				alert("<%=mm.getMessage("CPCOM_1020", locale)%>");
				closeWin();
				return;
			}
			doQuery_Pass(companyId, userId, userNm);
			//doQueryEnd_Pass();
			break;
	}
}
function closeWin(){
	self.close();
}
</script>
<%@ include file="../ext/include/include_css.jsp"%>
</head>
<body onload="init();" class="cpFindIdPassWrap">
	<p class="cpFindIdPassTitle"><%=mm.getMessage("WTMG_1005", locale)%></p>
	<table class="cpFindIdPass">
	<tr>
		<td><input type="radio" id="radioId" name="idPass" value="id" onclick="clickIdPass();"/><%=mm.getMessage("CPCOM_1141", locale)%></td>
		<td><input type="radio" id="radioPass" name="idPass" value="passwd" onclick="clickIdPass();"/><%=mm.getMessage("CPCOM_1142", locale)%></td>
	</tr>
	</table>
	<table class="cpFindIdPassBoard">
	<tr>
		<th><%=mm.getMessage("CPCOM_1112", locale)%></th>
		<td>
			<input type="text" class="p_text" id="company" size ="20" onkeypress="if(event.keyCode==13) {searchCompany();}" />
			<input type="hidden" id="companyId" value=""/>
			<a href="javascript:searchCompany();"><img class="search" src="../ext/images/<%=css%>/icon/i_search.png"></a>
		</td>
	</tr>
	<tr id="trId" style="display:none;">
		<th><%=mm.getMessage("CPCOM_1143", locale)%></th>
		<td>
			<input type="text" class="p_text"id="id" size ="20" />
		</td>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1144", locale)%></th>
		<td>
			<input type="text" class="p_text"id="userNm" size ="20" onkeypress="if(event.keyCode==13) {searchIdPass();}"/>
		</td>
	</tr>
	<tr id="trPhone" style="display:none;">
		<th><%=mm.getMessage("CPCOM_1145", locale)%></th>
		<td>
			<input type="text" class="p_text"id="telNo" size ="20" onkeypress="if(event.keyCode==13) {searchIdPass();}" />
			<font><%=mm.getMessage("CPCOM_1021", locale)%></font>
		</td>
	</tr>
	<tr id="resultId" style="display:none;">
		<td colspan="3">
			<font id="userId"></font>
		</td>
	</tr>
	<tr id="resultPass" style="display:none;">
		<td colspan="3">
			<font><%=mm.getMessage("CPCOM_1022", locale)%></font>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="btnarea9">
				<a class="btn" href="javascript:searchIdPass();"><span><%=mm.getMessage("SUMG_1151", locale)%></span></a>
			</div>
			<div class="btnarea6">
				<a class="btn" href="javascript:closeWin();"><span><%=mm.getMessage("CPCOM_1104", locale)%></span></a>
			</div>
		</td>
	</tr>	
</table>
</body>
</html>