<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>        
<%
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String css = pm.getString("component.ui.portal.css");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=mm.getMessage("WTMG_1002")%></title>
<link rel="stylesheet" type="text/css" href="../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.0.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body style="overflow:auto;">
<!-- // Window Popup start -->
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("CPCOM_1218")%></span>
        <a href="javascript:window.close();"><img src="../ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="body">
        <div class="content2">
<%=mm.getMessage("CPCOM_1219")%><br/> 
<%=mm.getMessage("CPCOM_1220")%><br/><br/>

<%=mm.getMessage("CPCOM_1221")%>
<%=mm.getMessage("CPCOM_1222")%>
<%=mm.getMessage("CPCOM_1223")%>
<%=mm.getMessage("CPCOM_1224")%>
<%=mm.getMessage("CPCOM_1225")%>
<%=mm.getMessage("CPCOM_1226")%>
<%=mm.getMessage("CPCOM_1227")%>
<%=mm.getMessage("CPCOM_1228")%>
<%=mm.getMessage("CPCOM_1229")%>
<%=mm.getMessage("CPCOM_1233")%>
<%=mm.getMessage("CPCOM_1230")%>
<%=mm.getMessage("CPCOM_1231")%>
<%=mm.getMessage("CPCOM_1232")%>
        </div>
        <ul class="btn_crud close mb20">
            <li><a href="javascript:window.close();"><span><%=mm.getMessage("CPCOM_1104")%></span></a></li>
        </ul>
        <div class="clear"> </div>
    </div>
</div>
<!-- // Window Popup end -->
</body>
</html>
