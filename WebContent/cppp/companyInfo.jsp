<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String authCode = s_user.getProperty("USER_ROLL").toString();
String userCompanyId="";
if("S07".equals(authCode)){
	userCompanyId = s_user.getProperty("COMPANY_ID").toString();
}
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

 IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName_;
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	
	IMenu menu = mnu.getMenu("CPCOM_0302", s_user);
	leftMenuName_ = menu.getName(s_user.getLocale());
} 
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String locale = s_user.getLocale().toString();
if(locale.equals("ko")) {
	locale = "";
} else {
	locale = "/en";
} 
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/include_css.jsp"%>
<%@ include file="../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../ext/include/su_grid_common_render.jsp"%>
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../ext/js/json2.js"></script>
<script>

var GridObj 		= {};
var G_SERVLETURL = "/mg/cppp/member.do";

function init() {
	if("S07"=="<%=authCode%>"){
		GridObj = setGridDraw(GridObj);	
		GridObj.attachEvent("onXLE",doQueryEnd);
		doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
	}else{
		document.getElementById("body").innerHTML="<br><br><img src='../ext/images<%=locale%>/error_Page_.jpg'/>";
	}
}

function doQuery() {
	var grid_col_id  = "<%=grid_col_id%>";
	var argu = "&COMPANY_ID="+"<%=userCompanyId%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectSysCompUser&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
	
}

function doQueryEnd(GridObj, RowCnt) {
	var listCount = GridObj.getRowsNum();
	document.getElementById("totalCntTD").innerHTML = " <b>"+listCount+"</b>";
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	
	doCompanyInfo();
	
	return true;
	
}
function doCompanyInfo(){
	var arg = "&COMPANY_ID=" + "<%=userCompanyId %>"; 
	var url = G_SERVLETURL + "?mod=selectCompanyInfo"+arg; 
	sendRequest(doCompanyInfoEnd, " " ,"POST", url , false, false); 
}
function doCompanyInfoEnd(oj){
	var jData = JSON.parse(oj.responseText);
	document.getElementById("companyId").innerText=jData.COMPANY_ID;
	document.getElementById("companyNm").innerText=jData.COMPANY_NM;
	document.getElementById("ceoNm").innerText=jData.CEO_NM;
	document.getElementById("comTelNo").innerText=jData.COMPANY_TELNO;
	document.getElementById("addr").innerText=jData.ADDR;
}

</script>
</head>
<body onload="init();" onresize="resize();" id="body" class="cpBoardWrap">
<%@ include file="../mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<!-- <tr>
		<td>설명 텍스트 입력</td>
	</tr> -->
</table>
</div>
<p class="cpMemberTitle"><%=mm.getMessage("CPCOM_1110", s_user.getLocale())%></p>
<table class="cpMemberBoard">
	<tr>
		<% if("ILJIN".equals(pm.getString("component.site.company"))) {%>
			<th><%=mm.getMessage("CPCOM_1112", s_user.getLocale())%></th>
			<td colspan="3" id="companyNm"></td>
			<th style="display:none;"><%=mm.getMessage("CPCOM_1111", s_user.getLocale())%></th>
			<td style="display:none;" id="companyId"></td>
		<%} else {%>
			<th><%=mm.getMessage("CPCOM_1111", s_user.getLocale())%></th>
			<td id="companyId"></td>
			<th><%=mm.getMessage("CPCOM_1112", s_user.getLocale())%></th>
			<td id="companyNm"></td>
		<%} %>
	</tr>
	<tr>
		<th><%=mm.getMessage("CPCOM_1113", s_user.getLocale())%></th>
		<td id="ceoNm"></td>
		<th><%=mm.getMessage("CPCOM_1114", s_user.getLocale())%></th>
		<td id="comTelNo"></td>
	</tr>
	<tr>
		<th><%=mm.getMessage("SUMG_1183", s_user.getLocale())%></th>
		<td colspan="3" id="addr"></td>
	</tr>
</table>
<p class="cpMemberTitle pb0"><%=mm.getMessage("CPCOM_1115", s_user.getLocale())%></p>
<table class="cpSubTxt h26">
	<tr>
		<td><%=mm.getMessage("COMG_1014", s_user.getLocale())%> : <span class='point'  id="totalCntTD"></span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%></td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid h38p"></div>
</body>
</html>