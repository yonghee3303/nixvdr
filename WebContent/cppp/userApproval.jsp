<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName_;
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	
	IMenu menu = mnu.getMenu("CPCOM_0102", s_user);
	leftMenuName_ = menu.getName(s_user.getLocale());
}
String contextPath = request.getContextPath();

IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");

%>

<html>
<head>
<%@ include file="../ext/include/include_css.jsp"%>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/portal_grid_common.jsp"%>	
<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var myDataProcessor = {};
var G_SERVLETURL ="/mg/cppp/joiningUser.do";

function init() {
	
	setFormDraw();
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 

}
function setFormDraw(){
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onXLE",doQueryEnd);
	//GridObj.attachEvent("onRowDblClicked",doOnRowSelected);
	GridObj.setSizes();
}

function doQuery() {
	var grid_col_id  = "<%=grid_col_id%>";
	var argu = "&STAFF_ID="+"<%=s_user.getId()%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUserApproval&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}
function doQueryEnd(GridObj, RowCnt) {
	
	var listCount = GridObj.getRowsNum();
	document.getElementById("listCount").innerHTML = "<b>"+listCount+"</b>";
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}
//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("CPMG_1160",s_user.getLocale())%>")) {  
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&STAFF_ID="+"<%=s_user.getId()%>";
        var SERVLETURL = G_SERVLETURL + "?mod=approvalUser&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}
function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}
function doReturn(){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=returnUser&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}

</script>
</head>
<body onload="init();" class="cpBoardWrap">
<%@ include file="/mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<!-- <tr>
		<td>설명 텍스트 입력</td>
	</tr> -->
</table>
</div>
<table class="cpSubSrch">
	<tr>
		<td><%=mm.getMessage("COMG_1014",s_user.getLocale())%> : <font id="listCount"></font>건</td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid"></div>
<table class="cpBtnArea">
	<tr>
		<td>
			<div class="btnarea9">
				<a class="btn" href="javascript:doSave();"><span><%=mm.getMessage("CPMG_1108",s_user.getLocale())%></span></a>
			</div>
			<div class="btnarea6">
				<a class="btn" href="javascript:doReturn();"><span><%=mm.getMessage("CPMG_1107",s_user.getLocale())%></span></a>
			</div>
		</td>
	 </tr>
</table>
</body>
</html>