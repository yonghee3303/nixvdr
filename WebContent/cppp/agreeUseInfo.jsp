<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>    
<%
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String css = pm.getString("component.ui.portal.css");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=mm.getMessage("CPCOM_1201")%></title>
<link rel="stylesheet" type="text/css" href="../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../js/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.0.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script>
function goJoinUser(){
	var agree1 = document.form.agree1.checked;
	//var agree2 = document.form.agree2.checked;
	//var agree3 = document.form.agree3.checked;
	//var agree4 = document.form.agree4.checked;
	
	if(!agree1){
		alert("<%=mm.getMessage("CPCOM_1001")%>");
		return;
	}
	location.href="/cppp/joiningUser.jsp";
}
</script>
</head>
<body>
<form name="form" method="post">
<!-- // Window Popup start -->
<div class="winPop dvMinheight">
    <div class="header">
        <span><%=mm.getMessage("CPCOM_1201")%></span><!-- 개인 정보 수집 및 이용에 대한 동의 -->
        <a href="javascript:window.close();"><img src="../ext/images/btn_popup_close.png" alt="팝업닫기" /></a>
    </div>
    <div class="body">
        <div class="content">
            <p class="provision-txt"><%=mm.getMessage("CPCOM_1202")%></p>
            <div class="provision">
                <dl>
                <dt>■ <%=mm.getMessage("CPCOM_1201")%></dt>
                <dd>
                <textarea name="">
<%=mm.getMessage("CPCOM_1203")%>
<%=mm.getMessage("CPCOM_1204")%>
<%=mm.getMessage("CPCOM_1205")%>
<%=mm.getMessage("CPCOM_1234")%>
<%=mm.getMessage("CPCOM_1235")%>


<%=mm.getMessage("CPCOM_1206")%>
<%=mm.getMessage("CPCOM_1207")%>

<%=mm.getMessage("CPCOM_1208")%>
<%=mm.getMessage("CPCOM_1209")%>

<%=mm.getMessage("CPCOM_1210")%>

<%=mm.getMessage("CPCOM_1211")%> 

<%=mm.getMessage("CPCOM_1212")%>
</textarea>
                </dd>
                <dd class="provision-txt2"><input type="checkbox" name="agree1"/> <span><%=mm.getMessage("CPCOM_1213")%></span></dd>
                </dl>
            </div>
        </div>
        <ul class="btn_crud close2">
			<li><a class="darken" href="#" onclick="goJoinUser();"><span><%=mm.getMessage("CPCOM_1103")%></span></a></li>
            <li><a class="none" href="javascript:window.close();"><span><%=mm.getMessage("CPCOM_1104")%></span></a></li>
        </ul>
        <div class="clear"> </div>
    </div>
</div>
<!-- // Window Popup end -->
</form>
</body>
</html>
