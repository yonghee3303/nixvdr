<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String userRoll = s_user.getProperty("USER_ROLL").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);

IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String leftMenuName_;
String mileStoneName_ = request.getParameter("mileStoneName");
if(request.getParameter("leftMenuName")!=null){
 	leftMenuName_ = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8"); 
}else{
	
	IMenu menu = mnu.getMenu("CPCOM_0202", s_user);
	leftMenuName_ = menu.getName(s_user.getLocale());
} 
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
%>

<html>
<head>
<%@ include file="../ext/include/include_css.jsp"%>
<link rel="stylesheet" type="text/css" href="../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../ext/include/su_grid_common_render.jsp"%>	

<script type="text/javascript" src="../ext/js/ui_scripts.js"></script>
<script>

var GridObj 		= {};
var G_SERVLETURL = "/mg/cppp/board.do";

function init() {
	
	GridObj = setGridDraw(GridObj);

	GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
		parent.mainFrame.location.href ='content.jsp?BOARD_NO='+GridObj.cells(rId,GridObj.getColIndexById("BOARD_NO")).getValue()+'&title='+GridObj.cells(rId,GridObj.getColIndexById("BTITLE")).getValue()+'&BOARD_CLS_CD=P&leftMenuName=<%=leftMenuName_%>&mileStoneName=<%=mileStoneName_%>';
	});
	GridObj.attachEvent("onXLE",doQueryEnd);
	/* GridObj.enableSmartRendering(false);
	GridObj.enablePaging(true, 15, 5, "recinfoArea"); //페이징으로 전환하기 위한...
	GridObj.setPagingSkin("toolbar","dhx_skyblue");
	 */
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
	
}

function doQuery() {
	var selectBox = document.getElementById("searchTerm");
	var term="all";
	for(var i=0; i<selectBox.options.length; i++){
		if(selectBox.options[i].selected ==true){
			term = selectBox.options[i].value;
		}
	}
	var argu="";
	var mod="selectNotice";
	var con = encodeURIComponent(document.getElementById("con").value);
	switch(term){
		case "title":
			argu="&BTITLE="+con;
		break;
		
		case "user":
			argu="&SEARCH_ID="+con;
		break;
		
		case "number":
			argu="&BOARD_NO="+con;
		break;
		
		case "all":
			argu="&BTITLE="+con+"&SEARCH_ID="+con+"&BOARD_NO="+con;
			mod="searchAllNotice";
		break;
	}
	var grid_col_id  = "<%=grid_col_id%>";
	argu +="&BOARD_CLS_CD=P";
	GridObj.loadXML(G_SERVLETURL+"?mod="+mod+"&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

function doQueryEnd(GridObj, RowCnt) {
	
	var listCount = GridObj.getRowsNum();
	document.getElementById("listCount").innerHTML = listCount;
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doAdd(){
	parent.mainFrame.location.href ='htmlEditor.jsp?BOARD_CLS_CD=P&leftMenuName='+encodeURIComponent("FAQ");
}
</script>
</head>
<body onload="init();">
<%@ include file="/mg/common/milestone_cpcom.jsp"%>
<div id="header_top">
<table class="cpSubTitle">
	<tr>
		<th><%=leftMenuName_%></th>
	</tr>
	<!-- <tr>
		<td>설명 텍스트 입력</td>
	</tr> -->
</table>
</div>
<table class="cpSubSrch">
	<colgroup>
		<col class="col40p"/>
		<col/>
	</colgroup>
	<tr>
		<th>
			<select id="searchTerm" class="searchTerm">
				<option value="all"><%=mm.getMessage("COMG_1032", s_user.getLocale())%></option>
				<option value="title"><%=mm.getMessage("CPCOM_1119", s_user.getLocale())%></option>
				<option value="user"><%=mm.getMessage("CPCOM_1116", s_user.getLocale())%></option>
				<option value="number">No.</option>
			</select>
			<input type="text" class="p_text" id="con"  onkeypress="if(event.keyCode==13) {doQuery();}"/>
			<a href="javascript:doQuery();"><img class="search" src="../ext/images/<%=css%>/icon/i_search.png"></a>
		</th>
		<td><%=mm.getMessage("COMG_1014",s_user.getLocale())%> : <font id="listCount"></font>건</td>
	</tr>
</table>
<div id="gridbox" class="cpSubGrid"></div>
<div id="recinfoArea"></div>
<table class="cpBtnArea">
	<tr>
		<td>
			<div class="btnarea9">
			<%if("S01".equals(userRoll)){ %>
				<a href="javascript:doAdd();" class="btn"><span><%=mm.getMessage("SUMG_1169",s_user.getLocale())%></span></a>
			<%} %>  	
	        </div>
	   	</td>
	</tr>
</table>
</body>
</html>