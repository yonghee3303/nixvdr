<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	
	IUser s_user = (IUser) session.getAttribute("j_user");
	String contextPath = request.getContextPath();
	String css = pm.getString("component.ui.portal.css");
	String userRoll = s_user.getProperty("USER_ROLL").toString();
	String beforeDay = pm.getString("cliptorplus.dashboard.period.day");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="<%=contextPath%>/ext/js/lgchem/jquery-1.9.0.js"></script>
<!-- <script type="text/javascript" src="../../ext/js/lgchem/common.js"></script> -->
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="<%=contextPath%>/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/summaryInfo.do";
function init(){
	<%if("S03".equals(userRoll) || "S02".equals(userRoll) || "S01".equals(userRoll)){ %>
	doApproverAjax();
	<%}else{%>
	doAjax();
	<%}%>
	
}
//------------------------------------------------------------------------------------------------------------------승인자 관련
function doApproverAjax(){
	var url = G_SERVLETURL+"?mod=getDashBoardApprover&USER_ID=<%=s_user.getId()%>";
	sendRequest(doApproverAjaxEnd, "","POST", url, false, false);
}
function doApproverAjaxEnd(oj){
	var jData = JSON.parse(oj.responseText);
	 
	var allReciverRepCnt = jData.allReciverRepCnt; //총 반출 신청 건수
	var allApproveReqCnt = jData.allApproveReqCnt; //총 승인건수
	var allBanReqCnt = jData.allBanReqCnt; //총 반려건수
	var expireReqCnt = jData.expireReqCnt; //기한내 미 승인 
	var waitReqCnt = jData.waitReqCnt;//14일 내 승인대기 건수
	var approvalReqCnt = jData.approvalReqCnt; //14일 내 승인완료 건수 
	loadCheckApprover(allReciverRepCnt, allApproveReqCnt, allBanReqCnt, expireReqCnt, waitReqCnt, approvalReqCnt);
}
function loadCheckApprover(allReciverRepCnt, allApproveReqCnt, allBanReqCnt, expireReqCnt,waitReqCnt, approvalReqCnt){
	 //보안문서 승인권자 - 총 수신된 반출신청건수
	document.getElementById("allReciverRepCnt").innerText=allReciverRepCnt;
	document.getElementById("allReciverRepCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//보안문서 승인권자-승인건수: 총 신청받은 건수 중에서 승인이 된거
	document.getElementById("allApproveReqCnt").innerText=allApproveReqCnt;
	document.getElementById("allApproveReqCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//보안문서 승인권자 -반려건수 : 총 신청받은 건수 중에 반려된 건수
	document.getElementById("allBanReqCnt").innerText=allBanReqCnt;
	document.getElementById("allBanReqCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//보안문서 승인권자-미처리 : 현재 승인 대기 중 신청 기한이 15일 이상 지난 경우
	document.getElementById("expireReqCnt").innerText=expireReqCnt;
	document.getElementById("expireReqCnt").title="<%=mm.getMessage("CPMG_1055",beforeDay,s_user.getLocale())%>";
	//보안문서 승인권자 -승인대기 : 형재 승인 대기 중 신청 기한이 14일이하인 경우
	document.getElementById("waitReqCnt").innerText=waitReqCnt;
	document.getElementById("waitReqCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	//보안문서 승인권자 -승인완료 : 승인이 완료된 2주동안의 건수
	document.getElementById("approvalReqCnt").innerText=approvalReqCnt;
	document.getElementById("approvalReqCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
}
function clickBoardApprover(flag){
	var url="<%=contextPath%>/cp/portal/contents/approvalExportList.jsp";
	var param="?gridid=GridObj&formid=CPP_0401";
	
	switch(flag){
	case 1:
		param +="&FLAG=1";
		break;
	case 2:
		param+="&FLAG=2";
		break;
	case 3:
		param+="&FLAG=3";
		break;
	case 4:
		param +="&FLAG=4";
		break;
	case 5:
		param +="&FLAG=5";
		break;
	case 6:
		param +="&FLAG=6";
		break;
	}
	
	var leftMenuId="CPP_0401";
	 top.pageRedirect(url, param);
	 top.onClickChangePage(leftMenuId.substring(0, leftMenuId.length-2), leftMenuId);
}
//----------------------------------------------------------------------------------------------------반출자 관련
function doAjax(){
	var url = G_SERVLETURL+"?mod=getDashBoardExport&USER_ID=<%=s_user.getId().toString()%>";
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj){
	var jData = JSON.parse(oj.responseText);
	 
	 var reqCnt = jData.reqCnt; //총 반출 신청 건수
	 var aprovalCnt = jData.aprovalCnt; //총 승인건수
	 var deniedCnt = jData.deniedCnt; //총 반려건수
	 var expireCnt = jData.expireCnt; //기한내 미 승인 
	 var waitCnt = jData.waitCnt;//14일 내 승인대기 건수
	 var finishCnt = jData.finishCnt; //14일 내 승인완료 건수 
	 var noReadCnt = jData.noReadCnt; //반출 후 단 한명의 수신자도 읽지 않았을 경우
	 loadCheck(reqCnt, aprovalCnt, deniedCnt, expireCnt, waitCnt, finishCnt, noReadCnt);
	
}
function loadCheck(reqCnt, aprovalCnt, deniedCnt, expireCnt,waitCnt, finishCnt, noReadCnt){
 	 //반출 사용자 - 총 반출신청건수: 현재까지 모든 신청건수, 즉, 신청,승인,반려 상관없이 자신이 신청했을 경우
	document.getElementById("reqCnt").innerText=reqCnt;
	document.getElementById("reqCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//반출 사용자-승인건수: 총 신청한 건수 중에서 승인이 된거
	document.getElementById("aprovalCnt").innerText=aprovalCnt;
	document.getElementById("aprovalCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//반출 사용자 -반려건수 : 총 신청한 건수 중에 반려된 건수
	document.getElementById("deniedCnt").innerText=deniedCnt;
	document.getElementById("deniedCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//반출 사용자-미처리 : 현재 승인 대기 중 신청 기한이 15일 이상 지난 경우
	document.getElementById("expireCnt").innerText=expireCnt;
	document.getElementById("expireCnt").title="<%=mm.getMessage("CPMG_1055",beforeDay,s_user.getLocale())%>";
	//반출 사용자 -승인대기 : 형재 승인 대기 중 신청 기한이 14일이하인 경우
	document.getElementById("waitCnt").innerText=waitCnt;
	document.getElementById("waitCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	//반출 사용자 -승인완료 : 승인이 완료된 2주동안의 건수
	document.getElementById("finishCnt").innerText=finishCnt;
	document.getElementById("finishCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	//반출 사용자-읽지않음 : 반출 후 수신자가 한번이라도 로그인을 하지않았을 경우 (한명이라도 읽지 않았다면 읽지 않은 것으로 간주)
	document.getElementById("noReadCnt").innerText=noReadCnt;
	document.getElementById("noReadCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	
}
function clickBoard(flag){
	var url="<%=contextPath%>/cp/portal/contents/reqExportList.jsp";
	var param="?gridid=GridObj&formid=CPP_0102";
	
	switch(flag){
	case 1:
		param +="&FLAG=1";
		break;
	case 2:
		param+="&FLAG=2";
		break;
	case 3:
		param+="&FLAG=3";
		break;
	case 4:
		param +="&FLAG=4";
		break;
	case 5:
		param +="&FLAG=5";
		break;
	case 6:
		param +="&FLAG=6";
		break;
	case 7:
		param +="&FLAG=7";
		break;
	}
	
	var leftMenuId="CPP_0102";
	 top.pageRedirect(url, param);
	 top.onClickChangePage(leftMenuId.substring(0, leftMenuId.length-2), leftMenuId);
}
</script>
</head>
<body onload="init();" style="overflow:hidden;border:0px;" oncontextmenu="return false">
<%if("S03".equals(userRoll) || "S02".equals(userRoll) || "S01".equals(userRoll)){ %>
		<div class="SContents">
            <div class="content01">
                <div class="dashboard">
                    <dl> 
                    <dt><%=mm.getMessage("CPMG_1205",s_user.getLocale())%></dt>
                    <dd class="dashboard-bg1">
                    	<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1206",s_user.getLocale())%><span id="allReciverRepCnt"></span></p>
		                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1207",s_user.getLocale())%><span id="allApproveReqCnt"></span></p>
		                    <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1208",s_user.getLocale())%><span id="allBanReqCnt"></span></p>
		                    <p class="dashboard-txt4"><%=mm.getMessage("CPMG_1209",s_user.getLocale())%><span id="expireReqCnt"></span></p>
						<%} else {%>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1206",s_user.getLocale())%><span><a href="#" onclick="clickBoardApprover(1)" id="allReciverRepCnt"></a></span></p>
		                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1207",s_user.getLocale())%><span><a href="#" onclick="clickBoardApprover(2)" id="allApproveReqCnt"></a></span></p>
		                    <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1208",s_user.getLocale())%><span><a href="#" onclick="clickBoardApprover(3)" id="allBanReqCnt"></a></span></p>
		                    <p class="dashboard-txt4"><%=mm.getMessage("CPMG_1209",s_user.getLocale())%><span><a href="#" onclick="clickBoardApprover(4)" id="expireReqCnt"></a></span></p>	
						<%}%>
                    </dd>
                    <dd class="dashboard-bg6">
                    	<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1153",s_user.getLocale())%><span id="waitReqCnt"></span></p>
                    		<p class="dashboard-txt4"><%=mm.getMessage("CPMG_1207",s_user.getLocale())%><span id="approvalReqCnt"></span></p>
						<%} else {%>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1153",s_user.getLocale())%><span><a href="#" onclick="clickBoardApprover(5)" id="waitReqCnt"></a></span></p>
	                    	<p class="dashboard-txt4"><%=mm.getMessage("CPMG_1207",s_user.getLocale())%><span><a href="#" onclick="clickBoardApprover(6)" id="approvalReqCnt"></a></span></p>	
						<%}%>
                    </dd>
                    </dl>
                    <div class="clear"></div>
                </div>
            </div>
		</div>
<% }else{ %>
		<div class="SContents">
            <div class="content01">
                <div class="dashboard">
                    <dl>
                    <dt><%=mm.getMessage("CPMG_1210",s_user.getLocale())%></dt>
                    <dd class="dashboard-bg1">
                    	<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1211",s_user.getLocale())%><span id="reqCnt"></span></p>
		                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1207",s_user.getLocale())%><span id="aprovalCnt"></span></p>
		                    <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1208",s_user.getLocale())%><span id="deniedCnt"></span></p>
		                    <p class="dashboard-txt4"><%=mm.getMessage("CPMG_1209",s_user.getLocale())%><span id="expireCnt"></span></p>
						<%} else {%>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1211",s_user.getLocale())%><span><a href="#" onclick="clickBoard(1)" id="reqCnt"></a></span></p>
		                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1207",s_user.getLocale())%><span><a href="#" onclick="clickBoard(2)" id="aprovalCnt"></a></span></p>
		                    <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1208",s_user.getLocale())%><span><a href="#" onclick="clickBoard(5)" id="deniedCnt"></a></span></p>
		                    <p class="dashboard-txt4"><%=mm.getMessage("CPMG_1209",s_user.getLocale())%><span><a href="#" onclick="clickBoard(6)" id="expireCnt"></a></span></p>	
						<%}%>
                    </dd>
                    <dd class="dashboard-bg2">
                    	<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1153",s_user.getLocale())%><span id="waitCnt"></span></p>
		                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1148",s_user.getLocale())%><span id="finishCnt"></span></p>
		                    <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1212",s_user.getLocale())%><span id="noReadCnt"></span></p>
						<%} else {%>
							<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1153",s_user.getLocale())%><span><a href="#" onclick="clickBoard(3)" id="waitCnt"></a></span></p>
		                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1148",s_user.getLocale())%><span><a href="#" onclick="clickBoard(7)" id="finishCnt"></a></span></p>
		                    <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1212",s_user.getLocale())%><span><a href="#" onclick="clickBoard(4)" id="noReadCnt"></a></span></p>	
						<%}%>
                    </dd>
                    </dl>
                    <div class="clear"></div>
                </div>
            </div>
		</div>
<%} %>
</body>
</html>