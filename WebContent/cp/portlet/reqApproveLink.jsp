<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>    
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
String contextPath = request.getContextPath();
String leftMenuName = "";
String mileStoneName = mnu.getMenu("CPCOM_01", s_user).getName(s_user.getLocale());
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String locale = s_user.getLocale().toString();
if(locale.equals("ko")) {
	locale = "";
} else if(locale.equals("en") || locale.equals("zh")) {
	locale = "/" + locale;
} else {
	locale = "/en";
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>


<script>

function exportList(){
	//alert("exportList");
<%-- 	parent.location.href = "/cppp/docDownList.jsp?formid=CPCOM_0101&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>"; --%>
	//parent.location.href="/cp/portal/contents/reqExportDoc.jsp?formid=CPP_0101&gridid=GridObj";
	top.mainTop.clickTopMenu("/cp/portal/contents/reqExportDoc.jsp", "CPP_01");
}

function approvalUser(){
	//alert("approvalUser");
<%-- 	parent.location.href = "/cppp/repackingUpList.jsp?formid=CPCOM_0102&gridid=GridObj&leftMenuName=<%=java.net.URLEncoder.encode(leftMenuName,"utf-8")%>&mileStoneName=<%=java.net.URLEncoder.encode(mileStoneName,"utf-8")%>"; --%>
	//parent.location.href="/cp/portal/contents/portalUserApproval.jsp?formid=CPP_0301&gridid=GridObj";
	top.mainTop.clickTopMenu("/cp/portal/contents/portalUserApproval.jsp", "CPP_03");
}

</script>
</head>
<body style="padding:0px;margin:0px;" oncontextmenu="return false">
<div style="padding:0px;margin:0px;">
	<a href="javascript:exportList();"><img src="<%=contextPath%>/ext/images/<%=css %><%=locale%>/bnr_out.gif" width="234px" height="310px" border="none"/></a><a href="javascript:approvalUser();"><img src="<%=contextPath%>/ext/images<%=locale%>/bnr_approve.jpg" width="234px" height="310px" border="none"/></a>
</div>
</body>
</html>