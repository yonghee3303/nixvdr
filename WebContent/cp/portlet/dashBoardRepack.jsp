<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	
	IUser s_user = (IUser) session.getAttribute("j_user");
	String contextPath = request.getContextPath();
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String beforeDay = pm.getString("cliptorplus.dashboard.period.day");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="<%=contextPath%>/ext/js/lgchem/jquery-1.9.0.js"></script>
<!-- <script type="text/javascript" src="../../ext/js/lgchem/common.js"></script> -->
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="<%=contextPath%>/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/summaryInfo.do";
function init(){
	doAjax();
}
function doAjax(){
	var url = G_SERVLETURL+"?mod=getDashBoardRepack&USER_ID=<%=s_user.getId().toString()%>";
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj){
	var jData = JSON.parse(oj.responseText);
	 
	 var allRepCnt = jData.allRepCnt; //총 반입건수
	 var waitRepCnt = jData.waitRepCnt; //반입된 문서 중 다운로드를 받지 않은 문서
	 var waitUserCnt = jData.waitUserCnt; //파트너스 사용자 승인 요청이 들어온 건수
	 var approvalUserCnt = jData.approvalUserCnt; //파트너스 사용자 승인을 한 건수
	
	 loadCheck(allRepCnt, waitRepCnt, waitUserCnt, approvalUserCnt);
}
function loadCheck(allRepCnt, waitRepCnt, waitUserCnt, approvalUserCnt){
	//반입 문서 - 총 반입된 문서 : 기한에 상관 없이 반입받은 문서 건수
	document.getElementById("allRepCnt").innerText=allRepCnt;
	document.getElementById("allRepCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//반입문서-대기건수 : repacking 업로드 후 확인 또는 저장하지 않은 원문문서 
	document.getElementById("waitRepCnt").innerText=waitRepCnt;
	document.getElementById("waitRepCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	//협업포탈 사용자 승인-승인대기 : 자신을 담당자로 지정하여 등록한 승인을 기다리는 사용자 수.
	document.getElementById("waitUserCnt").innerText=waitUserCnt;
	document.getElementById("waitUserCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
	//외부협업문서 공유 사용자 관리 - 나의 협업 사용자 : 내가 사용 승인해준 협업문서 공유 시스템 사용자
	document.getElementById("approvalUserCnt").innerText=approvalUserCnt;
	document.getElementById("approvalUserCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
}

function clickBoard(flag){
	var url="/cp/portal/contents/";
	var param="?gridid=GridObj&formid=";
	var leftMenuId="";
	switch(flag){
	case 1:
		url +="repackingList.jsp";
		param +="CPP_0201&FLAG=4";
		leftMenuId="CPP_0201";
		break;
	case 2:
		url +="repackingList.jsp";
		param +="CPP_0201&FLAG=5";
		leftMenuId="CPP_0201";
		break;
	case 3:
		url +="portalUserList.jsp";
		param +="CPP_0302&FLAG=7";
		leftMenuId="CPP_0302";
		break;
	case 4:
		url +="portalUserApproval.jsp";
		param +="CPP_0301";
		leftMenuId="CPP_0301";
		break;
	}
	 top.pageRedirect(url, param);
	 top.onClickChangePage(leftMenuId.substring(0, leftMenuId.length-2), leftMenuId);

}
</script>
</head>
<body onload="init();" style="overflow:hidden;" oncontextmenu="return false">
<div class="SContents">
            <div class="content01">
                <div class="dashboard">
					<dl class="mr23">
                    <dt><%=mm.getMessage("CPMG_1213",s_user.getLocale())%></dt>
                    <dd class="dashboard-bg3">
	                    <p class="dashboard-txt1"><%=mm.getMessage("CPMG_1214",s_user.getLocale())%><span><a href="#" onclick="clickBoard(1)" id="allRepCnt"></a></span></p>
	                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1151",s_user.getLocale())%><span><a href="#" onclick="clickBoard(2)" id="waitRepCnt"></a></span></p>
                    </dd>
                    </dl>
                    <dl class="ml10">
                    <dt><%=mm.getMessage("CPMG_1215",s_user.getLocale())%></dt>
                    <dd class="dashboard-bg3">
	                    <p class="dashboard-txt1"><%=mm.getMessage("CPMG_1216",s_user.getLocale())%><span><a href="#" onclick="clickBoard(3)" id="approvalUserCnt"></a></span></p>
	                    <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1153",s_user.getLocale())%><span><a href="#" onclick="clickBoard(4)" id="waitUserCnt"></a></span></p>
                    </dd>
                    </dl>
                    <div class="clear"></div>
                </div>
            </div>
</div>
</body>
</html>