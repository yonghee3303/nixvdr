<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
String contextPath = request.getContextPath();
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
%>
<html>
<head>
<title>포틀릿 재고현황</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/grid_common.jsp"%><%-- <%@ include file="../../ext/include/su_grid_common_render.jsp" %> --%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>


<script type="text/javascript" src="<%=contextPath%>/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/JDate.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/doAjaxCombo.js"></script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
  	GridObj.setSizes();
    doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.x`
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
	
	if( header_name == "FILE_ICON" ) {
		popupAttachFile("File Upload", '', '', 440, 400,"regMgmt", docId, "read",<%=pm.getString("component.contextPath.root")%>);
	} else {
		// 문서등록 화면으로 이동
		var arqu_param = "?DOC_NO=" + docId; 
		doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", arqu_param);
	}

}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id = "<%=grid_col_id%>";
	var argu  = "&APPROVER_ID="	+ "<%=s_user.getId() %>";
	    argu += "&APPROVAL_YN=N";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectApproval&grid_col_id="+grid_col_id + argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	GridObj.setSizes();
	// document.getElementById("totalCntTD").innerHTML = "총 <span class='point'> : " + GridObj.getRowsNum()+"</span> 건 ";
	
	return true;
}

function init() {
	setFormDraw();
}


</script>
</head>
<body onload="init();" oncontextmenu="return false">
<form name="form" method="post">

</form>
<div id="gridbox" name="gridbox" width="100%" height="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="<%=contextPath%>/ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=0"/>
</jsp:include>
</body>
</html>
