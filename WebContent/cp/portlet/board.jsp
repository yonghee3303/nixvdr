<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String contextPath = request.getContextPath();
String leftMenuName = "";
String mileStoneName = mnu.getMenu("CPCOM_02", s_user).getName(s_user.getLocale());
String css = pm.getString("component.ui.portal.css");

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="../../ext/css/cooperation_<%=css %>.css" />
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>

<script>

var G_SERVLETURL = "<%=contextPath%>"+"/mg/cppp/board.do";

var BTITLE0 = "";
var BTITLE1 = "";
var BTITLE2 = "";
var BTITLE3 = "";
var BTITLE4 = "";
var BTITLE5 = "";
var BTITLE6 = "";
var BTITLE7 = "";
var REG_DT0 = "";
var REG_DT1 = "";
var REG_DT2 = "";
var REG_DT3 = "";
var REG_DT4 = "";
var REG_DT5 = "";
var REG_DT6 = "";
var REG_DT7 = "";
var BOARD_NO0 = "";
var BOARD_NO1 = "";
var BOARD_NO2 = "";
var BOARD_NO3 = "";
var BOARD_NO4 = "";
var BOARD_NO5 = "";
var BOARD_NO6 = "";
var BOARD_NO7 = "";
	
function init(){
	
	doAjax();
}

function doAjax() {
	var url = G_SERVLETURL + "?mod=selectRecentList&callFrom=CP";
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj) {
	
	var jData = JSON.parse(oj.responseText);
	
	<%-- <%for(int i=0;i<8;i++){%>
		if(jData.BITITLE<%=i%>==null){
			var BITITLE<%=i%> = "";
		}else{
			var BITITLE<%=i%> = jData.BITITLE<%=i%>;
		}
	<%}%> --%>
	
	
	/* for(var i=0;i<8;i++){
		eval(
				"if(jData.BITITLE" + i + "==''){"
			+ "var BITITLE" + i + "='';"
			+ "}else{"
			+ "var BITTLE" + i + "=jData.BTITLE" + i + ";"
			+ "}"
				);
		
		eval(
				"if(jData.REG_DT" + i + "==''){"
			+ "var REG_DT" + i + "='';"
			+ "}else{"
			+ "var REG_DT" + i + "=jData.REG_DT" + i + ";"
			+ "}"
				);
	} */
	
	
	BTITLE0 = jData.BTITLE0;
	BTITLE1 = jData.BTITLE1;
	BTITLE2 = jData.BTITLE2;
	BTITLE3 = jData.BTITLE3;
	BTITLE4 = jData.BTITLE4;
	BTITLE5 = jData.BTITLE5;
	BTITLE6 = jData.BTITLE6;
	BTITLE7 = jData.BTITLE7;
	REG_DT0 = jData.REG_DT0;
	REG_DT1 = jData.REG_DT1;
	REG_DT2 = jData.REG_DT2;
	REG_DT3 = jData.REG_DT3;
	REG_DT4 = jData.REG_DT4;
	REG_DT5 = jData.REG_DT5;
	REG_DT6 = jData.REG_DT6;
	REG_DT7 = jData.REG_DT7;
	BOARD_NO0 = jData.BOARD_NO0;
	BOARD_NO1 = jData.BOARD_NO1;
	BOARD_NO2 = jData.BOARD_NO2;
	BOARD_NO3 = jData.BOARD_NO3;
	BOARD_NO4 = jData.BOARD_NO4;
	BOARD_NO5 = jData.BOARD_NO5;
	BOARD_NO6 = jData.BOARD_NO6;
	BOARD_NO7 = jData.BOARD_NO7;
	
	
	/* document.getElementById("BTITLE0").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO0 + ");'>" + BTITLE0 + "</a>";
	document.getElementById("REG_DT0").innerHTML =REG_DT0;
	document.getElementById("BTITLE1").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO1 + ");'>" + BTITLE1 + "</a>";
	document.getElementById("REG_DT1").innerHTML =REG_DT1;
	document.getElementById("BTITLE2").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO2 + ");'>" + BTITLE2 + "</a>";
	document.getElementById("REG_DT2").innerHTML = REG_DT2;
	document.getElementById("BTITLE3").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO3 + ");'>" + BTITLE3 + "</a>";
	document.getElementById("REG_DT3").innerHTML = REG_DT3;
	document.getElementById("BTITLE4").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO4 + ");'>" + BTITLE4 + "</a>";
	document.getElementById("REG_DT4").innerHTML = REG_DT4;
	document.getElementById("BTITLE5").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO5 + ");'>" + BTITLE5 + "</a>";
	document.getElementById("REG_DT5").innerHTML = REG_DT5;
	document.getElementById("BTITLE6").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO6 + ");'>" + BTITLE6 + "</a>";
	document.getElementById("REG_DT6").innerHTML = REG_DT6;
	document.getElementById("BTITLE7").innerHTML = "<a href='javascript:clickContent("+ BOARD_NO7 + ");'>" + BTITLE7 + "</a>";
	document.getElementById("REG_DT7").innerHTML = REG_DT7; */
	
	document.getElementById("BTITLE0").innerHTML = "<a href='javascript:clickContent(0);'>" + BTITLE0 + "</a>";
	document.getElementById("REG_DT0").innerHTML =REG_DT0;
	document.getElementById("BTITLE1").innerHTML = "<a href='javascript:clickContent(1);'>" + BTITLE1 + "</a>";
	document.getElementById("REG_DT1").innerHTML =REG_DT1;
	document.getElementById("BTITLE2").innerHTML = "<a href='javascript:clickContent(2);'>" + BTITLE2 + "</a>";
	document.getElementById("REG_DT2").innerHTML = REG_DT2;
	document.getElementById("BTITLE3").innerHTML = "<a href='javascript:clickContent(3);'>" + BTITLE3 + "</a>";
	document.getElementById("REG_DT3").innerHTML = REG_DT3;
	document.getElementById("BTITLE4").innerHTML = "<a href='javascript:clickContent(4);'>" + BTITLE4 + "</a>";
	document.getElementById("REG_DT4").innerHTML = REG_DT4;
	document.getElementById("BTITLE5").innerHTML = "<a href='javascript:clickContent(5);'>" + BTITLE5 + "</a>";
	document.getElementById("REG_DT5").innerHTML = REG_DT5;
	document.getElementById("BTITLE6").innerHTML = "<a href='javascript:clickContent(6);'>" + BTITLE6 + "</a>";
	document.getElementById("REG_DT6").innerHTML = REG_DT6;
	document.getElementById("BTITLE7").innerHTML = "<a href='javascript:clickContent(7);'>" + BTITLE7 + "</a>";
	document.getElementById("REG_DT7").innerHTML = REG_DT7;
	
}


function clickContent(no) {
	
	//alert(no  + "번째 게시글 선택");
	
	var boardNo = "";
	eval("boardNo = BOARD_NO" + no);
	
	if(no<4){
		// 공지사항 게시글 클릭시
		parent.location.href = "../portal/contents/content.jsp?BOARD_NO=" + boardNo + '&BOARD_CLS_CD=B&formid=CPCOM_0201';
		top.onClickChangePage("CPP_05", "CPP_0501");
	}else{
		// FAQ 게시글 클릭시
		parent.location.href = "../portal/contents/content.jsp?BOARD_NO=" + boardNo + '&BOARD_CLS_CD=F&formid=CPCOM_0202';
		top.onClickChangePage("CPP_05", "CPP_0502");
	}
	
}
function goList(boardCd){
	if(boardCd ==0){
		parent.location.href ="../portal/contents/notice.jsp?formid=CPCOM_0201&gridid=GridObj";
		top.onClickChangePage("CPP_05", "CPP_0501");
	}else{
		parent.location.href ="../portal/contents/faq.jsp?formid=CPCOM_0202&menuId=CPP_0501&gridid=GridObj";
		top.onClickChangePage("CPP_05", "CPP_0502");
	}
}
</script>
</head>
<body onload="init();" class="cpBoardWrap" oncontextmenu="return false">
<table class="cpBoardList">
	<tr>
		<th colspan="2">
        	<span>N</span>OTICE<a href="#" onclick="goList(0)"><img class="boardMore" src="/ext/images/<%=css%>/btn_more.gif" alt="more" /></a>
		</th>
	</tr>
	<tr>
		<td><span id="BTITLE0" class="table_txt"></span></td>
		<td id="REG_DT0" class="cpBoardDate"></td>
	</tr>
	<tr>
		<td><span id="BTITLE1" class="table_txt"></span></td>
		<td id="REG_DT1" class="cpBoardDate"></td>
	</tr>
	<tr>
		<td><span id="BTITLE2" class="table_txt"></span></td>
		<td id="REG_DT2" class="cpBoardDate"></td>
	</tr>
	<tr>
		<td><span id="BTITLE3" class="table_txt"></span></td>
		<td id="REG_DT3" class="cpBoardDate"></td>
	</tr>
</table>
<table class="cpBoardList lineTop">
	<tr>
		<th colspan="2" class="pt3">
        	<span>F</span>AQ<a href="#" onclick="goList(1)"><img class="boardMore" src="/ext/images/<%=css%>/btn_more.gif" alt="more" /></a>
		</th>
	</tr>
	<tr>
		<td><span id="BTITLE4" class="table_txt"></span></td>
		<td id="REG_DT4" class="cpBoardDate"></td>
	</tr>
	<tr>
		<td><span id="BTITLE5" class="table_txt"></span></td>
		<td id="REG_DT5" class="cpBoardDate"></td>
	</tr>
	<tr>
		<td><span id="BTITLE6" class="table_txt"></span></td>
		<td id="REG_DT6" class="cpBoardDate"></td>
	</tr>
	<tr>
		<td><span id="BTITLE7" class="table_txt"></span></td>
		<td id="REG_DT7" class="cpBoardDate"></td>
	</tr>
</table>
</body>
</html>