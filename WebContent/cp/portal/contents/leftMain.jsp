<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	
	IUser s_user = (IUser) session.getAttribute("j_user");
	String contextPath = request.getContextPath();

	String userNm = s_user.getName();
	String deptNm = s_user.getProperty("DEPT_NM").toString();
  	String position="";
  	
  	if(s_user.getProperty("POSITION_NM")== null && s_user.getProperty("FUNCTION_NM")== null) {
  		position = "unknown";
  	} else {
  		if(s_user.getProperty("POSITION_NM")== null){
  			position = s_user.getProperty("FUNCTION_NM").toString();
  		} else{
  			position = s_user.getProperty("POSITION_NM").toString();
  		}
  	}
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String locale = s_user.getLocale().toString();
	if(locale.equals("ko")) {
		locale = "";
	} else if(locale.equals("en") || locale.equals("zh")) {
		locale = "/" + locale;
	} else {
		locale = "/en";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<!-- <script type="text/javascript" src="../../ext/js/lgchem/common.js"></script> -->
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
function logout() {
	if(confirm("<%=mm.getMessage("COMG_1034", s_user.getLocale())%>")) {  // 시스템을 종료 하시겠습니까 ?
	   top.location.href = "<%=contextPath%>/Logout.do";
	}
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
function openSystemMap(){
	centerWin("/mg/common/system_map.jsp",510,365,"yes");
	//window.open("/mg/common/system_map.jsp",'','width=510px,height=365px,scrollbars=yes,resizable=no');
}
</script>
</head>
<body oncontextmenu="return false">
<div class="leftMenu ml20">
            <a class="leftTop close"><span class="Lnodisplay">접고 펼치기 버튼</span></a>
            <div class="closeOpen">
                <div class="leftSystemInfo3">
                    <div class="leftSysname3"><%=mm.getMessage("WTMG_1002", s_user.getLocale())%></div>
                    <ul class="leftLinks2">
                    	<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
                    	<li><span><p class="floatLeft" style="width:auto;"><%=deptNm%></p>
                    	<a href="javascript:logout();" class="btn leftSystemLogout"><img src="../../../ext/images/btn_logout1.gif" alt="Logout" /></a></span>
                    	<p><span><%=userNm%> <%=position%></span></p></li>
                        <%} else {%>
                        <li><p><%=deptNm%></p>
                    	<p><span><%=userNm%> <%=position%></span></p></li>
                        <%}%>
                    </ul>
                </div>
                <ul class="leftUnderBanner2">
                    <li><a href="#" onclick="javascript:openSystemMap();" ><img src="../../../ext/images/<%=css %><%=locale%>/bnr_systemmap.gif" ></a></li>
                    <li>
                    	<a href="../../../ext/guide/User'sGuide.pdf" target="_blank">
                    		<img src="../../../ext/images/<%=css %><%=locale%>/bnr_download.gif" >
                    	</a>
                    </li>
                    <%-- <li><img src="../../../ext/images/<%=css %><%=locale%>/bnr_inquiry.gif" ></li> --%>
                </ul>
            </div>
</div>
</body>
</html>