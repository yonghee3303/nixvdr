<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user"); 
  //String companyId = request.getParameter("companyId");
  //String companyNm = request.getParameter("companyNm");
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String flag = StringUtils.paramReplace(request.getParameter("FLAG"));
  String contextPath = request.getContextPath();

  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  String css = pm.getString("component.ui.portal.css");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=contextPath%>/ext/css/common.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>
<%@ include file="../../../ext/include/portal_grid_common.jsp" %>	
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<!-- <script type="text/javascript" src="../../../ext/js/JDate.js"></script> -->
<script type="text/javascript" src="../../../ext/js/doAjaxCombo.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var G_SERVLETURL = "<%=contextPath%>/mg/cppp/member.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

//위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedRowId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedRowId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}
function doOnRowSelect(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
}
function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() 
{
	<%-- var companyId = "<%=companyId%>";  --%>
	var grid_col_id = "<%=grid_col_id%>";

    var companyNm =document.getElementById("companyNm").value;
	//var coRegNo =document.getElementById("coRegNo").value;
	//var ceo =document.getElementById("ceo").value;
	var userNm=document.getElementById("userNm").value;
    if(companyNm =="" && userNm ==""){
    	return;
    }   
	var url= "&COMPANY_NM="+encodeURIComponent(companyNm);
	//url +="&REG_NO="+coRegNo;
	//url +="&CEO_NM="+encodeURIComponent(ceo);
	url +="&USER_NM="+encodeURIComponent(userNm);
	/* if( companyId == "") return; */
	/* GridObj.loadXML(G_SERVLETURL+"?mod=selectSysCompUserList&grid_col_id="+grid_col_id+"&COMPANY_ID="+companyId); */
	GridObj.loadXML(G_SERVLETURL+"?mod=selectSysCompUserList&grid_col_id="+grid_col_id+url);																					
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) 
{
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	if(status == "false") alert(msg);
	return true;
}
function checkMail(strEmail){
	
	 var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	
	 if(!strEmail.match(regExp)){
		return false;
	}else{
		return true;
	}
}
function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	for(var i=0; i < grid_array.length; i++) {

		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("USER_NM")).getValue() == "") {
			alert("<%=mm.getMessage("CPMG_1165",s_user.getLocale())%>");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("EMAIL")).getValue() == "") {
			alert("<%=mm.getMessage("CPMG_1166",s_user.getLocale())%>");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("POSITION_NM")).getValue() == "") {
			alert("<%=mm.getMessage("CPMG_1167",s_user.getLocale())%>");
			return;
		}
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("TEL_NO")).getValue() == "") {
			alert("<%=mm.getMessage("CPMG_1168",s_user.getLocale())%>");
			return;
		}
		if(!checkMail(GridObj.cells(grid_array[i], GridObj.getColIndexById("EMAIL")).getValue())){
			alert("<%=mm.getMessage("CPMG_1169",s_user.getLocale())%>");
			return;
		}
	}
	 
	if (confirm("<%=mm.getMessage("COMG_1009",s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
		<%-- var argu = "&COMPANY_ID="+"<%=companyId%>"+"&STAFF_ID="+"<%=s_user.getId()%>"+"&COMPANY_NM="+encodeURIComponent("<%=companyNm%>"); --%>
		var SERVLETURL = G_SERVLETURL + "?mod=saveCompUserStaff&col_ids="+cols_ids+argu;
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}
function doAddRow() {
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
<%-- 	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_NM")).setValue("<%=companyNm%>"); --%>

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function init() {
	setFormDraw();
	
	switch("<%=flag%>"){
	case "7": //모든 건수
		doQueryMyCompUser();
		break;
	default:
		doQueryMyCompUser();
		break;
	}
}
function doQueryMyCompUser(){
	<%-- var companyId = "<%=companyId%>";  --%>
	var grid_col_id = "<%=grid_col_id%>";

/*     var companyNm =document.getElementById("companyNm").value;
	var coRegNo =document.getElementById("coRegNo").value;
	var ceo =document.getElementById("ceo").value;
	var userNm=document.getElementById("userNm").value;
       
	var url= "&COMPANY_NM="+encodeURIComponent(companyNm);
	url +="&REG_NO="+coRegNo;
	url +="&CEO_NM="+encodeURIComponent(ceo);
	url +="&USER_NM="+encodeURIComponent(userNm); */
 
	var url ="&SATFF_ID="+"<%=s_user.getId() %>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectSysCompUserList&grid_col_id="+grid_col_id+url);																					
	GridObj.clearAll(false);	
}

/* function loadCombobox() {
	// 마스터 데이터 Combo 검색조건 표현
} */
</script>
<title></title>
</head>
<body onload="init();" oncontextmenu="return false">
         <!-- contents start -->
        <div class="contents">
            <!-- innerContent start -->
            <div class="innerContent">
                <!-- Page Title start -->
                <!-- <div class="title">
                    <h1>협업포탈 사용자 검색</h1>
                    <div class="titleRight">
                        <ul class="pageLocation">
                            <li><span><a href="#">Home</a></span></li>
                            <li><span><a href="#">협업포탈 사용자 관리</a></span></li>
                            <li class="lastLocation"><span><a href="#">협업포탈 사용자 검색</a></span></li>
                        </ul>
                    </div>
                </div> -->
                <%@ include file="/mg/common/milestone_cpportal.jsp"%>
                <!--// Page Title end -->

                <div class="tableInquiry">
                    <table>
                    <caption>여러행조회</caption>
                    <colgroup>
                    <col class="col_7p" />
                    <col class="col_15p" />
                    <col class="col_12p" />
                    <col class="col_15p" />
                    <col class="col_6p" />
                    <col class="col_15p" />
                    <col class="col_6p" />
                    <col />
                    </colgroup>
                    <tbody>
                    <tr>	
                        <th><%=mm.getMessage("CPMG_1162",s_user.getLocale())%></th>
                        <td><input type="text" id="companyNm" class="w110" /></td>
                        <%-- <th><%=mm.getMessage("CPMG_1170",s_user.getLocale())%></th>
                        <td><input type="text" id="coRegNo" class="w110" /></td> --%>
                        <%-- <th><%=mm.getMessage("CPMG_1171",s_user.getLocale())%></th>
                        <td><input type="text" id="ceo" class="w110" /></td> --%>
                        <th><%=mm.getMessage("SUMG_1137",s_user.getLocale())%></th>
                        <td><input type="text" id="userNm" class="w110" /></td>
                        <th></th>
                        <td></td>
                        <th></th>
                    	<td></td>
                    </tr>
                    </tbody>
                    </table>
                    <div class="tableBtnSearch"><button onclick="doQuery();"><span><%=mm.getMessage("CPMG_1164",s_user.getLocale())%></span></button></div>
                    <div class="clear"> </div>
                </div>
                <!--// Inquiry Table (2 cases) end -->

                <!-- listArea start -->
                <div class="listArea">
                    <table border="0" class="mb5">
				       <colgroup>
				           <col width="10%" />
				           <col width="90%" />
				       </colgroup>
				       <tr>
				       	<td><div class="title_num" id="totalCntTD"></div></td>
				           <td>
				            <div class="btnarea">
				           		<%-- <a href="javascript:doTransDetail();" class="btn"><span><%=mm.getMessage("CPMG_1143", s_user.getLocale())%></span></a>
				            	<a href="javascript:doSave('N');" class="btn"><span><%=mm.getMessage("CPMG_1107", s_user.getLocale())%></span></a>
				            	<a href="javascript:doSave('Y');" class="btn"><span><%=mm.getMessage("CPMG_1108", s_user.getLocale())%></span></a> --%>
				            </div>
				           </td>
				       </tr>
				    </table>
                    <div>
                 		 <div id="gridbox" class="cpSubGrid"></div>
                    </div>
                </div>
                <!--// listArea end -->
            </div>
            <!-- innerContent end -->
        </div>
        <!--// contents end -->
</body>
</html>