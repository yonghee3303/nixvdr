<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	
	IUser s_user = (IUser) session.getAttribute("j_user");
	String contextPath = request.getContextPath();

	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<!-- <script type="text/javascript" src="../../ext/js/lgchem/common.js"></script> -->
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
function init(){
	
}
</script>
</head>
<body onload="init();" oncontextmenu="return false">
<div class="SContents" style="left:0px;">
            <div class="content01">
                <div class="dashboard">
                    <h2><img src="../../../ext/images/txt_dash.gif" alt="dash board" /></h2>
                    <dl>
                    <dt>반출 사용자</dt>
                    <dd class="dashboard-bg1">
                    <p>총 반출신청건수</p><span class="dashboard-txt1">5237</span>
                    <p>승인건수</p><span class="dashboard-txt2">50</span>
                    <p>반려건수</p><span class="dashboard-txt3">8</span>
                    <p>미처리</p><span class="dashboard-txt4">256</span>
                    </dd>
                    <dd class="dashboard-bg2">
                    <p>승인대기</p><span class="dashboard-txt1">5237</span>
                    <p>승인완료</p><span class="dashboard-txt2">50</span>
                    <p>읽지않음</p><span class="dashboard-txt3">8</span>
                    </dd>
                    </dl>
                    <dl>
                    <dt>반입 문서</dt>
                    <dd class="dashboard-bg3">
                    <p>총 반입된 문서</p><span class="dashboard-txt1">987</span>
                    <p>미확인</p><span class="dashboard-txt2">11</span>
                    </dd>
                    </dl>
                    <dl>
                    <dt>외부협업문서 공유 사용자 관리</dt>
                    <dd class="dashboard-bg4">
                    <p>나의 협업사용자</p><span class="dashboard-txt1">10</span>
                    <p>승인대기</p><span class="dashboard-txt2">3</span>
                    </dd>
                    </dl>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content02">
                <div class="BigBanner">
                    <a href="#"><img src="../../../ext/images/bnr_out.jpg" alt="협업문서 반출신청" /></a><a href="#"><img src="../../../ext/images/bnr_approve.jpg" alt="협업사용자 승인" /></a>
                </div>
                <div class="SNotice">
                    <div class="subHeader">
                        <img src="../../../ext/images/txt_notice.gif" alt="notice" />
                        <a href="#"><img class="boardMore" src="../../../ext/images/<%=css%>/btn_more.gif" alt="more" /></a>
                    </div>
                    <ul>
                        <li>
                        <a href="#"><span>2013년 경비처리가이드</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>2012년 기말결산 일정 (12.31(월)) 승인 마감</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>법인카드, 업무택시카드 급여공제 요청</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>골프 접대/행사 포함시 경비처리 지침</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>2012년 상반기 경비모니터링 위반사항 및 처리방법 안내</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                    </ul>
                </div>
                <div class="SQna">
                    <div class="subHeader">
                        <img src="../../../ext/images/txt_faq.gif" alt="faq" />
                        <a href="#"><img class="boardMore" src="../../../ext/images/<%=css%>/btn_more.gif" alt="more" /></a>
                    </div>
                    <ul>
                        <li>
                        <a href="#"><span>2013년 경비처리가이드</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>2012년 기말결산 일정 (12.31(월)) 승인 마감</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>법인카드, 업무택시카드 급여공제 요청</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>골프 접대/행사 포함시 경비처리 지침</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                        <li>
                        <a href="#"><span>2012년 상반기 경비모니터링 위반사항 및 처리방법 안내</span>
                        <span class="date">[13.01.16]</span></a>
                        </li>
                    </ul>
                </div>
            </div>
</div>
</body>
</html>