<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String userRoll = s_user.getProperty("USER_ROLL").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();

IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");

%>

<html>
<head>
<%@ include file="../../../ext/include/include_css.jsp"%>
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />

<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>	

<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script>

var GridObj 		= {};
var G_SERVLETURL = "<%=contextPath%>/mg/cppp/board.do";

function init() {
	
	GridObj = setGridDraw(GridObj);

	GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
/* 		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY")).setValue(policy); */
		parent.pageRedirect("content.jsp",'?BOARD_NO='+GridObj.cells(rId,GridObj.getColIndexById("BOARD_NO")).getValue()+'&title='+GridObj.cells(rId,GridObj.getColIndexById("BTITLE")).getValue()+'&BOARD_CLS_CD=F&formid=CPCOM_0202');
	});
	GridObj.attachEvent("onXLE",doQueryEnd);
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
}

function doQuery() {
	var selectBox = document.getElementById("searchTerm");
	var term="all";
	for(var i=0; i<selectBox.options.length; i++){
		if(selectBox.options[i].selected ==true){
			term = selectBox.options[i].value;
		}
	}
	var argu="";
	var mod="selectNotice";
	var con = encodeURIComponent(document.getElementById("con").value);
	switch(term){
		case "title":
			argu="&BTITLE="+con;
		break;
		
		case "user":
			argu="&SEARCH_ID="+con;
		break;
		
		case "number":
			argu="&BOARD_NO="+con;
		break;
		
		case "all":
			argu="&BTITLE="+con+"&SEARCH_ID="+con+"&BOARD_NO="+con;
			mod="searchAllNotice";
		break;
	}
	var grid_col_id  = "<%=grid_col_id%>";
	argu +="&BOARD_CLS_CD=F";
	GridObj.loadXML(G_SERVLETURL+"?mod="+mod+"&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false);
}

function doQueryEnd(GridObj, RowCnt) {
	
	var listCount = GridObj.getRowsNum();
	//document.getElementById("listCount").innerHTML = listCount;
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doAdd(){
	parent.pageRedirect("htmlEditor.jsp",'?BOARD_CLS_CD=F&formid=CPCOM_0202');
}
</script>
</head>
<body onload="init();" oncontextmenu="return false">
 <!-- contents start -->
<div class="contents">
<!-- innerContent start -->
<div class="innerContent">
<%@ include file="/mg/common/milestone_cpportal.jsp"%>
<!--// Page Title end -->
<table class="cpSubSrch">
	<colgroup>
		<col class="col40p"/>
		<col/>
	</colgroup>
	<tr>
		<th>
			<select id="searchTerm" class="searchTerm">
				<option value="all"><%=mm.getMessage("COMG_1032", s_user.getLocale())%></option>
				<option value="title"><%=mm.getMessage("CPCOM_1119", s_user.getLocale())%></option>
				<option value="user"><%=mm.getMessage("CPCOM_1116", s_user.getLocale())%></option>
				<option value="number">No.</option>
			</select>
			<input type="text" class="p_text" id="con"  onkeypress="if(event.keyCode==13) {doQuery();}"/>
			<a href="javascript:doQuery();"><img class="search" src="/ext/images/<%=css%>/icon/i_search.png"></a>
		</th>
		<td></td>
	</tr>
</table>
<!-- listArea start -->
<div class="listArea">
	<div class="buttonArea">
		<ul class="btn_crud">
		<%if("S01".equals(userRoll)){ %>
			<li><a href='#' onclick="doAdd()"><span class="ls-2"><%=mm.getMessage("SUMG_1169",s_user.getLocale())%></span></a></li>
			<%} %>
			<%--  <li><a href="#" onclick="doReturn()"><span><%=mm.getMessage("CPMG_1107",s_user.getLocale())%></span></a></li> --%>
		</ul>
	</div>
<div>
	<div id="gridbox" class="cpSubGrid"></div>
</div>
</div>
<!--// listArea end -->
</div>
<!-- innerContent end -->
</div>
<!--// contents end -->
<div class="clear"> </div>
<!--// container end -->
</body>
</html>