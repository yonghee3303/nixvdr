<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%> 

<%
Log log = LogFactory.getLog(this.getClass());
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
IUser s_user = (IUser) session.getAttribute("j_user");

//로그인 사용자의 권한을 가져와 메인포털 화면배치를 다르게 설정
boolean isAdmin = false;
String initpage = "";
log.info("로그인 사용자 권한 : " + s_user.getProperty("USER_ROLL").toString());
if( "S01".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
}

//log.info("세션유저 정보" + s_user.toString());
log.info("레이아웃 형태 : " + s_user.getProperty("LAYOUT").toString());
log.info("레이아웃 주소 : " + s_user.getProperty("LAYOUT_URL").toString());
log.info("레이아웃 formId : " + s_user.getProperty("LAYOUT_FORMID").toString());
log.info("레이아웃 gridId : " + s_user.getProperty("LAYOUT_GRIDID").toString());
log.info("레이아웃 width : " + s_user.getProperty("LAYOUT_WIDTH").toString());
log.info("레이아웃 height : " + s_user.getProperty("LAYOUT_HEIGHT").toString());
//log.info("레이아웃 개수" + s_user.getProperty("LAYOUT_CNT").toString());

//String - int 변환 (Layout형태, 크기)
/* String[] tmpLayout = s_user.getProperty("LAYOUT").toString().split("\\|");
String[] tmpWidth = s_user.getProperty("LAYOUT_WIDTH").toString().split("\\|");
String[] tmpHeight = s_user.getProperty("LAYOUT_HEIGHT").toString().split("\\|");
int layoutCnt = 0; 

int[] arr_layout = new int[tmpLayout.length];
int[] arr_height = new int[tmpLayout.length];

for(int i=0;i<tmpLayout.length;i++){
	arr_layout[i] = Integer.parseInt(tmpLayout[i]);
	arr_height[i] = Integer.parseInt(tmpHeight[i]);
	layoutCnt += arr_layout[i];
}

//log.info("계산된 layout 총 개수 : " + layoutCnt);

int[] arr_width = new int[layoutCnt];

for(int i=0;i<layoutCnt;i++){
	arr_width[i] = Integer.parseInt(tmpWidth[i]);
}

String[] arr_url = s_user.getProperty("LAYOUT_URL").toString().split("\\|");
String[] arr_formId = s_user.getProperty("LAYOUT_FORMID").toString().split("\\|");
String[] arr_gridId = s_user.getProperty("LAYOUT_GRIDID").toString().split("\\|"); */
//String[] arr_url = new String[] {"../../cppp/joiningUser.jsp","../../ext/SmartEditor/SmartEditor2Skin.html","../../ext/SmartEditor/SmartEditor2Skin.html","../../cppp/docDownList.jsp","/cppp/notice.jsp"};
//String[] arr_formId = new String[] {"CPCOM_0403","","","CPCOM_0101","CPCOM_0201"};
//String[] arr_gridId = new String[] {"GridObj","GridObj","GridObj","GridObj","GridObj"};


/*                 ,'3|2' as LAYOUT,
                '../../cppp/portlet/userInfo.jsp|../../cppp/portlet/guideManual.jsp|../../cppp/portlet/banners.jsp|../../cppp/portlet/upDownLink.jsp|../../cppp/portlet/board.jsp' as LAYOUT_URL,
				'|||CPCOM_0101|CPCOM_0201' as LAYOUT_FORMID,
				'GridObj|GridObj|GridObj|GridObj|GridObj' as LAYOUT_GRIDID,
				'326|192|459|494|495' as LAYOUT_WIDTH,
				'211|313' as LAYOUT_HEIGHT
 */
 String[] tmpLayout = "2|2".split("\\|");
 String[] tmpWidth = "482|503|482|503".split("\\|");
 String[] tmpHeight = "200|303".split("\\|");
 int layoutCnt = 0;

 int[] arr_layout = new int[tmpLayout.length];
 int[] arr_height = new int[tmpLayout.length];

 for(int i=0;i<tmpLayout.length;i++){
 	arr_layout[i] = Integer.parseInt(tmpLayout[i]);
 	arr_height[i] = Integer.parseInt(tmpHeight[i]);
 	layoutCnt += arr_layout[i];
 }

 //log.info("계산된 layout 총 개수 : " + layoutCnt);

 int[] arr_width = new int[layoutCnt];

 for(int i=0;i<layoutCnt;i++){
 	arr_width[i] = Integer.parseInt(tmpWidth[i]);
 }

 String[] arr_url = new String[] {"../../portlet/dashBoardExport.jsp","../../portlet/dashBoardRepack.jsp","../../portlet/reqApproveLink.jsp","../../portlet/board.jsp"};
 String[] arr_formId = new String[] {"","","",""};
 String[] arr_gridId = new String[] {"GridObj","GridObj","GridObj","GridObj"};
 
%>

<html>
<head>

<title>CP Community Main</title>

<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

//전역변수 동적 선언
var layoutCnt = <%=layoutCnt%>;

<%for(int i=1;i<layoutCnt+1;i++){%>
	
	var layout<%=i%>;
	var cell<%=i%>;
	
<%}%>

var arr_url = new Array(<%=layoutCnt%>);
var arr_formId = new Array(<%=layoutCnt%>);
var arr_gridId = new Array(<%=layoutCnt%>);


function init() {
	
	//자바스크립트 배열로 데이터 전달
	<%for(int i=0;i<layoutCnt;i++){%>
	
		arr_url[<%=i%>] = "<%=arr_url[i]%>";
		arr_formId[<%=i%>] = "<%=arr_formId[i]%>";
		arr_gridId[<%=i%>] = "<%=arr_gridId[i]%>";
		
	<%}%>
	
	
	
	//총 개수만큼 레이아웃객체 동적 생성 및 페이지 연결
	for(var i=1;i<layoutCnt+1;i++){
		
		eval(
				"layout" + i + "=new dhtmlXLayoutObject({"
			    + "parent: document.getElementById(\"layoutObj"+ i + "\"),"
			    + "pattern: \"1C\","
			    + "skin: \"dhx_web\""
			+ "});"
			+ "cell"+ i +" = layout"+ i +".cells(\"a\");"
			/* + "cell"+ i +".setText(\"" + i + "번째 레이아웃\");" */
			+ "cell"+ i +".hideHeader();"
			);
		
		eval(
				"cell"+i+".attachURL(\""+ arr_url[i-1] +"\",null,  {"
				+	"formid: \""+ arr_formId[i-1] +"\","
				+	"gridid: \""+ arr_gridId[i-1] +"\""
				+"});"
		);
		
	}
	
	
	
	
	<%-- if(<%=isAdmin%> == true) {
		document.getElementById("auth").value = "관리자 화면배치가 적용";
	}else{
		document.getElementById("auth").value = "일반사용자 화면배치가 적용";
	} --%>
	
}

function goCpMain() {
	location.href = "./su_main.jsp?flag=cp";
}

function goSuMain() {
	location.href = "./su_main.jsp?flag=su";
}


<%-- function resize() {
	
	<%for(int i=1;i<layoutCnt+1;i++){%>
		
		layout<%=i%>.setSizes();
		
	<%}%>
	
} --%>

</script>

<style>


/*레이아웃 패딩간격을 없앤다*/
.dhxlayout_base_dhx_web div.dhx_cell_layout div.dhx_cell_cont_layout {
	padding: 0px;
	border:none;
}
.dhxlayout_base_dhx_web div.dhx_cell_layout div.dhx_cell_hdr {
	border:none;
}

/* 동적으로 선택자 생성 */
<%for(int i=1;i<layoutCnt+1;i++){
	
	if(i==layoutCnt){
		out.print("#layoutObj"+i);
	}else{
		out.print("#layoutObj"+i+",");
	}
}
%> {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px,0px,0px,0px;
        overflow: hidden;
    }
    
.dhxlayout_base_dhx_web DIV.dhx_cell_layout DIV.dhx_cell_cont_layout{
overflow: hidden;
}
.dhxlayout_base_dhx_web DIV.dhx_cell_layout{
overflow: hidden;
}
</style>

</head>


<body onload="init();" style="margin:0; padding:0; overflow:auto;border:0px;" oncontextmenu="return false">
<!-- 동적 테이블 생성 -->
<%
int cnt = 1;

for(int i=0;i<arr_layout.length;i++){%>
	<table border="0" cellspacing="4" width="1000px" height="<%=arr_height[i]+26%>px" margin="0" padding="0">
	<tr>
		<%for(int j=0;j<arr_layout[i];j++){%>
			<td margin="0" padding="0" width="<%=arr_width[cnt-1] %>px"><div id="layoutObj<%=cnt %>" style="position: relative;"></div></td>
		<%cnt++;
			}%>
		
	</tr>
	</table>
<%}%>
</body>
</html>