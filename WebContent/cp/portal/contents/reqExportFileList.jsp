<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.code.ICodeManagement"%>
<%@ page import="com.core.component.code.ICode"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<% 
    IUser s_user = (IUser) session.getAttribute("j_user");
	Locale locale4 = null;

	if(s_user != null) {
		locale4 = s_user.getLocale();
	} else {
		String lang4 = request.getParameter("j_janguage");	//language
		
		if("ko".equals(lang4)) {
			locale4 = Locale.KOREA;
		} else if("en".equals(lang4)) {
			locale4 = Locale.US;
		} else if("zh".equals(lang4)) {
			locale4 = Locale.CHINA;
		} else if("ja".equals(lang4)) {
			locale4 = Locale.JAPAN;
		} else {
			locale4 = Locale.KOREA;
		}	
	}

	String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
	String docId = StringUtils.paramReplace(request.getParameter("docId"));
	String authority = StringUtils.paramReplace(request.getParameter("authority"));
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String maxFileCount = pm.getString("component.upload.maxCount");
	String contextPath = request.getContextPath();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>File Upload</title>
<%-- <%@ include file="../../../ext/include/include_css.jsp"%> --%>
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/portal_grid_common_render.jsp"%><%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var queryCount  = 0;

var G_SERVLETURL = "<%=contextPath%>/mg/Upload.do";

////////////////////////////////////////////////////////////////프로그래스바 처리때문에...추가
function doQueryDuring2()
{ 
	dhxWins.window("prg_win").setModal(true); //폼들을 비활성화 시킴...
	prg_win.show();
	prg_win.hide();
	prg_win.show(); 
}
function doQueryModalEnd2()
{	
	prg_win.hide();
	prg_win.show(); 
	dhxWins.window("prg_win").setModal(false); //폼을 활성화시킴
	prg_win.hide(); 
	
} 
////////////////////////////////////
// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
  	GridObj.attachEvent("onXLE", doQueryEnd);
   	doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

function doOnRowSelect(rowId,cellInd){
	
}
// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if(grid_array.length > 0) {
		
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {

	var grid_col_id = "<%=grid_col_id%>";
	var argu = "&pgmId=<%=pgmId%>&docId=<%=docId%>";

	GridObj.loadXML(G_SERVLETURL+"?mod=search&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
	if(GridObj.getRowsNum()==0){
		return;
	}
	//
	if(GridObj.getRowsNum()>=<%=maxFileCount%>){
		alert("<%=mm.getMessage("CPMG_1043",maxFileCount,s_user.getLocale())%>");
		<%-- alert("<%=mm.getMessage("CPMG_1043", locale4)%>"); --%>
	}
	parent.document.form.EXE_FILE.value="<%=pm.getString("cliptorplus.export.default.filename")%>_"+getFirstFileName()+"<%=pm.getString("cliptorplus.export.default.extension")%>";
	
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doDelete() {
	
	if(!checkRows()){
		return;
	}
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if(GridObj.getRowsNum()==1){
		parent.document.form.EXE_FILE.value="";
	}
	
	if (confirm("<%=mm.getMessage("CPMG_1027", locale4)%>")) { // 선택하신 반출파일을 삭제하시겠습니까?

		var cols_ids = "<%=grid_col_id%>";
		
		var argu = "&pgmId=" + "<%=pgmId%>";
			  argu += "&docId=" + "<%=docId%>";
	
        var SERVLETURL = G_SERVLETURL + "?mod=tx_delete&col_ids="+cols_ids+argu;	
      
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}	
}
function getFirstFileName(){
	if(GridObj.getRowsNum()==0){
		return;
	}
	var file = GridObj.cells(GridObj.getRowId(0),GridObj.getColIndexById("FILE_NAME")).getValue().replace(/&amp;/gi,"&");

	var fileName = file.slice(0,file.lastIndexOf("."));
	return fileName;
}
function getFilelist()
{
	var fileList="";
	var sep="";
	
	for(var row = 0; row < GridObj.getRowsNum(); row++)
	{	
		var tmpPath = GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById("FILE_PATH")).getValue() + "/" + GridObj.cells(GridObj.getRowId(row), GridObj.getColIndexById("FILE_NAME")).getValue().replace(/&amp;/gi,"&");
		
		fileList += sep + "<%=paramemter.getString("component.upload.directory")%>" + tmpPath;
		sep = "|";
	}
	return fileList;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	if(stage==0) {
		return true;
	} else if(stage==1) {
	} else if(stage==2) {
	    return true;
	}
	return false;
}

function doUpload() {
	
}

function uploadResult(msg){
	loadAttachFile();
	doQueryModalEnd2();
	if(msg == "SUCCESS") {
		doQuery();
	} else {
		alert("<%=mm.getMessage("CPMG_1029", locale4)%>");  // Upload 중 오류가 발생  되었습니다.
	}
}

function loadAttachFile()
{
    var argu = "pgmId=<%=pgmId%>&docId=<%=docId%>&fileCnt="+String(GridObj.getRowsNum());
    parent.uploadframe.location.href='/cp/portal/contents/fileUpload.jsp?' + argu;
	
}

function doCancel()
{
	window.close();
}

function init() {
	 setFormDraw();
	 loadAttachFile();
}



</script>
<style>
input {border:none;}
</style>
</head>
<body onload="init();" style="overflow:hidden;" oncontextmenu="return false">
<form name="form" method="post">
</form>	
<div id="gridbox" name="gridbox" width="100%" height="80%" style="background-color:white;overflow:hidden"></div>
<div class="buttonArea mt8"><%=mm.getMessage("CPMG_1043",maxFileCount,s_user.getLocale())%>
	            <ul class="btn_crud mr-6">
                  <li><a onclick="javascript:doDelete();"><span><%=mm.getMessage("CPMG_1183",locale4)%></span></a></li>
                </ul>
</div>
</body>
</html>
