<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="java.net.*"%>

<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();
String flag = StringUtils.paramReplace(request.getParameter("FLAG"));
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String beforeDay = pm.getString("cliptorplus.dashboard.period.day");
String css = pm.getString("component.ui.portal.css");
String token=null;
if("ILJIN".equals(pm.getString("component.site.company"))) {
	token = s_user.getProperty("TOKEN").toString();	
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=contextPath%>/ext/css/common.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>

<%@ include file="../../../ext/include/portal_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	 if( header_name != "FILE_ICON" || header_name != "RETRY" ) {
		 
		 // 문서등록 화면으로 이동
		 var argu_param = "?DOC_NO=" +  GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		      argu_param += "&CP_STS_CD=" + GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();
		      argu_param += "&from=CPP_0102";
		      argu_param += "&formid=CPP_0101";
			  argu_param += "&gridid=gridObj";
			  argu_param += "&callFrom=list";
		 parent.pageRedirect("/cp/portal/contents/reqExportDocInfo.jsp", argu_param);
	}
}

//Grid Row one Clieck Event 처리
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
	if( header_name == "FILE_ICON" ) {
		var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		popupAttachFile("File Download", '', '', 440, 400, "regMgmt", docId, "", "readPortal",<%=pm.getString("component.contextPath.root")%>);
	} else if( header_name == "RETRY" ) {
		var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		var sts_cd = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();
		
		if(sts_cd == "06") {
			<%-- var url = "https://eip.iljin.com/myoffice/ezApprovalConn/AutoLogin_ApvConn.aspx?UUID=<%=token%>&docid=108&pConnkey="+docId; --%>
			var url = "<%=pm.getString("component.interlock.request.url")%>/myoffice/ezApprovalConn/AutoLogin_ApvConn.aspx?UUID=<%=token%>&docid=108&pConnkey="+docId;
			centerWin(url, '', '', 'yes')
   		}   		
	}
}

function doTransDetail()
{
	if(row_id > 0) {
		 // 문서등록 화면으로 이동
		 var argu_param = "?DOC_NO=" + GridObj.cells(row_id, GridObj.getColIndexById("DOC_NO")).getValue(); 
		      argu_param += "&CP_STS_CD=" +  GridObj.cells(row_id, GridObj.getColIndexById("CP_STS_CD")).getValue();
		      argu_param += "&form=";
		 doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", argu_param);
	}
}

function OkFileUpload(fileList) {
	
}

function OkPopupDeptGrid(dept_cd, plant_cd, dept_nm) {

}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+ "<%=s_user.getId() %>";
		argu += "&DOC_TITLE="	+encodeURIComponent(document.form.DOC_TITLE.value);
		argu += "&EXE_FILE="	+encodeURIComponent(document.form.EXE_FILE.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		argu += "&IN_CL_CD=C";
		if(document.form.CP_STS_CD.options.selectedIndex != 0){
			argu += "&CP_STS_CD=" + document.form.CP_STS_CD.value;
		}
	GridObj.loadXML(G_SERVLETURL+"?mod=selectDocList&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	
	if(status == "false") {
		alert(msg);
	}
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	
   	if(stage==0) {
   		if(cellInd == 0) {
	   		var sts_cd = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();
	   		if(sts_cd == "01") {
	   			sel_change = GridObj.cells(rowId, cellInd).getValue();
	   			return true;
	   		} else {
	   			// var sts_nm = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_NM")).getValue();
	   			alert("<%=mm.getMessage("CPMG_1037", s_user.getLocale())%>"); // 완료 또는 진행중인 문서는 삭제 할 수 없습니다.
	   			return false;
	   		}
   		}
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		
	parent.mainLeft.refresh();
	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doAddRow() {

}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	for(var i=0; i < grid_array.length; i++) {
		if(GridObj.cells(grid_array[i], GridObj.getColIndexById("CP_STS_CD")).getValue() != "01") {
			alert("<%=mm.getMessage("CPMG_1037", s_user.getLocale())%>");
			return;
		}
	}
	
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteDoc&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}
//라디오버튼 클릭시 발생이벤트
function beforeDay(day){

    var ago = new Date();
    
	ago.setDate(ago.getDate() - day);	    
	
	var yy  = ago.getFullYear();
    var mm = ago.getMonth() + 1;
    var dd   = ago.getDate();
    var hh  = ago.getHours();
    var min   = ago.getMinutes();
	
	if (("" + mm).length == 1) { mm = "0" + mm; }
    if (("" + dd).length   == 1) { dd   = "0" + dd;   }
    if (("" + hh).length  == 1) { hh  = "0" + hh;  }
    if (("" + min).length   == 1) { minute   = "0" + min;   }
	
	return "" + yy + "-" + mm + "-" + dd;
	
}
function init() {	
	document.form.DOC_TITLE.focus();
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	
	setFormDraw();

	switch("<%=flag%>"){
	case "1": //모든 건수
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		doQuery();
		break;
	case "2": //승인완료 된 건수(반려,신청상태 빼고 다)
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		doQueryFinish();
		break;
	case "3": //실패(재상신)
		document.form.fromDate.value=beforeDay(<%=beforeDay%>);
		document.form.CP_STS_CD.value="06";
		doQuery();
		break;
	case "4": //읽지않음
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		document.form.CP_STS_CD.value="04";
		GridObj.setColumnHidden(GridObj.getColIndexById("RETRY"), true);
		doQueryNoRead();
		break;
	case "5"://총 반려건수
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		document.form.CP_STS_CD.value="07";
		doQuery();
		break;
	case "6"://미처리 2주이상 전에 신청 했지만 아직 대기상태
		document.form.CP_STS_CD.value="01";
		document.form.fromDate.value="";
		document.form.toDate.value=beforeDay(<%=beforeDay%>);
		doQuery();
		break;
	case "7"://승인 완료
		doQueryFinish();
		break;
	default:
		doQuery();
	}

}
function doQueryFinish() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+ "<%=s_user.getId() %>";
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
	GridObj.loadXML(G_SERVLETURL+"?mod=selectApprovalFinishList&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}
function doQueryNoRead(){
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+ "<%=s_user.getId() %>";

	GridObj.loadXML(G_SERVLETURL+"?mod=selectDocNoRead&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);		
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
</script>
</head>
<body onload="init();" oncontextmenu="return false">
<form name="form" method="post">
<!-- contents start -->
        <div class="contents">
            <!-- innerContent start -->
            <div class="innerContent">
                <!-- Page Title start -->
                <!-- <div class="title">
                    <h1>반출목록</h1>
                    <div class="titleRight">
                        <ul class="pageLocation">
                            <li><span><a href="#">Home</a></span></li>
                            <li><span><a href="#">협업문서 반출</a></span></li>
                            <li class="lastLocation"><span><a href="#">반출목록</a></span></li>
                        </ul>
                    </div>
                </div> -->
                <%@ include file="/mg/common/milestone_cpportal.jsp"%>
                <!--// Page Title end -->

                <!-- Inquiry Table (2 cases) start -->
                <div class="tableInquiry">
                    <table>
                    <caption>여러행조회</caption>
                    <colgroup>
                    <col class="col_11p" />
                    <col class="col_24p" />
                    <col />
                    <col />
                    <col class="col_11p" />
                    <col class="col_24p" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><%=mm.getMessage("CPMG_1155",s_user.getLocale())%></th>
                        <td class="tdDate">
                            <input type="text" id="fromDate" value="" class="reqInput6 w65" align="center"><img id="imgFromDate" src="../../../ext/images/<%=css%>/icon/ico_calendar.gif"> ~
                            <input type="text" id="toDate" value="" class="reqInput6 w65" align="center" ><img id="imgToDate" src="../../../ext/images/<%=css%>/icon/ico_calendar.gif">
                        </td>
                      <%-- <th><%=mm.getMessage("CPMG_1109",s_user.getLocale())%></th>
                        <td><input type="text" id="DOC_NO"/></td> --%>
                        <th><%=mm.getMessage("CPMG_1156",s_user.getLocale())%></th>
                        <td><input type="text" id="DOC_TITLE" class="w173" /></td>
                    </tr>
                    <tr>
                        <th><%=mm.getMessage("CPMG_1157",s_user.getLocale())%></th>
                        <td><input type="text" id="EXE_FILE" class="w173" /></td>
                        <th><%=mm.getMessage("SUMG_1175",s_user.getLocale())%></th>
                        <td>
                           	<jsp:include page="/mg/common/comboCode.jsp" >
								<jsp:param name="codeId" value="A0302"/>
								<jsp:param name="tagname" value="CP_STS_CD"/>
								<jsp:param name="def" value=""/>
							</jsp:include>
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                    </tbody>
                    </table>
                    <div class="tableBtnSearch"><button onclick="doQuery();"><span><%=mm.getMessage("CPMG_1164",s_user.getLocale())%></span></button></div>
                    <div class="clear"> </div>
                </div>
                <!--// Inquiry Table (2 cases) end -->

                <!-- listArea start -->
                <div class="listArea">
                    <table border="0" class="mb5">
				       <colgroup>
				           <col width="10%" />
				           <col width="90%" />
				       </colgroup>
				       <tr>
				       	<td><div class="title_num" id="totalCntTD"></div></td>
				           <td>
				            <div class="btnarea">
				           		<%-- <a href="javascript:doTransDetail();" class="btn"><span><%=mm.getMessage("CPMG_1143", s_user.getLocale())%></span></a>
				            	<a href="javascript:doSave('N');" class="btn"><span><%=mm.getMessage("CPMG_1107", s_user.getLocale())%></span></a>
				            	<a href="javascript:doSave('Y');" class="btn"><span><%=mm.getMessage("CPMG_1108", s_user.getLocale())%></span></a> --%>
				            </div>
				           </td>
				       </tr>
				    </table>
                    <div>
                           <div id="gridbox" class="cpSubGrid"></div>
                    </div>
                </div>
                <!--// listArea end -->
            </div>
            <!-- innerContent end -->
        </div>
        <!--// contents end -->
</form>
</body>
</html>