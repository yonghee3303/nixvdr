<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>   
<%@ page import="com.core.component.user.IUser"%>
<% 
	IUser s_user = (IUser) session.getAttribute("j_user");
	String managerName = (String)request.getParameter("manager"); 
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	String contextPath = request.getContextPath();
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");

%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../../ext/css/cppp_portal_<%=css %>.css" />
<%@ include file="../../../ext/include/include_css.jsp"%>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>
<title><%=mm.getMessage("WTMG_1009", s_user.getLocale())%></title>
<script>
var GridObj 		= {};
var G_SERVLETURL = "<%=contextPath%>/mg/cppp/joiningUser.do";
var managerName = "<%= managerName%>";

function init() {
	
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onRowDblClicked", function(rId,cInd){
		opener.document.getElementById("managerId").value =GridObj.cells(rId,0).getValue();
		opener.document.getElementById("manager").value=GridObj.cells(rId,1).getValue();
		closeWin();		
	});
	document.getElementById("managerNm").value = managerName;
	if(managerName!=""){
		doQuery();
	}
	document.treeFrame.location.href = "/mg/common/cpCom_dept_form.jsp";
}
function doQuery() {
	var managerNm = document.getElementById("managerNm").value;
	var dept_cd =document.getElementById("deptCd").value;
	var p_dept_cd=document.getElementById("pDeptCd").value;
	
	var grid_col_id  = "<%=grid_col_id%>";
	var argu = "&USER_NM="+encodeURIComponent(managerNm)+"&DEPT_CD="+dept_cd+"&P_DEPT_CD="+p_dept_cd;
	GridObj.loadXML(G_SERVLETURL+"?mod=searchManager&grid_col_id="+grid_col_id+argu);
	GridObj.clearAll(false); 
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	return true;
}
function deptClick(dept_cd, p_dept_cd){
	document.getElementById("deptCd").value = dept_cd;
	document.getElementById("pDeptCd").value = p_dept_cd;
	doQuery();
}
function resize() {
	GridObj.setSizes();
	
}
function closeWin(){
	self.close();
}

</script>
</head>
<body onload="init();" onresize="resize();" class="cpFindIdPassWrap" oncontextmenu="return false">
<input type="hidden" id="deptCd" value=""/>
<input type="hidden" id="pDeptCd" value=""/>
<p class="cpFindIdPassTitle"><%=mm.getMessage("WTMG_1009", s_user.getLocale())%></p>
<p class="cpSrchComTxt"><%=mm.getMessage("CPMG_1217", s_user.getLocale())%></p>
<table class="cpSrchComSearch">
	<tr>	
		<td>
			<%=mm.getMessage("CPMG_1105", s_user.getLocale())%> <input type="text" class="p_text" id="managerNm"  onkeypress="if(event.keyCode==13) {doQuery();}">
			<a href="javascript:doQuery();"><img class="search" src="../../../ext/images/<%=css%>/icon/i_search.png"></a>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td class="cpSearchManagerBox">
			<iframe name="treeFrame" id="treeFrame" title="treeFrame" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no' class="cpSubGrid3"></iframe>
		</td>
		<td class="cpSearchManagerBox2">
			<div id="gridbox" class="cpSubGrid2"></div>
		</td>
	</tr>
</table>
<table class="cpEnrollComBtn">
	<tr>
		<td>
			<div class="btnarea9">
				<a class="btn" href="javascript:closeWin();"><span><%=mm.getMessage("CPCOM_1104", s_user.getLocale())%></span></a>
			</div>
		</td>
	</tr>
</table>
</body>
</html>