<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();

IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=contextPath%>/ext/css/common.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>
<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/portal_grid_common_render.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var myDataProcessor = {};
var G_SERVLETURL ="<%=contextPath%>/mg/cppp/joiningUser.do";

function init() {
	setFormDraw();
	doQuery(); //load될 때부터 최신순으로 정렬된 모든 데이터들을 얻어온다. 
}
function setFormDraw(){
	GridObj = setGridDraw(GridObj);
	GridObj.attachEvent("onXLE",doQueryEnd);
}
function doQuery() {
	var userNm = document.getElementById("userNm").value;
	var comNm = document.getElementById("comNm").value;
	var email = document.getElementById("email").value;
	var argu = "&USER_NM="+encodeURIComponent(userNm)+"&COMPANY_NM="+encodeURIComponent(comNm)+"&EMAIL="+email+"&STAFF_ID="+"<%=s_user.getId()%>";
	var grid_col_id = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=selectUserApproval&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}
function doQueryEnd(){
	var rowsCount = GridObj.getRowsNum(); //총 갯수
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
}
function resize() {
	GridObj.setSizes();	
}
function initSearch(){ //초기화 버튼을 눌렀을 때 값들을 다 초기화 시켜준다.
	document.getElementById("userNm").value="";
	document.getElementById("comNm").value="";
	document.getElementById("email").value="";
}
//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("CPMG_1160")%>")) {  
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&STAFF_ID="+"<%=s_user.getId()%>";
		var SERVLETURL = G_SERVLETURL + "?mod=approvalUser&col_ids="+cols_ids+argu;	
		myDataProcessor = new dataProcessor(SERVLETURL);
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}
function doReturn(){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	
	if (confirm("<%=mm.getMessage("CPMG_1042", s_user.getLocale())%>")) { //반려하시겠습니까?
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
			var argu = "&STAFF_ID="+"<%=s_user.getId()%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=returnUser&col_ids="+cols_ids+argu;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}
function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}
</script>
<title>협력업체 포탈 사용 승인</title>
</head>
<body onload="init();" oncontextmenu="return false">
        <!-- contents start -->
        <div class="contents">
            <!-- innerContent start -->
            <div class="innerContent">
                <%@ include file="/mg/common/milestone_cpportal.jsp"%>
                <!--// Page Title end -->

                <!-- Inquiry Table (2 cases) start -->
                <div class="tableInquiry">
                    <table>
                    <caption>여러행조회</caption>
                    <colgroup>
                    <col class="col_10p" />
                    <col class="col_17p" />
                    <col class="col_9p" />
                    <col class="col_17p" />
                    <col class="col_10p" />
                    <col />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><%=mm.getMessage("CPMG_1161",s_user.getLocale())%></th>
                        <td><input type='text' id='userNm' value='' class="w173" onkeypress="if(event.keyCode==13) {doQuery();}"></td>
                        <th><%=mm.getMessage("CPMG_1162",s_user.getLocale())%></th>
                        <td><input type='text' id='comNm' value='' class="w173" onkeypress="if(event.keyCode==13) {doQuery();}"></td>
                        <th><%=mm.getMessage("CPMG_1163",s_user.getLocale())%></th>
                        <td><input type='text' id='email' value='' class="w173" onkeypress="if(event.keyCode==13) {doQuery();}"></td>
                    </tr>
                    </tbody>
                    </table>
                    <div class="tableBtnSearch"><button onclick="doQuery()"><span><%=mm.getMessage("CPMG_1164",s_user.getLocale())%></span></button></div>
                    <div class="clear"> </div>
                </div>
                <!--// Inquiry Table (2 cases) end -->

                <!-- listArea start -->
                <div class="listArea">
                  	<table border="0" class="mb5">
				       <colgroup>
				           <col width="10%" />
				           <col width="90%" />
				       </colgroup>
				       <tr>
				       	<td><div class="title_num" id="totalCntTD"></div></td>
				           <td>
				            <div class="btnarea">
				           		<a href="#" onclick="doReturn()" class="btn"><span><%=mm.getMessage("CPMG_1107", s_user.getLocale())%></span></a>
				            	<a href='#' onclick="doSave()" class="btn"><span><%=mm.getMessage("CPMG_1108", s_user.getLocale())%></span></a>
				            </div>
				           </td>
				       </tr>
				    </table>
                    <div>
                        <div id="gridbox" class="cpSubGrid"></div>
                    </div>
                </div>
                <!--// listArea end -->
            </div>
            <!-- innerContent end -->
        </div>
        <!--// contents end -->
        <div class="clear"> </div>
    <!--// container end -->
</body>
</html>