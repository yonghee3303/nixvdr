<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="java.net.*"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String flag = StringUtils.paramReplace(request.getParameter("FLAG"));
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String beforeDay = pm.getString("cliptorplus.dashboard.period.day");
boolean isAdmin = false;
if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
}
String contextPath =request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=contextPath%>/ext/css/common.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>

<%@ include file="../../../ext/include/portal_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
    if(header_name != "APPROVAL_MSG" && header_name != "FILE_ICON" ) {
    	var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
    	var stsCd = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();
	      
		// 문서등록 화면으로 이동
		var argu_param = "?DOC_NO=" + docId; 
		     argu_param += "&CP_STS_CD="+ stsCd;
		     argu_param += "&from=CPP_0401";
		     argu_param += "&formid=CPP_0101";
		     argu_param += "&gridid=gridObj";
		     argu_param += "&callFrom=list";
		<%-- doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", argu_param); --%>
		parent.pageRedirect("/cp/portal/contents/reqExportDocInfo.jsp", argu_param);
	}
}

// Grid Row one Clieck Event 처리
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
	if( header_name == "FILE_ICON" ) {
		var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		popupAttachFile('File Download', '', '', 440, 350, "regMgmt", docId, "", "readPortal",<%=pm.getString("component.contextPath.root")%>);
	}
}

function doTransDetail()
{
	if(row_id > 0) {
		 // 문서등록 화면으로 이동
		 var argu_param = "?DOC_NO=" + GridObj.cells(row_id, GridObj.getColIndexById("DOC_NO")).getValue(); 
		      argu_param += "&CP_STS_CD=" + GridObj.cells(row_id, GridObj.getColIndexById("CP_STS_CD")).getValue(); 
		      argu_param += "&form=approvalList";
		 doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", argu_param);
	}
}

function OkFileUpload(fileList) {
	
}

function OkPopupDeptGrid(dept_cd, plant_cd, dept_nm) {

}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&APPROVER_ID="	+ document.form.APPROVER_ID.value;
		argu += "&DOC_TITLE="	+encodeURIComponent(document.form.DOC_TITLE.value);
		argu += "&EXE_FILE="	+encodeURIComponent(document.form.EXE_FILE.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		if(document.form.CP_STS_CD.options.selectedIndex != 0){
			argu += "&CP_STS_CD=" + document.form.CP_STS_CD.value;
		}
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectApproval&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	var sts_cd = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();

   	if(stage==0) {
   		
   		if(cellInd == 0) {
	   		if(sts_cd == "01") {
	   			sel_change = GridObj.cells(rowId, cellInd).getValue();
	   			return true;
	   		} else {
	   			var sts_nm = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_NM")).getValue();
	   			// 완료 또는 진행중인 문서는 승인 할 수 없습니다."
	   			alert("<%=mm.getMessage("CPMG_1011", s_user.getLocale())%>");  
	   			return false;
	   		}
   		} else if(cellInd == 7) {  // 반려사유
   			if(sts_cd == "01") {
	   			return true;
	   		} else {
	   			return false;
	   		}
   		}
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doSave(approvalYn) {
	if(!checkRows()) return;
	
	if("N" == "<%=pm.getString("component.ui.approve.useYn")%>"){
		alert("<%=mm.getMessage("CPMG_1045", s_user.getLocale())%>");
		return;
	}
	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	var msg="", chkMsg="";
	if(approvalYn == "N") {
		msg = "<%=mm.getMessage("CPMG_1012", s_user.getLocale())%>";  // 반려
		chkMsg = "<%=mm.getMessage("CPMG_1032", s_user.getLocale())%>";  // 반려
	} else {
		msg = "<%=mm.getMessage("CPMG_1013", s_user.getLocale())%>";  // 승인
		chkMsg = "<%=mm.getMessage("CPMG_1033", s_user.getLocale())%>";  
	}
	
	if (confirm(msg)) { 
		for(var i=0; i < grid_array.length; i++) {
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("CP_STS_CD")).getValue() != "01") {
				alert("<%=mm.getMessage("CPMG_1041", s_user.getLocale())%>");
				return;
			}
			
/* 			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("APPROVAL_MSG")).getValue() == "") {
				alert(chkMsg);
				return;
			} */
		}

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
			var argu = "&APPROVAL_YN=" + approvalYn;
	        var SERVLETURL = G_SERVLETURL + "?mod=saveApproval&col_ids="+cols_ids+argu;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doAddRow() {

}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("CPMG_1013",s_user.getLocale())%>")) { //삭제하시겠습니까?

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteDoc&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function doApproverPop() {
	if(<%=isAdmin%> == true) {
		popupUser("승인자 선택", '', '', 440, 400);
	}
}

function OkPopupUser(userId, userNm) {
	document.form.APPROVER_ID.value = userId;
	document.form.APPROVER_NM.value = userNm;	
}
function beforeDay(day){

    var ago = new Date();
    
	ago.setDate(ago.getDate() - day);	    
	
	var yy  = ago.getFullYear();
    var mm = ago.getMonth() + 1;
    var dd   = ago.getDate();
    var hh  = ago.getHours();
    var min   = ago.getMinutes();
	
	if (("" + mm).length == 1) { mm = "0" + mm; }
    if (("" + dd).length   == 1) { dd   = "0" + dd;   }
    if (("" + hh).length  == 1) { hh  = "0" + hh;  }
    if (("" + min).length   == 1) { minute   = "0" + min;   }
	
	return "" + yy + "-" + mm + "-" + dd;
	
}
function init() {
	
	document.form.DOC_TITLE.focus();
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	if(<%=isAdmin%> == true) {
		document.form.APPROVER_ID.readOnly =  false;
		document.form.APPROVER_NM.readOnly  = false;
	} else {
		document.form.APPROVER_ID.readOnly =  true;
		document.form.APPROVER_NM.readOnly  = true;
	}
	
	setFormDraw();
	
	switch("<%=flag%>"){
	case "1": //모든 건수
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		doQuery();
		break;
	case "2": //총 승인완료 된 건수(반려,신청상태 빼고 다)
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		doQueryFinish();
		break;
	case "3"://총 반려건수
		document.form.fromDate.value = "";
		document.form.toDate.value = "";
		document.form.CP_STS_CD.value="07";
		doQuery();
		break;
	case "4"://미처리 2주이상 전에 신청 했지만 아직 대기상태
		document.form.CP_STS_CD.value="01";
		document.form.fromDate.value="";
		document.form.toDate.value=beforeDay(<%=beforeDay%>);
		doQuery();
		break;
	case "5": //승인대기
		document.form.fromDate.value=beforeDay(<%=beforeDay%>);
		document.form.CP_STS_CD.value="01";
		doQuery();
		break;
	case "6": //승인건수
		document.form.fromDate.value=beforeDay(<%=beforeDay%>);
		doQueryFinish();
		break;
	default:
		doQuery();
	}
	if("N" == "<%=pm.getString("component.ui.approve.useYn")%>"){
		document.getElementById("ApprovalBtn").style.visibility="hidden";
	}
}
function doQueryFinish() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&APPROVER_ID="	+ document.form.APPROVER_ID.value;
	argu += "&FROM_YMD=" + document.form.fromDate.value;	
	argu += "&TO_YMD=" + document.form.toDate.value;
	if(document.form.CP_STS_CD.options.selectedIndex != 0){
		argu += "&CP_STS_CD=" + document.form.CP_STS_CD.value;
	}
	GridObj.loadXML(G_SERVLETURL+"?mod=selectRequestFinishList&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}
</script>
</head>
<body onload="init();" oncontextmenu="return false">
<form name="form" method="post">
<input type="hidden" name="APPROVER_ID" value="<%=s_user.getId()%>">
<input type="hidden" name="APPROVER_NM" value="<%=s_user.getName()%>">
<!-- contents start -->
        <div class="contents">
            <!-- innerContent start -->
            <div class="innerContent">
                <%@ include file="/mg/common/milestone_cpportal.jsp"%>
                <!-- Inquiry Table (2 cases) start -->
                <div class="tableInquiry">
                    <table>
                    <caption>여러행조회</caption>
                    <colgroup>
                    <col class="col_12p" />
                    <col class="col_23p" />
                    <col class="col_12p" />
                    <col class="col_23p" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th><%=mm.getMessage("CPMG_1155",s_user.getLocale())%></th>
                        <td class="tdDate">
							<input type="text" id="fromDate" value="" class="reqInput6 w65" align="center"><img id="imgFromDate" src="../../../ext/images/<%=css%>/icon/ico_calendar.gif"> ~
                            <input type="text" id="toDate" value="" class="reqInput6 w65" align="center" ><img id="imgToDate" src="../../../ext/images/<%=css%>/icon/ico_calendar.gif">
                        </td>
						<%-- <th><%=mm.getMessage("CPMG_1109",s_user.getLocale())%></th>
                        <td><input type="text" id="DOC_NO"/></td> --%>
                        <th><%=mm.getMessage("CPMG_1156",s_user.getLocale())%></th>
                        <td><input type="text" id="DOC_TITLE" class="w173" /></td>
                    </tr>
                    <tr>
                        <th><%=mm.getMessage("CPMG_1157",s_user.getLocale())%></th>
                        <td><input type="text" id="EXE_FILE" class="w173" /></td>
                        <th><%=mm.getMessage("SUMG_1175",s_user.getLocale())%></th>
                        <td>
            				<jsp:include page="/mg/common/comboCode.jsp" >
								<jsp:param name="codeId" value="A0302"/>
								<jsp:param name="tagname" value="CP_STS_CD"/>
								<jsp:param name="def" value=""/>
							</jsp:include>
                        </td>
                    </tr>
                    </tbody>
                    </table>
                    <div class="tableBtnSearch"><button onclick="doQuery();"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></button></div>
                    <div class="clear"> </div>
                </div>
                <!--// Inquiry Table (2 cases) end -->

                <!-- listArea start -->
                <div class="listArea">
                	<table border="0" class="mb5" id="ApprovalBtn" style="visibility:visible;">
				       <colgroup>
				           <col width="10%" />
				           <col width="90%" />
				       </colgroup>
				       <tr>
				       	<td><div class="title_num" id="totalCntTD"></div></td>
				           <td>
				            <div class="btnarea">
				            	<a href="#" onclick="doSave('N');" class="btn"><span><%=mm.getMessage("CPMG_1107", s_user.getLocale())%></span></a>
				            	<a href="#" onclick="doSave('Y');" class="btn"><span><%=mm.getMessage("CPMG_1108", s_user.getLocale())%></span></a>
				            </div>
				           </td>
				       </tr>
				    </table>
                    <div>
                           <div id="gridbox" class="cpSubGrid"></div>
                    </div>
                </div>
                <!--// listArea end -->
            </div>
            <!-- innerContent end -->
        </div>
        <!--// contents end -->
</form>
</body>
</html>