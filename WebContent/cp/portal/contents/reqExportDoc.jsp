<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="java.net.*"%>
<%
	IUser s_user = (IUser) session.getAttribute("j_user");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  	String deptCd = s_user.getProperty("DEPT_CD").toString();
 	String deptNm = s_user.getProperty("DEPT_NM").toString();
  	String serverDay = s_user.getProperty("TODAY").toString();
  	String exportUser = pm.getString("component.export.user");
  	String contextPath = request.getContextPath();
  	String position="";
  	String token=null;
  	
  	if(s_user.getProperty("POSITION_NM")== null && s_user.getProperty("FUNCTION_NM")== null) {
  		position = "unknown";
  	} else {
  		if(s_user.getProperty("POSITION_NM")== null){
  			position = s_user.getProperty("FUNCTION_NM").toString();
  		} else{
  			position = s_user.getProperty("POSITION_NM").toString();
  		}
  	}
	if("ILJIN".equals(pm.getString("component.site.company"))) {
		token = s_user.getProperty("TOKEN").toString();	
	}
	String css = pm.getString("component.ui.portal.css");
	String cpVersion = pm.getString("component.site.product.version");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- css수정필요 -->
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/portal_grid_common_render.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../../ext/js/doAjaxCombo.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var userType		= "approver";
//등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var jDataPolicy;
var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

function init(){
	// Exe 생성번호 취득
	doAjaxGetCpDocNo(); //doAjaxCombo.js에 되어있다. doAjaxCpDocNo()에서 문서번호를 화면에 보여준다.
	if("<%=pm.getString("cliptorplus.export.request.noApprover")%>" != "Y") {
		doGetApprover(); //승인자 얻어오기
	}
	document.form.EXE_RULE.value="USER Creation";
	document.form.DOC_TITLE.focus();
	setFormDraw();	//그리드 그리기
	doPolicyCombo(); //콤보박스 생성
}
//SYSUSERAPPROVER에서 APPROVER_ID를 얻어와 SYSUSER에서 승인자 USER_ID를 구한다.
function doGetApprover() {
	<%if("personal".equals(pm.getString("component.export.approver"))){%>
		var url =G_SERVLETURL+"?mod=getUserApprover&USER_ID=<%=s_user.getId()%>";
		sendRequest(doGetApproverEnd, "", 'POST', url , false, false);
	<%} else{%>
			document.form.APPROVER_NM.value="<%=s_user.getProperty("MANAGER_NM").toString()%>";
			document.form.APPROVER_ID.value="<%=s_user.getProperty("MANAGER_ID").toString()%>";
	<%} if("Y".equals(pm.getString("component.ui.approver.searchYn"))){%>
			document.getElementById("searchApprover_a").style.display ="inline";
			document.getElementById("searchApprover_img").style.display ="inline";
			document.getElementById("searchApprover_img").style.margin = "0 0 3px -5px";
	<%} if("Y".equals(s_user.getProperty("SELF_EXP_YN").toString())){%>
			document.form.APPROVER_NM.value="<%=s_user.getProperty("USER_NM").toString()%>";
			document.form.APPROVER_ID.value="<%=s_user.getId().toString()%>";
			document.getElementById("selfApprover").innerText="<%=mm.getMessage("CPMG_1233", s_user.getLocale())%>";
	<%}%>
}
function doGetApproverEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	var approverId = jData.APPROVER_ID;
	var approverNm = jData.APPROVER_NM;
	document.form.APPROVER_NM.value=approverNm;
	document.form.APPROVER_ID.value=approverId;
	
}
function cpGrpChange(){
	var policyNo = document.form.policyCode.value;
	var url =G_SERVLETURL+"?mod=getPolicyGrp&POLICY_NO="+policyNo;
	sendRequest(cpGrpChangeEnd, "", 'POST', url , false, false);
}
function cpGrpChangeEnd(oj){
	jDataPolicy = JSON.parse(oj.responseText);
	
	var policyNo = document.form.policyCode.value;
	var policyCd = jDataPolicy.POLICY_CL_CD;
	var policy = jDataPolicy.POLICY;
	var policyDoc = jDataPolicy.POLICY_DOC;
	var policyDisp = jDataPolicy.POLICY_DISP;
	var expireDate = jDataPolicy.EXPIRE_DATE;
	var readCnt = jDataPolicy.READ_COUNT;
	var macYn = jDataPolicy.MAC_YN;
	var outdevice = jDataPolicy.OUTDEVICE.split(",");
	var outRadio =document.form.OUTDEVICE;
	document.form.readCnt.value=readCnt;
	var editCd = jDataPolicy.USEREDIT_CD;
	
	var exportTypeRadio = document.form.exportType;
	for(var i=0; i < exportTypeRadio.length; i++){
		exportTypeRadio[i].checked=false;
    }
	for(var i=0; i < exportTypeRadio.length; i++){
		if(exportTypeRadio[i].value==policyCd){
			exportTypeRadio[i].checked=true;
		}
    }
 	
 	if(policy.substring(1,2) == "1") {
 		document.form.chkExpireDt.checked = true;
 	} else {
 		document.form.chkExpireDt.checked = false;
 		expireDate="";
 	}
 	document.form.expireDt.value = expireDate;

 	if(policy.substring(2,3) == "1") {
 		document.form.chkExe.checked = true;
 	} else {
 		document.form.chkExe.checked = false;
 	}
 		
 		
	/* 	if(policy.substring(3,4) == "1") {
			document.form.chkDocAuth.checked = true;
		} else {
			document.form.chkDocAuth.checked = false;
		}
		 */
		
		/* if(policy.substring(4,5) == "1") {
			document.form.chkLogTracking.checked = true;
		} else {
			document.form.chkLogTracking.checked = false;
		} */
		// 문서권한
	/* 	if(policyDoc.substring(0,1) == "1") {
		document.form.chkSave.checked = true;
	} else {
		document.form.chkSave.checked = false;
	} */
		if(policyDoc.substring(1,2) == "1") {
			document.form.chkPrint.checked = true;
		} else {
			document.form.chkPrint.checked = false;
		}
		if(policyDoc.substring(2,3) == "1") {
			document.form.chkClipCopy.checked = true;
		} else {
			document.form.chkClipCopy.checked = false;
		}
		if(policyDoc.substring(3,4) == "1") {
			document.form.chkRepacking.checked = true;
		} else {
			document.form.chkRepacking.checked = false;
		}
		if(policyDoc.substring(4,5) == "1") {
			document.form.chkExtCopy.checked = true;
		} else {
			document.form.chkExtCopy.checked = false;
		}
			
		if(macYn =="Y"){
			document.form.chkMac.checked = true;
		} else {
			document.form.chkMac.checked = false;
		}
		
	if(policyCd=="1"){	//서버인증 보안반출
		document.form.AUTHORITY.value="4";
		document.getElementById("setPolicy1").className="blockDisplay";
		//document.getElementById("setPolicy2").className="blockDisplay";
		document.getElementById("setMac").className ="blockDisplay";
		document.getElementById("setDevice").className="Lnodisplay";
		document.form.OUTDEVICE_01.checked=false;
		document.form.OUTDEVICE_02.checked=false;
		document.form.OUTDEVICE_03.checked=false;
	}else if(policyCd=="2"){ //원문반출
		document.form.AUTHORITY.value="6";
		document.getElementById("setPolicy1").className="Lnodisplay";
		//document.getElementById("setPolicy2").className="Lnodisplay";
		document.getElementById("setMac").className ="Lnodisplay";
		document.getElementById("setDevice").className="blockDisplay";
	
		for(var i=0; i < outRadio.length; i++){
			for(var j=0; j <outdevice.length; j++){
				if(outRadio[i].value==outdevice[j]){
					outRadio[i].checked=true;
				}
			}
	    }
	}else if(policyCd=="3"){ //자체인증 보안반출
		document.form.AUTHORITY.value="3";
		document.getElementById("setPolicy1").className="blockDisplay";
		//document.getElementById("setPolicy2").className="blockDisplay";
		document.getElementById("setMac").className ="blockDisplay";
		document.getElementById("setDevice").className="Lnodisplay";
		document.form.OUTDEVICE_01.checked=false;
		document.form.OUTDEVICE_02.checked=false;
		document.form.OUTDEVICE_03.checked=false;
	}
		
	//정책변경 불가
	document.form.chkMac.disabled = true;
	document.form.chkExe.disabled = true;
	document.form.chkExpireDt.disabled = true;
/* 	document.form.chkNetwork.disabled = true; */
	document.form.chkPrint.disabled = true;
	document.form.chkClipCopy.disabled = true;
	document.form.chkRepacking.disabled = true;
	document.form.chkExtCopy.disabled = true;
	for(var i=0; i < outRadio.length; i++){
		outRadio[i].disabled=true;
    }
	for(var j=0; j < exportTypeRadio.length; j++ ){
		exportTypeRadio[j].disabled=true;
	}

	switch(editCd){
	case "0": //수정허용하지 않음
		document.form.expireDt.readOnly = true;
		document.form.expireDt.className="w60 mr164 disableInput";
		document.form.readCnt.readOnly = true;
		document.form.readCnt.className="w50 reqInput5 disableInput";
		break;
	case "1": //인증횟수 수정허용
		document.form.expireDt.readOnly = true;
		document.form.expireDt.className="w60 mr164 disableInput";
		document.form.readCnt.readOnly = false;
		document.form.readCnt.className="w50 reqInput5";
		break;
	case "2": //만료일자 수정허용
		document.form.expireDt.readOnly = false;
		document.form.expireDt.className="w60 mr164";
		document.form.readCnt.readOnly = true;
		document.form.readCnt.className="w50 reqInput5 disableInput";
		break;
	case "8": //인증횟수/만료일자 수정허용
		document.form.expireDt.readOnly = false;
		document.form.expireDt.className="w60 mr164";
		document.form.readCnt.readOnly = false;
		document.form.readCnt.className="w50 reqInput5";
		break;
}
	
	setPolicyAllGrid(policy, policyDoc, expireDate, readCnt, policyNo, policyDisp,jDataPolicy.OUTDEVICE);
	
}
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	//GridObj.setSizes();
}
function doAjaxCpDocNo(docNo) {
	document.form.DOC_NO.value = docNo;
	filelistframe.location.href="reqExportFileList.jsp?formid=CPP_0103&gridid=GridObj&pgmId=regMgmt&docId="+docNo+"&authority=4";
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	//goPopupPolicy(rowId, "grid");
	//row_id = rowId;
}
function doOnRowSelect(rowId, cellInd) {
	
}
function doOnCellChange(){
	
}
function doUserPop() {
	popupCompUser_portal("<%=mm.getMessage("CPMG_1140", s_user.getLocale())%>", '', '', 500, 450, "<%=exportUser%>", "", <%=pm.getString("component.contextPath.root")%>);
}
//정책이 변경되면 그리드의 숨겨진 컬럼에 추가 각각 수신자 별 정책을 따로 심을 경우도 생각해서... 현재 체크된 사용자에게만 그 정책을 심음
function setPolicyGrid(policy, policyDoc, expireDate, readCnt, policyNo, policyDis,outdevice){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	for(var i=0; i < grid_array.length; i++) {
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY")).setValue(policy);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_DOC")).setValue(policyDoc);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDate);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("READ_COUNT")).setValue(readCnt);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_NO")).setValue(policyNo);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_DISP")).setValue(policyDis);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("OUTDEVICE")).setValue(outdevice);
		//grid_arry[i]에는 선택된 rowId값이 들어가있다.
	}
}
//일괄정책 심기
function setPolicyAllGrid(policy, policyDoc, expireDate, readCnt, policyNo, policyDis,outdevice){
	for(var i=1; i <= GridObj.getRowsNum(); i++) {
		GridObj.cells(i, GridObj.getColIndexById("POLICY")).setValue(policy);
		GridObj.cells(i, GridObj.getColIndexById("POLICY_DOC")).setValue(policyDoc);
		GridObj.cells(i, GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDate);
		GridObj.cells(i, GridObj.getColIndexById("READ_COUNT")).setValue(readCnt);
		GridObj.cells(i, GridObj.getColIndexById("POLICY_NO")).setValue(policyNo);
		GridObj.cells(i, GridObj.getColIndexById("POLICY_DISP")).setValue(policyDis);
		GridObj.cells(i, GridObj.getColIndexById("OUTDEVICE")).setValue(outdevice);
	}
}
//수정된 정책 반영 
function setEditPolicyAllGrid(expireDate, readCnt){
	for(var i=1; i <= GridObj.getRowsNum(); i++) {
		GridObj.cells(i, GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDate);
		GridObj.cells(i, GridObj.getColIndexById("READ_COUNT")).setValue(readCnt);
	}
}
function OkPopupCompUser(row_id, user_id, user_nm, company_id,company_nm, email, position, tel_no, macAddr) {
    var nMaxRow2 = parseInt(row_id);
	<%-- var row_data = "<%=grid_col_id%>"; --%>
	
		for(var i = 0; i< GridObj.getRowsNum(); i++) {			
			var compUser = GridObj.cells2(i, GridObj.getColIndexById("USER_ID")).getValue();
			if( user_id ==  compUser ) {
				alert( "[" + compUser + "] <%=mm.getMessage("CPMG_1039", s_user.getLocale())%>");    // 이미 등록된 수신자 입니다.
				return;
			}
		}
		dhtmlx_last_row_id++;
		nMaxRow2 = dhtmlx_last_row_id;
		GridObj.enableSmartRendering(true);
		GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));


  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = false;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_ID")).setValue(user_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_NM")).setValue(user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_ID")).setValue(company_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_NM")).setValue(company_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EMAIL")).setValue(email);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POSITION_NM")).setValue(position);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TEL_NO")).setValue(tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAC_LIST")).setValue(macAddr);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MOD_STOP")).setValue("0");	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY")).setValue(jDataPolicy.POLICY);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_DOC")).setValue(jDataPolicy.POLICY_DOC);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXPIRE_DT")).setValue(jDataPolicy.EXPIRE_DATE);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("READ_COUNT")).setValue(jDataPolicy.READ_COUNT);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_NO")).setValue(document.form.policyCode.value);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_DISP")).setValue(jDataPolicy.POLICY_DISP);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("OUTDEVICE")).setValue(jDataPolicy.OUTDEVICE);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;

	ed_flag = "Y";
	
}
function validationFileName(checkStr){ //보안문서로 생성할 파일명에 \ / : * ? " < > | 가 포함되서는 안된다.
	var banStrArr = ["\\","/",":","*","?","\"","<",">","|"];
	for(var i=0; i < banStrArr.length; i++){
		if(checkStr.indexOf(banStrArr[i])>-1){
			return false;
		}
	}
	return true;
}
function doSave() {
	
	var expireDate=document.form.expireDt.value; 
	var readCnt=document.form.readCnt.value;
	
	document.form.FILE_LIST.value = filelistframe.getFilelist();
	cpGrpChange();
	setEditPolicyAllGrid(expireDate,readCnt);
	document.form.expireDt.value=expireDate; 
	document.form.readCnt.value=readCnt;
	
	if(document.form.DOC_TITLE.value == "") {  // "문서명을 입력하여 주십시요."
		alert("<%=mm.getMessage("CPMG_1015", s_user.getLocale())%>");
		return;
	}
	
	if(document.form.FILE_LIST.value == "") {  //반출할 파일을 Upload 해 주십시요.
		alert("<%=mm.getMessage("CPMG_1017", s_user.getLocale())%>");
		return;
	}
	
	//50개 이상의 파일을 POST 방식으로 전달하기 위한 수정
	for(var i=1; i <= GridObj.getRowsNum(); i++) {
		GridObj.cells(i, GridObj.getColIndexById("FILE_LIST")).setValue(encodeURIComponent(document.form.FILE_LIST.value));
	}
	
	// if(document.form.OUT_CL_CD[0].checked) {   // 원본반출시에만 파일명 입력
		if(document.form.EXE_FILE.value == "") { // 파일명을 입력하여 주십시요.
			alert("<%=mm.getMessage("CPMG_1016", s_user.getLocale())%>");
			return;
		}
	// }	
	
	if(document.form.DOC_MSG.value == "") { // 반출 사유를 입력 해 주십시요.
		alert("<%=mm.getMessage("CPMG_1019", s_user.getLocale())%>");
		return;
	}
	
	for(var i = 0; i< GridObj.getRowsNum(); i++) {
		GridObj.cells(GridObj.getRowId(i), GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
		GridObj.cells(GridObj.getRowId(i), GridObj.getColIndexById("SELECTED")).setValue("1");
	}
	
	//if(document.form.AUTHORITY.value != "6") { // 수신자를 등록 해 주십시요.
		if(!checkRows()) {
			alert("<%=mm.getMessage("CPMG_1018", s_user.getLocale())%>");
			return;
		}
	//}
	if(jDataPolicy.MAC_YN =="N"){
		for(var i=1; i <= GridObj.getRowsNum(); i++) {
			GridObj.cells(i, GridObj.getColIndexById("MAC_LIST")).setValue("");
		}
	}
	
	var createType="E";
	
	if("2" =="<%=cpVersion%>"){
		var createTypeRadio = document.form.createType;
		for(var i=0; i < createTypeRadio.length; i++){
			if(createTypeRadio[i].checked) createType=createTypeRadio[i].value;
    	}
	}
	
	if(createType == "D"){
		var exeFile = document.form.EXE_FILE.value;
		var extension = exeFile.substring(exeFile.lastIndexOf('.')+1, exeFile.len);
		extension = extension.toLowerCase();

		if("."+extension == "<%=pm.getString("cliptorplus.export.default.extension")%>"){
			document.form.EXE_FILE.value = document.form.EXE_FILE.value.replace("<%=pm.getString("cliptorplus.export.default.extension")%>", ".cpd"); 
		}else if(extension != "cpd"){
			document.form.EXE_FILE.value = document.form.EXE_FILE.value+".cpd"; 
		}
	}
	
	if(document.form.AUTHORITY.value == "6"){
		document.form.EXE_FILE.value = document.form.EXE_FILE.value.replace("<%=pm.getString("cliptorplus.export.default.extension")%>", ".zip"); 
	}
	if(!validationFileName(document.form.EXE_FILE.value)){//파일명에 \ / : * ? " < > | 를 포함하여 생성할 수 없습니다. 파일명을 수정해주세요.
		alert("<%=mm.getMessage("CPMG_1060", s_user.getLocale())%>");
		return;
	}
	var grid_array = getGridChangedRows2(GridObj, "SELECTED");

	 if (confirm("<%=mm.getMessage("CPMG_1020", s_user.getLocale())%>")) {
		var fileList = document.form.FILE_LIST.value;
		var filn = fileList.split("|");
	 	
		var url = "&DOC_NO=" + document.form.DOC_NO.value; 		
			url += "&DOC_TITLE=" + encodeURIComponent(document.form.DOC_TITLE.value); 
		 	url += "&USER_ID=" + document.form.USER_ID.value;
			url += "&EXE_RULE=" + encodeURIComponent(document.form.EXE_RULE.value);  
		 	url += "&AUTHORITY=" + document.form.AUTHORITY.value;
			url += "&CREATOR_EMAIL=" + encodeURIComponent(document.form.CREATOR_EMAIL.value);
			url += "&WAITING_TIME=" + document.form.WAITING_TIME.value; 
			url += "&IN_PROCESS=" + encodeURIComponent(document.form.IN_PROCESS.value); 
			url += "&FILE_NUM=" + filn.length;
			//url += "&FILE_LIST=" + encodeURIComponent(document.form.FILE_LIST.value);
			url += "&P_DOC_NO=" ;/* + document.form.P_DOC_NO.value; */
			url += "&EXE_FILE=" + encodeURIComponent(document.form.EXE_FILE.value);
			url += "&DOC_MSG=" + encodeURIComponent(document.form.DOC_MSG.value);
			url += "&OUT_CL_CD=01";
			if("<%=pm.getString("cliptorplus.export.request.noApprover")%>" != "Y") {
				url += "&APPROVER_ID=" + encodeURIComponent(document.form.APPROVER_ID.value);
			}
			if("2" == "<%=cpVersion%>"){ 
				url+="&CRE_TYPE="+createType;
			}
			
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=saveRequest&col_ids="+cols_ids+url;
			myDataProcessor = new dataProcessor(SERVLETURL);
					
			sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
			 
	}
}  
function doSaveEnd(obj) {
	var message = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");	
	
	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		if("<%=pm.getString("component.site.company")%>" == "ILJIN") {
			var docNo = document.form.DOC_NO.value;
			<%-- var url = "https://eip.iljin.com/myoffice/ezApprovalConn/AutoLogin_ApvConn.aspx?UUID=<%=token%>&docid=108&pConnkey="+docNo; --%>
			var url = "<%=pm.getString("component.interlock.request.url")%>/myoffice/ezApprovalConn/AutoLogin_ApvConn.aspx?UUID=<%=token%>&docid=108&pConnkey="+docNo;
			
			centerWin(url, '', '', 'yes')
			
			//재상신 버튼이 있는 일진 전용페이지로 이동하게 변경해아함
			parent.mainLeft.init();
			parent.pageRedirect("/cp/portal/contents/reqExportList_Iljin.jsp","?formid=CPP_0104&gridid=GridObj");
		} else {
			// 문서목록 화면으로 이동
			parent.mainLeft.init();
			parent.pageRedirect("/cp/portal/contents/reqExportList.jsp","?formid=CPP_0102&gridid=GridObj");	
		}
	} else {
		alert(message);
	}
	return false;
}
//체크가 된 목록이 있나 확인
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
//추가한 사용자 중 체크된 사용자만 삭제하기 
function doDelete() {
	if(!checkRows()) return;
	GridObj.enableSmartRendering(false);
	var grid_array = getGridChangedRows2(GridObj, "SELECTED"); //선택된 목록의 rowId값을 배열로 가져옴.

	//삭제하시겠습니까?
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { 

		for(var i=0; i < grid_array.length; i++) {
			GridObj.deleteRow(grid_array[i]);
			// dhtmlx_last_row_id--;
		}		
	}
}
//policy combo 동적으로 생성
function doPolicyCombo() { 
	<%if("Y".equals(pm.getString("cliptorplus.export.isExeOnly"))){%>
	var url = "/mg/cpgrp.do?mod=getComboCpGrpHtml&cl_cd=13";
	<%}else{%>
	var url = "/mg/cpgrp.do?mod=getComboCpGrpHtml";
	<%}%>
	sendRequest(doAjaxResult, "", 'POST', url , false, false);
}

function doAjaxResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyCombo = document.getElementById("comboCpGrp");
	policyCombo.innerHTML = oj.responseText;//jData;
	document.form.policyCode.value ="<%=pm.getString("cliptorplus.export.default.policyCd")%>";
	//LG화학에서 기본 정책만 사용
	<%if(pm.getBoolean("component.ui.cpPolicyCode.isReadOnly")){%>
		document.form.policyCode.disabled=true;
	<%}%>
	cpGrpChange(); //정책그룹 읽어서 뿌려주기
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
function doApproverPop(){
	centerWin("/cp/portal/contents/searchApprover.jsp?manager="+encodeURIComponent(document.getElementById("manager").value)+"&formid=CPCOM_0402&gridid=GridObj",850,530,"yes");
}

//DB 저장시 글자수 제한을 위한 함수, Ascii 코드를 넘어가는 값은 모두 3byte 로 처리한다.
//element : elementById
//limit : DB 컬럼내 제한량
function len_chk(element, limit) {
	
	if('Y' == '<%=pm.getString("component.ui.input.limit")%>') {
		
		var frm = document.getElementById(element);
		var cnt = 0; 
		
		for(var i=0; i<frm.value.length; i++) {
			var chr = frm.value.substring(i,i+1);
			var numUnicode = chr.charCodeAt(0);	//number of the decimal Unicode
			
			if(numUnicode >= 0 && numUnicode <= 127) {
				cnt += 1;	
			} else {
				cnt += 3;
			}
			if(cnt > limit) {
				alert('<%=mm.getMessage("CPMG_1054",s_user.getLocale())%>');
				frm.value = frm.value.substring(0,i);
				frm.focus();
				break;
			}
		}
	}
}
</script>
<title></title>
</head>
<body onload="init();" oncontextmenu="return false">
<form name="form" method="post">

<input type="hidden" name="DOC_NO" value="">
<input type="hidden" name="EXE_RULE" value="">
<input type="hidden" name="FILE_LIST" value="">
<input type="hidden" name="AUTHORITY" value="4">
<input type="hidden" name="WAITING_TIME" size="10" value="10" class="text">
<input type="hidden" name="IN_PROCESS" size="46" class="text" style="IME-MODE:disabled">
<input type="hidden" name="P_DOC_NO" size="46" class="text">
<input type="hidden" name="CREATOR_EMAIL" value="<%=s_user.getProperty("EMAIL").toString() %>">

<div class="contents">
  <!-- innerContent start -->
  <div class="innerContent">
    <%@ include file="/mg/common/milestone_cpportal.jsp"%>
    <!--// Page Title end -->
    <!-- listArea start -->
    <div class="listArea">
      <!-- tableArea start -->
      <div class="tableArea">
        <h2 class="subtitle"><%=mm.getMessage("CPMG_1174",s_user.getLocale())%></h2>
        <!-- Table start -->
        <div class="table">
          <table class="tableGeneral">
          <caption>반출정보</caption>
          <colgroup>
          <col class="col_13p" />
          <col class="col_37p" />
          <col class="col_13p" />
          <col class="col_37p" />
          </colgroup>
          <tbody>
          <tr>
            <th><%=mm.getMessage("CPMG_1161",s_user.getLocale())%></th>
            <td><%=s_user.getName()%><input type="hidden" name="USER_NM" value="<%=s_user.getName()%>"/></td>
            <th class="th02"><%=mm.getMessage("CPMG_1175",s_user.getLocale())%></th>
            <td class="td02"><%=s_user.getId()%><input type="hidden" name="USER_ID" value="<%=s_user.getId()%>"/></td>
          </tr>
          <tr>
            <th><%=mm.getMessage("CPMG_1177",s_user.getLocale())%></th>
            <td><%=deptNm%><input type="hidden" name="USER_NM" value="<%=deptNm%>"/></td>
            <th class="th02"><%=mm.getMessage("CPMG_1176",s_user.getLocale())%></th>
            <td class="td02"><%=position%><input type="hidden" name="USER_NM" value="<%=position%>"/></td>
          </tr>
          <tr>
            <th><%=mm.getMessage("CPMG_1178",s_user.getLocale())%></th>
            <td colspan="3">
              <input type="text" name="DOC_TITLE" class="reqInput2" onKeyup="len_chk('DOC_TITLE', 256);"/>
            </td>
          </tr>
          <%
          if(!"Y".equals(pm.getString("cliptorplus.export.request.noApprover"))) {
          %>
          <tr>
            <th><%=mm.getMessage("CPMG_1105",s_user.getLocale())%></th>
            <td colspan="3">
             <input type="text" id="manager" name="APPROVER_NM" class="fixedWidth" size="9" value="" readOnly="true"/>
             <a onclick="doApproverPop();" id="searchApprover_a" style="display:none;"><img class="inquiryBtn" id="searchApprover_img" src="/ext/images/<%=css %>/icon/ico_inline_inquiry.gif" alt="<%=mm.getMessage("COMG_1043",s_user.getLocale())%>" style="display:none;"/></a>             
             <input type="hidden" id="managerId" name="APPROVER_ID" value=""/><font id="selfApprover" class="selfApproverText"></font>
            </td>
          </tr>
          <%
          }
          %> 
          <tr>
            <th><%=mm.getMessage("CPMG_1179",s_user.getLocale())%></th>
            <td colspan="3">
              <textarea name="DOC_MSG" id="textarea01" cols="20" rows="3" onKeyup="len_chk('DOC_MSG', 256);"> </textarea>
            </td>
          </tr>
          </table>
        </div>
        <!--// Table end -->
      </div>
      <!--// tableArea end -->
      <!-- tableArea start -->
      <div class="tableArea">
        <h2 class="subtitle"><%=mm.getMessage("CPMG_1180",s_user.getLocale())%></h2>
        <!-- Table start -->
        <div class="table">
          <table class="tableGeneral">
          <caption><%=mm.getMessage("CPMG_1180",s_user.getLocale())%></caption>
          <colgroup>
          <col class="col_13p" />
          <col />
          </colgroup>
          <tbody>
          <tr>
            <th><%=mm.getMessage("CPMG_1181",s_user.getLocale())%></th>
            <td>
              <input id="input_exp05" class="reqInput1" type="text" size="9" onkeypress="if(event.keyCode==13) {doUserPop();}"/><a onclick="doUserPop();"><img class="inquiryBtn" src="/ext/images/<%=css %>/icon/ico_inline_inquiry.gif" alt="<%=mm.getMessage("COMG_1043",s_user.getLocale())%>"/></a>
            </td>
          </tr>
          <tr>     
            <th><%=mm.getMessage("CPMG_1182",s_user.getLocale())%></th>
            <td>
              <div id="gridbox" class="cpSubGrid2"></div>
              <div class="buttonArea mt8">
                <ul class="btn_crud mr-6">
                  <li><a onclick="doDelete()"><span><%=mm.getMessage("CPMG_1183",s_user.getLocale())%></span></a></li>
                </ul>
              </div>
            </td>
          </tr>
          </table>
        </div>
        <!--// Table end -->
      </div>
      <!--// tableArea end -->
      <!-- tableArea start -->
      <div class="tableArea">
        <h2 class="subtitle"><%=mm.getMessage("CPMG_1184",s_user.getLocale())%></h2>
        <!-- Table start -->
        <div class="table">
          <table class="tableGeneral">
          <caption><%=mm.getMessage("CPMG_1184",s_user.getLocale())%></caption>
          <colgroup>
          <col class="col_13p" />
          <col />
          <col />
          </colgroup>
          <tbody>
          <tr>
            <th><%=mm.getMessage("CPMG_1186",s_user.getLocale())%></th>
            <td colspan="2">
              <iframe id="uploadframe" name="uploadframe" class="cpSubGrid3" frameborder="0" scrolling="no"></iframe>
            </td>
          </tr>
          <tr>
          	<th><%=mm.getMessage("CPMG_1187",s_user.getLocale())%></th>
            <td colspan="2" style="padding-bottom:5px;">
          	  <iframe id="filelistframe" name="filelistframe" class="cpSubGrid4" frameborder="0" scrolling="auto"></iframe>
          	</td>
          </tr>
           <tr>
            <th><%=mm.getMessage("CPMG_1157",s_user.getLocale())%></th>
            <td colspan="2"><input type="text" class="reqInput2" name="EXE_FILE" /><span><%=mm.getMessage("CPMG_1185",s_user.getLocale())%></span>
            </td>
          </tr>
         <%
          if("2".equals(cpVersion)){ %>      
          <tr>
          	<th>생성 타입</th>
          	<td><input type="radio" name="createType" value="E" checked> EXE 파일</td>
          	<td><input type="radio" name="createType" value="D"> CPD 파일</td>
          </tr>
         <%} %>
          <tr>
               <th><%=mm.getMessage("CPMG_1188",s_user.getLocale())%></th>
                <td colspan="2">
                	<font id="comboCpGrp"></font>
                </td>
          </tr>
          <tr>
                <th rowspan="4"><%=mm.getMessage("CPMG_1189",s_user.getLocale())%></th>
                <%-- <td id="setPolicy1" class="blockDisplay"><input type="checkbox" name="chkExtCopy"/><%=mm.getMessage("CPMG_1190",s_user.getLocale())%><br /><input type="checkbox" name="chkClipCopy"/><%=mm.getMessage("CPMG_1192",s_user.getLocale())%><br /><input type="checkbox" name="chkRepacking"/><%=mm.getMessage("CPMG_1193",s_user.getLocale())%></td>
                <td id="setPolicy2" class="blockDisplay"><input type="checkbox" name="chkPrint"/><%=mm.getMessage("CPMG_1191",s_user.getLocale())%><br /><input type="checkbox" name="chkNetwork" /><%=mm.getMessage("CPMG_1194",s_user.getLocale())%></td> --%>
                <td colspan="2" id="setPolicy1" class="blockDisplay">
                	<table class="exePolicyTable">
                		<tr>
                			<td><input type="radio" name="exportType" value="1"><%=mm.getMessage("CPMG_1231", s_user.getLocale())%></td>
                			<td><input type="radio" name="exportType" value="3"><%=mm.getMessage("CPMG_1232", s_user.getLocale())%></td>
                		</tr>
                		<tr>
                			<td><input type="checkbox" name="chkExtCopy"/><%=mm.getMessage("CPMG_1190",s_user.getLocale())%></td>
                			<td><input type="checkbox" name="chkRepacking"/><%=mm.getMessage("CPMG_1193",s_user.getLocale())%></td>
                		</tr>
                		<tr>
                			<td><input type="checkbox" name="chkClipCopy"/><%=mm.getMessage("CPMG_1192",s_user.getLocale())%></td>
                			<td><input type="checkbox" name="chkPrint"/><%=mm.getMessage("CPMG_1191",s_user.getLocale())%></td>
                		</tr>
                	</table>
                </td>
          </tr>
          <tr>
          	<td colspan="2" id="setMac" class="blockDisplay"><input type="checkbox" name="chkMac" class="reqInput3" /><%=mm.getMessage("CPMG_1219",s_user.getLocale())%></td>
          </tr>
          <tr>
          	<td colspan="2" id="setDevice" class="Lnodisplay">
      			<jsp:include page="/mg/common/chkBoxPolicy.jsp" >
					<jsp:param name="codeId" value="A0313"/>
					<jsp:param name="tagname" value="OUTDEVICE"/>
				</jsp:include>
          	</td>
          </tr>
          <tr>
                <td colspan="2"><input type="checkbox" name="chkExpireDt" class="reqInput3" /><%=mm.getMessage("CPMG_1195",s_user.getLocale())%> <input type="text" class="reqInput4 w60 disableInput" value="<%=serverDay %>" readOnly="true"/> ~ <input type="text" class="w60 mr164" name="expireDt"/><input type="checkbox" name="chkExe" /><%=mm.getMessage("CPMG_1196",s_user.getLocale())%> <input type="text" class="w50 reqInput5" name="readCnt"/> <%=mm.getMessage("COMG_1017",s_user.getLocale())%></td>
           </tr>
          </table>
        </div>
        <!--// Table end -->
        <div class="buttonArea mt8">
          <ul class="btn_crud">
            <!-- <li><a><span>초기화</span></a></li> -->
            <%-- <li><a class="darken" onclick="doSave();"><span><%=mm.getMessage("CPMG_1197",s_user.getLocale())%></span></a></li> --%>
            <li><a class="darken" onclick="doSave();"><span><%=mm.getMessage("CPMG_1197",s_user.getLocale())%></span></a></li>
          </ul>
        </div>
        <div class="clear"> </div>
      </div>
      <!--// tableArea end -->
    </div>
    <!--// listArea end -->
  </div>
  <!-- innerContent end -->
  </div>
 </form> 
</body>
</html>