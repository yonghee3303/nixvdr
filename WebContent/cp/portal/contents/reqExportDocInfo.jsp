<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>

<%
	IUser s_user = (IUser) session.getAttribute("j_user");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String docNo = StringUtils.paramReplace(request.getParameter("DOC_NO"));
	if(docNo.length() > 20) {
		docNo = docNo.substring(0, 20);		
	}
	String cpStateCd = StringUtils.paramReplace(request.getParameter("CP_STS_CD"));
	String serverDay = s_user.getProperty("TODAY").toString();
	String fromMail = StringUtils.paramReplace(request.getParameter("mail"));
	String callFrom = StringUtils.paramReplace(request.getParameter("from"));
	//if(stsCd == null || stsCd == "null") stsCd = "01";
  	String contextPath = request.getContextPath();
  	String css = pm.getString("component.ui.portal.css");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/su_grid_common_render.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>
<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../../ext/js/doAjaxCombo.js"></script>
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var myDataProcessor = {};

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

function init(){
	setFormDraw();
	doPolicyCombo();
	doAjaxGetCpDocInfo("<%=docNo %>");
}
function doAjaxCpDocInfo(JsonData){
	var jData = JSON.parse(JsonData);
	
	document.form.DOC_NO.value = jData.DOC_NO;
	document.getElementById("DOC_TITLE").innerText=jData.DOC_TITLE;
	document.getElementById("USER_ID").innerText=jData.USER_ID;
	document.getElementById("USER_NM").innerText=jData.USER_NM;
	document.form.EXE_RULE.value = jData.EXE_RULE;
	document.getElementById("DEPT_NM").innerText=jData.DEPT_NM;
	
	document.form.AUTHORITY.value = jData.AUTHORITY;
	//document.form.CREATOR_EMAIL.value = jData.CREATOR_EMAIL;
	//document.form.WAITING_TIME.value = jData.WAITING_TIME;
	//document.form.IN_PROCESS.value = jData.IN_PROCESS;
	document.form.FILE_LIST.value = jData.FILE_LIST;
	//document.form.P_DOC_NO.value = jData.P_DOC_NO;
	
	document.form.APPROVER_ID.value = jData.APPROVER_ID;
	document.getElementById("EXE_FILE").innerText=jData.EXE_FILE;
	document.getElementById("APPROVER_NM").innerText=jData.APPROVER_NM;
	document.getElementById("DOC_MSG").innerText=jData.DOC_MSG;
	
	if("N" == "<%=pm.getString("component.ui.approve.useYn")%>" || "<%=s_user.getId()%>"!=jData.APPROVER_ID){
		//접속한 사람이 승인자가 아니라면 승인/반려 메세지 입력란을 보여주지 않는다.
		document.getElementById("APPROVAL").style.display ="none";
		//접속한 사람이 승인자가 아니라면 승인/반려 버튼을 보여주지 않는다.
		document.getElementById("ApprovalBtn").style.visibility="hidden";
	}else if("<%=s_user.getId()%>"==jData.APPROVER_ID && ("<%=cpStateCd%>"=="01" || jData.CP_STS_CD=="01")){
		//접속한 사람이 승인자라면 승인/반려 메세지 입력란을 보여준다.
		document.getElementById("APPROVAL").style.display ="block";
		//접속한 사람이 승인자라면 승인/반려 버튼을 보여준다.
		document.getElementById("ApprovalBtn").style.visibility="visible";
	}
	
	
	filelistframe.location.href="reqExportFileList.jsp?formid=CPP_0103&gridid=GridObj&pgmId=regMgmt&docId="+jData.DOC_NO+"&authority=4";
	doQuery();
}
function setPolicy(){
	
	var policyNo= GridObj.cells(1, GridObj.getColIndexById("POLICY_NO")).getValue();
	var policy = GridObj.cells(1, GridObj.getColIndexById("POLICY")).getValue();
	var policyDoc =  GridObj.cells(1, GridObj.getColIndexById("POLICY_DOC")).getValue();
	var expireDate = GridObj.cells(1, GridObj.getColIndexById("EXPIRE_DT")).getValue(); 
	var readCnt = GridObj.cells(1, GridObj.getColIndexById("READ_COUNT")).getValue();
	var outdevice = GridObj.cells(1, GridObj.getColIndexById("OUTDEVICE")).getValue().split(",");
	var outRadio =document.getElementsByName("OUTDEVICE");
	document.form.policyCode.value =policyNo;
	document.form.readCnt.value=readCnt;
	document.form.expireDt.value = expireDate;
/*  	if(policy.substring(0,1) == "1") {
		document.form.chkNetwork.checked = true;
	} else {
		document.form.chkNetwork.checked = false;
	} 
 */
	if(policy.substring(1,2) == "1") {
		document.form.chkExpireDt.checked = true;
	} else {
		document.form.chkExpireDt.checked = false;
	}
	
	if(policy.substring(2,3) == "1") {
		document.form.chkExe.checked = true;
	} else {
		document.form.chkExe.checked = false;
	}
	
	
/* 	if(policy.substring(3,4) == "1") {
		document.form.chkDocAuth.checked = true;
	} else {
		document.form.chkDocAuth.checked = false;
	}
	 */
	
	/* if(policy.substring(4,5) == "1") {
		document.form.chkLogTracking.checked = true;
	} else {
		document.form.chkLogTracking.checked = false;
	} */
	// 문서권한
/* 	if(policyDoc.substring(0,1) == "1") {
		document.form.chkSave.checked = true;
	} else {
		document.form.chkSave.checked = false;
	} */
	if(policyDoc.substring(1,2) == "1") {
		document.form.chkPrint.checked = true;
	} else {
		document.form.chkPrint.checked = false;
	}
	if(policyDoc.substring(2,3) == "1") {
		document.form.chkClipCopy.checked = true;
	} else {
		document.form.chkClipCopy.checked = false;
	}
	if(policyDoc.substring(3,4) == "1") {
		document.form.chkRepacking.checked = true;
	} else {
		document.form.chkRepacking.checked = false;
	}
	if(policyDoc.substring(4,5) == "1") {
		document.form.chkExtCopy.checked = true;
	} else {
		document.form.chkExtCopy.checked = false;
	}

	
	var exportTypeRadio = document.form.exportType;
	for(var i=0; i < exportTypeRadio.length; i++){
		exportTypeRadio[i].checked=false;
    }
	for(var i=0; i < exportTypeRadio.length; i++){
		if(exportTypeRadio[i].value=="1"){
			if(document.form.AUTHORITY.value=="4")exportTypeRadio[i].checked=true;
		}else if(exportTypeRadio[i].value=="3"){
			if(document.form.AUTHORITY.value=="3")exportTypeRadio[i].checked=true;
		}
    } 
	
	if(document.form.AUTHORITY.value=="4"){	
		document.getElementById("setPolicy1").className="blockDisplay";
		//document.getElementById("setPolicy2").className="blockDisplay";
		//document.getElementById("setMac").className ="blockDisplay";
		document.getElementById("setDevice").className="Lnodisplay";
		document.form.OUTDEVICE_01.checked=false;
		document.form.OUTDEVICE_02.checked=false;
		document.form.OUTDEVICE_03.checked=false;
	}else if(document.form.AUTHORITY.value=="6"){
		document.getElementById("setPolicy1").className="Lnodisplay";
		//document.getElementById("setPolicy2").className="Lnodisplay";
		//document.getElementById("setMac").className ="Lnodisplay";
		document.getElementById("setDevice").className="blockDisplay";
		for(var i=0; i < outRadio.length; i++){
			for(var j=0; j <outdevice.length; j++){
				if(outRadio[i].value==outdevice[j])	outRadio[i].checked=true;
			}
		}
	}else if(document.form.AUTHORITY.value=="3"){ //자체인증 보안반출
		document.getElementById("setPolicy1").className="blockDisplay";
		//document.getElementById("setPolicy2").className="blockDisplay";
		//document.getElementById("setMac").className ="blockDisplay";
		document.getElementById("setDevice").className="Lnodisplay";
		for(var i=0; i < outRadio.length; i++){
			outRadio[i].checked=false;
		}
	}
	//정책변경 불가
	document.form.policyCode.disabled=true;
	document.form.chkExe.disabled = true;
	document.form.chkExpireDt.disabled = true;
/* 	document.form.chkNetwork.disabled = true; */
	document.form.chkPrint.disabled = true;
	document.form.chkClipCopy.disabled = true;
	document.form.chkRepacking.disabled = true;
	document.form.chkExtCopy.disabled = true;
	document.form.expireDt.readOnly = true;
	document.form.readCnt.readOnly = true;
	for(var i=0; i < outRadio.length; i++){
		outRadio[i].disabled=true;
    }
	for(var j=0; j < exportTypeRadio.length; j++ ){
		exportTypeRadio[j].disabled=true;
	}

}
function doQuery(){
	var docNo = document.form.DOC_NO.value;
	if(docNo == "") {
		return;
	}
	var grid_col_id = "<%=grid_col_id%>";
	var argu = "&DOC_NO=" + docNo;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectDocUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);
}
function doQueryEnd(){
	setPolicy();
}
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
  	GridObj.attachEvent("onXLE", doQueryEnd);
   	//GridObj.setSizes();
}
//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	//goPopupPolicy(rowId, "grid");
	//row_id = rowId;
}
function doOnRowSelect(rowId, cellInd) {
	
}
function doOnCellChange(){
	
}
//정책 콤보
function doPolicyCombo() {
	var url = "/mg/cpgrp.do?mod=getComboCpGrpHtml";
	sendRequest(doPolicyComboEnd, "", 'POST', url , false, false);
}

function doPolicyComboEnd(oj) {
	var policyCombo = document.getElementById("comboCpGrp");
	policyCombo.innerHTML = oj.responseText;//jData;
	//document.form.policyCode.value ="P001";
	//cpGrpChange(); //정책그룹 읽어서 뿌려주기
}

//승인 반려
function doApproval(approvalYn) {
	//
	if("N" == "<%=pm.getString("component.ui.approve.useYn")%>"){
		alert("<%=mm.getMessage("CPMG_1045", s_user.getLocale())%>");
		return;
	}else if("<%=s_user.getId()%>"!=document.form.APPROVER_ID.value){
		alert("<%=mm.getMessage("CPMG_1046", s_user.getLocale())%>");
		return;
	}else if("<%=cpStateCd%>"!="01"){
		alert("<%=mm.getMessage("CPMG_1047", s_user.getLocale())%>");
		return;
	}
	
	var msg="", chkMsg="";
	if(approvalYn == "N") {
		msg = "<%=mm.getMessage("CPMG_1012", s_user.getLocale())%>";  // 반려
		chkMsg = "<%=mm.getMessage("CPMG_1032", s_user.getLocale())%>";  // 반려
	} else {
		msg = "<%=mm.getMessage("CPMG_1013", s_user.getLocale())%>";  // 승인
		chkMsg = "<%=mm.getMessage("CPMG_1033", s_user.getLocale())%>";  
	}
	if(document.form.APPROVAL_MSG.value == "") {
		alert(chkMsg);
		return;
	}
	
	if (confirm(msg)) { 
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&APPROVAL_YN=" + approvalYn;
			argu += "&DOC_NO=" + document.form.DOC_NO.value;
			argu += "&DOC_TITLE=" + document.getElementById("DOC_TITLE").innerText;
			argu += "&APPROVAL_MSG="  + encodeURIComponent(document.form.APPROVAL_MSG.value);
			argu += "&DOC_MSG=" + encodeURIComponent(document.getElementById("DOC_MSG").innerText);
        	argu += "&AUTHORITY="+document.form.AUTHORITY.value;
        	argu += "&FILE_LIST="+document.form.FILE_LIST.value;
        	argu += "&EXE_FILE=" + encodeURIComponent(document.getElementById("EXE_FILE").innerText);
			var SERVLETURL = G_SERVLETURL + "?mod=singleApproval&"+argu;	    
	    sendRequest(doAjaxResult, "", 'POST', SERVLETURL , false, false);
	}
}

// 승인결과 Response  -> 승인 또는 반려 되면 목록 화면으로 이동 한다.
function doAjaxResult(oj) {
	// alert(oj.responseText);
	if (confirm(oj.responseText)){
		var msg = oj.responseText.substring(0, 2);
		if("승인" == msg || "반려" == msg) {   // 승인 화면으로 이동함.
			if("<%=fromMail%>"=="1"){
				self.close();
			}
			window.history.back();
			parent.mainLeft.init();
		}
	}
}
</script>
<title>Insert title here</title>
</head>
<body onload="init();" oncontextmenu="return false">
<form name="form" method="post">
<input type="hidden" name="DOC_NO" value="">
<input type="hidden" name="EXE_RULE" value="">
<input type="hidden" name="EXE_FILE" value="">
<input type="hidden" name="APPROVER_ID" value="">
<input type="hidden" name="FILE_LIST" value="">
<input type="hidden" name="AUTHORITY" value="">
	<!-- contents start -->
    <div class="contents">
      <div class="innerContent">
   <%@ include file="/mg/common/milestone_cpportal.jsp"%>
        <!--// Page Title end -->
        <!-- listArea start -->
        <div class="listArea">
          <!-- tableArea start -->
          <div class="tableArea">
            <h2 class="subtitle"><%=mm.getMessage("CPMG_1174",s_user.getLocale())%></h2>
            <!-- Table start -->
            <div class="table">
              <table class="tableGeneral">
              <caption>반출정보</caption>
              <colgroup>
              <col class="col_13p" />
              <col class="col_37p" />
              <col class="col_13p" />
              <col class="col_37p" />
              </colgroup>
              <tbody>
              <tr>
                <th><%=mm.getMessage("CPMG_1161",s_user.getLocale())%></th>
                <td><font id="USER_NM"></font></td>
                <th class="th02"><%=mm.getMessage("CPMG_1175",s_user.getLocale())%></th>
                <td class="td02"><font id="USER_ID"></font></td>
              </tr>
              <tr>
                <th><%=mm.getMessage("CPMG_1177",s_user.getLocale())%></th>
                <td><font id="DEPT_NM"></font></td>
                <th class="th02"><%=mm.getMessage("CPMG_1176",s_user.getLocale())%></th>
                <td class="td02"><font id="FUNCTION_NM"></font></td>
              </tr>
              <tr>
                <th><%=mm.getMessage("CPMG_1178",s_user.getLocale())%></th>
                <td colspan="3"><font id="DOC_TITLE"></font></td>
              </tr>
              <tr>
                <th><%=mm.getMessage("CPMG_1105",s_user.getLocale())%></th>
                <td colspan="3"><font id="APPROVER_NM"></font></td>
              </tr>
              <tr>
                <th><%=mm.getMessage("CPMG_1179",s_user.getLocale())%></th>
                <td colspan="3"><font id="DOC_MSG"></font></td>
              </tr>
              <tr id="APPROVAL" style="display:none;">
            	<th><%=mm.getMessage("CPMG_1198",s_user.getLocale())%></th>
            	<td colspan="3">
              		<textarea name="APPROVAL_MSG" id="textarea01" cols="40" rows="10" class="h40"> </textarea>
            	</td>
          	  </tr>
              </table>
            </div>
            <!--// Table end -->
          </div>
          <!--// tableArea end -->
          <!-- tableArea start -->
          <div class="tableArea">
            <h2 class="subtitle"><%=mm.getMessage("CPMG_1180",s_user.getLocale())%></h2>
            <!-- Table start -->
            <div class="table">
              <table class="tableGeneral">
              <caption><%=mm.getMessage("CPMG_1180",s_user.getLocale())%></caption>
              <colgroup>
              <col class="col_13p" />
              <col />
              </colgroup>
              <tbody>
              <tr>
                <th><%=mm.getMessage("CPMG_1182",s_user.getLocale())%></th>
                <td>
                    <div id="gridbox" style="width:815px; height:175px; background-color:white; overflow:hidden;"></div>
                  <div class="buttonArea">
                   <!--  <ul class="btn_crud mr-6">
                      <li><a href="#" class="inlineBtn dimmed"><span>선택삭제</span></a></li>
                    </ul> -->
                  </div>
                </td>
              </tr>
              </table>
            </div>
            <!--// Table end -->
          </div>
          <!--// tableArea end -->
          <!-- tableArea start -->
          <div class="tableArea">
            <h2 class="subtitle"><%=mm.getMessage("CPMG_1184",s_user.getLocale())%></h2>
            <!-- Table start -->
            <div class="table">
              <table class="tableGeneral">
              <caption>보안파일정보</caption>
              <colgroup>
              <col class="col_13p" />
              <col />
              <col />
              </colgroup>
              <tbody>
              <tr>
                <th><%=mm.getMessage("CPMG_1157",s_user.getLocale())%></th>
                <td colspan="2"><font id="EXE_FILE"></font></td>
              </tr>
              <tr>
          		 <th><%=mm.getMessage("CPMG_1187",s_user.getLocale())%></th>
                 <td colspan="2">
          			<iframe name="filelistframe" width="815px" height="175px" frameborder="0" scrolling="auto"></iframe>
          		</td>
          	  </tr>
          	  <tr>
          	       <th><%=mm.getMessage("CPMG_1188",s_user.getLocale())%></th>
                <td colspan="2">
                  <font id="comboCpGrp"></font>
                </td>
              </tr> 
              <tr>
                <th rowspan="4"><%=mm.getMessage("CPMG_1189",s_user.getLocale())%></th>
                <%-- <td id="setPolicy1" class="blockDisplay"><input type="checkbox" name="chkExtCopy"/><%=mm.getMessage("CPMG_1190",s_user.getLocale())%><br /><input type="checkbox" name="chkClipCopy"/><%=mm.getMessage("CPMG_1192",s_user.getLocale())%><br /><input type="checkbox" name="chkRepacking"/><%=mm.getMessage("CPMG_1199",s_user.getLocale())%></td>
                <td id="setPolicy2" class="blockDisplay"><input type="checkbox" name="chkPrint"/><%=mm.getMessage("CPMG_1191",s_user.getLocale())%><br /><input type="checkbox" name="chkNetwork"/><%=mm.getMessage("CPMG_1194",s_user.getLocale())%></td> --%>
                <td colspan="2" id="setPolicy1" class="blockDisplay">
                	<table class="exePolicyTable">
                		<tr>
                			<td><input type="radio" name="exportType" value="1"><%=mm.getMessage("CPMG_1231", s_user.getLocale())%></td>
                			<td><input type="radio" name="exportType" value="3"><%=mm.getMessage("CPMG_1232", s_user.getLocale())%></td>
                		</tr>
                		<tr>
                			<td><input type="checkbox" name="chkExtCopy"/><%=mm.getMessage("CPMG_1190",s_user.getLocale())%></td>
                			<td><input type="checkbox" name="chkRepacking"/><%=mm.getMessage("CPMG_1193",s_user.getLocale())%></td>
                		</tr>
                		<tr>
                			<td><input type="checkbox" name="chkClipCopy"/><%=mm.getMessage("CPMG_1192",s_user.getLocale())%></td>
                			<td><input type="checkbox" name="chkPrint"/><%=mm.getMessage("CPMG_1191",s_user.getLocale())%></td>
                		</tr>
                	</table>
                </td>
              </tr>
          	  <tr>
          	<td colspan="2" id="setDevice" class="Lnodisplay">
      			<jsp:include page="/mg/common/chkBoxPolicy.jsp" >
					<jsp:param name="codeId" value="A0313"/>
					<jsp:param name="tagname" value="OUTDEVICE"/>
				</jsp:include>
          	</td>
          	  </tr>
              <tr>
                <td colspan="2"><input type="checkbox" name="chkExpireDt"/><%=mm.getMessage("CPMG_1195",s_user.getLocale())%> <input type="text" class="w60 ml30" value="<%=serverDay %>" readOnly="ture"/> ~ <input type="text" class="w60" name="expireDt"/></td>
              </tr>
              <tr>
                <td colspan="2"><input type="checkbox" name="chkExe"/><%=mm.getMessage("CPMG_1196",s_user.getLocale())%> <input type="text" class="w50 ml30" name="readCnt"/></td>
              </tr>
          </table>
        </div>
        <div class="buttonArea mt8" id="ApprovalBtn" style="visibility:hidden;">
          <ul class="btn_crud">
            <li><a onclick="doApproval('N');"><span><%=mm.getMessage("CPMG_1107",s_user.getLocale())%></span></a></li>
            <li><a class="darken" onclick="doApproval('Y');"><span><%=mm.getMessage("CPMG_1108",s_user.getLocale())%></span></a></li>
          </ul>
        </div>
        <div class="clear"> </div>
      </div>
      <!--// tableArea end -->
    </div>
    <!--// listArea end -->
  </div>
  <!-- innerContent end -->
  </div>
 </form> 
</body>
</html>