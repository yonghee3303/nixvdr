<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	Locale locale = null;
	String lang = request.getParameter("j_janguage");	//language
	
	if("ko".equals(lang)) {
		locale = Locale.KOREA;
	} else if("en".equals(lang)) {
		locale = Locale.US;
	} else if("zh".equals(lang)) {
		locale = Locale.CHINA;
	} else if("ja".equals(lang)) {
		locale = Locale.JAPAN;
	} else {
		locale = Locale.KOREA;
	}
	
	String param = request.getParameter("param");		//filelist
	String userId = request.getParameter("SECUSER");	//userId
	
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  	String exportUser = pm.getString("component.export.user");
  	String contextPath = request.getContextPath();
	String css = pm.getString("component.ui.portal.css");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- css수정필요 -->
<link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="../../../ext/js/lgchem/jquery-1.9.0.js"></script>
<script type="text/javascript" src="../../../ext/js/lgchem/common.js"></script>

<%@ include file="../../../ext/include/dhtmlx_v403_scripts_portal.jsp" %>
<%@ include file="../../../ext/include/portal_grid_common_render.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../../ext/js/ajaxlib/jslb_ajax.js"></script>
<!-- <script type="text/javascript" src="../../../ext/js/JDate.js"></script> -->
<!-- <script type="text/javascript" src="../../../ext/js/doAjaxCombo.js"></script> -->
<script type="text/javascript" src="../../../ext/js/json2.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var userType		= "approver";
//등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var jDataPolicy;
var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

var token = null;
var s_user = null;
var positionNm = null;
var tokenPage = null;
var failCnt = 0;

function init(){
	//token 생성 및 해당 정보 취득
	if("<%=pm.getString("component.site.company")%>" == "ILJIN") {
		doCreateToken();
	}
	//사용자 정보 취득
	doAjaxGetUserByProperties();
	// Exe 생성번호 취득
	getDocId();
	
	if("<%=pm.getString("cliptorplus.export.request.noApprover")%>" != "Y") {
		doGetApprover(); //승인자 얻어오기
	}
	document.form.EXE_RULE.value="USER Creation";
	document.form.DOC_TITLE.focus();
	setFormDraw();	//그리드 그리기
	doPolicyCombo(); //콤보박스 생성
}

function doCreateToken() {
	<%-- var url = "https://eip.iljin.com/myoffice/ezApprovalConn/CryptoPlus/GetOraSSO.aspx?USERID=<%=userId%>"; --%>
	var url = "<%=pm.getString("component.interlock.request.url")%>/myoffice/ezApprovalConn/CryptoPlus/GetOraSSO.aspx?USERID=<%=userId%>";
	tokenPage = window.open(url,"tokenPage","width=10, height=10");
}

//token 정보 취득
function doAjaxGetToken() {
	var url = G_SERVLETURL + "?mod=getToken&userId=<%=userId%>";
	sendRequest(doAjaxGetTokenEnd, "", 'POST', url , false, false);
}

function doAjaxGetTokenEnd(oj) {
	token = oj.responseText;
	doSave();
}

//사용자 정보 취득 (IUser s_user = (IUser) session.getAttribute("j_user"))
function doAjaxGetUserByProperties() {
	var url = G_SERVLETURL + "?mod=getUserByProperties&userId=<%=userId%>";
	sendRequest(doAjaxGetUserByPropertiesEnd, "", 'POST', url , false, false);
}

function doAjaxGetUserByPropertiesEnd(oj) {
	s_user = JSON.parse(oj.responseText);
	
	positionNm = "";
	if(s_user.POSITION_NM == null) {
		positionNm = s_user.FUNCTION_NM;
	} else {
		positionNm = s_user.POSITION_NM;
	}
	
	document.getElementById("LABEL_USER_NM").innerHTML = s_user.USER_NM;
	document.form.USER_NM.value = s_user.USER_NM;
	document.getElementById("LABEL_USER_ID").innerHTML = s_user.USER_ID;
	document.form.USER_ID.value = s_user.USER_ID;
	document.getElementById("LABEL_DEPT_NM").innerHTML = s_user.DEPT_NM;
	document.form.DEPT_NM.value = s_user.DEPT_NM;
	document.getElementById("LABEL_POSITION_NM").innerHTML = s_user.POSITION_NM;
	document.form.POSITION_NM.value = s_user.POSITION_NM;
	document.form.TODAY.value = s_user.TODAY;
	document.form.CREATOR_EMAIL.value = s_user.EMAIL;
}

function getDocId() {
	var docId;
	var param = JSON.parse('<%=param%>');
	var filelist = param.filelist;
	
	//alert("file count : " + filelist.length);
	
	if(filelist.length > 0) {
		docId = filelist[0].docId;
		//alert("docId : " + docId);
		doAjaxCpDocNo(docId);
	} else {
		alert("error :: not found file!");
	}
}

function doAjaxCpDocNo(docNo) {
	document.form.DOC_NO.value = docNo;
	filelistframe.location.href="reqExportFileList.jsp?formid=CPP_0103&gridid=GridObj&pgmId=regMgmt&docId="+docNo+"&authority=4";
}

//SYSUSERAPPROVER에서 APPROVER_ID를 얻어와 SYSUSER에서 승인자 USER_ID를 구한다.
function doGetApprover() {
	<%if("personal".equals(pm.getString("component.export.approver"))){%>
		var url =G_SERVLETURL+"?mod=getUserApprover&USER_ID=" + s_user.USER_ID;
		sendRequest(doGetApproverEnd, "", 'POST', url , false, false);
	<%}
	else{%>
			document.form.APPROVER_NM.value= s_user.MANAGER_NM;
			document.form.APPROVER_ID.value= s_user.MANAGER_ID;
	<%} 
	if("Y".equals(pm.getString("component.ui.approver.searchYn"))){%>
			document.getElementById("searchApprover_a").style.display ="inline";
			document.getElementById("searchApprover_img").style.display ="inline";
			document.getElementById("searchApprover_img").style.margin = "0 0 3px -5px";
	<%}%>
	if("Y" == s_user.SELF_EXP_YN) {
			document.form.APPROVER_NM.value= s_user.USER_NM;
			document.form.APPROVER_ID.value= s_user.USER_ID;
			document.getElementById("selfApprover").innerText="<%=mm.getMessage("CPMG_1233", locale)%>";
	}
}

function doGetApproverEnd(oj) {
	var jData = JSON.parse(oj.responseText);
	var approverId = jData.APPROVER_ID;
	var approverNm = jData.APPROVER_NM;
	document.form.APPROVER_NM.value=approverNm;
	document.form.APPROVER_ID.value=approverId;
}

function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	//GridObj.setSizes();
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	//goPopupPolicy(rowId, "grid");
	//row_id = rowId;
}
function doOnRowSelect(rowId, cellInd) {
	
}
function doOnCellChange(){
	
}

//체크가 된 목록이 있나 확인
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

//policy combo 동적으로 생성
function doPolicyCombo() { 
	<%if("Y".equals(pm.getString("cliptorplus.export.isExeOnly"))){%>
	var url = "/mg/cpgrp.do?mod=getComboCpGrpHtml&cl_cd=13";
	<%}else{%>
	var url = "/mg/cpgrp.do?mod=getComboCpGrpHtml";
	<%}%>
	sendRequest(doAjaxResult, "", 'POST', url , false, false);
}

function doAjaxResult(oj) {
	//var jData = JSON.parse(oj.responseText);
	var policyCombo = document.getElementById("comboCpGrp");
	policyCombo.innerHTML = oj.responseText;//jData;
	document.form.policyCode.value ="<%=pm.getString("cliptorplus.export.default.policyCd")%>";
	//LG화학에서 기본 정책만 사용
	<%if(pm.getBoolean("component.ui.cpPolicyCode.isReadOnly")){%>
		document.form.policyCode.disabled=true;
	<%}%>
	cpGrpChange(); //정책그룹 읽어서 뿌려주기
}

function cpGrpChange(){
	var policyNo = document.form.policyCode.value;
	var url =G_SERVLETURL+"?mod=getPolicyGrp&POLICY_NO="+policyNo;
	sendRequest(cpGrpChangeEnd, "", 'POST', url , false, false);
}

function cpGrpChangeEnd(oj){
	jDataPolicy = JSON.parse(oj.responseText);
	
	var policyNo = document.form.policyCode.value;
	var policyCd = jDataPolicy.POLICY_CL_CD;
	var policy = jDataPolicy.POLICY;
	var policyDoc = jDataPolicy.POLICY_DOC;
	var policyDisp = jDataPolicy.POLICY_DISP;
	var expireDate = jDataPolicy.EXPIRE_DATE;
	var readCnt = jDataPolicy.READ_COUNT;
	var macYn = jDataPolicy.MAC_YN;
	var outdevice = jDataPolicy.OUTDEVICE.split(",");
	var outRadio =document.form.OUTDEVICE;
	document.form.readCnt.value=readCnt;
	var editCd = jDataPolicy.USEREDIT_CD;
	
	var exportTypeRadio = document.form.exportType;
	for(var i=0; i < exportTypeRadio.length; i++){
		exportTypeRadio[i].checked=false;
    }
	for(var i=0; i < exportTypeRadio.length; i++){
		if(exportTypeRadio[i].value==policyCd){
			exportTypeRadio[i].checked=true;
		}
    }
 	
 	if(policy.substring(1,2) == "1") {
 		document.form.chkExpireDt.checked = true;
 	} else {
 		document.form.chkExpireDt.checked = false;
 		expireDate="";
 	}
 	document.form.expireDt.value = expireDate;

 	if(policy.substring(2,3) == "1") {
 		document.form.chkExe.checked = true;
 	} else {
 		document.form.chkExe.checked = false;
 	}
 		
 		
	/* 	if(policy.substring(3,4) == "1") {
			document.form.chkDocAuth.checked = true;
		} else {
			document.form.chkDocAuth.checked = false;
		}
		 */
		
		/* if(policy.substring(4,5) == "1") {
			document.form.chkLogTracking.checked = true;
		} else {
			document.form.chkLogTracking.checked = false;
		} */
		// 문서권한
	/* 	if(policyDoc.substring(0,1) == "1") {
		document.form.chkSave.checked = true;
	} else {
		document.form.chkSave.checked = false;
	} */
	if(policyDoc.substring(1,2) == "1") {
		document.form.chkPrint.checked = true;
	} else {
		document.form.chkPrint.checked = false;
	}
	if(policyDoc.substring(2,3) == "1") {
		document.form.chkClipCopy.checked = true;
	} else {
		document.form.chkClipCopy.checked = false;
	}
	if(policyDoc.substring(3,4) == "1") {
		document.form.chkRepacking.checked = true;
	} else {
		document.form.chkRepacking.checked = false;
	}
	if(policyDoc.substring(4,5) == "1") {
		document.form.chkExtCopy.checked = true;
	} else {
		document.form.chkExtCopy.checked = false;
	}
		
	if(macYn =="Y"){
		document.form.chkMac.checked = true;
	} else {
		document.form.chkMac.checked = false;
	}
		
	if(policyCd=="1"){	//서버인증 보안반출
		document.form.AUTHORITY.value="4";
		document.getElementById("setPolicy1").className="blockDisplay";
		//document.getElementById("setPolicy2").className="blockDisplay";
		document.getElementById("setMac").className ="blockDisplay";
		document.getElementById("setDevice").className="Lnodisplay";
		document.form.OUTDEVICE_01.checked=false;
		document.form.OUTDEVICE_02.checked=false;
		document.form.OUTDEVICE_03.checked=false;
	}else if(policyCd=="2"){ //원문반출
		document.form.AUTHORITY.value="6";
		document.getElementById("setPolicy1").className="Lnodisplay";
		//document.getElementById("setPolicy2").className="Lnodisplay";
		document.getElementById("setMac").className ="Lnodisplay";
		document.getElementById("setDevice").className="blockDisplay";
	
		for(var i=0; i < outRadio.length; i++){
			for(var j=0; j <outdevice.length; j++){
				if(outRadio[i].value==outdevice[j]){
					outRadio[i].checked=true;
				}
			}
	    }
	}else if(policyCd=="3"){ //자체인증 보안반출
		document.form.AUTHORITY.value="3";
		document.getElementById("setPolicy1").className="blockDisplay";
		//document.getElementById("setPolicy2").className="blockDisplay";
		document.getElementById("setMac").className ="blockDisplay";
		document.getElementById("setDevice").className="Lnodisplay";
		document.form.OUTDEVICE_01.checked=false;
		document.form.OUTDEVICE_02.checked=false;
		document.form.OUTDEVICE_03.checked=false;
	}
		
	//정책변경 불가
	document.form.chkMac.disabled = true;
	document.form.chkExe.disabled = true;
	document.form.chkExpireDt.disabled = true;
/* 	document.form.chkNetwork.disabled = true; */
	document.form.chkPrint.disabled = true;
	document.form.chkClipCopy.disabled = true;
	document.form.chkRepacking.disabled = true;
	document.form.chkExtCopy.disabled = true;
	for(var i=0; i < outRadio.length; i++){
		outRadio[i].disabled=true;
    }
	for(var j=0; j < exportTypeRadio.length; j++ ){
		exportTypeRadio[j].disabled=true;
	}

	switch(editCd){
		case "0": //수정허용하지 않음
			document.form.expireDt.readOnly = true;
			document.form.expireDt.className="w60 mr164 disableInput";
			document.form.readCnt.readOnly = true;
			document.form.readCnt.className="w50 reqInput5 disableInput";
			break;
		case "1": //인증횟수 수정허용
			document.form.expireDt.readOnly = true;
			document.form.expireDt.className="w60 mr164 disableInput";
			document.form.readCnt.readOnly = false;
			document.form.readCnt.className="w50 reqInput5";
			break;
		case "2": //만료일자 수정허용
			document.form.expireDt.readOnly = false;
			document.form.expireDt.className="w60 mr164";
			document.form.readCnt.readOnly = true;
			document.form.readCnt.className="w50 reqInput5 disableInput";
			break;
		case "8": //인증횟수/만료일자 수정허용
			document.form.expireDt.readOnly = false;
			document.form.expireDt.className="w60 mr164";
			document.form.readCnt.readOnly = false;
			document.form.readCnt.className="w50 reqInput5";
			break;
	}
	setPolicyAllGrid(policy, policyDoc, expireDate, readCnt, policyNo, policyDisp,jDataPolicy.OUTDEVICE);
}


function doUserPop() {
	popupCompUser_portal("<%=mm.getMessage("CPMG_1140", locale)%>", '', '', 500, 450, "<%=exportUser%>", "<%=userId%>", <%=pm.getBoolean("component.contextPath.root")%>);
}
//정책이 변경되면 그리드의 숨겨진 컬럼에 추가 각각 수신자 별 정책을 따로 심을 경우도 생각해서... 현재 체크된 사용자에게만 그 정책을 심음
function setPolicyGrid(policy, policyDoc, expireDate, readCnt, policyNo, policyDis,outdevice){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	for(var i=0; i < grid_array.length; i++) {
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY")).setValue(policy);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_DOC")).setValue(policyDoc);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDate);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("READ_COUNT")).setValue(readCnt);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_NO")).setValue(policyNo);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("POLICY_DISP")).setValue(policyDis);
		GridObj.cells(grid_array[i], GridObj.getColIndexById("OUTDEVICE")).setValue(outdevice);
		//grid_arry[i]에는 선택된 rowId값이 들어가있다.
	}
}
//일괄정책 심기
function setPolicyAllGrid(policy, policyDoc, expireDate, readCnt, policyNo, policyDis,outdevice){
	for(var i=1; i <= GridObj.getRowsNum(); i++) {
		GridObj.cells(i, GridObj.getColIndexById("POLICY")).setValue(policy);
		GridObj.cells(i, GridObj.getColIndexById("POLICY_DOC")).setValue(policyDoc);
		GridObj.cells(i, GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDate);
		GridObj.cells(i, GridObj.getColIndexById("READ_COUNT")).setValue(readCnt);
		GridObj.cells(i, GridObj.getColIndexById("POLICY_NO")).setValue(policyNo);
		GridObj.cells(i, GridObj.getColIndexById("POLICY_DISP")).setValue(policyDis);
		GridObj.cells(i, GridObj.getColIndexById("OUTDEVICE")).setValue(outdevice);
	}
}
//수정된 정책 반영 
function setEditPolicyAllGrid(expireDate, readCnt){
	for(var i=1; i <= GridObj.getRowsNum(); i++) {
		GridObj.cells(i, GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDate);
		GridObj.cells(i, GridObj.getColIndexById("READ_COUNT")).setValue(readCnt);
	}
}
function OkPopupCompUser(row_id, user_id, user_nm, company_id,company_nm, email, position, tel_no, macAddr) {
    var nMaxRow2 = parseInt(row_id);
	<%-- var row_data = "<%=grid_col_id%>"; --%>
	
		for(var i = 0; i< GridObj.getRowsNum(); i++) {			
			var compUser = GridObj.cells2(i, GridObj.getColIndexById("USER_ID")).getValue();
			if( user_id ==  compUser ) {
				alert( "[" + compUser + "] <%=mm.getMessage("CPMG_1039", locale)%>");    // 이미 등록된 수신자 입니다.
				return;
			}
		}
		dhtmlx_last_row_id++;
		nMaxRow2 = dhtmlx_last_row_id;
		GridObj.enableSmartRendering(true);
		GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));


  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = false;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("0");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_ID")).setValue(user_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_NM")).setValue(user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_ID")).setValue(company_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_NM")).setValue(company_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EMAIL")).setValue(email);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POSITION_NM")).setValue(position);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("TEL_NO")).setValue(tel_no);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAC_LIST")).setValue(macAddr);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MOD_STOP")).setValue("0");	
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY")).setValue(jDataPolicy.POLICY);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_DOC")).setValue(jDataPolicy.POLICY_DOC);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXPIRE_DT")).setValue(jDataPolicy.EXPIRE_DATE);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("READ_COUNT")).setValue(jDataPolicy.READ_COUNT);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_NO")).setValue(document.form.policyCode.value);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_DISP")).setValue(jDataPolicy.POLICY_DISP);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("OUTDEVICE")).setValue(jDataPolicy.OUTDEVICE);
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;

	ed_flag = "Y";
	
}

function doSave() {
	
	var expireDate=document.form.expireDt.value; 
	var readCnt=document.form.readCnt.value;
	
	document.form.FILE_LIST.value = filelistframe.getFilelist();
	cpGrpChange();
	setEditPolicyAllGrid(expireDate,readCnt);
	document.form.expireDt.value=expireDate; 
	document.form.readCnt.value=readCnt;
	
	if(document.form.DOC_TITLE.value == "") {  // "문서명을 입력하여 주십시요."
		alert('<%=mm.getMessage("CPMG_1015", locale)%>');
		return;
	}
	
	if(document.form.FILE_LIST.value == "") {  //반출할 파일을 Upload 해 주십시요.
		alert('<%=mm.getMessage("CPMG_1017", locale)%>');
		return;
	}
	
	// if(document.form.OUT_CL_CD[0].checked) {   // 원본반출시에만 파일명 입력
		if(document.form.EXE_FILE.value == "") { // 파일명을 입력하여 주십시요.
			alert('<%=mm.getMessage("CPMG_1016", locale)%>');
			return;
		}
	// }	
	if(document.form.AUTHORITY.value == "6"){
		document.form.EXE_FILE.value = document.form.EXE_FILE.value.replace('<%=pm.getString("cliptorplus.export.default.extension")%>', ".zip"); 
	}
	

	if(document.form.DOC_MSG.value == "") { // 반출 사유를 입력 해 주십시요.
		alert('<%=mm.getMessage("CPMG_1019", locale)%>');
		return;
	}
	
	for(var i = 0; i< GridObj.getRowsNum(); i++) {
		GridObj.cells(GridObj.getRowId(i), GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
		GridObj.cells(GridObj.getRowId(i), GridObj.getColIndexById("SELECTED")).setValue("1");
	}
	
	//if(document.form.AUTHORITY.value != "6") { // 수신자를 등록 해 주십시요.
		if(!checkRows()) {
			alert("<%=mm.getMessage("CPMG_1018", locale)%>");
			return;
		}
	//}
	if(jDataPolicy.MAC_YN =="N"){
		for(var i=1; i <= GridObj.getRowsNum(); i++) {
			GridObj.cells(i, GridObj.getColIndexById("MAC_LIST")).setValue("");
		}
	}
	
	var grid_array = getGridChangedRows2(GridObj, "SELECTED");

	 if (confirm("<%=mm.getMessage("CPMG_1020", locale)%>")) {
		
		//CP 문서 정보
		var fileList = document.form.FILE_LIST.value;
		var filn = fileList.split("|");
	 	
		var url = "&DOC_NO=" + document.form.DOC_NO.value;
		url += "&DOC_TITLE=" + encodeURIComponent(document.form.DOC_TITLE.value); 
		url += "&USER_ID=" + document.form.USER_ID.value;
		url += "&EXE_RULE=" + encodeURIComponent(document.form.EXE_RULE.value);  
		url += "&AUTHORITY=" + document.form.AUTHORITY.value;
		url += "&CREATOR_EMAIL=" + encodeURIComponent(document.form.CREATOR_EMAIL.value);
		url += "&WAITING_TIME=" + document.form.WAITING_TIME.value; 
		url += "&IN_PROCESS=" + encodeURIComponent(document.form.IN_PROCESS.value); 
		url += "&FILE_NUM=" + filn.length;
		url += "&FILE_LIST=" + encodeURIComponent(document.form.FILE_LIST.value);
		url += "&P_DOC_NO=" ;/* + document.form.P_DOC_NO.value; */
		url += "&EXE_FILE=" + encodeURIComponent(document.form.EXE_FILE.value);
		url += "&DOC_MSG=" + encodeURIComponent(document.form.DOC_MSG.value);
		url += "&OUT_CL_CD=01";
		if("<%=pm.getString("cliptorplus.export.request.noApprover")%>" != "Y") {
			url += "&APPROVER_ID=" + encodeURIComponent(document.form.APPROVER_ID.value);
		}
		
		//사용자 정보
		var userInfo = JSON.stringify(s_user);
		url += "&userInfo=" + encodeURIComponent(userInfo);
		url += "&j_janguage=" + encodeURIComponent("<%=lang%>");
		
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=saveRequest&col_ids="+cols_ids+url;
		myDataProcessor = new dataProcessor(SERVLETURL);
				
		sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}  

function doSaveEnd(obj) {
	var message = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");	
	
	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		//모든 작업 완료 후 일진 결재 페이지로 redirect
		if("<%=pm.getString("component.site.company")%>" == "ILJIN") {
			var docNo = document.form.DOC_NO.value;
			//var url = "https://eip.iljin.com/myoffice/ezApprovalConn/AutoLogin_ApvConn.aspx?UUID="+ token +"&docid=108&pConnkey="+docNo;
			var url ="<%=pm.getString("component.interlock.request.url")%>/myoffice/ezApprovalConn/AutoLogin_ApvConn.aspx?UUID="+ token +"&docid=108&pConnkey="+docNo;
			//아래 방법으로 안될 경우 주석처리된 것으로 처리
			location.href = url;
			/* centerWin(url, '', '', 'yes');
			window.close(); */
		} else {
			window.close();	
		}
	} else {
		alert(message);
	}
	return false;
}

//추가한 사용자 중 체크된 사용자만 삭제하기 
function doDelete() {
	if(!checkRows()) return;
	GridObj.enableSmartRendering(false);
	var grid_array = getGridChangedRows2(GridObj, "SELECTED"); //선택된 목록의 rowId값을 배열로 가져옴.

	//삭제하시겠습니까?
	if (confirm("<%=mm.getMessage("COMG_1010", locale)%>")) { 

		for(var i=0; i < grid_array.length; i++) {
			GridObj.deleteRow(grid_array[i]);
			// dhtmlx_last_row_id--;
		}		
	}
}

function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
function doApproverPop(){
	centerWin("/cp/portal/contents/searchApprover.jsp?manager="+encodeURIComponent(document.getElementById("manager").value)+"&formid=CPCOM_0402&gridid=GridObj",850,530,"yes");
}

//DB 저장시 글자수 제한을 위한 함수, Ascii 코드를 넘어가는 값은 모두 3byte 로 처리한다.
//element : elementById
//limit : DB 컬럼내 제한량
function len_chk(element, limit) {
	
	if('Y' == '<%=pm.getString("component.ui.input.limit")%>') {
		
		var frm = document.getElementById(element);
		var cnt = 0; 
		
		for(var i=0; i<frm.value.length; i++) {
			var chr = frm.value.substring(i,i+1);
			var numUnicode = chr.charCodeAt(0);	//number of the decimal Unicode
			
			if(numUnicode >= 0 && numUnicode <= 127) {
				cnt += 1;	
			} else {
				cnt += 3;
			}
			if(cnt > limit) {
				alert('<%=mm.getMessage("CPMG_1054",locale)%>');
				frm.value = frm.value.substring(0,i);
				frm.focus();
				break;
			}
		}
	}
}
</script>
<title></title>
</head>
<body onload="init();" oncontextmenu="return false">
<form name="form" method="post">

<input type="hidden" name="DOC_NO" value="">
<input type="hidden" name="EXE_RULE" value="">
<input type="hidden" name="FILE_LIST" value="">
<input type="hidden" name="AUTHORITY" value="4">
<input type="hidden" name="WAITING_TIME" size="10" value="10" class="text">
<input type="hidden" name="IN_PROCESS" size="46" class="text" style="IME-MODE:disabled">
<input type="hidden" name="P_DOC_NO" size="46" class="text">
<input type="hidden" name="CREATOR_EMAIL" value="">

<div class="contents">
  <!-- innerContent start -->
  <div class="innerContent">
    <%@ include file="/mg/common/milestone_cpportal.jsp"%>
    <!--// Page Title end -->
    <!-- listArea start -->
    <div class="listArea">
      <!-- tableArea start -->
      <div class="tableArea">
        <h2 class="subtitle"><%=mm.getMessage("CPMG_1174",locale)%></h2>
        <!-- Table start -->
        <div class="table">
          <table class="tableGeneral">
          <caption>반출정보</caption>
          <colgroup>
          <col class="col_13p" />
          <col class="col_37p" />
          <col class="col_13p" />
          <col class="col_37p" />
          </colgroup>
          <tbody>
          <tr>
            <th><%=mm.getMessage("CPMG_1161",locale)%></th>
            <td><span id="LABEL_USER_NM"></span><input type="hidden" name="USER_NM" value=""/></td>
            <th class="th02"><%=mm.getMessage("CPMG_1175",locale)%></th>
            <td class="td02"><span id="LABEL_USER_ID"></span><input type="hidden" name="USER_ID" value=""/></td>
          </tr>
          <tr>
            <th><%=mm.getMessage("CPMG_1177",locale)%></th>
            <td><span id="LABEL_DEPT_NM"></span><input type="hidden" name="DEPT_NM" value=""/></td>
            <th class="th02"><%=mm.getMessage("CPMG_1176",locale)%></th>
            <td class="td02"><span id="LABEL_POSITION_NM"></span><input type="hidden" name="POSITION_NM" value=""/></td>
          </tr>
          <tr>
            <th><%=mm.getMessage("CPMG_1178",locale)%></th>
            <td colspan="3">
              <input type="text" name="DOC_TITLE" class="reqInput2" onKeyup="len_chk('DOC_TITLE', 256);"/>
            </td>
          </tr>
          <%
          if(!"Y".equals(pm.getString("cliptorplus.export.request.noApprover"))) {
          %>
          <tr>
            <th><%=mm.getMessage("CPMG_1105",locale)%></th>
            <td colspan="3">
             <input type="text" id="manager" name="APPROVER_NM" class="fixedWidth" size="9" value="" readOnly="true"/>
             <a onclick="doApproverPop();" id="searchApprover_a" style="display:none;"><img class="inquiryBtn" id="searchApprover_img" src="/ext/images/<%=css %>/icon/ico_inline_inquiry.gif" alt="<%=mm.getMessage("COMG_1043",locale)%>" style="display:none;"/></a>             
             <input type="hidden" id="managerId" name="APPROVER_ID" value=""/><font id="selfApprover" class="selfApproverText"></font>
            </td>
          </tr>
          <%
          }
          %> 
          <tr>
            <th><%=mm.getMessage("CPMG_1179",locale)%></th>
            <td colspan="3">
              <textarea name="DOC_MSG" id="textarea01" cols="20" rows="3" onKeyup="len_chk('DOC_MSG', 256);"> </textarea>
            </td>
          </tr>
          </table>
        </div>
        <!--// Table end -->
      </div>
      <!--// tableArea end -->
      <!-- tableArea start -->
      <div class="tableArea">
        <h2 class="subtitle"><%=mm.getMessage("CPMG_1180",locale)%></h2>
        <!-- Table start -->
        <div class="table">
          <table class="tableGeneral">
          <caption><%=mm.getMessage("CPMG_1180",locale)%></caption>
          <colgroup>
          <col class="col_13p" />
          <col />
          </colgroup>
          <tbody>
          <tr>
            <th><%=mm.getMessage("CPMG_1181",locale)%></th>
            <td>
              <input id="input_exp05" class="reqInput1" type="text" size="9" onkeypress="if(event.keyCode==13) {doUserPop();}"/><a onclick="doUserPop();"><img class="inquiryBtn" src="/ext/images/<%=css %>/icon/ico_inline_inquiry.gif" alt="<%=mm.getMessage("COMG_1043",locale)%>"/></a>
            </td>
          </tr>
          <tr>     
            <th><%=mm.getMessage("CPMG_1182",locale)%></th>
            <td>
              <div id="gridbox" class="cpSubGrid2"></div>
              <div class="buttonArea mt8">
                <ul class="btn_crud mr-6">
                  <li><a onclick="doDelete()"><span><%=mm.getMessage("CPMG_1183",locale)%></span></a></li>
                </ul>
              </div>
            </td>
          </tr>
          </table>
        </div>
        <!--// Table end -->
      </div>
      <!--// tableArea end -->
      <!-- tableArea start -->
      <div class="tableArea">
        <h2 class="subtitle"><%=mm.getMessage("CPMG_1184",locale)%></h2>
        <!-- Table start -->
        <div class="table">
          <table class="tableGeneral">
          <caption><%=mm.getMessage("CPMG_1184",locale)%></caption>
          <colgroup>
          <col class="col_13p" />
          <col />
          <col />
          </colgroup>
          <tbody>
          <tr>
            <th><%=mm.getMessage("CPMG_1186",locale)%></th>
            <td colspan="2">
              <iframe id="uploadframe" name="uploadframe" class="cpSubGrid3" frameborder="0" scrolling="no"></iframe>
            </td>
          </tr>
          <tr>
          	<th><%=mm.getMessage("CPMG_1187",locale)%></th>
            <td colspan="2" style="padding-bottom:5px;">
          	  <iframe id="filelistframe" name="filelistframe" class="cpSubGrid4" frameborder="0" scrolling="auto"></iframe>
          	</td>
          </tr>
           <tr>
            <th><%=mm.getMessage("CPMG_1157",locale)%></th>
            <td colspan="2"><input type="text" class="reqInput2" name="EXE_FILE" /><span><%=mm.getMessage("CPMG_1185",locale)%></span>
            </td>
          </tr>
          <tr>
               <th><%=mm.getMessage("CPMG_1188",locale)%></th>
                <td colspan="2">
                	<font id="comboCpGrp"></font>
                </td>
          </tr>
          <tr>
                <th rowspan="4"><%=mm.getMessage("CPMG_1189",locale)%></th>
                <%-- <td id="setPolicy1" class="blockDisplay"><input type="checkbox" name="chkExtCopy"/><%=mm.getMessage("CPMG_1190",locale)%><br /><input type="checkbox" name="chkClipCopy"/><%=mm.getMessage("CPMG_1192",locale)%><br /><input type="checkbox" name="chkRepacking"/><%=mm.getMessage("CPMG_1193",locale)%></td>
                <td id="setPolicy2" class="blockDisplay"><input type="checkbox" name="chkPrint"/><%=mm.getMessage("CPMG_1191",locale)%><br /><input type="checkbox" name="chkNetwork" /><%=mm.getMessage("CPMG_1194",locale)%></td> --%>
                <td colspan="2" id="setPolicy1" class="blockDisplay">
                	<table class="exePolicyTable">
                		<tr>
                			<td><input type="radio" name="exportType" value="1"><%=mm.getMessage("CPMG_1231", locale)%></td>
                			<td><input type="radio" name="exportType" value="3"><%=mm.getMessage("CPMG_1232", locale)%></td>
                		</tr>
                		<tr>
                			<td><input type="checkbox" name="chkExtCopy"/><%=mm.getMessage("CPMG_1190",locale)%></td>
                			<td><input type="checkbox" name="chkRepacking"/><%=mm.getMessage("CPMG_1193",locale)%></td>
                		</tr>
                		<tr>
                			<td><input type="checkbox" name="chkClipCopy"/><%=mm.getMessage("CPMG_1192",locale)%></td>
                			<td><input type="checkbox" name="chkPrint"/><%=mm.getMessage("CPMG_1191",locale)%></td>
                		</tr>
                	</table>
                </td>
          </tr>
          <tr>
          	<td colspan="2" id="setMac" class="blockDisplay"><input type="checkbox" name="chkMac" class="reqInput3" /><%=mm.getMessage("CPMG_1219",locale)%></td>
          </tr>
          <tr>
          	<td colspan="2" id="setDevice" class="Lnodisplay">
      			<jsp:include page="/mg/common/chkBoxPolicy_noUserInfo.jsp" >
					<jsp:param name="codeId" value="A0313"/>
					<jsp:param name="tagname" value="OUTDEVICE"/>
					<jsp:param name="locale" value="<%=lang%>"/>
				</jsp:include>
          	</td>
          </tr>
          <tr>
                <td colspan="2"><input type="checkbox" name="chkExpireDt" class="reqInput3" /><%=mm.getMessage("CPMG_1195",locale)%> <input type="text" class="reqInput4 w60 disableInput" name="TODAY" value="" readOnly="true"/> ~ <input type="text" class="w60 mr164" name="expireDt"/><input type="checkbox" name="chkExe" /><%=mm.getMessage("CPMG_1196",locale)%> <input type="text" class="w50 reqInput5" name="readCnt"/> <%=mm.getMessage("COMG_1017",locale)%></td>
           </tr>
          </table>
        </div>
        <!--// Table end -->
        <div class="buttonArea mt8">
          <ul class="btn_crud">
            <!-- <li><a><span>초기화</span></a></li> -->
            <%-- <li><a class="darken" onclick="doSave();"><span><%=mm.getMessage("CPMG_1197",locale)%></span></a></li> --%>
            <li><a class="darken" onclick="doAjaxGetToken();"><span><%=mm.getMessage("CPMG_1197",locale)%></span></a></li>
          </ul>
        </div>
        <div class="clear"> </div>
      </div>
      <!--// tableArea end -->
    </div>
    <!--// listArea end -->
  </div>
  <!-- innerContent end -->
  </div>
 </form> 
</body>
</html>