<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
String pgmId = StringUtils.paramReplace(request.getParameter("pgmId"));
String docId = StringUtils.paramReplace(request.getParameter("docId"));
String fileCnt =request.getParameter("fileCnt");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName() + ":" + request.getServerPort() + path + "/";
 
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String css = pm.getString("component.ui.portal.css");
String maxFileCount = pm.getString("component.upload.maxCount");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
    <base href="<%=basePath%>">
    
    <title>Excel Upload</title>
    <link rel="stylesheet" type="text/css" href="../../../ext/css/cooperation_<%=css %>.css" />
	<script type="text/javascript">

		function fileUpload() {
						
			var filename = document.Form.j_file.value;
			
			var lastIndex = filename.lastIndexOf('.'); 
			
    		var extension = filename.substring(lastIndex+1, filename.len);
			extension = extension.toLowerCase();
			
			
			if(extension == "html" || extension == "htm" || extension == "php" || extension == "inc"  || extension == "jsp" || extension == "exe"
			   || extension == "bat" || extension == "cmd") {
				alert("<%=mm.getMessage("CPMG_1158", s_user.getLocale())%>");
			}else if(filename == '' ) {
				alert("<%=mm.getMessage("CPMG_1159", s_user.getLocale())%>");
			}else if(Number("<%=fileCnt%>") > Number("<%=maxFileCount%>")){
				alert("<%=mm.getMessage("CPMG_1043",maxFileCount,s_user.getLocale())%>");
			}else {
				parent.filelistframe.doQueryDuring2();
				document.Form.submit();
			}
		}
		
		
			
	</script>

  </head>
  
  <body oncontextmenu="return false">
  <form name="Form" action="/mg/Upload.do" method="post" enctype="multipart/form-data">
  	<input type="file" name="j_file" onchange="fileUpload(this)" class="j_file" />
    <input type="hidden" name="mod" value="tx_create_LG" />
    <input type="hidden" name="docId" value="<%=docId %>"/>
    <input type="hidden" name="pgmId" value="<%=pgmId %>"/>
   </form>

  </body>
</html>
