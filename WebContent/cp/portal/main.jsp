<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="java.text.*" %>
<%@ page import="java.net.*"%>
<%@ page import="java.util.Vector"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%
Log log = LogFactory.getLog(this.getClass());
String contextPath = request.getContextPath();
String type = request.getParameter("type");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);

String company = paramemter.getString("component.site.product");
String logo_image =  paramemter.getString("component.site.image");

IUser s_user = (IUser) session.getAttribute("j_user");
String m_path = ("en".equals(s_user.getLocale().getLanguage()) ? "/ext/images/en/" : "/ext/images/");
String userNm = s_user.getName();
String deptNm = s_user.getProperty("DEPT_NM").toString();

//로그인 사용자의 권한을 가져와 관리자/사용자 초기화면을 다르게 설정
String pgmType = paramemter.getString("component.site.product");
String css = paramemter.getString("component.ui.portal.css");

%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/ext/css/cooperation_<%=css %>.css" />
<link rel="stylesheet" href="<%=contextPath%>/ext/css/common.css" type="text/css">
<script type="text/javascript" src="<%=contextPath%>/ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/sec.js"></script>
<script language="JavaScript" src="<%=contextPath%>/ext/js/JUtil.js"></script>
<title><%=mm.getMessage("WTMG_1002", s_user.getLocale())%></title>
<script type="text/javascript">
function pageRedirect(page,argu){
	mainContent.location.href=  page + argu;
}
function init(){
	mainTop.location.href="<%=contextPath%>/cp/portal/mainTop.jsp";
	mainLeft.location.href="<%=contextPath%>/cp/portal/contents/leftMain.jsp";
	mainContent.location.href="<%=contextPath%>/cp/portal/contents/mainPortlet.jsp";
	//창크기에 따라 스크롤 생성
	document.getElementById("mainLeft_td").style.height=getWindowHeight()-40;
	document.getElementById("mainLeft").style.height=getWindowHeight()-40;
	document.getElementById("mainContent_td").style.height=getWindowHeight()-40;
	document.getElementById("mainContent_td").style.width=getWindowWidth()-230;
	document.getElementById("mainContent").style.height=getWindowHeight()-40;
	document.getElementById("mainContent").style.width=getWindowWidth()-230;
	
	var isReadNotice = getCookie("IsReadNotice");
 	if( isReadNotice != "1")
 		centerWin("/mg/common/popupNotice.jsp",530,450,"no");
 	
}
function centerWin(url, w, h, scroall){
	var winL = (screen.width-100-w)/2; 
	var winT = (screen.height-100-h)/2; 
	var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
	window.open(url,'',winOpt);
}
//content창 변환 없이 상단 및 왼쪽 메뉴 활성화
function onClickChangePage(topMenuId, subMenuId){//활성화할 상단메뉴 아이디와 활성화할 그 상단메뉴의 서브메뉴 아이디를 넘겨준다.
	mainTop.changeOnTopMenu(topMenuId); //1.상단메뉴 활성화
	if(subMenuId=="CPP_05"){
		mainLeft.location.href="<%=contextPath%>/cp/portal/mainLeft.jsp?menuId="+topMenuId+"&onLeftMenuId="+subMenuId; //2.서브메뉴 리프레쉬 
	}else{
		mainLeft.location.href="<%=contextPath%>/cp/portal/mainLeft.jsp?menuId="+topMenuId+"&onLeftMenuId="+subMenuId;
	}
}
function clickTop(link, subMenuId){
	if(subMenuId=="CPP_05"){
		mainLeft.location.href="<%=contextPath%>/cp/portal/mainLeft.jsp?menuId="+subMenuId;
		mainContent.location.href = "<%=contextPath%>"+link+"?formid="+"CPCOM_0201"+"&gridid=GridObj";
	}else{
		mainLeft.location.href="<%=contextPath%>/cp/portal/mainLeft.jsp?menuId="+subMenuId;
		mainContent.location.href = "<%=contextPath%>"+link+"?formid="+subMenuId+"01"+"&gridid=GridObj";
	}
}
window.onresize = function() { 
	//창 크기에 따라 스크롤 생성
	document.getElementById("mainLeft_td").style.height=getWindowHeight()-40;
	document.getElementById("mainLeft").style.height=getWindowHeight()-40;
	document.getElementById("mainContent_td").style.height=getWindowHeight()-40;
	document.getElementById("mainContent_td").style.width=getWindowWidth()-230;
	document.getElementById("mainContent").style.height=getWindowHeight()-40;
	document.getElementById("mainContent").style.width=getWindowWidth()-230;
}
function getWindowWidth(){
	var w = 0;

	if (typeof (window.innerWidth) == 'number') { //Chrome
		 w = window.innerWidth;
	} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		 w = document.documentElement.clientWidth;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) { //IE9
		 w = document.body.clientWidth;
	}
	return w;
}
function getWindowHeight(){
	var h = 0;

	if (typeof (window.innerWidth) == 'number') { //Chrome
		 h = window.innerHeight;
	} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
		 h = document.documentElement.clientHeight;
	} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) { //IE9
		 h = document.body.clientHeight;
	}
	return h;
}
</script>

</head>
<BODY onload="init();" unload="form_onunload();" topmargin="0" leftmargin="0" bgcolor="#f8f9f9" style='overflow:hidden' oncontextmenu="return false">
<form name="search" style="margin-bottom:0px">
	<input type="hidden" name="btn_flg" 			value=''>
	<input type="hidden" name="order_seq" 			value=''>
	<input type="hidden" name="menu_type" 			value=''>
	<input type="hidden" name="totaldata" 			value=''>
	<input type="hidden" name="lastLoginDateTime" 	value=''>
	<input type="hidden" name="compare_menu_type" 	value=''>
	<input type="hidden" name="flag" 				value=''>
	<input type="hidden" name="top_menuname"		value=''>
	<input type="hidden" name="url" 				value=''>
	<input type="hidden" name="MENU_OBJECT_CODE" 	value=''>
	<input type="hidden" name="mileStoneName"		value="">	
	<input type="hidden" name="leftMenuName"  		value="">
</form>
<table>
	<tr>
		<td style="width:100%; height:40px;"colspan="2">
			<iframe name="mainTop" id="mainTop" title="mainTop" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
		</td>
	</tr>
	<tr>
		<td id="mainLeft_td" style="width:230px; height:1200px;">
			<iframe name="mainLeft" id="mainLeft" title="mainLeft" marginwidth="0" marginheight="0" scrolling="no" frameborder='no' style="width:100%; height:100%;"></iframe>
		</td>
		<td id="mainContent_td" style="width:990px; height:1200px;">
			<iframe name="mainContent" id="mainContent" title="mainContent" marginwidth="0" marginheight="0" scrolling="yes" frameborder='no' style="width:100%; height:100%;"></iframe>
		</td>
	</tr>
</table>
</body>
</html>
