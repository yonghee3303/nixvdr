<%@ page contentType="text/html; charset=UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
	
	IUser s_user = (IUser) session.getAttribute("j_user");
	
	String userNm = s_user.getName();
	String topMenuId = request.getParameter("menuId");
	IMenu menu = mnu.getMenu(topMenuId, s_user);
	String deptNm = s_user.getProperty("DEPT_NM").toString();
  	String position="";
  	if(s_user.getProperty("POSITION_NM")== null && s_user.getProperty("FUNCTION_NM")== null) {
  		position = "unknown";
  	} else {
  		if(s_user.getProperty("POSITION_NM")== null){
  			position = s_user.getProperty("FUNCTION_NM").toString();
  		} else{
  			position = s_user.getProperty("POSITION_NM").toString();
  		}
  	}
	String onLeftMenuId = StringUtils.paramReplace(request.getParameter("onLeftMenuId"));
	IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = pm.getString("component.ui.portal.css");
	String contextPath = request.getContextPath();
	String beforeDay = pm.getString("cliptorplus.dashboard.period.day");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="<%=contextPath%>/ext/js/lgchem/jquery-1.9.0.js"></script>
<!-- <script type="text/javascript" src="../../ext/js/lgchem/common.js"></script> -->
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="<%=contextPath%>/ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="<%=contextPath%>/ext/js/json2.js"></script>
<script>
var G_SERVLETURL = "<%=contextPath%>/mg/summaryInfo.do";

function logout() {
	if(confirm("<%=mm.getMessage("COMG_1034", s_user.getLocale())%>")) {  // 시스템을 종료 하시겠습니까 ?
	   top.location.href = "<%=contextPath%>/Logout.do";
	}
}

function clickLeftMenu(link, menuId, pMenuName, menuName){
	changeOnMenu(menuId);
	if(menuId.substring(0, 6)=="CPP_05" || menuId.substring(0, 6)=="CPP_05"){
		parent.mainContent.location.href = "<%=contextPath%>"+link+"?formid=CPCOM_02"+menuId.substring(6,menuId.len)+"&gridid=GridObj&mileStoneName="+pMenuName+"&leftMenuName="+menuName;
		
	}else{
		parent.mainContent.location.href="<%=contextPath%>"+link+"?formid="+menuId+"&gridid=GridObj&mileStoneName="+pMenuName+"&leftMenuName="+menuName;
	}
}
function changeOnMenu(menu_id){
	<% for (int i = 0; i < menu.getChildren().size(); i++) { %>
		document.getElementById("<%=i%>").firstChild.className="off";
	<%}%>
		document.getElementById(menu_id).className="on";
}
function init(){
	doAjax();
	//메뉴 활성화 파라미터가 넘어오지 않았다면 가장 첫번째 메뉴를 활성화 시킨다.
	if("<%=onLeftMenuId%>" !=  "" && "<%=onLeftMenuId%>" != "null"){
		changeOnMenu("<%=onLeftMenuId%>");
	}else{
		document.getElementById("0").firstChild.className="on";
	}
	
	//15일간의 조회 결과 입니다.
	document.getElementById("waitCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	document.getElementById("noReadCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	document.getElementById("waitRepCnt").title="<%=mm.getMessage("CPMG_1056",beforeDay,s_user.getLocale())%>";
	document.getElementById("waitUserCnt").title="<%=mm.getMessage("CPMG_1057",s_user.getLocale())%>";
}
function doAjax(){
	var url = G_SERVLETURL+"?mod=getCountCP&USER_ID=<%=s_user.getId().toString()%>";
	sendRequest(doAjaxEnd, " " ,"POST", url , false, false);
}

function doAjaxEnd(oj){
	var jData = JSON.parse(oj.responseText);
	 
	 var reqCnt = jData.reqCnt; //사용현황-신청건수
	 var aprovalCnt = jData.aprovalCnt; //사용현황-승인완료
	 var waitCnt = jData.waitCnt; //반출-승인대기
	 var noReadCnt = jData.noReadCnt; //반출-읽지않음 
	 var waitRepCnt = jData.waitRepCnt; //반입문서-대기건수 
	 var waitUserCnt = jData.waitUserCnt; //협업포탈 사용자 승인-승인대기 
	 var approvalUserCnt = jData.approvalUserCnt;
	 
	 loadCheck(reqCnt, aprovalCnt, waitCnt, noReadCnt, waitRepCnt,waitUserCnt, approvalUserCnt);	 
	
}
function loadCheck(reqCnt, aprovalCnt, waitCnt, noReadCnt, waitRepCnt,waitUserCnt, approvalUserCnt){
	//각 값들을 페이지에 보여주기 위한 처리다.
	
 	 //사용현황-신청건수: 현재까지 모든 신청건수, 즉, 신청,승인,반려 상관없이 자신이 신청했을 경우
	//document.getElementById("reqCnt").innerText=reqCnt;
	 //사용현황-승인완료: 신청한 건수 중에서 승인이 된거
	//document.getElementById("aprovalCnt").innerText=aprovalCnt;
	
	//반출-승인대기 : 현재 승인 대기 중인 건수
	document.getElementById("waitCnt").innerText=waitCnt;
	
	//반출-읽지않음 : 반출 후 수신자가 한번이라도 로그인을 하지않았을 경우 (수신자를 기준으로....문서번호가 아니라!!!!)
	document.getElementById("noReadCnt").innerText=noReadCnt;
	
	//반입문서-대기건수 : repacking 업로드 후 확인 또는 저장하지 않은 원문문서 
	document.getElementById("waitRepCnt").innerText=waitRepCnt;
	
	//협업포탈 사용자 승인-승인대기 : 자신을 담당자로 지정하여 등록한 승인을 기다리는 사용자 수.
	document.getElementById("waitUserCnt").innerText=waitUserCnt;
	
	//현재 자신이 승인한 파트너스 포탈 유저 수
	//document.getElementById("approvalUserCnt").innerText=approvalUserCnt;
	
}
function refresh(){
	init();
}
function clickBoard(flag){
	var url="<%=contextPath%>/cp/portal/contents/";
	var param="?gridid=GridObj&formid=";
	var leftMenuId="";
	switch(flag){
	case 1:
		url+="reqExportList.jsp";
		param +="CPP_0102&FLAG=1";
		leftMenuId="CPP_0102";
		break;
	case 2:
		url +="reqExportList.jsp";
		param+="CPP_0102&FLAG=2";
		leftMenuId="CPP_0102";
		break;
	case 3:
		<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
			url +="reqExportList_Iljin.jsp";
			param+="CPP_0104&FLAG=3";
			leftMenuId="CPP_0104";
		<%} else {%>
			url +="reqExportList.jsp";
			param+="CPP_0102&FLAG=3";
			leftMenuId="CPP_0102";
		<%}%>
		break;
	case 4:
		<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
			url +="reqExportList_Iljin.jsp";
			param+="CPP_0104&FLAG=4";
			leftMenuId="CPP_0104";
		<%} else {%>
			url +="reqExportList.jsp";
			param+="CPP_0102&FLAG=4";
			leftMenuId="CPP_0102";
		<%}%>
		break;
	case 5:
		url +="repackingList.jsp";
		param +="CPP_0201&FLAG=5";
		leftMenuId="CPP_0201";
		break;
	case 6:
		url +="portalUserApproval.jsp";
		param +="CPP_0301&FLAG=6";
		leftMenuId="CPP_0301";
		break;
	case 7:
		url +="portalUserList.jsp";
		param +="CPP_0302&FLAG=7";
		leftMenuId="CPP_0302";
		break;
	}
	 parent.pageRedirect(url, param);
	 top.onClickChangePage(leftMenuId.substring(0, leftMenuId.length-2), leftMenuId);
	 
}
</script>
</head>
<body onload="init();" oncontextmenu="return false">
  <div class="leftMenu">
      <a class="leftTop close" href="javascript:clsopnleftMenu();"><span class="Lnodisplay">접고 펼치기 버튼</span></a>
      <div class="leftBg">
        <div class="closeOpen">
          <div class="leftSystemInfo">
            <div class="leftSysname">
            	<ul class="leftLinks2">
	            	<%if("ILJIN".equals(pm.getString("component.site.company"))){ %>
	            	<li><span class="floatLeft"><p class="floatLeft" style="width:auto;"><%=deptNm%></p>
	            	<a href="javascript:logout();" class="btn leftSystemLogout"><img src="../../../ext/images/btn_logout1.gif" alt="Logout" /></a></span>
	            	<p><span><%=userNm%> <%=position%></span></p></li>
	                <%} else {%>
	    			<p><%=deptNm%></p>
	    			<p><span><%=userNm%> <%=position%></span></p>            
	                <%} %>
                </ul>
            </div>
            <div class="leftLinks">
              <ul>
                <li>
               <%--  <%-- <p><%=mm.getMessage("CPMG_1146",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1147",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(1)" id="reqCnt"></a></span>
                <p><%=mm.getMessage("CPMG_1146",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1148",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(2)" id="aprovalCnt"></a></span> --%>
                <div class="dashboard-title1"><%=mm.getMessage("CPMG_1149",s_user.getLocale())%></div>             
          		<p class="dashboard-txt1"><%=mm.getMessage("CPMG_1150",s_user.getLocale())%><span><a href="#" onclick="clickBoard(3)" id="waitCnt"></a></span></p>             		          
                </li>
                <li>                
                <%-- <p><%=mm.getMessage("CPMG_1149",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1150",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(3)" id="waitCnt"></a></span>
                <p><%=mm.getMessage("CPMG_1149",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1151",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(4)" id="noReadCnt"></a></span> --%>
                <div class="dashboard-title2"><%=mm.getMessage("CPMG_1204",s_user.getLocale())%></div>
                <p class="dashboard-txt2"><%=mm.getMessage("CPMG_1151",s_user.getLocale())%><span><a href="#" onclick="clickBoard(4)" id="noReadCnt"></a></span></p>                                            
                </li>
                <li>                
                <%-- <p><%=mm.getMessage("CPMG_1152",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1153",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(5)" id="waitRepCnt"></a></span> --%>
 
                <div class="dashboard-title3"><%=mm.getMessage("CPMG_1152",s_user.getLocale())%></div>
                <p class="dashboard-txt3"><%=mm.getMessage("CPMG_1151",s_user.getLocale())%><span><a href="#" onclick="clickBoard(5)" id="waitRepCnt"></a></span></p>                
                </li>
                <li>          
                <%-- <p><%=mm.getMessage("CPMG_1154",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1150",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(6)" id="waitUserCnt"></a></span>
                <p><%=mm.getMessage("CPMG_1154",s_user.getLocale())%>-<%=mm.getMessage("CPMG_1148",s_user.getLocale())%></p><span><a href="#" onclick="clickBoard(7)" id="approvalUserCnt"></a></span> --%>
                <div class="dashboard-title4"><%=mm.getMessage("CPMG_1154",s_user.getLocale())%></div>
                <p class="dashboard-txt4"><%=mm.getMessage("CPMG_1153",s_user.getLocale())%><span><a href="#" onclick="clickBoard(6)" id="waitUserCnt"></a></span></p>
                </li>
              </ul>
            </div>
          </div>
          <ul id="leftMenuBlock" class="slideMenu">
                 <%
       		   
                 String parentMenuName =menu.getName(s_user.getLocale());
                 out.println("<li><a class='leftDepth1 first'><span>"+parentMenuName+"</span></a><ul class='2depthArea'>");
		        for (int i = 0; i < menu.getChildren().size(); i++) {
		            
		            IMenu child = (IMenu) menu.getChildren().get(i);
		            String menu_name = child.getName(s_user.getLocale());
        			String menu_link = child.hasTarget() ? child.getTarget() : "N/A";
        			String menuId = child.getId();
        			
              		if(!menuId.equals("") && i < 4) {
						//out.println("<li><a class='leftDepth2' href='#' onclick=clickLeftMenu('"+menu_link+"','"+menuId+"','"+parentMenuName+"','"+menu_name+"')'><span>"+menu_name+"</span></a></li>");
              		%>	
              		<li><a class='leftDepth2' href='#' id='<%=i%>' onclick='clickLeftMenu("<%=menu_link%>","<%=menuId%>","<%=java.net.URLEncoder.encode(parentMenuName,"utf-8")%>","<%=java.net.URLEncoder.encode(menu_name,"utf-8")%>","<%=i%>")'>
              				<span id='<%=menuId %>' class='off'>- <%=menu_name%></span>
              			</a>
              		</li>
              		<%} else {
						//out.println("<li class='last'><a parent.leftFrame.location.href='"+(val.equals("") ? "javascript:;" : "menu_main_left.jsp?top_menuname="+java.net.URLEncoder.encode(menu_name, "utf-8")+"&url="+menu_link+"&MENU_OBJECT_CODE="+val)+"' target='left' onClick=menu_click('"+i+"','"+val+"','"+left_image+"') onFocus='this.blur()'><img src='"+menu_image+"'  id='juil_"+i+"' onMouseOver=change('"+ over_image+"','"+i+"') onMouseOut=change('"+ down_image+"','"+i+"') style='cursor:hand'></a></li>"+"\n");
              		}
        		}
		         out.println("</ul></li>");
				%>
          </ul>
          <div class="leftBottom"> </div>
        </div>
      </div>
    </div>
</body>
</html>