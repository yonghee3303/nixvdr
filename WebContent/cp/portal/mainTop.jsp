<%@ page contentType = "text/html; charset=UTF-8"  session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="java.util.Locale"%>
<%
	String type = request.getParameter("type");
    IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
	IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
    IUser s_user = (IUser) session.getAttribute("j_user");
    
	IMenu menu = mnu.getMenu("CPP", s_user);
	
	IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	
    String initpage = paramemter.getString("component.site.initpage");
    String initmenu = paramemter.getString("component.site.initmenu");
    String logo_image =  paramemter.getString("component.site.image");
    String menuimg =  paramemter.getString("component.site.initmenuimage");

    String m_path = ("en".equals(s_user.getLocale().getLanguage()) ? "/ext/images/en/" : "/ext/images/");
	String userNm = s_user.getName();
	String deptNm = s_user.getProperty("DEPT_NM").toString();
	
	String css = paramemter.getString("component.ui.portal.css");
	String contextPath = request.getContextPath();
	String locale = s_user.getLocale().toString();
	if(locale.equals("ko")) {
		locale = "";
	} else if(locale.equals("en") || locale.equals("zh")) {
		locale = "/" + locale;
	} else {
		locale = "/en";
	} 
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=contextPath%>/ext/css/cooperation_<%=css %>.css" />
<script type="text/javascript" src="<%=contextPath%>/ext/js/lgchem/jquery-1.9.0.js"></script>
<!-- <script type="text/javascript" src="../../ext/js/lgchem/common.js"></script> -->
<!-- <link rel="stylesheet" type="text/css" href="../../ext/css/common.css" /> -->
<script type="text/javascript" src="<%=contextPath%>/ext/js/ui_scripts.js"></script>
<script language="text/javascript" src="<%=contextPath%>/ext/js/sec.js"></script>
<script language="javascript">

function clickTopMenu(link, flag)
{	
	changeOnTopMenu(flag); //메뉴 활성화
	parent.clickTop(link, flag);
}
function changeOnTopMenu(menu_id){
	<% for (int i = 0; i < menu.getChildren().size(); i++) { %>
	document.getElementById("<%=i%>").firstChild.className="off";
	<% } %>
	document.getElementById(menu_id).className="on";
}
function init(){
	parent.init();
}
</script>
</head>
<body oncontextmenu="return false">
<form name="search" style="margin-bottom:0px">
	<input type="hidden" name="btn_flg" 			value=''>
	<input type="hidden" name="order_seq" 			value=''>
	<input type="hidden" name="menu_type" 			value=''>
	<input type="hidden" name="totaldata" 			value=''>
	<input type="hidden" name="lastLoginDateTime" 	value=''>
	<input type="hidden" name="compare_menu_type" 	value=''>
	<input type="hidden" name="flag" 				value=''>
	<input type="hidden" name="top_menuname"		value=''>
	<input type="hidden" name="url" 				value=''>
	<input type="hidden" name="MENU_OBJECT_CODE" 	value=''>
	<input type="hidden" name="mileStoneName"		value="">	
	<input type="hidden" name="leftMenuName"  		value="">
</form>
<div class="header">
    <a class="hLogo" href="#" onclick="init();"><img src="<%=contextPath%>/ext/images/<%=css %><%=locale%>/logo_header.gif" /></a>
    <ul class="hMenu">
     <%
		        for (int i = 0; i < menu.getChildren().size(); i++) {
		            
		        	IMenu child = (IMenu) menu.getChildren().get(i);
		            String menu_name = child.getName(s_user.getLocale());
		            /*
		            String menu_name = null;
		            if(s_user.getLocale().toString().equals("ko")) {
		            	menu_name = child.getName(s_user.getLocale());
		            } else {
		            	menu_name = child.getName(Locale.US);
		            }
		            */
        			String menu_link = child.hasTarget() ? child.getTarget() : "N/A";
        			String menuId = child.getId();
        			
              		if(!menuId.equals("")) {
						out.println("<li id='"+i+"'class='hUser"+i+"'>");
						out.println("<a href='#' id='"+menuId+"' class='off' onclick=clickTopMenu('"+menu_link+"','"+menuId+"')>"+menu_name+"</a>");
						out.println("</li>");
              		}
        		}
	%>
	</ul>
 </div>
</body>
</html>
