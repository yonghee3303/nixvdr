<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String userIp = request.getRemoteAddr();
String contextPath = request.getContextPath();
%>

<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>

<title>보안반출 정책 관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../ext/include/su_grid_common_render.jsp"%>
<script>

var GridObj 		= {};
var G_SERVLETURL = "/mg/cpgrp.do";
var myDataProcessor = {};
var clickAdd=0;
var ipAddr;

var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var selRowId;

function init() {
	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	GridObj.attachEvent("onRowDblClicked", doOnRowSelected);
   	GridObj.attachEvent("onXLE", doQueryEnd);
   	doQuery();
}
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=getCpGrpList&grid_col_id="+grid_col_id+"&POLICY_CL_CD=2");																						
	GridObj.clearAll(false);	
}

function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	
	if(selRowId!=null){
		doOnRowSelected(selRowId,1);
	}
	
	return true;
}

function doOnRowSelected(rowId,cellInd) {
	selRowId = rowId;
	clickAdd =0;
	
	if(GridObj.getRowsNum() !=0){
		var allId = GridObj.getAllRowIds().split(",");
		for(var i=0; i < allId.length; i++){
			GridObj.cells(allId[i], GridObj.getColIndexById("SELECTED")).setValue("0");
		}
	}
	
	GridObj.cells(rowId, GridObj.getColIndexById("SELECTED")).setValue("1");
	document.form.POLICY_NO.value = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_NO")).getValue();
	var policyNm = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_NM")).getValue();
	document.form.POLICY_NM.value=policyNm;
	
	var expireDay =GridObj.cells(rowId, GridObj.getColIndexById("EXPIRE_DAY")).getValue();
	document.form.EXPIRE_DAY.value=expireDay;
	var readCnt = GridObj.cells(rowId, GridObj.getColIndexById("READ_COUNT")).getValue();
	document.form.READ_COUNT.value=readCnt;
	
	var outCheck =document.getElementsByName("OUTDEVICE");
	for(var i=1; i <= outCheck.length; i++){
		eval("document.getElementById(\"OUTDEVICE_0"+i+"\").checked=false;");
    }

	
	var outDevice = GridObj.cells(rowId, GridObj.getColIndexById("OUTDEVICE")).getValue();

	var outDeviceArray = outDevice.split(",");
	for(var i=0; i<outDeviceArray.length; i++){
		for(var j=1; j <= outCheck.length; j++){
		/* 	alert(eval("document.getElementById(\"OUTDEVICE_0"+j+"\").value")+" / "+outDeviceArray[i]); */
			if(eval("document.getElementById(\"OUTDEVICE_0"+j+"\").value") == outDeviceArray[i]){
				eval("document.getElementById(\"OUTDEVICE_0"+j+"\").checked=true;");
			}
		}
	}

	var policy = GridObj.cells(rowId, GridObj.getColIndexById("POLICY")).getValue();

	if(policy.substring(1,2) == "1") {
		document.form.chkExpireDay.checked = true;
	} else {
		document.form.chkExpireDay.checked = false;
	}
	
	if(policy.substring(2,3) == "1") {
		document.form.chkReadCnt.checked = true;
	} else {
		document.form.chkReadCnt.checked = false;
	}
	
}
function doAddRow() {

	if(GridObj.getRowsNum() !=0){
		var allId = GridObj.getAllRowIds().split(",");
		for(var i=0; i < allId.length; i++){
			GridObj.cells(allId[i], GridObj.getColIndexById("SELECTED")).setValue("0");
		}
	}
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	selRowId = nMaxRow2;
	ed_flag = "Y";
	
	document.form.POLICY_NO.value="";
	document.form.POLICY_NM.value="";
	document.form.chkExpireDay.checked=false;
	document.form.EXPIRE_DAY.value="";
	document.form.chkReadCnt.checked=false;
	document.form.READ_COUNT.value="";
	var outCheck =document.getElementsByName("OUTDEVICE");
	for(var i=1; i <= outCheck.length; i++){
		eval("document.getElementById(\"OUTDEVICE_0"+i+"\").checked=false;");
    }
}
function doInsertSet() {
	var policyNo = document.form.POLICY_NO.value;
	var policyNm = document.form.POLICY_NM.value;
	if(policyNm ==""){
		alert("정책명을 입력하지 않으셨습니다.");
		return;
	}
	var expireDay="";
	var readCnt="";
	var policy="0";

	if(document.form.chkExpireDay.checked){
		policy +="1";
		expireDay = document.form.EXPIRE_DAY.value;
		if(expireDay=="" || expireDay=="0"){
			alert("유효기간을 입력하세요.");
			return;
		}
	}else{
		policy +="0";
	}
	if(document.form.chkReadCnt.checked){
		policy +="1";
		readCnt = document.form.READ_COUNT.value;
		if(readCnt =="" || readCnt=="0"){
			alert("다운로드 가능횟수를 입력하세요.");
			return;
		}
	}else{
		policy +="0";
	}
	policy +="00"; //문서권한 및 Log Tracking은 기본적으로 사용할수 없음

	var policyDoc="00000";
	var outDevice="";
	var tmpCnt=0;
	var outCheck =document.getElementsByName("OUTDEVICE");
		for(var i=1; i <= outCheck.length; i++){
			if(eval("document.getElementById(\"OUTDEVICE_0"+i+"\").checked")){
				if(tmpCnt==0){
					outDevice=eval("document.getElementById(\"OUTDEVICE_0"+i+"\").value;");
				}else{
					outDevice+=","+eval("document.getElementById(\"OUTDEVICE_0"+i+"\").value;");
				}
				tmpCnt++;
			}
		}
/* 	var outRadio =document.getElementsByName("OUTDEVICE");
	for(var i=0; i < outRadio.length; i++){
		if(outRadio[i].checked){
			outDevice=outRadio[i].value;
		}
    } */
	
	var url = G_SERVLETURL + "?mod=saveCpGrp";
		url+= "&POLICY_NO=" + policyNo;
		url+= "&POLICY_NM=" + encodeURIComponent(policyNm); 
		url+= "&POLICY=" + policy;
		url+= "&POLICY_DOC=" +policyDoc;
		url+= "&EXPIRE_DAY="+expireDay;
		url+= "&READ_COUNT="+readCnt;
		url+= "&IS_CHANGE=0";
		url+= "&IPADDR=";
		url+= "&IS_IPADDR=N";
		url+= "&POLICY_CL_CD=2";
		url+= "&POLICY_DISP=(I)(N)(P)(C)(R)";
		url+= "&OUTDEVICE="+outDevice;
	sendRequest(doInsertSetEnd, " " ,"POST", url , false, false);
}

function doInsertSetEnd(){
	alert("저장 되었습니다.");
	doQuery();
}
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doDelRow(){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteCpGrp&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}
function doSaveEnd(obj){
	var messsage = obj.getAttribute("message");
	var status   = obj.getAttribute("status");		
	var mode     = obj.getAttribute("mode");
	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	} 
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}
</script>

<style>

div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
	
	/* Tabbar Web스킨 사용시 간격조정 */
	.dhxtabbar_base_dhx_web div.dhx_cell_tabbar div.dhx_cell_cont_tabbar {
		
		padding: 0px;
		
	}

</style>


</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:5px; overflow: auto;">
<form name="form" method="post">
<input type="hidden" name="POLICY_NO" value=""/>
<table class="topPolicySet">
	<tr>
		<th class="topth"><%=mm.getMessage("CPMG_1223", s_user.getLocale())%></th>
		<td class="toptd">
			<table class="createPolicySet">
				<tr>	
					<td>
						<div id="gridbox" style="width:100%; height:250px; background-color:white; overflow:hidden;"></div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="btnarea">
	            			<a class="btn" onclick="doDelRow()"><span><%=mm.getMessage("CPMG_1183", s_user.getLocale())%></span></a>
	            			<a class="btn" onclick="doAddRow()"><span><%=mm.getMessage("CPMG_1224", s_user.getLocale())%></span></a> 
	            		</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th class="topth"><%=mm.getMessage("CPMG_1226", s_user.getLocale())%></th>
		<td class="toptd">
			<table>
				<tr>
					<td>
						<%=mm.getMessage("CPMG_1225", s_user.getLocale())%> <input type="text" size="40" name="POLICY_NM"/>
					</td>
					<td>
						
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th rowspan="2" class="topth"><%=mm.getMessage("CPMG_1227", s_user.getLocale())%></th>
		<td class="toptd">
			<table>
				<tr>
					<td style="width:50%;"><input type="checkbox" name="chkExpireDay"> <%=mm.getMessage("CPMG_1230", s_user.getLocale())%></td>
					<td ><%=mm.getMessage("CPMG_1222", s_user.getLocale())%> <input type="text" size="3" name="EXPIRE_DAY"> <%=mm.getMessage("COMG_1069", s_user.getLocale())%></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<table>
				<tr>
					<td style="width:50%;"><input type="checkbox" name="chkReadCnt"> <%=mm.getMessage("CPMG_1228", s_user.getLocale())%></td>
					<td><input type="text" size="3" name="READ_COUNT"> <%=mm.getMessage("COMG_1017", s_user.getLocale())%></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th class="topth"><%=mm.getMessage("CPMG_1229", s_user.getLocale())%></th>
		<td class="toptd">
			<table>
				<tr>
					<td>
						<jsp:include page="/mg/common/chkBoxPolicy.jsp" >
							<jsp:param name="codeId" value="A0313"/>
							<jsp:param name="tagname" value="OUTDEVICE"/>
						</jsp:include>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>	
	</tr>
	<tr>                            
		<td></td>
		<td>
			<div class="btnarea">
	        	<a class="btn" onclick="doInsertSet()"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a> 
	        </div>
		</td>
	</tr>
</table>
</form>
</body>
</html>