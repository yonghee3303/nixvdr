<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
String editFormId = formId.substring(0, 7); //formId = 'CP_051003' 메뉴를 바꾸면서 formId가 바뀌게 되어 추가로 이러한 처리를 해주었다.
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>CP 예외정책 등록</title>
<%@ include file="../../ext/include/include_css.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script>

function init() 
{
	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>" + "&user_id=";
	masterframe.location.href='<%=contextPath%>/cp/policy/cpExcPolicyMaster.jsp?eventframe=masterframe' + argu;
	refresh("");
}

function refresh(excNo) 
{	
	var argu = "&formid=<%=editFormId%>01&gridid=<%=grid_obj%>&excNo=" + excNo;
	detailframe1.location.href='<%=contextPath%>/cp/policy/cpExcPolicyDetail1.jsp?' + argu;
	
	argu = "&formid=<%=editFormId%>02&gridid=<%=grid_obj%>&excNo=" + excNo;
	detailframe2.location.href='<%=contextPath%>/cp/policy/cpExcPolicyDetail2.jsp?' + argu;
}

function doQuery() 
{
	masterframe.doQuery();
}

function getExcNo()
{
		
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<%@ include file="../../mg/common/milestone.jsp"%>
   </td>
</tr>

<tr>
	<td>
      <table width="100%" border="0">
       <rowgroup>
            <row height="60%" />
            <row height="20%" />
        </rowgroup>
        <tr>
          <td valign="top">
				<iframe name="masterframe" width="100%"  height="300" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
        </tr>
        <tr>
          <td>
               <table width="100%" border="0">
			        <colgroup>
			            <col width="50%" />
			            <col width="50%" />
			        </colgroup>
          			<tr>
          				<td>
          					<iframe name="detailframe1" width="100%"  height="200" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
          				</td>
          				<td>
          					<iframe name="detailframe2" width="100%"  height="200" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
          				</td>
          			</tr>
          		</table>
           </td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
</body>
</html>
