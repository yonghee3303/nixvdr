<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html>
<head>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>
<title>CP 정책 세트 관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<script>

var tabbar;

function init() {

	tabbar = new dhtmlXTabBar("tabbar");
	
	tabbar.addTab("exePolicy","보안반출 정책",null,null,true);
	tabbar.addTab("originalPolicy","원문반출 정책");

	tabbar.tabs("exePolicy").attachURL("/cp/policy/exePolicySetMgmt.jsp", null, {
		formid: "<%=request.getParameter("formid")%>",
		gridid: "GridObj"
	});	
	tabbar.tabs("originalPolicy").attachURL("/cp/policy/originalPolicySetMgmt.jsp", null, {
		formid: "<%=request.getParameter("formid")%>",
		gridid: "GridObj"
	});
	
	tabbar.enableAutoReSize(); 
}

</script>

<style>
/* Tabbar Web스킨 사용시 간격조정 */
.dhxtabbar_base_dhx_web div.dhx_cell_tabbar div.dhx_cell_cont_tabbar {	
	padding: 0px;
}

/* .board-search { width:100%; margin-bottom:10px; border:1px solid #dde1e3;} */
.board-search { width:100%; margin-bottom:10px; border:0;}
.board-search td { font-size:12px; text-align:left; padding-left:10px; border-bottom:0; background:#ffffff;}
	
/* .board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#f4f4f4 url(../images/bul_arrow3.gif) 8px 50% no-repeat;} */
.board-search td.tit { font-size:12px; padding-left:16px; height:27px; text-align:left; background:#ffffff url(../../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}

.btnarea {
	background-color:#ffffff;
}	
</style>
</head>
<body onload="init();" style="margin: 0px; padding:5px; overflow: auto;">
<div id="tabbar" style="width:100%; height:90%;"></div>
</body>
</html>