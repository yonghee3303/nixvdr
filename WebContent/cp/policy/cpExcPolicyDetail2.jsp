<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");

  String excNo= StringUtils.paramReplace(request.getParameter("excNo"));
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>CP 예외정책 제한폴더등록</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common.jsp"%> --%>
<%@ include file="../../ext/include/su_grid_common_render.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp"%> 

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/cp/policy.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
	GridObj.attachEvent("onRowDblClicked", doOnRowSelected);
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	
	row_id = rowId;
	if( header_name == "FOLDER_ICON") {
		openPopupCode("드라이브 코드 선택", '', '', 440, 400, "A0311", "OkPopupCode",<%=pm.getString("component.contextPath.root")%>);
	} 
}

function OkPopupCode(cd_key, cd_nm) {
    GridObj.cells(row_id, GridObj.getColIndexById("FOLDER_ROOT")).setValue(cd_key);
    checkGrid(GridObj, row_id);
}

// 데이터 조회시점에 호출되는 함수입니다.
function doQuery() 
{
	if("<%=excNo%>" == "") {
		// alert("정책이 선택되지 않았습니다.");
		return;
	}
	var argu = "&EXC_NO=<%=excNo%>";
	var grid_col_id = "<%=grid_col_id%>";	
	GridObj.loadXML(G_SERVLETURL+"?mod=selectRestFolder&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	// if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		return true;
	}
	return false;
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

function doAddRow() {
   	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
   	
   	if("<%=excNo%>" == "") {
   	  alert("정책을 선택 하셔야 예외 허용폴더를 등록 할 수 있습니다.");
   	  return;
   	}
   	  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXC_NO")).setValue("<%=excNo%>");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("FOLDER_ICON")).setValue("../../ext/images/button/bt_search.gif");
	
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";
}

function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
		
	if (confirm("<%=mm.getMessage("COMG_1009", s_user.getLocale())%>")) {  // 저장하시겠습니까?
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=saveRestFolder&col_ids="+cols_ids;	
		myDataProcessor = new dataProcessor(SERVLETURL);
	    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		for(var i=0; i < grid_array.length; i++) {
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("EXC_NO")).getValue() == "") {
				GridObj.deleteRow(grid_array[i]);
			}
		}
		grid_array = getGridChangedRows(GridObj, "SELECTED");
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteRestFolder&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function init() {
	setFormDraw();
	doQuery();
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<table width="100%" border="0">
        <colgroup>
            <col width="30%" />
            <col width="40%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<a href="javascript:doAddRow();" class="btn"><span><%=mm.getMessage("COMG_1011", s_user.getLocale())%></span></a>
	            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("COMG_1013", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=30"/>
</jsp:include>
</body>
</html>
