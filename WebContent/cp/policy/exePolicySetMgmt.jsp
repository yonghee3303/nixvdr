<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String userIp = request.getRemoteAddr();
String contextPath = request.getContextPath();
%>

<html>
<head>
<!-- Json 사용시 참조해야할 js -->
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>

<title>보안반출 정책 관리</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>
<%@ include file="../../ext/include/su_grid_common_render.jsp"%>
<script>

var GridObj 		= {};
var G_SERVLETURL = "/mg/cpgrp.do";
var myDataProcessor = {};
var clickAdd=0;
var ipAddr;

var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";
var selRowId;

function init() {
	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	GridObj.attachEvent("onRowDblClicked", doOnRowSelected);
   	GridObj.attachEvent("onXLE", doQueryEnd);
   	doQuery();
}
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	GridObj.loadXML(G_SERVLETURL+"?mod=getCpGrpList&grid_col_id="+grid_col_id+"&POLICY_CL_CD=1");																						
	GridObj.clearAll(false);	
}

function doQueryEnd(GridObj, RowCnt) {

	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	if(status == "false") alert(msg);
	
	if(selRowId!=null){
		doOnRowSelected(selRowId,1);
	}
	
	return true;
}

function doOnRowSelected(rowId,cellInd) {
	selRowId = rowId;
	clickAdd =0;
	
	if(GridObj.getRowsNum() !=0){
		var allId = GridObj.getAllRowIds().split(",");
		for(var i=0; i < allId.length; i++){
			GridObj.cells(allId[i], GridObj.getColIndexById("SELECTED")).setValue("0");
		}
	}
	
	GridObj.cells(rowId, GridObj.getColIndexById("SELECTED")).setValue("1");
	document.form.POLICY_NO.value = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_NO")).getValue();
	var policyNm = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_NM")).getValue();
	document.form.POLICY_NM.value=policyNm;
	
	var expireDay =GridObj.cells(rowId, GridObj.getColIndexById("EXPIRE_DAY")).getValue();
	document.form.EXPIRE_DAY.value=expireDay;
	var readCnt = GridObj.cells(rowId, GridObj.getColIndexById("READ_COUNT")).getValue();
	document.form.READ_COUNT.value=readCnt;

	var isChange = GridObj.cells(rowId, GridObj.getColIndexById("IS_CHANGE")).getValue();
	if(isChange =="1"){
		document.form.chkIsChange.checked=true;
	}else{
		document.form.chkIsChange.checked=false;
	}
	
	
	
	var macYn = GridObj.cells(rowId, GridObj.getColIndexById("MAC_YN")).getValue();
	if(macYn =="Y"){
		document.form.chkMac.checked=true;
	}else{
		document.form.chkMac.checked=false;
	}
	
	// 김민정 수정 검토 사항 (임성진과 상의)
	// POLICY_CL_CD 는 원문반출 or 보안반출인데 화면은 서버인증 or 자체 인증 으로 표시 되는지 .... 화면 수정후 제대로 확인하지 않은 듯 보임
	// 보안 1, 원문 2, 자체 3 으로 관리되어짐 
	var exportType = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_CL_CD")).getValue();
	var exportTypeRadio = document.form.exportType;
	for(var i=0; i < exportTypeRadio.length; i++){
		exportTypeRadio[i].checked=false;
    }
	for(var i=0; i < exportTypeRadio.length; i++){
		if(exportTypeRadio[i].value==exportType){
			exportTypeRadio[i].checked=true;
		}
    }
	changeExportType(exportType);
	
	var policy = GridObj.cells(rowId, GridObj.getColIndexById("POLICY")).getValue();
/* 	if(policy.substring(0,1) == "1") { //Network연결 실패시 조회 금지
		document.form.chkNetwork.checked = true;
	} else {
		document.form.chkNetwork.checked = false;
	}  */

	if(policy.substring(1,2) == "1") { //만료일자 사용 여부
		document.form.chkExpireDay.checked = true;
	} else {
		document.form.chkExpireDay.checked = false;
	}
	
	if(policy.substring(2,3) == "1") { //추출 횟수 사용 여부
		document.form.chkReadCnt.checked = true;
	} else {
		document.form.chkReadCnt.checked = false;
	}
	
	
/* 	if(policy.substring(3,4) == "1") { //문서 권한 설정 사용 여부
		document.form.chkDocAuth.checked = true;
	} else {
		document.form.chkDocAuth.checked = false;
	}
	 */
	
	/* if(policy.substring(4,5) == "1") { // Log Tracking 사용 여부
		document.form.chkLogTracking.checked = true;
	} else {
		document.form.chkLogTracking.checked = false;
	} */
	// 문서권한
	var policyDoc = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_DOC")).getValue();
/* 	if(policyDoc.substring(0,1) == "1") { // 저장,수정 금지
		document.form.chkSave.checked = true;
	} else {
		document.form.chkSave.checked = false;
	} 
	 */
	if(policyDoc.substring(1,2) == "1") { //출력 금지
		document.form.chkPrint.checked = true;
	} else {
		document.form.chkPrint.checked = false;
	}
	if(policyDoc.substring(2,3) == "1") { //Clip Board 금지
		document.form.chkClipCopy.checked = true;
	} else {
		document.form.chkClipCopy.checked = false;
	}
	if(policyDoc.substring(3,4) == "1") { //Repacking 금지
		document.form.chkRepacking.checked = true;
	} else {
		document.form.chkRepacking.checked = false;
	}
	if(policyDoc.substring(4,5) == "1") { // 외부로 문서복사 금지 
		document.form.chkExtCopy.checked = true;
	} else {
		document.form.chkExtCopy.checked = false;
	}
	var chkIp = GridObj.cells(rowId, GridObj.getColIndexById("IS_IPADDR")).getValue();
	if(chkIp == "Y") {
		document.form.chkIp.checked = true;
	} else {
		document.form.chkIp.checked = false;
	} 
	ipAddr = GridObj.cells(rowId, GridObj.getColIndexById("IPADDR")).getValue();
	createIpTable(ipAddr);
	
	var editCd = GridObj.cells(rowId, GridObj.getColIndexById("USEREDIT_CD")).getValue();
	
	switch(editCd){
		case "0": //수정허용하지 않음
			document.form.chkEdit1.checked=false;
			document.form.chkEdit2.checked=false;
			break;
		case "1": //인증횟수 수정허용
			document.form.chkEdit1.checked=true;
			document.form.chkEdit2.checked=false;
			break;
		case "2": //만료일자 수정허용
			document.form.chkEdit1.checked=false;
			document.form.chkEdit2.checked=true;
			break;
		case "8": //인증횟수/만료일자 수정허용
			document.form.chkEdit1.checked=true;
			document.form.chkEdit2.checked=true;
			break;
	}

	
}
function createIpTable(ipAddrStr){
	var ipAddrArray = ipAddrStr.split(",");
	var tableHtml = "<table class='ipAddrInput'>";
	if(ipAddrStr !="" && ipAddrStr!=null){
		for(var i=0; i<ipAddrArray.length;i++){
			var ipArray = ipAddrArray[i].split("~");
			var tempIp1 = ipArray[0].split(".");
			tableHtml += "<tr id='ipList"+i+"'><td class='iptd'><input type='checkbox' name='chkIp"+i+"'></td>";
			tableHtml += "<td class='iptd'><input type='text' size='3' name='fromIp"+i+"_0' value='"+tempIp1[0]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_1' value='"+tempIp1[1]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_2' value='"+tempIp1[2]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_3' value='"+tempIp1[3]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += "~";
			var tempIp2;
			if(ipArray.length>1){
				tempIp2 = ipArray[1].split(".");
				tableHtml += "<input type='text' size='3' name='toIp"+i+"_0' value='"+tempIp2[0]+"' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_1' value='"+tempIp2[1]+"' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_2' value='"+tempIp2[2]+"' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_3' value='"+tempIp2[3]+"' onkeypress='return onlyNumber(this, event)'></td></tr>";
			}else{
				tableHtml += "<input type='text' size='3' name='toIp"+i+"_0' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp"+i+"_1' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp"+i+"_2' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp"+i+"_3' value='' onkeypress='return onlyNumber(this, event)'></td></tr>";
			}
		}
	}else{
		tableHtml += "<tr id='ipList"+i+"'><td class='iptd'><input type='checkbox' name='chkIp"+1+"'></td>";
		tableHtml += "<td class='iptd'><input type='text' size='3' name='fromIp0_0' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='fromIp0_1' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='fromIp0_2' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='fromIp0_3' value='' onkeypress='return onlyNumber(this, event)'>";
		tableHtml += "~";
		tableHtml += "<input type='text' size='3' name='toIp0_0' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp0_1' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp0_2' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp0_3' value='' onkeypress='return onlyNumber(this, event)'></td></tr>";
	}
	tableHtml +="<tr><td colspan='2' class='ipBtn'><div class='btnarea'><a href='#' class='btn' onclick='delRow()'><span>선택 삭제</span></a><a href='#' class='btn' onclick='addTableRow()'><span>구간 추가</span></a></div></td></tr></table>";
	//document.getElementById("divIpAddr").innerHTML=tableHtml;
	
	removeAllChild(document.getElementById("divIpAddr"));
	document.getElementById("divIpAddr").insertAdjacentHTML("afterBegin", tableHtml);
}
function removeAllChild(nodeTemp){
	while(nodeTemp.hasChildNodes()){
		nodeTemp.removeChild(nodeTemp.lastChild);
	}
}
function getIpAddr(){
	var editIpAddrArray = document.getElementById("divIpAddr").innerHTML.split("ipList");
	var editIpAddr="";
	for(var i=0; i<(editIpAddrArray.length-1); i++){
		if(eval("document.form.fromIp"+i+"_0.value") != ""){
			if(editIpAddr.length !=0){
				editIpAddr +=",";
			}
			editIpAddr += eval("document.form.fromIp"+i+"_0.value")+"."+eval("document.form.fromIp"+i+"_1.value")+"."+eval("document.form.fromIp"+i+"_2.value")+"."+eval("document.form.fromIp"+i+"_3.value");
			if(eval("document.form.toIp"+i+"_0.value") != ""){
				editIpAddr +="~";
				editIpAddr +=eval("document.form.toIp"+i+"_0.value")+"."+eval("document.form.toIp"+i+"_1.value")+"."+eval("document.form.toIp"+i+"_2.value")+"."+eval("document.form.toIp"+i+"_3.value");
			}
		}
	}
	return editIpAddr;
}
function addTableRow(){
	clickAdd++;
	var ipAddrArray =getIpAddr().split(",");
	var tableHtml = "<table class='ipAddrInput'>";
	for(var i=0; i<(ipAddrArray.length+clickAdd);i++){
		if(i<ipAddrArray.length && ipAddrArray!=""){
			var ipArray = ipAddrArray[i].split("~");
			tempIp1 = ipArray[0].split(".");
			tableHtml += "<tr id='ipList"+i+"'><td class='iptd'><input type='checkbox' name='chkIp"+i+"'></td>";
			tableHtml += "<td class='iptd'><input type='text' size='3' name='fromIp"+i+"_0' value='"+tempIp1[0]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_1' value='"+tempIp1[1]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_2' value='"+tempIp1[2]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_3' value='"+tempIp1[3]+"' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += "~";
			if(ipArray.length>1){
				tempIp2 = ipArray[1].split(".");
				tableHtml += "<input type='text' size='3' name='toIp"+i+"_0' value='"+tempIp2[0]+"' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_1' value='"+tempIp2[1]+"' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_2' value='"+tempIp2[2]+"' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_3' value='"+tempIp2[3]+"' onkeypress='return onlyNumber(this, event)'></td></tr>";
			}else{
				tableHtml += "<input type='text' size='3' name='toIp"+i+"_0' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp"+i+"_1' value='' onkeypress='return onlyNumber(this, event)'>";
				tableHtml += ".<input type='text' size='3' name='toIp"+i+"_2' value='' onkeypress='return onlyNumber(this, event)'>.<input type='text' size='3' name='toIp"+i+"_3' value='' onkeypress='return onlyNumber(this, event)'></td></tr>";
			}
		}else{
			tableHtml += "<tr id='ipList"+i+"'><td class='iptd'><input type='checkbox' name='chkIp"+i+"'></td>";
			tableHtml += "<td class='iptd'><input type='text' size='3' name='fromIp"+i+"_0' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_1' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_2' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='fromIp"+i+"_3' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += "~";
			tableHtml += "<input type='text' size='3' name='toIp"+i+"_0' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='toIp"+i+"_1' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='toIp"+i+"_2' value='' onkeypress='return onlyNumber(this, event)'>";
			tableHtml += ".<input type='text' size='3' name='toIp"+i+"_3' value='' onkeypress='return onlyNumber(this, event)'></td></tr>";
		}
	}
	tableHtml +="<tr><td colspan='2' class='ipBtn'><div class='btnarea'><a href='#' class='btn' onclick='delRow()'><span>선택 삭제</span></a><a href='#' class='btn' onclick='addTableRow()'><span>구간 추가</span></a></div></td></tr></table>";
	removeAllChild(document.getElementById("divIpAddr"));
	document.getElementById("divIpAddr").insertAdjacentHTML("afterBegin", tableHtml);
	//document.getElementById("divIpAddr").innerHTML=tableHtml;
}
function delRow(){
	var ipAddrArray =getIpAddr().split(",");
	for(var i=0; i<ipAddrArray.length;i++){
		if(eval("document.form.chkIp"+i+".checked")==true){
			for(var j=0; j<4; j++){
				eval("document.form.fromIp"+i+"_"+j+".value=''");
				eval("document.form.toIp"+i+"_"+j+".value=''");
			}
		}
	}
	clickAdd--;
	addTableRow();
}
//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}
function doDelRow(){
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("COMG_1010", s_user.getLocale())%>")) { //삭제하시겠습니까?
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteCpGrp&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}
function doAddRow() {
	
	if(GridObj.getRowsNum() !=0){
		var allId = GridObj.getAllRowIds().split(",");
		for(var i=0; i < allId.length; i++){
			GridObj.cells(allId[i], GridObj.getColIndexById("SELECTED")).setValue("0");
		}
	}
   	//dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id+1;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");

	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	selRowId = nMaxRow2;
	ed_flag = "Y";
	
	document.form.POLICY_NO.value="";
	document.form.POLICY_NM.value="";
    document.form.exportType.value="1";
	document.form.chkExtCopy.checked=false;
	document.form.chkPrint.checked=false;
	document.form.chkClipCopy.checked=false;
	document.form.chkRepacking.checked=false;
	document.form.chkExpireDay.checked=false;
	document.form.EXPIRE_DAY.value="";
	document.form.chkReadCnt.checked=false;
	document.form.READ_COUNT.value="";
	document.form.chkIsChange.checked=false;
	document.form.chkIp.checked=false;
	document.form.chkEdit1.checked=false;
	document.form.chkEdit2.checked=false;
	createIpTable("");
}
function doInsertSet() {
	
	var exportTypeRadio = document.form.exportType;
	var exportType;
	for(var i=0; i < exportTypeRadio.length; i++){
		if(exportTypeRadio[i].checked){
			exportType = exportTypeRadio[i].value;
		}
	}
	
	var policyNo = document.form.POLICY_NO.value;
	var policyNm = document.form.POLICY_NM.value;
	if(policyNm ==""){
		alert("정책명을 입력하지 않으셨습니다.");
		return;
	}
	var expireDay="";
	var readCnt="";
	var policy;
	var saveIpAddr="";
	/* if(document.form.chkNetwork.checked){ */
		policy ="1";
	/* }else{
		policy ="0"; 
	} */
	if(expireDay==""){
		expireDay="0";
	}
	if(document.form.chkExpireDay.checked){
		policy +="1";
		expireDay = document.form.EXPIRE_DAY.value;
		if(expireDay=="0"){
			alert("유효기간을 입력하세요.");
			return;
		}
	}else{
		document.form.chkEdit2.checked=false;
		policy +="0";
	}
	if(document.form.chkReadCnt.checked){
		policy +="1";
		readCnt = document.form.READ_COUNT.value;
		if(readCnt =="" || readCnt=="0"){
			alert("실행허용횟수를 입력하세요.");
			return;
		}
	}else{
		document.form.chkEdit1.checked=false;
		policy +="0";
	}
	policy +="11"; //문서권한 및 Log Tracking은 기본적으로 사용

	var policyDoc="1";//저장/수정 금지 : 해당 정책이 0이 되면 보안폴더가 아닌 그냥 폴더에 복사해버린다.

	if(document.form.chkPrint.checked){
		policyDoc +="1";
	}else{
		policyDoc +="0";
	}
	if(document.form.chkClipCopy.checked){
		policyDoc +="1";
	}else{
		policyDoc +="0";
	}
	if(document.form.chkRepacking.checked){ //리패킹금지
		policyDoc +="1";
	}else{
		if(exportType!="1"){
			policyDoc +="1";
		}else{
			policyDoc +="0";
		}
	}

	if(document.form.chkExtCopy.checked){
		policyDoc +="1";
	}else{
		policyDoc +="0";
	}
	
	var chkIsChange="";
	if(document.form.chkIsChange.checked){
		chkIsChange = "1";
	}else{
		chkIsChange = "0";
	}
	
	var macYn="";
	if(document.form.chkMac.checked){
		macYn = "Y";
	}else{
		macYn = "N";
	}
	
	var isIpAddr="";
	saveIpAddr=getIpAddr();
	if(saveIpAddr !=""){
		if(!validIpAddr(saveIpAddr)){
			alert("Ip 주소를 잘못 입력하셨습니다.");
			return;
		}
	}
	
	if(document.form.chkIp.checked){
		if(saveIpAddr==""){
			alert("Ip구간을 입력하세요.");
			return;
		}
		isIpAddr="Y";
	}else{
		isIpAddr="N";
	}

	<%if("Y".equals(pm.getString("component.ui.policy.classifyYn"))){%>
		policyNm = policyNm.replace(" [서버인증]","").replace(" [자체인증]","");
		if(exportType=="1"){
			policyNm+=" [서버인증]";
		}else{
			policyNm+=" [자체인증]";	
		}
	<%}%>
	
	if(exportType!="1"){	
		chkIsChange="0";
	}
	
	var editCd="0";

	if(document.form.chkEdit1.checked && document.form.chkEdit2.checked){
		editCd="8";
	}else if(document.form.chkEdit1.checked){
		editCd="1";
	}else if(document.form.chkEdit2.checked){
		editCd="2";
	}
	
	var url = G_SERVLETURL + "?mod=saveCpGrp";
		url+= "&POLICY_NO=" + policyNo;
		url+= "&POLICY_NM=" + encodeURIComponent(policyNm); 
		url+= "&POLICY=" + policy;
		url+= "&POLICY_DOC=" +policyDoc;
		url+= "&EXPIRE_DAY="+expireDay;
		url+= "&READ_COUNT="+readCnt;
		url+= "&IS_CHANGE="+chkIsChange;
		url+= "&IPADDR="+saveIpAddr;
		url+= "&POLICY_CL_CD="+exportType;
		url+= "&IS_IPADDR="+isIpAddr;
		url+= "&POLICY_DISP=(I)(N)(P)(C)(R)";
		url+= "&MAC_YN="+macYn;
		url+= "&USEREDIT_CD="+editCd;
	sendRequest(doInsertSetEnd, " " ,"POST", url , false, false);
}
function doInsertSetEnd(){
	alert("저장되었습니다.");
	doQuery();
}
function doSaveEnd(obj){
	var messsage = obj.getAttribute("message");
	var status   = obj.getAttribute("status");		
	var mode     = obj.getAttribute("mode");
	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	} 
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}
function validIpAddr(ip){
	
	 var ipAddressArray = ip.split(",");
	
	 for(var i=0; i<ipAddressArray.length; i++){
		var ipAddress=ipAddressArray[i].split("~");
		if(ipAddress.length ==2){
	    	var temp1 = ipAddress[0].split(".");
	    	var temp2 = ipAddress[1].split(".");
	    	if(temp1[0] != temp2[0] || temp1[1] != temp2[1] || temp1[2] != temp2[2]) return false;
	    	if(temp1[3] >= temp2[3]) return false;
	    	
	    	for(var j=0; j<4; j++){
	    		if(temp1[j] =="" || temp1[j] == null) return false;
	    		if(temp2[j] =="" || temp2[j] == null) return false;
	    	}
	    }else{
	    	var temp1 = ipAddress[0].split(".");
	    	for(var j=0; j<4; j++){
	    		if(temp1[j] =="" || temp1[j] == null) return false;
	    	}
	    }	
	}
		return true;
}
//숫자 및 3자리만 입력되도록
function onlyNumber(element, event) {
    var key = window.event ? event.keyCode : event.which;    
	//alert(element.value);
    if ((event.shiftKey == false) && ((key  > 47 && key  < 58) || (key  > 95 && key  < 106)
    || key  == 35 || key  == 36 || key  == 37 || key  == 39  // 방향키 좌우,home,end  
    || key  == 8  || key  == 46)
    ) {
    	if(element.value.length >=3 && key != 8){
    		//alert(element.value);
      		return false;
      	}else{
      		return true;	
      	} 
    }else {
        return false;
    }    
}
function changeExportType(type){
	
	if(type=="1"){
		document.form.chkRepacking.disabled = false;
		document.form.chkIsChange.disabled = false;
	}else{
		document.form.chkRepacking.disabled = true;
		document.form.chkIsChange.disabled = true;
		document.form.chkRepacking.checked = true;
		document.form.chkIsChange.checked = false;
	}	
	
}
</script>

<style>

div.gridbox_dhx_skyblue.gridbox table.hdr td {
		text-align: center;
	}
	
	div.gridbox_dhx_skyblue.gridbox table.hdr td div.hdrcell {
		padding-left:0px;
	}
	
	/* Tabbar Web스킨 사용시 간격조정 */
	.dhxtabbar_base_dhx_web div.dhx_cell_tabbar div.dhx_cell_cont_tabbar {
		
		padding: 0px;
		
	}

</style>


</head>
<body onload="init();" onresize="resize();" style="margin: 0px; padding:5px; overflow: auto;">
<form name="form" method="post">
<input type="hidden" name="POLICY_NO" value=""/>
<table class="topPolicySet">
	<tr>
		<th class="topth"><%=mm.getMessage("CPMG_1223", s_user.getLocale())%></th>
		<td class="toptd">
			<table class="createPolicySet">
				<tr>	
					<td>
						<div id="gridbox" style="width:100%; height:250px; background-color:white; overflow:hidden;"></div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="btnarea">
	            			<a class="btn" onclick="doDelRow()"><span><%=mm.getMessage("CPMG_1183", s_user.getLocale())%></span></a>
	            			<a class="btn" onclick="doAddRow()"><span><%=mm.getMessage("CPMG_1224", s_user.getLocale())%></span></a> 
	            		</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th rowspan="8" class="topth"><%=mm.getMessage("CPMG_1189", s_user.getLocale())%></th>
		<td class="toptd">
			<table>
				<tr>
					<td>
						<%=mm.getMessage("CPMG_1225", s_user.getLocale())%>  <input type="text" size="40" name="POLICY_NM"/>
					</td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<table>
				<colgroup>
		 		 	<col style="width:51%;" />
		  			<col/>
	  			</colgroup>
				<tr>
					<td><input type="radio" name="exportType" value="1"  onclick="changeExportType('1');"> <%=mm.getMessage("CPMG_1231", s_user.getLocale())%></td>
					<td><input type="radio" name="exportType" value="3"  onclick="changeExportType('3');"> <%=mm.getMessage("CPMG_1232", s_user.getLocale())%></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<table>
				<colgroup>
		 		 	<col style="width:51%;" />
		  			<col/>
	  			</colgroup>
				<tr>
					<td><input type="checkbox" name="chkExtCopy"> <%=mm.getMessage("CPMG_1190", s_user.getLocale())%></td>
					<td><input type="checkbox" name="chkPrint"> <%=mm.getMessage("CPMG_1191", s_user.getLocale())%></td>
					<td></td>
				</tr>
				<tr>
					<td><input type="checkbox" name="chkClipCopy"> <%=mm.getMessage("CPMG_1192", s_user.getLocale())%></td>
					<td><input type="checkbox" name="chkRepacking"> <%=mm.getMessage("CPMG_1193", s_user.getLocale())%></td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<table>
	  			<colgroup>
		 		 	<col style="width:30%;" />
		  			<col style="width:40%;" />
		  			<col style="width:30%;" />
	  			</colgroup>
				<tr>
					<td><input type="checkbox" name="chkExpireDay"> <%=mm.getMessage("CPMG_1124", s_user.getLocale())%></td>
					<td><%=mm.getMessage("CPMG_1222", s_user.getLocale())%> <input type="text" size="3" name="EXPIRE_DAY"> <%=mm.getMessage("COMG_1069", s_user.getLocale())%></td>
					<td><input type="checkbox" name="chkEdit2"> 사용자 변경 허용</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<table>
	  			<colgroup>
		 		 	<col style="width:30%;" />
		  			<col style="width:40%;" />
		  			<col style="width:30%;" />
	  			</colgroup>
				<tr>
					<td><input type="checkbox" name="chkReadCnt"> <%=mm.getMessage("CPMG_1221", s_user.getLocale())%></td>
					<td><input type="text" size="3" name="READ_COUNT"> <%=mm.getMessage("COMG_1017", s_user.getLocale())%></td>
					<td><input type="checkbox" name="chkEdit1"> 사용자 변경 허용</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="
		<%if("Y".equals(pm.getString("component.ui.policy.auth.hiding"))){%>
			display:none;	
		<%} else {%>
			display:block;
		<%}%>
	">
		<td class="toptd">
			<input type="checkbox" name="chkIsChange"> <%=mm.getMessage("CPMG_1220", s_user.getLocale())%>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<input type="checkbox" name="chkMac"> <%=mm.getMessage("CPMG_1219", s_user.getLocale())%>
		</td>
	</tr>
	<tr>
		<td class="toptd">
			<input type="checkbox" name="chkIp"> <%=mm.getMessage("CPMG_1218", s_user.getLocale())%>
			<div class="ipAddr" id="divIpAddr">
			<table class="ipAddrInput">
				<tr>
					<td class="iptd">
						<input type="checkbox">
					</td>
					<td class="iptd">
						<input type="text" size="3" name='fromIp0_0' value="" onkeypress="return onlyNumber(this, event)">.<input type="text" size="3" name='fromIp0_1' value="" onkeypress='return onlyNumber(this, event)'>
						.<input type="text" size="3" name='fromIp0_2' value="" onkeypress='return onlyNumber(this, event)'>.<input type="text" size="3" name='fromIp0_3' value="" onkeypress='return onlyNumber(this, event)'>
						~
						<input type="text" size="3" name='toIp0_0' value="" onkeypress='return onlyNumber(this, event)'>.<input type="text" size="3" name='toIp0_1' value="" onkeypress='return onlyNumber(this, event)'>
						.<input type="text" size="3" name='toIp0_2' value="" onkeypress='return onlyNumber(this, event)'>.<input type="text" size="3" name='toIp0_3' value="" onkeypress='return onlyNumber(this, event)'> 
					</td>
				</tr>
				<tr>
					<td colspan="2" class="ipBtn">
						<div class="btnarea">
	            			<a class="btn" onclick="delRow()"><span><%=mm.getMessage("CPMG_1183", s_user.getLocale())%></span></a>
	            			<a class="btn" onclick="addTableRow()"><span><%=mm.getMessage("COMG_1068", s_user.getLocale())%></span></a> 
	            		</div>
					</td>
				</tr>
			</table>
			</div>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>	
	</tr>
	<tr>
		<td></td>
		<td>
			<div class="btnarea">
	        	<a class="btn" onclick="doInsertSet()"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a> 
	        </div>
		</td>
	</tr>
</table>
</form>
</body>
</html>