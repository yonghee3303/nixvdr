<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
String contextPath = request.getContextPath();
%>

<html>
<head>
<title>CP 예외정책 등록</title>
<%@ include file="../../ext/include/include_css.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script>

function init() 
{
	var argu = "&formid=<%=formId%>&gridid=<%=grid_obj%>" + "&user_id=";
	masterframe.location.href='<%=contextPath%>/cp/policy/cpExcPolicyMaster.jsp?eventframe=masterframe' + argu;
	
	refresh("", "allo");
}

function refresh(excNo, gubun) 
{	
	var argu = "&formid=<%=formId%>01&gridid=<%=grid_obj%>&excNo=" + excNo + "&gubun=" + gubun;
	detailframe1.location.href='<%=contextPath%>/cp/policy/cpExcPolicyDetail.jsp?' + argu;
}

function doQuery() 
{
	masterframe.doQuery();
}

function getExcNo()
{
		
}

</script>
</head>
<body onload="init();">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<%@ include file="../../mg/common/milestone.jsp"%>
   </td>
</tr>

<tr>
	<td>
      <table width="100%" border="0">
       <rowgroup>
            <row height="60%" />
            <row height="20%" />
        </rowgroup>
        <tr>
          <td valign="top">
				<iframe name="masterframe" width="100%"  height="300" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
        </tr>
        <tr>
          <td>
               <table width="100%" border="0">
			        <colgroup>
			            <col width="50%" />
			            <col width="50%" />
			        </colgroup>
          			<tr>
          				<td>
          					<iframe name="detailframe" width="100%"  height="200" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
          				</td>
          			</tr>
          		</table>
           </td>
        </tr>
      </table>
	</td>
</tr>
</table>
</form>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="detailframe=162"/>
</jsp:include>
</body>
</html>
