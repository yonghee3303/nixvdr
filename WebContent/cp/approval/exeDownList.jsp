<%@ page contentType ="text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String contextPath = request.getContextPath();

%>

<html>
<head>
<title>반출파일 다운로드</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%@ include file="../../ext/include/su_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	try {
		var header_name = GridObj.getColumnId(cellInd);
		
		if( header_name == "EXE_URL" ) {
			// windows.clipboardData.setData("Text", s);
			var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
			var baseUrl = GridObj.cells(rowId, GridObj.getColIndexById("BASEURL")).getValue();
			copyToClipboard(baseUrl + "/cp/approval.do?mod=down&DOC_NO=" + docId + "&PGM_ID=regMgmt");
			alert("<%=mm.getMessage("CPMG_1023", s_user.getLocale()) %>");
		} else if( header_name != "EXE_ICON")  {
			// 문서등록 화면으로 이동
			var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
			var arqu_param = "?DOC_NO=" + docId + "&PGM_ID=regMgmt"; 
			     argu_param += "&CP_STS_CD=04";
			     argu_param += "&form=";
			doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", arqu_param);
		}
	}catch(err) {
		
	}
}

//Grid Row one Clieck Event 처리
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
	if( header_name == "EXE_ICON" ) {
		 var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		 var SERVLETURL = G_SERVLETURL + "?mod=down&PGM_ID=regMgmt";	
		 var argu = "&DOC_NO=" + docId;
		 fileDownload(SERVLETURL + argu);
	}
}

//파라메터의 문자열을 Clipboard로 복사
function copyToClipboard(s)
{
    if( window.clipboardData && clipboardData.setData )
    {
        clipboardData.setData("Text", s);
    }
    else
    {
        // You have to sign the code to enable this or allow the action in about:config by changing
        user_pref("signed.applets.codebase_principal_support", true);
        netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

        var clip = Components.classes['@mozilla.org/widget/clipboard;[[[[1]]]]'].createInstance(Components.interfaces.nsIClipboard);
        if (!clip) return;

        // create a transferable
        var trans = Components.classes['@mozilla.org/widget/transferable;[[[[1]]]]'].createInstance(Components.interfaces.nsITransferable);
        if (!trans) return;

        // specify the data we wish to handle. Plaintext in this case.
        trans.addDataFlavor('text/unicode');

        // To get the data from the transferable we need two new objects
        var str = new Object();
        var len = new Object();

        var str = Components.classes["@mozilla.org/supports-string;[[[[1]]]]"].createInstance(Components.interfaces.nsISupportsString);

        var copytext=meintext;

        str.data=copytext;

        trans.setTransferData("text/unicode",str,copytext.length*[[[[2]]]]);

        var clipid=Components.interfaces.nsIClipboard;

        if (!clip) return false;

        clip.setData(trans,null,clipid.kGlobalClipboard);      
    }
}

function OkFileUpload(fileList) {
	
}

function OkPopupDeptGrid(dept_cd, plant_cd, dept_nm) {
	
}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&APPROVER_ID="	+ "<%=s_user.getId() %>";
		argu += "&DOC_TITLE="	+encodeURIComponent(document.form.DOC_TITLE.value);
		argu += "&EXE_FILE="	+encodeURIComponent(document.form.EXE_FILE.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=exeDownList&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum() +"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doSave(approvalYn) {
	if(!checkRows()) return;	
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}


function doDelete() {
	if(!checkRows()) return;
}

function init() {
	document.form.DOC_TITLE.focus();
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	
	setFormDraw();
	
	doQuery();
}

</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<%-- <%@ include file="../../mg/common/milestone.jsp"%> --%>

        <table class="board-search">
        <colgroup>
            <col width="10%" />
            <col width="15%" />
            <col width="15%" />
            <col width="15%" />
            <col width="10%" />
            <col width="25%" />
            <col width="80px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1101", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="DOC_TITLE" size="15" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1102", s_user.getLocale())%></td>
            <td>
				<input type="text" name="EXE_FILE" size="15" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1103", s_user.getLocale())%></td>
            <td>
            	<input type="text" id="fromDate" value="" size="8" class="text" align="center">
				<img id="imgFromDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand"> ~
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id="imgToDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
			</td>
			<td>
			    <div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="88%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event2.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=142"/>
</jsp:include>
</body>
</html>
