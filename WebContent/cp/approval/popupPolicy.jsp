<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.user.IUser"%>
<%
    IUser s_user = (IUser) session.getAttribute("j_user");
	String rowId = StringUtils.paramReplace(request.getParameter("rowId"));
	String user_id = StringUtils.paramReplace(request.getParameter("user_id"));
	String user_nm = StringUtils.paramReplace(request.getParameter("user_nm"));
	String company_id = StringUtils.paramReplace(request.getParameter("company_id"));
	String company_nm = StringUtils.paramReplace(request.getParameter("company_nm"));
	String policy = StringUtils.paramReplace(request.getParameter("policy"));
	String expireDt = StringUtils.paramReplace(request.getParameter("expireDt"));
	String policyDoc = StringUtils.paramReplace(request.getParameter("policyDoc"));
	String readCount = StringUtils.paramReplace(request.getParameter("readCount"));
	String modStop = StringUtils.paramReplace(request.getParameter("modStop"));
	String macList = StringUtils.paramReplace(request.getParameter("macList"));
	String msg = StringUtils.paramReplace(request.getParameter("msg"));
	String email = StringUtils.paramReplace(request.getParameter("email"));
	String stsCd = StringUtils.paramReplace(request.getParameter("stsCd"));
	String callFlag = StringUtils.paramReplace(request.getParameter("callFlag"));
	String callForm = StringUtils.paramReplace(request.getParameter("callform"));
	IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
    String exportUser = paramemter.getString("component.export.user");
    String iftype = paramemter.getString("component.site.iftype"); // REG
    IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
    boolean isAdmin = false;
    if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString())) {
    	isAdmin = true;
    }
%>
<html>
<head>
<title>수신자 정책등록</title>
<%@ include file="../../ext/include/include_css.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#EXPIRE_DT" ).datepicker();
			$( "#imgInsDate").click(function() {$( "#EXPIRE_DT" ).focus();});
		});
	</script> 
	
<script>

function doUserPop() {
	if("<%=stsCd%>" == "01" && "<%=callForm %>" != "cpDocList") {
		popupCompUser("수신자 선택", '', '', 440, 400, "<%=exportUser%>", "",<%=paramemter.getString("component.contextPath.root")%>);
	}
}

function OkPopupCompUser(user_id, user_nm, company_id,company_nm, email) {
	document.form.USER_ID.value = user_id;
	document.form.USER_NM.value = user_nm;	
	document.form.COMPANY_ID.value = company_id;
	document.form.COMPANY_NM.value = company_nm;
	document.form.EMAIL.value = email;
}

function doAddMac() {
 	var macAddr = document.form.MAC_LIST.value;
 	if(document.form.MAC_LIST.value == "") {
 		document.form.MAC_LIST.value = document.form.inMac.value;
 	} else {
 		document.form.MAC_LIST.value = macAddr + "|" + document.form.inMac.value;
 	}
}

function doSave() {
	
	if(document.form.USER_ID.value  == "") 
	{
		alert("<%=mm.getMessage("CPMG_1024", s_user.getLocale())%>");  // 수신자를 선택하여 주십시요.
		return;
	}
	
	var user_id = document.form.USER_ID.value;
	var user_nm = document.form.USER_NM.value;
	var company_id = document.form.COMPANY_ID.value;
	var company_nm = document.form.COMPANY_NM.value;
	var email = document.form.EMAIL.value;
	
	/*
	1번째 자리 : Network 연결 실패시 조회 금지
	2번째 자리 : 만료 일자 사용 여부 
	3번째 자리 : 추출 횟수 사용 여부 
	4번째 자리 : 문서 권한 설정 사용 여부 
	5번째 자리 : Log Tracking 사용 여부 
	*/
	
	var expireDt="";
	var exeCount="";
	var policy;
	/*
	if(document.form.chkNetwork.checked) {  // Network 연결 실패시 조회 금지
		policy = "1";
	} else {
		policy = "0";
	}
	*/
	
	policy = "1"; //Network연결 실패시 조회 금지 
	if(document.form.chkExpireDt.checked) {
		if(document.form.EXPIRE_DT.value == "") {
			alert("<%=mm.getMessage("CPMG_1025", s_user.getLocale())%>");  // 만료일자를 선택하여 주십시요.
			return;
		}
		expireDt = document.form.EXPIRE_DT.value;
		policy += "1";
	} else {
		policy += "0";
	}
	
	if(document.form.chkExe.checked) {
		if(document.form.READ_COUNT.value == "") {
			alert("<%=mm.getMessage("CPMG_1026", s_user.getLocale())%>");  //실행 회수를 입력하여 주십시요.
			return;
		}
		exeCount = document.form.READ_COUNT.value;
		policy += "1";
	} else {
		policy += "0";
	}
	
	// 문서권한 사용여부
	policy += "1";
	/*
	if(document.form.chkDocAuth.checked) {
		policy += "1";
	} else {
		policy += "0";
	}
	*/
	
	
	// Log Tracking 사용여부
	if(document.form.chkLogTracking.checked) {
		policy += "1";
	} else {
		policy += "0";
	}
	
	
	/*
	1번째 자리 : 저장/수정 금지  (1=금지, 0=금지 안 함)
	2번째 자리 : 출력 금지 
	3번째 자리 : Clip Board 사용 금지 
	4번째 자리 : Edit/파일생성 가능 
	*/
	var docPolicy="";
	// 문서권한 사용여부 - 저장
	if(document.form.chkSave.checked) {
		docPolicy += "1";
	} else {
		docPolicy += "0";
	}
	// 인쇄 사용여부
	if(document.form.chkPrint.checked) {
		docPolicy += "1";
	} else {
		docPolicy += "0";
	}
	// 클립보드 복사 사용여부
	if(document.form.chkClipCopy.checked) {
		docPolicy += "1";
	} else {
		docPolicy += "0";
	}
	// 재배포 사용여부
	if(document.form.chkRepacking.checked) {
		docPolicy += "1";
	} else {
		docPolicy += "0";
	}
	// 외부로 문서복사 사용여부
	if(document.form.chkExtCopy.checked) {
		docPolicy += "1";
	} else {
		docPolicy += "0";
	}
	
	var nonStop="0";
	// 접근제한
	if(document.form.chkNonStop.checked) {
		nonStop = "1";
	} 
		
	var macList = document.form.MAC_LIST.value;
	macList = macList.toUpperCase();
	var msg = document.form.DOC_MESSAGE.value;
	
	if("<%=callFlag%>" == "grid") {
		opener.OkPopupPolicy("<%=rowId%>",user_id, user_nm, company_id, company_nm, policy, expireDt, exeCount, docPolicy, nonStop, macList, msg, email);
	} else {
		opener.batchPopupPolicy("<%=rowId%>",user_id, user_nm, company_id, company_nm, policy, expireDt, exeCount, docPolicy, nonStop, macList, msg, email);
	}
	window.close();
}

function doClose() {
	window.close();
}

function init() {
	
	if("<%=modStop %>" == "1") {
		document.form.chkNonStop.checked = true;
	}
	
	var policy = "<%=policy %>";
	
	/*
	if(policy.substring(0,1) == "1") {
		document.form.chkNetwork.checked = true;
	} else {
		document.form.chkNetwork.checked = false;
	}
	*/
	
	if(policy.substring(1,2) == "1") {
		document.form.chkExpireDt.checked = true;
	} else {
		document.form.chkExpireDt.checked = false;
	}
	
	if(policy.substring(2,3) == "1") {
		document.form.chkExe.checked = true;
	} else {
		document.form.chkExe.checked = false;
	}
	
	/*
	if(policy.substring(3,4) == "1") {
		document.form.chkDocAuth.checked = true;
	} else {
		document.form.chkDocAuth.checked = false;
	}
	*/
	
	if(policy.substring(4,5) == "1") {
		document.form.chkLogTracking.checked = true;
	} else {
		document.form.chkLogTracking.checked = false;
	}
	
	// 문서권한
	var policyDoc =  "<%=policyDoc %>";
	if(policyDoc.substring(0,1) == "1") {
		document.form.chkSave.checked = true;
	} else {
		document.form.chkSave.checked = false;
	}
	if(policyDoc.substring(1,2) == "1") {
		document.form.chkPrint.checked = true;
	} else {
		document.form.chkPrint.checked = false;
	}
	if(policyDoc.substring(2,3) == "1") {
		document.form.chkClipCopy.checked = true;
	} else {
		document.form.chkClipCopy.checked = false;
	}
	if(policyDoc.substring(3,4) == "1") {
		document.form.chkRepacking.checked = true;
	} else {
		document.form.chkRepacking.checked = false;
	}
	if(policyDoc.substring(4,5) == "1") {
		document.form.chkExtCopy.checked = true;
	} else {
		document.form.chkExtCopy.checked = false;
	}
	
	
	
	if("<%=callForm %>" == "cpDocList" && "<%=iftype%>"== "REG") {
		if(<%=isAdmin%>){
			document.form.COMPANY_NM.readOnly = true;
			document.form.chkNonStop.disabled = false; 
		}else{
		document.form.COMPANY_NM.readOnly = true;
		document.form.chkSave.disabled = true;
		document.form.chkLogTracking.disabled = true;
		document.form.chkSave.disabled = true;
		document.form.chkPrint.disabled = true;
		document.form.chkClipCopy.disabled = true;
		document.form.chkRepacking.disabled = true;
		document.form.chkExtCopy.disabled = true;
		document.form.inMac.readOnly = true;
		document.form.MAC_LIST.readOnly = true;
		document.form.chkNonStop.disabled = false;  // 접근제한
		}
	} else {
		document.form.COMPANY_NM.readOnly = true;
		document.form.chkNonStop.disabled = false;  // 접근제한
	}
}

function chkSaveClick(cb)
{
	if(!cb.checked) {
		document.form.chkPrint.checked = false;
		document.form.chkPrint.disabled = true;
		document.form.chkClipCopy.checked = false;
		document.form.chkClipCopy.disabled = true ;
		document.form.chkRepacking.checked = false;
		document.form.chkRepacking.disabled = true ;
		document.form.chkExtCopy.checked = false;
		document.form.chkExtCopy.disabled = true ;
	} else {
		document.form.chkPrint.disabled = false;
		document.form.chkClipCopy.disabled = false;
		document.form.chkRepacking.disabled = false;
		document.form.chkExtCopy.disabled = false;
		document.form.chkRepacking.checked = true;
		document.form.chkExtCopy.checked = true;
	}
}


</script>
</head>
<body onload="init();" >
<form name="form">
<table width="100%">
<tr>
   	<td>
        <table class="board-search">
        <colgroup>
            <col width="10%" />
            <col width="40%" />	  
			<col width="10%" />
			<col width="40%" />
        </colgroup>
        <tr>
            <td colspan="4">
			</td> 
        </tr>
        <tr>
            <td class="tit"><%=mm.getMessage("CPMG_1121", s_user.getLocale())%></td>
            <td colspan="3">
				<input type="text" name="USER_ID"  size="6"  value="<%=user_id %>" class="text" readonly="readonly">
				<a href="javascript:doUserPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="Search User" class="mgl-5" /></a>
				<input type="text" name="USER_NM"  size="20" value="<%=user_nm %>" class="text" readonly="readonly">&nbsp;&nbsp;
				<input type="hidden" name="COMPANY_ID"  size="6" value="<%=company_id %>" class="text">
				<input type="text" name="COMPANY_NM"  size="20" value="<%=company_nm %>" class="text"  readonly="readonly">
				<input type="hidden" name="EMAIL"  size="20" value="<%=email %>"  class="text">
			</td> 
        </tr>
        <tr>        
			<td class="tit"><%=mm.getMessage("CPMG_1122", s_user.getLocale())%></td>
            <td colspan="3">			
				<input type="checkbox" name="chkExpireDt" class="text"><%=mm.getMessage("CPMG_1124", s_user.getLocale())%>&nbsp;&nbsp;
				<input type="text" id="EXPIRE_DT" name="EXPIRE_DT" value="<%=expireDt %>" size="10" class="text"/>
				<img id="imgInsDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand"/>
            </td>
        </tr>
        <tr>        
			<td class="tit"><%=mm.getMessage("CPMG_1123", s_user.getLocale())%></td>
            <td colspan="3">			
				<input type="checkbox" name="chkExe" class="text"><%=mm.getMessage("CPMG_1125", s_user.getLocale())%>&nbsp;&nbsp;
				<input type="text" id="READ_COUNT" value="<%=readCount %>" size="5" align="right"  class="text" /> <%=mm.getMessage("COMG_1017", s_user.getLocale())%>
            </td>
        </tr>
        <!--  
        <tr>        
			<td class="tit">기타제한</td>
            <td colspan="3">			
				<input type="hidden" name="chkNetwork" class="text" checked="checked">Network연결 실패시 사용금지    
            </td>
        </tr>
        -->
        <tr>        
			<td class="tit">Log</td>
            <td colspan="3">			
				<input type="checkbox" name="chkLogTracking" class="text" checked="checked" ><%=mm.getMessage("CPMG_1126", s_user.getLocale())%>   
            </td>
        </tr>
        <!--  
        <tr>        
			<td class="tit"></td>
            <td colspan="3">			  
				<input type="checkbox" name="chkDocAuth" class="text" checked="checked">문서권한 설정 사용    
            </td>
        </tr>
        -->
        <tr>        
			<td class="tit"><%=mm.getMessage("CPMG_1127", s_user.getLocale())%></td>
            <td colspan="3">			
				<input type="checkbox" name="chkSave" class="text" onclick='chkSaveClick(this);'><%=mm.getMessage("CPMG_1128", s_user.getLocale())%>
            </td>
        </tr>
        <tr>
        	<td></td>
            <td colspan="3">			
				<input type="checkbox" name="chkPrint" class="text"><%=mm.getMessage("CPMG_1129", s_user.getLocale())%> 
            </td>
        </tr>
         <tr>
        	<td></td>
            <td colspan="3">			
				<input type="checkbox" name="chkClipCopy" class="text"><%=mm.getMessage("CPMG_1130", s_user.getLocale())%>  
            </td>
        </tr>
        <tr>
        	<td></td>
            <td colspan="3">			
				<input type="checkbox" name="chkRepacking" class="text" checked="checked"><%=mm.getMessage("CPMG_1131", s_user.getLocale())%> 
            </td>
        </tr>
        <tr>        
			<td></td>
            <td colspan="3">			
				<input type="checkbox" name="chkExtCopy" class="text"><%=mm.getMessage("CPMG_1132", s_user.getLocale())%>
            </td>
        </tr> 
        <tr>        
			<td class="tit"><%=mm.getMessage("CPMG_1133", s_user.getLocale())%></td>
            <td colspan="3">			
				<input type="checkbox" name="chkNonStop" class="text" disabled="true"><%=mm.getMessage("CPMG_1134", s_user.getLocale())%>
            </td>
        </tr>
        <tr>        
			<td class="tit">MAC Address</td>
            <td colspan="3">			
				<input type="text" name="inMac" size="18" class="text"/> 
				<a href="javascript:doAddMac();"><img src="../../ext/images/btnAdd.gif" align="absmiddle" alt="<%=mm.getMessage("CPMG_1135", s_user.getLocale())%>" class="mgl-5" /></a>
			</td>
        </tr>
        <tr>        
			<td class="tit"></td>
            <td colspan="3">			
				<input type="text" name="MAC_LIST" value="<%=macList %>" size="73" class="text"/> 
			</td>
        </tr>
        <tr>        
			<td class="tit"><%=mm.getMessage("CPMG_1136", s_user.getLocale())%></td>
            <td colspan="3">	
            	<input type="text" name="DOC_MESSAGE" value="<%=msg %>" size="73" class="text"/> 	
			</td>
        </tr>
        </table>
        
		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
            <td>
	            <div class="btnarea">
		            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
		            	<a href="javascript:doClose();" class="btn"><span><%=mm.getMessage("COMG_1016", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
        
	</td>
</tr>
</table>
</form>
</body>
</html>
