<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import ="com.core.component.util.StringUtils" %>
<%
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
String docNo = StringUtils.paramReplace(request.getParameter("DOC_NO"));
String stsCd = StringUtils.paramReplace(request.getParameter("CP_STS_CD"));
String callForm = StringUtils.paramReplace(request.getParameter("form"));
String policyClCd = StringUtils.paramReplace(request.getParameter("policyClCd"));

IUser s_user = (IUser) session.getAttribute("j_user");
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
String flag = (String)request.getParameter("flag");
String leftMenuName = java.net.URLDecoder.decode(request.getParameter("leftMenuName"),"utf-8");
%>

<html>
<head>

<title>Insert title here</title>

<%@ include file="../../ext/include/dhtmlx_v403_scripts.jsp" %>

<script>

var leftMenuName="<%=leftMenuName%>";

var layout;
var cell;

function init() {
	

	layout = new dhtmlXLayoutObject({
	    parent: document.getElementById("layoutObj"),
	    pattern: "1C",
	    skin: "dhx_web"
	});
	
	
	cell = layout.cells('a');

	cell.setText("<table style='padding:0; margin0; width:98%; height:100%;'><tr><td id='caption'>"+leftMenuName+"</td></tr></table>");

	cell.attachURL("cpReqDetail.jsp", null,  {
		formid: "<%=formId%>",
		gridid: "<%=grid_obj%>",
		DOC_NO: "<%=docNo%>",
		CP_STS_CD: "<%=stsCd%>",
		form: "<%=callForm%>",
		POLICY_CL_CD: "<%=policyClCd%>"
	});
}

function resize() {
	layout.setSizes();
}


</script>

<style>
    #layoutObj {
        width: 100%;
        height: 90%;
        margin: 0px;
        overflow: hidden;
    }
	#caption {
		white-space: nowrap;
		cursor: default;
		font-family: Tahoma;
		font-size: 12px;
		color: #ffffff;
		font-weight: bold;
		height:100%;
		width: 50%;
		padding:0;
		margin:0;
	}

	#goMain {
		white-space: nowrap;
		cursor: default;
		line-height: 31px;
		font-family: Tahoma;
		font-size: 12px;
		color: yellow;
		font-weight: bold;
		width: 50%;
		height:100%;
		text-align:right;
		padding:0;
		margin:0;
	}
</style>

</head>

<body onload="init();" onresize="resize();">

<div id="layoutObj" style="position: relative;"></div>

</body>
</html>