<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  String contextPath = request.getContextPath();
  String param = request.getParameter("param");
  String job = request.getParameter("job");
  String docNo = request.getParameter("docNo");

  //로그 조회용 시간값 계산
  Calendar fromCal = Calendar.getInstance();
  Calendar toCal = Calendar.getInstance();
  fromCal.set(Calendar.HOUR_OF_DAY, toCal.get(Calendar.HOUR_OF_DAY)-1);
  toCal.set(Calendar.HOUR_OF_DAY, toCal.get(Calendar.HOUR_OF_DAY));
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
  String fromDate = sdf.format(fromCal.getTime());
  String toDate = sdf.format(toCal.getTime());
%>

<html>
<head>
<title>로그 검색/조회</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common.jsp"%> --%>
<%-- <%@ include file="../../ext/include/su_grid_common.jsp" %> --%>
<%@ include file="/ext/include/dhtmlx_v403_scripts.jsp"%>
<%@ include file="../../ext/include/su_grid_common_render2.jsp" %>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>   <%-- Dhtmlx Grid Combobox JSP --%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JSON-js-master/json2.js"></script>
<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/mg/log.do";
var formId = "<%=formId%>";
var param = '<%=param%>';

//검색바 달력
var calendar1;
var calendar2;

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
	//기존 소스
	//GridObj = setGridDraw(GridObj);
	//contextPath 추가
	GridObj = setGridDraw(GridObj, <%=pm.getString("component.contextPath.root")%>);	//contextPath가 root면 true 아니면 false
   	GridObj.setSizes();
   	GridObj.setEditable(false);
   	GridObj.attachEvent("onXLE", doQueryEnd);	//조회시 doQueryEnd 처리 구문 추가
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
/* function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
} */

// 데이터 조회시점에 호출되는 함수입니다.
function doQuery() 
{
	var tbCl = document.form.TB_CL.value;
	if(checkTbCl('search')) {
		var grid_col_id = "<%=grid_col_id%>";
		var grid_col_hidden = "<%=grid_col_hidden%>";
		var argu = getParams();
		
		if(tbCl=="IO"){
			GridObj.loadXML("<%=contextPath%>/mg/log.do?mod=selectLogList&grid_col_id="+grid_col_id+"&grid_col_hidden="+grid_col_hidden+argu);																						
		}else if(tbCl=="DOC"){
			GridObj.loadXML("<%=contextPath%>/cp/approval.do?mod=selectDocLog&grid_col_id="+grid_col_id+"&grid_col_hidden="+grid_col_hidden+argu);	
		}
		GridObj.clearAll(false);
	}																																				
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	resize();
	
	// if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

//그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
//이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {

}

function doOnRowSelect(rowId, cellInd) {}

//gird refresh check
function checkTbCl(job) {
	var tbCl = document.form.TB_CL.value;
	var fromDate = document.form.fromDate.value;
	var toDate = document.form.toDate.value;
	//var fileNm = document.form.FILE_NM.value;
	var workPcIp = document.form.WORK_PC_IP.value;
	var userId = document.form.USER_ID.value;
	var userNm = document.form.USER_NM.value;
	var docNo = document.form.SVR_ID.value;
	//var svrNm = document.form.SVR_NM.value;
	//var deptCd = document.form.DEPT_CD.value;
	//var deptNm = document.form.DEPT_NM.value;
	//var all = document.form.ALL.value;
	var chgFormId;
	var chkChgForm = false;
	
	/* if(svrNm.length == 0) {
		svrId = "";
		document.form.SVR_ID.value = svrId;
	} 
	if(deptNm.length == 0) {
		deptCd = "";
		document.form.DEPT_CD.value = deptCd;
	} */
	
	//FormId 세팅
	if(tbCl == "IO") {
		chgFormId = "CP_0207";
	}  else {
		tbCl = "DOC";
		chgFormId = "CP_0202";
	}
	if(formId != chgFormId) {
		formId = chgFormId;
		chkChgForm = true;
	}

	//refresh
	if(chkChgForm == true) {	//페이지 전체 refresh()
		var jparam = new Object();
	
		jparam.TB_CL = encodeURIComponent(tbCl);
		jparam.fromDate = fromDate;
		jparam.toDate = toDate;
		//jparam.FILE_NM = encodeURIComponent(fileNm); 
		jparam.WORK_PC_IP = encodeURIComponent(workPcIp);
		jparam.USER_ID = encodeURIComponent(userId);
		jparam.USER_NM = encodeURIComponent(userNm);
		jparam.DOC_NO = encodeURIComponent(docNo);
		//jparam.SVR_NM = encodeURIComponent(svrNm);
		//jparam.DEPT_CD = encodeURIComponent(deptCd);
		//jparam.DEPT_NM = encodeURIComponent(deptNm);
		//jparam.ALL = encodeURIComponent(all);
		param = JSON.stringify(jparam);

		parent.cell2.attachURL("cpLogSearch.jsp", null,  {
			formid: formId,
			gridid: "<%=grid_obj%>",
			docNo: docNo,
			param: param
		});	
		return false;
	} else {				//그리드 refresh
		return true;
	}
}

function getParams() {
	
	var tbCl = document.form.TB_CL.value;
	var fromDate = document.form.fromDate.value;
	var toDate = document.form.toDate.value;
	//var fileNm = document.form.FILE_NM.value;
	var workPcIp = document.form.WORK_PC_IP.value;
	var userId = document.form.USER_ID.value;
	var userNm = document.form.USER_NM.value;
	var docNo = document.form.SVR_ID.value;
	//var svrNm = document.form.SVR_NM.value;
	//var deptCd = document.form.DEPT_CD.value;
	//var deptNm = document.form.DEPT_NM.value;
	//var all = document.form.ALL.value;
	
	if(tbCl == " ") {
		tbCl = "";
	}
	
	return "&TB_CL="+encodeURIComponent(tbCl)+"&FROM_YMD="+fromDate+"&TO_YMD="+toDate+"&DOC_NO="+encodeURIComponent(docNo)
			+"&IP_ADDR="+encodeURIComponent(workPcIp)+"&USER_ID="+encodeURIComponent(userId)+"&USER_NM="+encodeURIComponent(userNm);
}

//검색쿼리 세팅
function setParams() {
	if(param != "null") {
		var jData = JSON.parse(param);
		
		document.form.TB_CL.value = decodeURIComponent(jData.TB_CL);
		document.form.fromDate.value = decodeURIComponent(jData.fromDate);
		document.form.toDate.value = decodeURIComponent(jData.toDate);
		//document.form.FILE_NM.value = decodeURIComponent(jData.FILE_NM);
		document.form.WORK_PC_IP.value = decodeURIComponent(jData.WORK_PC_IP);
		document.form.USER_ID.value = decodeURIComponent(jData.USER_ID);
		document.form.USER_NM.value = decodeURIComponent(jData.USER_NM);
		document.form.SVR_ID.value = decodeURIComponent(jData.DOC_NO);
		//document.form.SVR_NM.value = decodeURIComponent(jData.SVR_NM);
		//document.form.DEPT_CD.value = decodeURIComponent(jData.DEPT_CD);
		//document.form.DEPT_NM.value = decodeURIComponent(jData.DEPT_NM);
		//document.form.ALL.value = decodeURIComponent(jData.ALL);
	} else {
		document.form.fromDate.value = "<%=fromDate%>";
		document.form.toDate.value = "<%=toDate%>";
	}
}


//========== 달력 관련 설정 ========== 
function calendarSet() {
	
	//한글로 달력에 표시하기 위해서는 아래와 같은 데이터들이 필요로한다.
 	dhtmlxCalendarObject.prototype.langData["ko"]={
			dateformat: '%Y-%m-%d %H:%i',
			monthesFNames:["1월", "2월", "3월", "4월", "5월","6월", "7월", "8월", "9월", "10월", "11월", "12월"]	,
			monthesSNames:["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			daysFNames:["일요일","월요일", "화요일", "수요일", "목요일", "금요일","토요일"],
			daysSNames:["일", "월","화","수","목","금","토"],
			weekstart:7,
			weekname:"주"
	}; 
	
 	//아래 방식으로는 input 태그에 캘린더가 연결되지 않는다. 원인 규명이 필요함.
 	//대안으로 img 태그에 onClick="calendar.show();" 하여 캘린더 연동
	//new dhtmlXCalendarObject({input:"toDate", button:"calendar_icon2"});
	calendar1 = new dhtmlXCalendarObject("fromDate");
	calendar1.loadUserLanguage("ko");
	calendar1.setPosition("bottom");
	calendar2 = new dhtmlXCalendarObject("toDate");
	calendar2.loadUserLanguage("ko");
	calendar2.setPosition("bottom");
}
function setSens(id, k) {
	var val = byId(id).value;
	if(val != "") {
		// update range
		if (k == "min") {
			calendar2.setSensitiveRange(byId(id).value, null);
		} else {
			calendar1.setSensitiveRange(null, byId(id).value);
		}	
	}
}
function byId(id) {
	return document.getElementById(id);
}
//========== 달력 관련 설정 END ==========//

//POPUP을 띄운다. (
function popupWindow(url, left, top, width, height, scrollbars) {

    if (left == '') left = 50;
    if (top == '') top = 100;
    if (width == '') width = 540;
    if (height == '') height = 500;
    if (scrollbars == '') scrollbars = 'no';
    
    //화면 가운데로 배치
    var dim = new Array(2);
    dim = CenterWindow(height,width);
    top = dim[0];
    left = dim[1];

    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var resizable = 'yes';
    var location = 'no';
	
    var sFeatures = 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable+', location='+location;
	var ret = window.open(url, '', sFeatures);
	ret.focus();
	
	return ret;
}

//검색조건에 맞는 엑셀 파일 다운로드
function doExcelDown()
{
	if(checkTbCl('down')) {
		var grid_col_id     = "<%=grid_col_id%>";
		var grid_col_width  = "<%=grid_init_widths%>";
		var grid_col_header = encodeURIComponent("<%=grid_header%>");
		var grid_col_hidden = "<%=grid_col_hidden%>";
		var grid_col_type	= "<%=grid_col_type%>";
		var grid_col_combo_id	= "<%=grid_col_combo_id%>";
				
		var argu  = getParams();
		fileDownload(G_SERVLETURL+"?mod=excelLogList&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+"&grid_col_hidden="+grid_col_hidden+"&grid_col_type="+grid_col_type+"&grid_col_combo_id="+grid_col_combo_id+argu);		
	}			
}

function resize() {
	document.getElementById('gridbox').style.height = document.body.clientHeight - document.getElementById("searchBar").offsetHeight;   
}

function popupMessage(docNo, tbCl) {

	//var url = "popupClipMessage.jsp"; 
	var url = "popupMessage.jsp";
	var argu_param = "?SCRT_DOC_NO=" + docNo;
	argu_param += "&TB_CL="+tbCl;
	
	url += argu_param;
	popupWindow(url, '', '', 500, 352, 'yes');	
}

function init() {
	form.SVR_ID.value="<%=docNo%>";
	calendarSet();
	setParams();
	setFormDraw();
	
	
	
	//param이 존재할 경우 formId가 바뀐 재검색
	if(param != "null") {
			doQuery();	
	}
	resize();
}
</script>
<style>
input.text2 {padding:0; height:19px; font-size:11px; border:1px solid #ddd;}

.m0 { margin: 0 0 0 0 !important;}
.p {padding: 6px 15px 0 0 !important;}

.w40p {width: 40px;}


/* 검색조건(div-span) */
.board-search2 { width:100%; border:1px solid #dde1e3; background:#f4f4f4;}
.board-search2 div span {display:inline-block; font-size:12px; text-align:left; padding-left:10px; background:#f4f4f4; margin-top: 3px; margin-bottom: 3px;}
.board-search2 div span.tit { font-size:12px; padding-left:16px; text-align:left; background:#f4f4f4 url(../../ext/images/bul_arrow3.gif) 8px 50% no-repeat;}
.board-search2 div span a {display: inline-block; vertical-align: bottom;}

.searchbtn2 { float:right; width:auto; margin:0; height:auto;}
.searchbtn2 a.imgBtn { float:left; padding: 6px 0 0 0; margin-right:15px; }
.searchbtn2 a.btn { float:left; display: block; margin-right:5px; height:25px; padding-left:15px; background: transparent url('../../ext/images/btn3_left_bg.gif') 0 3 no-repeat; font:bold 11px Malgun Gothic, Verdana, Trebuchet MS; line-height: 19px; text-decoration: none; }
.searchbtn2 a.btnEnd {margin-right:15px !important;}
.searchbtn2 a.btn:link, .searchbtn2 a.btn:visited, .searchbtn2 a:active { color: #FFF;}
.searchbtn2 a.btn span {display:block; padding: 0 0px 4px 0; background:transparent url('../../ext/images/btn3_right_bg.gif') no-repeat top right;}
.searchbtn2 a.btn:hover span { color: #FFF;}

</style>

</head>
<body onload="init();" style="overflow:hidden;" onresize="resize();">
<form name="form" method="post">
<input type="hidden" name="TB_CL" value="DOC">
<table id="searchBar" width="100%">
<tr>
   	<td class="board-search2 noneBorder">
   		<div style="table-layout:fixed; margin-bottom: 0px !important;">
   			<%-- <span>
   				<span class="tit"><%=mm.getMessage("CPMG_1234", s_user.getLocale())%></span>
	   			<span>
	        		<jsp:include page="/mg/common/comboCode.jsp" >
						<jsp:param name="codeId" value="A0903"/>
						<jsp:param name="tagname" value="TB_CL"/>
					</jsp:include>
   				</span>
   			</span> --%>
   			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1236", s_user.getLocale())%></span>
	   			<span>
	   				<input type="text" id="fromDate" value="" size="17" class="text2" align="center" onclick="setSens('toDate', 'max');" >
					<img id='calendar_icon' src='../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand" onclick="setSens('toDate', 'max'); setTimeout('calendar1.show();', 10);" > ~
					<input type="text" id="toDate" value="" size="17" class="text2" align="center" onclick="setSens('fromDate', 'min');">
					<img id='calendar_icon2' src='../../ext/images/calendar.png' width="13" height="15" border="0" align="absmiddle" style="cursor:hand" onclick="setSens('fromDate', 'min'); setTimeout('calendar2.show();', 10);">
	   			</span>
   			</span>
   			<%-- <span>
   				<span class="tit"><%=mm.getMessage("CPMG_1237", s_user.getLocale())%></span>
	   			<span>
	   				<input type="text" name="FILE_NM" class="text2" size="10" />
	   			</span>
   			</span> --%>
   			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1238", s_user.getLocale())%></span>
	   			<span>
	   				<input type="text" name="WORK_PC_IP" class="text2" size="16" />
	   			</span>
   			</span>
   			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1239", s_user.getLocale())%></span>
	   			<span>
	   				<input type="text" name="USER_ID" class="text2" size="10" />
	   			</span>
   			</span>
   			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1240", s_user.getLocale())%></span>
	   			<span>
	   				<input type="text" name="USER_NM" class="text2" size="10" />
	   			</span>
   			</span>
   			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1109", s_user.getLocale())%></span>
	   			<span>
	   				<!-- <input type="hidden" name="SVR_ID" /> -->
	        		<input type="text" name="SVR_ID" class="text2" size="20" />
	        		<%-- <a id="svrBtn" onclick="doPopupSvr();"><img src="<%=contextPath%>/ext/images/button/bt_search.gif"	alt="<%=mm.getMessage("COMG_1043", s_user.getLocale())%>" /></a> --%>
	   			</span>
   			</span>
<%--    			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1242", s_user.getLocale())%></span>
	   			<span>
	   				<input type="hidden" name="DEPT_CD" />
	        		<input type="text" name="DEPT_NM" class="text2" size="10" onclick="checkPopupDept();" />
	        		<a id="deptBtn" onclick="doPopupDept();"><img src="<%=contextPath%>/ext/images/button/bt_search.gif"	alt="<%=mm.getMessage("COMG_1043", s_user.getLocale())%>" /></a>
	   			</span>
   			</span> 
   			<span>
   				<span class="tit"><%=mm.getMessage("CPMG_1241", s_user.getLocale())%></span>
	   			<span>
	   				<input type="text" name="ALL" class="text2" size="10" />
	   			</span> 
   			</span> --%>
   			<span class="searchbtn2 m0">
               <%-- 	<a href="javascript:doExcelDown();" class="imgBtn"><img alt="<%=mm.getMessage("COMG_1049", s_user.getLocale())%>" src="<%=contextPath%>/ext/images/icon/excel.gif"></a> --%>
               	<a href="javascript:doQuery();" class="btn btnEnd"><span class="w40p"><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
   			</span>
		</div>
   		
		<table width="100%" border="0">
        <colgroup>
            <col width="30%" />
            <col width="40%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD">
        	<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : - </span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%>;
        	</div></td>
            <td>
	            <div class="btnarea">	            
	            	<%-- <a href="javascript:doAddRow();" class="btn"><span><%=mm.getMessage("COMG_1011", s_user.getLocale())%></span></a>
	            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("COMG_1013", s_user.getLocale())%></span></a> --%>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<!-- btnarea 내부에 버튼 존재할 경우 : gridbox=30 / 존재하지 않을 경우  gridbox=18 -->
<%-- <jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=45"/>
</jsp:include> --%>
</body>
</html>
