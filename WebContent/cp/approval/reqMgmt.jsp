<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
  IUser s_user = (IUser) session.getAttribute("j_user");
  String deptCd = s_user.getProperty("DEPT_CD").toString();
  String serverDay = s_user.getProperty("TODAY").toString();
  String docNo = StringUtils.paramReplace(request.getParameter("DOC_NO"));
  String stsCd = StringUtils.paramReplace(request.getParameter("CP_STS_CD"));
  String callForm = StringUtils.paramReplace(request.getParameter("form"));
  if(stsCd == null || stsCd == "null") stsCd = "01";
  IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
  IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
  String contextPath = request.getContextPath();
%>

<html>
<head>
<title>반출신청</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common.jsp"%> --%><%@ include file="../../ext/include/su_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<script type="text/javascript" src="../../ext/js/json2.js"></script>

<script type="text/javascript">
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;
var userType		= "approver";
// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";


// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
   	// GridObj.setColSorting("<%=grid_col_sort%>");
   	// doQuery();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	goPopupPolicy(rowId, "grid");
	row_id = rowId;
}

function doOnRowSelect(rowId, cellInd) {}

function goPopupPolicy(rowId, callFlag)
{
	var user_id = GridObj.cells(rowId, GridObj.getColIndexById("USER_ID")).getValue();
	var user_nm = GridObj.cells(rowId, GridObj.getColIndexById("USER_NM")).getValue();
	var company_id = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_ID")).getValue();
	var company_nm = GridObj.cells(rowId, GridObj.getColIndexById("COMPANY_NM")).getValue();
	var policy = GridObj.cells(rowId, GridObj.getColIndexById("POLICY")).getValue();
	var expireDt = GridObj.cells(rowId, GridObj.getColIndexById("EXPIRE_DT")).getValue();
	var policyDoc = GridObj.cells(rowId, GridObj.getColIndexById("POLICY_DOC")).getValue();
	var readCount = GridObj.cells(rowId, GridObj.getColIndexById("READ_COUNT")).getValue();
	var modStop = GridObj.cells(rowId, GridObj.getColIndexById("MOD_STOP")).getValue();
	var macList = GridObj.cells(rowId, GridObj.getColIndexById("MAC_LIST")).getValue();
	var msg = GridObj.cells(rowId, GridObj.getColIndexById("MESSAGE")).getValue();
	var email = GridObj.cells(rowId, GridObj.getColIndexById("EMAIL")).getValue();
	
	var url = "<%=contextPath%>/cp/approval/popupPolicy.jsp?rowId=" + rowId;
		url += "&user_id=" + user_id;
		url += "&user_nm=" + user_nm;
		url += "&company_id=" + company_id;
		url += "&company_nm=" + company_nm;
		url += "&policy=" + policy;
		url += "&expireDt=" + expireDt;
		url += "&policyDoc=" + policyDoc;
		url += "&readCount=" + readCount;
		url += "&modStop=" + modStop;
		url += "&macList=" + macList;
		url += "&msg=" + msg;
		url += "&email=" + email;
		url += "&stsCd=<%=stsCd%>";
		url += "&callFlag=" + callFlag;
		url += "&rowId=" + rowId;
		url += "&callform=<%=callForm%>";
	popupPolicy("OO", url, 540, 390);
}

//그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {

	var docNo = document.form.DOC_NO.value;
	if(docNo == "") {
		return;
	}

	var grid_col_id = "<%=grid_col_id%>";
	var argu = "&DOC_NO=" + docNo;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectDocUser&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";

	if(status == "false") alert(msg);
	return true;
}

function doSave() {
		
	if(document.form.DOC_TITLE.value == "") {  // "문서명을 입력하여 주십시요."
		alert("<%=mm.getMessage("CPMG_1015", s_user.getLocale())%>");
		return;
	}
	
	// if(document.form.OUT_CL_CD[0].checked) {   // 원본반출시에만 파일명 입력
		if(document.form.EXE_FILE.value == "") { // 파일명을 입력하여 주십시요.
			alert("<%=mm.getMessage("CPMG_1016", s_user.getLocale())%>");
			return;
		}
	// }
	
	if(document.form.FILE_LIST.value == "") {  //반출할 파일을 Upload 해 주십시요.
		alert("<%=mm.getMessage("CPMG_1017", s_user.getLocale())%>");
		return;
	}

	if(document.form.DOC_MSG.value == "") { // 반출 사유를 입력 해 주십시요.
		alert("<%=mm.getMessage("CPMG_1019", s_user.getLocale())%>");
		return;
	}
	
	// alert(document.form.AUTHORITY.value);
	// if(document.form.AUTHORITY.value != "5") { // 수신자를 등록 해 주십시요.
		if(!checkRows()) {
			alert("<%=mm.getMessage("CPMG_1018", s_user.getLocale())%>");
			return;
		}
	// }
		
	/*
	var outCode="01";
	if(document.form.OUT_CL_CD[1].checked == true){ // 원본반출
		outCode="02";
	}
	*/
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
		
	if (confirm("<%=mm.getMessage("CPMG_1020", s_user.getLocale())%>")) {
		var fileList = document.form.FILE_LIST.value;
		var filn = fileList.split("|");
		var url = "&DOC_NO=" + document.form.DOC_NO.value;
			url += "&DOC_TITLE=" + encodeURIComponent(document.form.DOC_TITLE.value);
			url += "&USER_ID=" + document.form.USER_ID.value;
			url += "&EXE_RULE=" + encodeURIComponent(document.form.EXE_RULE.value);
			url += "&AUTHORITY=" + document.form.AUTHORITY.value;
			url += "&CREATOR_EMAIL=" + encodeURIComponent(document.form.CREATOR_EMAIL.value);
			url += "&WAITING_TIME=" + document.form.WAITING_TIME.value;
			url += "&IN_PROCESS=" + encodeURIComponent(document.form.IN_PROCESS.value);
			url += "&FILE_NUM=" + filn.length;
			url += "&FILE_LIST=" + encodeURIComponent(document.form.FILE_LIST.value);
			url += "&P_DOC_NO=" + document.form.P_DOC_NO.value;
			url += "&EXE_FILE=" + encodeURIComponent(document.form.EXE_FILE.value);
			url += "&APPROVER_ID=" + encodeURIComponent(document.form.APPROVER_ID.value);
			url += "&DOC_MSG=" + encodeURIComponent(document.form.DOC_MSG.value);
			url += "&OUT_CL_CD=01";
		
		var cols_ids = "<%=grid_col_id%>";
        var SERVLETURL = G_SERVLETURL + "?mod=saveRequest&col_ids="+cols_ids+url;
		myDataProcessor = new dataProcessor(SERVLETURL);
		
		/*
		if(document.form.AUTHORITY.value == "5") {
			var turi = "&ids=&col_ids=";
			sendRequest(doAjaxTransction, "", 'POST', G_SERVLETURL + "?mod=saveRequest" + url + turi , false, false);
		} else {
			*/
			sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		// }
	}
}

function doAjaxTransction(oj) {
	 var result = oj.responseText; 
	 // alert(result);
}

function doSaveEnd(obj) {
	var message = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");	
	
	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {	
		if(message == "CP_0101") {  // 등록성공시 신청목록 조회 화면으로 이동
			// 문서등록 화면으로 이동
			var argu_param = "";
			parent.parent.mainFrame.location.href="./requestListLayout.jsp?formid=CP_0101&gridid=GridObj";
			
		} else {
			doQuery();
		}

	} else {
		alert(message);
	}
	return false;
}

function doApproval(approvalYn) {
	var msg="", chkMsg="";
	if(approvalYn == "N") {
		msg = "<%=mm.getMessage("CPMG_1012", s_user.getLocale())%>";  // 반려
		chkMsg = "<%=mm.getMessage("CPMG_1032", s_user.getLocale())%>";  // 반려
	} else {
		msg = "<%=mm.getMessage("CPMG_1013", s_user.getLocale())%>";  // 승인
		chkMsg = "<%=mm.getMessage("CPMG_1033", s_user.getLocale())%>";  
	}
	if(document.form.APPROVAL_MSG.value == "") {
		alert(chkMsg);
		return;
	}
	
	if (confirm(msg)) { 
		var cols_ids = "<%=grid_col_id%>";
		var argu = "&APPROVAL_YN=" + approvalYn;
			argu += "&DOC_NO=" + document.form.DOC_NO.value;
			argu += "&DOC_TITLE=" + document.form.DOC_TITLE.value;
			argu += "&APPROVAL_MSG="  + encodeURIComponent(document.form.APPROVAL_MSG.value);
			argu += "&DOC_MSG=" + encodeURIComponent(document.form.DOC_MSG.value);
        var SERVLETURL = G_SERVLETURL + "?mod=singleApproval&"+argu;	    
	    sendRequest(doAjaxResult, "", 'POST', SERVLETURL , false, false);
	}
}

// 승인결과 Response  -> 승인 또는 반려 되면 목록 화면으로 이동 한다.
function doAjaxResult(oj) {
	// alert(oj.responseText);
	if (confirm(oj.responseText + "\n승인화면으로 이동 하시겠습니까 ?")){
		var msg = oj.responseText.substring(0, 2);
		if("승인" == msg || "반려" == msg) {   // 승인 화면으로 이동함.
			window.history.back();
		}
	}
}

function doDelete() {
	if(!checkRows()) return;
	
	if (confirm("<%=mm.getMessage("CPMG_1021", s_user.getLocale())%>")) {  // 선택한 수신자를 삭제하시겠습니까?
		var grid_array = getGridChangedRows(GridObj, "SELECTED");
		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteDoc&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}


function doOnCellChange(stage,rowId,cellInd) 
{
	var max_value = GridObj.cells(rowId, cellInd).getValue();
	   
   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

// 반출 대상 파일 Upload
function doUpload() {
	var docId = document.form.DOC_NO.value;
	var authority = document.form.AUTHORITY.value;

	popupAttachFile("File Upload", '', '', 440, 400, "regMgmt", docId, authority, "write",<%=pm.getString("component.contextPath.root")%>);
}

function OkFileUpload(fileList) {
	document.form.FILE_LIST.value = fileList;
}

// 수신자 추가 함수
function doAddRow() {
		var url = "<%=contextPath%>/cp/approval/popupPolicy.jsp?rowId=";
			url += "&user_id=";
			url += "&user_nm=";
			url += "&company_id=";
			url += "&company_nm=";
			url += "&policy=11111";
			url += "&expireDt="+ "<%=s_user.getProperty("NEXTMONDAY") %>";
			url += "&policyDoc=10011";
			url += "&readCount=10";
			url += "&modStop=0";
			url += "&macList=";
			url += "&msg=";
			url += "&email=";
			url += "&stsCd=01";
			url += "&callFlag=grid";
			url += "&rowId=";
			url += "&callform=";
		popupPolicy("OO", url, 540, 390);
}

function OkPopupPolicy(row_id, user_id, user_nm, company_id, company_nm, policy, expireDt, exeCount, docPolicy, nonStop, macList, message, email) {
    var nMaxRow2 = parseInt(row_id);
	var row_data = "<%=grid_col_id%>";
	
	if(row_id=="")   // 신규 등록 이면 이미 추가된 사용자 검사 
	{
		for(var i = 1; i<= GridObj.getRowsNum(); i++) {
			var compUser = GridObj.cells(i, GridObj.getColIndexById("USER_ID")).getValue();
	
			if( user_id ==  compUser ) {
				alert( "[" + compUser + "] <%=mm.getMessage("CPMG_1039", s_user.getLocale())%>");    // 이미 등록된 수신자 입니다.
				return;
			}
		}
		dhtmlx_last_row_id++;
		nMaxRow2 = dhtmlx_last_row_id;
		GridObj.enableSmartRendering(true);
		GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
	}

  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_ID")).setValue(user_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("USER_NM")).setValue(user_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_ID")).setValue(company_id);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("COMPANY_NM")).setValue(company_nm);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY")).setValue(policy);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EXPIRE_DT")).setValue(expireDt);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("POLICY_DOC")).setValue(docPolicy);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("READ_COUNT")).setValue(exeCount);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MOD_STOP")).setValue(nonStop);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MAC_LIST")).setValue(macList);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("MESSAGE")).setValue(message);
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("EMAIL")).setValue(email);
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;

	ed_flag = "Y";
}

function batchPopupPolicy(row_id, user_id, user_nm, company_id, company_nm, policy, expireDt, exeCount, docPolicy, nonStop, macList, message, email) 
{
	var url = G_SERVLETURL + "?mod=changePolicy";
	url += "&POLICY=" + policy;
	url += "&EXPIRE_DT="+ expireDt;
	url += "&POLICY_DOC=" + docPolicy;
	url += "&READ_COUNT=" + exeCount;
	url += "&MOD_STOP=" + nonStop;
	url += "&MAC_LIST=" + macList;
	url += "&MESSAGE=" + encodeURIComponent(message);
	url += "&DOC_NO="  + document.form.DOC_NO.value;

	sendRequest(doAjaxChangePolicyResult, " " ,'POST', url , false, false);
}

// 일괄 정책 변경 결과
function doAjaxChangePolicyResult(oj) {
	if(oj.responseText == "Success") {
		// alert(oj.responseText);
		doQuery();
	} else {
		alert(oj.responseText);
	}
}


function init() {
	setFormDraw();
	if("<%=docNo %>" == "null") {
		// Exe 생성번호 취득
		doAjaxGetCpDocNo();
		document.form.EXE_RULE.value="USER Creation";
		document.form.DOC_TITLE.focus();
	} else {  // 문서조회
		doAjaxGetCpDocInfo("<%=docNo %>");
	}
	
	// 신청중문서가 아니면 Master  정보를 바꿀 수 없다.
	if("<%=stsCd%>" != "01") {
		document.form.EXE_FILE.readOnly = true;
		document.form.DOC_TITLE.readOnly = true;
		document.form.DOC_MSG.readOnly = true;
		document.form.AUTHORITY.disabled = true;
		document.form.USER_ID.readOnly = true;
		document.form.APPROVER_ID.readOnly = true;
		document.form.FILE_LIST.readOnly = true;
	}
}

function doAjaxCpDocInfo(JsonData) {
	var jData = JSON.parse(JsonData);
	
	document.form.DOC_NO.value = jData.DOC_NO;
	document.form.DOC_TITLE.value = jData.DOC_TITLE;
	document.form.USER_ID.value = jData.USER_ID;
	document.form.USER_NM.value = jData.USER_NM;
	document.form.EXE_RULE.value = jData.EXE_RULE;
	document.form.AUTHORITY.value = jData.AUTHORITY;
	document.form.CREATOR_EMAIL.value = jData.CREATOR_EMAIL;
	document.form.WAITING_TIME.value = jData.WAITING_TIME;
	document.form.IN_PROCESS.value = jData.IN_PROCESS;
	document.form.FILE_LIST.value = jData.FILE_LIST;
	document.form.P_DOC_NO.value = jData.P_DOC_NO;
	document.form.EXE_FILE.value = jData.EXE_FILE;
	document.form.APPROVER_ID.value = jData.APPROVER_ID;
	document.form.APPROVER_NM.value = jData.APPROVER_NM;
	document.form.DOC_MSG.value = jData.DOC_MSG;
	/*
	if(jData.OUT_CL_CD == "01") {
		document.form.OUT_CL_CD[0].checked = true;
	} else {
		document.form.OUT_CL_CD[1].checked = true;
	}
	*/
	doQuery();
}

function doAjaxCpDocNo(docNo) {
	document.form.DOC_NO.value = docNo;
}

function doUserPop() {
	userType = "user";
	popupUser("승인자 선택", '', '', 440, 400);
}

function doApproverPop() {
    // 승인자는 선택 할 수 없도록 함. : 권한자만 선택 할 수 있도록 열림.
	// userType = "approver";
	// popupUser("작성자 선택", '', '', 440, 400);
}

function OkPopupUser(userId, userNm) {
	if(userType == "approver") {
		document.form.APPROVER_ID.value = userId;
		document.form.APPROVER_NM.value = userNm;	
	} else {
		document.form.USER_ID.value = userId;
		document.form.USER_NM.value = userNm;	
	}
}

function doChangePolicy()
{
	goPopupPolicy(1, "batch");
}



</script>

</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table class="main-table">
<tr>
   	<td>
		<%-- <%@ include file="../../mg/common/milestone.jsp"%> --%>

        <table class="board-search">
        <colgroup>
            <col width="20%" />
            <col width="30%" />	  
            <col width="12%" />
            <col width="30%" />       
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1109", s_user.getLocale())%></td>
        	<td>
        		<input type="text" name="DOC_NO" size="46" class="text" readonly="readonly" style="IME-MODE:disabled">
			</td>
			<td class="tit"><%=mm.getMessage("CPMG_1110", s_user.getLocale())%></td>
        	<td>
        		<input type="text" name="P_DOC_NO" size="46" class="text" readonly="readonly" style="IME-MODE:disabled">
        	<!-- 원본반출 기능 무인증으로 대체 함. 
        		<input type="hidden" name="P_DOC_NO" size="46" class="text" readonly="readonly" style="IME-MODE:disabled">
				<div class="title_num" align="left">
        			 <input type="radio" name="OUT_CL_CD" value="01" checked>
        			 <input type="radio" name="OUT_CL_CD" value="02">
        		</div>
        	-->
			</td>
        </tr>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1101", s_user.getLocale())%></td>
        	<td>
				<input type="text" name="DOC_TITLE" size="46" class="text">
			</td>
			<td class="tit"><%=mm.getMessage("CPMG_1102", s_user.getLocale())%></td>
        	<td>
				<input type="text" name="EXE_FILE" size="46" class="text" style="IME-MODE:disabled">
			</td>
		</tr>
		<tr>
			<td class="tit"><%=mm.getMessage("CPMG_1111", s_user.getLocale())%></td>
            <td>			
				<jsp:include page="../../mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0301"/>
					<jsp:param name="tagname" value="AUTHORITY"/>
				</jsp:include>
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1112", s_user.getLocale())%></td>
        	<td>
				<input type="text" name="EXE_RULE" size="46" class="text" readonly="readonly">
			</td>
		</tr>
		<tr>
			<td class="tit"><%=mm.getMessage("CPMG_1113", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_ID"  size="6"  value="<%=s_user.getId() %>" class="text" onchange="document.form.USER_NM.value=''">
				<a href="javascript:doUserPop();"><img src="../../ext/images/btn_i_search.gif" align="middle" alt="조회" class="mgl-5" /></a>
				<input type="text" name="USER_NM"  size="10" value="<%=s_user.getName() %>" class="text">
			</td>  
            <td class="tit">E-Mail</td>
        	<td>
				<input type="text" name="CREATOR_EMAIL" value="<%=s_user.getProperty("EMAIL").toString() %>" size="46" class="text">
			</td>
		</tr>  
		<tr>
			<td class="tit"><%=mm.getMessage("CPMG_1105", s_user.getLocale())%></td>
        	<td colspan="3">
				<input type="hidden" name="WAITING_TIME" size="10" value="10" class="text">
				<input type="text" name="APPROVER_ID"  size="6"  value="<%=s_user.getProperty("MANAGER_ID").toString() %>" class="text"  readonly="readonly" style="IME-MODE:disabled">
				<a href="javascript:doApproverPop();"><img src="../../ext/images/btn_i_search.gif" align="middle" alt="조회" class="mgl-5" /></a>
				<input type="text" name="APPROVER_NM"  size="10" value="<%=s_user.getProperty("MANAGER_NM").toString() %>" class="text" readonly="readonly" style="IME-MODE:disabled">
			</td>
			<!--  
			<td class="tit"><%=mm.getMessage("CPMG_1114", s_user.getLocale())%></td>
            <td>
				<input type="text" name="IN_PROCESS" size="46" class="text" style="IME-MODE:disabled">
			</td>
			-->
			<input type="hidden" name="IN_PROCESS" size="46" class="text" style="IME-MODE:disabled">
		</tr>
		<tr>
			<td class="tit"><%=mm.getMessage("CPMG_1115", s_user.getLocale())%></td>
           	<% if("approvalList".equals(callForm) && "01".equals(stsCd) ) { %>
	           	<td>
					<input type="text" name="DOC_MSG" size="60" class="text"> 
				</td> 
				<td class="tit"><%=mm.getMessage("CPMG_1144", s_user.getLocale())%></td>
	            <td>
					<input type="text" name="APPROVAL_MSG" size="46" class="text"> 
				</td> 
           	<% } else { %> 
           		<td colspan="3">
					<input type="text" name="DOC_MSG" size="60" class="text"> 
				</td> 
           	<%} %>
		</tr> 
		<tr>
			<td class="tit"><%=mm.getMessage("CPMG_1116", s_user.getLocale())%></td>
        	<td colspan="2">
				<input type="text" name="FILE_LIST" size="100" class="text" readonly="readonly" style="IME-MODE:disabled">
			</td>
            <td>
				<div class="searchbtn">
				    <% if(Integer.parseInt(stsCd) < 3 && !"approvalList".equals(callForm) && !"cpDocList".equals(callForm)) { %>
                	<a href="javascript:doUpload();" class="btn"><span>Upload</span></a>
                	<% } %>
                </div>
			</td>
		</tr>
        </table>
        
        <table class="main-table">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            	<% if("cpDocList".equals(callForm)) { %>
	            		<a href="javascript:doChangePolicy();" class="btn"><span><%=mm.getMessage("CPMG_1145", s_user.getLocale())%></span></a>
		            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            	<% } else if("approvalList".equals(callForm) && "01".equals(stsCd)) { %>
		            	<a href="javascript:doApproval('N');" class="btn"><span><%=mm.getMessage("CPMG_1107", s_user.getLocale())%></span></a>
		            	<a href="javascript:doApproval('Y');" class="btn"><span><%=mm.getMessage("CPMG_1108", s_user.getLocale())%></span></a>
	            	<% } else if( Integer.parseInt(stsCd) < 3 ) { %>
	            		<a href="javascript:doAddRow();" class="btn"><span><%=mm.getMessage("CPMG_1117", s_user.getLocale())%></span></a>
		            	<a href="javascript:doDelete();" class="btn"><span><%=mm.getMessage("CPMG_1118", s_user.getLocale())%></span></a>
		            	<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("CPMG_1119", s_user.getLocale())%></span></a>
	            	<% } else { %>
	            		<a href="javascript:doSave();" class="btn"><span><%=mm.getMessage("COMG_1012", s_user.getLocale())%></span></a>
	            	<% } %>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="65%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event2.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=302"/>
</jsp:include>
</body>
</html>
