<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.menu.IMenu"%>
<%@ page import="com.core.component.menu.IMenuManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="java.net.*"%>

<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
boolean isAdmin = false;
if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
}
String contextPath =request.getContextPath();
%>

<html>
<head>
<title>승인내역 조회</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common_render.jsp" %> --%><%@ include file="../../ext/include/su_grid_common.jsp" %>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp" %>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
    if(header_name != "APPROVAL_MSG" && header_name != "FILE_ICON" ) {
    	var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
    	var stsCd = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();
	      
		// 문서등록 화면으로 이동
		
		<%
			// 메뉴이름을 얻어오기 위한 작업
			IMenuManagement mnu = (IMenuManagement) ComponentRegistry.lookup(IMenuManagement.class);
			IMenu menu = mnu.getMenu("CP_0102", s_user);
			String menuName = menu.getName(s_user.getLocale());
		%>
	
		var menuName = "<%=java.net.URLEncoder.encode(menuName,"utf-8")%>";
		
		
		
		var argu_param = "?DOC_NO=" + docId; 
		     argu_param += "&CP_STS_CD="+ stsCd;
		     argu_param += "&form=approvalList";
		     if(GridObj.cells(rowId, GridObj.getColIndexById("POLICY_CL_CD")).getValue()=="2" && "<%=pm.getString("cliptorplus.vf.export")%>" == "Y") argu_param += "&formid=CP_0209";
		     else argu_param += "&formid=CP_0205";
		     argu_param += "&gridid=gridObj";
		     argu_param += "&leftMenuName=" + menuName; 
		     argu_param += "&policyClCd=" + GridObj.cells(rowId, GridObj.getColIndexById("POLICY_CL_CD")).getValue(); 
		<%-- doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", argu_param); --%>
		
		parent.parent.pageRedirect("cp/approval/cpReqDetailLayout.jsp", argu_param);
	}
}

// Grid Row one Clieck Event 처리
function doOnRowSelect(rowId, cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	row_id = rowId;
	
	if( header_name == "FILE_ICON" ) {
		var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();
		popupAttachFile("File Upload", '', '', 440, 400, "regMgmt", docId, "", "read",<%=pm.getString("component.contextPath.root")%>);
	}
}

function doTransDetail()
{
	if(row_id > 0) {
		 // 문서등록 화면으로 이동
		 var argu_param = "?DOC_NO=" + GridObj.cells(row_id, GridObj.getColIndexById("DOC_NO")).getValue(); 
		      argu_param += "&CP_STS_CD=" + GridObj.cells(row_id, GridObj.getColIndexById("CP_STS_CD")).getValue(); 
		      argu_param += "&form=approvalList";
		 doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", argu_param);
	}
}

function OkFileUpload(fileList) {
	
}

function OkPopupDeptGrid(dept_cd, plant_cd, dept_nm) {

}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&APPROVER_ID="	+ document.form.APPROVER_ID.value;
		argu += "&DOC_TITLE="	+encodeURIComponent(document.form.DOC_TITLE.value);
		argu += "&EXE_FILE="	+encodeURIComponent(document.form.EXE_FILE.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		argu += "&CP_STS_CD=" + document.form.CP_STS_CD.value;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectApproval&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();
   	var sts_cd = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_CD")).getValue();

   	if(stage==0) {
   		
   		if(cellInd == 0) {
	   		if(sts_cd == "01") {
	   			sel_change = GridObj.cells(rowId, cellInd).getValue();
	   			return true;
	   		} else {
	   			var sts_nm = GridObj.cells(rowId, GridObj.getColIndexById("CP_STS_NM")).getValue();
	   			// 완료 또는 진행중인 문서는 승인 할 수 없습니다."
	   			alert("<%=mm.getMessage("CPMG_1011", s_user.getLocale())%>");  
	   			return false;
	   		}
   		} else if(cellInd == 7) {  // 반려사유
   			if(sts_cd == "01") {
	   			return true;
	   		} else {
	   			return false;
	   		}
   		}
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}

		return true;
	}
	return false;
}

function doSave(approvalYn) {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	var msg="", chkMsg="";
	if(approvalYn == "N") {
		msg = "<%=mm.getMessage("CPMG_1012", s_user.getLocale())%>";  // 반려
		chkMsg = "<%=mm.getMessage("CPMG_1032", s_user.getLocale())%>";  // 반려
	} else {
		msg = "<%=mm.getMessage("CPMG_1013", s_user.getLocale())%>";  // 승인
		chkMsg = "<%=mm.getMessage("CPMG_1033", s_user.getLocale())%>";  
	}
	
	if (confirm(msg)) { 
		for(var i=0; i < grid_array.length; i++) {
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("CP_STS_CD")).getValue() != "01") {
				alert("<%=mm.getMessage("CPMG_1041", s_user.getLocale())%>");
				return;
			}
			
			if(GridObj.cells(grid_array[i], GridObj.getColIndexById("APPROVAL_MSG")).getValue() == "") {
				alert(chkMsg);
				return;
			}
		}

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
			var argu = "&APPROVAL_YN=" + approvalYn;
	        var SERVLETURL = G_SERVLETURL + "?mod=saveApproval&col_ids="+cols_ids+argu;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doAddRow() {
	dhtmlx_last_row_id++;
   	var nMaxRow2 = dhtmlx_last_row_id;
   	var row_data = "<%=grid_col_id%>";
  	
	GridObj.enableSmartRendering(true);
	GridObj.addRow(nMaxRow2, "", GridObj.getRowIndex(nMaxRow2));
  	GridObj.selectRowById(nMaxRow2, false, true);
  	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).cell.wasChanged = true;
	GridObj.cells(nMaxRow2, GridObj.getColIndexById("SELECTED")).setValue("1");
		
	dhtmlx_before_row_id = nMaxRow2;
	addRowId = nMaxRow2;
	ed_flag = "Y";

}

function doDelete() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
	if (confirm("<%=mm.getMessage("CPMG_1013")%>")) { //삭제하시겠습니까?

		if(grid_array.length == 0) {
			doQuery();
		} else {
			var cols_ids = "<%=grid_col_id%>";
	        var SERVLETURL = G_SERVLETURL + "?mod=deleteDoc&col_ids="+cols_ids;	
			myDataProcessor = new dataProcessor(SERVLETURL);
		    sendTransactionGrid(GridObj, myDataProcessor, "SELECTED", grid_array);
		}
	}	
}

function doApproverPop() {
	if(<%=isAdmin%> == true) {
		popupUser("승인자 선택", '', '', 440, 400);
	}
}

function OkPopupUser(userId, userNm) {
	document.form.APPROVER_ID.value = userId;
	document.form.APPROVER_NM.value = userNm;	
}

function init() {
	document.form.DOC_TITLE.focus();
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	if(<%=isAdmin%> == true) {
		document.form.APPROVER_ID.readOnly =  false;
		document.form.APPROVER_NM.readOnly  = false;
	} else {
		document.form.APPROVER_ID.readOnly =  true;
		document.form.APPROVER_NM.readOnly  = true;
	}
	
	setFormDraw();
	
	doQuery();
}
</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<%-- <%@ include file="/mg/common/milestone.jsp"%> --%>

        <table class="board-search">
        <colgroup>
            <col width="10%" />
            <col width="15%" />
            <col width="15%" />
            <col width="15%" />
            <col width="10%" />
            <col width="25%" />
            <col width="80px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1101",s_user.getLocale())%></td>
            <td>
            	<input type="text" name="DOC_TITLE"  size="15" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1102",s_user.getLocale())%></td>
            <td>
				<input type="text" name="EXE_FILE" size="15" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1103", s_user.getLocale())%></td>
            <td>
            	<input type="text" id="fromDate" value="" size="8" class="text" align="center">
				<img id="imgFromDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand"> ~
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id="imgToDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
			</td>
			<td>
            </td>
        </tr>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1104", s_user.getLocale())%></td>
            <td>
            	<jsp:include page="/mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0302"/>
					<jsp:param name="tagname" value="CP_STS_CD"/>
				</jsp:include>
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1105", s_user.getLocale())%></td>
            <td colspan="3">
				<input type="text" name="APPROVER_ID"  size="6"  value="<%=s_user.getId() %>" class="text" onchange="document.form.APPROVER_NM.value=''">
				<a href="javascript:doApproverPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="Call Search Popup" class="mgl-5" /></a>
				<input type="text" name="APPROVER_NM"  size="10" value="<%=s_user.getName() %>" class="text">
			</td>  
            <td>
                <div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	           		<a href="javascript:doTransDetail();" class="btn"><span><%=mm.getMessage("CPMG_1143", s_user.getLocale())%></span></a>
	            	<a href="javascript:doSave('N');" class="btn"><span><%=mm.getMessage("CPMG_1107", s_user.getLocale())%></span></a>
	            	<a href="javascript:doSave('Y');" class="btn"><span><%=mm.getMessage("CPMG_1108", s_user.getLocale())%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="83%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event2.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=182"/>
</jsp:include>
</body>
</html>
