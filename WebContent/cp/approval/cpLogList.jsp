<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String exportUser = pm.getString("component.export.user");
String iftype = pm.getString("component.site.iftype"); // REG
boolean isAdmin = false;
if( "S01".equals(s_user.getProperty("USER_ROLL").toString()) || "S02".equals(s_user.getProperty("USER_ROLL").toString())) {
	isAdmin = true;
}
boolean isSearchUser = true;
if("N".equals(pm.getString("cliptorplus.form.search.user"))) {
	isSearchUser = false;
}
String contextPath =request.getContextPath();
%>

<html>
<head>
<title>반출로그조회</title>
<%@ include file="../../ext/include/include_css.jsp"%>
<%-- <%@ include file="../../ext/include/grid_common.jsp"%> --%><%@ include file="../../ext/include/su_grid_common.jsp"%>	<%-- Dhtmlx Grid용 JSP--%>
<%@ include file="../../ext/include/dhtmlx_ComboAjax.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>
var GridObj 		= {};
var MenuObj 		= {};
var combobox        = {};
var myDataProcessor = {};
var row_id          = 0;
var filter_idx      = 0;

// 등록화면
var addRowId		= 0;
var ed_flag			= ""; //행추가 여부 Y일때만 팝업이벤트 발생
var sel_change		= "";

var G_SERVLETURL = "<%=contextPath%>/cp/approval.do";

// Body Onload 시점에 setGridDraw 호출시점에 grid_common.jsp에서 SLANG 테이블 SCREEN_ID 기준으로 모든 컬럼을 Draw 해주고
// 이벤트 처리 및 마우스 우측 이벤트 처리까지 해줍니다.
function setFormDraw() {
  	GridObj = setGridDraw(GridObj);
   	GridObj.setSizes();
}

// 위로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowUp() {
	GridObj.moveRow(GridObj.getSelectedId(),"up");
}

// 아래로 행이동 시점에 이벤트 처리해 줍니다.
function doMoveRowDown() {
	GridObj.moveRow(GridObj.getSelectedId(),"down");
}

// 그리드 클릭 이벤트 시점에 호출 됩니다. rowId 는 행의 ID이며 cellInd 값은 컬럼 인덱스 값이며
// 이벤트 처리시 컬럼명 과 동일하게 처리하시려면 GridObj.getColIndexById("selected") == cellInd 이렇게 처리하시면 됩니다.
function doOnRowSelected(rowId,cellInd) {
	var header_name = GridObj.getColumnId(cellInd);
	var docId = GridObj.cells(rowId, GridObj.getColIndexById("DOC_NO")).getValue();

	// 문서등록 화면으로 이동
	var argu_param = "?DOC_NO=" + docId; 
	     argu_param += "&CP_STS_CD=04";
	     argu_param += "&form=";
	if("<%=iftype%>" == "LINK") {
		doAjaxMenuTrans("CP_02", "CP_0205", "<%=col_del%>", argu_param);
	} else {
		doAjaxMenuTrans("CP_01", "CP_0102", "<%=col_del%>", argu_param);
	}
	
}

function doOnRowSelect(rowId, cellInd) {}


function OkFileUpload(fileList) {
	
}

function OkPopupDeptGrid(dept_cd, plant_cd, dept_nm) {

}

// 그리드의 선택된 행의 존재 여부를 리턴하는 함수 입니다.
function checkRows() {
	var grid_array = getGridChangedRows(GridObj, "SELECTED");

	if(grid_array.length > 0) {
		return true;
	}
	return false;
}

// 데이터 조회시점에 호출되는 함수입니다.
// 조회조건은 encodeURIComponent() 함수로 다 전환하신 후에 loadXML 해 주십시요
// 그렇지 않으면 다국어 지원이 안됩니다.
function doQuery() {
	var grid_col_id     = "<%=grid_col_id%>";
	var argu  = "&USER_ID="	+ document.form.USER_ID.value;
		argu += "&DOC_NO="	+encodeURIComponent(document.form.DOC_NO.value);
		argu += "&DOC_TITLE="	+encodeURIComponent(document.form.DOC_TITLE.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		argu += "&REQUESTER_ID=" + document.form.REQUESTER_ID.value;
		
	GridObj.loadXML(G_SERVLETURL+"?mod=selectDocLog&grid_col_id="+grid_col_id+argu);																						
	GridObj.clearAll(false);																																											
}

// doQuery 종료 시점에 호출 되는 이벤트 입니다. 인자값은 그리드객체 및 전체행숫자 입니다.
// GridObj.getUserData 함수는 서블릿에서 message, status, data_type, setUserObject 시점에 값을 읽어오는 함수 입니다.
// setUserObject Name 값은 0, 1, 2... 이렇게 읽어 주시면 됩니다.
function doQueryEnd(GridObj, RowCnt) {
   	var msg        = GridObj.getUserData("", "message");
	var status     = GridObj.getUserData("", "status");
	document.getElementById("totalCntTD").innerHTML = "<%=mm.getMessage("COMG_1014", s_user.getLocale())%> <span class='point'> : " + GridObj.getRowsNum()+"</span> <%=mm.getMessage("COMG_1015", s_user.getLocale())%> ";
	if(status == "false") alert(msg);
	return true;
}

function doExcelDown()
{	
	var grid_col_id     = "<%=grid_col_id%>";
	var grid_col_width  = "<%=grid_init_widths%>";
	var grid_col_header = encodeURIComponent("<%=grid_header%>");
	
	var argu  = "&USER_ID="	+ document.form.USER_ID.value;
		argu += "&DOC_NO="	+encodeURIComponent(document.form.DOC_NO.value);
		argu += "&DOC_TITLE="	+encodeURIComponent(document.form.DOC_TITLE.value);
		argu += "&FROM_YMD=" + document.form.fromDate.value;	
		argu += "&TO_YMD=" + document.form.toDate.value;
		argu += "&REQUESTER_ID=" + document.form.REQUESTER_ID.value;
		
	fileDownload(G_SERVLETURL+"?mod=excelDocLog&grid_col_id="+grid_col_id+"&grid_col_header="+grid_col_header+"&grid_col_width="+grid_col_width+argu);	
}


function doOnCellChange(stage,rowId,cellInd) 
{
   	var max_value = GridObj.cells(rowId, cellInd).getValue();

   	if(stage==0) {
   		sel_change = GridObj.cells(rowId, cellInd).getValue();
   		return true;
	} else if(stage==1) {
	} else if(stage==2) {
		var tmpchange = GridObj.cells(rowId, cellInd).getValue();
		if( tmpchange == sel_change && cellInd != 4  ) {
			uncheckGrid(GridObj, rowId);
		}
		return true;
	}
	return false;
}

function doSave() {
	if(!checkRows()) return;
	var grid_array = getGridChangedRows(GridObj, "SELECTED");
}

function doSaveEnd(obj) {
	var messsage = obj.getAttribute("message");
	var mode     = obj.getAttribute("mode");
	var status   = obj.getAttribute("status");		

	myDataProcessor.stopOnError = true;

	if(dhxWins != null) {
		dhxWins.window("prg_win").hide();
		dhxWins.window("prg_win").setModal(false);
	}
	if(status == "true") {
		alert(messsage);		
		doQuery();
	} else {
		alert(messsage);
	}
	return false;
}

function doAddRow() {

}

function doDelete() {
	if(!checkRows()) return;	
}

function doUserPop() {
	var requster = document.form.REQUESTER_ID.value;
	if(requster == "") {
		alert("<%=mm.getMessage("CPMG_1036", s_user.getLocale())%>");
	} else {
		popupCompUser("수신자 선택", '', '', 440, 400, "<%=exportUser%>", requster,<%=pm.getString("component.contextPath.root")%> );
	}
	
}

function OkPopupCompUser(user_id, user_nm, company_id,company_nm, email) {
	document.form.USER_ID.value = user_id;
	document.form.USER_NM.value = user_nm;	
	document.form.COMPANY_ID.value = company_id;
	document.form.COMPANY_NM.value = company_nm;	
}

function doRequesterPop() {
	if(<%=isAdmin%> == true) {
		popupUser("반출자 선택", '', '', 440, 400);
	}
}

function OkPopupUser(user_id, user_nm) {
	document.form.REQUESTER_ID.value = user_id;
	document.form.REQUESTER_NM.value = user_nm;	
}

function init() {
	document.form.DOC_TITLE.focus();
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	
	if(<%=isSearchUser%> == true) {
		document.form.REQUESTER_ID.value = "<%=s_user.getId()%>";
		document.form.REQUESTER_NM.value = "<%=s_user.getName() %>";
	}
	if(<%=isAdmin%> == true) {
		document.form.REQUESTER_ID.readOnly =  false;
		document.form.REQUESTER_NM.readOnly  = false;
	} else {
		document.form.REQUESTER_ID.readOnly =  true;
		document.form.REQUESTER_NM.readOnly  = true;
	}
		
	setFormDraw();
	
	doQuery();
}
</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%">
<tr>
   	<td>
		<%-- <%@ include file="/mg/common/milestone.jsp"%> --%>

        <table class="board-search">
        <colgroup>
            <col width="10%" />
            <col width="15%" />
            <col width="10%" />
            <col width="20%" />
            <col width="10%" />
            <col width="25%" />
            <col width="80px" />
        </colgroup>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1109", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="DOC_NO"  size="15" class="text">
            </td>
        	<td class="tit"><%=mm.getMessage("CPMG_1101", s_user.getLocale())%></td>
            <td>
            	<input type="text" name="DOC_TITLE"  size="15" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1103", s_user.getLocale())%></td>
            <td>
            	<input type="text" id="fromDate" value="" size="8" class="text" align="center">
				<img id="imgFromDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand"> ~
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id="imgToDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
			</td>
			<td>
            </td>
        </tr>
        <tr>
        	<td></td>
        	<td></td>
        	<td class="tit"><%=mm.getMessage("CPMG_1113", s_user.getLocale())%></td>
            <td>
				<input type="text" name="REQUESTER_ID"  size="6"  value="<%=s_user.getId() %>" class="text" readonly="readonly" onchange="document.form.REQUESTER_NM.value=''">
				<a href="javascript:doRequesterPop();"><img src="../../ext/images/btn_i_search.gif" name="search1" align="absmiddle" alt="Call Search Popup" class="mgl-5" /></a>
				<input type="text" name="REQUESTER_NM"  size="10" value="<%=s_user.getName() %>" readonly="readonly" class="text">
			</td> 
			<td class="tit"><%=mm.getMessage("CPMG_1121", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_ID"  size="6"  value="" class="text" >
				<a href="javascript:doUserPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="Search User" class="mgl-5" /></a>
				<input type="text" name="USER_NM"  size="20" value="" class="text">
				<input type="hidden" name="COMPANY_ID"  size="6"  value="" class="text" >
				<input type="hidden" name="COMPANY_NM"  size="6"  value="" class="text" >
			</td>  
            <td>
                <div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>

		<table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div>
        	</td> 
            <td>
                <div class="btnarea">
	            	<a href="javascript:doExcelDown();" class="btn"><span><%=mm.getMessage("COMG_1049")%></span></a>
	            </div>
            </td>
        </tr>
        </table>
	</td>
</tr>
</table>
</form>
<div id="gridbox" name="gridbox" height="89%" width="100%" style="background-color:white;overflow:hidden"></div>
<div id="pagingArea"></div>
<jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="gridbox=162"/>
</jsp:include>
</body>
</html>
