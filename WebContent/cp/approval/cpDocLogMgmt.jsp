<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>

<%
// Dthmlx Grid 전역변수들..
// String grid_obj  = "GridObj"
IUser s_user = (IUser) session.getAttribute("j_user");
String formId = request.getParameter("formid");
String grid_obj = request.getParameter("gridid");
String fromDay = s_user.getProperty("FROMDAY").toString();
String serverDay = s_user.getProperty("TODAY").toString();
IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
IParameterManagement pm = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
String exportUser = pm.getString("component.export.user");
String iftype = pm.getString("component.site.iftype"); // REG
String contextPath =request.getContextPath();
%>

<html>
<head>
<title>반출로그조회</title>
<%@ include file="../../ext/include/include_css.jsp"%>

<script type="text/javascript" src="../../ext/js/ui_scripts.js"></script>
<script type="text/javascript" src="../../ext/js/ajaxlib/jslb_ajax.js"></script>
<script type="text/javascript" src="../../ext/js/JDate.js"></script>
<script type="text/javascript" src="../../ext/js/doAjaxCombo.js"></script>
<link rel="stylesheet" href="../../jquery/development-bundle/themes/base/jquery.ui.all.css">
	<script src="../../jquery/development-bundle/jquery-1.5.1.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.core.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.widget.js"></script>
	<script src="../../jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script src="../../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-ko.js"></script>
	<script>
		$(function() {
			$( "#fromDate" ).datepicker();
			$( "#imgFromDate").click(function() {$( "#fromDate" ).focus();});
		});
		$(function() {
			$( "#toDate" ).datepicker();
			$( "#imgToDate").click(function() {$( "#toDate" ).focus();});
		});
	</script>

<script>

var userType="";
function doUserPop() {
	userType = "user";
	popupUser("작성자 선택", '', '', 440, 400);
}

function doApproverPop() {
	userType = "approver";
	popupUser("승인자 선택", '', '', 440, 400);
}

function OkPopupUser(userId, userNm) {
	if(userType == "approver") {
		document.form.APPROVER_ID.value = userId;
		document.form.APPROVER_NM.value = userNm;	
	} else {
		document.form.USER_ID.value = userId;
		document.form.USER_NM.value = userNm;	
	}
}

function init() 
{
	document.form.fromDate.value = "<%=fromDay%>";
	document.form.toDate.value = "<%=serverDay%>";
	
	var argu = "&formid=CP_0208&gridid=<%=grid_obj%>" + "&user_id=";
	masterframe.location.href='<%=contextPath%>/cp/approval/cpDocLogMaster.jsp?eventframe=detailframe' + argu;
	
	parent.refresh("");
}

function doQuery() 
{
	masterframe.doQuery();
}
</script>
</head>
<body onload="init();" style="overflow:hidden;">
<form name="form" method="post">
<table width="100%" height="100%">
<tr>
   	<td>
		<%-- <%@ include file="/mg/common/milestone.jsp"%> --%>

        <table class="board-search">
        <colgroup>
            <col width="10%" />
            <col width="15%" />
	        <col width="10%" />
	        <col width="20%" />
            <col width="10%" />
            <col width="25%" />
            <col width="80px" />
        </colgroup>
        <tr>
       		
        	<td class="tit"><%=mm.getMessage("CPMG_1109",s_user.getLocale())%></td>
            <td>
            	<input type="text" name="DOC_NO"  size="15" class="text">
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1102",s_user.getLocale())%></td>
            <td>
				<input type="text" name="EXE_FILE" size="15" class="text">
            </td>
        
            <td class="tit"><%=mm.getMessage("CPMG_1103", s_user.getLocale())%></td>
            <td>
            	<input type="text" id="fromDate" value="" size="8" class="text" align="center">
				<img id="imgFromDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand"> ~
				<input type="text" id="toDate" value="" size="8" class="text" align="center" >
				<img id="imgToDate" src="../../ext/images/body/IcoCal.gif" width="17" height="20" border="0" align="absmiddle" style="cursor:hand">
			</td>
			<td>
            </td>
        </tr>
        <tr>
        	<td class="tit"><%=mm.getMessage("CPMG_1104", s_user.getLocale())%></td>
            <td>
            	<jsp:include page="../../mg/common/comboCode.jsp" >
					<jsp:param name="codeId" value="A0302"/>
					<jsp:param name="tagname" value="CP_STS_CD"/>
					<jsp:param name="def" value=" "/>
				</jsp:include>
            </td>
            <td class="tit"><%=mm.getMessage("CPMG_1105", s_user.getLocale())%></td>
            <td>
				<input type="text" name="APPROVER_ID"  size="6"  value="" class="text" onchange="document.form.APPROVER_NM.value=''">
				<a href="javascript:doApproverPop();"><img src="../../ext/images/btn_i_search.gif" align="absmiddle" alt="Call Search Popup" class="mgl-5" /></a>
				<input type="text" name="APPROVER_NM"  size="10" value="" class="text">
			</td> 
			<td class="tit"><%=mm.getMessage("CPMG_1113", s_user.getLocale())%></td>
            <td>
				<input type="text" name="USER_ID"  size="6"  value="" class="text" onchange="document.form.USER_NM.value=''">
				<a href="javascript:doUserPop();"><img src="../../ext/images/btn_i_search.gif" align="middle" alt="조회" class="mgl-5" /></a>
				<input type="text" name="USER_NM"  size="10" value="" class="text">
			</td>   
            <td>
                <div class="searchbtn">
                	<a href="javascript:doQuery();" class="btn"><span><%=mm.getMessage("COMG_1043", s_user.getLocale())%></span></a>
                </div>
            </td>
        </tr>
		</table>
	
	    <table width="100%" border="0">
        <colgroup>
            <col width="40%" />
            <col width="60%" />
        </colgroup>
        <tr>
        	<td><div class="title_num" id="totalCntTD"></div></td>
            <td>
	            <div class="btnarea">
	            </div>
            </td>
        </tr>
      </table>
   </td>
</tr>

<!-- <tr>
	<td>
      <table width="100%" border="0">
       <rowgroup>
            <row height="20%" />
            <row height="40%" />
        </rowgroup>
        <tr>
          <td valign="top">
				<iframe name="masterframe" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
        </tr>
        <tr>
          <td valign="top">
               <iframe name="detailframe" width="100%" marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
           </td>
        </tr>
      </table>
	</td>
</tr> -->

<tr>
	<td height="70%" valign="top">
				<iframe name="masterframe" height="100%" width="100%"  marginwidth="0" marginheight="0" scrolling="no" frameborder='no'></iframe>
			</td>
</tr>

</table>
</form>
<%-- <jsp:include page="../../ext/include/window_height_resize_event.jsp" >
	<jsp:param name="grid_object_name_height" value="detailframe=162"/>
</jsp:include> --%>
</body>
</html>
