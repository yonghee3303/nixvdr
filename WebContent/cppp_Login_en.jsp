<%@ page contentType="text/html; charset=UTF-8" session="true" 
         import="com.core.component.util.WebUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="java.util.Locale"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?xml version="1.0" encoding="UTF-8"?>
<%
	String contextPath = request.getContextPath();
    Locale locale = null;
	String imgLocale = null;
    Cookie[] cookies = request.getCookies();
    String login_name = "";
    
    for(int i=0; cookies != null && i<cookies.length; i++){
    
        if("login_name".equals(cookies[i].getName())) {
            login_name = cookies[i].getValue();
        }
        /*   ---
        if("locale".equals(cookies[i].getName())){
            locale = new java.util.Locale(cookies[i].getValue());
            session.setAttribute("org.apache.struts.action.LOCALE", locale);
        }
        */
    }
    
    String linkUid = StringUtils.paramReplace(request.getParameter("uid"));
 
    String log_name = StringUtils.paramReplace(request.getParameter("log_name"));
    String j_username = StringUtils.paramReplace(request.getParameter("j_username"));
    String j_language =StringUtils.paramReplace(request.getParameter("j_language"));
    
    //Message locale
    if(j_language.equals("ko")) {
		locale = Locale.KOREA;
	} else if(j_language.equals("zh")) {
		locale = Locale.CHINA;
	} else if(j_language.equals("ja")) {
		locale = Locale.JAPAN;
	} else {
		locale = Locale.US;
	}
    
	//Image locale
    if(j_language.equals("ko")) {
    	imgLocale = "";
	 } else if(j_language.equals("en") || j_language.equals("zh")) {
		 imgLocale = "/" + j_language;
	 } else {
		 imgLocale = "/en";
	 }
  
    /*
    if(j_language == null || j_language == "")  {
    	j_language = "en";
    }
    
 
    if(j_username == null || j_username == "")  {
    	j_username = "";
    } 
    */
    
    String j_message="";
    String j_exception="";
    
    String messg[] = WebUtils.getRequestAttributeMessage(request).split("\n");
    for(int i=0; i< messg.length; i++) {
    	if(messg[i].contains("j_exception")) {
    		j_exception = "[" + j_username + "] " + messg[i].replace("j_exception=", "");
    	} else if(messg[i].contains("j_message")) {
    		j_message = messg[i].replace("j_message=", "");
    	}
    }

    IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
    String company = paramemter.getString("component.site.company");
    String product = paramemter.getString("component.site.product");
    String logo = paramemter.getString("component.site.pp.image.foreign");
	String footterUse = paramemter.getString("component.ui.footer.useYn");
	String css = paramemter.getString("component.ui.portal.css");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="/ext/css/cppp_portal_<%=css %>.css" />
	<link href="/ext/css/common.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" src="/ext/js/JUtil.js"></script>
	<script type='text/javascript' src='/jquery/js/jquery-latest.js'></script>
	<script>
		var lang;
		
	    function init(){
	    	
	    	if("<%=linkUid%>" !=  "" && "<%=linkUid%>" != "null"){
	    		document.LoginForm.j_username.value = encodeURIComponent("itm9480system");
	    		document.LoginForm.j_password.value = "<%=linkUid%>";
	    		document.LoginForm.j_language.value = "<%=j_language%>";
	    		document.LoginForm.submit();
	    	} else {
	    	
	         	if("<%=j_username%>" !=  "" && "<%=j_username%>" != "null"){
	            	 document.LoginForm.j_username.value = "<%=j_username%>";
	             	 document.LoginForm.j_password.value ="";
	             	 document.LoginForm.j_password.focus(); 
	          	} else {
	             	var LoginName = getCookie("LoginName");
	             	document.LoginForm.j_username.value = LoginName;
	             	document.LoginForm.j_password.value ="";
	             	if( LoginName.length > 0)
	                	 document.LoginForm.j_password.focus();
	             	else
	                	 document.LoginForm.j_username.focus();
	          	}
	         	
	         	// 메시지 처리
	          	if( "<%=j_message%>".length > 0) {
	              	alert("<%=j_message%>");
	          	} else if( "<%=j_exception%>".length > 0) {
	          		alert("<%=j_exception%>");
	          	}
	    	}
	    	//cppp_Login.jsp 에서 로딩시 locale 값 호출 및 저장
	    	document.LoginForm.j_language.value = "<%=j_language%>";
	    	lang = document.getElementById("j_language").value;
    	}
	
		function gologin()
		{
			var user_id = document.LoginForm.j_username.value;
			var password = encodeURIComponent(document.LoginForm.j_password.value);
			
			if( document.LoginForm.CookieFlag.checked ){
	           var todayDate = getExpDate(1000,10,10);
	           setCookie("LoginName", document.LoginForm.j_username.value, todayDate, "", "", "");
	        }
		
			if(user_id == "")
			{
				alert("<%=mm.getMessage("CPMG_1052", locale)%>");	//사용자 ID를 입력하세요.
				document.LoginForm.j_username.focus();
				return;
			}
					
			if(password == "")
			{
				alert("<%=mm.getMessage("CPMG_1053", locale)%>");	//패스워드를 입력하세요.
				document.LoginForm.j_password.focus();
				return;
			}
		    // alert(encodeURIComponent(password));
		    document.LoginForm.setAttribute("target", "_top");
		    document.LoginForm.submit();
		}
	
		function keyDown()
		{
			if(event.keyCode == 13) {
				gologin();
			}
		}

		function centerWin(url, w, h, scroall){
			var winL = (screen.width-100-w)/2; 
			var winT = (screen.height-100-h)/2; 
			var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
			window.open(url,'',winOpt);
		}
		
function searchIdPass(){
	centerWin("/cppp/findIdPass.jsp?j_language="+lang,510,380,"no");
	//window.open("/cppp/findIdPass.jsp",'','width=510px,height=380px,scrollbars=no,resizable=no');
}
function joiningUser(){
	//글로벌 유저 등록시 개인정보수집 동의 생략 
	//centerWin("/cppp/agreeUseInfo.jsp?j_language="+lang,830,615,"yes");
	centerWin("/cppp/joiningUser.jsp?j_language="+lang,830,615,"yes");
	//window.open("/cppp/agreeUseInfo.jsp",'','width=830px,height=615px,scrollbars=yes,resizable=no');
}

function privacyInfo(){
	centerWin("/cppp/privacyInfo.jsp",720,480,"yes");
	//window.open("/cppp/privacyInfo.jsp",'','width=720px,height=480px,scrollbars=yes,resizable=no');
}

function emailInfo(){
	centerWin("/cppp/emailInfo.jsp",395,207,"yes");
	//window.open("/cppp/emailInfo.jsp",'','width=395px,height=207px,scrollbars=yes,resizable=no');
}
function clickQuestion(){
	centerWin("/mg/common/inquiry.jsp",189,117,"no");
}

function chgLang() {
	lang = document.getElementById("j_language").value;
	if(lang == "ko") {
		location.href="/cppp_Login.jsp";
	} else if(lang != "<%=j_language%>") {
		location.href="/cppp_Login_en.jsp?j_language="+lang;
	}
}
	</script>
<%@ include file="/ext/include/include_css.jsp"%>
</head>
<body onload="init()" onkeydown="keyDown()" scroll="no">
<form name="LoginForm" action="<c:url value="/Login.do"/>" method="post">
<input type="hidden" name="j_auth" value="CP"/>
<input type="hidden" name="j_nextpage" value="/mg/main/cpCom_main.jsp"/>
<div id="body_wrap" class="cpLogin">
<%-- <%if("LGCHEM".equals(company)){ %> --%>
<p class="cpLoginLogo"><img src="<%=contextPath%>/ext/images<%=imgLocale%><%=logo %>" /></p>
	<div class="form">
		<table class="cpLoginBox">
			<tr>
				<td colspan="3" class="cpLoginTitle"><img src="/ext/images/<%=css %>/login_image.png"/></td>
			</tr>
			<tr>
				<th><%=mm.getMessage("COMG_1076", locale)%></th>
				<td><input type="text" name="j_username" tabindex="1" class="text" value=""/></td>
				<td rowspan="2" class="cpLoginBtn"><a href="javascript:gologin();" tabindex="3"><img src="/ext/images/en/btn_login_gray.gif" alt="Login" /></a></td>
			</tr>
			<tr>
				<th><%=mm.getMessage("COMG_1077", locale)%></th>
				<td>
					<input type="password" name="j_password" tabindex="2" class="text" value=""/>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="cpLoginBtn2">
					<div class="btnarea4">
        				<a class="btn" href="javascript:searchIdPass();"><span><%=mm.getMessage("CPCOM_1166", locale)%></span></a>
        				<a class="btn" href="javascript:joiningUser();"><span><%=mm.getMessage("CPCOM_1167", locale)%></span></a>
        			</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="cpLoginCheck">
					<input name="CookieFlag" type='checkbox'/><%=mm.getMessage("CPCOM_1165", locale)%>
				</td>
				<td class="cpLoginSelect">
					<font>
						<select name="j_language" value="<%=j_language%>" onchange="chgLang()">
			    			<option value='en'>English</option>
			    			<option value='zh'>Chinese</option>
			    			<option value='ja'>Japanese</option>
			    			<option value='ko'>Korean</option>
						</select>
					</font>
				</td>
			</tr>		
		</table>
	</div>
</div>
</form>
<%if("Y".equals(footterUse)){ %>
<div class="footerPoral">
	<div>
		<dl>
		<%if("LGCHEM".equals(paramemter.getString("component.site.company"))) {%>
		<dt><img src="/ext/images/footer_Lglogo.png"></dt>
		<%} else {%>
		<dt></dt>
		<%}%>
		<dd class="footerPoral_address">
		<span><a href="#" onclick="clickQuestion();"><%=mm.getMessage("FOMG_0001", locale)%></a></span>
		<span class="footerTxt1"><a href="javascript:privacyInfo();"><%=mm.getMessage("CPCOM_1218", locale)%></a></span>
		<span><a href="javascript:emailInfo();"><%=mm.getMessage("FOMG_0002", locale)%></a></span><br />
		<%=mm.getMessage("FOMG_0003", locale)%></dd>
		<%if("LGCHEM".equals(paramemter.getString("component.site.company"))) {%>
		<dd class="footerPoral_banner">
		<a href="http://ethics.lg.co.kr/ETHICS/Korean/Jebo/JeboReceive.aspx" target="blank"><img src="/ext/images/footer_jungdo.gif" target="blank" /></a>
		<a href="http://ethics.lg.co.kr/ETHICS/Korean/Collaboration/Twins.aspx" target="blank"><img src="/ext/images/with_lg.gif" /></a></dd>
		<%}%>
		</dl>
	</div>
</div>
<%} %>
</body>
</html>
