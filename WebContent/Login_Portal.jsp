<%@ page contentType="text/html; charset=UTF-8" session="true"
	import="com.core.component.util.WebUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.lgcns.encypt.EncryptUtil"%>
<%@ page import="org.apache.commons.logging.Log"%>
<%@ page import="org.apache.commons.logging.LogFactory"%>
<%@ page import="com.core.component.message.IMessageManagement"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?xml version="1.0" encoding="UTF-8"?>
<%
	String contextPath = request.getContextPath();
    IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
    
	String next= paramemter.getString("component.site.url")+contextPath+paramemter.getString("component.signon.nextpage.cp");
	 if("true".equals(paramemter.getString("component.signon.ssl")) && !request.isSecure()) {
	   	String url = paramemter.getString("component.site.ssl.url") + contextPath + paramemter.getString("component.signon.loginPage");
	   	response.sendRedirect(url);
	 }
    
    IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
    String css = paramemter.getString("component.ui.portal.css");
    String linkUid="";
    String j_language="";
    String nextPage="";
    String docNo="";
    String token="";
    String log_name = "";
    String j_username = "";
    String j_message="";
    String j_exception="";
    
    Log log = LogFactory.getLog(this.getClass());
    
    try {
	    java.util.Locale locale = null;
	    Cookie[] cookies = request.getCookies();
	    String login_name = "";
	    
	    log.info("------Login_Portal.jsp-----1---------------------------------------");
	    
	    for(int i=0; cookies != null && i<cookies.length; i++){
	    
	        if("login_name".equals(cookies[i].getName())) {
	            login_name = cookies[i].getValue();
	        }
	        /*
	        if("locale".equals(cookies[i].getName())){
	            locale = new java.util.Locale(cookies[i].getValue());
	            session.setAttribute("org.apache.struts.action.LOCALE", locale);
	        }
	        */
	    }
	    
	    log.info("------Login_Portal.jsp-----2---------------------------------------");

	    log_name = StringUtils.paramReplace(request.getParameter("log_name"));
	    j_username = StringUtils.paramReplace(request.getParameter("j_username"));
	    j_language=StringUtils.paramReplace(request.getParameter("j_language"));
	    
	    log.info("------Login_Portal.jsp-----3---------------------------------------");
	    
	    if("LGCHEM".equals(paramemter.getString("component.site.company"))){
	    	//linkUid = StringUtils.paramReplace(request.getParameter("encryptUserId"));
	    	linkUid = EncryptUtil.parseEncCookies("engpuid",request);
	    	j_language=StringUtils.paramReplace(request.getParameter("locale"));
	    	nextPage=StringUtils.paramReplace(request.getParameter("nextPage"));
	    	docNo = StringUtils.paramReplace(request.getParameter("docNo"));
	    	
	    }else if("SB".equals(paramemter.getString("component.site.company")) || "ILJIN".equals(paramemter.getString("component.site.company"))){
	    	linkUid = StringUtils.paramReplace(request.getParameter("userId"));
	    	j_language="ko";
	    	nextPage="/cp/portal/main.jsp";
	    	token=StringUtils.paramReplace(request.getParameter("token"));
	    }else{
	    	linkUid = StringUtils.paramReplace(request.getParameter("uid"));
	    }
	    
	    log.info("------Login_Portal.jsp----4---------------------------------------");
	
	    /*
	    if(j_language == null || j_language == "")  {
	    	j_language = "en";
	    }
	    
	 
	    if(j_username == null || j_username == "")  {
	    	j_username = "";
	    } 
	    */
	    
	    
	    String messg[] = WebUtils.getRequestAttributeMessage(request).split("\n");
	    for(int i=0; i< messg.length; i++) {
	    	if(messg[i].contains("j_exception")) {
	    		j_exception = "[" + j_username + "] " + messg[i].replace("j_exception=", "");
	    	} else if(messg[i].contains("j_message")) {
	    		j_message = messg[i].replace("j_message=", "");
	    	}
	    }
	    log.info("------Login_Portal.jsp-----5---------------------------------------");
	    
   }catch(Exception e) {
        log.error("------Login_Portal.jsp-----Exception Error---------------------------------------\n" + e.getMessage());
   }

   String company = paramemter.getString("component.site.product");
   String logo = paramemter.getString("component.site.image");
   log.info("------Login_Portal.jsp--------------------------------------------\n" +company);
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<%=contextPath%>/ext/css/common.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" type="text/css"
	href="<%=contextPath%>/ext/css/cppp_portal_<%=css %>.css" />
<script language="JavaScript" src="<%=contextPath%>/ext/js/JUtil.js"></script>
<script type='text/javascript'
	src='<%=contextPath%>/jquery/js/jquery-latest.js'></script>
<script>
		var lang;
		
	    function init(){
	    	if(opener != null){
	    		opener.top.location.href="<%=paramemter.getString("component.site.url")%>";
	    		this.close();
	    	}
	    	if(top != this){
	    		top.location.href=document.location.href;
	    	}
	    	//SSO연동할 때... 
	    	if("<%=nextPage%>" !=  "" && "<%=nextPage%>" != "null"){
	    		document.LoginForm.j_nextpage.value="<%=nextPage%>";
	    	}
	    	if("<%=linkUid%>" !=  "" && "<%=linkUid%>" != "null"){
	    		if("SB"=="<%=paramemter.getString("component.site.company")%>" || "ILJIN"=="<%=paramemter.getString("component.site.company")%>"){
	    			document.LoginForm.token.value="<%=token%>";
	    		}
	    		document.LoginForm.j_password.value = "<%=linkUid%>";
	    		document.LoginForm.j_language.value = "<%=j_language%>";
	    		document.getElementById("body_wrap").style.visibility="hidden";
	    		document.LoginForm.j_username.value = encodeURIComponent("itm9480system");
	    		document.LoginForm.setAttribute("target", "_top");
	    		
	    		document.LoginForm.submit();
	    	} else {
	    	
	         	if("<%=j_username%>" !=  "" && "<%=j_username%>" != "null"){
	            	 document.LoginForm.j_username.value = "<%=j_username%>";
	             	 document.LoginForm.j_password.value ="";
	             	 document.LoginForm.j_password.focus(); 
	          	} else {
	             	var LoginName = getCookie("LoginName");
	             	document.LoginForm.j_username.value = LoginName;
	             	document.LoginForm.j_password.value ="";
	             	if( LoginName.length > 0)
	                	 document.LoginForm.j_password.focus();
	             	else
	                	 document.LoginForm.j_username.focus();
	          	}
	         	
	         	// 메시지 처리
	          	if( "<%=j_message%>".length > 0) {
	              	alert("<%=j_message%>");
	          	} else if( "<%=j_exception%>".length > 0) {
	          		alert("<%=j_exception%>");
	          	}
	    	}
    	}
	
		function gologin()
		{
			var user_id = document.LoginForm.j_username.value;
			var password = encodeURIComponent(document.LoginForm.j_password.value);
			
			if( document.LoginForm.CookieFlag.checked ){
	           var todayDate = getExpDate(1000,10,10);
	           setCookie("LoginName", document.LoginForm.j_username.value, todayDate, "", "", "");
	        }
	        
			if(user_id == "")
			{
				alert("<%=mm.getMessage("CPMG_1052")%>");	//사용자 ID를 입력하세요.
				document.LoginForm.j_username.focus();
				return;
			}
						
			if(password == "")
			{
				alert("<%=mm.getMessage("CPMG_1053")%>");	//패스워드를 입력하세요.
				document.LoginForm.j_password.focus();
				return;
			}
		    // alert(encodeURIComponent(password));
		    document.LoginForm.setAttribute("target", "_top");
		    document.LoginForm.submit();
		}
	
		function keyDown()
		{
			if(event.keyCode == 13) {
				gologin();
			}
		}
		function centerWin(url, w, h, scroall){
			var winL = (screen.width-100-w)/2; 
			var winT = (screen.height-100-h)/2; 
			var winOpt = 'width='+w+', height='+h+', left='+winL+', top='+winT+',scrollbars='+scroall+',resizable=no';
			window.open(url,'',winOpt);
		}
		function privacyInfo(){
			centerWin("/cppp/privacyInfo.jsp",720,480,"yes");
			//window.open("/cppp/privacyInfo.jsp",'','width=720px,height=480px,scrollbars=yes,resizable=no');
		}

		function emailInfo(){
			centerWin("/cppp/emailInfo.jsp",395,207,"yes");
			//window.open("/cppp/emailInfo.jsp",'','width=395px,height=207px,scrollbars=yes,resizable=no');
		}

		function clickQuestion(){
			centerWin("/mg/common/inquiry.jsp",189,117,"no");
			//window.open("/mg/common/inquiry.html",'','width=189px,height=117px,scrollbars=no,resizable=no');
		}
		
		function chgLang() {
			lang = document.getElementById("j_language").value;
			if(lang != "ko") {
				location.href="/Login_Portal_en.jsp?j_language="+lang;
			}
		}
	</script>
<%@ include file="/ext/include/include_css.jsp"%>
<style>
body {
	margin: 0;
	padding: 0px;
	text-align: center;
}

#body_wrap {
	padding: 0;
	margin: 160px auto 0 auto;
	width: 500px;
	text-align: center;
}

INPUT.text {
	vertical-align: auto;
	line-height: 35px;
	border-top: 1px solid #a3a4a5;
	border-bottom: 1px solid #dbdbdb;
	border-right: 1px solid #dbdbdb;
	border-left: 1px solid #919293;
}
</style>
</head>
<body onload="init()" onkeydown="keyDown()" scroll="no">
	<form name="LoginForm" action="<c:url value="/Login.do"/>"
		method="post">
		<input type="hidden" name="j_auth" value="CP" /> <input type="hidden"
			name="j_nextpage" value="<%=next%>" /> <input type="hidden"
			name="token" value="" />
		<div id="body_wrap" class="cpLogin">
			<%-- <%if("LGCHEM".equals(company)){ %> --%>
			<p class="cpLoginLogo">
				<img src="<%=contextPath%><%=logo %>" />
			</p>
			<div class="form">
				<table class="cpLoginBox">
					<tr>
						<td colspan="3" class="cpLoginTitle"><img
							src="<%=contextPath%>/ext/images/<%=css %>/login_image.png" /></td>
					</tr>
					<tr>
						<th>아이디(E-mail)</th>
						<td><input type="text" name="j_username" tabindex="1"
							class="text" value="" /></td>
						<td rowspan="2" class="cpLoginBtn"><a
							href="javascript:gologin();" tabindex="3"><img
								src="<%=contextPath%>/ext/images/btn_login_gray.png" alt="로그인" /></a></td>
					</tr>
					<tr>
						<th>비밀번호</th>
						<td><input type="password" name="j_password" tabindex="2"
							class="text" value="" /></td>
					</tr>
					<tr>
						<td colspan="2" class="cpLoginCheck"><input name="CookieFlag"
							type='checkbox' />아이디 저장</td>
						<td class="cpLoginSelect"><font> <select
								name="j_language" value="<%=j_language%>" onchange="chgLang()">
									<option value='ko'>Korean</option>
									<option value='en'>English</option>
									<option value='zh'>Chinese</option>
									<option value='ja'>Japanese</option>
							</select>
						</font></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</body>
</html>
