<%@ page contentType="text/html; charset=UTF-8" session="true"
	import="com.core.component.util.WebUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>
<%@ page import="com.lgcns.encypt.EncryptUtil"%>
<%@ page import="com.core.component.user.IUser"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?xml version="1.0" encoding="UTF-8"?>
<%
	
	IUser s_user = (IUser) session.getAttribute("j_user");
	IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	
    java.util.Locale locale = null;
    Cookie[] cookies = request.getCookies();
    String login_name = "";
     
    for(int i=0; cookies != null && i<cookies.length; i++){
    
        if("login_name".equals(cookies[i].getName())) {
            login_name = cookies[i].getValue();
        }
    }
    String linkUid="";
    String j_language;
    String nextPage="";
    String token="";
    
    String log_name = StringUtils.paramReplace(request.getParameter("log_name"));
    String j_username = StringUtils.paramReplace(request.getParameter("j_username"));
    j_language=StringUtils.paramReplace(request.getParameter("j_language"));
    
    if("LGCHEM".equals(paramemter.getString("component.site.company"))){
    	linkUid = EncryptUtil.parseEncCookies("engpuid",request);
    	j_language=StringUtils.paramReplace(request.getParameter("locale"));
    	nextPage = "/cp/portal/contents/reqExportDocInfo.jsp?callFrom=list&mail=1&gridid=gridObj&formid=CPP_0101&DOC_NO="+StringUtils.paramReplace(request.getParameter("docNo"));
    }else if("SB".equals(paramemter.getString("component.site.company"))){
    	linkUid = StringUtils.paramReplace(request.getParameter("userId"));
    	j_language=StringUtils.paramReplace(request.getParameter("locale"));
    	token=StringUtils.paramReplace(request.getParameter("token"));
    	nextPage="/cp/portal/main.jsp";
    }else{
    	linkUid = StringUtils.paramReplace(request.getParameter("uid"));
    }
    
    String j_message="";
    String j_exception="";
    
    String messg[] = WebUtils.getRequestAttributeMessage(request).split("\n");
    for(int i=0; i< messg.length; i++) {
    	if(messg[i].contains("j_exception")) {
    		j_exception = "[" + j_username + "] " + messg[i].replace("j_exception=", "");
    	} else if(messg[i].contains("j_message")) {
    		j_message = messg[i].replace("j_message=", "");
    	}
    }

%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/ext/css/common.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="/ext/js/JUtil.js"></script>
<script type='text/javascript' src='/jquery/js/jquery-latest.js'></script>
<script>
	    function init(){
	    	//SSO연동할 때... 
	    <%if( null != s_user){ %>
			location.href="<%=nextPage%>";
		<%}%>
	    	if("<%=linkUid%>" !=  "" && "<%=linkUid%>" != "null"){
	    		if("SB"=="<%=paramemter.getString("component.site.company")%>"){
	    			document.LoginForm.token.value="<%=token%>";
	    		}
	    		document.LoginForm.j_username.value = encodeURIComponent("itm9480system");
	    		document.LoginForm.j_password.value = "<%=linkUid%>";
	    		document.LoginForm.j_language.value = "ko";
	    		document.LoginForm.j_nextpage.value="<%=nextPage%>";
	    		document.LoginForm.submit();
	    } else {
	    	
	         	if("<%=j_username%>" !=  "" && "<%=j_username%>" != "null"){
	            	 document.LoginForm.j_username.value = "<%=j_username%>";
	             	 document.LoginForm.j_password.value ="";
	             	 document.LoginForm.j_password.focus(); 
	          	} else {
	             	var LoginName = getCookie("LoginName");
	             	document.LoginForm.j_username.value = LoginName;
	             	document.LoginForm.j_password.value ="";
	             	if( LoginName.length > 0)
	                	 document.LoginForm.j_password.focus();
	             	else
	                	 document.LoginForm.j_username.focus();
	          	}
	         	
	         	// 메시지 처리
	          	if( "<%=j_message%>".length > 0) {
	              	alert("<%=j_message%>");
	          	} else if( "<%=j_exception%>".length > 0) {
	          		alert("<%=j_exception%>");
	          	}
	    	}
    	}
	
		function gologin()
		{
			var user_id = document.LoginForm.j_username.value;
			var password = encodeURIComponent(document.LoginForm.j_password.value);
			
			if(user_id == "")
			{
				alert("사용자 ID를 입력하세요.");
				document.LoginForm.j_username.focus();
				return;
			}
			if(password == "")
			{
				alert("패스워드를 입력하세요.");
				document.LoginForm.j_password.focus();
				return;
			}
		    // alert(encodeURIComponent(password));
		    document.LoginForm.setAttribute("target", "_top");
		    document.LoginForm.submit();
		}
	
		function keyDown()
		{
			if(event.keyCode == 13) {
				gologin();
			}
		}
	</script>
<%@ include file="/ext/include/include_css.jsp"%>
<style>
body {
	margin: 0;
	padding: 0px;
	text-align: center;
}

#body_wrap {
	padding: 0;
	margin: 300px auto 0 auto;
	width: 500px;
	text-align: center;
}

INPUT.text {
	vertical-align: auto;
	line-height: 35px;
	border-top: 1px solid #a3a4a5;
	border-bottom: 1px solid #dbdbdb;
	border-right: 1px solid #dbdbdb;
	border-left: 1px solid #919293;
}
</style>
</head>
<body onload="init()" onkeydown="keyDown()" scroll="no">
	<form name="LoginForm" action="<c:url value="/Login.do"/>"
		method="post">
		<input type="hidden" name="j_auth" value="CP" /> <input type="hidden"
			name="j_nextpage" value="/cp/portal/main.jsp" /> <input type="hidden"
			name="j_username" value="" /> <input type="hidden" name="j_password"
			value="" /> <input type="hidden" name="j_language" value="" /> <input
			type="hidden" name="token" value="" />
		<div id="body_wrap" style="height: 100%;">
			<img
				src="/dhtmlx_v403/sources/dhtmlxWindows/codebase/imgs/dhxwins_terrace/dhxwins_progress.gif"
				width="62px" height="62px" />
		</div>
	</form>
</body>
</html>