<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>

 <script type="text/javascript" src="/ext/js/ajaxlib/jslb_ajax.js"></script>

 <title></title>

 <script type='text/javascript'>
 	var progress_ing = null;
 
	  function DoEncode() 
	  {
		  if(document.form.txtInput.value == "" || document.form.txtInput.value == null) {
		   		alert("Input Value 를 먼저 입력 해 주세요 !!");
		  } else {
		  	 var url = "/mg/Encrypt.do?mod=ariaEncode&srcword="+ document.form.txtInput.value;
		  	 var tmpval = "";
			 sendRequest(doAjaxResult, tmpval,'POST', url , false, false);
		  }
	  }
	  
	  function doAjaxResult(oj) {
		    var result = oj.responseText; 
		    document.form.txtEncode.value = result;
	  }
	  
	  function DoEncodeList() 
	  {
		  if(document.form.txtInput.value == "" || document.form.txtInput.value == null) {
		   		alert("Input Value 를 먼저 입력 해 주세요 !!");
		  } else {
		  	 var url = "/mg/Encrypt.do?mod=seedKeyEncode&srcword="+ document.form.txtInput.value;
		  	 var tmpval = "";
			 sendRequest(doAjaxResult, tmpval,'POST', url , false, false);
		  }
	  }
  
 
  	function DoDecode() 
  	{
  		if(document.form.txtEncode.value == "" || document.form.txtEncode.value == null) {
	   		alert("Encode를 먼저 실행해 주세요 !!");
	   } else {
	  	  var url = "/mg/Encrypt.do?mod=ariaDecode&srcword=" + encodeURIComponent(document.form.txtEncode.value);
	  	  var tmpval = "";
		  sendRequest(doAjaxComboResult, tmpval,'POST', url , false, false);
	   }
	}

	function doAjaxComboResult(oj) {
	    var result = oj.responseText; 
	     document.form.txtDecode.value = result;
	}
	
	function DoDecodeList() 
  	{
  		if(document.form.txtEncode.value == "" || document.form.txtEncode.value == null) {
	   		alert("Encode를 먼저 실행해 주세요 !!");
	   } else {
	  	  var url = "/mg/Encrypt.do?mod=seedKeyDecode&srcword=" + encodeURIComponent(document.form.txtEncode.value);
	  	  var tmpval = "";
		  sendRequest(doAjaxComboResult, tmpval,'POST', url , false, false);
	   }
	}
	
	function DecodeTime() 
  	{
  	   var cnt = document.form.txtCount.value;
  	   
  	   /*
  	   if( cnt > 10000) {
  	   	   openDialog();
  	   }
  	   */

	   if(cnt == "" || cnt == null) {
	   		alert("반복 횟수를 입력해 주세요 !!");
	   		document.form.txtCount.focus();
	   } else {
	   	    document.getElementById("ING").style.visibility = 'visible';
	   	    document.getElementById("MSG").style.visibility = 'visible';
	   	    setTimeout(function() { 
				var url = "/mg/Encrypt.do?mod=seedDecodeCnt&cnt=" + document.form.txtCount.value + "&srcword=" + encodeURIComponent(document.form.txtEncode.value);
				sendRequest(doAjaxTimeResult, '','GET', url , false, false);
	   	   	},100);
	  	   
	  }
	}

	function doAjaxTimeResult(oj) {
	     var result = oj.responseText; 
	     document.getElementById("td_msg").innerHTML = result + " 초";
	     document.getElementById("ING").style.visibility = 'hidden';
	     document.getElementById("MSG").style.visibility = 'hidden';
	     document.getElementById("ING").style.refresh();
	     /*
	     if( progress_ing != null ) {
	     	progress_ing.close();
	     	progress_ing = null;
	     }
	     */
	}
	
	
	 function openDialog() {
    	   //progress_ing = window.open("/ext/include/progress_ing.htm", '', "dialogHeight=100px;dialogWidth=80px;scroll=no; status=no; help=no; center=yes");
    	
    	    var height = 30;
    	    var width = 80;
			var top = (screen.height - height)/2;
			var left = (screen.width - width)/2;

			progress_ing = window.open("/ext/include/progress_ing.htm", '', 'left='+left+', top='+top+',width='+width+',height='+height+', toolbar=no, menubar=no, status=no, scrollbars=no, location=no, resizable=no');
			// progress_ing.focus();
     }

  
 </script>
 </head>
 

 <body>
<form name="form">
 <table cellspacing="0" cellpadding="2" border="0" width="80%" align="center">
 <tbody>
  
  <tr><td colspan="2"><h3>-- Common IT Infra Encription Test (ARIA) --</h3></td></tr>
  <tr>
   <td align="left">Input Value: </td>
   <td align="left"><input type="text" width="120px" name="txtInput" id="txtInput" maxlength="16"/></td>
  </tr>
  <tr>
   <td align="left">Encode Value: </td>
   <td align="left"><input type="text" width="120px" name="txtEncode" id="txtEncode" /></td>
  </tr>
    <tr>
   <td align="left">Decode Value: </td>
   <td align="left"><input type="text" width="120px" name="txtDecode" id="txtDecode" /></td>
  </tr>
  <tr>
   <td/>
   <td align="left"><input type="button" id="btnEncode" name="btnEncode" value="Encode"  onclick="DoEncode()"/>
   				    <input type="button" id="btnDecode" name="btnDecode" value="Decode"  onclick="DoDecode()"/></td>
  </tr>
   <tr>
   <td/>
   <td align="left"><input type="button" id="encodeList" name="encodeList" value="encodeKey"  onclick="DoEncodeList()"/>
   				    <input type="button" id="decodeList" name="decodeList" value="decodeKey"  onclick="DoDecodeList()"/></td>
  </tr>
    <tr>
   <td align="left">Process Count: </td>
   <td align="left"><input type="text" width="120px" name="txtCount" id="txtCount" /></td>
  </tr>
     <td align="left">
   		<input type="button" id="btnTime" name="btnDecode" value="DecodeTime"  onclick="DecodeTime()"/></td>
   		<td colspan="2" style="color:red" id="td_msg">초</td>
  </tr>
  
  </tr>
     <td align="left"> </td>
   	 <td> <DIV ID="ING" style="visibility:hidden;"> <IMG SRC="/ext/images/ajax-loader.gif"> </img></DIV> </td>
  </tr>
  </tr>
     <td > </td>
   	 <td align="left"> <DIV ID="MSG" style="visibility:hidden;"> 처리중 입니다. 잠시만 기다려 주세요 !! </img></DIV> </td>
  </tr>
  
  
</form>

 </tbody>
 </table>
 </body>
 </html>
