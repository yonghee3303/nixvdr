(window["webpackJsonp"]=window["webpackJsonp"]||[]).push([["chunk-d1738644"],{"0591":function(e,t,n){},"70a2":function(e,t,n){"use strict";var r=n("0591"),s=n.n(r);s.a},"7fcc":function(e,t,n){"use strict";var r=function(){var e=this,t=e.$createElement,n=e._self._c||t;return n("el-dialog",{directives:[{name:"loading",rawName:"v-loading",value:e.loading,expression:"loading"},{name:"draggable",rawName:"v-draggable"}],ref:"windowRef",attrs:{id:"enrollUserDlg","close-on-click-modal":e.closeOnClickModal,title:e.title,width:e.width,"before-close":e.onClose,visible:e.dialogVisible,"show-close":e.showClose,"close-on-press-escape":!1},on:{"update:visible":function(t){e.dialogVisible=t}}},[e.isAgree?n("enroll-user-ctn",{ref:"enrollUserCtn_ref",attrs:{viewType:e.type,widgetId:"selectUserGroup",containerHeight:e.containerHeight,gridSelectable:e.gridSelectable},on:{selected:e.onSelected,loadingPage:e.loadingPage}}):n("enroll-user-agree-ctn",{ref:"EnrollUserAgreeCtn_ref"}),n("span",{staticClass:"dialog-footer",attrs:{slot:"footer"},slot:"footer"},[e.showClose?n("el-button",{on:{click:e.onClose}},[e._v(e._s(e.txtCancel))]):e._e(),e.isAgree?n("el-button",{attrs:{type:"primary"},on:{click:e.onSaveAndClose}},[e._v(e._s(e.txtEnroll))]):n("el-button",{attrs:{type:"primary"},on:{click:e.onAgree}},[e._v(e._s(e.txtOk))])],1)],1)},s=[],o=(n("8e6e"),n("ac6a"),n("456d"),n("bd86")),i=n("2f62"),a=n("12cb");function l(e,t){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var r=Object.getOwnPropertySymbols(e);t&&(r=r.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),n.push.apply(n,r)}return n}function c(e){for(var t=1;t<arguments.length;t++){var n=null!=arguments[t]?arguments[t]:{};t%2?l(Object(n),!0).forEach((function(t){Object(o["a"])(e,t,n[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):l(Object(n)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(n,t))}))}return e}var d={name:"EnrollUserDlg",props:{title:String,type:String,width:{default:"50%",type:String},containerHeight:String,gridSelectable:String},data:function(){return{txtCancel:a["a"].t("buttons.cancel"),txtEnroll:a["a"].t("buttons.upload"),txtInit:a["a"].t("buttons.init"),txtOk:a["a"].t("buttons.ok"),dialogVisible:!1,selectedData:null,closeOnClickModal:!1,isAgree:!1,loading:!1}},computed:c({},Object(i["d"])(["userInfo"]),{showClose:function(){return!this.userInfo||this.userInfo&&!this.userInfo.is_invite}}),components:{"enroll-user-ctn":function(){return Promise.all([n.e("chunk-26952d46"),n.e("chunk-e121d0d0")]).then(n.bind(null,"cfbc"))},"enroll-user-agree-ctn":function(){return n.e("chunk-2d226579").then(n.bind(null,"e7f7"))}},mounted:function(){this.dialogVisible=!0},methods:{onClose:function(e){this.$close(null)},onSelected:function(e){this.selectedData=e},loadingPage:function(e){this.loading=e},onSaveAndClose:function(e){if(this.$refs.enrollUserCtn_ref.checkFields()){this.loadingPage(!0);var t=this.$refs.enrollUserCtn_ref.doSave();t&&(this.$close({item:{onClick:"doSave"},event:e}),this.loadingPage(!1))}},onInit:function(){this.$refs.enrollUserCtn_ref.doInit()},onAgree:function(){var e=this.$refs.EnrollUserAgreeCtn_ref.isUseTerms,t=this.$refs.EnrollUserAgreeCtn_ref.isUsePrivacy;this.isAgree="1"==e&&"1"==t,this.$refs.EnrollUserAgreeCtn_ref.isShowErrorMsg=!this.isAgree}}},u=d,p=n("2877"),g=Object(p["a"])(u,r,s,!1,null,null,null);t["a"]=g.exports},"959a":function(e,t,n){"use strict";var r=function(){var e=this,t=e.$createElement,n=e._self._c||t;return n("div",[n("dialogs-wrapper",{ref:"dialogs-file",attrs:{"wrapper-name":"file-layout",name:e.wrapperName}})],1)},s=[],o={props:{wrapperName:{type:String,default:"default"}}},i=o,a=(n("70a2"),n("2877")),l=Object(a["a"])(i,r,s,!1,null,null,null);t["a"]=l.exports},9935:function(e,t,n){"use strict";n.r(t),n.d(t,"create",(function(){return f})),n.d(t,"DialogsWrapper",(function(){return p})),n.d(t,"install",(function(){return h})),n.d(t,"makeDialog",(function(){return m}));var r=n("a026");function s(){}n.d(t,"DialogComponent",(function(){return r["default"]}));var o="vue-modal-dialogs:close",i="vue-modal-dialogs:error",a={tag:String,appear:Boolean,css:Boolean,mode:String,type:String,moveClass:String,enterClass:String,leaveClass:String,enterToClass:String,leaveToClass:String,enterActiveClass:String,leaveActiveClass:String,appearClass:String,appearActiveClass:String,appearToClass:String,duration:[Number,String,Object]};function l(e,t){return 0===e.length&&t[0]&&"object"===typeof t[0]?t[0]:e.reduce((function(e,n,r){return e[n]=t[r],e}),{})}function c(e){if(null!=e){var t=typeof e;if("object"===t)return"function"===typeof e.then||"function"===typeof e.render||"string"===typeof e.template;if("function"===t)return"number"===typeof e.cid}return!1}function d(e,t){var n={},r=[t];function s(e){null!=e&&r.push(e)}while(t=r.shift()){var o=void 0;Array.isArray(t.props)?o=t.props:"object"===typeof t.props&&(o=Object.keys(t.props)),o&&o.forEach((function(e){n[e]=!0})),s(t.options),s(t.extends),Array.isArray(t.mixins)&&t.mixins.forEach(s)}return e.filter((function(e){return!n[e]}))}var u={},p={name:"DialogsWrapper",props:Object.assign({},{name:{type:String,default:"default",validator:function(e){return e}},transitionName:String},a),data:function(){return{id:0,dialogs:{}}},computed:{dialogIds:function(){return Object.keys(this.dialogs)}},created:function(){u[this.name]=this},beforeDestroy:function(){u[this.name]=void 0},render:function(e){var t=this,n=Object.assign({},this.$listeners),r=n["after-leave"]||s;n["after-leave"]=function(e){e.$afterLeave(),r(e)};var a=Object.assign({},this.$options.propsData,{name:this.transitionName}),l=this.dialogIds.map((function(n){var r=t.dialogs[n],s={};return s[o]=r.close,s[i]=r.error,e(r.component,{on:s,key:r.id,props:r.propsData})}));return e("transition-group",{on:n,props:a},l)},methods:{add:function(e,t){var n,r,s=this,o=this.id++,i=new Promise((function(e,t){n=e,r=t})).then((function(e){return s.remove(o),e})).catch((function(e){throw s.remove(o),e})),a=new Promise((function(t){e.createdCallback=t})),c=a.then((function(e){return new Promise((function(t){e.$el.$afterLeave=t}))})).then((function(){return i})),d=e.component.then((function(a){var c=Object.assign({},{arguments:t},l(e.props,t)),d=Object.freeze({id:o,propsData:c,component:a,close:n,error:r});return s.$set(s.dialogs,o,d),i}));return Object.assign(d,{close:n,error:r,transition:function(){return c},getInstance:function(){return a}})},remove:function(e){this.$delete(this.dialogs,e)}}};function g(e,t){var n;return n={props:e,createdCallback:s,component:Promise.resolve(t).then((function(t){return{extends:t.default||t,props:d(["arguments"].concat(e),t),created:function(){n.createdCallback(this)},methods:{$close:function(e){this.$emit(o,e)},$error:function(e){this.$emit(i,e)}}}}))}}function f(e){var t=[],n=arguments.length-1;while(n-- >0)t[n]=arguments[n+1];if(null==e)return null;var r="default",s=e;if(c(e.component))s=e.component,r=e.wrapper||r,t=e.props||[];else if(!c(e))return null;var o=g(t,s);return function(){var e=[],t=arguments.length;while(t--)e[t]=arguments[t];return u[r]?u[r].add(o,e):Promise.reject(new TypeError("Undefined reference to wrapper "+r))}}function h(e,t){e.component("DialogsWrapper",p)}function m(){var e=[],t=arguments.length;while(t--)e[t]=arguments[t];return f.apply(void 0,e)}},c148:function(e,t,n){"use strict";n.r(t);var r=function(){var e=this,t=e.$createElement,n=e._self._c||t;return n("div",{attrs:{id:"login-page"}},[n("div",{staticClass:"login-wrap"},[n("div",{staticClass:"login-sub_wrap"},[n("form",{on:{submit:function(t){return t.preventDefault(),e.submit(t)}}},[n("ul",[n("li",[n("select",{directives:[{name:"model",rawName:"v-model",value:e.lang,expression:"lang"}],staticClass:"login-language_select",on:{change:[function(t){var n=Array.prototype.filter.call(t.target.options,(function(e){return e.selected})).map((function(e){var t="_value"in e?e._value:e.value;return t}));e.lang=t.target.multiple?n:n[0]},e.changeLanguage]}},e._l(e.language,(function(t,r){return n("option",{key:r,domProps:{value:r}},[e._v(e._s(t))])})),0)]),n("li",[n("input",{directives:[{name:"model",rawName:"v-model",value:e.j_username,expression:"j_username"}],staticClass:"login-input_text",attrs:{type:"text",placeholder:e.$t("login.username")},domProps:{value:e.j_username},on:{input:function(t){t.target.composing||(e.j_username=t.target.value)}}})]),n("li",[n("input",{directives:[{name:"model",rawName:"v-model",value:e.j_password,expression:"j_password"}],staticClass:"login-input_text",attrs:{type:"password",placeholder:e.$t("login.password")},domProps:{value:e.j_password},on:{input:function(t){t.target.composing||(e.j_password=t.target.value)}}})]),n("li",[e.isError?n("div",{staticClass:"login-error_txt"},[e._v("\r\n              "+e._s(e.$t("login.errorMessage"))+"\r\n            ")]):e._e()]),n("li",[n("button",{staticClass:"login-submit_btn",attrs:{type:"submit"}},[e._v(e._s(e.$t("login.submit")))])])])]),n("div",{staticClass:"login-saveid_ck"},[n("el-checkbox",{model:{value:e.isCheckedSaveId,callback:function(t){e.isCheckedSaveId=t},expression:"isCheckedSaveId"}},[n("span",[e._v(e._s(e.$t("login.saveId")))])])],1),n("div",[n("table",{staticClass:"login-tb_etc",on:{click:e.loginEtcClick}},[n("tbody",[n("tr",[n("td",{attrs:{id:"findPass"}},[e._v(e._s(e.$t("login.findpassword")))]),n("td",{staticClass:"login-td_etc_contour"},[e._v("|")]),n("td",{attrs:{id:"signUp"}},[e._v(e._s(e.$t("login.joinMembership")))]),n("td",{staticClass:"login-td_etc_contour"},[e._v("|")]),n("td",{attrs:{id:"serviceCenter"}},[e._v(e._s(e.$t("login.customerService")))])])])])])])]),n("dialogs-content",{attrs:{"wrapper-name":"portalLogin"}})],1)},s=[],o=(n("a481"),n("5f87")),i=(n("fa7d"),n("7c15")),a=n("959a"),l=n("9935"),c=n("7fcc"),d=function(){var e=this,t=e.$createElement,n=e._self._c||t;return n("el-dialog",{directives:[{name:"loading",rawName:"v-loading",value:e.loading,expression:"loading"}],attrs:{title:e.title,width:e.width,"before-close":e.onClose,visible:e.dialogVisible,"close-on-click-modal":e.closeOnClickModal,"close-on-press-escape":!1},on:{"update:visible":function(t){e.dialogVisible=t}}},[n("div",{staticStyle:{"maring-top":"20px"}},[[e.isCheckedId?e.isCheckedCertStr?n("div",{staticClass:"parent-forgot_pass",staticStyle:{"margin-top":"10px","font-size":"15px"}},[n("div",{staticClass:"box-forgot_pass"},[n("el-input",{attrs:{type:"password",placeholder:e.$t("login.changePw"),maxlength:"15"},model:{value:e.passwd1,callback:function(t){e.passwd1=t},expression:"passwd1"}}),n("el-input",{staticStyle:{"margin-top":"15px"},attrs:{type:"password",placeholder:e.$t("login.changePwAgain"),maxlength:"15"},model:{value:e.passwd2,callback:function(t){e.passwd2=t},expression:"passwd2"}}),e.isPassWdError?n("label",{staticClass:"txt_alert"},[e._v("* "+e._s(e.$t("adminMenu.rule_pwcheck2")))]):e._e()],1)]):n("div",{staticClass:"parent-forgot_pass"},[n("div",[e._v(e._s(e.$t("adminMenu.rule_findpw2")))]),n("div",{staticClass:"box-forgot_pass"},[n("el-radio",{attrs:{label:"email"},model:{value:e.authMethod,callback:function(t){e.authMethod=t},expression:"authMethod"}},[e._v(e._s(e.$t("login.authByEmail")))]),n("div",{staticStyle:{"margin-top":"10px"}},[n("span",[e._v(e._s(e.$t("login.sendaccreditNumber")))]),n("el-button",{staticStyle:{"margin-left":"10px","padding-top":"3px","padding-bottom":"5px"},attrs:{size:"small"},on:{click:e.onSendCertStr}},[e._v(e._s(e.$t("buttons.certificationNumber")))]),n("el-input",{staticStyle:{"margin-top":"3px"},attrs:{type:"text",placeholder:e.$t("login.accreditNumber")},model:{value:e.certStr,callback:function(t){e.certStr=t},expression:"certStr"}}),e.isFindCertError?n("label",{staticClass:"txt_alert"},[e._v("* "+e._s(e.$t("adminMenu.rule_authnumber")))]):e._e()],1)],1)]):n("div",{staticClass:"parent-forgot_pass"},[n("div",[e._v(e._s(e.$t("adminMenu.rule_findpw")))]),n("div",{staticClass:"box-forgot_pass"},[n("el-input",{attrs:{type:"text",placeholder:e.$t("login.useremailname")},model:{value:e.findId,callback:function(t){e.findId=t},expression:"findId"}}),e.isFindIdError?n("label",{staticClass:"txt_alert"},[e._v("* "+e._s(e.$t("adminMenu.rule_email3")))]):e._e()],1)])]],2),n("span",{staticClass:"dialog-footer",attrs:{slot:"footer"},slot:"footer"},[e.isCheckedId?e.isCheckedCertStr?n("el-button",{attrs:{type:"primary"},on:{click:e.onPassUpdate}},[e._v(e._s(e.txtUpload))]):n("el-button",{attrs:{type:"primary"},on:{click:e.onNextPasswordPart}},[e._v(e._s(e.txtNextBtn))]):n("el-button",{attrs:{type:"primary"},on:{click:e.onNextCertPart}},[e._v(e._s(e.txtNextBtn))])],1)])},u=[],p=n("12cb"),g={props:{title:String,width:String},data:function(){return{txtNextBtn:p["a"].t("buttons.next"),txtOk:p["a"].t("buttons.ok"),txtUpload:p["a"].t("buttons.upload"),dialogVisible:!1,closeOnClickModal:!1,loading:!1,isCheckedId:!1,findId:null,isFindIdError:!1,authMethod:"email",isCheckedCertStr:!1,certStr:null,recvCertStr:null,isFindCertError:!1,passwd1:null,passwd2:null,isPassWdError:!1}},methods:{onClose:function(e){this.$close(null)},onNextCertPart:function(e){var t=this;i["p"]("/vdr/forgotPass.do",jQuery.param({mod:"getUserInfo",USER_ID:this.findId}),!1).then((function(e){0==e.errcode?(t.isCheckedId=!0,t.isFindIdError=!1):(t.isCheckedId=!1,t.isFindIdError=!0)})).catch((function(e){t.isCheckedId=!1,t.isFindIdError=!0}))},onSendCertStr:function(){var e=this;this.loading=!0,i["p"]("/vdr/forgotPass.do",jQuery.param({mod:"sendCertId",USER_ID:this.findId,LANG:this.$i18n.locale}),!1).then((function(t){e.$showSuccess(e.$t("room.successMessage",{what:e.$t("prompts.sendMail")})),e.recvCertStr=t.data.certStr})).catch((function(t){e.$showError(e.$t("room.errorMessage",{what:e.$t("prompts.sendMail")}))})).finally((function(){e.loading=!1}))},onNextPasswordPart:function(){this.certStr&&""!=this.certStr.trim()&&this.certStr==this.recvCertStr?(this.isCheckedCertStr=!0,this.isFindCertError=!1):(this.isCheckedCertStr=!1,this.isFindCertError=!0)},onPassUpdate:function(){var e=this;this.passwd1&&this.passwd2&&this.passwd1==this.passwd2?i["p"]("/vdr/forgotPass.do",jQuery.param({mod:"updatePassword",USER_ID:this.findId,PASSWD:this.passwd1}),!1).then((function(t){0==t.errcode?(e.isPassWdError=!1,e.$showSuccess(e.$t("event.user.passwordchanged.short")),e.onClose()):e.isPassWdError=!0})).catch((function(t){e.isPassWdError=!0})):this.isPassWdError=!0}},mounted:function(){this.dialogVisible=!0}},f=g,h=n("2877"),m=Object(h["a"])(f,d,u,!1,null,null,null),v=m.exports,_=Object(l["create"])({component:c["a"],wrapper:"portalLogin"}),b=Object(l["create"])({component:v,wrapper:"portalLogin"}),C={name:"portal-login",data:function(){return{j_username:localStorage.getItem("j_username"),j_password:"",isError:!1,isCheckedSaveId:1==localStorage.getItem("isCheckedSaveId"),language:this.$Constants.language,lang:this.$i18n.locale}},created:function(){this.$store.commit("setRoomId","")},mounted:function(){window.resizeLogin()},methods:{submit:function(e){var t=this;o["a"].login(this.j_username,this.j_password).then((function(e){i["p"]("/vdr/room.do",jQuery.param({mod:"selectRoom",grid_col_id:"ROOM_ID",Preview:"true"}),!1).then((function(e){0==e.errcode?(t.isError=!1,localStorage.setItem("j_username",t.isCheckedSaveId?t.j_username:""),1<e.data.length?t.$router.replace({path:"/dashboard/room"}):(1==e.data.length&&t.$store.commit("setRoomId",e.data[0].ROOM_ID),t.$router.replace({path:"/dashboard/room"}))):(t.isError=!0,localStorage.setItem("j_username",""))})).catch((function(e){t.isError=!0,localStorage.setItem("j_username","")}))})).catch((function(e){t.isError=!0,localStorage.setItem("j_username","")}))},loginEtcClick:function(e){if(e.target.id)if("signUp"==e.target.id)_({name:"enrollUserDlg",type:"title",title:this.$t("login.joinMembership"),message:this.$t("messages.saveMessage"),width:"35%"});else if("findPass"==e.target.id)b({name:"forgotPassUserDlg",type:"title",title:this.$t("login.findpassword"),message:this.$t("messages.saveMessage"),width:"35%"})},changeLanguage:function(e){p["a"].locale=e.target.value,sessionStorage.setItem("language",p["a"].locale)}},watch:{isCheckedSaveId:function(e){localStorage.setItem("isCheckedSaveId",e?1:0)}},components:{DialogsContent:a["a"]}},w=C,S=Object(h["a"])(w,r,s,!1,null,null,null);t["default"]=S.exports}}]);
//# sourceMappingURL=chunk-d1738644.2c9cd192.js.map