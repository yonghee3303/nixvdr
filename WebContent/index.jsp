<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.user.IUser"%>

<%@ include file="ext/include/ui_common.jsp"%>
<%!
    private String getDomain(HttpServletRequest request) {
        StringBuffer sbDomain = new StringBuffer();
        sbDomain.append(request.getScheme()).append("://").append(
                request.getServerName()).append(":").append(
                request.getServerPort()).append(request.getContextPath());
        return sbDomain.toString();
    };
%>

<% 
    IUser user = (IUser) session.getAttribute("j_user");
 	String userNm = user.getName();
 	String userRoll = "A"; 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="ext/css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/ext/js/ui_scripts.js"></script>
<script>

function logout() {
    if(confirm("로그아웃 하시겠습니까?")) {
        top.location.href = "logout_process.jsp";
    }
}

function go(type) {
    document.location.href = "./mg/main/menu_process_mgt_sub.jsp?type=" + type;
}

function change_tab(obj,name1,name2) {
    document.getElementById("tab1_1").className = "off";
    document.getElementById("tab1_2").className = "off";
    document.getElementById(name1).className = "on";

    document.all["tab1_1_contents"].style.display = "none";
    document.all["tab1_2_contents"].style.display = "none";
    document.all[name2].style.display = "";
}

function change_tab2(obj,name1,name2) {
    document.getElementById("tab2_1").className = "off";
    document.getElementById("tab2_2").className = "off";
    document.getElementById("tab2_3").className = "off";
    document.getElementById(name1).className = "on";

    document.all["tab2_1_contents"].style.display = "none";
    document.all["tab2_2_contents"].style.display = "none";
    document.all["tab2_3_contents"].style.display = "none";
    document.all[name2].style.display = "";
}
function goNotice() {
    var width = 950;
    var height = 470;
	dim = ToCenter(height, width);
	var top = dim[0];
	var left = dim[1];
    var windowW = width;
    var windowH = height;
    var toolbar = 'no';
    var menubar = 'no';
    var status = 'yes';
    var scrollbars = 'no';
    var resizable = 'no';
    var library = window.open( '/info/bulletin_list_new_pop.jsp', 'summaryBoardList', 'left='+left+', top='+top+', width='+width+', height='+height+', toolbar='+toolbar+', menubar='+menubar+', status='+status+', scrollbars='+scrollbars+', resizable='+resizable);
}	
</script>
</head>
<body bgcolor="#f8f9f9">
<!-- 상단 메뉴 -->
<table width="100%" border="0" rules=groups>
<colgroup>
    <col width="220px" />
    <col width="" />
</colgroup>
<tr>
    <td rowspan="2" class="logo_area"><a href="#"><img src="./ext/images/logo_serviceace.gif" alt="serviceAce" /></a></td>
    <td>
        <div class="gnb_userdata">
            <strong><%=userNm %></strong> 님 안녕하세요!
            <a href="javascript:logout();" class="btn"><img src="./ext/images/btn_logout1.gif" alt="로그아웃" /></a>
        </div>
    </td>
</tr>
<tr>
    <td class="gnb_area">
        <ul class="main_menu">
            <li><a href="javascript:go(0);"><img src="./ext/images/gnb_menu_1.gif" alt="입출고관리"/></a></li>
            <li><a href="javascript:go(1);"><img src="./ext/images/gnb_menu_2.gif" alt="재고현황관" /></a></li>
            <li><a href="javascript:go(2);"><img src="./ext/images/gnb_menu_3.gif" alt="집계마감관리" /></a></li>
            <li><a href="javascript:go(3);"><img src="./ext/images/gnb_menu_4.gif" alt="마스터관리" /></a></li>
<%--             <%if( userRoll.equals("A")) {%> --%>
            <li class="last"><a href="javascript:go(4);"><img src="./ext/images/gnb_menu_5.gif" alt="시스템관" /></a></li>
<%--             <%}%> --%>
        </ul>
    </td>
    <td>
    </td>
</tr>
</table>
<!-- //상단 메뉴 -->

<table width="980" border="0" class="mgt-55" align="center">
<colgroup>
    <col width="460px" />
    <col width="520" />
</colgroup>
<tr>
    <td valign="top"><img src="./ext/images/main_visual.jpg" alt="e-ACCOUNT SYSTEM" /></td>
    <td valign="top">
        <table width="100%" border="0">
        <colgroup>
            <col width="255px" />
            <col width="10px" />
            <col width="255px" />
        </colgroup>
        <tr>
            <td>
                <p><a href="javascript:go(1);"><img src="./ext/images/main_img_01.gif" alt="SAP전표" /></a></p>
                <p class="mgt-20"><a href="javascript:go(3);"><img src="./ext/images/main_img_02.gif" alt="보고서" /></a></p>
            </td>
            <td>&nbsp;</td>
            <td valign="top">
				<form name="form">
				<input type="hidden" name="mileStoneName" 	value="" />
				<input type="hidden" name="leftMenuName"  	value="" />
				<input type="hidden" name="action_type"  	value="" />
                <div class="news_list">
                    <div class="intro_news">
                        <h3><img src="./ext/images/main_img_03.gif" alt="공지사항" /></h3>
                        <p class="more"><a href="javascript:goNotice();"><img src="./ext/images/btn_more.gif" alt="공지사항더보기"></a></p>
                    </div>
                    <div class="notice_list">
                    </div>
                </div>
                </form>
            </td>
        </tr>
        </table>

        <table border="0" class="mgt-25">
        <colgroup>
            <col width="255px" />
            <col width="10px" />
            <col width="255px" />
        </colgroup>
        <tr>
            <td valign="top">
                <div class="news_list">
                    <div class="intro_news">
                        <h3><img src="./ext/images/main_img_04.gif" alt="법인카드관리" /></h3>
                        <p class="more"><a href="javascript:go(0);"><img src="./ext/images/btn_more.gif" alt="법인카드관리 이동"></a></p>
                    </div>
                    <div class="intro_service mgt-10">
                        <table cellpadding="0" cellspacing="0" border="0" class="tab_service">
                        <colgroup>
                            <col width="84px" />
                            <col width="82px" />
                            <col width="83px" />
                        </colgroup>
                        <tr>
                            <td id="tab1_1" class="on"><a href="javascript:change_tab(this,'tab1_1','tab1_1_contents')">미처리(<span class="point">1</span>)</a></td>
                            <td id="tab1_2" class="off"><a href="javascript:change_tab(this,'tab1_2','tab1_2_contents')">미상신(<span class="point">2</span>)</a></td>
                            <td class="none">&nbsp;</td>
                        </tr>
                        </table>

                        <div class="service_cont"  id=tab1_1_contents >

                        </div>

                        <div class="service_cont" id=tab1_2_contents  style="display:none;" >

                        </div>
                    </div>
                </div>
            </td>
            <td>&nbsp;</td>
            <td valign="top">
                <div class="news_list">
                    <div class="intro_news">
                        <h3><img src="./ext/images/main_img_05.gif" alt="결재정보" /></h3>
                        <p class="more"><a href="javascript:go(2);"><img src="./ext/images/btn_more.gif" alt="결재정보 이동"></a></p>
                    </div>

                    <div class="intro_service mgt-10">
                        <table cellpadding="0" cellspacing="0" border="0" class="tab_service">
                        <colgroup>
                            <col width="84px" />
                            <col width="82px" />
                            <col width="83px" />
                        </colgroup>
                        <tr>
                            <td id="tab2_1" class="on"><a href="javascript:change_tab2(this,'tab2_1','tab2_1_contents')">미결함(<span class="point">1</span>)</a></td>
                            <td id="tab2_2" class="off"><a href="javascript:change_tab2(this,'tab2_2','tab2_2_contents')">진행중(<span class="point">2</span>)</a></td>
                            <td id="tab2_3" class="off"><a href="javascript:change_tab2(this,'tab2_3','tab2_3_contents')">결재반려(<span class="point">3</span>)</a></td>
                        </tr>
                        </table>

                        <div id="tab2_1_contents" class="service_cont">
                        <table width="100%" border="0" class="board_style1">
                        <colgroup>
                            <col width="80%" />
                            <col width="20%" />
                        </colgroup>

                        </table>
                        </div>

                        <div id="tab2_2_contents" class="service_cont" style="display:none;">
                        <table width="100%" border="0" class="board_style1">
                        <colgroup>
                            <col width="80%" />
                            <col width="20%" />
                        </colgroup>

                        </table>
                        </div>

                        <div id="tab2_3_contents" class="service_cont" style="display:none;">
                        <table width="100%" border="0" class="board_style1">
                        <colgroup>
                            <col width="80%" />
                            <col width="20%" />
                        </colgroup>

                        </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        </table>
    </td>
</tr>
</table>

</body>
</html>