<%@ page contentType = "text/html; charset=UTF-8" session="true"%>
<%@ page import="com.core.component.message.IMessageManagement"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%> 
<%
	IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
	String css = paramemter.getString("component.ui.portal.css");
	String epURL = paramemter.getString("component.site.ep.url");
	IMessageManagement mm = (IMessageManagement) ComponentRegistry.lookup(IMessageManagement.class);
%>


<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>JEUS 500</title>

</head>

<body style="text-align: center;">
<div style="height: 296px; width: 403px; padding: 20px;margin-top:165px;">
	<img src="/ext/images/<%=css%>/errorLogin.gif"/>
	<div style="width: 300px; height: 60px; padding: 10px; border: 3px solid #C8C8C8; font-size: 12px;">
		<%=mm.getMessage("COMG_1070")%>
	</div>
</div>
</body>
</html>


