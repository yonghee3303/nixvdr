<%@ page contentType="text/html; charset=UTF-8" session="true"
	import="com.core.component.util.WebUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page import="com.core.base.ComponentRegistry"%>
<%@ page import="com.core.component.parameter.IParameterManagement"%>
<%@ page import="com.core.component.util.StringUtils"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?xml version="1.0" encoding="UTF-8"?>
<%
    java.util.Locale locale = null;
    Cookie[] cookies = request.getCookies();
    String login_name = "";
     
    for(int i=0; cookies != null && i<cookies.length; i++){
    
        if("login_name".equals(cookies[i].getName())) {
            login_name = cookies[i].getValue();
        }
        /*
        if("locale".equals(cookies[i].getName())){
            locale = new java.util.Locale(cookies[i].getValue());
            session.setAttribute("org.apache.struts.action.LOCALE", locale);
        }
        */
    }
    
    String linkUid = StringUtils.paramReplace(request.getParameter("uid"));
    String log_name = StringUtils.paramReplace(request.getParameter("log_name"));
    String j_username = StringUtils.paramReplace(request.getParameter("j_username"));
    String j_language =StringUtils.paramReplace(request.getParameter("j_language"));
    
    /*
    if(j_language == null || j_language == "")  {
    	j_language = "en";
    }
    
 
    if(j_username == null || j_username == "")  {
    	j_username = "";
    } 
    */
    
    String j_message="";
    String j_exception="";
    
    String messg[] = WebUtils.getRequestAttributeMessage(request).split("\n");
    for(int i=0; i< messg.length; i++) {
    	if(messg[i].contains("j_exception")) {
    		j_exception = "[" + j_username + "] " + messg[i].replace("j_exception=", "");
    	} else if(messg[i].contains("j_message")) {
    		j_message = messg[i].replace("j_message=", "");
    	}
    }

    IParameterManagement paramemter = (IParameterManagement) ComponentRegistry.lookup(IParameterManagement.class);
    String company = paramemter.getString("component.site.product");
    String logo_css = ("CP".equals(company)) ? "Cploginbox" : "Rploginbox";

%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/ext/css/common.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="/ext/js/JUtil.js"></script>
<script type='text/javascript' src='/jquery/js/jquery-latest.js'></script>
<script>
	    function init(){
	    	
	    	alert("Check Session");
	    	
	    	var parentElem = window.parent.document.getElementById("mainFrame");
            if (!parentElem) {
                parentElem = elem.parentNode;
            }
            alert ("The id of the parent element: " + parentElem.name);
	    	
	    	if("<%=linkUid%>" !=  "" && "<%=linkUid%>" != "null"){
	    		document.LoginForm.j_username.value = encodeURIComponent("itm9480system");
	    		document.LoginForm.j_password.value = "<%=linkUid%>";
	    		document.LoginForm.j_language.value = "<%=j_language%>";
	    		document.LoginForm.submit();
	    	} else {
	    	
	         	if("<%=j_username%>" !=  "" && "<%=j_username%>" != "null"){
	            	 document.LoginForm.j_username.value = "<%=j_username%>";
	             	 document.LoginForm.j_password.value ="";
	             	 document.LoginForm.j_password.focus(); 
	          	} else {
	             	var LoginName = getCookie("LoginName");
	             	document.LoginForm.j_username.value = LoginName;
	             	document.LoginForm.j_password.value ="";
	             	if( LoginName.length > 0)
	                	 document.LoginForm.j_password.focus();
	             	else
	                	 document.LoginForm.j_username.focus();
	          	}
	         	
	         	// 메시지 처리
	          	if( "<%=j_message%>".length > 0) {
	              	alert("<%=j_message%>");
	          	} else if( "<%=j_exception%>".length > 0) {
	          		alert("<%=j_exception%>");
	          	}
	    	}
    	}
	
		function gologin()
		{
			var user_id = document.LoginForm.j_username.value;
			var password = encodeURIComponent(document.LoginForm.j_password.value);
			
			if( document.LoginForm.CookieFlag.checked ){
	           var todayDate = getExpDate(1000,10,10);
	           setCookie("LoginName", document.LoginForm.j_username.value, todayDate, "", "", "");
	        }
	        
			if(user_id == "")
			{
				alert("사용자 ID를 입력하세요.");
				document.LoginForm.j_username.focus();
				return;
			}
						
			if(password == "")
			{
				alert("패스워드를 입력하세요.");
				document.LoginForm.j_password.focus();
				return;
			}
		    // alert(encodeURIComponent(password));
		    
		    document.LoginForm.submit();
		}
	
		function keyDown()
		{
			if(event.keyCode == 13) {
				gologin();
			}
		}
		

	</script>
</head>
<body onload="init()" onkeydown="keyDown()">
	<form name="LoginForm" action="<c:url value="/Login.do"/>"
		method="post">
		<input type="hidden" name="j_auth" value="CP" />
		<!-- <input type="hidden" name="j_nextpage" value="/mg/main/main_portal.jsp"/> -->
		<input type="hidden" name="j_nextpage" value="/mg/main/su_main.jsp" />
		<div class="<%=logo_css%>">
			<div class="form">
				<table>
					<tr>
						<td><input type="text" name="j_username" class="text"
							tabindex="1" style="width: 200px;" value="" /></td>
						<td rowspan="2" style="padding-left: 5px"><a
							href="javascript:gologin();" tabindex="3"><img
								src="/ext/images/btn_login.gif" alt="로그인" /></a></td>
					</tr>
					<tr>
						<td><input type="password" name="j_password" class="text"
							tabindex="2" style="width: 200px;" value="" /></td>
					</tr>
				</table>
				<table>
					<tr>
						<colgroup>
							<col width="30%" />
							<col width="15%" />
							<col width="30%" />
						</colgroup>
						<td style='padding: 0 0 0 0; vertical-align: middle' align="left"><input
							name="CookieFlag" type='checkbox' border:solid 1 #AAAAAA'/><font
							size="1.5" font-family="Malgun Gothic">아이디 저장</font></td>
						<td><font size="1.5" font-family="Malgun Gothic">Language</font></td>
						<td><font size="1.5" font-family="Malgun Gothic"> <SELECT
								name="j_language" value="<%=j_language%>">
									<option value='ko'>Korean</option>
									<option value='en'>English</option>
							</SELECT>
						</font></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</body>
</html>
